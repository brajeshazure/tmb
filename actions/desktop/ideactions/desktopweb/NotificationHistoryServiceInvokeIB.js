/*
var gblNotificationDetailsCache = new Array();
var tempPushArray = new Array();
var gblNotificationHistoryModified = true;
var NotificationHomePageState = false;
var locale = null;
var ServiceResult = new Array();
*/
var tempGloIndex = null;
var tempGlo = null;
var gblDetailPageStateChanger = "start";
var gblCurrentNotfnOnDetails = null;
var getDetailsOfNotfn = null;
var selectedNotification = null;
//GET NOTIFICATION HISTORY
function getNotificationHistoryIB() {
    showLoadingScreenPopup();
    var inputParams = {};
    invokeServiceSecureAsync("notificationhistory", inputParams, getNotificationHistoryIBCB)
}

function getNotificationHistoryIBCB(status, resultset) {
    if (status == 400) {
        if (resultset["status"] == "1") {
            if (resultset["Results"] != null) {
                //DO NOTHING
            } else {
                showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
            }
            frmIBNotificationHome.lblNoNotfn.isVisible = true;
            frmIBNotificationHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
            //frmIBNotificationHome.btnCancel.text = "";
            //frmIBNotificationHome.btnCancel.skin = "lblWhiteBgBlackFnt";
            //frmIBNotificationHome.btnCancel.focusSkin = "lblWhiteBgBlackFnt";
            //	frmIBNotificationHome.btnCancel.setEnabled(true);
            //	frmIBNotificationHome.btnCancel.skin = "lblWhiteBgBlackFnt";
            //	frmIBNotificationHome.btnCancel.focusSkin = "lblWhiteBgBlackFnt";
            //	frmIBNotificationHome.btnCancel.hoverSkin = "lblWhiteBgBlackFnt";
            frmIBNotificationHome.btnCancel.setVisibility(false);
            //frmIBNotificationHome.btnRight.text = "DeleteDisabled";
            frmIBNotificationHome.btnRight.skin = "btnDeleteActive";
            frmIBNotificationHome.btnRight.focusSkin = "btnDeleteActive";
            frmIBNotificationHome.btnRight.setEnabled(false);
            frmIBNotificationHome.btnRight.isVisible = false;
            frmIBNotificationHome.vBoxRcRight.padding = [0, 50, 0, 0];
            frmIBNotificationHome.imgTMBLogo.isVisible = true;
            frmIBNotificationHome.scrollbox474082218273129.isVisible = false;
            frmIBNotificationHome.segRequestHistory.removeAll();
        } else if (resultset["status"] == "0") {
            //
            if (resultset["Results"].length > 0) {
                ServiceResult.length = 0;
                for (var i = 0; i < resultset["Results"].length; i++) {
                    ServiceResult.push(resultset["Results"][i]);
                }
                gblMaxSetImportant = resultset["maxsetimportant"];
                populateNotificationsNormalStateIB();
                frmIBNotificationHome.lblNoNotfn.isVisible = false;
                gblMYInboxCountFrom = true;
                unreadInboxMessagesTrackerLocalDBSyncIB();
            } else {
                frmIBNotificationHome.lblNoNotfn.isVisible = true;
                frmIBNotificationHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
                //	frmIBNotificationHome.btnCancel.setEnabled(true);
                //	frmIBNotificationHome.btnCancel.skin = "lblWhiteBgBlackFnt";
                //	frmIBNotificationHome.btnCancel.focusSkin = "lblWhiteBgBlackFnt";
                //	frmIBNotificationHome.btnCancel.hoverSkin = "lblWhiteBgBlackFnt";
                frmIBNotificationHome.btnCancel.setVisibility(false);
                //frmIBNotificationHome.btnRight.text = "DeleteDisabled";
                frmIBNotificationHome.btnRight.skin = "btnDeleteActive";
                frmIBNotificationHome.btnRight.focusSkin = "btnDeleteActive";
                frmIBNotificationHome.btnRight.setEnabled(false);
                frmIBNotificationHome.btnRight.isVisible = false;
                frmIBNotificationHome.vBoxRcRight.padding = [0, 50, 0, 0];
                frmIBNotificationHome.imgTMBLogo.isVisible = true;
                frmIBNotificationHome.scrollbox474082218273129.isVisible = false;
                frmIBNotificationHome.segRequestHistory.removeAll();
            }
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
            frmIBNotificationHome.segRequestHistory.removeAll();
        }
    }
    dismissLoadingScreenPopup();
}

function populateNotificationsNormalStateIB() {
    locale = kony.i18n.getCurrentLocale();
    NotificationHomePageState = false;
    if (gblDetailPageStateChanger == "start") {
        frmIBNotificationHome.vBoxRcRight.padding = [0, 50, 0, 0];
        frmIBNotificationHome.imgTMBLogo.isVisible = true;
        frmIBNotificationHome.scrollbox474082218273129.isVisible = false;
    } else if (gblDetailPageStateChanger == "delete") {
        if (gblCurrentNotfnOnDetails == null) {
            //LET THE PREVIOUS STATE REMAIN
        } else {
            getNotificationDetailsIB();
        }
    } else if (gblDetailPageStateChanger == "markimp" || gblDetailPageStateChanger == "notfndeletederror" || gblDetailPageStateChanger == "notfndetails") {
        //LET THE PREVIOUS STATE REMAIN
    }
    populateNotificationsIB();
    //frmIBNotificationHome.btnCancel.text = "";
    //frmIBNotificationHome.btnCancel.skin = "lblWhiteBgBlackFnt";
    //frmIBNotificationHome.btnCancel.focusSkin = "lblWhiteBgBlackFnt";
    //	frmIBNotificationHome.btnCancel.setEnabled(true);
    //	frmIBNotificationHome.btnCancel.skin = "lblWhiteBgBlackFnt";
    //	frmIBNotificationHome.btnCancel.focusSkin = "lblWhiteBgBlackFnt";
    //	frmIBNotificationHome.btnCancel.hoverSkin = "lblWhiteBgBlackFnt";
    frmIBNotificationHome.btnCancel.setVisibility(false);
    //frmIBNotificationHome.btnRight.text = "Delete";
    frmIBNotificationHome.btnRight.setEnabled(true);
    frmIBNotificationHome.btnRight.skin = "btnIBNotHomeDelNormal";
    frmIBNotificationHome.btnRight.focusSkin = "btnIBNotHomeDelFocus";
    frmIBNotificationHome.btnRight.isVisible = true;
    frmIBNotificationHome.segRequestHistory.onRowClick = getNotificationDetailsIB;
    frmIBNotificationHome.segRequestHistory.selectionBehavior = constants.SEGUI_DEFAULT_BEHAVIOR;
}

function populateNotificationsDeleteStateIB() {
    locale = kony.i18n.getCurrentLocale();
    NotificationHomePageState = true;
    frmIBNotificationHome.segRequestHistory.onRowClick = SelectForDeleteTrackerIB;
    frmIBNotificationHome.segRequestHistory.selectionBehavior = constants.SEGUI_MULTI_SELECT_BEHAVIOR;
    populateNotificationsIB();
    //frmIBNotificationHome.btnCancel.text = "Cancel";
    //	frmIBNotificationHome.btnCancel.setEnabled(true);
    frmIBNotificationHome.btnCancel.setVisibility(true);
    //   frmIBNotificationHome.btnCancel.skin = "btnCancel";
    //  frmIBNotificationHome.btnCancel.focusSkin = "btnCancel";
    //   frmIBNotificationHome.btnCancel.hoverSkin = "btnIBNotHomeBackFocus";
    //frmIBNotificationHome.btnRight.text = "DeleteNo";
    frmIBNotificationHome.btnRight.setEnabled(true);
    frmIBNotificationHome.btnRight.skin = "btnDeleteActive";
    frmIBNotificationHome.btnRight.focusSkin = "btnDeleteActive";
    frmIBNotificationHome.btnRight.isVisible = true;
}

function SelectForDeleteTrackerIB() {
    if (frmIBNotificationHome.segRequestHistory.selectedItems != null) {
        //frmIBNotificationHome.btnRight.text = "DeleteYes";
        frmIBNotificationHome.btnRight.skin = "btnIBNotHomeDelNormal";
        frmIBNotificationHome.btnRight.focusSkin = "btnIBNotHomeDelFocus";
    } else {
        //frmIBNotificationHome.btnRight.text = "DeleteNo";
        frmIBNotificationHome.btnRight.skin = "btnDeleteActive";
        frmIBNotificationHome.btnRight.focusSkin = "btnDeleteActive";
    }
}

function populateNotificationsIB() {
    gblNotificationDetailsCache.length = 0;
    var NotificationOnDate = new Array();
    var localeSup = "EN";
    var skinImp = null;
    var skinRowRead = null;
    if (locale == "th_TH") {
        localeSup = "TH";
    }
    var onclickBTNI = null;
    if (!NotificationHomePageState) {
        onclickBTNI = markNotificationImportantIB;
    }
    var date = ServiceResult[0]["date"];
    for (var i = 0; i < ServiceResult.length; i++) {
        skinImp = "btnImpN";
        //skinRowRead = "segRowReadN";
        skinRowRead = "hbxSkinSegGrey280Hover";
        skinLblNotfnType = "lblNicknameBold";
        skinlblNotfnSub = "lbltxt16pxBold";
        if (ServiceResult[i]["imp"] == "Y") {
            skinImp = "btnImpY";
        }
        if (ServiceResult[i]["read"] == "Y") {
            skinRowRead = "hbxSkinSegNormal280";
            skinLblNotfnType = "lblNickname";
            skinlblNotfnSub = "lbltxt16px";
        }
        if (date == ServiceResult[i]["date"]) {
            var timeR = cutTime(ServiceResult[i]["time"]);
            //var timeR = time24toAMPM(ServiceResult[i]["time"]);
            if (selectedNotification != null && selectedNotification.trim() != "" && ServiceResult[i]["id"].trim() == selectedNotification.trim()) {
                if (skinImp == "btnImpY") skinImp = "btnImpY";
                var tempRecord = {
                    hbox476793199281301: {
                        skin: "hbxSkinSegFocus280"
                    },
                    ID: ServiceResult[i]["id"],
                    lblNotfnType: {
                        text: ServiceResult[i]["type" + localeSup],
                        skin: skinLblNotfnType
                    },
                    lblNotfnSub: {
                        text: ServiceResult[i]["subject" + localeSup],
                        skin: skinlblNotfnSub
                    },
                    lblTime: {
                        text: timeR,
                        skin: "lblBlue80"
                    },
                    btnImp: {
                        skin: skinImp,
                        onClick: onclickBTNI
                    },
                    imgDeleteSelect: {
                        isVisible: NotificationHomePageState
                    },
                    imgArrow: {
                        src: "navarrow3.png"
                    }
                }
            } else {
                var tempRecord = {
                    hbox476793199281301: {
                        skin: skinRowRead
                    },
                    ID: ServiceResult[i]["id"],
                    lblNotfnType: {
                        text: ServiceResult[i]["type" + localeSup],
                        skin: skinLblNotfnType
                    },
                    lblNotfnSub: {
                        text: ServiceResult[i]["subject" + localeSup],
                        skin: skinlblNotfnSub
                    },
                    lblTime: {
                        text: timeR,
                        skin: "lblBlue80"
                    },
                    btnImp: {
                        skin: skinImp,
                        onClick: onclickBTNI
                    },
                    imgDeleteSelect: {
                        isVisible: NotificationHomePageState
                    },
                    imgArrow: {
                        src: "navarrow3.png"
                    }
                }
            }
            //
            NotificationOnDate.push(tempRecord);
        } else {
            var tempPushArray = new Array();
            var dateR = dateFormatTMB(date);
            tempPushArray.push({
                lblDateHeader: dateR
            });
            tempPushArray.push(NotificationOnDate);
            gblNotificationDetailsCache.push(tempPushArray);
            var NotificationOnDate = new Array();
            date = ServiceResult[i]["date"];
            i = i - 1;
        }
    }
    var tempPushArray = new Array();
    var dateR = dateFormatTMB(date);
    tempPushArray.push({
        lblDateHeader: dateR
    });
    tempPushArray.push(NotificationOnDate);
    gblNotificationDetailsCache.push(tempPushArray);
    var NotificationOnDate = new Array();
    frmIBNotificationHome.segRequestHistory.removeAll();
    frmIBNotificationHome.segRequestHistory.setData(gblNotificationDetailsCache);
    frmIBNotificationHome.segRequestHistory.setVisibility(true);
}
//GET NOTIFICATION DETAILS
function getNotificationDetailsIB() {
    showLoadingScreenPopup();
    frmIBNotificationHome.image2101262343726494.isVisible = true;
    var msg = null;
    var msgindex = null;
    if (getDetailsOfNotfn == null) {
        msg = frmIBNotificationHome.segRequestHistory.selectedItems[0].ID;
        msgindex = frmIBNotificationHome.segRequestHistory.selectedIndex;
    } else {
        msg = getDetailsOfNotfn;
    }
    var inputParams = {
        messageId: msg
    };
    tempGloIndex = msgindex;
    tempGlo = msg;
    selectedNotification = msg;
    getDetailsOfNotfn = null;
    invokeServiceSecureAsync("notificationdetails", inputParams, getNotificationDetailsIBCB)
}

function getNotificationDetailsIBCB(status, resultset) {
    if (status == 400) {
        if (resultset["status"] == "1") {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        } else if (resultset["status"] == "0") {
            if (resultset["Result"].length > 0) {
                tempNotfnDetails = {
                    lblNotfnTypeEN: resultset["Result"][0]["typeEN"],
                    lblNotfnTypeTH: resultset["Result"][0]["typeTH"],
                    lblNotfnSubEN: resultset["Result"][0]["subjectEN"],
                    lblNotfnSubTH: resultset["Result"][0]["subjectTH"],
                    lblNotfnMessageEN: resultset["Result"][0]["messageEN"],
                    lblNotfnMessageTH: resultset["Result"][0]["messageTH"],
                    lblNotfnDate: dateFormatTMB(resultset["Result"][0]["date"]) + ", " + cutTime(resultset["Result"][0]["time"]),
                    msgCODE: resultset["Result"][0]["msgCODE"],
                    executeDate: resultset["Result"][0]["executeDate"],
                    executeTime: resultset["Result"][0]["executeTime"],
                    dueDate: resultset["Result"][0]["dueDate"],
                    contentEN: resultset["Result"][0]["messageENParams"],
                    contentTH: resultset["Result"][0]["messageTHParams"]
                }
                gblDetailPageStateChanger = "notfndetails";
                populateNotiDetailsIB(); //kony.i18n.getCurrentLocale());
                getNotificationHistoryIB();
                frmIBNotificationHome.vBoxRcRight.padding = [0, 0, 0, 0];
                frmIBNotificationHome.imgTMBLogo.isVisible = false;
                frmIBNotificationHome.scrollbox474082218273129.isVisible = true;
                gblCurrentNotfnOnDetails = tempGlo;
            }
        } else if (resultset["status"] == "2") {
            showAlertRcMB(kony.i18n.getLocalizedString("keyMessageDeleted"), kony.i18n.getLocalizedString("info"), "info")
            gblDetailPageStateChanger = "notfndeletederror";
            //gblNotificationHistoryModified = true;
            //frmIBNotificationHome.show();
            getNotificationHistoryIB();
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    }
    dismissLoadingScreenPopup();
    gblMYInboxCountFrom = true;
    //unreadInboxMessagesTrackerLocalDBSyncIB();
}

function populateNotiDetailsIB() {
    var localeSup = "EN";
    if (locale == "th_TH") {
        localeSup = "TH";
    }
    frmIBNotificationHome.label474082218274029.text = tempNotfnDetails["lblNotfnType" + localeSup];
    frmIBNotificationHome.label474082218273331.text = tempNotfnDetails["lblNotfnSub" + localeSup];
    frmIBNotificationHome.lblNotfnDate.text = tempNotfnDetails["lblNotfnDate"];
    frmIBNotificationHome.lblNotfnMessage.text = tempNotfnDetails["lblNotfnMessage" + localeSup];
    if (tempNotfnDetails["msgCODE"] == "N02" || tempNotfnDetails["msgCODE"] == "N03") {
        frmIBNotificationHome.btnExecute.isVisible = true;
        frmIBNotificationHome.btnExecute.text = kony.i18n.getLocalizedString("Transfer");
        frmIBNotificationHome.btnExecute.onClick = ExecuteSend2SaveFromNotificationIB;
    } else if (tempNotfnDetails["msgCODE"] == "N04") {
        frmIBNotificationHome.btnExecute.isVisible = true;
        frmIBNotificationHome.btnExecute.text = kony.i18n.getLocalizedString("keyPay");
        //frmIBNotificationHome.btnExecute.onClick = ExecuteBeepAndBillFromNotificationIB;
        var validateExecution = compareDatesBB(kony.os.date("yyyy-mm-dd"), tempNotfnDetails["dueDate"]);
        if (validateExecution != null) {
            if (validateExecution == "less") {
                frmIBNotificationHome.btnExecute.skin = "btnIB158";
                frmIBNotificationHome.btnExecute.onClick = ExecuteBeepAndBillFromNotificationIB;
            } else if (validateExecution == "equal") {
                //validateExecution = compareTimes(getCurrentTime(), tempNotfnDetails["executeTime"]);
                //if(validateExecution == "less"){
                frmIBNotificationHome.btnExecute.skin = "btnIB158";
                frmIBNotificationHome.btnExecute.onClick = ExecuteBeepAndBillFromNotificationIB;
                //}
                //				else if(validateExecution == "more" || validateExecution == "less"){
                //					frmIBNotificationHome.btnExecute.skin = "btnDisabledGray";
                //					frmIBNotificationHome.btnExecute.onClick = null;
                //				}
            } else if (validateExecution == "more") {
                frmIBNotificationHome.btnExecute.skin = "btnIB158disabled";
                frmIBNotificationHome.btnExecute.onClick = null;
            }
        }
    } else {
        frmIBNotificationHome.btnExecute.isVisible = false;
    }
}
//DELETE NOTIFICATION/S
function deleteNotificationIB() {
    showLoadingScreenPopup();
    var messages = "";
    var length = frmIBNotificationHome.segRequestHistory.selectedItems.length;
    for (var i = 0; i < length; i++) {
        if (i == (length - 1)) {
            messages = messages + frmIBNotificationHome.segRequestHistory.selectedItems[i].ID;
            var idPrev = frmIBNotificationHome.segRequestHistory.selectedItems[i].ID;
            //frmIBNotificationHome.segRequestHistory.data[0][1][1].ID
            for (var j = 0; j < frmIBNotificationHome.segRequestHistory.data.length; j++) {
                for (var j1 = 0; j1 < frmIBNotificationHome.segRequestHistory.data[j][1].length; j1++) {
                    if (frmIBNotificationHome.segRequestHistory.data[j][1][j1].ID == idPrev) {
                        if (j1 == (frmIBNotificationHome.segRequestHistory.data[j][1].length - 1)) {
                            if (j == (frmIBNotificationHome.segRequestHistory.data.length - 1)) { //LAST
                                getDetailsOfNotfn = frmIBNotificationHome.segRequestHistory.data[0][1][0].ID;
                            } else { //NOT LAST
                                getDetailsOfNotfn = frmIBNotificationHome.segRequestHistory.data[j + 1][1][0].ID;
                            }
                        } else {
                            getDetailsOfNotfn = frmIBNotificationHome.segRequestHistory.data[j][1][j1 + 1].ID;
                        }
                        break;
                    }
                }
            }
        } else {
            messages = messages + frmIBNotificationHome.segRequestHistory.selectedItems[i].ID + ",";
        }
    }
    var inputParams = {
        messageId: messages
    };
    invokeServiceSecureAsync("notificationdelete", inputParams, deleteNotificationIBCB)
}

function deleteNotificationIBCB(status, resultset) {
    if (status == 400) {
        if (resultset["status"] == "1") {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        } else if (resultset["status"] == "0") {
            gblDetailPageStateChanger = "delete";
            //gblNotificationHistoryModified = true;	
            //frmIBNotificationHome.show();
            getNotificationHistoryIB();
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    }
    dismissLoadingScreenPopup();
}
//MARK NOTIFICATION IMPORTANT
function markNotificationImportantIB() {
    showLoadingScreenPopup();
    var imp = frmIBNotificationHome.segRequestHistory.selectedItems[0]["btnImp"]["skin"];
    var ind = frmIBNotificationHome.segRequestHistory.selectedIndex;
    var imp1 = ind[0];
    var imp2 = ind[1];
    var msgID = gblNotificationDetailsCache[imp1][1][imp2]["ID"];
    var impStat = "Y";
    if (imp == "btnImpY") {
        impStat = "N";
    }
    var inputParams = {
        messageId: msgID,
        impStatus: impStat
    };
    invokeServiceSecureAsync("notificationimportant", inputParams, markNotificationImportantIBCB)
}

function markNotificationImportantIBCB(status, resultset) {
    if (status == 400) {
        if (resultset["status"] == "1") {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        } else if (resultset["status"] == "0") {
            gblDetailPageStateChanger = "markimp";
            //gblNotificationHistoryModified = true;
            //frmIBNotificationHome.show();
            getNotificationHistoryIB();
        } else if (resultset["status"] == "2") {
            if (resultset["error"] == "MaxLimitReached") {
                dismissLoadingScreenPopup();
                showAlertRcMB(kony.i18n.getLocalizedString("keyMessagesMaxLimit") + " " + gblMaxSetImportant + " " + kony.i18n.getLocalizedString("keyMessagesMaxLimit1"), kony.i18n.getLocalizedString("info"), "info");
                frmIBNotificationHome.segRequestHistory.removeAll();
                frmIBNotificationHome.segRequestHistory.setData(gblNotificationDetailsCache);
            } else if (resultset["error"] == "NotificationDeleted") {
                gblDetailPageStateChanger = "notfndeletederror";
                //gblNotificationHistoryModified = true;
                //frmIBNotificationHome.show();
                getNotificationHistoryIB();
            }
        } else {
            showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
        }
    }
    dismissLoadingScreenPopup();
}

function popupDeleteConfirmIB() {
    popUpNotfnDeleteIB.dismiss();
    deleteNotificationIB();
}

function popupDeleteCancelIB() {
    popUpNotfnDeleteIB.dismiss();
}

function ExecuteBeepAndBillFromNotificationIB() {
    var locale = kony.i18n.getCurrentLocale();
    var result = ""
    if (locale = "en_US") {
        result = tempNotfnDetails["contentEN"].split("|");
    } else {
        result = tempNotfnDetails["contentTH"].split("|");
    }
    var bnb = new Object();
    bnb["pinCode"] = result[0];
    bnb["dueDate"] = tempNotfnDetails["dueDate"];
    bnb["toAcctNickname"] = result[2];
    bnb["ref1"] = result[3];
    bnb["finTxnAmount"] = result[4];
    bnb["txnFeeAmount"] = result[5];
    bnb["BBCompcode"] = result[6];
    bnb["ref2"] = "";
    if (result.length > 7) bnb["ref2"] = result[7];
    customerPaymentStatusInquiryForBBNotificationHistory(bnb);
}

function ExecuteSend2SaveFromNotificationIB() {
    gblExeS2S = true;
    gbls2sEditFlag = "false";
    gblSSApply = "false";
    invokeRetailLiquidityTransferInqForIB();
}

function NotificationIBLocaleChanger() {
    if (locale == kony.i18n.getCurrentLocale()) {
        //NO CHANGE
    } else {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            locale = "en_US";
        } else {
            locale = "th_TH";
        }
        frmIBNotificationHome.label474082218268567.text = kony.i18n.getLocalizedString("keylblNotification");
        frmIBNotificationHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
        frmIBNotificationHome.label476822990280082.text = kony.i18n.getLocalizedString("keylblNotificationDetails");
        populateNotificationsIB();
        populateNotiDetailsIB();
    }
}

function syncIBNotfnInboxLocale() {
    if (kony.application.getCurrentForm().id == "frmIBNotificationHome") {
        NotificationIBLocaleChanger();
    }
    if (kony.application.getCurrentForm().id == "frmIBInboxHome") {
        InboxIBLocaleChanger();
    }
}

function clearGVNotificationInboxIB() {
    //NOTIFICATION
    tempGloIndex = null;
    tempGlo = null;
    gblDetailPageStateChanger = "start";
    gblCurrentNotfnOnDetails = null;
    getDetailsOfNotfn = null;
    selectedNotification = null;
    //-------------
    gblNotificationDetailsCache = new Array();
    tempPushArray = new Array();
    gblNotificationHistoryModified = true;
    NotificationHomePageState = false;
    locale = kony.i18n.getCurrentLocale();
    ServiceResult = new Array();
    tempNotfnDetails = null;
    //INBOX
    SortByTypeIN = "Date/Time";
    SortByOrderIN = "ascending";
    tempGloIN = null;
    gblDetailPageStateChangerIN = "start";
    gblCurrentNotfnOnDetailsIN = null;
    getDetailsOfNotfnIN = null;
    //-------------
    gblUnreadCount = "0";
    gblInboxDetailsCache = new Array();
    gblInboxDetailsCacheForSort = new Array();
    tempPushArrayInbox = new Array();
    gblInboxHistoryModified = true;
    InboxHomePageState = false;
    var ServiceResultInbox = new Array();
    SortByType = "Date/Time";
    SortByOrder = "ascending";
    gblInboxSortedOrNormal = false;
    tempInboxDetails = null;
}

function unreadInboxMessagesTrackerLocalDBSyncIB() {
    //showLoadingScreen();
    showLoadingScreenPopup();
    var inputParams = {};
    invokeServiceSecureAsync("getinboxunreadcount", inputParams, unreadInboxMsgTrackerLocalDBSyncCBIB);
}

function unreadInboxMsgTrackerLocalDBSyncCBIB(status, resultset) {
    if (status == 400) {
        if (resultset["status"] == "1") {
            gblMYInboxCount = "0";
        } else if (resultset["status"] == "0") {
            gblMYInboxCount = resultset["unread"];
        } else {
            gblMYInboxCount = "0";
        }
        var inputParams = {};
        invokeServiceSecureAsync("getInboxMessagesUnreadCount", inputParams, unreadInboxMessagesTrackerLocalDBSyncCBIB);
    }
}

function unreadInboxMessagesTrackerLocalDBSyncCBIB(status, resultset) {
    if (status == 400) {
        if (resultset["status"] == "1") {
            gblMyInboxMSGTotalCount = "0";
        } else if (resultset["status"] == "0") {
            gblMyInboxMSGTotalCount = resultset["unread"];
        } else {
            gblMyInboxMSGTotalCount = "0";
        }
        //dismissLoadingScreenPopup();				#Commented as fix for DEF15894#
        gblMyInboxTotalCount = parseInt(gblMYInboxCount) + parseInt(gblMyInboxMSGTotalCount);
        if (gblMYInboxCountFrom == false) {
            /* removing as per ENH_028 - IB Menu on Home Screen
            frmIBPostLoginDashboard.btnMenuMyInbox.text = gblMyInboxTotalCount
            */
        } else {
            var currForm = kony.application.getCurrentForm();
            currForm.btnMenuMyInbox.text = (parseInt(gblMyInboxTotalCount) == 0 ? "" : gblMyInboxTotalCount);;
            //segMyInboxLoadRefresh();
            gblMenuSelection = 3;
            segMyInboxLoad();
        }
        gblMYInboxCountFrom = false;
    }
}

function frmIBNotificationHome_frmIBNotificationHome_postShow() {
    addIBMenu();
    selectedNotification = null;
    frmIBNotificationHome.image2101262343726494.isVisible = false;
    getNotificationHistoryIB();
    destroyAllForms();
}
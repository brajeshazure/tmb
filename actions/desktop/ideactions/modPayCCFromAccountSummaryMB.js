BillerList = null;

function onClickCreditAcctSummary() {
 	showLoadingScreen();
 if(checkMBUserStatus()){
	gbl_accId = gblAccountTable["custAcctRec"][glb_accountId]["accId"];
	//gbl_accId = rem
	gbl_accId = gbl_accId.substring(8,gbl_accId.length )
	//
	
	gblReference1 = gbl_accId; 
	gblMyBillerTopUpBB = 0;
	gblAccountType = gblAccountTable["custAcctRec"][glb_accountId]["accType"];
    gblProductCode = gblAccountTable["custAcctRec"][glb_accountId]["productID"];
    
    gblMyBillerTopUpBB = 0;
    gblBillerPresentInMyBills = false;
    gblFromAccountSummary = true;
	getCompcodeMB(gblProductCode, gblAccountType)//"0699";
	
    }
}
function onclickCreditfromDetails() {
	showLoadingScreen();
	if(checkMBUserStatus()){
	    gbl_accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	  	gbl_accId = gbl_accId.substring(8,gbl_accId.length )
	  	gblAccountType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
	    gblProductCode = gblAccountTable["custAcctRec"][gblIndex]["productID"];
		gblMyBillerTopUpBB = 0;
		gblReference1 = gbl_accId;
		
		gblMyBillerTopUpBB = 0;
    	gblBillerPresentInMyBills = false;
		
		getCompcodeMB(gblProductCode, gblAccountType)//"0699";
	}
}

function masterBillerForCreditCardLoanMB() {
 var inputParams = {
  IsActive: "1",
  BillerGroupType: gblGroupTypeBiller,
  flagBillerList : "MB"
 };
 invokeServiceSecureAsync("masterBillerInquiry", inputParams, masterBillerForAccountSummaryCallBackMB);
}
function masterBillerForAccountSummaryCallBackMB(status, callBackResponse) {
 
 if (status == 400) {
  if (callBackResponse["opstatus"] == "0") {
   var responseData = callBackResponse["MasterBillerInqRs"];
	   if (responseData.length > 0) {
			for (var i = 0; i < responseData.length; i++) {
				if (responseData[i]["BillerCompcode"] == gblCompCode) {
								
					//Setting required global variables to complete the bill payment
					
					gblCompCode = responseData[i]["BillerCompcode"];
					gblBillerId = responseData[i]["BillerID"];
					gblbillerGroupType = responseData[i]["BillerGroupType"];
					gblBillerBancassurance = responseData[i]["IsBillerBancassurance"];
					gblAllowRef1AlphaNum = responseData[i]["AllowRef1AlphaNum"];	
					
			        gblRef1LblEN = responseData[i]["LabelReferenceNumber1EN"];
			        gblRef2LblEN = responseData[i]["LabelReferenceNumber2EN"];
			     	gblRef1LblTH = responseData[i]["LabelReferenceNumber1TH"];
			        gblRef2LblTH = responseData[i]["LabelReferenceNumber2TH"];
					
					gblRef1LenMB = responseData[i]["Ref1Len"];
					gblRef2LenMB = responseData[i]["Ref2Len"];
					                        
					gblreccuringDisablePay = responseData[i]["IsRequiredRefNumber2Pay"];
					gblreccuringDisableAdd = responseData[i]["IsRequiredRefNumber2Add"];
			
					gblBillerMethod = responseData[i]["BillerMethod"];
					gblPayFull = responseData[i]["IsFullPayment"];
					
					gblBillerCompCodeEN = responseData[i]["BillerNameEN"] + " (" + gblCompCode + ")"; 
					gblBillerCompCodeTH = responseData[i]["BillerNameTH"] + " (" + gblCompCode + ")";
					
					gblToAccountKey = responseData[i]["ToAccountKey"];
					
					break;
				}
			}
			
			callCustomerBillerInqForCredit();
	   } else {
	    dismissLoadingScreen();
	    alert("No Suggested Biller  found");
	   }
  } else {
   dismissLoadingScreen();
   alert("No Suggested Biller found");
  }
 } else {
	  if (status == 300) {
	   	dismissLoadingScreen();
	   	alert("No Suggested Biller found");
	  }
 	}
}
function callCustomerBillerInqForCredit(){
var inputParams = {
		crmId: gblcrmId,
		IsActive: "1",
		clientDate: getCurrentDate()
	};
	//showLoadingScreen();
	invokeServiceSecureAsync("customerBillInquiry", inputParams, callCustomerBillerInqForCreditCallBack);
	
}

function callCustomerBillerInqForCreditCallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
        	var nickName = "";
            var responseData = callBackResponse["CustomerBillInqRs"];
            gblMyBillList = [];
            if (responseData.length > 0) {
                gblMyBillList = responseData;
                for (var i = 0; i < responseData.length; i++) {
                    if (gblCompCode == responseData[i]["BillerCompcode"] && gblReference1 == responseData[i]["ReferenceNumber1"]) {
                    	if(responseData[i]["IsRequiredRefNumber2Add"] == "Y") {
							if(responseData[i]["ReferenceNumber2"] == gblReference2) {
								gblBillerPresentInMyBills = true;
								nickName = responseData[i]["BillerNickName"] ;
								gblBillerID = responseData[i]["CustomerBillID"] ;
								break;
							}
						} else {
							gblBillerPresentInMyBills = true;
                    		nickName = responseData[i]["BillerNickName"] ;
							gblBillerID = responseData[i]["CustomerBillID"] ;
							break;
						}
                    }
                }
             } 
               
			clearBillpaymentScreen();			
			launchBillPaymentFirstTime();
			gblFirstTimeBillPayment = false;
				
			frmBillPayment.lblForFullPayment.text = ""; 
          	frmBillPayment.tbxAmount.text = "";
			frmBillPayment.imgBillerImage.src =  BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblCompCode + "&modIdentifier=MyBillers";
			frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
			frmBillPayment.hbxPenalty.setVisibility(false);
			frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
			frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
			frmBillPayment.hbxFullSpecButton.setVisibility(false);
			frmBillPayment.hbxPenalty.setVisibility(false);
			frmBillPayment.lblForFullPayment.setVisibility(false);
			frmBillPayment.hbxRef.setVisibility(true);
			frmBillPayment.hbxRef1.setVisibility(true);
			frmBillPayment.hbxRef2.setVisibility(true);
			
			var locale = kony.i18n.getCurrentLocale();
			if (locale=="en_US") {
				frmBillPayment.lblCompCode.text = shortenBillerName(gblBillerCompCodeEN, 18);
				frmBillPayment.lblRef1.text = gblRef1LblEN;
			} else {
				frmBillPayment.lblCompCode.text = shortenBillerName(gblBillerCompCodeTH, 18);
				frmBillPayment.lblRef1.text = gblRef1LblTH;
			}
			
			if(isNotBlank(nickName)) {
				frmBillPayment.lblCompCode.text = nickName;
			}
			
			if(gblBillerBancassurance == "Y" || gblAllowRef1AlphaNum == "Y") {
				frmBillPayment.lblRef1Value.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
				frmBillPayment.lblRef1Value.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT;
			} else {
				frmBillPayment.lblRef1Value.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
				frmBillPayment.lblRef1Value.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
			}
			
			frmBillPayment.lblRef1Value.text = gblReference1;
			frmBillPayment.lblRef1Value.maxTextLength=parseInt(gblRef1LenMB);
			//below line is added for CR - PCI-DSS masked Credit card no
			frmBillPayment.lblRef1ValueMasked.text = maskCreditCard(gblReference1);

            if (!frmBillPayment.hbxRef1.isVisible) {
                frmBillPayment.hbxRef1.setVisibility(true);
                //frmBillPayment.lineRef2.setVisibility(true);
            }
            if (gblreccuringDisablePay == "Y") {
                frmBillPayment.hbxRef2.setVisibility(true);
                frmBillPayment.lineRef1.setVisibility(true);
                if (locale=="en_US") {
                 	frmBillPayment.lblRef2.text = appendColon(gblRef2LblEN);
                } else {
                	frmBillPayment.lblRef2.text = appendColon(gblRef2LblTH);
                }
               
				frmBillPayment.tbxRef2Value.text = gblReference2;
				frmBillPayment.tbxRef2Value.maxTextLength=parseInt(gblRef2LenMB);
            } else {
                frmBillPayment.hbxRef2.setVisibility(false);
                frmBillPayment.lineRef1.setVisibility(false);
            }
                
				if(gblBillerBancassurance == "Y") {
					
					gblBillPaymentEdit =true;
					gblPenalty = false;
					gblFullPayment = false;
					frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
					frmBillPayment.hbxPenalty.setVisibility(false);
					frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
					frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
					frmBillPayment.hbxFullSpecButton.setVisibility(false);
					frmBillPayment.tbxAmount.text = numberWithCommas(removeCommos(gblBAPolicyAmount));
					frmBillPayment.tbxRef2Value.text = gblReference2; 
						
					//Ref2 should not editable
					frmBillPayment.tbxRef2Value.setEnabled(false);
     				frmBillPayment.tbxAmount.setEnabled(false);
     				frmBillPayment.tbxAmount.setVisibility(true);
					callBillPaymentCustomerAccountService();
				} else {
				    frmBillPayment.tbxRef2Value.setEnabled(true);
     				frmBillPayment.tbxAmount.setEnabled(true);
     				if(gblAccountType == "LOC"){
				    	populateLoanOnBillers(gblReference1, gblReference2)
				    }else if(gblAccountType == "CCA"){
				    	populateCreditOnbillers(gblReference1);
				    }
                   	
			  	}
			}
    	}
  }
  
function populateCreditOnbillers(cards) {

			//for CreditCard and Ready cash
			var inputParam = {};
			var toDayDate = getTodaysDate();
			
			var cardId = cards
			//	var cardId = "000010114966940108000010";
			
			//inputParam["cardId"]= frmBillPayment.lblRef1Value.text;
			inputParam["cardId"] = cardId;
			//inputParam["waiverCode"] = "I";
			inputParam["tranCode"] = TRANSCODEMIN;
			inputParam["postedDt"] = toDayDate;
			invokeServiceSecureAsync("creditcardDetailsInq", inputParam, BillPaymentcreditcardDetailsInqServiceCallBackMB);
			frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
			gblPenalty = false;
			frmBillPayment.hbxPenalty.setVisibility(false);
			frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
			frmBillPayment.hbxFullSpecButton.setVisibility(false);
			// if (!frmBillPayment.hbxFullMinSpecButtons.isVisible) {
			frmBillPayment.hbxFullMinSpecButtons.setVisibility(true);
			//frmBillPayment.lblForFullPayment.text = frmSelectBiller["segMyBills"]["selectedItems"][0]["fullAmount"] + "\u0E3F";
			frmBillPayment.button475004897849.skin = btnScheduleEndLeftFocus;
			frmBillPayment.button475004897851.skin = btnScheduleMid;
			frmBillPayment.button475004897853.skin = btnScheduleEndRight;
			//}
}
function getCompcodeMB(productCode ,AccountType){
	var inputParam  = {};
	inputParam["productCode"] = productCode;
	inputParam["accountType"] = AccountType;
	invokeServiceSecureAsync("getBillerCompCode", inputParam, getCompCodeCallBackCCLoanMB);
}
function getCompCodeCallBackCCLoanMB(status, result){
	if(status == "400"){
		if(result["opstatus"] = "0"){
		    if(isNotBlank(result["compCode"])){
		    	gblCompCode = result["compCode"];
				masterBillerForCreditCardLoanMB();
		    } else {
		    	alert("CompCode Not Found");
		    	dismissLoadingScreen();
		    	return;
		    }
		}
	}
}
 function BillPaymentcreditcardDetailsInqServiceCallBackMB(status, result) {

        fullAmt = "0.00";
        minAmt = "0.00";
        if (status == 400) //success responce
        {
            
            if (result["opstatus"] == 0) {
                var StatusCode = result["StatusCode"];
                var StatusDesc = result["StatusDesc"]
                /** checking for Soap status below  */
                
                if (StatusCode != 0) {
                	dismissLoadingScreen();
                    alert(result["errMsg"]);
                    return false;
                } else {

                    fullAmt = result["fullPmtAmt"];
                    if(parseInt(fullAmt)<0)gblPaymentOverpaidFlag=true;
                    minAmt = result["minPmtAmt"]
                    
                    if (fullAmt == "") {
                        fullAmt = "0.00";
                    }
                    if (minAmt == "") {
                        minAmt = "0.00";
                    }
                    
                }
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
			gblPenalty = false;
			frmBillPayment.hbxPenalty.setVisibility(false);
			frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
			frmBillPayment.hbxFullSpecButton.setVisibility(false);
			
				frmBillPayment.hbxFullMinSpecButtons.setVisibility(true);
				frmBillPayment.button475004897849.skin = btnScheduleEndLeftFocus;
				frmBillPayment.button475004897851.skin = btnScheduleMid;
				frmBillPayment.button475004897853.skin = btnScheduleEndRight;
				frmBillPayment.lblForFullPayment.text = numberWithCommas(removeCommos(fullAmt));
                frmBillPayment.lblForFullPayment.setVisibility(true);
                frmBillPayment.tbxAmount.setVisibility(false);
                frmBillPayment.tbxAmount.text = numberWithCommas(removeCommos(fullAmt));
                frmBillPayment.hbxFullSpecButton.setVisibility(false);
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(true);
                gblFullPayment = true;
                gblFirstTimeBillPayment =false;
                callBillPaymentCustomerAccountService();
                
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);

            }
        }
}

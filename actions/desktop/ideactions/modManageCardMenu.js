/*
 * Function on frmMBManageCard
 */
function frmMBManageCardMenuPreshow(){
	frmMBManageCardLocalePreshow();
	preShowfrmMBManageCard();
}

function frmMBManageCardLocalePreshow(){
	changeStatusBarColor();
	if(gblSelectedCard["cardType"] == "Debit Card"){
		frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("DBA_ManageDebitCard")
		frmMBManageCard.lblBlockCard.text = kony.i18n.getLocalizedString("DBA_BlockCard");
  	} else {
  		if(gblSelectedCard["cardType"] == "Credit Card"){
  			frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("keyManageCreditCard")
  		} else if(gblSelectedCard["cardType"] == "Ready Cash"){
  			frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("keyManageReadyCashCard")
		}
		frmMBManageCard.lblBlockCard.text = kony.i18n.getLocalizedString("btnBlockCard");
  	}
	
	frmMBManageCard.lblCashAdvance.text = kony.i18n.getLocalizedString("btnRequestCashAdvance");
	frmMBManageCard.lblChangePin.text = kony.i18n.getLocalizedString("CCP01_Instruction");
	frmMBManageCard.lblFullStm.text = kony.i18n.getLocalizedString("btnSeeFullStatement");
	frmMBManageCard.lblRequestPin.text = kony.i18n.getLocalizedString("CRP01_Instruction");
	frmMBManageCard.lblWaitPin.text = kony.i18n.getLocalizedString("CRP01_Instruction");
	frmMBManageCard.lblWaitPINDesc.text = kony.i18n.getLocalizedString("msgNotAllowRequestNewPIN");
	frmMBManageCard.buttonBack.text = kony.i18n.getLocalizedString("Back");
	
	var cardNoSkin = "lblDarkGrey36px";
	var gblDeviceInfo = kony.os.deviceInfo();

	var screenwidth = gblDeviceInfo["deviceWidth"];
	
	if(screenwidth > 640) {
		frmMBManageCard.lblWaitPINDesc.skin = "lblDarkGrey36px";
	} else {
		frmMBManageCard.lblWaitPINDesc.skin = "lblDarkGray150";
	}
}

function preShowfrmMBManageCard(){
	var cardNoSkin = getCardNoSkin();
	frmMBManageCard.lblCardNumber.skin = cardNoSkin;
	if(gblSelectedCard["cardType"] == "Debit Card"){
		frmMBManageCard.btnBlockCard.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnBlockCard.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
		frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("DBA_ManageDebitCard")
		frmMBManageCard.flxRequestPIN.setVisibility(false);
		frmMBManageCard.flxChangePIN.setVisibility(false);
		frmMBManageCard.flxCashAdvance.setVisibility(false);
		frmMBManageCard.flxBlockCard.setVisibility(true);
		frmMBManageCard.flxFullStm.setVisibility(false);
		frmMBManageCard.flxWaitPin.setVisibility(false);
  	} else {
  	
  		//for hover
  		frmMBManageCard.btnBlockCard.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnBlockCard.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
  		frmMBManageCard.btnChangePin.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnChangePin.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
  		frmMBManageCard.btnRequestPin.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnRequestPin.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
		frmMBManageCard.btnCashAdvance.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnCashAdvance.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
  		frmMBManageCard.btnFullStm.onTouchStart = frmMBManageCardOnTouchStart;
  		frmMBManageCard.btnFullStm.onTouchEnd = frmMBManageCardOnTouchEnd;
  		
  		if(gblSelectedCard["cardType"] == "Credit Card"){
  			frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("keyManageCreditCard")
  		} else if(gblSelectedCard["cardType"] == "Ready Cash"){
  			frmMBManageCard.lblManageCardTitle.text = kony.i18n.getLocalizedString("keyManageReadyCashCard")
		}
  		frmMBManageCard.flxBlockCard.setVisibility(true);
		frmMBManageCard.flxChangePIN.setVisibility(true);
		frmMBManageCard.flxCashAdvance.setVisibility(true);
		frmMBManageCard.flxFullStm.setVisibility(true);
		
		if(gblSelectedCard["allowRequestNewPin"] == "N"){
			frmMBManageCard.flxRequestPIN.setVisibility(false);
			frmMBManageCard.flxWaitPin.setVisibility(true);
		} else {
			frmMBManageCard.flxRequestPIN.setVisibility(true);
			frmMBManageCard.flxWaitPin.setVisibility(false);
		}
  	}
}

function frmMBManageCardOnTouchStart(eventobject){
	if (eventobject.id == "btnBlockCard") {
		frmMBManageCard.flxBlockCard.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnChangePin") {
		frmMBManageCard.flxChangePIN.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnRequestPin") {
		frmMBManageCard.flxRequestPIN.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnCashAdvance") {
		frmMBManageCard.flxCashAdvance.skin = "flexGreyBGBlueBorder";
	}else if (eventobject.id == "btnFullStm") {
		frmMBManageCard.flxFullStm.skin = "flexGreyBGBlueBorder";
	}
}
function frmMBManageCardOnTouchEnd(eventobject){
	if (eventobject.id == "btnBlockCard") {
		frmMBManageCard.flxBlockCard.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnChangePin") {
		frmMBManageCard.flxChangePIN.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnRequestPin") {
		frmMBManageCard.flxRequestPIN.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnCashAdvance") {
		frmMBManageCard.flxCashAdvance.skin = "flexWhiteBGBlueBorder";
	}else if (eventobject.id == "btnFullStm") {
		frmMBManageCard.flxFullStm.skin = "flexWhiteBGBlueBorder";
	}
}

function getCallbackShowManageCard(status, resulttable){
    var cardType = "";
    gblManageCardFlow = kony.application.getCurrentForm().id;
    if (status == 400) {
    	dismissLoadingScreen();
	    if (resulttable["opstatus"] == 0) {
	    	if(resulttable["errCode"] == undefined) {
	    		gblCardList = resulttable;
		        if (resulttable["creditCardRec"].length > 0) {
		            gblSelectedCard = resulttable["creditCardRec"][0]; 
				}     
		    	if (resulttable["readyCashRec"].length > 0) {
		            gblSelectedCard = resulttable["readyCashRec"][0];
		        }  
		        setGblSelectedCardData();
		        gotoManageCard(gblSelectedCard);
	    	} else {
		    	showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
	            return false;
	    	}
        } else{
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}
function onClickBackManageListCard(){
	if(gblManageCardFlow == "frmMBCardList") {
		if(gblisCardActivationCompleteFlow) {
			frmCardActivationDetails.destroy();
			frmCardActivationComplete.destroy();
			gblisCardActivationCompleteFlow = false;
			preShowMBCardList();
		} else {
			frmMBCardList.show();
		}
	} else if(gblManageCardFlow == "frmMBListDebitCard") {
		if(gblisCardActivationCompleteFlow) {
			frmCardActivationDetails.destroy();
			frmCardActivationComplete.destroy();
			gblisCardActivationCompleteFlow = false;
			callDebitCardInqService();
		} else {
			frmMBListDebitCard.show();
		}
	} else if(gblManageCardFlow == "frmAccountDetailsMB") {
		showAccountDetails({"id":"hboxSwipe"+gblIndex});
	} else {
		preShowMBCardList();
	}
}
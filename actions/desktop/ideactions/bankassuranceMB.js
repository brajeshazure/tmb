function MBcallBAPolicyListService(){

	//#ifdef iphone
		TMBUtil.DestroyForm(frmMBBankAssuranceSummary);		
	//#endif

	var inputParam = {};
	showLoadingScreen();
    invokeServiceSecureAsync("BAGetPolicyList", inputParam, callBAPolicyListServiceCallBackMB);
}

function callBAPolicyListServiceCallBackMB(status,resulttable){
	if (status == 400) {
		dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
        	gblBASummaryData=resulttable;
        	gblEmailId=resulttable["emailAddr"];
	        //
			if (kony.i18n.getCurrentLocale() == "en_US"){
				frmMBBankAssuranceSummary.lblAccntHolderName.text = gblCustomerName;
			}else{
				frmMBBankAssuranceSummary.lblAccntHolderName.text = gblCustomerNameTh;
			}
			frmMBBankAssuranceSummary.label475124774164.text=kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
			frmMBBankAssuranceSummary.lblBalanceValue.text=kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured");
			frmMBBankAssuranceSummary.btnBack1.text = kony.i18n.getLocalizedString("Back");
	        frmMBBankAssuranceSummary.imgProfile.src = frmAccountSummaryLanding.imgProfile.src;
			var totalSumInsured = gblAccountTable["totalSumInsured"];
			if(totalSumInsured != "-") {
				totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			frmMBBankAssuranceSummary.lblInvestmentValue.text = totalSumInsured;
			frmMBBankAssuranceSummary.show();
			frmMBBankAssuranceSummary.scrollboxMain.scrollToEnd();
	    }
	    else {
	       	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
        }
	}
}

function populateDataBankAssuranceSummaryMB(dataBankAssurance){
		
		var policyHeadersDS = dataBankAssurance["policyHeadersDS"];
		var policyDetailsDS = dataBankAssurance["policyDetailsDS"];
		var locale= kony.i18n.getCurrentLocale();
		var segmentData = [];
		for(var i=0;i<policyDetailsDS.length;i++){

			var companyName= "";
			var companynamePrev = "";
			var sumInsuredLabel="";
			var policyName = "";
			var isFlag = "";
			var logo="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
						 "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+"BA_"+policyDetailsDS[i]["DATA_SOURCE_VALUE_EN"]+"&modIdentifier=MFLOGOS";
			
			if(policyDetailsDS[i]["ALLOW_PAYMENT_VALUE_EN"] == "1"){
				isFlag = {"src":"ico_flag_active.png"};
			}
			
			if(locale == "en_US"){
				companyName = policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"];
				policyName = policyDetailsDS[i]["POLICY_UNIQUE_NAME_VALUE_EN"];
				sumInsuredLabel = policyHeadersDS[0]["SUM_INSURE_EN"]
				if(i!=0)
					companynamePrev = policyDetailsDS[i-1]["COMPANY_NAME_VALUE_EN"];
			}else{
				companyName = policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"];
				policyName = policyDetailsDS[i]["POLICY_UNIQUE_NAME_VALUE_TH"];
				sumInsuredLabel = policyHeadersDS[0]["SUM_INSURE_TH"]
				if(i!=0)
					companynamePrev = policyDetailsDS[i-1]["COMPANY_NAME_VALUE_TH"];
			}
			if(companynamePrev != companyName){
					var dataObjectHeader = {
					lblHead: companyName,
					lblHeadEN: policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"],
					lblHeadTH: policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"],
					template:MBMFHeader
				};
				segmentData.push(dataObjectHeader);
			}
				
			 var dataObject	= {
								imgLogo:logo,
								imgPayFlag:isFlag,
								imageRightArrow:{"src":"navarrow.png"},
								companyNameEN : policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"],
								companyNameTH : policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"],
								policyNameEN : policyDetailsDS[i]["POLICY_NAME_VALUE_EN"],
								policyNameTH : policyDetailsDS[i]["POLICY_NAME_VALUE_TH"],
								lblPolicyName : policyName,
								lblPolicyNumber : policyDetailsDS[i]["POLICY_NO_VALUE_EN"],
								lblSumInsured : sumInsuredLabel+":",
								sumInsuredEN : policyHeadersDS[0]["SUM_INSURE_EN"]+":",
								sumInsuredTH : policyHeadersDS[0]["SUM_INSURE_TH"]+":",
								lblSumInsuredValue : commaFormatted(policyDetailsDS[i]["SUM_INSURE_VALUE_EN"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
								allowPayBill: policyDetailsDS[i]["ALLOW_PAYMENT_VALUE_EN"],
								allowTaxDoc: policyDetailsDS[i]["ALLOW_TAX_DOCUMENT_VALUE_EN"],
							    template:MBBAContent
						    
						}
				
			segmentData.push(dataObject);
		}
		return segmentData;
}


function frmMBBankAssuranceSummaryPreShow(){
	changeStatusBarColor();
	if (kony.i18n.getCurrentLocale() == "en_US"){
		frmMBBankAssuranceSummary.lblAccntHolderName.text = gblCustomerName;
	}
	else{
		frmMBBankAssuranceSummary.lblAccntHolderName.text = gblCustomerNameTh;
	}
	frmMBBankAssuranceSummary.label475124774164.text=kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
	frmMBBankAssuranceSummary.lblBalanceValue.text=kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured");
	//MIB-1418  > MIB-1669
	frmMBBankAssuranceSummary.btnBack1.text = kony.i18n.getLocalizedString("Back");
	//MIB-1418  > MIB-1669
	var segmentData= populateDataBankAssuranceSummaryMB(gblBASummaryData);
         
	frmMBBankAssuranceSummary.segAccountDetails.widgetDataMap ={
		lblHead: "lblHead",
       	imgLogo:"imgLogo",
		lblPolicyName:"lblPolicyName",
		lblPolicyNumber: "lblPolicyNumber",
		lblSumInsured:"lblSumInsured",
		lblSumInsuredValue:"lblSumInsuredValue",
		imageRightArrow:"imageRightArrow",
		imgPayFlag:"imgPayFlag"
    };
    frmMBBankAssuranceSummary.segAccountDetails.removeAll();
   	frmMBBankAssuranceSummary.segAccountDetails.setData(segmentData);
}

function callBAPolicyDetailsServiceMB(){
	var policynumber = frmMBBankAssuranceSummary.segAccountDetails.selectedItems[0]["lblPolicyNumber"];
	var imgLogo  = frmMBBankAssuranceSummary.segAccountDetails.selectedItems[0]["imgLogo"];
	var inputParam = {};
	showLoadingScreen();
	TMBUtil.DestroyForm(frmMBBankAssuranceDetails);
	inputParam["policyNumber"] =policynumber;
	frmMBBankAssuranceDetails.imgAccountDetailsPic.src = imgLogo;
	inputParam["dataSet"] = "0";
    invokeServiceSecureAsync("BAGetPolicyDetails", inputParam, callBAPolicyDetailsServiceCallBackMB);
}

function callBAPolicyDetailsServiceCallBackMB(status,resulttable){
	if(status == 400){
		dismissLoadingScreen();
		if(resulttable["opstatus"] == 0){
			gblBAPolicyDetailsData = resulttable;
			frmMBBankAssuranceDetails.show();
		}
	}
}

function frmMBBankAssuranceDetailsPreShow(){
	changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	var languageExtension = "EN";
	if(locale == "en_US"){
		languageExtension = "EN";
	}else{
		languageExtension = "TH";
	}
	
	frmMBBankAssuranceDetails.lblHead.text = kony.i18n.getLocalizedString("BA_lbl_Policy_details");
	frmMBBankAssuranceDetails.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
	frmMBBankAssuranceDetails.btnTaxDoc.text = kony.i18n.getLocalizedString("BA_Tax_doc");
	frmMBBankAssuranceDetails.btnBack1.text = kony.i18n.getLocalizedString("Back");
	 
	var policyDetailsValueDS = gblBAPolicyDetailsData["policyDetailsValueDS"];
	var premiumPaymentValueDS = gblBAPolicyDetailsData["premiumPaymentValueDS"];
	var coverageValueDS = gblBAPolicyDetailsData["coverageValueDS"];
	var cashbackValueDS = gblBAPolicyDetailsData["cashbackValueDS"];
	var billPaymentValueDS = gblBAPolicyDetailsData["billPaymentValueDS"];
	
	removeWidgetsFromContainerMB();
	
	//Logic to show the Tax Doc and Bill Pay button
	frmMBBankAssuranceDetails.hbxOnHandClick.setVisibility(false);
	var allowPaybill=(policyDetailsValueDS.length > 0 && policyDetailsValueDS[0]["value_EN"] == "1" && billPaymentValueDS.length>0);
	var allowTaxDoc=(policyDetailsValueDS[1]["value_EN"] == "1");
	
	if(allowPaybill){
		frmMBBankAssuranceDetails.btnPayBill.setVisibility(true);
		frmMBBankAssuranceDetails.hbxOnHandClick.setVisibility(true);
		
		//#ifdef android
			frmMBBankAssuranceDetails.btnPayBill.containerWeight=40;
			frmMBBankAssuranceDetails.hbxOnHandClick.padding=[33,0,28,0];
		//#else 
			//#ifdef iphone
				frmMBBankAssuranceDetails.btnPayBill.containerWeight=60;
				frmMBBankAssuranceDetails.hbxOnHandClick.padding=[30,0,0,0];
			//#endif
		//#endif
		
		
	}else{
		frmMBBankAssuranceDetails.btnPayBill.setVisibility(false);
		frmMBBankAssuranceDetails.btnPayBill.containerWeight=0;
	}
	
	if(allowTaxDoc){
		frmMBBankAssuranceDetails.btnTaxDoc.setVisibility(true);
		frmMBBankAssuranceDetails.hbxOnHandClick.setVisibility(true);
		
		//#ifdef android
			frmMBBankAssuranceDetails.btnTaxDoc.containerWeight=40;
			frmMBBankAssuranceDetails.hbxOnHandClick.padding=[30,0,31,0];
		//#else 
			//#ifdef iphone
				frmMBBankAssuranceDetails.btnTaxDoc.containerWeight=60;
				frmMBBankAssuranceDetails.hbxOnHandClick.padding=[30,0,0,0];
			//#endif
		//#endif
		
	}else{
		frmMBBankAssuranceDetails.btnTaxDoc.setVisibility(false);
		frmMBBankAssuranceDetails.btnTaxDoc.containerWeight=0;
	}
	if(allowPaybill && allowTaxDoc){
	
		//#ifdef android
			frmMBBankAssuranceDetails.btnPayBill.containerWeight=40;
			frmMBBankAssuranceDetails.btnTaxDoc.containerWeight=40;
			frmMBBankAssuranceDetails.hbxOnHandClick.padding=[11,0,9,0];
		//#else 
			//#ifdef iphone
				frmMBBankAssuranceDetails.btnPayBill.containerWeight=50;
				frmMBBankAssuranceDetails.btnTaxDoc.containerWeight=50;
				frmMBBankAssuranceDetails.hbxOnHandClick.padding=[10,0,10,0];
			//#endif
		//#endif
		
	}
	
	if(policyDetailsValueDS.length > 0)
		addHeaderToBASectionMB("policyDetails",kony.i18n.getLocalizedString("BA_lbl_Policy_details"));
			
	frmMBBankAssuranceDetails.lblPolicyName.text = policyDetailsValueDS[3]["value_"+languageExtension];
	frmMBBankAssuranceDetails.lblPolicyNo.text = policyDetailsValueDS[2]["displayName_"+languageExtension]+":";
	frmMBBankAssuranceDetails.lblPolicyNoValue.text = policyDetailsValueDS[2]["value_"+languageExtension];
	frmMBBankAssuranceDetails.lblCompany.text = policyDetailsValueDS[7]["displayName_"+languageExtension]+":";
	frmMBBankAssuranceDetails.lblCompanyValue.text = policyDetailsValueDS[7]["value_"+languageExtension];
	frmMBBankAssuranceDetails.lblStatus.text = policyDetailsValueDS[6]["displayName_"+languageExtension]+":";
	frmMBBankAssuranceDetails.lblStatusValue.text = policyDetailsValueDS[6]["value_"+languageExtension];
	var counter = 0; 
	for(var i=9;i<policyDetailsValueDS.length;i++){
		addwidgetsforBADetailsMB("policyDetails",policyDetailsValueDS[i]["displayName_"+languageExtension], policyDetailsValueDS[i]["value_"+languageExtension], counter);
		if(isNotBlank(policyDetailsValueDS[i]["displayName_"+languageExtension]) && isNotBlank(policyDetailsValueDS[i]["value_"+languageExtension])){
			counter++;
		}
	}
	
	counter = 0;
	if(coverageValueDS.length > 0)
		addHeaderToBASectionMB("coverageDetails", kony.i18n.getLocalizedString("BA_lbl_Coverage_info"));
	for(var i=0;i<coverageValueDS.length;i++){
		addwidgetsforBADetailsMB("coverageDetails",coverageValueDS[i]["displayName_"+languageExtension], coverageValueDS[i]["value_"+languageExtension], counter);
		if(isNotBlank(coverageValueDS[i]["displayName_"+languageExtension]) && isNotBlank(coverageValueDS[i]["value_"+languageExtension])){
			counter++;
		}
	}
	
	counter = 0;
	if(cashbackValueDS.length > 0)
	    addHeaderToBASectionMB("cashbackDetails", kony.i18n.getLocalizedString("BA_lbl_Cash_back_dividend"));
	for(var i=0;i<cashbackValueDS.length;i++){
		addwidgetsforBADetailsMB("cashbackDetails",cashbackValueDS[i]["displayName_"+languageExtension], cashbackValueDS[i]["value_"+languageExtension], counter);
		if(isNotBlank(cashbackValueDS[i]["displayName_"+languageExtension]) && isNotBlank(cashbackValueDS[i]["value_"+languageExtension])){
			counter++;
		}
	}
	
	counter = 0;
	if(premiumPaymentValueDS.length > 0)
		addHeaderToBASectionMB("premiumPayment", kony.i18n.getLocalizedString("BA_lbl_Premium_payment_info"));
	for(var i=0;i<premiumPaymentValueDS.length;i++){
		addwidgetsforBADetailsMB("premiumPayment",premiumPaymentValueDS[i]["displayName_"+languageExtension], premiumPaymentValueDS[i]["value_"+languageExtension], counter);
		if(isNotBlank(premiumPaymentValueDS[i]["displayName_"+languageExtension]) && isNotBlank(premiumPaymentValueDS[i]["value_"+languageExtension])){
			counter++;
		}
	}
}

function removeWidgetsFromContainerMB(){
	frmMBBankAssuranceDetails.vbxDynamicDetails.removeFromParent();
	
	 var vbxDynamicDetails = new kony.ui.Box({
        "id": "vbxDynamicDetails",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "vExpand": false,
        "hExpand": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbxDynamicDetails.add();
	        	
    frmMBBankAssuranceDetails.hbxDynamicDetails.add(
    vbxDynamicDetails);
}

function addwidgetsforBADetailsMB(dataSetName,displayName,value,number){
		if(!isNotBlank(value) || !isNotBlank(displayName)){
			return;
		}
		var hboxSkin = "";
		if(number%2 == 0){
			hboxSkin =	"hboxLightGrey";
		}else{
			hboxSkin =	"hboxWhite";
		}
	
		 var lblUseful = new kony.ui.Label({
        "id": dataSetName+"lblUseful"+number,
        "isVisible": true,
        "text": displayName+":",
        "skin": "lblGray"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 50
    }, {
        "toolTip": null
    });
    var lblUsefulValue = new kony.ui.Label({
        "id": dataSetName+"lblUsefulValue"+number,
        "isVisible": true,
        "text": value,
        "skin": "lblGray"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 50
    }, {
        "toolTip": null
    });
    var hbxUseful = new kony.ui.Box({
        "id": dataSetName+"hbxUseful"+number,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": hboxSkin,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 5,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [3, 1, 3, 1],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxUseful.add(
    lblUseful, lblUsefulValue);
    frmMBBankAssuranceDetails.vbxDynamicDetails.add(hbxUseful);
}

function addHeaderToBASectionMB(headerName,headerText){
	var lblHeader1 = new kony.ui.Label({
	        "id": "lblHeader"+headerName,
	        "isVisible": true,
	        "text": headerText,
	        "skin": "lblBlackMediumNormal"
	    }, {
	        "widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_LEFT,
	        "vExpand": false,
	        "hExpand": true,
	        "margin": [2, 2, 0, 0],
	        "padding": [0, 0, 0, 0],
	        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
	        "marginInPixel": false,
	        "paddingInPixel": false,
	        "containerWeight": 98
	    }, {
	        "toolTip": null
	    });
	   frmMBBankAssuranceDetails.vbxDynamicDetails.add(lblHeader1);
}

function showBATaxDocEmailConfirmPopup(){
	if(emailValidatn(gblEmailId)){
		popAllowOrNot.imgIcon.setVisibility(false);
		popAllowOrNot.lblTitle.setVisibility(false);
		var message=kony.i18n.getLocalizedString("keyBATaxDocReqConfirmation");
		popAllowOrNot.lblMsgText.text=message.replace("[emailId]", gblEmailId);
		popAllowOrNot.btnAllow.onClick=callBARequestTaxDocServiceMB;
		popAllowOrNot.show();
	}else{
		showAlert(kony.i18n.getLocalizedString("keyEmailInCorrect"), kony.i18n.getLocalizedString("info"));
	}
}

function callBARequestTaxDocServiceMB(){
	popAllowOrNot.imgIcon.setVisibility(true);
	popAllowOrNot.lblTitle.setVisibility(true);
	popAllowOrNot.dismiss();
	var locale = kony.i18n.getCurrentLocale();
	var languageExtension = "EN";
	if(locale == "en_US"){
		languageExtension = "EN";
	}else{
		languageExtension = "TH";
	}

	var policynumber = gblBAPolicyDetailsData["policyDetailsValueDS"][2]["value_"+languageExtension];
	var inputParam = {};
	showLoadingScreenPopup();
	inputParam["policyNumber"] = policynumber;
	inputParam["policyName"] = gblBAPolicyDetailsData["policyDetailsValueDS"][3]["value_EN"];
	
    invokeServiceSecureAsync("BAReqTaxDocument", inputParam, callBARequestTaxDocServiceCallBackMB);
}

function callBARequestTaxDocServiceCallBackMB(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == "0000") {
        	dismissLoadingScreenPopup();
        	showAlert(kony.i18n.getLocalizedString("keyBATaxDocReqSuccess"), kony.i18n.getLocalizedString("info"));
        	
        }else{
        	dismissLoadingScreenPopup();
        	if(resulttable["StatusCode"] == "BA3031"){
        		showAlert(kony.i18n.getLocalizedString("keyBATaxDocReqAlready"), kony.i18n.getLocalizedString("info"));
        	}else{
        		showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        	}
        }
     }
 }
 
function loadPromotionCxb(){
		var temp=[];
		temp.push(["0",kony.i18n.getLocalizedString("Promotions_AllPromotions")]);
		temp.push(["1",kony.i18n.getLocalizedString("Promotions_Deposit")]);
		temp.push(["2",kony.i18n.getLocalizedString("Promotions_Loan")]);
		temp.push(["3",kony.i18n.getLocalizedString("Promotions_CreditCard")]);
		temp.push(["4",kony.i18n.getLocalizedString("Promotions_Others")]);
		frmPromotion.cbxPromotion.masterData = temp;
		frmPromotion.cbxPromotion.selectedKeyValue = promotionSearchKey;
}

function onClickPromotions(){
		 gblIsNewOffersExists = false;		 
		 if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {
			onClickPromotionsIphone();
	 	 }	else {
			onClickPromotionsAndroid();
	 	 }	 	 
}

function onClickPromotionsAndroid()
{
	try{
		
		//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
		frmPromotion.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotions"); 
		var inputParams = {};
		showLoadingScreen();
		promotionSearchKey = "All Promotions";
		promotionFilter = false;
		/*
		selectedValue = 0;
		var temp=[];
		temp.push(["0",kony.i18n.getLocalizedString("Promotions_AllPromotions")]);
		temp.push(["1",kony.i18n.getLocalizedString("Promotions_Deposit")]);
		temp.push(["2",kony.i18n.getLocalizedString("Promotions_Loan")]);
		temp.push(["3",kony.i18n.getLocalizedString("Promotions_CreditCard")]);
		temp.push(["4",kony.i18n.getLocalizedString("Promotions_Others")]);
		frmPromotion.cbxPromotion.masterData = temp;
		frmPromotion.cbxPromotion.selectedKey = selectedValue;
		*/
		frmPromotion.cbxPromotionbtn.text=kony.i18n.getLocalizedString("Promotions_AllPromotions");
		gblPromotionsViewCount = gblPromotionsViewCount + 1;
		
		inputParams["crmId"] = gblcrmId;
		inputParams["appChannel"] = "M";
		invokeServiceSecureAsync("GetHotPromotion", inputParams, successCallBackPromotions)
	}catch(e){
		//alert(e);
	}	
}

function onClickPromotionsIphone()
{
	try{
		
		//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
		frmPromotion.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotions"); 
		var inputParams = {};
		showLoadingScreen();
		promotionSearchKey = "All Promotions";
		promotionFilter = false;
		selectedValue = 0;
		var temp=[];
		temp.push(["0",kony.i18n.getLocalizedString("Promotions_AllPromotions")]);
		temp.push(["1",kony.i18n.getLocalizedString("Promotions_Deposit")]);
		temp.push(["2",kony.i18n.getLocalizedString("Promotions_Loan")]);
		temp.push(["3",kony.i18n.getLocalizedString("Promotions_CreditCard")]);
		temp.push(["4",kony.i18n.getLocalizedString("Promotions_Others")]);
		frmPromotion.cbxPromotion.masterData = temp;
		frmPromotion.cbxPromotion.selectedKey = selectedValue;
		
		gblPromotionsViewCount = gblPromotionsViewCount + 1;
		
		inputParams["crmId"] = gblcrmId;
		inputParams["appChannel"] = "M";
		invokeServiceSecureAsync("GetHotPromotion", inputParams, successCallBackPromotions)
	}catch(e){
		//alert(e);
	}
}

function isValidPromotionCodeMB(){
		var cmpType = gblCampaignResp.split("~")[1];
		var cmpCode = gblCampaignResp.split("~")[2];
		for(var i = 0; i < gblPromotions["hotPromotions"].length; i++){
			if(cmpCode == gblPromotions["hotPromotions"][i]["cmpCode"] && cmpType == gblPromotions["hotPromotions"][i]["cmpType"]){
				return true;
			}
		}	
		alert(kony.i18n.getLocalizedString("keyCampaignInboxNotFound"));
		dismissLoadingScreen();
		return false;
}

function successCallBackPromotions(status, resulttable){
	gblCampaignDataENIL = "";
	gblCampaignDataTHIL = "";
	gblCampaignDataENEL = "";
	gblCampaignDataTHEL = "";
	var promotionsReslutSet = [];
	//var screenwidth = gblDeviceInfo["deviceWidth"];
	//var refHeight =60;
	//var refWidth =80;
	try{
		
		if(!promotionFilter){
			gblPromotions = resulttable;
			if(gblMyOffersDetails){
    			if(!isValidPromotionCodeMB()){
    				return;
    			}
            }
		}
		gblCurrentPromotions = resulttable;
		
		var refHeight =60;
		var refWidth =80;
        var refWidthAndHeight = getMyOffersThumbNailRefWidthAndHeight();
		refWidth =  parseInt(refWidthAndHeight.split("-")[0]);
		refHeight = parseInt(refWidthAndHeight.split("-")[1]);
		
	if(status == 400){
		    
			if (resulttable["opstatus"] == 0) {
				for (var i = 0; i < resulttable["hotPromotions"].length; i++) {
					var tempPromotionData = [];
					var subject = "";
					var shorMsg = "";
					var imageIcon = "";
					var detailsImg1 = "";
					var detailsImg2 = "";
					var detailsImg3 = "";
					var prmText1 = "";
					var prmText2 = "";
					var prmText3 = "";
					var prmText4 = "";
					var prmText5 = "";
					var description = "";
					var searchKey = "";
					var cmpCode = "";
					var cmpType="";
					var cmpEndDate = "";
					var cmpIntExt = "";
					 
					if(promotionFilter == true){
						searchKey = resulttable["hotPromotions"][i]["productOffer"];
					}else{
						searchKey = "";
					}
					if(promotionSearchKey == "All Promotions"){
						cmpCode = resulttable["hotPromotions"][i]["cmpCode"];
						cmpType =resulttable["hotPromotions"][i]["cmpType"];
						cmpIntExt = resulttable["hotPromotions"][i]["hotpromoLinkFlagMob"];
						cmpEndDate = resulttable["hotPromotions"][i]["cmpEndDate"];	
						if(kony.i18n.getCurrentLocale() == "en_US"){
							subject = resulttable["hotPromotions"][i]["subjectEN"];
							shorMsg = resulttable["hotPromotions"][i]["shortMsgEN"];
							imageIcon = resulttable["hotPromotions"][i]["imgHotpromoIcon1MobEN"];
							detailsImg1 = resulttable["hotPromotions"][i]["imgHotpromoId1MobEN"];
							detailsImg2 = resulttable["hotPromotions"][i]["imgHotpromoId2MobEN"];
							detailsImg3 = resulttable["hotPromotions"][i]["imgHotpromoId3MobEN"];
							description = resulttable["hotPromotions"][i]["longMsgLine1EN"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine2EN"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine3EN"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine4EN"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine5EN"];
							prmText1 = resulttable["hotPromotions"][i]["longMsgLine1EN"];
							prmText2 = resulttable["hotPromotions"][i]["longMsgLine2EN"];
							prmText3 = resulttable["hotPromotions"][i]["longMsgLine3EN"];
							prmText4 = resulttable["hotPromotions"][i]["longMsgLine4EN"];
							prmText5 = resulttable["hotPromotions"][i]["longMsgLine5EN"];
							gblCampaignDataENIL = resulttable["hotPromotions"][i]["hotpromoInternalLinkMobEN"];
							gblCampaignDataTHIL = resulttable["hotPromotions"][i]["hotpromoInternalLinkMobTH"];
							gblCampaignDataENEL = resulttable["hotPromotions"][i]["hotpromoExternalLinkMobEN"];
							gblCampaignDataTHEL = resulttable["hotPromotions"][i]["hotpromoExternalLinkMobTH"];
						}else if(kony.i18n.getCurrentLocale() == "th_TH"){
							subject = resulttable["hotPromotions"][i]["subjectTH"];
							shorMsg = resulttable["hotPromotions"][i]["shortMsgTH"];
							imageIcon = resulttable["hotPromotions"][i]["imgHotpromoIcon1MobTH"];
							detailsImg1 = resulttable["hotPromotions"][i]["imgHotpromoId1MobTH"];
							detailsImg2 = resulttable["hotPromotions"][i]["imgHotpromoId2MobTH"];
							detailsImg3 = resulttable["hotPromotions"][i]["imgHotpromoId3MobTH"];
							description = resulttable["hotPromotions"][i]["longMsgLine1TH"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine2TH"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine3TH"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine4TH"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine5TH"];
							prmText1 = resulttable["hotPromotions"][i]["longMsgLine1TH"];
							prmText2 = resulttable["hotPromotions"][i]["longMsgLine2TH"];
							prmText3 = resulttable["hotPromotions"][i]["longMsgLine3TH"];
							prmText4 = resulttable["hotPromotions"][i]["longMsgLine4TH"];
							prmText5 = resulttable["hotPromotions"][i]["longMsgLine5TH"];
							gblCampaignDataENIL = resulttable["hotPromotions"][i]["hotpromoInternalLinkMobEN"];
							gblCampaignDataTHIL = resulttable["hotPromotions"][i]["hotpromoInternalLinkMobTH"];
							gblCampaignDataENEL = resulttable["hotPromotions"][i]["hotpromoExternalLinkMobEN"];
							gblCampaignDataTHEL = resulttable["hotPromotions"][i]["hotpromoExternalLinkMobTH"];
						}
						tempPromotionData ={ imgprpic: {src: imageIcon.trim(),referenceWidth : parseInt(refWidth), referenceHeight : parseInt(refHeight)},
										 lblTitle: subject,
										 lblPrmDetail: shorMsg,
										 imgArrow: "bg_arrow_right.png",
										 desc: description,
										 promoText1 : prmText1,
										 promoText2 : prmText2,
										 promoText3 : prmText3,
										 promoText4 : prmText4,
										 promoText5 : prmText5,
										 detailsImage1: detailsImg1.trim(),
										 detailsImage2: detailsImg2.trim(),
										 detailsImage3: detailsImg3.trim(),
										 campCode:cmpCode,
										 camType:cmpType,
										 cmpIntExt: cmpIntExt,
										 gblCampaignDataENIL: gblCampaignDataENIL,
										 gblCampaignDataTHIL: gblCampaignDataTHIL,
										 gblCampaignDataENEL: gblCampaignDataENEL,
										 gblCampaignDataTHEL: gblCampaignDataTHEL,
										 cmpEndDate : cmpEndDate
										  };
					
					promotionsReslutSet.push(tempPromotionData);
					}else if(searchKey == promotionSearchKey){
						cmpCode = resulttable["hotPromotions"][i]["cmpCode"];
						cmpType =resulttable["hotPromotions"][i]["cmpType"];
						cmpIntExt = resulttable["hotPromotions"][i]["hotpromoLinkFlagMob"];
						cmpEndDate = resulttable["hotPromotions"][i]["cmpEndDate"];
						if(kony.i18n.getCurrentLocale() == "en_US"){
							subject = resulttable["hotPromotions"][i]["subjectEN"];
							shorMsg = resulttable["hotPromotions"][i]["shortMsgEN"];
							imageIcon = resulttable["hotPromotions"][i]["imgHotpromoIcon1MobEN"];
							detailsImg1 = resulttable["hotPromotions"][i]["imgHotpromoId1MobEN"];
							detailsImg2 = resulttable["hotPromotions"][i]["imgHotpromoId2MobEN"];
							detailsImg3 = resulttable["hotPromotions"][i]["imgHotpromoId3MobEN"];
							description = resulttable["hotPromotions"][i]["longMsgLine1EN"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine2EN"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine3EN"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine4EN"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine5EN"];
							prmText1 = resulttable["hotPromotions"][i]["longMsgLine1EN"];
							prmText2 = resulttable["hotPromotions"][i]["longMsgLine2EN"];
							prmText3 = resulttable["hotPromotions"][i]["longMsgLine3EN"];
							prmText4 = resulttable["hotPromotions"][i]["longMsgLine4EN"];
							prmText5 = resulttable["hotPromotions"][i]["longMsgLine5EN"];
							gblCampaignDataENIL = resulttable["hotPromotions"][i]["hotpromoInternalLinkMobEN"];
							gblCampaignDataTHIL = resulttable["hotPromotions"][i]["hotpromoInternalLinkMobTH"];
							gblCampaignDataENEL = resulttable["hotPromotions"][i]["hotpromoExternalLinkMobEN"];
							gblCampaignDataTHEL = resulttable["hotPromotions"][i]["hotpromoExternalLinkMobTH"];
						}else if(kony.i18n.getCurrentLocale() == "th_TH"){
							subject = resulttable["hotPromotions"][i]["subjectTH"];
							shorMsg = resulttable["hotPromotions"][i]["shortMsgTH"];
							imageIcon = resulttable["hotPromotions"][i]["imgHotpromoIcon1MobTH"];
							detailsImg1 = resulttable["hotPromotions"][i]["imgHotpromoId1MobTH"];
							detailsImg2 = resulttable["hotPromotions"][i]["imgHotpromoId2MobTH"];
							detailsImg3 = resulttable["hotPromotions"][i]["imgHotpromoId3MobTH"];
							description = resulttable["hotPromotions"][i]["longMsgLine1TH"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine2TH"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine3TH"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine4TH"]+"<br/>"+resulttable["hotPromotions"][i]["longMsgLine5TH"];
							prmText1 = resulttable["hotPromotions"][i]["longMsgLine1TH"];
							prmText2 = resulttable["hotPromotions"][i]["longMsgLine2TH"];
							prmText3 = resulttable["hotPromotions"][i]["longMsgLine3TH"];
							prmText4 = resulttable["hotPromotions"][i]["longMsgLine4TH"];
							prmText5 = resulttable["hotPromotions"][i]["longMsgLine5TH"];
							gblCampaignDataENIL = resulttable["hotPromotions"][i]["hotpromoInternalLinkMobEN"];
							gblCampaignDataTHIL = resulttable["hotPromotions"][i]["hotpromoInternalLinkMobTH"];
							gblCampaignDataENEL = resulttable["hotPromotions"][i]["hotpromoExternalLinkMobEN"];
							gblCampaignDataTHEL = resulttable["hotPromotions"][i]["hotpromoExternalLinkMobTH"];
						}
					
						tempPromotionData ={ imgprpic: {src: imageIcon.trim(),referenceWidth : parseInt(refWidth), referenceHeight : parseInt(refHeight)},
										 lblTitle: subject,
										 lblPrmDetail: shorMsg,
										 imgArrow: "bg_arrow_right.png",
										 desc: description,
										 promoText1 : prmText1,
										 promoText2 : prmText2,
										 promoText3 : prmText3,
										 promoText4 : prmText4,
										 promoText5 : prmText5,
										 detailsImage1: detailsImg1.trim(),
										 detailsImage2: detailsImg2.trim(),
										 detailsImage3: detailsImg3.trim(),
										 campCode:cmpCode,
										 camType:cmpType,
										 cmpIntExt: cmpIntExt,
										 gblCampaignDataENIL: gblCampaignDataENIL,
										 gblCampaignDataTHIL: gblCampaignDataTHIL,
										 gblCampaignDataENEL: gblCampaignDataENEL,
										 gblCampaignDataTHEL: gblCampaignDataTHEL,
										 cmpEndDate: cmpEndDate 
										 };
					
						promotionsReslutSet.push(tempPromotionData);
					}

				}//End of for loop.
				
				frmPromotion.segPromotions.setData(promotionsReslutSet);
				showLineAtEnd();
				dismissLoadingScreen();
				 if (promotionsReslutSet.length == 0) {
                    //fix for 1913
					frmPromotion.lineAtEnd.setVisibility(false);
					if(gblDeviceInfo["name"]!="android")
                    {
	                    if (promotionFilter == false) {
	                        frmPromotion.hbox477746511278660.setVisibility(false);
	                    } else {
	                        frmPromotion.hbox477746511278660.setVisibility(true);
	                    }
	                }
	                else
	                {
	                	if (promotionFilter == false) {
	                        frmPromotion.hbox4483668429763.setVisibility(false);
	                    } else {
	                        frmPromotion.hbox4483668429763.setVisibility(true);
	                    }
	                }
                    if(!flowSpa){
						frmPromotion.segPromotions.setVisibility(false);
					}
					frmPromotion.lblNoPromotions.setVisibility(true);
					frmPromotion.lblNoPromotions.text = kony.i18n.getLocalizedString("Promotions_noData");
                    //added below if else condition MIB-966
                    if(gblMyOffersDetails){
                    	directPromotionDetailsViewMB();
                    }else{
                    	frmPromotion.show();
                    }
                    
                } else {
                	 if(!flowSpa){
						frmPromotion.segPromotions.setVisibility(true);
					 }
					frmPromotion.lblNoPromotions.setVisibility(false);
                    //added below if else condition MIB-966
                    if(gblMyOffersDetails){
                    	directPromotionDetailsViewMB();
                    }else{
                    	frmPromotion.show();
                    }
                }
			}else{
				
				dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("MenuHotPromotions"));
			}
		}
	}catch(e){
		
	}
}

function showLineAtEnd(){
	try{
		if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {
			frmPromotion.lineAtEnd.setVisibility(false);
	 	 }	else {
			if(frmPromotion.segPromotions.isVisible){
				frmPromotion.lineAtEnd.setVisibility(true);
			}
	 	 }
				
	}catch(e){
		alert("Error while showing line at End of segment : "+e);
	}
}

function onClickPromotionSeg(){
	try{
		gblMyOffersDetails = false;
		//gblPromotionsClickCount = gblPromotionsClickCount + 1;
		var cmpType=frmPromotion.segPromotions.selectedItems[0].camType;
		if (gblDeviceInfo["name"] != "android")
		{
			frmPromotionDetails.button1399939188243213.setEnabled(false);
		}
		if(cmpType !='M'){
			gblCampaignResp = "frmPromotion" + "~" + frmPromotion.segPromotions.selectedItems[0].camType + "~" + frmPromotion.segPromotions.selectedItems[0].campCode + "~" + frmPromotion.segPromotions.selectedItems[0].cmpEndDate + "~" + "02" + "~" + "O";
			campaignBannerCount("D","O");
		}				
		promotionSelectIndex = "";
		promotionTitle = "";
		promotionSelectIndex = frmPromotion.segPromotions.selectedIndex[1];
		promotionTitle = frmPromotion.segPromotions.selectedItems[0].lblTitle;
		
		frmPromotionDetails.lblPromTitle.text = frmPromotion.segPromotions.selectedItems[0].lblTitle;
		frmPromotionDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotionsDetails"); 
		frmPromotionDetails.bttnBack.text = kony.i18n.getLocalizedString("Back");
		frmPromotionDetails.button1399939188243213.text = kony.i18n.getLocalizedString("Back");
		var cmpIntExt = frmPromotion.segPromotions.selectedItems[0].cmpIntExt;
		if(cmpIntExt == "04") {
			frmPromotionDetails.btnMBpromo.text = kony.i18n.getLocalizedString("FindTMB_Call");
		}else{
			frmPromotionDetails.btnMBpromo.text = kony.i18n.getLocalizedString("keyClick");
		}
		//if(flowSpa)
        //frmPromotionDetails.lblRichText.text =  frmPromotion.segPromotions.selectedItems[0].desc;
       // else
		if (gblDeviceInfo["name"] == "android")
		{
			frmPromotionDetails.rchPromDetails.text = frmPromotion.segPromotions.selectedItems[0].desc;
		}
        else
        {
        	frmPromotionDetails.rchPromDetails.text = ".";
        }
        
		promotionDetailsText1 = ""; promotionDetailsText2 = ""; promotionDetailsText3 = ""; promotionDetailsText4 = ""; promotionDetailsText5 = "";
		
		promotionDetailsText1 = frmPromotion.segPromotions.selectedItems[0].promoText1;
		promotionDetailsText2 = frmPromotion.segPromotions.selectedItems[0].promoText2;
		promotionDetailsText3 = frmPromotion.segPromotions.selectedItems[0].promoText3;
		promotionDetailsText4 = frmPromotion.segPromotions.selectedItems[0].promoText4;
		promotionDetailsText5 = frmPromotion.segPromotions.selectedItems[0].promoText5;
		var imageData = [];
		imageURL1 = ""; imageURL2 = ""; imageURL3 = "";
		imageURL1 = frmPromotion.segPromotions.selectedItems[0].detailsImage1;
		imageURL2 = frmPromotion.segPromotions.selectedItems[0].detailsImage2;
		imageURL3 = frmPromotion.segPromotions.selectedItems[0].detailsImage3;
		//var screenwidth = gblDeviceInfo["deviceWidth"];
		var refHeight = 225;
        var refWidth = 300;
        var refWidthAndHeight = getInboxAndMyOffersRefWidthAndHeight();
		refWidth =  parseInt(refWidthAndHeight.split("-")[0]);
		refHeight = parseInt(refWidthAndHeight.split("-")[1]);
		
		//imageData = [{imgProm: {src: imageURL1}},{imgProm: {src: null}},{imgProm: {src: imageURL3}}];
		if(imageURL1 != ""){
			imageData.push({imgProm: {src: imageURL1,referenceWidth : parseInt(refWidth), referenceHeight : parseInt(refHeight)}})
		}
		if(imageURL2 != ""){
		   	imageData.push({imgProm: {src: imageURL2,referenceWidth : parseInt(refWidth), referenceHeight : parseInt(refHeight)}})
		}
		if(imageURL3 != ""){
			imageData.push({imgProm: {src: imageURL3,referenceWidth : parseInt(refWidth), referenceHeight : parseInt(refHeight)}})
		}
		if(imageURL1 !='' || imageURL2!='' || imageURL3 !='') {
				frmPromotionDetails.segPromImage.setVisibility(true);
				frmPromotionDetails.segPromImage.setData(imageData);
				frmPromotionDetails.segPromImage.pageSkin =  "segTransparent";
				frmPromotionDetails.segPromImage.selectedIndex = [0,0];
		} else {
				frmPromotionDetails.segPromImage.setVisibility(false);
		}
		gblCmpIntExt = frmPromotion.segPromotions.selectedItems[0].cmpIntExt;
		if(gblCmpIntExt != "00")
			  {
			    frmPromotionDetails.hboxnormal.setVisibility(false);
			    frmPromotionDetails.hboxpromo.setVisibility(true);
			  } else
			  {
			    frmPromotionDetails.hboxpromo.setVisibility(false);
			    frmPromotionDetails.hboxnormal.setVisibility(true);
			  }
		
		if(gblCmpIntExt == "01" ){
				gblCampaignDataEN = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataENIL;
				gblCampaignDataTH = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataTHIL;
		} else if( gblCmpIntExt == "02"){
				gblCampaignDataEN = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataENEL;
				gblCampaignDataTH = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataTHEL;
		} else if(gblCmpIntExt == "03"){
				gblCampaignDataENIL = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataENIL;
				gblCampaignDataTHIL = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataTHIL;
				gblCampaignDataENEL = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataENEL;
				gblCampaignDataTHEL = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataTHEL;				
		} else if(gblCmpIntExt == "04"){
				gblCampaignDataENEL = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataENEL;
				gblCampaignDataTHEL = frmPromotion.segPromotions.selectedItems[0].gblCampaignDataTHEL;				
		}
		frmPromotionDetails.show();
		if (gblDeviceInfo["name"] != "android")
		{
			try
			{
				kony.timer.cancel("displayPromoDesc")
			} catch (e)
			{
			}
			kony.timer.schedule("displayPromoDesc", displaydescPromo, 1, false);
		}
	}catch(e){
		
	}
}

function displaydescPromo()
{
	frmPromotionDetails.rchPromDetails.text = frmPromotion.segPromotions.selectedItems[0].desc;
	frmPromotionDetails.button1399939188243213.setEnabled(true);
}

function directPromotionDetailsViewMB(){
	try{
		
		gblOfferDetailsObj = {};
		var cmpType = gblCampaignResp.split("~")[1];
		var cmpCode = gblCampaignResp.split("~")[2];
		//alert("cmpType : "+cmpType+" , cmpCode : "+cmpCode);
		var cmpEndDate  = gblCampaignResp.split("~")[3];

		promotionSelectIndex = "";
		promotionTitle = "";
		
		var count = 0;
		if(undefined != gblCurrentPromotions && null != gblCurrentPromotions){
			for(var i = 0; i < gblCurrentPromotions["hotPromotions"].length; i++){
				if(cmpCode == gblCurrentPromotions["hotPromotions"][i]["cmpCode"] && cmpType == gblCurrentPromotions["hotPromotions"][i]["cmpType"]){
					count = 1;
					gblOfferDetailsObj.cmpCode = gblCurrentPromotions["hotPromotions"][i]["cmpCode"];
					gblOfferDetailsObj.cmpType = gblCurrentPromotions["hotPromotions"][i]["cmpType"];
					gblOfferDetailsObj.cmpEndDate = gblCurrentPromotions["hotPromotions"][i]["cmpEndDate"];
					gblOfferDetailsObj.cmpIntExt = gblCurrentPromotions["hotPromotions"][i]["hotpromoLinkFlagMob"];
					if (kony.i18n.getCurrentLocale() == "en_US") {
						gblOfferDetailsObj.lblTitle = gblCurrentPromotions["hotPromotions"][i]["subjectEN"];
						gblOfferDetailsObj.shorMsg = gblCurrentPromotions["hotPromotions"][i]["shortMsgEN"];
						gblOfferDetailsObj.imageIcon = gblCurrentPromotions["hotPromotions"][i]["imgHotpromoIcon1MobEN"];
						gblOfferDetailsObj.desc = gblCurrentPromotions["hotPromotions"][i]["longMsgLine1EN"] + "<br/>" + gblCurrentPromotions["hotPromotions"][i]["longMsgLine2EN"] 
											+ "<br/>" + gblCurrentPromotions["hotPromotions"][i]["longMsgLine3EN"] + "<br/>" 
											+ gblCurrentPromotions["hotPromotions"][i]["longMsgLine4EN"] 
											+ "<br/>" + gblCurrentPromotions["hotPromotions"][i]["longMsgLine5EN"];
						gblOfferDetailsObj.detailsImage1 = gblCurrentPromotions["hotPromotions"][i]["imgHotpromoId1MobEN"];
						gblOfferDetailsObj.detailsImage2 = gblCurrentPromotions["hotPromotions"][i]["imgHotpromoId2MobEN"];
						gblOfferDetailsObj.detailsImage3 = gblCurrentPromotions["hotPromotions"][i]["imgHotpromoId3MobEN"];
						
						gblOfferDetailsObj.promoText1 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine1EN"];
						gblOfferDetailsObj.promoText2 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine2EN"];
						gblOfferDetailsObj.promoText3 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine3EN"];
						gblOfferDetailsObj.promoText4 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine4EN"];
						gblOfferDetailsObj.promoText4 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine5EN"];
												
					} else if (kony.i18n.getCurrentLocale() == "th_TH") {
						gblOfferDetailsObj.lblTitle = gblCurrentPromotions["hotPromotions"][i]["subjectTH"];
						gblOfferDetailsObj.shorMsg = gblCurrentPromotions["hotPromotions"][i]["shortMsgTH"];
						gblOfferDetailsObj.imageIcon = gblCurrentPromotions["hotPromotions"][i]["imgHotpromoIcon1MobTH"];
						
						gblOfferDetailsObj.desc = gblCurrentPromotions["hotPromotions"][i]["longMsgLine1TH"] + "<br/>" + gblCurrentPromotions["hotPromotions"][i]["longMsgLine2TH"] 
											+ "<br/>" + gblCurrentPromotions["hotPromotions"][i]["longMsgLine3TH"] + "<br/>" 
											+ gblCurrentPromotions["hotPromotions"][i]["longMsgLine4TH"] 
											+ "<br/>" + gblCurrentPromotions["hotPromotions"][i]["longMsgLine5TH"];
						gblOfferDetailsObj.detailsImage1 = gblCurrentPromotions["hotPromotions"][i]["imgHotpromoId1MobTH"];
						gblOfferDetailsObj.detailsImage2 = gblCurrentPromotions["hotPromotions"][i]["imgHotpromoId2MobTH"];
						gblOfferDetailsObj.detailsImage3 = gblCurrentPromotions["hotPromotions"][i]["imgHotpromoId3MobTH"];
						
						gblOfferDetailsObj.promoText1 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine1TH"];
						gblOfferDetailsObj.promoText2 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine2TH"];
						gblOfferDetailsObj.promoText3 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine3TH"];
						gblOfferDetailsObj.promoText4 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine4TH"];
						gblOfferDetailsObj.promoText4 = gblCurrentPromotions["hotPromotions"][i]["longMsgLine5TH"];
												
					}
					gblOfferDetailsObj.gblCampaignDataENIL = gblCurrentPromotions["hotPromotions"][i]["hotpromoInternalLinkMobEN"];
					gblOfferDetailsObj.gblCampaignDataTHIL = gblCurrentPromotions["hotPromotions"][i]["hotpromoInternalLinkMobTH"];
					gblOfferDetailsObj.gblCampaignDataENEL = gblCurrentPromotions["hotPromotions"][i]["hotpromoExternalLinkMobEN"];
					gblOfferDetailsObj.gblCampaignDataTHEL = gblCurrentPromotions["hotPromotions"][i]["hotpromoExternalLinkMobTH"];
				}
				//already one recod is matched with cmpCode and cmpType, no need to iterate entire records.
				if(count == 1){
					//if cmpType is not M then capturing display count for my offers
					if(cmpType !='M'){
						gblCampaignResp = "frmPromotion" + "~" + cmpType + "~" + cmpCode + "~" + cmpEndDate + "~" + "02" + "~" + "O";
						campaignBannerCount("D","O");
					}
					promotionSelectIndex = i;
					break;
				}
			}
			
		}
		promotionTitle = gblOfferDetailsObj.lblTitle;
		frmPromotionDetails.lblPromTitle.text = gblOfferDetailsObj.lblTitle;
		frmPromotionDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotionsDetails"); 
		frmPromotionDetails.bttnBack.text = kony.i18n.getLocalizedString("Back");
		if (gblDeviceInfo["name"] == "android")
		{
			frmPromotionDetails.rchPromDetails.text = gblOfferDetailsObj.desc;
		}
        else
        {
        	frmPromotionDetails.rchPromDetails.text = ".";
        }
		promotionDetailsText1 = ""; promotionDetailsText2 = ""; promotionDetailsText3 = ""; promotionDetailsText4 = ""; promotionDetailsText5 = "";
		
		promotionDetailsText1 = gblOfferDetailsObj.promoText1;
		promotionDetailsText2 = gblOfferDetailsObj.promoText2;
		promotionDetailsText3 = gblOfferDetailsObj.promoText3;
		promotionDetailsText4 = gblOfferDetailsObj.promoText4;
		promotionDetailsText5 = gblOfferDetailsObj.promoText5;
		var imageData = [];
		imageURL1 = ""; imageURL2 = ""; imageURL3 = "";
		imageURL1 = gblOfferDetailsObj.detailsImage1;
		imageURL2 = gblOfferDetailsObj.detailsImage2;
		imageURL3 = gblOfferDetailsObj.detailsImage3;
		//var screenwidth = gblDeviceInfo["deviceWidth"];
		var refHeight = 225;
        var refWidth = 300;
        var refWidthAndHeight = getInboxAndMyOffersRefWidthAndHeight();
		refWidth =  parseInt(refWidthAndHeight.split("-")[0]);
		refHeight = parseInt(refWidthAndHeight.split("-")[1]);
		
		//imageData = [{imgProm: {src: imageURL1}},{imgProm: {src: null}},{imgProm: {src: imageURL3}}];
		if(imageURL1 != ""){
			imageData.push({imgProm: {src: imageURL1,referenceWidth : parseInt(refWidth), referenceHeight : parseInt(refHeight)}})
		}
		if(imageURL2 != ""){
		   	imageData.push({imgProm: {src: imageURL2,referenceWidth : parseInt(refWidth), referenceHeight : parseInt(refHeight)}})
		}
		if(imageURL3 != ""){
			imageData.push({imgProm: {src: imageURL3,referenceWidth : parseInt(refWidth), referenceHeight : parseInt(refHeight)}})
		}
		if(imageURL1 !='' || imageURL2!='' || imageURL3 !='') {
				frmPromotionDetails.segPromImage.setVisibility(true);
				frmPromotionDetails.segPromImage.setData(imageData);
				frmPromotionDetails.segPromImage.pageSkin =  "segTransparent";
				frmPromotionDetails.segPromImage.selectedIndex = [0,0];
		} else {
				frmPromotionDetails.segPromImage.setVisibility(false);
		}
		gblCmpIntExt = gblOfferDetailsObj.cmpIntExt;
		if(gblCmpIntExt != "00"){
		    frmPromotionDetails.hboxnormal.setVisibility(false);
		    frmPromotionDetails.hboxpromo.setVisibility(true);
		}else {
		    frmPromotionDetails.hboxpromo.setVisibility(false);
		    frmPromotionDetails.hboxnormal.setVisibility(true);
		}
		
		if(gblCmpIntExt == "01" ){
				gblCampaignDataEN = gblOfferDetailsObj.gblCampaignDataENIL;
				gblCampaignDataTH = gblOfferDetailsObj.gblCampaignDataTHIL;
		} else if( gblCmpIntExt == "02"){
				gblCampaignDataEN = gblOfferDetailsObj.gblCampaignDataENEL;
				gblCampaignDataTH = gblOfferDetailsObj.gblCampaignDataTHEL;
		} else if(gblCmpIntExt == "03"){
				gblCampaignDataENIL = gblOfferDetailsObj.gblCampaignDataENIL;
				gblCampaignDataTHIL = gblOfferDetailsObj.gblCampaignDataTHIL;
				gblCampaignDataENEL = gblOfferDetailsObj.gblCampaignDataENEL;
				gblCampaignDataTHEL = gblOfferDetailsObj.gblCampaignDataTHEL;				
		} else if(gblCmpIntExt == "04"){
				gblCampaignDataENEL = gblOfferDetailsObj.gblCampaignDataENEL;
				gblCampaignDataTHEL = gblOfferDetailsObj.gblCampaignDataTHEL;				
		}
		if(count == 1){
			frmPromotionDetails.show();
			if (gblDeviceInfo["name"] != "android")
			{
				try
				{
					kony.timer.cancel("displayRichtextfont")
				} catch (e)
				{
				}
				kony.timer.schedule("displayRichtextfont", displayRichtextfont, 1, false);
			}
			
		}else {
			frmPromotion.show();
		}
		
	}catch(e){
		
	}
}

function displayRichtextfont()
{
	frmPromotionDetails.rchPromDetails.text = gblOfferDetailsObj.desc;
}

function getMyOffersThumbNailRefWidthAndHeight(){
	var screenwidth = gblDeviceInfo["deviceWidth"];
  	var refWidth = 80;
  	var refHeight = 60;
	if (gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"] == "spa") {
		if (screenwidth == 414) {
			refHeight = 78;
			refWidth = 104;
		} else if (screenwidth == 375) {
			refHeight = 70;
			refWidth = 94;
		} else if (screenwidth == 360) {
			refHeight = 68;
			refWidth = 90;
		} else if (screenwidth == 384) {
			refHeight = 72;
			refWidth = 96;
		}
	} //End of if condition
	else if (gblDeviceInfo["name"] == "iPhone") {
		if (gblDeviceInfo["model"] == "iPhone 6 Plus") {
			refHeight = 77;
			refWidth = 103;
		} else if (gblDeviceInfo["model"] == "iPhone 6") {
			refHeight = 70;
			refWidth = 94;
		}
	} else if (gblDeviceInfo["name"] == "android") {
		if (screenwidth == 1080) {
			refHeight = 67;
			refWidth = 270;
		} else if (screenwidth == 720) {
			refHeight = 67;
			refWidth = 180;
		} else if (screenwidth == 768) {
			refHeight = 67;
			refWidth = 180;
		}
	} //End of else condition
	return refWidth + "-" + refHeight;
}

function getInboxAndMyOffersRefWidthAndHeight(){
	var screenwidth = gblDeviceInfo["deviceWidth"];
  	var refWidth = 300;
  	var refHeight = 225;
  	if (gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"] == "spa") {
  		//for SPA dimensions
  		if (screenwidth == 414) {
  			refHeight = 291;
  			refWidth = 388;
  		} else if (screenwidth == 375) {
  			refHeight = 264;
  			refWidth = 352;
  		} else if (screenwidth == 360) {
  			refHeight = 252;
  			refWidth = 338;
  		} else if (screenwidth == 384) {
  			refHeight = 270;
  			refWidth = 360;
  		}
  	} else if(gblDeviceInfo["name"] == "iPhone") {
  		if (gblDeviceInfo["model"] == "iPhone 6 Plus") {
  			refHeight = 291;
  			refWidth = 388;
  		} else if (gblDeviceInfo["model"] == "iPhone 6") {
  			refHeight = 263;
  			refWidth = 352;
  		}
  	} else if (gblDeviceInfo["name"] == "android") {
  		if (screenwidth == 1080) {
  			refHeight = 253;
  			refWidth = 1012;
  		} else if (screenwidth == 720) {
  			refHeight = 253;
  			refWidth = 675;
  		} else if (screenwidth == 768) {
  			refHeight = 253;
  			refWidth = 675;
  		}
  	}
  	return refWidth + "-" + refHeight;
}

function setPromotionsTimer(){
	try{
		
		kony.timer.schedule("promoTimer", setTimeforPromImage, 5, true);
	}catch(e){
		
	}
}

function setTimeforPromImage(){
	try{
		//
		var index = frmPromotionDetails.segPromImage.selectedIndex[1];
		if(index == 2){
			index = -1;
		}
		index = index+1;
		frmPromotionDetails.segPromImage.selectedIndex = [0,index];
	}catch(e){
		
		//kony.timer.cancel("promoTimer");
	}
}

function stopPromotionsTimer(){
	try{
		
		kony.timer.cancel("promoTimer");
	}catch(e){
		
	}
}

function onSelectPromtionPopUpdrop(){
	try{
		
		//var selectedValue = popPromotions.segPromtions.selectedItems[0].lblPromotionType;
		var indexValue= frmPromotion.cbxPromotion.selectedKey;
		promotionFilter = true;
		if(indexValue == 0){
			promotionSearchKey = "All Promotions";
		}else if(indexValue == 1){
			promotionSearchKey = "Deposit";
		}else if(indexValue == 2){
			promotionSearchKey = "Loan";
		}else if(indexValue == 3){
			promotionSearchKey = "Credit Card";
		}else if(indexValue == 4){
			promotionSearchKey = "Others";
		}
		
		//popPromotions.dismiss();
		showLoadingScreen();
		//frmPromotion.btnPromotions.text = selectedValue;
		frmPromotion.cbxPromotion.selectedKeyValue[1] = promotionSearchKey;
		successCallBackPromotions(400, gblPromotions)
	}catch(e){
		
	}
}

function onSelectPromtionPopUp(){
	try{
		
		var indexValue = popPromotions.segPromtions.selectedIndex[1];
		//var indexValue= frmPromotion.cbxPromotion.selectedKey;
		promotionFilter = true;
		if(indexValue == 0){
			promotionSearchKey = "All Promotions";
			frmPromotion.cbxPromotionbtn.text=kony.i18n.getLocalizedString("Promotions_AllPromotions");
		}else if(indexValue == 1){
			promotionSearchKey = "Deposit";
			frmPromotion.cbxPromotionbtn.text=kony.i18n.getLocalizedString("Promotions_Deposit");
		}else if(indexValue == 2){
			promotionSearchKey = "Loan";
			frmPromotion.cbxPromotionbtn.text=kony.i18n.getLocalizedString("Promotions_Loan");
		}else if(indexValue == 3){
			promotionSearchKey = "Credit Card";
			frmPromotion.cbxPromotionbtn.text=kony.i18n.getLocalizedString("Promotions_CreditCard");
		}else if(indexValue == 4){
			promotionSearchKey = "Others";
			frmPromotion.cbxPromotionbtn.text=kony.i18n.getLocalizedString("Promotions_Others");
		}		

		//popPromotions.dismiss();
		showLoadingScreen();
		//frmPromotion.btnPromotions.text = selectedValue;
		//frmPromotion.cbxPromotionbtn.text=promotionSearchKey;
		//frmPromotion.cbxPromotion.selectedKeyValue[1] = promotionSearchKey;
		popPromotions.dismiss();
		successCallBackPromotions(400, gblPromotions)
	}catch(e){
		
	}
}


function onClickShareIcon(){
	try{
		
		var btnSkin = frmPromotionDetails.btnSave.skin;
		if(btnSkin == "btnShare"){
			frmPromotionDetails.btnSave.skin = "btnShareFoc";
			frmPromotionDetails.hboxSaveImage.setVisibility(true);
			frmPromotionDetails.imgHeaderMiddle.src = "empty.png";
			frmPromotionDetails.imgHeaderRight.src = "arrowtop.png";
		}else if(btnSkin == "btnShareFoc"){
			frmPromotionDetails.btnSave.skin = "btnShare";
			frmPromotionDetails.hboxSaveImage.setVisibility(false);
			frmPromotionDetails.imgHeaderMiddle.src = "arrowtop.png";
			frmPromotionDetails.imgHeaderRight.src = "empty.png";
		}
	}catch(e){
		
	}
}

function savePromotionsPDF(filetype) {
	try{
		
		var inputParam = {};
		var dynamicdata = {};
		//showLoadingScreen();
		var currentLocale = kony.i18n.getCurrentLocale();
		dynamicdata={
		   			 "image1URL" : imageURL1,
		      		 "image2URL" : imageURL2,
		   			 "image3URL" : imageURL3,
		       		 "para1text" : promotionDetailsText1,
		  			 "para2text" : promotionDetailsText2,
		  			 "para3text" : promotionDetailsText3,
		  			 "para4text" : promotionDetailsText4,
		  			 "para5text" : promotionDetailsText5,
		  			 "localeId" : currentLocale,
		  			 "tittle" : promotionTitle
		    		};
		inputParam["outputtemplatename"]= "promotionaldetails";
		inputParam["filetype"] = filetype;
		inputParam["templatename"] = "PromotionDetailsTemplate";
		inputParam["nowatermark"] = "Y";
		inputParam["datajson"] = JSON.stringify(dynamicdata, "", "");
		
		
		invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
		//invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
	}catch(e){
		
	}
}

function sharePromotionOnFB(){
	try{
		
		gblPOWcustNME = "TMB User"
		gblPOWtransXN = "TMBPromotions"//promotionDetailsText1+" "+promotionDetailsText2+" "+promotionDetailsText3+" "+promotionDetailsText4+" "+promotionDetailsText5;
		gblPOWchannel = "MB"
		requestfromform = "frmPromotions"
		postOnWall();
	}catch(e){
		
	}
}

function frmPromotionPreShow()
{
changeStatusBarColor();
		//#ifdef android
			frmPromotionPreShowAndroid();
		//#endif

		//#ifdef iphone
			frmPromotionPreShowIphone();
		//#endif
}

function frmPromotionPreShowAndroid(){
	try{
		//alert("Test point");
		if(kony.application.getCurrentForm().id == "frmPromotion" || kony.application.getPreviousForm().id == "frmPromotion"){
			//popPromotions.destroy();
			frmPromotion.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotions");
			//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			//loadPromotionCxb();
			frmPromotion.cbxPromotionbtn.text=kony.i18n.getLocalizedString("Promotions_AllPromotions");
			//promotionSearchKey = frmPromotion.cbxPromotion.selectedKeyValue[1];
			//alert("promotionSearchKey : "+promotionSearchKey);
			//frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			//successCallBackPromotions(400, gblCurrentPromotions);
			onClickBackPromotionDetails();
			if(promotionSearchKey == "All Promotions"){
				frmPromotion.cbxPromotionbtn.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			}else if(promotionSearchKey == "Deposit"){
				frmPromotion.cbxPromotionbtn.text = kony.i18n.getLocalizedString("Promotions_Deposit");
			}else if(promotionSearchKey == "Loan"){
				frmPromotion.cbxPromotionbtn.text = kony.i18n.getLocalizedString("Promotions_Loan");
			}else if(promotionSearchKey == "Credit Card"){
				frmPromotion.cbxPromotionbtn.text = kony.i18n.getLocalizedString("Promotions_CreditCard");
			}else if(promotionSearchKey == "Others"){
				frmPromotion.cbxPromotionbtn.text = kony.i18n.getLocalizedString("Promotions_Others");
			}
		}
	}catch(e){
		//alert(e)
	}
}

function frmPromotionPreShowIphone(){
	try{
		//alert("Test point");
		if(kony.application.getCurrentForm().id == "frmPromotion" || kony.application.getPreviousForm().id == "frmPromotion"){
			//popPromotions.destroy();
			frmPromotion.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotions");
			//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			loadPromotionCxb();
			//promotionSearchKey = frmPromotion.cbxPromotion.selectedKeyValue[1];
			//alert("promotionSearchKey : "+promotionSearchKey);
			//frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			//successCallBackPromotions(400, gblCurrentPromotions);
			onClickBackPromotionDetails();
			//if(promotionSearchKey == "All Promotions"){
//				//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
//				frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_AllPromotions");
//			}else if(promotionSearchKey == "Deposit"){
//				//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_Deposit");
//				frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_Deposit");
//			}else if(promotionSearchKey == "Loan"){
//				//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_Loan");
//				frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_Loan");
//			}else if(promotionSearchKey == "Credit Card"){
//				//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_CreditCard");
//				frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_CreditCard");
//			}else if(promotionSearchKey == "Others"){
//				//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_Others");
//				frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_Others");
//			}
		}
	}catch(e){
		//alert(e)
	}
}

function frmPromotionDetailsPreShow(){
changeStatusBarColor();
	//alert("frmPromotionDetailsPreShow");
	try{
		
		if(kony.application.getCurrentForm().id == "frmPromotionDetails" || kony.application.getPreviousForm().id == "frmPromotionDetails"){
			//popPromotions.destroy();
			frmPromotion.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotions");
			synPromotiondetails();
			//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			try{
				//frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_AllPromotions");
				if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {
					frmPromotion.cbxPromotionbtn.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			 	 }	else {
					frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			 	 }
			}catch(e1){
				//alert("frmPromotionDetailsPreShow"+e1);
			}
			
			//successCallBackPromotions(400, gblCurrentPromotions);
		}
	}catch(e){
		
	}
}


function synPromotiondetails(){
	try{
		
		var curLoc = "";
		if(kony.i18n.getCurrentLocale() == "en_US"){
			curLoc = "EN"
		}else if(kony.i18n.getCurrentLocale() == "th_TH"){
			curLoc = "TH"
		}		
			
		frmPromotionDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotionsDetails"); 
		frmPromotionDetails.bttnBack.text = kony.i18n.getLocalizedString("Back");
		frmPromotionDetails.button1399939188243213.text = kony.i18n.getLocalizedString("Back");
		if ("04" == gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["hotpromoLinkFlagMob"]) {
			frmPromotionDetails.btnMBpromo.text = kony.i18n.getLocalizedString("FindTMB_Call");
		}else{
			frmPromotionDetails.btnMBpromo.text = kony.i18n.getLocalizedString("keyClick");
		}
		frmPromotionDetails.lblPromTitle.text = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["subject"+curLoc];
		promotionTitle = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["subject"+curLoc];
		var descp = "";
		descp = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine1"+curLoc]+"<br/>"+gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine2"+curLoc]+"<br/>"+gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine3"+curLoc]+"<br/>"+gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine4"+curLoc]+"<br/>"+gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine5"+curLoc];
		// to fix DEF338 QA defect, commentted below code.
		//if(flowSpa) {
//       		frmPromotionDetails.lblRichText.text =  descp;
//       	} else {
//       		frmPromotionDetails.rchPromDetails.text = descp;
//       	}
		//alert("descp : "+descp);
       	frmPromotionDetails.rchPromDetails.text = descp;        	
        promotionDetailsText1 = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine1"+curLoc];
		promotionDetailsText2 = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine2"+curLoc];
		promotionDetailsText3 = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine3"+curLoc];
		promotionDetailsText4 = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine4"+curLoc];
		promotionDetailsText5 = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["longMsgLine5"+curLoc];
		
		var imageData = [];
		imageURL1 = ""; imageURL2 = ""; imageURL3 = "";
		imageURL1 = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["imgHotpromoId1Mob"+curLoc];
		imageURL2 = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["imgHotpromoId2Mob"+curLoc];
		imageURL3 = gblCurrentPromotions["hotPromotions"][promotionSelectIndex]["imgHotpromoId3Mob"+curLoc];
		if(imageURL1 != ""){
			imageData.push({imgProm: {src: imageURL1}})
		}
		if(imageURL2 != ""){
		   	imageData.push({imgProm: {src: imageURL2}})
		}
		if(imageURL3 != ""){
			imageData.push({imgProm: {src: imageURL3}})
		}
		//imageData = [{imgProm: {src: imageURL1}},{imgProm: {src: imageURL2}},{imgProm: {src: imageURL3}}];
		frmPromotionDetails.segPromImage.setData(imageData);
		frmPromotionDetails.segPromImage.selectedIndex = [0,0];
		frmPromotionDetails.show();
	}catch(e){
		//alert("synPromotiondetails : "+e);
	}
}

function onClickBackPromotionDetails(){
	try{
		gblMyOffersDetails = false;
		frmPromotion.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuHotPromotions");
		/*if(promotionSearchKey == "All Promotions"){
			//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_AllPromotions");
			frmPromotion.cbxPromotion.selectedKey = "0";
		}else if(promotionSearchKey == "Deposit"){
			//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_Deposit");
			frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_Deposit");
			frmPromotion.cbxPromotion.selectedKey = "1";
		}else if(promotionSearchKey == "Loan"){
			//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_Loan");
			frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_Loan");
			frmPromotion.cbxPromotion.selectedKey = "2";
		}else if(promotionSearchKey = "Credit Card"){
			//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_CreditCard");
			frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_CreditCard");
			frmPromotion.cbxPromotion.selectedKey = "3";
		}else if(promotionSearchKey = "Others"){
			//frmPromotion.btnPromotions.text = kony.i18n.getLocalizedString("Promotions_Others");
			frmPromotion.cbxPromotion.selectedKeyValue[1] = kony.i18n.getLocalizedString("Promotions_Others");
			frmPromotion.cbxPromotion.selectedKey = "4";
		} */
		successCallBackPromotions(400, gblCurrentPromotions);
	}catch(e){
		
	}
}

function frmPromotion_btnHdrMenu_onClick() {
  //frmPromotion.scrollboxMain.scrollToEnd();
   //gblIndex = -1;
   // isMenuShown = false;
    handleMenuBtn();
}

function setDatatoSeg()
{
	var tem=[];
	
	popPromotions.segPromtions.widgetDataMap = {
			lblPromotionType: "lblPromotionType"
		}
		
	temp = [{
	"lblPromotionType": kony.i18n.getLocalizedString("Promotions_AllPromotions")
	}, {
	"lblPromotionType": kony.i18n.getLocalizedString("Promotions_Deposit")
	}, {
	"lblPromotionType": kony.i18n.getLocalizedString("Promotions_Loan")
	}, {
	"lblPromotionType": kony.i18n.getLocalizedString("Promotions_CreditCard")
	},{
	"lblPromotionType": kony.i18n.getLocalizedString("Promotions_Others")
	}];
	
	popPromotions.segPromtions.setData(temp);
	popPromotions.show();
}
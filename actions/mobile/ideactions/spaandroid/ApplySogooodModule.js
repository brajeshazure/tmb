function loadInterestPlans() {
    var interestPlans = [];
    var plan1 = gblPlan1;
    var plan2 = gblPlan2;
    var plan3 = gblPlan3;
    frmMBSoGooodPlanSelect.lblCardPlan1.text = plan1 + " " + kony.i18n.getLocalizedString("keymonths");
    frmMBSoGooodPlanSelect.lblCardPlan2.text = plan2 + " " + kony.i18n.getLocalizedString("keymonths");
    frmMBSoGooodPlanSelect.lblCardPlan3.text = plan3 + " " + kony.i18n.getLocalizedString("keymonths");
    frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal1.text = "";
    frmMBSoGooodPlanSelect.lblTotalInterestValue1.text = "";
    frmMBSoGooodPlanSelect.lblTotalAmountVal1.text = "";
    frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal2.text = "";
    frmMBSoGooodPlanSelect.lblTotalInterestValue2.text = "";
    frmMBSoGooodPlanSelect.lblTotalAmountVal2.text = "";
    frmMBSoGooodPlanSelect.lblMonthlyInstallmentVal3.text = "";
    frmMBSoGooodPlanSelect.lblTotalInterestValue3.text = "";
    frmMBSoGooodPlanSelect.lblTotalAmountVal3.text = "";
    frmMBSoGooodPlanSelect.FlexPlanDetail1.setVisibility(true);
    frmMBSoGooodPlanSelect.FlexPlanDetail2.setVisibility(true);
    frmMBSoGooodPlanSelect.FlexPlanDetail3.setVisibility(true);
    calculateAllPlanInterestForTxns();
}

function OnPlanSelectService(eventObject) {
    var buttonId = eventObject.id;
    var isToggle = "false";
    var planId = "";
    if (kony.string.equalsIgnoreCase(buttonId, "btnPlan1")) {
        if (frmMBSoGooodPlanSelect.btnPlan1.skin == "btnScheduleEndLeftFocus") {
            frmMBSoGooodPlanSelect.btnPlan1.skin = "btnScheduleEndLeft";
            isToggle = "false";
        } else {
            frmMBSoGooodPlanSelect.btnPlan1.skin = "btnScheduleEndLeftFocus";
            isToggle = "true";
        }
        frmMBSoGooodPlanSelect.btnPlan2.skin = "btnScheduleEndMid";
        frmMBSoGooodPlanSelect.btnPlan3.skin = "btnScheduleEndRight";
        planId = "1";
        plan = gblPlan1;
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnPlan2")) {
        frmMBSoGooodPlanSelect.btnPlan1.skin = "btnScheduleEndLeft";
        if (frmMBSoGooodPlanSelect.btnPlan2.skin == "btnScheduleEndMidFocus") {
            frmMBSoGooodPlanSelect.btnPlan2.skin = "btnScheduleEndMid";
            isToggle = "false";
        } else {
            frmMBSoGooodPlanSelect.btnPlan2.skin = "btnScheduleEndMidFocus";
            isToggle = "true";
        }
        frmMBSoGooodPlanSelect.btnPlan3.skin = "btnScheduleEndRight";
        planId = "2";
        plan = gblPlan2;
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnPlan3")) {
        frmMBSoGooodPlanSelect.btnPlan1.skin = "btnScheduleEndLeft";
        frmMBSoGooodPlanSelect.btnPlan2.skin = "btnScheduleEndMid";
        if (frmMBSoGooodPlanSelect.btnPlan3.skin == "btnScheduleRightFocus") {
            frmMBSoGooodPlanSelect.btnPlan3.skin = "btnScheduleEndRight";
            isToggle = "false";
        } else {
            frmMBSoGooodPlanSelect.btnPlan3.skin = "btnScheduleRightFocus";
            isToggle = "true";
        }
        planId = "3";
        plan = gblPlan3;
    }
    if (isToggle == "true") {
        disableOKButtonOfPlanSelect();
        calculateInterestForTxns(plan, "MB");
    } else {
        disableOKButtonOfPlanSelect();
        calculateInterestForTxns("untoggle", "MB");
    }
    //calculateInterestOnplanSelect(planId, plan, "MB");
}

function OnNewPlanSelectService(eventObject) {
    var buttonId = eventObject.id;
    var buttonSelect = eventObject.text;
    var planId = "";
    if (buttonSelect == kony.i18n.getLocalizedString("btnSelect")) {
        if (kony.string.equalsIgnoreCase(buttonId, "btnSelect1")) {
            planId = "1";
            plan = gblPlan1;
        } else if (kony.string.equalsIgnoreCase(buttonId, "btnSelect2")) {
            planId = "2";
            plan = gblPlan2;
        } else if (kony.string.equalsIgnoreCase(buttonId, "btnSelect3")) {
            planId = "3";
            plan = gblPlan3;
        }
        MBconfirmCalculateInterestForTxns(plan, "MB");
    }
}
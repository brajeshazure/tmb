gblSelectBillerCategoryID = "0";
gblBillerPresentInMyBills = false;
gblMyBillList = [];
gblDisplayBalanceBillPayment = true;
serviceCallRef1 = "";
serviceCallRef2 = "";

function otherBillerSelected() {
    gblBillerPresentInMyBills = false;
    clearBillpaymentScreen();
    launchBillPaymentFirstTime();
    gblFirstTimeBillPayment = false;
    gblBillPayFromScan = false;
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.hbxRef.setVisibility(true);
    gblToAccountKey = frmSelectBiller.segMyBills.selectedItems[0].ToAccountKey;
    gblBillerID = "";
    gblBillerMethod = frmSelectBiller.segMyBills.selectedItems[0].BillerMethod;
    gblPayFull = frmSelectBiller.segMyBills.selectedItems[0].IsFullPayment;
    gblRef1LblEN = frmSelectBiller.segMyBills.selectedItems[0].hdnLabelRefNum1EN.text;
    gblRef1LblTH = frmSelectBiller.segMyBills.selectedItems[0].hdnLabelRefNum1TH.text;
    gblRef2LblEN = frmSelectBiller.segMyBills.selectedItems[0].hdnLabelRefNum2EN.text;
    gblRef2LblTH = frmSelectBiller.segMyBills.selectedItems[0].hdnLabelRefNum2TH.text;
    gblBillerCompCodeEN = frmSelectBiller.segMyBills.selectedItems[0].lblBillerNameENFull;
    gblBillerCompCodeTH = frmSelectBiller.segMyBills.selectedItems[0].lblBillerNameTHFull;
    gblbillerGroupType = frmSelectBiller.segMyBills.selectedItems[0].BillerGroupType;
    gblreccuringDisableAdd = frmSelectBiller.segMyBills.selectedItems[0].IsRequiredRefNumber2Add;
    gblreccuringDisablePay = frmSelectBiller.segMyBills.selectedItems[0].IsRequiredRefNumber2Pay;
    gblBillerBancassurance = frmSelectBiller.segMyBills.selectedItems[0].billerBancassurance;
    gblAllowRef1AlphaNum = frmSelectBiller.segMyBills.selectedItems[0].allowRef1AlphaNum;
    frmBillPayment.hbxRef1.setVisibility(true);
    //frmBillPayment.lineRef2.setVisibility(true);
    if (gblreccuringDisablePay == "Y") {
        frmBillPayment.hbxRef2.setVisibility(true);
        frmBillPayment.lineRef1.setVisibility(true);
    } else {
        frmBillPayment.hbxRef2.setVisibility(false);
        frmBillPayment.lineRef1.setVisibility(false);
    }
    //ENH_113
    gblCompCode = frmSelectBiller.segMyBills.selectedItems[0].BillerCompCode;
    if (gblCompCode == "2533") {
        frmBillPayment.lblForFullPayment.setVisibility(true);
        frmBillPayment.tbxAmount.setVisibility(false);
        gblSegBillerDataMB = frmSelectBiller.segMyBills.selectedItems[0];
        gBillerStartTime = frmSelectBiller.segMyBills.selectedItems[0].billerStartTime;
        gBillerEndTime = frmSelectBiller.segMyBills.selectedItems[0].billerEndTime;
        allowSchedule = frmSelectBiller.segMyBills.selectedItems[0].billerAllowSetSched;
        if (allowSchedule == "N") {
            frmBillPayment.hbxPayBillOn.setEnabled(false);
        } else {
            frmBillPayment.hbxPayBillOn.setEnabled(true);
        }
    } else {
        frmBillPayment.hbxPayBillOn.setEnabled(true);
    }
    gblbillerServiceType = frmSelectBiller.segMyBills.selectedItems[0].billerServiceType;
    gblbillerTransType = frmSelectBiller.segMyBills.selectedItems[0].billerTransType;
    gblMeaFeeAmount = frmSelectBiller.segMyBills.selectedItems[0].billerFeeAmount;
    frmBillPayment.tbxRef2Value.setEnabled(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    frmBillPayment.lblCompCode.text = shortenBillerName(frmSelectBiller.segMyBills.selectedItems[0].lblSuggestedBiller, 18);
    frmBillPayment.imgBillerImage.src = frmSelectBiller.segMyBills.selectedItems[0].imgSuggestedBiller.src;
    if (gblBillerBancassurance == "Y" || gblAllowRef1AlphaNum == "Y") {
        frmBillPayment.lblRef1Value.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
        frmBillPayment.lblRef1Value.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT;
    } else {
        frmBillPayment.lblRef1Value.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
        frmBillPayment.lblRef1Value.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    }
    frmBillPayment.lblRef1Value.text = "";
    frmBillPayment.lblRef1Value.setFocus(true);
    frmBillPayment.lblRef1Value.maxTextLength = parseInt(frmSelectBiller.segMyBills.selectedItems[0].Ref1Len.text);
    frmBillPayment.lblRef1ValueMasked.text = "";
    frmBillPayment.lblRef1.text = frmSelectBiller.segMyBills.selectedItems[0].Ref1Label;
    frmBillPayment.lblRef2.text = frmSelectBiller.segMyBills.selectedItems[0].Ref2Label;
    frmBillPayment.tbxRef2Value.text = "";
    frmBillPayment.tbxRef2Value.maxTextLength = parseInt(frmSelectBiller.segMyBills.selectedItems[0].Ref2Len.text);
    callBillPaymentCustomerAccountService();
}

function frmSelectBillerLandingPreShow() {
    changeStatusBarColor();
    gblTopUpMore = 0;
    gblsearchtxt = "";
    isSearched = false;
    isCatSelected = false;
    frmSelectBillerLanding.hbxMyBills.setVisibility(false);
    frmSelectBillerLanding.segMyBills.setVisibility(false);
    frmSelectBillerLanding.imgBillPayLanding.setVisibility(false);
    frmSelectBillerLanding.scrollboxMain.scrollToEnd();
    frmSelectBillerLanding.lblHdrTxt.text = kony.i18n.getLocalizedString("keyBillPayment");
    frmSelectBillerLanding.lblMyBills.text = kony.i18n.getLocalizedString("keyMyBills");
    var locale = kony.i18n.getCurrentLocale();
    displaySelectBillerActive(false);
    displayScanBillActive(false);
    getBillerListMB();
}

function frmSelectBillerLandingPostShow() {
    frmSelectBillerLanding.scrollboxMain.scrollToEnd();
}

function callBillPaymentFromMenu() {
    if (checkMBUserStatus()) {
        gblMyBillerTopUpBB = 0;
        GblBillTopFlag = true;
        glb_accId = 0;
        gblFromAccountSummary = false;
        callMasterBillerService();
    }
}

function showBillPaymentLandingForm() {
    gblFlagMenu = "";
    destroyMenuSpa()
    gblMyBillerTopUpBB = 0;
    gblFirstTimeBillPayment = true;
    gblRetryCountRequestOTP = "0";
    frmSelectBillerLanding.segMyBills.removeAll();
    frmSelectBillerLanding.show();
}

function getBillerListMB() {
    var inputParams = {
        IsActive: "1"
    };
    showLoadingScreen();
    invokeServiceSecureAsync("customerBillInquiry", inputParams, getBillerListMBCallback);
}

function getBillerListMBCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["CustomerBillInqRs"];
            gblMyBillList = [];
            if (responseData.length > 0) {
                dismissLoadingScreen();
                gblMyBillList = responseData;
                populateMyBillerMBForSelectBillerLanding(callBackResponse["CustomerBillInqRs"]);
            } else {
                dismissLoadingScreen();
                displayImgBillPayLanding(true);
                frmSelectBillerLanding.sboxRight.enableScrolling = false;
            }
        } else {
            dismissLoadingScreen();
            displayImgBillPayLanding(true);
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } else {
        if (status == 300) {
            dismissLoadingScreen();
            displayImgBillPayLanding(true);
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function displayImgBillPayLanding(isDisplay) {
    frmSelectBillerLanding.hbxMyBills.setVisibility(!isDisplay);
    frmSelectBillerLanding.segMyBills.setVisibility(!isDisplay);
    frmSelectBillerLanding.imgBillPayLanding.setVisibility(isDisplay);
    if (isDisplay) {
        var mainbillpay_img_name = "_main_billpay";
        var locale = "";
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            locale = "th";
        } else {
            locale = "en";
        }
        mainbillpay_img_name = locale + mainbillpay_img_name;
        var randomnum = Math.floor((Math.random() * 10000) + 1);
        var mainbillpay_img_path = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + mainbillpay_img_name + "&modIdentifier=PRODUCTPACKAGEIMG&rr=" + randomnum;
        frmSelectBillerLanding.imgBillPayLanding.src = mainbillpay_img_path;
    }
}

function populateMyBillerMBForSelectBillerLanding(collectionData) {
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    var locale = kony.i18n.getCurrentLocale();
    if (kony.string.startsWith(locale, "en", true) == true) {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(locale, "th", true) == true) {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    myTopUpListRs.length = 0;
    myBillerTopupListMB = collectionData;
    for (var i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            var billerName = collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")";
            var billerNameEN = collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")";
            var billerNameTH = collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")";
            var isRef2Req = collectionData[i]["IsRequiredRefNumber2Pay"];
            //ENH_113	
            gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay = "";
            gblbillerAllowSetSched = "";
            gblbillerTotalPayAmtEn = "";
            gblbillerTotalPayAmtTh = "";
            gblbillerTotalIntEn = "";
            gblbillerTotalIntTh = "";
            gblbillerDisconnectAmtEn = "";
            gblbillerDisconnectAmtTh = "";
            gblbillerServiceType = "";
            gblbillerTransType = "";
            gblMeaFeeAmount = "";
            gblbillerAmountEn = "";
            gblbillerAmountTh = "";
            gblbillerMeterNoEn = "";
            gblbillerMeterNoTh = "";
            gblbillerCustNameEn = "";
            gblbillerCustNameTh = "";
            gblbillerCustAddressEn = "";
            gblbillerCustAddressTh = "";
            gblbillerMeterNoEn = "";
            gblbillerMeterNoTh = "";
            gblbillerCustNameEn = "";
            gblbillerCustNameTh = "";
            gblbillerCustAddressEn = "";
            gblbillerCustAddressTh = "";
            if (collectionData[i]["BillerCompcode"] == "2533") {
                if (collectionData[i]["ValidChannel"] != undefined && collectionData[i]["ValidChannel"].length > 0) {
                    for (var t = 0; t < collectionData[i]["ValidChannel"].length; t++) {
                        if (collectionData[i]["ValidChannel"][t]["ChannelCode"] == "01") {
                            gblMeaFeeAmount = collectionData[i]["ValidChannel"][t]["FeeAmount"];
                        }
                    }
                }
                if (collectionData[i]["BillerMiscData"] != undefined && collectionData[i]["BillerMiscData"].length > 0) {
                    for (var j = 0; j < collectionData[i]["BillerMiscData"].length; j++) {
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                            gblbillerStartTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                            gblbillerEndTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                            gblbillerFullPay = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                            gblbillerAllowSetSched = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                            gblbillerMeterNoEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                            gblbillerMeterNoTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                            gblbillerCustNameEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                            gblbillerCustNameTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                            gblbillerCustAddressEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                            gblbillerCustAddressTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                            gblbillerTotalPayAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                            gblbillerTotalPayAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                            gblbillerAmountEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                            gblbillerAmountTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                            gblbillerTotalIntEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                            gblbillerTotalIntTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                            gblbillerDisconnectAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                            gblbillerDisconnectAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                            gblbillerServiceType = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                            gblbillerTransType = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                    }
                }
            }
            //ENH113 end
            var tempRecord = {
                "crmId": collectionData[i]["CRMID"],
                "lblBillerName": {
                    "text": shortenBillerName(billerName, 34)
                },
                "lblBillerNickname": {
                    "text": collectionData[i]["BillerNickName"]
                },
                "BillerCategoryID": {
                    "text": collectionData[i]["BillerCategoryID"]
                },
                "BillerCompCode": {
                    "text": collectionData[i]["BillerCompcode"]
                },
                "lblRef1Value": collectionData[i]["ReferenceNumber1"],
                "lblRef1ValueMasked": maskCreditCard(collectionData[i]["ReferenceNumber1"]),
                "lblRef2Value": isNotBlank(collectionData[i]["ReferenceNumber2"]) ? collectionData[i]["ReferenceNumber2"] : "",
                "imgBillerLogo": {
                    "src": imagesUrl
                },
                "lblRef1": appendColon(collectionData[i][ref1label]),
                "lblRef2": (isRef2Req == "Y") ? appendColon(collectionData[i][ref2label]) : "",
                "lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"],
                "lblRef2TH": (isRef2Req == "Y") ? collectionData[i]["LabelReferenceNumber2TH"] : "",
                "lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"],
                "lblRef2EN": (isRef2Req == "Y") ? collectionData[i]["LabelReferenceNumber2EN"] : "",
                "Ref2Len": (isRef2Req == "Y") ? collectionData[i]["Ref2Len"] : "0",
                "Ref1Len": collectionData[i]["Ref1Len"],
                "lblBillerNameEN": shortenBillerName(billerNameEN, 20),
                "lblBillerNameTH": shortenBillerName(billerNameTH, 20),
                "lblBillerNameENFull": billerNameEN,
                "lblBillerNameTHFull": billerNameTH,
                "billerMethod": collectionData[i]["BillerMethod"],
                "billerGroupType": collectionData[i]["BillerGroupType"],
                "waiverCode": collectionData[i]["ReferenceNumber1"],
                "ToAccountKey": collectionData[i]["ToAccountKey"],
                "IsFullPayment": collectionData[i]["IsFullPayment"],
                "CustomerBillID": collectionData[i]["CustomerBillID"],
                "IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
                "IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
                "billerStartTime": gblbillerStartTime,
                "billerEndTime": gblbillerEndTime,
                "billerFullPay": gblbillerFullPay,
                "billerAllowSetSched": gblbillerAllowSetSched,
                "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                "billerTotalIntEn": gblbillerTotalIntEn,
                "billerTotalIntTh": gblbillerTotalIntTh,
                "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                "billerServiceType": gblbillerServiceType,
                "billerTransType": gblbillerTransType,
                "billerFeeAmount": gblMeaFeeAmount,
                "billerAmountEn": gblbillerAmountEn,
                "billerAmountTh": gblbillerAmountTh,
                "billerMeterNoEn": gblbillerMeterNoEn,
                "billerMeterNoTh": gblbillerMeterNoTh,
                "billerCustNameEn": gblbillerCustNameEn,
                "billerCustNameTh": gblbillerCustNameTh,
                "billerCustAddressEn": gblbillerCustAddressEn,
                "billerCustAddressTh": gblbillerCustAddressTh,
                "billerBancassurance": collectionData[i]["IsBillerBancassurance"],
                "allowRef1AlphaNum": collectionData[i]["AllowRef1AlphaNum"]
            }
            myTopUpListRs.push(tempRecord);
        }
    }
    if (myTopUpListRs.length > 0) {
        displayImgBillPayLanding(false);
    } else {
        displayImgBillPayLanding(true);
        frmSelectBillerLanding.sboxRight.enableScrolling = false;
    }
    mySelectBillerListMB = [];
    mySelectBillerListMB = myTopUpListRs;
    gblCustomerBillList = [];
    gblCustomerBillList = myTopUpListRs;
    frmSelectBillerLanding.segMyBills.removeAll();
    frmSelectBillerLanding.segMyBills.setData(myTopUpListRs);
}

function onClickSelectbillerCategory() {
    callBillerTopUpCategoryService();
}

function myBillsRowSelectedOnPreLanding() {
    showLoadingScreen();
    gblBillerPresentInMyBills = true;
    clearBillpaymentScreen();
    launchBillPaymentFirstTime();
    gblFirstTimeBillPayment = false;
    gblBillPayFromScan = false;
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.hbxRef.setVisibility(true);
    gblFromAccountSummaryBillerAdded = false;
    gblToAccountKey = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["ToAccountKey"];
    gblBillerID = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["CustomerBillID"];
    gblBillerMethod = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerMethod"];
    gblPayFull = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["IsFullPayment"];
    gblRef1LblTH = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1TH"];
    gblRef2LblTH = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef2TH"];
    gblRef1LblEN = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1EN"];
    gblRef2LblEN = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef2EN"];
    gblBillerCompCodeEN = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblBillerNameENFull"];
    gblBillerCompCodeTH = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblBillerNameTHFull"];
    gblbillerGroupType = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerGroupType"];
    gblreccuringDisableAdd = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["IsRequiredRefNumber2Add"];
    gblreccuringDisablePay = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["IsRequiredRefNumber2Pay"];
    gblBillerBancassurance = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerBancassurance"];
    gblAllowRef1AlphaNum = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["allowRef1AlphaNum"];
    frmBillPayment.hbxRef1.setVisibility(true);
    //frmBillPayment.lineRef2.setVisibility(true);
    if (gblreccuringDisablePay == "Y") {
        frmBillPayment.hbxRef2.setVisibility(true);
        frmBillPayment.lineRef1.setVisibility(true);
    } else {
        frmBillPayment.hbxRef2.setVisibility(false);
        frmBillPayment.lineRef1.setVisibility(false);
    }
    //ENH_113
    gblCompCode = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["BillerCompCode"]["text"]
    if (gblCompCode == "2533") {
        gblSegBillerDataMB = frmSelectBillerLanding["segMyBills"]["selectedItems"][0];
        gBillerStartTime = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerStartTime"];
        gBillerEndTime = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerEndTime"];
        allowSchedule = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerAllowSetSched"]
        if (allowSchedule == "N") {
            frmBillPayment.hbxPayBillOn.setEnabled(false);
        } else {
            frmBillPayment.hbxPayBillOn.setEnabled(true);
        }
    } else {
        frmBillPayment.hbxPayBillOn.setEnabled(true);
    }
    gblbillerServiceType = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerServiceType"];
    gblbillerTransType = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerTransType"];
    gblMeaFeeAmount = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["billerFeeAmount"];
    frmBillPayment.tbxRef2Value.setEnabled(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    if (gblBillerMethod == 0) {
        //for Offline Biller
        gblPenalty = false;
        gblFullPayment = false;
        frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
        frmBillPayment.hbxPenalty.setVisibility(false);
        frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
        frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
        frmBillPayment.hbxFullSpecButton.setVisibility(false);
        if (gblBillerBancassurance == "Y") {
            gblPolicyNumber = "";
            var inputParam = {};
            inputParam["policyNumber"] = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1Value"];
            inputParam["dataSet"] = "0";
            invokeServiceSecureAsync("BAGetPolicyDetails", inputParam, BillPaymentBAPolicyDetailsServiceCallBackMB);
        }
        callBillPaymentCustomerAccountService();
    } else if (gblBillerMethod == 1) {
        //for Online Biller
        // added for integration
        showLoadingScreen();
        gblScanAmount = 0;
        gblPenalty = false;
        var inputParam = {};
        inputParam["TrnId"] = "";
        inputParam["Amt"] = "0.00";
        inputParam["BankId"] = "011";
        inputParam["BranchId"] = "0001";
        inputParam["BankRefId"] = "";
        inputParam["Ref1"] = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1Value"];
        inputParam["Ref2"] = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef2Value"];
        inputParam["Ref3"] = "";
        inputParam["Ref4"] = "";
        inputParam["MobileNumber"] = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1Value"];
        inputParam["compCode"] = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["BillerCompCode"].text;
        inputParam["isChannel"] = "MB";
        inputParam["commandType"] = "Inquiry";
        if (gblCompCode == "2533") {
            inputParam["BillerStartTime"] = gBillerStartTime;
            inputParam["BillerEndTime"] = gBillerEndTime;
        }
        invokeServiceSecureAsync("onlinePaymentInq", inputParam, BillPaymentOnlinePaymentInqServiceCallBack);
        frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
        if (gblCompCode == "2533" || gblCompCode == "0016" || gblCompCode == "0947") {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(false);
        } else {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            if (!frmBillPayment.hbxFullSpecButton.isVisible) {
                frmBillPayment.hbxFullSpecButton.setVisibility(true);
                frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
                frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;
            }
        }
    } else if (gblBillerMethod == 2) {
        //for CreditCard and Ready cash
        showLoadingScreen();
        var inputParam = {};
        var toDayDate = getTodaysDate();
        var cardId = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1Value"];
        inputParam["cardId"] = cardId;
        inputParam["tranCode"] = TRANSCODEMIN;
        ownCard = false;
        var accountDetails = gblAccountTable;
        var accountLength = gblAccountTable["custAcctRec"].length;
        for (var i = 0; i < accountLength; i++) {
            if (gblAccountTable["custAcctRec"][i].accType == "CCA") {
                var accountNo = gblAccountTable["custAcctRec"][i].accId;
                accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
                if (accountNo == cardId) {
                    ownCard = true;
                    break;
                }
            }
        }
        if (!ownCard) {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(true);
            frmBillPayment.tbxAmount.setEnabled(true);
            frmBillPayment.tbxAmount.text = ""; //fullAmt
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            callBillPaymentCustomerAccountService();
        } else invokeServiceSecureAsync("creditcardDetailsInq", inputParam, BillPaymentcreditcardDetailsInqServiceCallBack);
    } else if (gblBillerMethod == 3) {
        showLoadingScreen();
        //for TMB Loan
        var inputParam = {};
        //for TMB Loan
        var TMB_BANK_FIXED_CODE = "0001";
        var TMB_BANK_CODE_ADD = "0011";
        var ZERO_PAD = "0000";
        var BRANCH_CODE;
        var ref2 = frmSelectBillerLanding.segMyBills.selectedItems[0].lblRef2Value;
        var ref1AccountIDLoan = frmSelectBillerLanding.segMyBills.selectedItems[0].lblRef1Value;
        var fiident;
        if (ref1AccountIDLoan.length == 10) {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
            ref1AccountIDLoan = "0" + ref1AccountIDLoan + ref2;
        }
        if (ref1AccountIDLoan.length == 13) {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
            ref1AccountIDLoan = "0" + ref1AccountIDLoan
        } else {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ref1AccountIDLoan[3] + ZERO_PAD;
        }
        inputParam["acctId"] = ref1AccountIDLoan;
        inputParam["acctType"] = "LOC";
        ownLoan = false;
        var accountDetails = gblAccountTable;
        var accountLength = gblAccountTable["custAcctRec"].length;
        for (var i = 0; i < accountLength; i++) {
            if (gblAccountTable["custAcctRec"][i].accType == "LOC") {
                var accountNo = gblAccountTable["custAcctRec"][i].accId;
                //accountNo = accountNo.substring(accountNo.length-13 , accountNo.length);
                if (accountNo == ref1AccountIDLoan) {
                    ownLoan = true;
                    break;
                }
            }
        }
        if (!ownLoan) {
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            gblPenalty = false;
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(true);
            frmBillPayment.tbxAmount.setEnabled(true);
            callBillPaymentCustomerAccountService();
        } else {
            invokeServiceSecureAsync("doLoanAcctInq", inputParam, BillPaymentdoLoanAcctInqServiceCallBack);
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            gblPenalty = false;
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(true);
            frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
            frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;
        }
    } else if (gblBillerMethod == 4) {
        //for Old Ready Cash
        var inputParam = {};
        var ref1AccountIDDeposit = frmSelectBillerLanding.segMyBills.selectedItems[0].lblRef1Value;
        inputparam["acctId"] = ref1AccountIDDeposit;
        ownRC = false;
        var accountDetails = gblAccountTable;
        var accountLength = gblAccountTable.length;
        for (var i = 0; i < accountLength; i++) {
            if (gblAccountTable.accType == "LOC") {
                var accountNo = gblAccountTable.accId;
                accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
                if (accountNo == ref1AccountIDDeposit) {
                    ownRC = true;
                    break;
                }
            }
        }
        if (!ownRC) {
            gblPenalty = false;
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("keyAmount");
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.tbxAmount.setEnabled(true);
            frmBillPayment.tbxAmount.setVisibility(true);
            callBillPaymentCustomerAccountService();
            return;
        }
        invokeServiceSecureAsync("depositAccountInquiry", inputParam, BillPaymentdepositAccountInquiryServiceCallBack);
        gblPenalty = false;
        frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
        frmBillPayment.hbxPenalty.setVisibility(false);
        frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
        frmBillPayment.hbxFullSpecButton.setVisibility(false);
        if (!frmBillPayment.hbxFullMinSpecButtons.isVisible) {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(true);
            frmBillPayment.button475004897849.skin = btnScheduleEndLeftFocus;
            frmBillPayment.button475004897851.skin = btnScheduleMid;
            frmBillPayment.button475004897853.skin = btnScheduleEndRight;
        }
    }
    //frmBillPayment.lblBillerName.text = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblBillerNickname"].text;
    frmBillPayment.lblCompCode.text = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblBillerNickname"].text;
    frmBillPayment.imgBillerImage.src = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["imgBillerLogo"].src;
    frmBillPayment.lblRef1Value.text = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1Value"];
    frmBillPayment.lblRef1Value.maxTextLength = parseInt(frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["Ref1Len"]);
    //below line is added for CR - PCI-DSS masked Credit card no
    frmBillPayment.lblRef1ValueMasked.text = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1ValueMasked"];
    frmBillPayment.lblRef1.text = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef1"];
    if (frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef2Value"] == undefined || frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef2Value"] == null) {
        frmBillPayment.tbxRef2Value.text = ""
    } else {
        frmBillPayment.tbxRef2Value.text = frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef2Value"];
        frmBillPayment.tbxRef2Value.maxTextLength = parseInt(frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["Ref2Len"]);
    }
    frmBillPayment.lblRef2.text = appendColon(frmSelectBillerLanding["segMyBills"]["selectedItems"][0]["lblRef2"]);
}

function onClickBackButtonbillerCategory() {
    if (gblMyBillerTopUpBB == 1) {
        gblCategoryFormClosed = true;
    }
    frmSelectBiller.show();
}

function onRowClickCategories() {
    showLoadingScreen();
    gblSelectBillerCategoryID = frmBillPaymentBillerCategories.segBillerCategories.selectedItems[0].BillerCategoryID.text;
    frmSelectBiller.lblCategories.text = frmBillPaymentBillerCategories.segBillerCategories.selectedItems[0].lblCategory.text;
    searchMyBillsAndSuggestedBillers();
    dismissLoadingScreen();
    frmSelectBiller.show();
}

function onClickSelectBillerSearch() {
    displaySelectBillerCancelBtn();
}

function displaySelectBillerCancelBtn() {
    frmSelectBiller.vbxCancel.setVisibility(true);
    frmSelectBiller.lblCancel.setVisibility(true);
    frmSelectBiller.vbxCancel.containerWeight = 18;
    frmSelectBiller.vbox508341360252723.containerWeight = 72;
    frmSelectBiller.vbxCancel.skin = "VboxDarkBlueBG";
    frmSelectBiller.vbxCancel.focusSkin = "VboxDarkBlueBG";
}

function disableSelectBillerCancelBtn() {
    frmSelectBiller.vbxCancel.setVisibility(false);
    frmSelectBiller.lblCancel.setVisibility(false);
    frmSelectBiller.vbxCancel.containerWeight = 0;
    frmSelectBiller.vbox508341360252723.containerWeight = 100;
    frmSelectBiller.vbxCancel.skin = NoSkin;
    frmSelectBiller.vbxCancel.focusSkin = NoSkin;
}

function onClickSelectBillerSearchCancelBtn() {
    frmSelectBiller.tbxSearch.text = "";
    frmSelectBiller.tbxSearch.setFocus(false);
    frmSelectBiller.hbxheaderRecipient.setFocus(true);
    disableSelectBillerCancelBtn();
    searchMyBillsAndSuggestedBillers();
}

function frmBillPaymentBillerCategoriesPreShow() {
    frmBillPaymentBillerCategories.lblHdrTxt.text = kony.i18n.getLocalizedString("MIB_BPCateTitle");
}

function displaySelectBillerActive(isActive) {
    var locale = kony.i18n.getCurrentLocale();
    frmSelectBillerLanding.lblSelectBiller.text = kony.i18n.getLocalizedString("keyBPSelectBiller");
    if (locale == 'en_US') {
        if (isActive) {
            frmSelectBillerLanding.imgSelectBillers.src = "icon_select_bill_active.png";
            frmSelectBillerLanding.lblSelectBiller.skin = "lblWhiteMedium200";
            frmSelectBillerLanding.vbxSelectBillers.skin = "vboxMenuShare";
        } else {
            frmSelectBillerLanding.imgSelectBillers.src = "icon_select_bill.png";
            frmSelectBillerLanding.lblSelectBiller.skin = "lblGreyMedium200";
            frmSelectBillerLanding.vbxSelectBillers.skin = "vboxWhiteBg";
        }
    } else {
        if (isActive) {
            frmSelectBillerLanding.imgSelectBillers.src = "icon_select_bill_active.png";
            frmSelectBillerLanding.lblSelectBiller.skin = "lblWhiteMedium200";
            frmSelectBillerLanding.vbxSelectBillers.skin = "vboxMenuShare";
        } else {
            frmSelectBillerLanding.imgSelectBillers.src = "icon_select_bill.png";
            frmSelectBillerLanding.lblSelectBiller.skin = "lblGreyMedium200";
            frmSelectBillerLanding.vbxSelectBillers.skin = "vboxWhiteBg";
        }
    }
}

function displayScanBillActive(isActive) {
    var locale = kony.i18n.getCurrentLocale();
    frmSelectBillerLanding.lblScanBarCode.text = kony.i18n.getLocalizedString("keyBPScanBill");
    if (locale == 'en_US') {
        if (isActive) {
            frmSelectBillerLanding.imgScanBarCode.src = "icon_scan_bill.png";
            frmSelectBillerLanding.lblScanBarCode.skin = "lblGreyMedium200";
            frmSelectBillerLanding.vbxScanBarCode.skin = "vboxWhiteBg";
        } else {
            frmSelectBillerLanding.imgScanBarCode.src = "icon_scan_bill.png";
            frmSelectBillerLanding.lblScanBarCode.skin = "lblGreyMedium200";
            frmSelectBillerLanding.vbxScanBarCode.skin = "vboxWhiteBg";
        }
    } else {
        if (isActive) {
            frmSelectBillerLanding.imgScanBarCode.src = "icon_scan_bill.png";
            frmSelectBillerLanding.lblScanBarCode.skin = "lblGreyMedium200";
            frmSelectBillerLanding.vbxScanBarCode.skin = "vboxWhiteBg";
        } else {
            frmSelectBillerLanding.imgScanBarCode.src = "icon_scan_bill.png";
            frmSelectBillerLanding.lblScanBarCode.skin = "lblGreyMedium200";
            frmSelectBillerLanding.vbxScanBarCode.skin = "vboxWhiteBg";
        }
    }
}

function onFocusLostOfRef1TextBox() {
    if (gblreccuringDisablePay == "Y") {
        if (isNotBlank(frmBillPayment.lblRef1Value.text) && isNotBlank(frmBillPayment.tbxRef2Value.text)) {
            if (!(frmBillPayment.lblRef1Value.text == serviceCallRef1 && frmBillPayment.tbxRef2Value.text == serviceCallRef2)) {
                verifyBillerMethodFromLanding();
            }
        }
    } else {
        if (isNotBlank(frmBillPayment.lblRef1Value.text)) {
            if (!(frmBillPayment.lblRef1Value.text == serviceCallRef1)) {
                verifyBillerMethodFromLanding();
            }
        }
    }
}

function onFocusLostOfRef2TextBox() {
    if (isNotBlank(frmBillPayment.lblRef1Value.text) && isNotBlank(frmBillPayment.tbxRef2Value.text)) {
        if (!(frmBillPayment.lblRef1Value.text == serviceCallRef1 && frmBillPayment.tbxRef2Value.text == serviceCallRef2)) {
            verifyBillerMethodFromLanding();
        }
    }
}

function verifyBillerMethodFromLanding() {
    serviceCallRef1 = frmBillPayment.lblRef1Value.text;
    serviceCallRef2 = frmBillPayment.tbxRef2Value.text;
    gblFullPayment = false;
    var billerNickName = getBillerNickNameFromMyBills(gblMyBillList, gblCompCode, frmBillPayment.lblRef1Value.text, frmBillPayment.tbxRef2Value.text);
    if (isNotBlank(billerNickName)) {
        frmBillPayment.lblCompCode.text = billerNickName;
    } else {
        var locale = kony.i18n.getCurrentLocale();
        if (locale == "en_US") {
            frmBillPayment.lblCompCode.text = shortenBillerName(gblBillerCompCodeEN, 18);
        } else {
            frmBillPayment.lblCompCode.text = shortenBillerName(gblBillerCompCodeTH, 18);
        }
    }
    showBillerNicknameForScheduleBillerNotInMyBills();
    frmBillPayment.tbxRef2Value.setEnabled(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    if (gblBillerMethod == 0) {
        //for Offline Biller
        frmBillPayment.tbxAmount.setVisibility(true);
        gblPenalty = false;
        gblFullPayment = false;
        frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
        frmBillPayment.hbxPenalty.setVisibility(false);
        frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
        frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
        frmBillPayment.hbxFullSpecButton.setVisibility(false);
        if (gblBillerBancassurance == "Y") {
            showLoadingScreen();
            gblPolicyNumber = "";
            var inputParam = {};
            inputParam["policyNumber"] = frmBillPayment.lblRef1Value.text;
            inputParam["dataSet"] = "0";
            frmBillPayment.tbxAmount.setFocus(false);
            invokeServiceSecureAsync("BAGetPolicyDetails", inputParam, BillPaymentBAPolicyDetailsServiceCallBackMB);
        }
    } else if (gblBillerMethod == 1) {
        //for Online Biller
        // added for integration
        showLoadingScreen();
        gblScanAmount = 0;
        gblPenalty = false;
        var inputParam = {};
        inputParam["TrnId"] = "";
        inputParam["Amt"] = "0.00";
        inputParam["BankId"] = "011";
        inputParam["BranchId"] = "0001";
        inputParam["BankRefId"] = "";
        inputParam["Ref1"] = frmBillPayment.lblRef1Value.text;
        inputParam["Ref2"] = frmBillPayment.tbxRef2Value.text;
        inputParam["Ref3"] = "";
        inputParam["Ref4"] = "";
        inputParam["MobileNumber"] = frmBillPayment.lblRef1Value.text;
        inputParam["compCode"] = gblCompCode;
        inputParam["isChannel"] = "MB";
        inputParam["commandType"] = "Inquiry";
        if (gblCompCode == "2533") {
            inputParam["BillerStartTime"] = gBillerStartTime;
            inputParam["BillerEndTime"] = gBillerEndTime;
        }
        frmBillPayment.tbxAmount.setFocus(false);
        invokeServiceSecureAsync("onlinePaymentInq", inputParam, BillPaymentOnlinePaymentInqServiceCallBack);
        frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
        if (gblCompCode == "2533" || gblCompCode == "0016" || gblCompCode == "0947") {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(false);
        } else {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            if (!frmBillPayment.hbxFullSpecButton.isVisible) {
                frmBillPayment.hbxFullSpecButton.setVisibility(true);
                frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
                frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;
            }
        }
    } else if (gblBillerMethod == 2) {
        //for CreditCard and Ready cash
        showLoadingScreen();
        var inputParam = {};
        var toDayDate = getTodaysDate();
        var cardId = frmBillPayment.lblRef1Value.text;
        inputParam["cardId"] = cardId;
        inputParam["tranCode"] = TRANSCODEMIN;
        ownCard = false;
        var accountDetails = gblAccountTable;
        var accountLength = gblAccountTable["custAcctRec"].length;
        for (var i = 0; i < accountLength; i++) {
            if (gblAccountTable["custAcctRec"][i].accType == "CCA") {
                var accountNo = gblAccountTable["custAcctRec"][i].accId;
                accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
                if (accountNo == cardId) {
                    ownCard = true;
                    break;
                }
            }
        }
        if (!ownCard) {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(true);
            frmBillPayment.tbxAmount.setEnabled(true);
            frmBillPayment.tbxAmount.text = ""; //fullAmt
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            dismissLoadingScreen();
        } else {
            frmBillPayment.tbxAmount.setFocus(false);
            invokeServiceSecureAsync("creditcardDetailsInq", inputParam, BillPaymentcreditcardDetailsInqServiceCallBack);
        }
    } else if (gblBillerMethod == 3) {
        showLoadingScreen();
        //for TMB Loan
        var inputParam = {};
        //for TMB Loan
        var TMB_BANK_FIXED_CODE = "0001";
        var TMB_BANK_CODE_ADD = "0011";
        var ZERO_PAD = "0000";
        var BRANCH_CODE;
        var ref2 = frmBillPayment.tbxRef2Value.text;
        var ref1AccountIDLoan = frmBillPayment.lblRef1Value.text;
        var fiident;
        if (ref1AccountIDLoan.length == 10) {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
            ref1AccountIDLoan = "0" + ref1AccountIDLoan + ref2;
        }
        if (ref1AccountIDLoan.length == 13) {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
            ref1AccountIDLoan = "0" + ref1AccountIDLoan
        } else {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ref1AccountIDLoan[3] + ZERO_PAD;
        }
        inputParam["acctId"] = ref1AccountIDLoan;
        inputParam["acctType"] = "LOC";
        ownLoan = false;
        var accountDetails = gblAccountTable;
        var accountLength = gblAccountTable["custAcctRec"].length;
        for (var i = 0; i < accountLength; i++) {
            if (gblAccountTable["custAcctRec"][i].accType == "LOC") {
                var accountNo = gblAccountTable["custAcctRec"][i].accId;
                //accountNo = accountNo.substring(accountNo.length-13 , accountNo.length);
                if (accountNo == ref1AccountIDLoan) {
                    ownLoan = true;
                    break;
                }
            }
        }
        if (!ownLoan) {
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            gblPenalty = false;
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(true);
            frmBillPayment.tbxAmount.setEnabled(true);
            dismissLoadingScreen();
        } else {
            frmBillPayment.tbxAmount.setFocus(false);
            invokeServiceSecureAsync("doLoanAcctInq", inputParam, BillPaymentdoLoanAcctInqServiceCallBack);
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            gblPenalty = false;
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(true);
            frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
            frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;
        }
    } else if (gblBillerMethod == 4) {
        //for Old Ready Cash
        var inputParam = {};
        var ref1AccountIDDeposit = frmBillPayment.lblRef1Value.text;
        inputparam["acctId"] = ref1AccountIDDeposit;
        ownRC = false;
        var accountDetails = gblAccountTable;
        var accountLength = gblAccountTable.length;
        for (var i = 0; i < accountLength; i++) {
            if (gblAccountTable.accType == "LOC") {
                var accountNo = gblAccountTable.accId;
                accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
                if (accountNo == ref1AccountIDDeposit) {
                    ownRC = true;
                    break;
                }
            }
        }
        if (!ownRC) {
            gblPenalty = false;
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.tbxAmount.setEnabled(true);
            frmBillPayment.tbxAmount.setVisibility(true);
            dismissLoadingScreenPopup();
            return;
        }
        frmBillPayment.tbxAmount.setFocus(false);
        invokeServiceSecureAsync("depositAccountInquiry", inputParam, BillPaymentdepositAccountInquiryServiceCallBack);
        gblPenalty = false;
        frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
        frmBillPayment.hbxPenalty.setVisibility(false);
        frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
        frmBillPayment.hbxFullSpecButton.setVisibility(false);
        if (!frmBillPayment.hbxFullMinSpecButtons.isVisible) {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(true);
            frmBillPayment.button475004897849.skin = btnScheduleEndLeftFocus;
            frmBillPayment.button475004897851.skin = btnScheduleMid;
            frmBillPayment.button475004897853.skin = btnScheduleEndRight;
        }
    }
}
//When User changed Ref1 or Ref2, Amount should clear 
//Amount should be calculated on Done of Ref1 and Ref2
function onTextChangeOfRef1AndRef2() {
    frmBillPayment.tbxBillerNickName.text = "";
    serviceCallRef1 = "";
    serviceCallRef2 = "";
    resetBillPayAmountDetailsMB();
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.tbxAmount.text = "";
    frmBillPayment.tbxAmount.placeholder = "0.00";
}

function resetBillPayAmountDetailsMB() {
    frmBillPayment.hbxPenalty.setVisibility(false);
    frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
    frmBillPayment.hbxFullSpecButton.setVisibility(false);
    frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
    frmBillPayment.tbxAmount.text = "";
    frmBillPayment.tbxAmount.placeholder = "0.00";
    frmBillPayment.lblForFullPayment.text = "0.00";
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.setVisibility(false);
}

function loadMEARelatedData(responseData) {
    //ENH113
    if (responseData["BillerCompcode"] == "2533") {
        if (responseData["ValidChannel"] != undefined && responseData["ValidChannel"].length > 0) {
            for (var t = 0; t < responseData["ValidChannel"].length; t++) {
                if (responseData["ValidChannel"][t]["ChannelCode"] == "01") {
                    gblMeaFeeAmount = responseData["ValidChannel"][t]["FeeAmount"];
                    gblSegBillerDataMB["billerFeeAmount"] = gblMeaFeeAmount;
                }
            }
        }
        if (responseData["BillerMiscData"] != undefined && responseData["BillerMiscData"].length > 0) {
            for (var j = 0; j < responseData["BillerMiscData"].length; j++) {
                if (responseData["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                    gblbillerStartTime = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerStartTime"] = gblbillerStartTime;
                    gBillerStartTime = gblbillerStartTime;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                    gblbillerEndTime = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerEndTime"] = gblbillerEndTime;
                    gBillerEndTime = gblbillerEndTime;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                    gblbillerFullPay = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerFullPay"] = gblbillerFullPay;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                    gblbillerAllowSetSched = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerAllowSetSched"] = gblbillerAllowSetSched;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                    gblbillerMeterNoEn = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerMeterNoEn"] = gblbillerMeterNoEn;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                    gblbillerMeterNoTh = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerMeterNoTh"] = gblbillerMeterNoTh;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                    gblbillerCustNameEn = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerCustNameEn"] = gblbillerCustNameEn;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                    gblbillerCustNameTh = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerCustNameTh"] = gblbillerCustNameTh;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                    gblbillerCustAddressEn = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerCustAddressEn"] = gblbillerCustAddressEn;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                    gblbillerCustAddressTh = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerCustAddressTh"] = gblbillerCustAddressTh;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                    gblbillerTotalPayAmtEn = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerTotalPayAmtEn"] = gblbillerTotalPayAmtEn;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                    gblbillerTotalPayAmtTh = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerTotalPayAmtTh"] = gblbillerTotalPayAmtTh;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                    gblbillerAmountEn = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerAmountEn"] = gblbillerAmountEn;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                    gblbillerAmountTh = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerAmountTh"] = gblbillerAmountTh;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                    gblbillerTotalIntEn = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerTotalIntEn"] = gblbillerTotalIntEn;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                    gblbillerTotalIntTh = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerTotalIntTh"] = gblbillerTotalIntTh;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                    gblbillerDisconnectAmtEn = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerDisconnectAmtEn"] = gblbillerDisconnectAmtEn;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                    gblbillerDisconnectAmtTh = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerDisconnectAmtTh"] = gblbillerDisconnectAmtTh;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                    gblbillerServiceType = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerServiceType"] = gblbillerServiceType;
                }
                if (responseData["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                    gblbillerTransType = responseData["BillerMiscData"][j]["MiscText"];
                    gblSegBillerDataMB["billerTransType"] = gblbillerTransType;
                }
            }
        }
    }
}

function hideUnhideBalanceBillPayment() {
    if (gblDisplayBalanceBillPayment) {
        frmBillPaymentComplete.hbxBalanceAfter.setVisibility(false);
        frmBillPaymentComplete.hbxFreeTrans.setVisibility(false);
        frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("show");
        gblDisplayBalanceBillPayment = false;
    } else {
        frmBillPaymentComplete.hbxBalanceAfter.setVisibility(true);
        if (frmBillPaymentConfirmationFuture.hbxFreeTrans.isVisible) {
            //if (gblBillpaymentNoFee){
            frmBillPaymentComplete.hbxFreeTrans.setVisibility(true);
        } else {
            frmBillPaymentComplete.hbxFreeTrans.setVisibility(false);
        }
        frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
        gblDisplayBalanceBillPayment = true;
    }
}

function onClickCancelBillPayConfirm() {
    callCustomerAccountService();
}

function formatBillPayAmountOnTextChange() {
    var enteredAmount = frmBillPayment.tbxAmount.text;
    if (isNotBlank(enteredAmount)) {
        enteredAmount = kony.string.replace(enteredAmount, ",", "");
        if (isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0) {;
        } else {
            frmBillPayment.tbxAmount.text = commaFormattedTransfer(enteredAmount);
        }
    }
}

function formatTopUpAmountOnTextChange() {
    var enteredAmount = frmTopUp.tbxExcludingBillerOne.text;
    if (isNotBlank(enteredAmount)) {
        enteredAmount = kony.string.replace(enteredAmount, ",", "");
        if (isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0) {;
        } else {
            frmTopUp.tbxExcludingBillerOne.text = commaFormattedTransfer(enteredAmount);
        }
    }
}

function onDoneEditingAmountTopUp() {
    frmTopUp.tbxExcludingBillerOne.text = onDoneEditingAmountValue(frmTopUp.tbxExcludingBillerOne.text);
}

function formatAmountBillPayOnTextChangeIB() {
    var enteredAmount = frmIBBillPaymentLP.txtBillAmt.text;
    if (isNotBlank(enteredAmount)) {
        enteredAmount = kony.string.replace(enteredAmount, ",", "");
        frmIBBillPaymentLP.txtBillAmt.text = commaFormattedTransfer(enteredAmount);
    }
}

function formatAmountTopUpOnTextChangeIB() {
    var enteredAmount = frmIBTopUpLandingPage.txtAmount.text;
    if (isNotBlank(enteredAmount)) {
        enteredAmount = kony.string.replace(enteredAmount, ",", "");
        frmIBTopUpLandingPage.txtAmount.text = commaFormattedTransfer(enteredAmount);
    }
}

function onDoneEditingAmountValue(amount) {
    if (!isNotBlank(amount) || parseFloat(amount) == 0) {
        return "";
    }
    if (amount.indexOf(".") > -1) {
        return numberWithCommas(fixedToTwoDecimal(amount));
    }
    return amount;
}

function removeCurrencyThaiBath(data) {
    if (isNotBlank(data)) {
        var AcctID;
        for (var i = 0; i < data.length; i++) {
            if (data[i] != kony.i18n.getLocalizedString("currencyThaiBaht")) {
                if (AcctID == null) {
                    AcctID = data[i];
                } else {
                    AcctID = AcctID + data[i];
                }
            }
        }
        return AcctID;
    }
    return "";
}
//MIB-2555 - MEA detail popup
function showMEADetailsOnPopUp() {
    popupMEADetails.lblRef1.text = frmBillPayment.lblRef1.text;
    popupMEADetails.lblRef1Val.text = frmBillPayment.lblRef1Value.text;
    popupMEADetails.lblMEACustName.text = frmBillPayment.lblMEACustName.text;
    popupMEADetails.lblMEACustNameValue.text = frmBillPayment.lblMEACustNameValue.text;
    popupMEADetails.lblMeterNo.text = frmBillPayment.lblMeterNo.text;
    popupMEADetails.lblMeterNoVal.text = frmBillPayment.lblMeterNoVal.text;
    if (kony.i18n.getCurrentLocale() == "en_US") {
        popupMEADetails.lblBillerNameCompCode.text = shortenBillerName(gblBillerCompCodeEN, 26);
        popupMEADetails.lblAddress.text = gblSegBillerDataMB["billerCustAddressEn"];
    } else {
        popupMEADetails.lblBillerNameCompCode.text = shortenBillerName(gblBillerCompCodeTH, 26);
        popupMEADetails.lblAddress.text = gblSegBillerDataMB["billerCustAddressTh"];
    }
    popupMEADetails.lblAddressVal.text = gblSegBillerDataMB["CustAddress"];
    popupMEADetails.btnClosePop.text = kony.i18n.getLocalizedString("keyClose");
    popupMEADetails.show();
}

function closeMEAPopup() {
    popupMEADetails.dismiss();
}

function showBillerNicknameForScheduleBillerNotInMyBills() {
    if (gblPaynow || gblBillerPresentInMyBills) {
        frmBillPayment.tbxBillerNickName.text = "";
        frmBillPayment.hbxBillerNickName.setVisibility(false);
        frmBillPayment.hbxBillerNickNameInfo.setVisibility(false);
        frmBillPayment.lineBillerNickName.setVisibility(false);
    } else {
        frmBillPayment.hbxBillerNickName.setVisibility(true);
        frmBillPayment.hbxBillerNickNameInfo.setVisibility(true);
        frmBillPayment.lineBillerNickName.setVisibility(true);
    }
}
//Auto focus to Ref1 if not blank else Amount if it is offline My Bill
function autoFocusAfterSelectBiller() {
    if (kony.application.getPreviousForm().id != "frmBillPaymentConfirmationFuture") {
        if (kony.application.getPreviousForm().id != "frmSchedule") {
            if (isNotBlank(frmBillPayment.lblRef1Value.text)) {
                if (frmBillPayment.hbxRef2.isVisible) {
                    if (isNotBlank(frmBillPayment.tbxRef2Value.text)) {
                        if (frmBillPayment.tbxAmount.isVisible) {
                            frmBillPayment.tbxAmount.setFocus(true);
                        }
                    } else {
                        frmBillPayment.tbxRef2Value.setFocus(true);
                    }
                } else {
                    if (frmBillPayment.tbxAmount.isVisible) {
                        frmBillPayment.tbxAmount.setFocus(true);
                    }
                }
            } else {
                frmBillPayment.lblRef1Value.setFocus(true);
            }
        } else {
            if (frmBillPayment.hbxBillerNickName.isVisible) {
                frmBillPayment.tbxBillerNickName.setFocus(true);
            }
        }
    }
}

function frmBillPaymentPostShow() {
    if (GblBillTopFlag) {
        gblFirstTimeBillPayment = false;
    } else {
        gblFirstTimeTopUp = false;
    }
    autoFocusAfterSelectBiller();
}

function callBillerTopUpCategoryService() {
    var tmpBillerCatGroupType = "";
    if (gblMyBillerTopUpBB == 0) {
        tmpBillerCatGroupType = gblBillerCategoryGroupType;
    } else if (gblMyBillerTopUpBB == 1) {
        tmpBillerCatGroupType = gblTopupCategoryGroupType;
    }
    var inputParams = {
        BillerCategoryGroupType: tmpBillerCatGroupType,
        clientDate: getCurrentDate()
    };
    showLoadingScreen();
    invokeServiceSecureAsync("billerCategoryInquiry", inputParams, callBillerTopUpCategoryServiceCallback);
}

function callBillerTopUpCategoryServiceCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            dismissLoadingScreen();
            var collectionData = callBackResponse["BillerCategoryInqRs"];
            var masterData = [];
            var tempData = "";
            var tmpCatDesc = "";
            var locale = kony.i18n.getCurrentLocale();
            if (kony.string.startsWith(locale, "en", true) == true) tmpCatDesc = "BillerCategoryDescEN";
            else if (kony.string.startsWith(locale, "th", true) == true) tmpCatDesc = "BillerCategoryDescTH";
            if (gblSelectBillerCategoryID == 0) {
                tempData = {
                    "lblCategory": {
                        "text": kony.i18n.getLocalizedString("MIB_BPAllCate"),
                        "skin": "lblBOzone171"
                    },
                    "BillerCategoryID": {
                        "text": 0
                    },
                    "hbox967430408167242": {
                        "skin": "hbxGreyBGstudio7"
                    }
                }
            } else {
                tempData = {
                    "lblCategory": {
                        "text": kony.i18n.getLocalizedString("MIB_BPAllCate")
                    },
                    "BillerCategoryID": {
                        "text": "0"
                    }
                }
            }
            masterData.push(tempData);
            for (var i = 0; i < collectionData.length; i++) {
                if (gblSelectBillerCategoryID == collectionData[i]["BillerCategoryID"]) {
                    tempData = {
                        "lblCategory": {
                            "text": collectionData[i][tmpCatDesc],
                            "skin": "lblBOzone171"
                        },
                        "lblCategoryEN": {
                            "text": collectionData[i]["BillerCategoryDescEN"],
                            "skin": "lblBOzone171"
                        },
                        "lblCategoryTH": {
                            "text": collectionData[i]["BillerCategoryDescTH"],
                            "skin": "lblBOzone171"
                        },
                        "BillerCategoryID": {
                            "text": collectionData[i]["BillerCategoryID"]
                        },
                        "imgArrow": "navarrow3.png",
                        "hbox967430408167242": {
                            "skin": "hbxGreyBGstudio7"
                        }
                    }
                } else {
                    tempData = {
                        "lblCategory": {
                            "text": collectionData[i][tmpCatDesc],
                            "skin": "lblBlueOzone171"
                        },
                        "lblCategoryEN": {
                            "text": collectionData[i]["BillerCategoryDescEN"],
                            "skin": "lblBOzone171"
                        },
                        "lblCategoryTH": {
                            "text": collectionData[i]["BillerCategoryDescTH"],
                            "skin": "lblBOzone171"
                        },
                        "BillerCategoryID": {
                            "text": collectionData[i]["BillerCategoryID"]
                        },
                        "imgArrow": "navarrow3.png"
                    }
                }
                masterData.push(tempData);
            }
            frmBillPaymentBillerCategories.segBillerCategories.setData(masterData);
            frmBillPaymentBillerCategories.show();
        } else {
            dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    } else {
        dismissLoadingScreen();
        if (status == 300) {
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}

function postShowFrmSchedule() {
    var prevForm = kony.application.getPreviousForm();
    if (prevForm["id"] == "frmBillPayment" || prevForm["id"] == "frmTopUp") {
        frmSchedule.lblPayOn.text = kony.i18n.getLocalizedString("keyBillPayPayBillOn");
    } else if (prevForm["id"] == "frmTransferLanding") {
        frmSchedule.lblPayOn.text = kony.i18n.getLocalizedString("TREnter_DebitOn");
    } else {
        frmSchedule.lblPayOn.text = "";
    }
}
var gblsegmentdataMB = [];
var count = 0;
var bindata = "";
var binlength = "";
var creditype;
var wcode;
var remainingcredit;
var startDateStmt = "";
var endDateStmt = "";
var isSortedMB;
var yeararray = [];
var responseData = "";
var noOfStmtServiceCalls = 0;
/**
 * Sort Data based on the sorting criteria in the column
 */
/*function sortbydateMBAscend(array, key,seq) {
    return array.sort(function (a, b) {
        var x = sortDateMB(a[key]);
        var y = sortDateMB(b[key]);
        var d = a[seq];
        var e = b[seq];
        return ((x < y) ? -1 : ((x > y) ? 1 : ((d > e))?1:-1));
    });
}*/
/*function sortDateMB(str) {
	var mdy = str.split('/')
	return new Date(mdy[2], mdy[1] - 1, mdy[0]);
}*/
/**
 * Sort Data based on the sorting criteria in the column
 */
/*function sortbydateMBDescend(array, key,seq) {
    return array.sort(function (a, b) {
        var x = sortDateMB(a[key]);
        var y = sortDateMB(b[key]);
        var d = a[seq];
        var e = b[seq];
        return ((x > y) ? -1 : ((x < y) ? 1 : ((d < e))?1:-1));
    });
}*/
function getdescriptionMB() {
    var descstmt;
    var channelstmt;
    if (gaccType == "SDA" || gaccType == "DDA") {
        descstmt = kony.i18n.getLocalizedString("keyMBDescriptionc");
        channelstmt = kony.i18n.getLocalizedString("keyMB_Channel");
    } else if (gaccType == "LOC") {
        descstmt = kony.i18n.getLocalizedString("keyMBPrinciplePaid");
        channelstmt = kony.i18n.getLocalizedString("keyMBInterestPaid");
    } else if (gaccType == "CCA") {
        descstmt = kony.i18n.getLocalizedString("keyMBDescriptionc");
        //		channelstmt = "";
    } else if (gaccType == "CDA") {
        descstmt = kony.i18n.getLocalizedString("keyMBDescriptionc");
        channelstmt = kony.i18n.getLocalizedString("keyMB_Channel");
    }
    var tab1 = [];
    var indexOfSelectedIndex = frmAccountStatementMB.segcredit.selectedItems[0];
    var indexOfSelectedRow = frmAccountStatementMB.segcredit.selectedIndex[1];
    var indexOfSelectedRowexp = gblsegmentdataMB[indexOfSelectedRow];
    if (gaccType == "CCA") {
        if (indexOfSelectedIndex.ishidden == "yes") {
            var selectedData = [{
                lblDate: indexOfSelectedIndex.lblDate,
                lblAmount: indexOfSelectedIndex.lblAmount,
                lblDescription: indexOfSelectedIndex.lblDescription,
                ishidden: "no",
                descexp: descstmt,
                descexpval: indexOfSelectedRowexp.descexpval,
                channelexp: channelstmt,
                channelexpval: indexOfSelectedRowexp.channelexpval,
                template: hbxaccntstmtexpCC
            }];
        }
        if (indexOfSelectedIndex.ishidden == "no") {
            var selectedData = [{
                lblDate: indexOfSelectedIndex.lblDate,
                lblAmount: indexOfSelectedIndex.lblAmount,
                lblDescription: indexOfSelectedIndex.lblDescription,
                ishidden: "yes",
                template: hbxaccntstmtCC
            }];
        }
    } else {
        if (indexOfSelectedIndex.ishidden == "yes") {
            var selectedData = [{
                lblDate: indexOfSelectedIndex.lblDate,
                lblAmount: indexOfSelectedIndex.lblAmount,
                lblDescription: indexOfSelectedIndex.lblDescription,
                ishidden: "no",
                descexp: descstmt,
                descexpval: indexOfSelectedRowexp.descexpval,
                channelexp: channelstmt,
                channelexpval: indexOfSelectedRowexp.channelexpval,
                template: hbxaccntstmtexpanded
            }];
        }
        if (indexOfSelectedIndex.ishidden == "no") {
            var selectedData = [{
                lblDate: indexOfSelectedIndex.lblDate,
                lblAmount: indexOfSelectedIndex.lblAmount,
                lblDescription: indexOfSelectedIndex.lblDescription,
                ishidden: "yes",
                template: hbxaccntstmt
            }];
        }
    }
    kony.table.insert(tab1, selectedData[0]);
    var tab2 = []
    for (var i = 0; i < indexOfSelectedRow; i++) {
        kony.table.insert(tab2, gblsegmentdataMB[i])
    }
    kony.table.append(tab2, tab1);
    for (var j = indexOfSelectedRow + 1; j < gblsegmentdataMB.length; j++) {
        kony.table.insert(tab2, gblsegmentdataMB[j]);
    }
    //frmAccountStatementMB.segcredit.removeAll();
    frmAccountStatementMB.segcredit.setData(tab2);
}
/**
 * view full statement for card management
 */
function viewFullStatementFromCardMgt() {
    gblStatementFlow = "frmMBManageCard";
    showLoadingScreen();
    if (isNotBlank(gblAccountTable["custAcctRec"]) && gblAccountTable["custAcctRec"].length > 0 && isNotBlank(gblSelectedCard) && isNotBlank(gblSelectedCard["acctsCardRefId"])) {
        //set gblIndex
        for (var i = 0; i < gblAccountTable["custAcctRec"].length; i++) {
            if (gblAccountTable["custAcctRec"][i]["cardRefId"] == gblSelectedCard["acctsCardRefId"]) {
                gblIndex = i;
                break;
            }
        }
        if (isNotBlank(gblIndex)) {
            gaccType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
            glb_viewName = gblAccountTable["custAcctRec"][gblIndex]["VIEW_NAME"];
            var inputparam = {};
            inputparam["cardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
            inputparam["tranCode"] = TRANSCODEUN;
            invokeServiceSecureAsync("creditcardDetailsInq", inputparam, viewFullStatementFromCardMgtCallBack);
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
}

function viewFullStatementFromCardMgtCallBack(status, result) {
    if (status == 400 && result["opstatus"] == 0) {
        //set some value for display in frmAccountStatementMB
        var accNickName = "";
        if (null != gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]) {
            accNickName = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
        } else {
            var sbStr = gblAccountTable["custAcctRec"][gblIndex]["accId"];
            sbStr = sbStr.substring(length - 4, length);
            if (locale == "en_US") {
                accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"] + " " + sbStr;
            } else {
                accNickName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"] + " " + sbStr;
            }
        }
        frmAccountStatementMB.lblname.text = accNickName;
        var randomnum = Math.floor((Math.random() * 10000) + 1);
        var imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"] + "&modIdentifier=PRODICON&dummy=" + randomnum;
        frmAccountStatementMB.imgacnt.src = imageName;
        frmAccountStatementMB.lblamounttot.text = commaFormatted(result["availCrLimit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        noOfStmtServiceCalls = 0;
        var input_params = {};
        invokeServiceSecureAsync("GetServerDateTime", input_params, clBackFullStmtShow);
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
}
/**
 * Method to invoke Full statement for accounts
 */
function viewFullStatement() {
    gblStatementFlow = "frmAccountDetailsMB";
    showLoadingScreen();
    noOfStmtServiceCalls = 0;
    var input_params = {};
    invokeServiceSecureAsync("GetServerDateTime", input_params, clBackFullStmtShow)
        //frmAccountStatementMB.show(); 
}

function clBackChangeMonth(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            getMonthCycleTest(result["date"], frmAccountStatementMB);
        }
    }
}

function clBackFullStmtShow(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            getMonthCycleTest(result["date"], frmAccountStatementMB);
            setSortBtnSkinMB();
            dismissLoadingScreen();
            frmAccountStatementMB.show();
        } else {
            dismissLoadingScreen();
        }
    }
}

function getMonthCycleTest(date, formname) {
    var montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"),
        kony.i18n.getLocalizedString("keyCalendarApr"), kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
        kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"), kony.i18n.getLocalizedString("keyCalendarSep"),
        kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")
    ];
    //todaysDate=date.replace("/", "-");
    todaysDate = date.split("/")[0] + "-" + date.split("/")[1] + "-" + date.split("/")[2];
    var mm = date.split("/")[1];
    var fullYear = date.split("/")[0];
    //yy = fullYear.toString();
    yy = fullYear.substring(2, 4);
    formname.btnm6.text = montharray[mm - 1] + " " + yy;
    //frmAccountStatementMB.btnm6.text= montharray[mm-1]+" "+yy;
    yeararray[5] = fullYear;
    mm = mm - 1;
    if (mm == 0) {
        mm = 12;
        fullYear = fullYear - 1;
        yy = fullYear.toString();
        yy = yy.substring(2, 4);
    }
    formname.btnm5.text = montharray[mm - 1] + " " + yy;
    //frmAccountStatementMB.btnm5.text= montharray[mm-1]+" "+yy;
    yeararray[4] = fullYear;
    mm = mm - 1;
    if (mm == 0) {
        mm = 12;
        fullYear = fullYear - 1;
        yy = fullYear.toString();
        yy = yy.substring(2, 4);
    }
    formname.btnm4.text = montharray[mm - 1] + " " + yy;
    //frmAccountStatementMB.btnm4.text= montharray[mm-1]+" "+yy;
    yeararray[3] = fullYear;
    mm = mm - 1;
    if (mm == 0) {
        mm = 12;
        fullYear = fullYear - 1;
        yy = fullYear.toString();
        yy = yy.substring(2, 4);
    }
    formname.btnm3.text = montharray[mm - 1] + " " + yy;
    //frmAccountStatementMB.btnm3.text= montharray[mm-1]+" "+yy;
    yeararray[2] = fullYear;
    mm = mm - 1;
    if (mm == 0) {
        mm = 12;
        fullYear = fullYear - 1;
        yy = fullYear.toString();
        yy = yy.substring(2, 4);
    }
    formname.btnm2.text = montharray[mm - 1] + " " + yy;
    //frmAccountStatementMB.btnm2.text= montharray[mm-1]+" "+yy;
    yeararray[1] = fullYear;
    mm = mm - 1;
    if (mm == 0) {
        mm = 12;
        fullYear = fullYear - 1;
        yy = fullYear.toString();
        yy = yy.substring(2, 4);
    }
    formname.btnm1.text = montharray[mm - 1] + " " + yy;
    //frmAccountStatementMB.btnm1.text= montharray[mm-1]+" "+yy;
    yeararray[0] = fullYear;
}

function viewAccntFullStatement() {
    sortOrderStmtMB = "descending";
    if (gblStatementFlow != "frmMBManageCard") {
        frmAccountStatementMB.lblname.text = frmAccountDetailsMB.lblAccountNameHeader.text;
        frmAccountStatementMB.imgacnt.src = frmAccountDetailsMB.imgAccountDetailsPic.src;
        if (gaccType == "CCA") {
            frmAccountStatementMB.lblamounttot.text = frmAccountDetailsMB.lblAccountBalanceHeader.text;
        } else {
            frmAccountStatementMB.lblamounttot.text = frmAccountDetailsMB.lblAccountBalanceHeader.text;
        }
    }
    endDateStmt = todaysDate;
    startDateStmt = todaysDate.split("-")[0] + "-" + todaysDate.split("-")[1] + "-" + "01"; //dateFormatChange(new Date(new Date().getFullYear(),new Date().getMonth(),1)); 
    gblsegmentdataMB = [];
    if (gaccType == "SDA" || gaccType == "DDA") {
        frmAccountStatementMB.hbxaccnt.setVisibility(true);
        frmAccountStatementMB.hbxbilled.setVisibility(false);
        frmAccountStatementMB.hbxunbilled.setVisibility(false);
        frmAccountStatementMB.hbxcreditheader.setVisibility(false);
        frmAccountStatementMB.lblHeaderTransaction.contentAlignment = 3;
        frmAccountStatementMB.segcredit.rowTemplate = hbxaccntstmt;
        frmAccountStatementMB.hbxSegHeader.setVisibility(true);
        frmAccountStatementMB.hbxCCheader.setVisibility(false);
        endDateStmt = todaysDate;
        startDateStmt = todaysDate.split("-")[0] + "-" + todaysDate.split("-")[1] + "-" + "01"; //dateFormatChange(new Date(new Date().getFullYear(),new Date().getMonth(),1)); 
        onMonth6Click();
        //frmAccountStatementMB.lblDateHeader.text= kony.i18n.getLocalizedString("keyDate");
        //		frmAccountStatementMB.lblHeaderTransaction.text=kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
        //		frmAccountStatementMB.lblHeaderBalance.text=kony.i18n.getLocalizedString("Balance");
        startCASAstatementservice(startDateStmt, endDateStmt);
    } else if (gaccType == "LOC") {
        frmAccountStatementMB.hbxaccnt.setVisibility(true);
        frmAccountStatementMB.hbxbilled.setVisibility(false);
        frmAccountStatementMB.hbxunbilled.setVisibility(false);
        frmAccountStatementMB.hbxcreditheader.setVisibility(false);
        frmAccountStatementMB.lblHeaderTransaction.contentAlignment = 3;
        frmAccountStatementMB.segcredit.rowTemplate = hbxaccntstmt;
        frmAccountStatementMB.hbxSegHeader.setVisibility(true);
        frmAccountStatementMB.hbxCCheader.setVisibility(false);
        endDateStmt = todaysDate;
        startDateStmt = todaysDate.split("-")[0] + "-" + todaysDate.split("-")[1] + "-" + "01"; //dateFormatChange(new Date(new Date().getFullYear(),new Date().getMonth(),1)); 
        onMonth6Click();
        //frmAccountStatementMB.lblDateHeader.text= kony.i18n.getLocalizedString("keyDate");
        //		frmAccountStatementMB.lblHeaderTransaction.text= kony.i18n.getLocalizedString("keyMBPaymentAmount");
        //		frmAccountStatementMB.lblHeaderBalance.text= kony.i18n.getLocalizedString("keyMBOutstandingstmt");
        startLoanStatementservice(startDateStmt, endDateStmt);
    } else if (gaccType == "CCA") {
        frmAccountStatementMB.hbxaccnt.setVisibility(false);
        frmAccountStatementMB.hbxbilled.setVisibility(false);
        frmAccountStatementMB.hbxunbilled.setVisibility(true);
        frmAccountStatementMB.hbxcreditheader.setVisibility(true);
        frmAccountStatementMB.lblHeaderTransaction.contentAlignment = 5;
        frmAccountStatementMB.segcredit.rowTemplate = hbxaccntstmtCC;
        frmAccountStatementMB.hbxSegHeader.setVisibility(false);
        frmAccountStatementMB.hbxCCheader.setVisibility(true);
        frmAccountStatementMB.btnunbilled.skin = "btnunbilledfoc";
        frmAccountStatementMB.btnunbilled.focusSkin = "btnunbilledfoc";
        frmAccountStatementMB.btnbilled.skin = "btnunbilled";
        frmAccountStatementMB.btnbilled.focusSkin = "btnunbilled";
        //frmAccountStatementMB.lbltransdate.text= kony.i18n.getLocalizedString("keyMBTransactionDate");
        //		frmAccountStatementMB.lbldescheader.text= kony.i18n.getLocalizedString("keyMBDescription");
        //		frmAccountStatementMB.lblbalHeader.text= kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
        startCCstatementservice("unbilled1", "0"); //make it 2
    } else if (gaccType == "CDA") {
        frmAccountStatementMB.hbxaccnt.setVisibility(true);
        frmAccountStatementMB.hbxbilled.setVisibility(false);
        frmAccountStatementMB.hbxunbilled.setVisibility(false);
        frmAccountStatementMB.hbxcreditheader.setVisibility(false);
        frmAccountStatementMB.lblHeaderTransaction.contentAlignment = 3;
        frmAccountStatementMB.segcredit.rowTemplate = hbxaccntstmt;
        frmAccountStatementMB.hbxSegHeader.setVisibility(true);
        frmAccountStatementMB.hbxCCheader.setVisibility(false);
        endDateStmt = todaysDate;
        startDateStmt = todaysDate.split("-")[0] + "-" + todaysDate.split("-")[1] + "-" + "01"; //dateFormatChange(new Date(new Date().getFullYear(),new Date().getMonth(),1)); 
        onMonth6Click();
        //frmAccountStatementMB.lblDateHeader.text= kony.i18n.getLocalizedString("keyDate");
        //		frmAccountStatementMB.lblHeaderTransaction.text=kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
        //		frmAccountStatementMB.lblHeaderBalance.text=kony.i18n.getLocalizedString("Balance");
        startTDstatementservice(startDateStmt, endDateStmt);
    }
}
/**
 * Method to invoke service for CCstatement
 */
function startCCstatementservice(cctype, wCode) {
    showLoadingScreen();
    wcode = wCode;
    var transcode;
    if (cctype == "unbilled1") {
        transcode = "8190";
        creditype = "unbilled1";
    } else if (cctype == "unbilled2") {
        transcode = "8292";
        creditype = "unbilled2";
    } else if (cctype == "billed") {
        transcode = "8290";
        creditype = "billed";
    }
    var todayDate = todaysDate; //getTodaysDateStmt();
    var CCinputParam = {};
    CCinputParam["sessionFlag"] = gblStmntSessionFlag;
    CCinputParam["CardId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    CCinputParam["WaiverCode"] = wcode;
    CCinputParam["TranCode"] = transcode;
    CCinputParam["PostedDt"] = todayDate;
    CCinputParam["BinData"] = bindata;
    CCinputParam["BinLength"] = binlength;
    CCinputParam["channelID"] = "MB";
    CCinputParam["flagSPA"] = "true";
    CCinputParam["accName"] = frmAccountStatementMB.lblname.text;
    CCinputParam["accType"] = gaccType;
    //Added for #36150 ---- pdf show all transactions 
    if (transcode != "8190") {
        noOfStmtServiceCalls++;
        CCinputParam["noOfServiceCall"] = noOfStmtServiceCalls + "";
        CCinputParam["serviceName"] = "CCstatement";
        invokeServiceSecureAsync("getAccntTransStmt", CCinputParam, startCCstatementserviceCallback);
    } else {
        invokeServiceSecureAsync("CCstatement", CCinputParam, startCCstatementserviceCallback);
    }
}
/**
 * Callback method for startCCstatementserviceCallback()
 */
function startCCstatementserviceCallback(status, callBackResponse) {
    var accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            //activityLogServiceCall("047", "", "01", "",  accId, frmAccountStatementMB.lblname.text, gaccType, "", "", "");
            gblStmntSessionFlag = callBackResponse["sesFlag"];
            kony.application.dismissLoadingScreen();
            //Fixing ticket 31556 issue where the upper contents are erased
            if (callBackResponse["StatusCode"] == 0) {
                if (callBackResponse["NewResult"].length == 0) {
                    //
                    //alert(" " + keyalertMBNoRecord);
                    frmAccountStatementMB.segcredit.removeAll();
                    frmAccountStatementMB.hbxLink.setVisibility(false);
                    //frmAccountStatementMB.hbxcreditheader.setVisibility(false);
                    alert(kony.i18n.getLocalizedString("NoRecsFound"));
                    return;
                }
                if (creditype == "unbilled1") {
                    remainingcredit = callBackResponse["NewResult"][0]["AvailCrLimit"] != undefined ? callBackResponse["NewResult"][0]["AvailCrLimit"] : "";
                    gblStmntSessionFlag = "2";
                    noOfStmtServiceCalls = 0;
                    startCCstatementservice("unbilled2", "0");
                    return;
                } else if (binlength == "" && creditype == "unbilled2") {
                    frmAccountStatementMB.hbxunbilled.setVisibility(true);
                    //remainingcredit =  callBackResponse["NewResult"][0]["AvailCrLimit"]!=undefined?callBackResponse["NewResult"][0]["AvailCrLimit"] :"";
                    frmAccountStatementMB.lbllimit.text = kony.i18n.getLocalizedString("keyMBCreditlimit");
                    frmAccountStatementMB.lbloutbal.text = kony.i18n.getLocalizedString("keyMBOutstanding");
                    frmAccountStatementMB.lblcycle.text = kony.i18n.getLocalizedString("keyMBSpendcycle");
                    frmAccountStatementMB.lblpoints.text = kony.i18n.getLocalizedString("keyMBAvailableR");
                    frmAccountStatementMB.lblexpire.text = kony.i18n.getLocalizedString("keyMBexpiring");
                    frmAccountStatementMB.lblexp.text = kony.i18n.getLocalizedString("keyMBPointexpiry");
                    //	frmAccountStatementMB.lbllimitval.text=commaFormatted(callBackResponse["NewResult"][0]["AvailCrLimit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmAccountStatementMB.lbllimitval.text = commaFormatted(remainingcredit) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    if (callBackResponse["NewResult"][0]["AvailCrLimit"] != undefined) {
                        frmAccountStatementMB.lbloutbalval.text = commaFormatted(callBackResponse["NewResult"][0]["AvailCrLimit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    } else {
                        frmAccountStatementMB.lbloutbalval.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    }
                    //frmAccountStatementMB.lbloutbalval.text= commaFormatted(callBackResponse["NewResult"][0]["AvailCrLimit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmAccountStatementMB.lblcycleval.text = commaFormatted(callBackResponse["NewResult"][0]["LedgerBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    //frmAccountStatementMB.lblpointsval.text= commaFormattedPoints(callBackResponse["NewResult"][0]["BonusPointAvail"]) +" "+ kony.i18n.getLocalizedString("keyMBPoints");
                    //frmAccountStatementMB.lblexpireval.text= commaFormattedPoints(callBackResponse["NewResult"][0]["BonusPointUsed"]) +" "+ kony.i18n.getLocalizedString("keyMBPoints");
                    //frmAccountStatementMB.lblpointsval.text= commaFormattedPoints(gblCC_RemainingPoint) +" "+ kony.i18n.getLocalizedString("keyMBPoints");
                    //frmAccountStatementMB.lblexpireval.text= commaFormattedPoints(gblCC_PointExpRemaining) +" "+ kony.i18n.getLocalizedString("keyMBPoints");
                    if (gblCC_RemainingPoint == "" || gblCC_RemainingPoint == null) gblCC_RemainingPoint = "0";
                    if (gblCC_PointExpRemaining == "" || gblCC_PointExpRemaining == null) gblCC_PointExpRemaining = "0";
                    frmAccountStatementMB.lblpointsval.text = commaFormattedPoints(gblCC_RemainingPoint) + " " + kony.i18n.getLocalizedString("keyMBPoints");
                    frmAccountStatementMB.lblexpireval.text = commaFormattedPoints(gblCC_PointExpRemaining) + " " + kony.i18n.getLocalizedString("keyMBPoints");
                    frmAccountStatementMB.lblexpval.text = callBackResponse["NewResult"][0]["StmtDt"] != undefined ? formatDateStmt(callBackResponse["NewResult"][0]["StmtDt"]) : "";
                    //pdfStmntDateMB = formatDateStmt(callBackResponse["NewResult"][0]["StmtDt"]);
                } else if (binlength == "" && creditype == "billed") {
                    frmAccountStatementMB.hbxbilled.setVisibility(true);
                    frmAccountStatementMB.lblfulpay.text = kony.i18n.getLocalizedString("keyMBFullPay");
                    frmAccountStatementMB.lblminpay.text = kony.i18n.getLocalizedString("keyMBMinimumPay");
                    frmAccountStatementMB.lbldue.text = kony.i18n.getLocalizedString("keyMBDueDate");
                    frmAccountStatementMB.lblpntsearned.text = kony.i18n.getLocalizedString("keyMBEarned");
                    frmAccountStatementMB.lbltotalpnts.text = kony.i18n.getLocalizedString("keyMBAvailable");
                    if (callBackResponse["NewResult"][0]["LedgerBal"] != undefined) {
                        frmAccountStatementMB.lblfulpayval.text = commaFormatted(callBackResponse["NewResult"][0]["LedgerBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    } else {
                        frmAccountStatementMB.lblfulpayval.text = "";
                    }
                    if (callBackResponse["NewResult"][0]["LastPmtAmt"] != undefined) {
                        frmAccountStatementMB.lblminpayval.text = commaFormatted(callBackResponse["NewResult"][0]["LastPmtAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    } else {
                        frmAccountStatementMB.lblminpayval.text = "";
                    }
                    frmAccountStatementMB.lbldueval.text = formatDateStmt(callBackResponse["NewResult"][0]["DueDt"]);
                    frmAccountStatementMB.lblpntsearnedval.text = commaFormattedPoints(callBackResponse["NewResult"][0]["BonusPoint"]) + " " + kony.i18n.getLocalizedString("keyMBPoints");
                    frmAccountStatementMB.lbltotalpntsval.text = commaFormattedPoints(callBackResponse["NewResult"][0]["BonusPointAvail"]) + " " + kony.i18n.getLocalizedString("keyMBPoints");
                    frmAccountStatementMB.lblexpval.text = formatDateStmt(callBackResponse["NewResult"][0]["StmtDt"]);
                }
                bindata = callBackResponse["BinData"];
                if (callBackResponse["BinLength"] != undefined) binlength = callBackResponse["BinLength"];
                else binlength = "";
                var temp = [];
                var endflag = callBackResponse["endFlag"].toString();
                if (endflag == "1") {
                    frmAccountStatementMB.hbxLink.setVisibility(false);
                } else frmAccountStatementMB.hbxLink.setVisibility(true);
                frmAccountStatementMB.hbxcreditheader.setVisibility(true);
                for (var i = 0; i < callBackResponse["NewResult"].length; i++) {
                    var segTable = {
                        seqid: i,
                        lblDate: formatDateStmt(callBackResponse["NewResult"][i]["TranDt"]),
                        lblDescription: checkCreditDesclength(callBackResponse["NewResult"][i]["StmtDesc"]),
                        lblAmount: commaFormattedStmt(callBackResponse["NewResult"][i]["TranAmt"]),
                        ishidden: "yes",
                        descexp: "",
                        descexpval: fieldemptyMB(callBackResponse["NewResult"][i]["StmtDesc"]),
                        channelexp: "",
                        channelexpval: ""
                    }
                    temp.push(segTable);
                }
                frmAccountStatementMB.segcredit.setData(temp); //43490 changed the function
                gblsegmentdataMB = frmAccountStatementMB.segcredit.data;
            } else {
                frmAccountStatementMB.hbxLink.setVisibility(false);
                alert(callBackResponse["errMsg"]);
                return;
            }
        } else {
            kony.application.dismissLoadingScreen();
            frmAccountStatementMB.hbxLink.setVisibility(false);
            if (callBackResponse["errMsg"] != undefined) {
                alert("" + callBackResponse["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("NoRecsFound"));
            }
        }
    } else {
        if (status == 300) {
            kony.application.dismissLoadingScreen();
            frmAccountStatementMB.hbxLink.setVisibility(false);
            alert("" + kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}
/**
 * 
 * Method to invoke service for LoanStatement
 */
function startLoanStatementservice(startdateStmt, enddateStmt) {
    showLoadingScreen();
    startDateStmt = startdateStmt;
    endDateStmt = enddateStmt;
    var startLoaninputParam = {};
    startLoaninputParam["sessionFlag"] = gblStmntSessionFlag;
    startLoaninputParam["AcctIdentValue"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    startLoaninputParam["FIIdent"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"];
    startLoaninputParam["StartDt"] = startdateStmt;
    startLoaninputParam["EndDt"] = enddateStmt;
    startLoaninputParam["BinData"] = bindata;
    startLoaninputParam["BinLength"] = binlength;
    startLoaninputParam["channelID"] = "MB";
    startLoaninputParam["accName"] = frmAccountStatementMB.lblname.text;
    startLoaninputParam["accType"] = gaccType;
    invokeServiceSecureAsync("LoanStatement", startLoaninputParam, startLoanStatementserviceCallback);
}
/**
 * Callback method for startLoanStatementservice()
 */
function startLoanStatementserviceCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            gblStmntSessionFlag = callBackResponse["sesFlag"];
            kony.application.dismissLoadingScreen();
            if (callBackResponse["endFlag"] == "1") frmAccountStatementMB.hbxLink.setVisibility(false);
            else frmAccountStatementMB.hbxLink.setVisibility(true);
            bindata = callBackResponse["BinData"];
            binlength = callBackResponse["BinLength"];
            var temp = [];
            if (callBackResponse["StatusCode"] == 0) {
                if (callBackResponse["NewResult"].length == 0) {
                    alert(kony.i18n.getLocalizedString("NoRecsFound"));
                    //alert(" " + keyalertMBNoRecord);
                    frmAccountStatementMB.segcredit.removeAll();
                    frmAccountStatementMB.hbxLink.setVisibility(false);
                    frmAccountStatementMB.hbxSegHeader.setVisibility(false);
                    return;
                }
                frmAccountStatementMB.hbxSegHeader.setVisibility(true);
                for (var i = 0; i < callBackResponse["NewResult"].length; i++) {
                    var segData = {
                        seqid: i,
                        lblDate: formatDateStmt(callBackResponse["NewResult"][i]["TranDt"]),
                        lblDescription: commaFormattedStmt(callBackResponse["NewResult"][i]["PaymentAmt"]),
                        lblAmount: commaFormattedStmt(callBackResponse["NewResult"][i]["Outbal"]),
                        ishidden: "yes",
                        descexp: "",
                        descexpval: commaFormattedStmt(callBackResponse["NewResult"][i]["Ppaid"]),
                        channelexp: "",
                        channelexpval: commaFormattedStmt(callBackResponse["NewResult"][i]["Ipaid"])
                    }
                    temp.push(segData);
                }
                frmAccountStatementMB.segcredit.addAll(temp);
                gblsegmentdataMB = frmAccountStatementMB.segcredit.data;
            } else {
                alert(callBackResponse["errMsg"]);
                return;
            }
        } else {
            kony.application.dismissLoadingScreen();
            if (callBackResponse["errMsg"] != undefined) {
                alert(" " + callBackResponse["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("NoRecsFound"));
            }
        }
    } else {
        if (status == 300) {
            kony.application.dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}
/**
 * Method to invoke service for CCstatement
 */
function startTDstatementservice(startdateStmt, enddateStmt) {
    showLoadingScreen();
    startDateStmt = startdateStmt;
    endDateStmt = enddateStmt;
    var TDinputParam = {};
    TDinputParam["sessionFlag"] = gblStmntSessionFlag;
    TDinputParam["AcctIdentValue"] = (gblAccountTable["custAcctRec"][gblIndex]["accId"]).substring(4);
    TDinputParam["FIIdent"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"];
    TDinputParam["BinData"] = bindata;
    TDinputParam["BinLength"] = binlength;
    TDinputParam["StartDt"] = startdateStmt;
    TDinputParam["EndDt"] = enddateStmt;
    TDinputParam["channelID"] = "MB";
    TDinputParam["accName"] = frmAccountStatementMB.lblname.text;
    TDinputParam["accType"] = gaccType;
    invokeServiceSecureAsync("TDStatement", TDinputParam, startTDstatementserviceCallback);
}
/**
 * Callback method for startCCstatementserviceCallback()
 */
function startTDstatementserviceCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            gblStmntSessionFlag = callBackResponse["sesFlag"];
            kony.application.dismissLoadingScreen();
            if (callBackResponse["endFlag"] == "1") frmAccountStatementMB.hbxLink.setVisibility(false);
            else frmAccountStatementMB.hbxLink.setVisibility(true);
            bindata = callBackResponse["BinData"];
            binlength = callBackResponse["BinLength"];
            var temp = [];
            if (callBackResponse["StatusCode"] == 0) {
                if (callBackResponse["NewResult"].length == 0) {
                    alert(kony.i18n.getLocalizedString("NoRecsFound"));
                    frmAccountStatementMB.segcredit.removeAll();
                    frmAccountStatementMB.hbxLink.setVisibility(false);
                    frmAccountStatementMB.hbxSegHeader.setVisibility(false);
                    return;
                }
                frmAccountStatementMB.hbxSegHeader.setVisibility(true);
                for (var i = 0; i < callBackResponse["NewResult"].length; i++) {
                    var segTable = {
                        seqid: i,
                        lblDate: formatDateStmt(callBackResponse["NewResult"][i]["TranDt"]),
                        lblDescription: checkDebitorCredit(commaFormattedStmt(callBackResponse["NewResult"][i]["DebitAmt"]), commaFormattedStmt(callBackResponse["NewResult"][i]["CreditAmt"])),
                        lblAmount: commaFormattedStmt(callBackResponse["NewResult"][i]["CurrentBalAmt"]),
                        ishidden: "yes",
                        descexp: "",
                        descexpval: callBackResponse["NewResult"][i]["TrnDesc1"] != undefined ? callBackResponse["NewResult"][i]["TrnDesc1"] : "-",
                        channelexp: "",
                        channelexpval: fieldemptyMB(callBackResponse["NewResult"][i]["TrnChannel"])
                    }
                    temp.push(segTable);
                }
                frmAccountStatementMB.segcredit.addAll(temp);
                gblsegmentdataMB = frmAccountStatementMB.segcredit.data;
            } else {
                alert(callBackResponse["errMsg"]);
                return;
            }
        } else {
            kony.application.dismissLoadingScreen();
            if (callBackResponse["errMsg"] != undefined) {
                alert(" " + callBackResponse["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("NoRecsFound"));
            }
        }
    } else {
        if (status == 300) {
            kony.application.dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}
/**
 * Method to invoke service for CCstatement
 */
function startCASAstatementservice(startdateStmt, enddateStmt) {
    showLoadingScreen();
    startDateStmt = startdateStmt;
    endDateStmt = enddateStmt;
    var spName;
    if (gaccType == "SDA") spName = "com.fnis.xes.ST";
    if (gaccType == "DDA") spName = "com.fnis.xes.IM";
    var CASAinputParam = {};
    CASAinputParam["sessionFlag"] = gblStmntSessionFlag;
    CASAinputParam["AcctIdentValue"] = gblAccountTable["custAcctRec"][gblIndex]["accId"]; //"0011001237";//
    CASAinputParam["FIIdent"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"]; //"0011000100010000";//
    CASAinputParam["BinData"] = bindata;
    CASAinputParam["BinLength"] = binlength;
    CASAinputParam["StartDt"] = startdateStmt; //"2012-04-01";//
    CASAinputParam["EndDt"] = endDateStmt; //"2012-05-01";//
    CASAinputParam["channelID"] = "MB";
    CASAinputParam["spName"] = spName;
    CASAinputParam["accName"] = frmAccountStatementMB.lblname.text;
    CASAinputParam["accType"] = gaccType;
    invokeServiceSecureAsync("AccntTransStmt", CASAinputParam, startCASAstatementserviceCallback);
}
/**
 * Callback method for startCCstatementserviceCallback()
 */
function startCASAstatementserviceCallback(status, callBackResponse) {
    //frmAccountStatementMB.segcredit.removeAll();
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            //activityLogServiceCall("047", "", "01", "", gblAccountTable["custAcctRec"][gblIndex]["accId"], frmAccountStatementMB.lblname.text, gaccType, "", "", "");
            gblStmntSessionFlag = callBackResponse["sesFlag"];
            kony.application.dismissLoadingScreen();
            if (callBackResponse["endFlag"] == "1") frmAccountStatementMB.hbxLink.setVisibility(false);
            else frmAccountStatementMB.hbxLink.setVisibility(true);
            bindata = callBackResponse["BinData"];
            binlength = callBackResponse["BinLength"];
            var temp = [];
            if (callBackResponse["StatusCode"] == 0) {
                if (callBackResponse["NewResult"].length == 0) {
                    alert(kony.i18n.getLocalizedString("NoRecsFound"));
                    //alert(" " + keyalertMBNoRecord);
                    frmAccountStatementMB.segcredit.removeAll();
                    frmAccountStatementMB.hbxLink.setVisibility(false);
                    frmAccountStatementMB.hbxSegHeader.setVisibility(false);
                    return;
                }
                frmAccountStatementMB.hbxSegHeader.setVisibility(true);
                for (var i = 0; i < callBackResponse["NewResult"].length; i++) {
                    var chqdata = "";
                    var description = "-";
                    if (callBackResponse["NewResult"][i]["ChkNumFormat"] != undefined) chqdata = callBackResponse["NewResult"][i]["ChkNumFormat"];
                    if (callBackResponse["NewResult"][i]["TrnDesc1"] != undefined) description = callBackResponse["NewResult"][i]["TrnDesc1"];
                    var segTable = {
                        seqid: i,
                        lblDate: formatDateStmt(callBackResponse["NewResult"][i]["PostedDt"]),
                        lblDescription: checkTranstype(commaFormattedStmt(callBackResponse["NewResult"][i]["Amount"]), callBackResponse["NewResult"][i]["TrnType"]),
                        lblAmount: commaFormattedStmt(callBackResponse["NewResult"][i]["Balance"]),
                        ishidden: "yes",
                        descexp: "",
                        descexpval: description + chqdata,
                        channelexp: "",
                        channelexpval: fieldemptyMB(callBackResponse["NewResult"][i]["TrnChannel"])
                    }
                    temp.push(segTable);
                }
                frmAccountStatementMB.segcredit.addAll(temp);
                gblsegmentdataMB = frmAccountStatementMB.segcredit.data;
            } else {
                alert(callBackResponse["errMsg"]);
                return;
            }
        } else {
            kony.application.dismissLoadingScreen();
            if (callBackResponse["errMsg"] != undefined) {
                alert(" " + callBackResponse["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("NoRecsFound"));
            }
        }
    } else {
        if (status == 300) {
            kony.application.dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function checkTranstype(data, type) {
    if (data == "" || data == null) return "-";
    else {
        if (data < 0) return data;
        else if (type == "Credit") {
            data = "+" + data;
            return data;
        } else if (type == "Debit") {
            data = "-" + data;
            return data;
        }
    }
}

function formatDateStmt(dateStr) {
    if (dateStr == "" || dateStr == null) return "-";
    else {
        dstr1 = dateStr.substring(0, 4);
        dstr2 = dateStr.substring(5, 7);
        dstr3 = dateStr.substring(8, 10);
        return dstr3 + "/" + dstr2 + "/" + dstr1;
    }
}

function commaFormattedStmt(amount) {
    var minus = '';
    if (amount == "" || amount == null) return "-";
    else {
        amount = amount.concat(".00")
        if (parseFloat(amount) < 0) {
            minus = '-';
        }
        var delimiter = ","; // replace comma if desired
        amount = new String(amount);
        var a = amount.split('.', 2)
        var d = a[1];
        var i = parseInt(a[0]);
        if (isNaN(i)) {
            return '';
        }
        i = Math.abs(i);
        var n = new String(i);
        var a = [];
        while (n.length > 3) {
            var nn = n.substr(n.length - 3);
            a.unshift(nn);
            n = n.substr(0, n.length - 3);
        }
        if (n.length > 0) {
            a.unshift(n);
        }
        n = a.join(delimiter);
        if (d.length < 1) {
            amount = n;
        } else {
            amount = n + '.' + d;
        }
        amount = minus + amount;
        return amount;
    }
}

function selectedMonthData() {
    if (gaccType == "SDA" || gaccType == "DDA") {
        startCASAstatementservice(startDateStmt, endDateStmt);
    } else if (gaccType == "LOC") {
        startLoanStatementservice(startDateStmt, endDateStmt);
    } else if (gaccType == "CDA") {
        startTDstatementservice(startDateStmt, endDateStmt);
    }
}

function loadMoreData() {
    //frmAccountStatementMB.segcredit.removeAll();
    var segmentdataMB = resetSegTemplateToCompress();
    frmAccountStatementMB.segcredit.setData(segmentdataMB);
    if (gaccType == "SDA" || gaccType == "DDA") {
        startCASAstatementservice(startDateStmt, endDateStmt);
    } else if (gaccType == "LOC") {
        startLoanStatementservice(startDateStmt, endDateStmt);
    } else if (gaccType == "CCA") {
        startCCstatementservice(creditype, wcode);
    } else if (gaccType == "CDA") {
        startTDstatementservice(startDateStmt, endDateStmt);
    }
}
/** get date in the required format from button text dynamically
 **/
function getDateFormatStmt(btntext, btnno) {
    var monthMB;
    var montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"),
        kony.i18n.getLocalizedString("keyCalendarApr"), kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
        kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"), kony.i18n.getLocalizedString("keyCalendarSep"),
        kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")
    ];
    var month = btntext.split(" ");
    for (i = 0; i < montharray.length; i++) {
        if (month[0] == montharray[i]) monthMB = i;
    }
    var yr = parseInt(month[1]); //+1;
    var date = new Date();
    date.setFullYear(yeararray[btnno], monthMB, 1);
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    startDateStmt = dateFormatChange(firstDay);
    endDateStmt = dateFormatChange(lastDay);
    if (btnno == 5) {
        endDateStmt = todaysDate; //getTodaysDateStmt();
    }
    selectedMonthData();
}
/**
 * fieldempty
 */
function fieldemptyMB(field) {
    var retField;
    if (field == "" || field == null) retField = "-";
    else retField = field;
    return retField;
}

function checkDebitorCredit(debit, credit) {
    if ((debit == "" || debit == null) && (credit == "" || credit == null)) return "-";
    else {
        if (debit != 0) return "-" + debit;
        else if (credit != 0) return "+" + credit;
        else if (credit < 0) return credit;
        else if (debit < 0) return debit;
    }
}

function checkCreditDesclength(creditdesc) {
    if (creditdesc == "" || creditdesc == null) return "-";
    else {
        if (creditdesc.length < 10) return creditdesc;
        else {
            var subdesc = creditdesc.substring(0, 10);
            var desc = subdesc + "...";
            return desc;
        }
    }
}

function clearCCValues() {
    frmAccountStatementMB.lblfulpayval.text = "";
    frmAccountStatementMB.lblminpayval.text = "";
    frmAccountStatementMB.lbldueval.text = "";
    frmAccountStatementMB.lblpntsearnedval.text = "";
    frmAccountStatementMB.lbltotalpntsval.text = "";
    frmAccountStatementMB.lbllimitval.text = "";
    frmAccountStatementMB.lbloutbalval.text = "";
    frmAccountStatementMB.lblcycleval.text = "";
    frmAccountStatementMB.lblpointsval.text = "";
    frmAccountStatementMB.lblexpireval.text = "";
    frmAccountStatementMB.lblexpval.text = "";
}

function getTodaysDateStmt() {
    var day = new Date();
    var dd = day.getDate();
    var mm = day.getMonth() + 1; //January is 0!
    var yyyy = day.getFullYear();
    if (dd < 10) dd = '0' + dd
    if (mm < 10) mm = '0' + mm
    day = yyyy + "-" + mm + "-" + dd
    return day;
}

function frmAccountStatementMBPreShow() {
    changeStatusBarColor();
    //if(!(LocaleController.isFormUpdatedWithLocale(frmAccountStatementMB.id))){
    if (frmAccountStatementMB.hbxOnHandClick.isVisible) {
        frmAccountStatementMB.hbxOnHandClick.isVisible = false;
    }
    frmAccountStatementMB.lblHead.text = kony.i18n.getLocalizedString("FullStat");
    frmAccountStatementMB.linkactivity.text = kony.i18n.getLocalizedString("MyActivities");
    if (flowSpa) {
        frmAccountStatementMB.btnBackSpa.text = kony.i18n.getLocalizedString("keyBackSpa");
    } else {
        frmAccountStatementMB.btnBack.text = kony.i18n.getLocalizedString("Back");
    }
    frmAccountStatementMB.btnunbilled.text = kony.i18n.getLocalizedString("keyMBUnbilled");
    frmAccountStatementMB.btnbilled.text = kony.i18n.getLocalizedString("keyMBBilled");
    frmAccountStatementMB.lblDateHeader.text = kony.i18n.getLocalizedString("keyMBTransactionDate");
    frmAccountStatementMB.lblHeaderTransaction.text = kony.i18n.getLocalizedString("keyMBDescription");
    frmAccountStatementMB.lblHeaderBalance.text = kony.i18n.getLocalizedString("keyAmount");
    frmAccountStatementMB.lblselect.text = kony.i18n.getLocalizedString("keyMBSelectMonth");
    if (gaccType == "SDA" || gaccType == "DDA") {
        frmAccountStatementMB.lblDateHeader.text = kony.i18n.getLocalizedString("keyDate"); //"Date";
        frmAccountStatementMB.lblHeaderTransaction.text = kony.i18n.getLocalizedString("keyAmount"); //"Amount";
        frmAccountStatementMB.lblHeaderBalance.text = kony.i18n.getLocalizedString("Balance"); //"Balance";
    } else if (gaccType == "LOC") {
        frmAccountStatementMB.lblDateHeader.text = kony.i18n.getLocalizedString("keyDate"); //"Date";
        frmAccountStatementMB.lblHeaderTransaction.text = kony.i18n.getLocalizedString("keyMBPaymentAmount"); //"Payment Amount";
        frmAccountStatementMB.lblHeaderBalance.text = kony.i18n.getLocalizedString("keyMBOutstandingstmt"); //"Outstanding Balance";
    } else if (gaccType == "CCA") {
        frmAccountStatementMB.lbltransdate.text = kony.i18n.getLocalizedString("keyMBTransactionDate"); //"Transaction Date";
        frmAccountStatementMB.lbldescheader.text = kony.i18n.getLocalizedString("keyMBDescription"); //"Description ";
        frmAccountStatementMB.lblbalHeader.text = kony.i18n.getLocalizedString("keyAmount"); //"Amount";
        frmAccountStatementMB.label589578196378807.text = kony.i18n.getLocalizedString("keySelectStmntCycle"); //added for 16833
        frmAccountStatementMB.linkMore.text = kony.i18n.getLocalizedString("More"); //added for 16833
        frmAccountStatementMB.lbllimit.text = kony.i18n.getLocalizedString("keyMBCreditlimit");
        frmAccountStatementMB.lbloutbal.text = kony.i18n.getLocalizedString("keyMBOutstanding");
        frmAccountStatementMB.lblcycle.text = kony.i18n.getLocalizedString("keyMBSpendcycle");
        frmAccountStatementMB.lblpoints.text = kony.i18n.getLocalizedString("keyMBAvailable");
        frmAccountStatementMB.lblexpire.text = kony.i18n.getLocalizedString("keyMBexpiring");
        frmAccountStatementMB.lblexp.text = kony.i18n.getLocalizedString("keyMBPointexpiry");
        frmAccountStatementMB.lblfulpay.text = kony.i18n.getLocalizedString("keyMBFullPay");
        frmAccountStatementMB.lblminpay.text = kony.i18n.getLocalizedString("keyMBMinimumPay");
        frmAccountStatementMB.lbldue.text = kony.i18n.getLocalizedString("keyMBDueDate");
        frmAccountStatementMB.lblpntsearned.text = kony.i18n.getLocalizedString("keyMBEarned");
        frmAccountStatementMB.lbltotalpnts.text = kony.i18n.getLocalizedString("keyMBAvailable");
        frmAccountStatementMB.lblpointsval.text = frmAccountStatementMB.lblpointsval.text.split(" ")[0] + " " + kony.i18n.getLocalizedString("keyMBPoints");
        frmAccountStatementMB.lblexpireval.text = frmAccountStatementMB.lblexpireval.text.split(" ")[0] + " " + kony.i18n.getLocalizedString("keyMBPoints");
        frmAccountStatementMB.lblpntsearnedval.text = frmAccountStatementMB.lblpntsearnedval.text.split(" ")[0] + " " + kony.i18n.getLocalizedString("keyMBPoints");
        frmAccountStatementMB.lbltotalpntsval.text = frmAccountStatementMB.lbltotalpntsval.text.split(" ")[0] + " " + kony.i18n.getLocalizedString("keyMBPoints");
    } else if (gaccType == "CDA") {
        //frmAccountStatementMB.segcredit.rowTemplate = hbxaccntstmt;		
        frmAccountStatementMB.lblDateHeader.text = kony.i18n.getLocalizedString("keyDate");
        frmAccountStatementMB.lblHeaderTransaction.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount"); //"Amount";
        frmAccountStatementMB.lblHeaderBalance.text = kony.i18n.getLocalizedString("Balance");
    }
    //LocaleController.updatedForm(frmAccountStatementMB.id);
    //  }
    frmAccountStatementMB.btnOptions.skin = "btnShare";
    frmAccountStatementMB.imgHeaderMiddle.src = "arrowtop.png";
    frmAccountStatementMB.imgHeaderRight.src = "empty.png";
    if (gaccType == "CDA") {
        frmAccountStatementMB.segcredit.rowTemplate = hbxaccntstmt;
    }
}
/*
function loadSortedData(temp) {
	columnsortby = "lblDate";
	sequence= "seqid";
	if(sortOrderStmtMB == "descending"){
		var sorteddata = sortbydateMBDescend(temp,columnsortby,sequence);
 	}
 	else if(sortOrderStmtMB == "acsending"){
		var sorteddata = sortbydateMBAscend(temp,columnsortby,sequence);
	}
return sorteddata;
}*/
/**
 * reset SortBtn Skin
 */
function setSortBtnSkinMB() {
    gblStmntSessionFlag = "2";
    noOfStmtServiceCalls = 0;
    bindata = "";
    binlength = "";
    frmAccountStatementMB.segcredit.removeAll();
    /*sortOrderStmt = "descending";
    if(gaccType == "CCA"){
    	frmAccountStatementMB.btnsortCC.skin = "btnsortwhite";
    	frmAccountStatementMB.btnsortCC.focusSkin = "btnsortwhite";
    }
    else{	
    	frmAccountStatementMB.btnsort.skin = "btnsortwhite";
    	frmAccountStatementMB.btnsort.focusSkin = "btnsortwhite";
    }*/
}
/**
 * on CCMonth1 Click
 */
function onCCMonth1Click() {
    setSortBtnSkinMB();
    clearCCValues();
    frmAccountStatementMB.btn1.skin = "btnTabLeftBlue";
    frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
    startCCstatementservice("billed", "1");
}
/**
 * on CCMonth2 Click
 */
function onCCMonth2Click() {
    setSortBtnSkinMB();
    clearCCValues();
    frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btn2.skin = "btnTabMiddleBlue";
    frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
    startCCstatementservice("billed", "2");
}
/**
 * on CCMonth3 Click
 */
function onCCMonth3Click() {
    setSortBtnSkinMB();
    clearCCValues();
    frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn3.skin = "btnTabMiddleBlue";
    frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
    startCCstatementservice("billed", "3");
}
/**
 * on CCMonth4 Click
 */
function onCCMonth4Click() {
    setSortBtnSkinMB();
    clearCCValues();
    frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn4.skin = "btnTabMiddleBlue";
    frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
    startCCstatementservice("billed", "4");
}
/**
 * on CCMonth5 Click
 */
function onCCMonth5Click() {
    setSortBtnSkinMB();
    clearCCValues();
    frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn5.skin = "btnTabMiddleBlue";
    frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
    startCCstatementservice("billed", "5");
}
/**
 * on CCMonth6 Click
 */
function onCCMonth6Click() {
    setSortBtnSkinMB();
    clearCCValues();
    frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btn6.skin = "btnTabRightBlue";
    startCCstatementservice("billed", "6");
}
/**
 * on Month1 Click
 */
function onMonth1Click() {
    setSortBtnSkinMB();
    frmAccountStatementMB.btnm1.skin = "btnTabLeftBlue";
    frmAccountStatementMB.btnm2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
}
/**
 * on Month2 Click
 */
function onMonth2Click() {
    setSortBtnSkinMB();
    frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btnm2.skin = "btnTabMiddleBlue";
    frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
}
/**
 * on Month3 Click
 */
function onMonth3Click() {
    setSortBtnSkinMB();
    frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btnm2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm3.skin = "btnTabMiddleBlue";
    frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
}
/**
 * on Month4 Click
 */
function onMonth4Click() {
    setSortBtnSkinMB();
    frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btnm2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm4.skin = "btnTabMiddleBlue";
    frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
}
/**
 * on Month5 Click
 */
function onMonth5Click() {
    setSortBtnSkinMB();
    frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btnm2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm5.skin = "btnTabMiddleBlue";
    frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
}
/**
 * on Month6 Click
 */
function onMonth6Click() {
    setSortBtnSkinMB();
    frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
    frmAccountStatementMB.btnm2.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
    frmAccountStatementMB.btnm6.skin = "btnTabRightBlue";
}

function resetSegTemplateToCompress() {
    var segDataStmt = [];
    if (gaccType == "CCA") {
        for (var i = 0; i < gblsegmentdataMB.length; i++) {
            var selectedData = {
                lblDate: gblsegmentdataMB[i]["lblDate"],
                lblAmount: gblsegmentdataMB[i]["lblAmount"],
                lblDescription: gblsegmentdataMB[i]["lblDescription"],
                ishidden: "yes",
                descexpval: gblsegmentdataMB[i]["descexpval"],
                template: hbxaccntstmtCC
            }
            segDataStmt.push(selectedData);
        }
    } else {
        for (var i = 0; i < gblsegmentdataMB.length; i++) {
            var selectedData = {
                lblDate: gblsegmentdataMB[i]["lblDate"],
                lblAmount: gblsegmentdataMB[i]["lblAmount"],
                lblDescription: gblsegmentdataMB[i]["lblDescription"],
                ishidden: "yes",
                descexpval: gblsegmentdataMB[i]["descexpval"],
                channelexpval: gblsegmentdataMB[i]["channelexpval"],
                template: hbxaccntstmt
            }
            segDataStmt.push(selectedData);
        }
    }
    return segDataStmt;
}

function savePDFAccountHistoryMB(fileType) {
    var inputParam = {};
    var suffix = "";
    var monthlyInstallment = "";
    var paymentDueDate = "";
    var interestRate = "";
    var accountNum = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    inputParam["filetype"] = fileType;
    inputParam["accType"] = gaccType;
    //alert(gaccType);
    var accountType = "";
    var d1 = dateFormatChange(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()));
    var credittype = "";
    if (creditype == "unbilled2" || creditype == "unbilled1") {
        creditType = "Unbilled";
    } else {
        creditType = "Billed";
    }
    if (gaccType == "LOC") {
        accountNum = accountNum.substring(1, 11);
        if (gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"] == "ICON-05") {
            if (gaccType == "LOC") { // condition added due to DEF578 defect
                //alert("in iffffff");
                suffix = frmAccountDetailsMB.lblUsefulValue3.text;
            } else {
                suffix = frmAccountDetailsMB.lblUsefulValue4.text;
            }
            monthlyInstallment = frmAccountDetailsMB.lblUsefulValue7.text + "" + kony.i18n.getLocalizedString("currencyThaiBaht");
            paymentDueDate = frmAccountDetailsMB.lblUsefulValue6.text;
            interestRate = frmAccountDetailsMB.segMaturityDisplay.data[0].lblTransaction;
        } else {
            //alert("in elseee"+frmAccountDetailsMB.lblUsefulValue3.text);
            suffix = frmAccountDetailsMB.lblUsefulValue3.text;
            monthlyInstallment = frmAccountDetailsMB.lblUsefulValue6.text;
            paymentDueDate = frmAccountDetailsMB.lblUsefulValue5.text;
        }
    }
    if (kony.i18n.getCurrentLocale() == "th_TH") custName = gblCustomerNameTh;
    else custName = gblCustomerName;
    var totalData = {
        "billed": wcode,
        "accountId": formatAccountNumber(accountNum),
        "printedDate": d1.split("-", 3)[2] + "/" + d1.split("-", 3)[1] + "/" + d1.split("-", 3)[0],
        "creditType": creditType,
        "startDate": startDateStmt.split("-", 3)[2] + "/" + startDateStmt.split("-", 3)[1] + "/" + startDateStmt.split("-", 3)[0],
        "endDate": endDateStmt.split("-", 3)[2] + "/" + endDateStmt.split("-", 3)[1] + "/" + endDateStmt.split("-", 3)[0],
        "localeId": kony.i18n.getCurrentLocale(),
        "accountType": custName,
        "suffixNo": suffix,
        "monthlyInstallment": monthlyInstallment,
        "paymentDueOn": paymentDueDate,
        "remBal": commaFormatted(gblAccountTable["custAcctRec"][gblIndex]["availableBal"]) + "" + kony.i18n.getLocalizedString("currencyThaiBaht"),
        "cardHolder": frmAccountDetailsMB.lblUsefulValue3.text,
        "creditCardNumber": frmAccountDetailsMB.lblUsefulValue2.text,
        "remainingCreditlimit": frmAccountStatementMB.lbllimitval.text,
        "SpendingBillingCycle": frmAccountStatementMB.lblcycleval.text,
        "oustandingBal": frmAccountStatementMB.lbloutbalval.text,
        "availableRewardPoints": frmAccountStatementMB.lblpointsval.text,
        "pointsExpiring": frmAccountStatementMB.lblexpireval.text,
        "pointsExpiryDate": frmAccountStatementMB.lblexpval.text,
        "fullPaymentAmnt": frmAccountStatementMB.lblfulpayval.text,
        "minPaymentAmnt": frmAccountStatementMB.lblminpayval.text,
        "paymentDueDate": frmAccountStatementMB.lbldueval.text,
        "totalBilledPointsEarned": frmAccountStatementMB.lblpntsearnedval.text,
        "totalBilledPoints": frmAccountStatementMB.lbltotalpntsval.text,
        "interestRate": interestRate,
        "ICON_ID": gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"],
        "amountDue": "",
        "stmtDate": frmAccountStatementMB.lblexpval.text,
        "totalAmountDue": ""
    };
    inputParam["outputtemplatename"] = gaccType + "Statement_" + d1.split("-", 3)[2] + "" + d1.split("-", 3)[1] + "" + d1.split("-", 3)[0];
    inputParam["datajson"] = JSON.stringify(totalData, "");
    if (gblDeviceInfo.name == "thinclient") {
        inputParam["channelId"] = GLOBAL_IB_CHANNEL;
    } else {
        inputParam["channelId"] = GLOBAL_MB_CHANNEL;
    }
    inputParam["accName"] = frmAccountStatementMB.lblname.text;
    var url = "";
    invokeServiceSecureAsync("generatePdfImageForActStmts", inputParam, mbRenderFileCallbackfunction);
}
function segBPSuggestedBillsListOnClick() {
    showLoadingScreenPopup();
    gblBillerPresentInMyBills = false;
    gblFirstTimeBillPayment = false;
    gblFullPayment = false;
    gblBillPaymentEdit = false;
    gblFromAccountSummary = false;
    
    IBBillsCurrentSystemDate();
    clearIBBillPaymentLP();
    showSchedBillerNicknameIB();
    frmIBBillPaymentLP.lblFullPayment.setVisibility(false);
    frmIBBillPaymentLP.lblBPAmount.containerWeight = 20;
    frmIBBillPaymentLP.lblBPAmount.setVisibility(true);
    frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
    frmIBBillPaymentLP.lblTHB.setVisibility(true);
    frmIBBillPaymentLP.imgBPBillerImage.setVisibility(true);
    fn_resetIBMEABillerDetails();
    gblBillerMethod = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["BillerMethod"];;
    gblToAccountKey = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["ToAccountKey"];
    gblToAccountKeyUnAltered = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["ToAccountKey"];
    gblBillerId = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["BillerID"];
    gblPayFull = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["IsFullPayment"];

    //ENh 113
    gblbillerGroupType = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["BillerGroupType"];

    gblreccuringDisableAdd = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["IsRequiredRefNumber2Add"];
    gblreccuringDisablePay = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["IsRequiredRefNumber2Pay"];
    
    gblBillerBancassurance = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["billerBancassurance"];
    gblAllowRef1AlphaNum = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["allowRef1AlphaNum"];
        
    //var billerGroupType = 0;
    if (!frmIBBillPaymentLP.hbxRef1.isVisible) {
        frmIBBillPaymentLP.hbxRef1.setVisibility(true);
    }

    if (gblreccuringDisablePay == "Y") {
        frmIBBillPaymentLP.hbxReference2.setVisibility(true);
        frmIBBillPaymentLP.lblRefVal2.setEnabled(true);
    } else {
        frmIBBillPaymentLP.hbxReference2.setVisibility(false);
    }
    gblCompCode = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["BillerCompCode"];
    frmIBBillPaymentLP.imgBPBillerImage.src = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["imgSgstdBiller"].src;
    frmIBBillPaymentLP.lblBPRef1Val.text = "";
    frmIBBillPaymentLP.lblBPRef1ValMasked.text = "";
    frmIBBillPaymentLP.lblBPRef1Val.maxTextLength = parseInt(frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref1Len"]);
    frmIBBillPaymentLP.lblBPRef1.text = appendColon(frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref1Label"]);
    frmIBBillPaymentLP.lblBPBillerName.text = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["lblSgstdBillerName"];
    gblRef1LblEN = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref1EN"];
    gblRef1LblTH = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref1TH"];
    gblRef2LblEN = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref2EN"];
    gblRef2LblTH = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref2TH"];
    gblBillerCompCodeEN = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["lblSgstdBillerNameEN"];
    gblBillerCompCodeTH = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["lblSgstdBillerNameTH"];
    
    frmIBBillPaymentLP.lblRefVal2.text = "";
    frmIBBillPaymentLP.lblRefVal2.maxTextLength = parseInt(frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref2Len"]);
    frmIBBillPaymentLP.lblRef2.text = appendColon(frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref2Label"]);
    frmIBBillPaymentLP.lblToCompCode.text = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["lblSgstdBillerName"];
    //frmIBBillPaymentLP.lblRefVal2.setEnabled(true);
	if(gblCompCode=="2533"){
		gblSegBillerData=frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0];
   		// var tmpIsRef2Req = "";
	    gBillerStartTime= frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["billerStartTime"];
		gBillerEndTime= frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["billerEndTime"];
		gblbillerTransType = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["billerTransType"];
		gblbillerServiceType = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["billerServiceType"];
		gblMeaFeeAmount=frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["billerFeeAmount"];
		
		frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
        frmIBBillPaymentLP.txtBillAmt.setVisibility(false);
		frmIBBillPaymentLP.lblTHB.setVisibility(false); 
	}else{
		frmIBBillPaymentLP.btnBPPayOn.setEnabled(true);
	}
	
	frmIBBillPaymentLP.hbxToBiller.setVisibility(false);
    frmIBBillPaymentLP.hbxImage.setVisibility(true);
	dismissLoadingScreenPopup();
    //frmIBBillPaymentLP.show();	
}



function onFocusLostOfRef1TextBoxIB() {
	
	if (gblreccuringDisablePay == "Y") {
		if(isNotBlank(frmIBBillPaymentLP.lblBPRef1Val.text) && isNotBlank(frmIBBillPaymentLP.lblRefVal2.text)) {
			verifyBillerMethodFromLandingIB();
		}
	} else {
		if(isNotBlank(frmIBBillPaymentLP.lblBPRef1Val.text)) {
			verifyBillerMethodFromLandingIB();
		}
	}
}

function onFocusLostOfRef2TextBoxIB() { 
	if(isNotBlank(frmIBBillPaymentLP.lblBPRef1Val.text) && isNotBlank(frmIBBillPaymentLP.lblRefVal2.text)) {
		verifyBillerMethodFromLandingIB();
	}
}


function verifyBillerMethodFromLandingIB() {

		
	var billerNickName = getBillerNickNameFromMyBills(gblMyBillList, gblCompCode, frmIBBillPaymentLP.lblBPRef1Val.text, frmIBBillPaymentLP.lblRefVal2.text);
	if(isNotBlank(billerNickName)) {
		frmIBBillPaymentLP.lblToCompCode.text = billerNickName;
	} else {
		var locale = kony.i18n.getCurrentLocale();
		if(locale == "en_US"){
			frmIBBillPaymentLP.lblToCompCode.text = gblBillerCompCodeEN;
		} else {
			frmIBBillPaymentLP.lblToCompCode.text = gblBillerCompCodeTH;
		}
	}
	
	showSchedBillerNicknameIB();
	
	if (gblBillerMethod == 0) { //Offline Biller
        mapOfflineBiller();

    } else if (gblBillerMethod == 1) { //Online Biller
        mapOnlineBiller();

    } else if (gblBillerMethod == 2) { // TMB Credit Card/Ready Cash
        mapTMBCreditCard();

    } else if (gblBillerMethod == 3) { //TMB Loan
        mapTMBLoan();

    } else if (gblBillerMethod == 4) { //Ready Cash		
        mapReadyCash();
    }
    
}

function onTextChangeRef1Ref2IB(){
	
	frmIBBillPaymentLP.txtScheduleNickname.text = "";
	gblFullPayment = false;
	if(gblBillerMethod != 0) {
		frmIBBillPaymentLP.lblFullPayment.setVisibility(false)
	    frmIBBillPaymentLP.lblBPAmount.setVisibility(true);
	    frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
	    frmIBBillPaymentLP.lblTHB.setVisibility(true);
	    frmIBBillPaymentLP.lblFullPayment.text = "0.00 "+kony.i18n.getLocalizedString("currencyThaiBaht");
		if(gblCompCode=="2533"){
			fn_resetIBMEABillerDetails();
			frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
	        frmIBBillPaymentLP.txtBillAmt.setVisibility(false);
			frmIBBillPaymentLP.lblTHB.setVisibility(false); 
		}
	}
}


function clearIBBillPaymentLPOnRef1Ref2Done() {
	
	if(gblBillPaymentEdit){
		gblBillPaymentEdit = false;
		if(frmIBBillPaymentLP.lblFullPayment.isVisible) {
			gblFullPayment = true;
		}else{
			gblFullPayment = false;
		}
	} else {
		gblFullPayment=true;
	    //below line is added for CR - PCI-DSS masked Credit card no
	    frmIBBillPaymentLP.hbxMinMax.setVisibility(false);
	    frmIBBillPaymentLP.txtBillAmt.text = "";
	    frmIBBillPaymentLP.lblPenaltyVal.text = "";
	    frmIBBillPaymentLP.txtBillAmt.placeholder = "0.00";
	    frmIBBillPaymentLP.lblFullPayment.text = "0.00 "+kony.i18n.getLocalizedString("currencyThaiBaht");
  }  
    
}

function searchMyBillsAndSuggestedBillersIB() {

	var searchMyBillList = [];
	var searchSuggestedBillList = [];
	var searchText = frmIBBillPaymentLP.txtBPSearch.text;
	var searchValue = ""; 
	
	//MyBills Search
	if(MySelectBillListRs != null && MySelectBillListRs.length > 0) { // Atleast one biller should be avilable
		if (isNotBlank(searchText) && searchText.length >= 3) { //Atleast 3 characters should be entered in search box
			
			searchText = searchText.toLowerCase();
			
			for (j = 0, i = 0; i < MySelectBillListRs.length; i++) {
				
				searchValue = MySelectBillListRs[i].lblBillsName.toLowerCase() 
								+ "~" + MySelectBillListRs[i].lblBillCompCodeEN.toLowerCase()
								+ "~" + MySelectBillListRs[i].lblBillCompCodeTH.toLowerCase() 
								+ "~" + MySelectBillListRs[i].BillerCompCode.toLowerCase() 
								+ "~" + MySelectBillListRs[i].lblRef1;
				
				if (searchValue.indexOf(searchText) > -1) {
					if(frmIBBillPaymentLP.cbxBillCategory.selectedKey == "0" || MySelectBillListRs[i].BillerCategoryID == frmIBBillPaymentLP.cbxBillCategory.selectedKey) {
						searchMyBillList[j] = MySelectBillListRs[i];
						j++;
					}
				}
			}//For Loop End

		} else { //searchText condition End
			// Search text is less than 3 characters hence show all My Bills
			if(frmIBBillPaymentLP.cbxBillCategory.selectedKey == "0") {
				searchMyBillList = MySelectBillListRs;
			} else {
				for (j = 0, i = 0; i < MySelectBillListRs.length; i++) {
					if(MySelectBillListRs[i].BillerCategoryID == frmIBBillPaymentLP.cbxBillCategory.selectedKey) {
						searchMyBillList[j] = MySelectBillListRs[i];
						j++;
					}
				}
			}
		}
	}
	frmIBBillPaymentLP.segBPBillsList.setData(searchMyBillList);
	//Suggested Biller Search 
	if(MySelectBillSuggestListRs != null && MySelectBillSuggestListRs.length > 0) { // Atleast one biller should be avilable
		if (isNotBlank(searchText) && searchText.length >= 3) { //Atleast 3 characters should be entered in search box
			
			searchText = searchText.toLowerCase();
			
			for (j = 0, i = 0; i < MySelectBillSuggestListRs.length; i++) {
				
				var searchValue = MySelectBillSuggestListRs[i].lblSgstdBillerNameEN.toLowerCase()
									+ "~" + MySelectBillSuggestListRs[i].lblSgstdBillerNameTH.toLowerCase() 
									+ "~" + MySelectBillSuggestListRs[i].BillerCompCode.toLowerCase();
				
				if (searchValue.indexOf(searchText) > -1) {
					if(frmIBBillPaymentLP.cbxBillCategory.selectedKey == "0" || MySelectBillSuggestListRs[i].BillerCategoryID.text == frmIBBillPaymentLP.cbxBillCategory.selectedKey) {
						searchSuggestedBillList[j] = MySelectBillSuggestListRs[i];
						j++;
					}
				}
			}//For Loop End

		} else { //searchText condition End
			// Search text is less than 3 characters hence show all My Bills
			if(frmIBBillPaymentLP.cbxBillCategory.selectedKey == "0") {
				searchSuggestedBillList = MySelectBillSuggestListRs;
			} else {
				for (j = 0, i = 0; i < MySelectBillSuggestListRs.length; i++) {
					if(MySelectBillSuggestListRs[i].BillerCategoryID.text == frmIBBillPaymentLP.cbxBillCategory.selectedKey) {
						searchSuggestedBillList[j] = MySelectBillSuggestListRs[i];
						j++;
					}
				}
			}
		}
	}
	
	frmIBBillPaymentLP.segBPSgstdBillerList.setData(searchSuggestedBillList);
	
	if(searchMyBillList.length == 0 && searchSuggestedBillList.length == 0){
		//frmSelectBiller.segMyBills.setVisibility(false);
		frmIBBillPaymentLP.lblMsg.setVisibility(true);
		frmIBBillPaymentLP.lblMsg.text = kony.i18n.getLocalizedString("keybillernotfound");
	}else{
		frmIBBillPaymentLP.lblMsg.setVisibility(false);
	}
	if(searchMyBillList.length == 0){
		frmIBBillPaymentLP.lblBPBillsList.setVisibility(false);
	}else{
		frmIBBillPaymentLP.lblBPBillsList.setVisibility(true);
	}
	if(searchSuggestedBillList.length == 0){
		frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(false);
	}else{
		frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
	}
}

function showSchedBillerNicknameIB() {
	if(gblPaynow || gblBillerPresentInMyBills) {
		frmIBBillPaymentLP.txtScheduleNickname.text = "";
		frmIBBillPaymentLP.hbxScheduleNickName.setVisibility(false);
		frmIBBillPaymentLP.hbxAddBillsBeforeSchedule.setVisibility(false);
	} else {
		frmIBBillPaymentLP.hbxScheduleNickName.setVisibility(true);
		frmIBBillPaymentLP.hbxAddBillsBeforeSchedule.setVisibility(true);
	}
}

function frmAccountDetailsMBMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        //frmAccountDetailsMB.scrollboxMain.scrollToEnd();
        gblPreviousForm = "frmAccountDetailsMB";
        frmAccountDetailsMB.flxCardImage.width = "90%";
        frmAccountDetailsMB.flxCardImage.height = "41%";
        frmAccountDetailsMB.imgcard.width = "95%";
        frmAccountDetailsMB.imgcard.height = "100%";
        frmAccountDetailsMB.lblCardNumber.skin = getCardNoSkin();
        frmAccountDetailsMB.lblCardNumber.top = "53%";
        frmAccountDetailsMB.lblCardNumber.left = "10%";
        frmAccountDetailsMB.lblCardNumber.width = "80%";
        frmAccountDetailsMB.lblCardNumber.height = "15%";
        frmAccountDetailsMB.lblCardAccountName.top = "67%";
        frmAccountDetailsMB.lblCardAccountName.left = "10%";
        frmAccountDetailsMB.lblCardAccountName.width = "80%";
        frmAccountDetailsMB.lblCardAccountName.height = "15%";
        //frmAccountDetailsMB.flxCardImage.height = "37.75%";
        //frmAccountDetailsMB.flxCardImage.width = "83%";
        //frmAccountDetailsMB.lblRibbon.top="16%";
        frmAccountDetailsMB.lblCardAccountName.top = "73%";
        frmAccountDetailsMB.lblCardNumber.top = "59%";
        frmAccountDetailsMB.lblCardAccountName.left = "12%";
        frmAccountDetailsMB.lblCardNumber.left = "12%";
        frmAccountDetailsMBPreShow.call(this);
        //DisableFadingEdges.call(this, frmAccountDetailsMB);
        //currForm = frmAccountDetailsMB;
        //var deviceHght = kony.os.deviceInfo().deviceHeight;
        //var osType = kony.os.deviceInfo().name;
        //var btnskin = "btnToDoIcon";
        //var btnFocusSkin = "btnToDoIconFoc";
        //frmAccountDetailsMB.hbxOnHandClick.isVisible = true;
        //hbxHdrCommon.btnRight.skin = btnFocusSkin;
        //frmAccountDetailsMB.btnOptions.skin = btnFocusSkin;
        //frmAccountDetailsMB.imgHeaderMiddle.src = "arrowtop.png";
        //frmAccountDetailsMB.imgHeaderRight.src = "empty.png";
        //populateBenfDetails(frmAccountDetailsMB);
    }
}

function frmAccountDetailsMBMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmAccountStatementMBMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        isSignedUser = true;
        frmAccountStatementMB.scrollboxMain.scrollToEnd();
        clearCCValues.call(this);
        frmAccountStatementMBPreShow.call(this);
        viewAccntFullStatement.call(this);
        DisableFadingEdges.call(this, frmAccountStatementMB);
    }
}

function frmAccountStatementMBMenuPostshow() {
    if (gblCallPrePost) {
        deviceInfo = kony.os.deviceInfo();
        if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
            isMenuRendered = false;
            isMenuShown = false;
            frmAccountStatementMB.scrollboxMain.scrollToEnd();
        }
    }
    assignGlobalForMenuPostshow();
}

function frmAccountSummaryLandingMenuPreshow() {
    if (gblCallPrePost) {
        registerForTimeOut.call(this);
        frmAccountSummaryLanding.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        frmAccountSummaryLandingPreShow.call(this);
    }
}
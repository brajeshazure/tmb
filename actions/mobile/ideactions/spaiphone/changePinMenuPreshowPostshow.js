function frmMBChangePINEnterExistsPinMenuPreshow() {
    if (gblCallPrePost) {
        frmMBChangePINEnterExistsPin.lblCardNumber.text = frmMBManageCard.lblCardNumber.text;
        frmMBChangePINEnterExistsPin.lblCardNumber.skin = getCardNoSkin();
        frmMBChangePINEnterExistsPin.lblCardAcctName.text = frmMBManageCard.lblCardAccountName.text;
        frmMBChangePINEnterExistsPin.imgCard.src = frmMBManageCard.imgCard.src;
        frmMBChangePINEnterExistsPinLocalePreshow();
    }
}

function frmMBChangePINEnterExistsPinLocalePreshow() {
    changeStatusBarColor();
    frmMBChangePINEnterExistsPin.lblHdrTxt.text = kony.i18n.getLocalizedString("ExistingPIN_Title");
    frmMBChangePINEnterExistsPin.lblenterpindesc.text = kony.i18n.getLocalizedString("ExistingPIN_Topic");
    frmMBChangePINEnterExistsPin.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    if (frmMBChangePINEnterExistsPin.flxnewpinlink.isVisible) {
        var errMsg = kony.i18n.getLocalizedString("PIN_Err_NotCorrect");
        errMsg = errMsg.replace("{x_time_wrong_pin}", remainAttempt);
        frmMBChangePINEnterExistsPin.lblErrMsg.text = errMsg;
        frmMBChangePINEnterExistsPin.lblforgotpin.text = kony.i18n.getLocalizedString("ExistingPIN_Instruction");
        frmMBChangePINEnterExistsPin.lblrequestnewpin.text = kony.i18n.getLocalizedString("RequestNewPIN_Linked");
    }
}

function frmMBChangePINEnterExistsPinPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBChangePinSuccessMenuPreshow() {
    if (gblCallPrePost) {
        frmMBChangePinSuccess.lblCardNumber.text = frmMBChangePINConfirm.lblCardNumber.text;
        frmMBChangePinSuccess.lblCardNumber.skin = getCardNoSkin();
        frmMBChangePinSuccess.lblCardAcctName.text = frmMBChangePINConfirm.lblCardAcctName.text;
        frmMBChangePinSuccess.imgCard.src = frmMBChangePINConfirm.imgCard.src;
        frmMBChangePinSuccessLocalePreshow();
    }
}

function frmMBChangePinSuccessLocalePreshow() {
    changeStatusBarColor();
    frmMBChangePinSuccess.lblHeader.text = kony.i18n.getLocalizedString("CAV08_keyTitle");
    var prodName = "";
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        prodName = gblSelectedCard["ProductNameEng"];
    } else {
        prodName = gblSelectedCard["ProductNameThai"];
    }
    var resultMsg = kony.i18n.getLocalizedString("ChangePIN_ResultDesc");
    resultMsg = resultMsg.replace("{Card_Type_name}", prodName);
    frmMBChangePinSuccess.lblCompletedesc.text = resultMsg;
    frmMBChangePinSuccess.lblManageCard.text = kony.i18n.getLocalizedString("CCA05_BtnCardDetail");
    frmMBChangePinSuccess.lblManageOtherCards.text = kony.i18n.getLocalizedString("CCA05_BtnActOtherCard");
}

function frmMBChangePinSuccessPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBChangePinFailurePreshow() {
    if (gblCallPrePost) {
        frmMBChangePinFailureLocalePreshow();
    }
}

function frmMBChangePinFailureLocalePreshow() {
    changeStatusBarColor();
    frmMBChangePinFailure.lblHeader.text = kony.i18n.getLocalizedString("CAV08_keyTitle");
    frmMBChangePinFailure.lblCallcenternum.text = kony.i18n.getLocalizedString("DBI03_Call1558");
    frmMBChangePinFailure.lblManageCards.text = kony.i18n.getLocalizedString("CCA05_BtnActOtherCard");
    frmMBChangePinFailure.rchAddress.text = kony.i18n.getLocalizedString("PIN_Err_NotChange");
}

function frmMBChangePINConfirmMenuPreshow() {
    if (gblCallPrePost) {
        frmMBChangePINConfirm.lblCardNumber.text = frmMBChangePINEnterExistsPin.lblCardNumber.text;
        frmMBChangePINConfirm.lblCardNumber.skin = getCardNoSkin();
        frmMBChangePINConfirm.lblCardAcctName.text = frmMBChangePINEnterExistsPin.lblCardAcctName.text;
        frmMBChangePINConfirm.imgCard.src = frmMBChangePINEnterExistsPin.imgCard.src;
        frmMBChangePINConfirmLocalePreshow();
    }
}

function frmMBChangePINConfirmLocalePreshow() {
    changeStatusBarColor();
    frmMBChangePINConfirm.lblHdrTxt.text = kony.i18n.getLocalizedString("SetPIN_Title");
    //frmMBChangePINConfirm.lblenterpindesc.text = kony.i18n.getLocalizedString("SetPIN_NewPINTopic");
    if (isConfirmPin) {
        frmMBChangePINConfirm.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("SetPIN_ConfirmPINTopic");
    } else {
        frmMBChangePINConfirm.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("SetPIN_NewPINTopic");
    }
    frmMBChangePINConfirm.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
}

function frmMBChangePINEnterExistsPinPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBChangePINConfirmMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBChangePinContactCenterPreshow() {
    if (gblCallPrePost) {
        frmMBChangePinContactCenterLocalePreshow();
    }
}

function frmMBChangePinContactCenterPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBChangePinFailurePostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBChangePinContactCenterLocalePreshow() {
    frmMBChangePinContactCenter.lblLockedMsg2.text = kony.i18n.getLocalizedString("PIN_Locked_1558");
    frmMBChangePinContactCenter.rchLockedMsg1.text = kony.i18n.getLocalizedString("PIN_Err_Locked3");
    frmMBChangePinContactCenter.btnCallCenterNum.text = kony.i18n.getLocalizedString("DBI03_Call1558");
}








function check() {
	var pass = frmMBsetPasswd.txtTransPass.text;
	flagNumber = flagSpecial = flagUpperCase = false;
	for (j = 0; j < pass.length; j++) {
		var i = pass.charCodeAt(j);
		if (i >= 65 && i < 91)
			flagUpperCase = true;
		if (i == 33)
			flagSpecial = true;
		if (i >= 48 && i <= 57)
			flagNumber = true;
	}
	if (flagNumber && flagSpecial && flagUpperCase && pass.length >= 6) {
		alert("Your Password is Strong");
		frmMBsetPasswd.imgStrength.src = "green2.png";
	} else if ((flagNumber || flagUpperCase || flagSpecial) && pass.length >= 6) {
		frmMBsetPasswd.imgStrength.src = "orange2.png";
	} else {
		frmMBsetPasswd.imgStrength.src = "red2.png";
	}
}

function passwordChanged() {
	var currForm = kony.application.getCurrentForm();
	currForm.lblPasswdStrength.setVisibility(true);
	var strength = 0;
	//var strongRegex = new RegExp("^(?=.{9,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
	//var mediumRegex = new RegExp("^(?=.{8,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
	//var enoughRegex = new RegExp("^(?=.{6,}).*", "g");
	var isUpper = new RegExp("[A-Z]");
	var isLower = new RegExp("[a-z]");
	var isDigit = new RegExp("[0-9]");
	var isSpecial = new RegExp("[^a-zA-Z0-9\s]");
	if (gblflag == 0) {
		var pwd = currForm.txtTransPass.text;
	} else if (gblflag == 1) {
		var pwd = currForm.txtTemp.text;
	}
	var i = 0;
	var count = 0;
	var len = pwd.length;
	for (i = 0; i < len; i++) {
		if (pwd[i] == ' ') return false;
	}
	for (i = 0; i < len; i++) {
		var temp = pwd[i] + "";
		if (isSpecial.test(temp)) {
			count = count + 1;
		}
	}
	if (len > 6) {
		strength++;
	}
	//if (len >= 1) {
	//		currForm.hbxPasswdStrength.setVisibility(true);
	//		//return false;
	//	}
	if (isDigit.test(pwd) && (isUpper.test(pwd) || isLower.test(pwd))) {
		strength++;
		//
	}
	if (isLower.test(pwd) && isUpper.test(pwd)) {
		strength++;
		//
	}
	if (isSpecial.test(pwd)) {
		strength++;
		//
	}
	if (count >= 2) {
		strength++;
		//
	}
	//if (pwd != null && len < 8) {
	//currForm.lblPasswdStrength.setVisibility(false);
	//}
	//else {
	//alert("stren :: " + strength)
	currForm.lblPasswdStrength.setVisibility(true);
	if (strength > 4) {
		currForm.hbxPasswdStrength.skin = hboxGreen;
		currForm.lblPasswdStrength.text = kony.i18n.getLocalizedString("passStrong");
		return true;
	} else if (strength >= 2 && strength <= 4) {
		currForm.hbxPasswdStrength.skin = hboxYellow;
		currForm.lblPasswdStrength.text = kony.i18n.getLocalizedString("passMedium");
		return true;
	} else if (strength < 2) {
		currForm.hbxPasswdStrength.skin = hboxRed;
		currForm.lblPasswdStrength.text = kony.i18n.getLocalizedString("passWeak");
		return false;
	}
	//}
}

function passwordStrength() {
	var currForm = kony.application.getCurrentForm();
	currForm.lblPasswdStrength.setVisibility(true);
	var strength = 0;
	var isUpper = new RegExp("[A-Z]");
	var isLower = new RegExp("[a-z]");
	var isDigit = new RegExp("[0-9]");
	var isSpecial = new RegExp("[^a-zA-Z0-9\s]");
	var pwd = currForm.txtTransPass.text;
	
	if (gblflag == 1) {
		pwd = currForm.txtTransPassShow.text;
	}
	var i = 0;
	var count = 0;
	var len = pwd.length;
	for (i = 0; i < len; i++) {
		if (pwd[i] == ' ') return false;
	}
	for (i = 0; i < len; i++) {
		var temp = pwd[i] + "";
		if (isSpecial.test(temp)) {
			count = count + 1;
		}
	}
	if (len > 6) {
		strength++;
	}

	if (isDigit.test(pwd) && (isUpper.test(pwd) || isLower.test(pwd))) {
		strength++;
	}
	if (isLower.test(pwd) && isUpper.test(pwd)) {
		strength++;
	}
	if (isSpecial.test(pwd)) {
		strength++;
	}
	if (count >= 2) {
		strength++;
	}

	currForm.lblPasswdStrength.setVisibility(true);
	if (strength > 4) {
		currForm.hbxPasswdStrength.skin = hboxGreen;
		currForm.lblPasswdStrength.text = kony.i18n.getLocalizedString("passStrong");
		return true;
	} else if (strength >= 2 && strength <= 4) {
		currForm.hbxPasswdStrength.skin = hboxYellow;
		currForm.lblPasswdStrength.text = kony.i18n.getLocalizedString("passMedium");
		return true;
	} else if (strength < 2) {
		currForm.hbxPasswdStrength.skin = hboxRed;
		currForm.lblPasswdStrength.text = kony.i18n.getLocalizedString("passWeak");
		return false;
	}
}
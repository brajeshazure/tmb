//const for term condition flow
var TNC_FLOW_TMB_DEBIT_CARD_ACTIVATION = "TMB_DEBIT_CARD_ACTIVATION";
var TNC_FLOW_TMB_CREDIT_CARD_ACTIVATION = "TMB_CREDIT_CARD_ACTIVATION";
var TNC_FLOW_TMB_READY_CASH_ACTIVATION = "TMB_READY_CASH_ACTIVATION";
gblCardRefId = "";
gblCardType = "C"
gblCardNumber = "";
gblServiceCall = "S";
gblPasswordSucces = true;
gblNickNameCard = "";
gblCardExpiryDate = "";
gblCardCitizenId = "";
gblCardTrans = "";
gblPinCheck = "valid";
isPopupCancel = false;

function cardManagementActivationCall(inCardType, inCardNumber, inCardRefId, inNickName) {
    try {
        gblPasswordSucces = true;
        if ("undefined" != typeof inCardType) {
            cardType = inCardType;
        }
        if ("undefined" != typeof inCardNumber) {
            cardNumber = inCardNumber;
        }
        if ("undefined" != typeof inCardRefId) {
            cardRefId = inCardRefId;
        }
        gblNickNameCard = inNickName;
        gblCardNumber = cardNumber;
        gblMaskCardNumber = cardNumber;
        gblCardRefId = cardRefId;
        frmCardActivationDetails.btnCardTypeImage.skin = "sknBtnGreyBG"
        frmCardActivationDetails.btnCardTypeImage.focusSkin = "sknBtnGreyBG"
        if (cardType == "Credit Card") {
            gblCardType = "C";
            gblMBNewTncFlow = TNC_FLOW_TMB_CREDIT_CARD_ACTIVATION;
            activationReadUTFFile();
            showCardDetailsFlex();
            if (cardNumber.substring(0, 1) == "4") {
                frmCardActivationDetails.btnCardTypeImage.skin = "sknBtnBGVisa"
                frmCardActivationDetails.btnCardTypeImage.focusSkin = "sknBtnBGVisa"
            } else if (cardNumber.substring(0, 1) == "5") {
                frmCardActivationDetails.btnCardTypeImage.skin = "sknBtnBGMaster"
                frmCardActivationDetails.btnCardTypeImage.focusSkin = "sknBtnBGMaster"
            }
            frmMBNewTncCreditCardClickBtnNext();
        } else if (cardType == "Debit Card") {
            gblCardType = "D";
            gblCardTrans = "A";
            gblMBNewTncFlow = TNC_FLOW_TMB_DEBIT_CARD_ACTIVATION;
            activationReadUTFFile();
            showCardDetailsFlex();
            frmCardActivationDetails.btnCardTypeImage.skin = "sknBtnBGVisa"
            frmCardActivationDetails.btnCardTypeImage.focusSkin = "sknBtnBGVisa"
        } else if (cardType == "Ready Cash") {
            frmCardActivationDetails.btnCardTypeImage.skin = "sknBtnBGReady"
            frmCardActivationDetails.btnCardTypeImage.focusSkin = "sknBtnBGReady"
            gblCardType = "R";
            gblMBNewTncFlow = TNC_FLOW_TMB_READY_CASH_ACTIVATION;
            activationReadUTFFile();
            showCardDetailsFlex();
            frmMBNewTncReadyCashClickBtnNext();
        } else {
            return false;
        }
    } catch (err) {}
}

function activationReadUTFFile() {
    try {
        var input_param = {};
        var locale = kony.i18n.getCurrentLocale();
        if (locale == "en_US") {
            input_param["localeCd"] = "en_US";
        } else {
            input_param["localeCd"] = "th_TH";
        }
        var moduleKey = "";
        if (gblMBNewTncFlow == TNC_FLOW_TMB_CREDIT_CARD_ACTIVATION) {
            moduleKey = "TMBCreditCardActivation";
        } else if (gblMBNewTncFlow == TNC_FLOW_TMB_DEBIT_CARD_ACTIVATION) {
            moduleKey = "TMBDebitCardActivation";
        } else if (gblMBNewTncFlow == TNC_FLOW_TMB_READY_CASH_ACTIVATION) {
            moduleKey = "TMBReadyCashActivation";
        }
        input_param["activationVia"] = "3";
        input_param["moduleKey"] = moduleKey;
        showLoadingScreen();
        invokeServiceSecureAsync("readUTFFile", input_param, activationReadUTFFileCallBack);
    } catch (err) {}
}

function activationReadUTFFileCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            gblDPPk = result["pk"];
            gblDPRandNumber = result["randomNumber"];
            if (gblCardType == "D" && gblCardTrans == "A") {
                frmMBNewTnC.btnRight.setVisibility(true);
                frmMBNewTnC.btnnext.text = kony.i18n.getLocalizedString("CAV03_btnAgree");
                frmMBNewTnC.btnback.text = kony.i18n.getLocalizedString("Back");
                frmMBNewTnC.richtextTnC.text = result["fileContent"];
                frmMBNewTnC.lblDescSubTitle.setFocus(true);
                dismissLoadingScreen();
                frmMBNewTnC.show();
            } else {
                dismissLoadingScreen();
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function formatCardNumberOnChange() {
    var cardNumberOne = frmCardActivationDetails.txtCCNumbetInputOne.text;
    var cardNumberTwo = frmCardActivationDetails.txtCCNumbetInputTwo.text;
    var cardNumberThree = frmCardActivationDetails.txtCCNumbetInputThree.text;
    var cardNumberFour = frmCardActivationDetails.txtCCNumbetInputFour.text;
    var cardNumber = replaceAll(cardNumberOne + cardNumberTwo + cardNumberThree + cardNumberFour, " ", "");
    if (parseInt(cardNumber.length) == 16) {
        gblCardNumber = cardNumber;
        verifyCardNumberForActivation();
    }
}

function verifyCardNumberForActivation() {
    showLoadingScreen();
    GBL_FLOW_ID_CA = 12;
    getUniqueID();
}

function validateCardNumberDetails(uniqueId) {
    var inputParam = {};
    inputParam["cardType"] = gblCardType;
    inputParam["deviceId"] = uniqueId;
    inputParam["cardNumber"] = encryptDPUsingE2EE(gblCardNumber);
    inputParam["cardRefId"] = gblCardRefId;
    inputParam["maskCardNumber"] = gblMaskCardNumber;
    showLoadingScreen();
    invokeServiceSecureAsync("verifyCardNumber", inputParam, validateCardNumberDetailsCallBack);
}

function setFocusToCardNumber() {
    frmCardActivationDetails.txtCCNumbetInputThree.setFocus(true);
    frmCardActivationDetails.txtCCNumbetInputThree.text = "";
}

function validateCardNumberDetailsCallBack(status, resulttable) {
    kony.print("validateCardNumberDetailsCallBack --> resulttable" + resulttable);
    if (status == 400) {
        if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == 0) {
            dismissLoadingScreen();
            if (gblCardType == "D") {
                gblPk = resulttable["pubKey"];
                gblRand = resulttable["serverRandom"];
                gblAlgorithm = resulttable["hashAlgorithm"];
                kony.print("Coming here :: before setting false to txtboxthree")
                frmCardActivationDetails.txtCCNumbetInputThree.setFocus(false);
                showTxnPasswordPopupActivateDebitCreditCard();
            } else if (gblCardType == "C") {
                gblDPPk = resulttable["pk"];
                gblDPRandNumber = resulttable["randomNumber"];
                gblCardExpiryDate = resulttable["expiryDate"];
                showCVVDetails();
            } else if (gblCardType == "R") {
                gblDPPk = resulttable["pk"];
                gblDPRandNumber = resulttable["randomNumber"];
                gblCardCitizenId = resulttable["citizendId"];
                showCitizenId();
            }
        } else if (resulttable["opstatus"] == "-1") {
            dismissLoadingScreen();
            gblDPPk = "";
            gblDPRandNumber = "";
            frmMBanking.show();
        } else {
            dismissLoadingScreen();
            if (resulttable["opstatus"] == "8005") {
                gblDPPk = resulttable["pk"];
                gblDPRandNumber = resulttable["randomNumber"];
                if (resulttable["errCode"] == "90001") {
                    frmCardActivationDetails.txtCCNumbetInputThree.text = "";
                    frmCardActivationDetails.txtCCNumbetInputThree.skin = "sknTxtLightBlack100px";
                    frmCardActivationDetails.txtCCNumbetInputThree.focusSkin = "sknTxtLightBlack100px";
                    showAlertWithCallBack(kony.i18n.getLocalizedString("ActCC_Err_InvalidCC"), kony.i18n.getLocalizedString("info"), setFocusToCardNumber);
                } else {
                    frmCardActivationDetails.txtCCNumbetInputThree.text = "";
                    frmCardActivationDetails.txtCCNumbetInputThree.skin = "sknTxtLightBlack100px";
                    frmCardActivationDetails.txtCCNumbetInputThree.focusSkin = "sknTxtLightBlack100px";
                    if (gblCardType == "D") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActDB_Err_LockDB"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    } else if (gblCardType == "C") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActCC_Err_LockCC"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    } else if (gblCardType == "R") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActCC_Err_LockCC"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    }
                }
            }
        }
    }
}

function callBackShowMBCardList() {
    frmCardActivationDetails.txtCCNumbetInputThree.text = "";
    frmCardActivationDetails.txtCCNumbetInputThree.skin = "sknTxtLightBlack100px";
    frmCardActivationDetails.txtCCNumbetInputThree.focusSkin = "sknTxtLightBlack100px";
    preShowMBCardList();
}

function mbValidateCardCVV() {
    showLoadingScreen();
    var creditCardCVV = decryptPinDigit(glbPin);
    validateCardCVV(creditCardCVV.substring(0, 3)); // temporary solution to make flow work
}

function validateCardCVV(atmPinEncrypted) {
    var inputParam = {};
    inputParam["cvv"] = encryptDPUsingE2EE(atmPinEncrypted);
    inputParam["cardType"] = gblCardType;
    inputParam["cardRefId"] = gblCardRefId;
    inputParam["cCardExpiryDate"] = gblCardExpiryDate;
    showLoadingScreen();
    invokeServiceSecureAsync("verifyCVVE2ECardActivation", inputParam, validateCardCVVCallBack);
}

function validateCardCVVCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            //PIN Verify Success
            gblDPPk = resulttable["pk"];
            gblDPRandNumber = resulttable["randomNumber"];
            showTxnPasswordPopupActivateDebitCreditCard();
        } else if (resulttable["opstatus"] == "-1") {
            //showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            gblDPPk = "";
            gblDPRandNumber = "";
            frmMBanking.show();
        } else {
            clearCVVMB_New(); // method to clear the virtual keyboard
            if (resulttable["opstatus"] == "8005") {
                gblDPPk = resulttable["pk"];
                gblDPRandNumber = resulttable["randomNumber"];
                if (resulttable["errCode"] == "90001") {
                    showAlert(kony.i18n.getLocalizedString("ActCC_Err_InvalidCVV"), kony.i18n.getLocalizedString("info"));
                } else {
                    if (gblCardType == "D") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActDB_Err_LockDB"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    } else if (gblCardType == "C") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActCC_Err_LockCC"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    } else if (gblCardType == "R") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActCC_Err_LockCC"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    }
                }
            }
            return false;
        }
    }
}

function verifyCardPinServiceForDebitCardActivation() {
    glbPinArr[1] = glbPin;
    if (glbPinArr[0] != glbPinArr[1]) {
        gblPinCheck = "invalid";
        assignATMCardPin("invalid");
        //showAlertWithCallBack(kony.i18n.getLocalizedString("ActDB_Err_InvalidPIN"),kony.i18n.getLocalizedString("info"),clearATMPINMBNew);
    } else {
        gblPinCheck = "valid";
        assignCardPinService();
    }
}

function assignCardPinService() {
    //service call shoud be done
    //Sample value
    encryptCardPinUsingE2EE(decryptPinDigit(glbPin));
    //			var RPIN = encryptPinUsingE2EE(glbPin);
    //			alert("RPIN: "+RPIN);
}

function encryptCardPinUsingE2EE(pin) {
    var e2eeResult = "";
    if (gblPk != "" && gblRand != "" && gblAlgorithm != "") {
        e2eeResult = encryptForATMPinAndroid(pin);
        if (undefined != e2eeResult) {
            assignATMCardPin(e2eeResult);
        } else {
            //Error handle when encryption fail
        }
    } else {
        //Error handle when init service failed
    }
}

function assignATMCardPin(encryptedPin) {
    showDismissLoadingMBIB(true);
    var inputParam = {};
    inputParam["rPin"] = encryptedPin;
    inputParam["pinVerify"] = gblPinCheck;
    var status = invokeServiceSecureAsync("assignPinCardActivation", inputParam, assignATMCardPinCallBack);
}

function assignATMCardPinCallBack(status, resulttable) {
    if (status == 400) {
        showDismissLoadingMBIB(false);
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == "0") {
                showCardActivationComplete();
            } else {
                gblPasswordSucces = false;
                showCardActivationComplete();
            }
        } else if (resulttable["opstatus"] == 1) {
            gblPasswordSucces = false;
            showCardActivationComplete();
        } else {
            gblPk = resulttable["pubKey"];
            gblRand = resulttable["serverRandom"];
            gblAlgorithm = resulttable["hashAlgorithm"];
            //showAlert("Waiting for Error Msg",kony.i18n.getLocalizedString("info"))
            dismissLoadingScreen();
            if (resulttable["opstatus"] == "8005") {
                if (resulttable["errCode"] == "90001") {
                    showAlertWithCallBack(kony.i18n.getLocalizedString("ActDB_Err_InvalidPIN"), kony.i18n.getLocalizedString("info"), resetPINwithInitOnlinePin);
                }
            }
        }
    }
}

function frmMBNewTncCreditCardClickBtnNext() {
    frmCardActivationDetails.show();
    frmCardActivationDetails.txtCCNumbetInputOne.setFocus(true);
}

function frmMBNewTncDebitCardClickBtnNext() {
    frmCardActivationDetails.show();
    frmCardActivationDetails.txtCCNumbetInputOne.setFocus(true);
}

function frmMBNewTncReadyCashClickBtnNext() {
    frmCardActivationDetails.show();
    frmCardActivationDetails.txtCCNumbetInputOne.setFocus(true);
}

function frmMBNewTncCreditCardOnClickBtnBack() {
    //frmStart.show();
}

function frmMBNewTncDebitCardOnClickBtnBack() {
    //frmStart.show();
}

function frmMBNewTncReadyCashOnClickBtnBack() {
    //frmStart.show();
}

function onTextChangeCardNumber() {
    var cardNoLen = replaceAll(frmCardActivationDetails.txtCCNumbetInputThree.text, " ", "").length;
    //alert("cardNoLen"+cardNoLen)
    if (cardNoLen == 6) {
        //alert("cardNoLen222"+cardNoLen)
        //showLoadingScreen();
        frmCardActivationDetails.lblHdrTxt.setFocus(true)
        frmCardActivationDetails.txtCCNumbetInputThree.setFocus(false);
        formatCardNumberOnChange();
    }
}
gblisCardActivationCompleteFlow = false;

function onClickManageOtherCards() {
    gblisCardActivationCompleteFlow = true;
    preShowMBCardList();
}

function onClickManageThisCard() {
    showLoadingScreen();
    inputparam = {};
    inputparam["cardFlag"] = "3";
    invokeServiceSecureAsync("customerCardList", inputparam, getCallbackShowCardListUpdated);
}

function getCallbackShowCardListUpdated(status, resulttable) {
    var cardType = "";
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            if (resulttable["errCode"] == undefined) {
                gblCardList = resulttable;
                if (gblCardType == "C" && resulttable["creditCardRec"].length > 0) {
                    loadCurrentCardDetail(resulttable["creditCardRec"]);
                } else if (gblCardType == "D" && resulttable["debitCardRec"].length > 0) {
                    loadCurrentCardDetail(resulttable["debitCardRec"]);
                } else if (gblCardType == "R" && resulttable["readyCashRec"].length > 0) {
                    loadCurrentCardDetail(resulttable["readyCashRec"]);
                }
            } else {
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}

function loadCurrentCardDetail(cardArray) {
    var selectedCard = "";
    currentCardNo = gblSelectedCard["cardNo"];
    for (var index = 0; index < cardArray.length; index++) {
        var indexCard = cardArray[index]["accountNoFomatted"].split("-").join("  ");
        if (currentCardNo == indexCard) {
            selectedCard = cardArray[index];
            imageName = cardArray[index]["IMG_CARD_ID"];
            cardNickName = cardArray[index]["acctNickName"];
            if (cardNickName == "") {
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = cardArray[index]["defaultCurrentNickNameEN"];
                } else {
                    cardNickName = cardArray[index]["defaultCurrentNickNameTH"];
                }
            }
            selectedCard["imgSrc"] = getImageSRC(imageName);
            selectedCard["cardNo"] = indexCard;
            selectedCard["cardName"] = cardNickName;
            break;
        }
    }
    gblSelectedCard = selectedCard;
    gotoManageCard(gblSelectedCard);
    dismissLoadingScreen();
}

function assignCardTypeToSuccessText() {
    kony.print("gblCardType" + gblCardType);
    var productName = "";
    var locale_app = kony.i18n.getCurrentLocale();
    if (locale_app == "th_TH") {
        productName = gblSelectedCard["ProductNameThai"];
    } else {
        productName = gblSelectedCard["ProductNameEng"];
    }
    if (gblCardType == "C") {
        frmCardActivationComplete.lblSuccessCardMessage.text = kony.i18n.getLocalizedString("CCA05_ResultDesc").toString();
    } else if (gblCardType == "D") {
        frmCardActivationComplete.lblSuccessCardMessage.text = kony.i18n.getLocalizedString("ActivateDB_Success").toString();
    } else if (gblCardType == "R") {
        frmCardActivationComplete.lblSuccessCardMessage.text = kony.i18n.getLocalizedString("RDA07_ResultDesc").toString();
    }
}

function frmCardActivationCompleteInit() {
    frmCardActivationComplete.btnMainMenu.onClick = handleMenuBtn;
    //frmCardActivationComplete.flexFooter.onTouchEnd = onClickManageOtherCards;
    frmCardActivationComplete.btnManageThisCard.onClick = onClickManageThisCard;
    frmCardActivationComplete.btnManageCards.onClick = onClickManageOtherCards;
}

function frmCardActivationCompletePostShow() {
    var locale_app = kony.i18n.getCurrentLocale();
    var accountName = "";
    kony.print("accountName 111" + gblSelectedCard["acctNickName"])
    if (gblSelectedCard["acctNickName"] == null || gblSelectedCard["acctNickName"] == undefined) {
        if (locale_app == "th_TH") {
            kony.print("accountName 22222" + gblSelectedCard["defaultCurrentNickNameTH"])
            accountName = gblSelectedCard["defaultCurrentNickNameTH"];
        } else {
            kony.print("accountName 33333" + gblSelectedCard["defaultCurrentNickNameEN"])
            accountName = gblSelectedCard["defaultCurrentNickNameEN"];
        }
    } else {
        kony.print("accountName 33333" + gblSelectedCard["acctNickName"])
        accountName = gblSelectedCard["acctNickName"];
    }
    gblisCardActivationCompleteFlow = false;
    if (gblCallPrePost) {
        frmCardActivationDetails.btnMainMenu.setEnabled(true);
        var maskedCardNo = gblCardNumber;
        frmCardActivationComplete.lblCardNumber.text = maskedCardNo.substring(0, 4) + "  " + maskedCardNo.substring(4, 6) + "XX  " + "XXXX" + "  " + maskedCardNo.substring(12, 16);
    }
    frmCardActivationComplete.lblCardName.text = accountName;
    assignGlobalForMenuPostshow();
}

function frmCardActivationCompletePreshow() {
    changeStatusBarColor();
    if (gblCallPrePost) {
        if (gblPasswordSucces) {
            assignCardTypeToSuccessText();
            frmCardActivationComplete.imgStatus.src = "iconcomplete.png"
            frmCardActivationComplete.flexCardImage.isVisible = true;
            frmCardActivationComplete.flexCallCC.isVisible = false;
            frmCardActivationComplete.btnManageThisCard.isVisible = true;
            frmCardActivationComplete.lblSuccessCardMessage.skin = "sknlblgrey110px"
            frmCardActivationComplete.imgCard.src = gblSelectedCard["imgSrc"];
            frmCardActivationComplete.lblCardNumber.skin = getCardNoSkin();
        } else {
            frmCardActivationComplete.btnManageThisCard.isVisible = false;
            frmCardActivationComplete.flexCardImage.isVisible = false;
            frmCardActivationComplete.flexCallCC.isVisible = true;
            frmCardActivationComplete.btnCall1558.onClick = CallTheNumber;
            frmCardActivationComplete.lblSuccessCardMessage.text = kony.i18n.getLocalizedString("ActCC_Err_FailAct");
            frmCardActivationComplete.lblSuccessCardMessage.skin = "lblGrey48px"
            frmCardActivationComplete.imgStatus.src = "iconnotcomplete.png"
        }
    }
    if (!gblPasswordSucces) {
        frmCardActivationComplete.lblSuccessCardMessage.text = kony.i18n.getLocalizedString("ActCC_Err_FailAct");
    }
    frmCardActivationComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("CAV08_keyTitle");
    frmCardActivationComplete.btnManageThisCard.text = kony.i18n.getLocalizedString("RDA07_ManageCardLink");
    frmCardActivationComplete.btnManageCards.text = kony.i18n.getLocalizedString("RDA07_BtnActOtherCard");
    frmCardActivationComplete.btnCall1558.text = kony.i18n.getLocalizedString("DBI03_Call1558");
}

function frmCardActivationDetailsPreShow() {
    changeStatusBarColor();
    frmCardActivationDetails.btnMainMenu.setEnabled(true);
    if (gblCallPrePost) {
        kony.print("calling preshow" + gblCallPrePost)
        if (frmCardActivationDetails.flexCVVInputDetails.isVisible) {
            frmCardActivationDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("CCA04_Title");
        } else if (frmCardActivationDetails.flexCitizenIDDetails.isVisible) {
            frmCardActivationDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("RDA04_Title");
        } else {
            frmCardActivationDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("RDA03_Title");
        }
        frmCardActivationDetails.lblEnterCCNumber.text = kony.i18n.getLocalizedString("CCA03_Instruction");
        frmCardActivationDetails.lblEnterCVV.text = kony.i18n.getLocalizedString("CCA04_Instruction");
        frmCardActivationDetails.CreditCardCVVNumber.text = kony.i18n.getLocalizedString("CCA04_CVV");
        frmCardActivationDetails.lblCVVName.text = kony.i18n.getLocalizedString("CCA04_CVV") + ":";
        frmCardActivationDetails.lblEnterCitizenId.text = kony.i18n.getLocalizedString("RDA04_Instruction");
        //frmCardActivationDetails.txtCCNumbetInputTwo.width = "5%";
        //frmCardActivationDetails.txtCCNumbetInputThree.width = "38%";
        //addNumberCheckListnerZeroToNine(frmCardActivationDetails.txtCCNumbetInputThree);
        //showCardDetailsFlex()
    }
}
/* 
function frmCardActivationDetailsPostShow(){
	if(gblCallPrePost){
	kony.print("calling preshow"+gblCallPrePost)
	assignCardNumberToCard();
	}
	assignGlobalForMenuPostshow();
	
} */
function frmCardActivationDetailsInit() {
    frmCardActivationDetails.btnMainMenu.onClick = handleMenuBtn;
    frmCardActivationDetails.txtCCNumbetInputThree.onTextChange = formatInputCreditCardNumber;
    frmCardActivationDetails.txtCititizenID.onTextChange = onTextChangeCitizenId;
}

function disbaleTextBoxesOnCardDetails() {
    frmCardActivationDetails.txtCCNumbetInputOne.setEnabled(false);
    frmCardActivationDetails.txtCCNumbetInputTwo.setEnabled(false);
    frmCardActivationDetails.txtCCNumbetInputFour.setEnabled(false);
}

function assignCardNumberToCard() {
    var cardNo = replaceAll(gblCardNumber, " ", "");
    frmCardActivationDetails.CCNumberOne.text = cardNo.substring(0, 4) + "  " + cardNo.substring(4, 6)
    frmCardActivationDetails.CCNumberThree.text = cardNo.substring(12, 16);
    frmCardActivationDetails.txtCCNumbetInputOne.text = cardNo.substring(0, 4);
    frmCardActivationDetails.txtCCNumbetInputTwo.text = cardNo.substring(4, 6);
    frmCardActivationDetails.txtCCNumbetInputFour.text = cardNo.substring(12, 16);
    frmCardActivationDetails.txtCCNumbetInputThree.setFocus(true);
    frmCardActivationDetails.txtCCNumbetInputThree.text = "";
    frmCardActivationDetails.txtCCNumbetInputThree.skin = "sknTxtLightBlack100px";
    frmCardActivationDetails.txtCCNumbetInputThree.focusSkin = "sknTxtLightBlack100px";
    frmCardActivationDetails.txtCCNumbetInputThree.placeholder = "_ _   _ _ _ _";
}

function frmCardActivationDetailsPostShow() {
    if (gblCallPrePost) {
        assignCardNumberToCard();
        disbaleTextBoxesOnCardDetails();
    }
    assignGlobalForMenuPostshow();
}

function disbaleTextBoxesOnCardDetails() {
    frmCardActivationDetails.txtCCNumbetInputOne.setEnabled(false);
    frmCardActivationDetails.txtCCNumbetInputTwo.setEnabled(false);
    frmCardActivationDetails.txtCCNumbetInputFour.setEnabled(false);
}

function showDebitCardSuccess() {
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    var refNo = "";
    var mobNO = "";
    showLoadingScreen();
    showOTPPopup(lblText, refNo, mobNO, showfrmMBAssignAtmPinNew, 3);
}

function showCardDetailsFlex() {
    frmCardActivationDetails.btnMainMenu.setEnabled(true);
    frmCardActivationDetails.flexCardDetails.isVisible = true;
    frmCardActivationDetails.flexCVVInputDetails.isVisible = false;
    frmCardActivationDetails.flexCVVSuccessDetails.isVisible = false;
    frmCardActivationDetails.flexCitizenIDDetails.isVisible = false;
    frmCardActivationDetails.flexDebitCardSuccess.isVisible = false;
    //init_functionCVVDetail();
}

function showCVVDetails() {
    frmCardActivationDetails.btnMainMenu.setEnabled(true);
    showLoadingScreen();
    frmCardActivationDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("CCA04_Title");
    frmCardActivationDetails.lblHdrTxt.setFocus(true);
    if (!isPopupCancel) {
        frmCardActivationDetails.txtCCNumbetInputThree.setFocus(false);
        isPopupCancel = false;
    }
    frmCardActivationDetails.flexCardDetails.isVisible = false;
    frmCardActivationDetails.flexCVVInputDetails.isVisible = true;
    frmCardActivationDetails.flexCVVSuccessDetails.isVisible = false;
    frmCardActivationDetails.flexCitizenIDDetails.isVisible = false;
    frmCardActivationDetails.flexDebitCardSuccess.isVisible = false;
    init_functionCVVDetail();
    dismissLoadingScreen();
}

function showCVVSuccess() {
    frmCardActivationDetails.btnMainMenu.setEnabled(false);
    frmCardActivationDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("CCA04_Title");
    frmCardActivationDetails.flexCardDetails.isVisible = false;
    frmCardActivationDetails.flexCVVInputDetails.isVisible = false;
    frmCardActivationDetails.flexCVVSuccessDetails.isVisible = true;
    frmCardActivationDetails.flexCitizenIDDetails.isVisible = false;
    frmCardActivationDetails.flexDebitCardSuccess.isVisible = false;
}

function showCitizenId() {
    frmCardActivationDetails.btnMainMenu.setEnabled(true);
    frmCardActivationDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("RDA04_Title");
    frmCardActivationDetails.flexCardDetails.isVisible = false;
    frmCardActivationDetails.flexCVVInputDetails.isVisible = false;
    frmCardActivationDetails.flexCVVSuccessDetails.isVisible = false;
    frmCardActivationDetails.flexCitizenIDDetails.isVisible = true;
    frmCardActivationDetails.flexDebitCardSuccess.isVisible = false;
    frmCardActivationDetails.txtCititizenID.text = "";
    frmCardActivationDetails.txtCititizenID.setFocus(true);
}

function NavigateToCompleteScreen() {
    kony.print("before the timer")
    var randNumber = parseInt((Math.random() * 1000000) + "") + "";
    //alert("randNumber"+randNumber)
    kony.print("before the timer" + randNumber)
    kony.timer.schedule(randNumber, showCardActivationComplete, 2, false);
}

function showCardActivationComplete() {
    frmCardActivationDetails.btnMainMenu.setEnabled(true);
    frmCardActivationComplete.show();
}

function validateCitizenId(citizendId) {
    var inputParam = {};
    inputParam["citizenId"] = encryptDPUsingE2EE(citizendId);
    inputParam["cardType"] = gblCardType;
    inputParam["cardRefId"] = gblCardRefId;
    inputParam["maskCardNumber"] = gblMaskCardNumber;
    showLoadingScreen();
    invokeServiceSecureAsync("verifyCitizenId", inputParam, validateCitizenIdCallBack);
}

function clearCitizenId() {
    kony.print("coming to callback::::::")
    frmCardActivationDetails.txtCititizenID.text = "";
}

function validateCitizenIdCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            //PIN Verify Success
            gblDPPk = resulttable["pk"];
            gblDPRandNumber = resulttable["randomNumber"];
            showTxnPasswordPopupActivateDebitCreditCard();
        } else if (resulttable["opstatus"] == "-1") {
            //showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            gblDPPk = "";
            gblDPRandNumber = "";
            frmMBanking.show();
        } else {
            if (resulttable["opstatus"] == "8005") {
                gblDPPk = resulttable["pk"];
                gblDPRandNumber = resulttable["randomNumber"];
                if (resulttable["errCode"] == "90001") {
                    showAlertWithCallBack(kony.i18n.getLocalizedString("ActRD_Err_InvalidCitizenID"), kony.i18n.getLocalizedString("info"), clearCitizenId);
                    //showAlert(kony.i18n.getLocalizedString("ActRD_Err_InvalidCitizenID"),kony.i18n.getLocalizedString("info"));
                    //kony.print("learing citizen Id ::::"+frmCardActivationDetails.txtCititizenID.text)
                    //kony.print("learing citizen Id ::::"+frmCardActivationDetails.txtCititizenID.text)
                } else {
                    kony.print("learing citizen Id in else::::" + frmCardActivationDetails.txtCititizenID.text)
                    frmCardActivationDetails.txtCititizenID.text = "";
                    if (gblCardType == "D") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActDB_Err_LockDB"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    } else if (gblCardType == "C") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActCC_Err_LockCC"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    } else if (gblCardType == "R") {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("ActCC_Err_LockCC"), kony.i18n.getLocalizedString("info"), callBackShowMBCardList);
                    }
                }
            }
            return false;
        }
    }
}

function showTxnPasswordPopupActivateDebitCreditCard() {
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    var refNo = "";
    var mobNO = "";
    showOTPPopup(lblText, refNo, mobNO, verifyPwdForDebitCreditCardActivation, 3);
}

function verifyPwdForDebitCreditCardActivation(tranPassword) {
    if (tranPassword == null || tranPassword == '') {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    } else {
        //if(gblServiceCall == "S"){
        showLoadingScreen();
        popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
        popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
        var inputParam = {};
        var locale = kony.i18n.getCurrentLocale();
        inputParam["password"] = tranPassword;
        inputParam["notificationAdd_appID"] = appConfig.appId;
        inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
        inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
        inputParam["locale"] = locale;
        invokeServiceSecureAsync("verifyCardTransPwdComposite", inputParam, verifyPwdForDebitCreditCardActivationCallBack);
        /*}else{
			if(tranPassword != 'kony@123'){
				gblPasswordSucces = false;
			}else{
				gblPasswordSucces = true;
			}
			popupTractPwd.dismiss();
			if(gblCardType == "C"){
	        	showCardActivationComplete();
			}else if(gblCardType == "D"){
				initOnlineDebitPIN();
				initializeAndNavigatetoPinAssignNew();
			}else if (gblCardType == "R"){
				showCardActivationComplete();
			}	
		}*/
    }
}

function verifyPwdForDebitCreditCardActivationCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
            return false;
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
            return false;
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMessage"]);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                alert("" + resulttable["errMsg"]);
                return false;
            } else {
                dismissLoadingScreen();
                popupTractPwd.dismiss();
                gblPasswordSucces = false;
                showCardActivationComplete();
                //alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                //return false;
            }
        } else if (resulttable["opstatus"] == 0) {
            //Do success
            popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
            popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
            popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
            popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
            popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
            popupTractPwd.dismiss();
            if (gblCardType == "C") {
                showCVVSuccess();
                NavigateToCompleteScreen();
                //showCardActivationComplete();
            } else if (gblCardType == "D") {
                initOnlineDebitPIN();
                initializeAndNavigatetoPinAssignNew();
            } else if (gblCardType == "R") {
                showCardActivationComplete();
            }
        } else {
            popupTractPwd.dismiss();
            gblPasswordSucces = false;
            showCardActivationComplete();
        }
    }
}
//new CVV Function
function init_functionCVVDetail() {
    initialParam();
    initialFunctionCVVNumber();
    clearCVVMB_New();
}

function initialFunctionCVVNumber() {
    //Digit 1-9
    frmCardActivationDetails.btn1.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn2.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn3.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn4.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn5.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn6.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn7.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn8.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn9.onClick = btnCVVPinClick;
    frmCardActivationDetails.btn11.onClick = btnCVVPinClick;
    //BtnClear
    frmCardActivationDetails.btn10.onClick = btnCVVPinClear;
    //BtnDelete
    frmCardActivationDetails.btn12.onClick = btnCVVPinDelete;
}

function verifyCVVRenderMB_New(pin) {
    if (isNotBlank(pin)) {
        if (glbPin.length < maxLenCVV) {
            addCVVMB_New(pin, ++glbIndex);
        }
    } else {
        renderCVVMB_New();
    }
}

function addCVVMB_New(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderCVVMB_New();
    if (glbPin.length == maxLenCVV) {
        mbValidateCardCVV();
    }
}

function renderCVVMB_New() {
    var btnObject = [];
    btnObject = [frmCardActivationDetails.imgcvvone, frmCardActivationDetails.imgcvvtwo, frmCardActivationDetails.imgcvvthree];
    var emptyPinDis = "cvv_empty.png";
    var emptyPin = "cvv_empty.png";
    var fillPin = "cvv_filled.png";
    for (var i = 0; i < glbPin.length; i++) {
        btnObject[i].src = fillPin;
    }
    for (var i = btnObject.length; i > glbPin.length; i--) {
        btnObject[i - 1].src = emptyPin;
        if (i > glbPin.length + 1) {
            btnObject[i - 1].src = emptyPinDis;
        }
    }
}

function clearCVVMB_New() {
    glbPin = "";
    glbIndex = 0;
    glbPinArr = [];
    verifyCVVRenderMB_New("");
}

function deleteCVVMB_New() {
    if (glbPinArr.length > 0 && glbPinArr[0].length == maxLenCVV) {
        glbPin = glbPinArr[0];
        glbIndex = glbPinArr[0].length;
        glbPinArr = [];
    }
    if (isNotBlank(glbPin)) {
        --glbIndex;
        glbPin = glbPin.substring(0, glbPin.length - 1);
        verifyCVVRenderMB_New("");
    }
}

function btnCVVPinClick(eventobject) {
    verifyCVVRenderMB_New.call(this, eventobject["text"]);
};

function btnCVVPinClear(eventobject) {
    clearCVVMB_New.call(this);
};

function btnCVVPinDelete(eventobject) {
    deleteCVVMB_New.call(this);
}
//PIN Setting
function btnPinClick(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function btnPinClear(eventobject) {
    resetPINwithInitOnlinePin.call(this);
};

function btnPinDelete(eventobject) {
    deleteATMPINMBNew.call(this);
};

function btnCancelEvent(eventobject) {
    goBackToPreviousPage();
};
gblTextboxLength = true;

function formatInputCreditCardNumber() {
    var input = frmCardActivationDetails.txtCCNumbetInputThree.text
    var inputLen = parseInt(input.length);
    if (inputLen == 0 || inputLen == 1) {
        gblTextboxLength = true;
        frmCardActivationDetails.txtCCNumbetInputThree.skin = "sknTxtLightBlack200px";
        frmCardActivationDetails.txtCCNumbetInputThree.focusSkin = "sknTxtLightBlack200px";
    }
    if (inputLen == 0) {
        frmCardActivationDetails.txtCCNumbetInputThree.skin = "sknTxtLightBlack100px";
        frmCardActivationDetails.txtCCNumbetInputThree.focusSkin = "sknTxtLightBlack100px";
    }
    if (inputLen == 2 && gblTextboxLength) {
        input = input.substring(0, 2) + "  " + input.substring(2, input.length);
        gblTextboxLength = false;
    }
    frmCardActivationDetails.txtCCNumbetInputThree.text = input;
    if (replaceAll(input, " ", "").length == 6) {
        formatCardNumberOnChange();
    }
}

function frmMBAssignAtmPinNewVRpreShow() {
    changeStatusBarColor();
    frmMBAssignAtmPinNew.lblHdrTxt.text = kony.i18n.getLocalizedString("DBA05_Title");
    frmMBAssignAtmPinNew.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmMBAssignAtmPinNew.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("DBA05_PINTopic");
    if (("undefined" != typeof isConfirmPin) && !isConfirmPin) {
        frmMBAssignAtmPinNew.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("DBA05_PINTopic");
    } else {
        frmMBAssignAtmPinNew.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("DBA05_ConfirmPINTopic");
    }
}

function toggleConfirmPINMBNew(command) {
    frmMBAssignAtmPinNew.hbxAtmPinConfirm.setVisibility(command);
    if (!command) {
        frmMBAssignAtmPinNew.hbxAtmPin.top = "25.2%";
        frmMBAssignAtmPinNew.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("DBA05_PINTopic");
    } else {
        frmMBAssignAtmPinNew.hbxAtmPin.top = 0;
        frmMBAssignAtmPinNew.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("DBA05_ConfirmPINTopic");
    }
}

function deleteATMPINMBNew() {
    if (isConfirmPin) {
        if (isNotBlank(glbPin)) {
            decreaseATMPINValMBNew();
        } else {
            glbPin = glbPinArr[0];
            glbIndex = glbPinArr[0].length;
            glbPinArr = [];
            toggleConfirmPINMBNew(false);
            isConfirmPin = false;
            decreaseATMPINValMBNew();
        }
    } else {
        if (glbPinArr.length > 0 && glbPinArr[0].length == maxLenPin) {
            glbPin = glbPinArr[0];
            glbIndex = glbPinArr[0].length;
            glbPinArr = [];
        }
        if (isNotBlank(glbPin)) {
            decreaseATMPINValMBNew();
            toggleConfirmPINMBNew(false);
            isConfirmPin = false;
        }
    }
}

function decreaseATMPINValMBNew() {
    --glbIndex;
    glbPin = glbPin.substring(0, glbPin.length - 1);
    assignATMPINRenderMBNew("");
}

function renderPINMBNew() {
    var btnObject = [];
    if (isConfirmPin) {
        btnObject = [frmMBAssignAtmPinNew.imgPin1Confirm, frmMBAssignAtmPinNew.imgPin2Confirm, frmMBAssignAtmPinNew.imgPin3Confirm, frmMBAssignAtmPinNew.imgPin4Confirm];
    } else {
        btnObject = [frmMBAssignAtmPinNew.imgPin1, frmMBAssignAtmPinNew.imgPin2, frmMBAssignAtmPinNew.imgPin3, frmMBAssignAtmPinNew.imgPin4];
    }
    var emptyPinDis = "pin_input.png";
    var emptyPin = "pin_input_active.png";
    var fillPin = "pin_input_filled.png";
    for (var i = 0; i < glbPin.length; i++) {
        btnObject[i].src = fillPin;
    }
    for (var i = btnObject.length; i > glbPin.length; i--) {
        btnObject[i - 1].src = emptyPin;
        if (i > glbPin.length + 1) {
            btnObject[i - 1].src = emptyPinDis;
        }
    }
}

function assignATMPINRenderMBNew(pin) {
    if (isNotBlank(pin)) {
        if (isConfirmPin) {
            if (glbPin.length < maxLenPin) {
                addConfirmPinMBNew(pin, ++glbIndex);
            }
        } else {
            addPinMBNew(pin, ++glbIndex);
        }
    } else {
        renderPINMBNew();
    }
}

function addConfirmPinMBNew(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderPINMBNew();
    if (glbPin.length == maxLenPin) {
        verifyCardPinServiceForDebitCardActivation();
    }
}

function addPinMBNew(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderPINMBNew();
    if (glbPin.length == maxLenPin) {
        //toggleKeyPad(true);
        toggleConfirmPINMBNew(true);
        isConfirmPin = true;
        glbPinArr[0] = glbPin;
        glbPin = "";
        glbIndex = 0;
        renderPINMBNew();
    }
}

function initializeAndNavigatetoPinAssignNew() {
    clearPIN();
    frmMBAssignAtmPinNew.show();
}

function clearPIN() {
    initialParam();
    clearATMPINMBNew();
}

function resetPINwithInitOnlinePin() {
    initOnlineDebitPIN();
    clearPIN();
}

function clearATMPINMBNew() {
    glbPin = "";
    glbIndex = 0;
    glbPinArr = [];
    toggleConfirmPINMBNew(false);
    isConfirmPin = false;
    assignATMPINRenderMBNew("");
}

function init_frmMBAssignAtmPinNew_Function() {
    frmMBAssignAtmPinNew.btn0.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn1.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn2.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn3.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn4.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn5.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn6.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn7.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn8.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btn9.onClick = btnPinClick;
    frmMBAssignAtmPinNew.btnClr.onClick = btnPinClear;
    frmMBAssignAtmPinNew.btnDel.onClick = btnPinDelete;
    frmMBAssignAtmPinNew.btnCancel.onClick = btnCancelEvent;
    frmMBAssignAtmPinNew.btnMainMenu.onClick = handleMenuBtn;
}

function formatInputCreditCardNumberOld() {
    var output;
    var currVal = frmCardActivationDetails.txtCCNumbetInputThree.text;
    currVal = currVal.replace(/_/g, "");
    currVal = currVal.replace(/ /g, "");
    if (false) {
        output = insertSpace(currVal, parseInt(currVal.length)) + insertUnderScore(parseInt(currVal.length), 6);
        frmCardActivationDetails.txtCCNumbetInputThree.text = output;
        //return output;
    } else {
        onTextChangeCardNumber();
    }
}

function insertUnderScore(currLen, maxLen) {
    var val = "";
    var totalInsert = parseInt(maxLen) - parseInt(currLen);
    for (var i = 0; i < totalInsert; i++) {
        if ((parseInt(currLen) == 2 && i == 0) || (parseInt(currLen) == 0 && i == 2) || (parseInt(currLen) == 1 && i == 1)) {
            val = val + "  _";
        } else {
            val = val + " _";
        };
    }
    return val;
}

function insertSpace(currVal, currLen) {
    var val = "";
    for (var i = 0; i < currLen; i++) {
        if (i == currLen - 1) {
            val = val + currVal.substring(i, i + 1);
        } else if (i == 1 && currLen > 2) {
            val = val + currVal.substring(i, i + 1) + "  ";
        } else {
            val = val + currVal.substring(i, i + 1) + " ";
        }
    }
    return val;
}

function onTextChangeCitizenId() {
    var cardNoLen = replaceAll(frmCardActivationDetails.txtCititizenID.text, " ", "").length;
    if (parseInt(cardNoLen) == 13) {
        //alert("cardNoLen222"+cardNoLen)
        //showLoadingScreen();
        var lblText = kony.i18n.getLocalizedString("transPasswordSub");
        var refNo = "";
        var mobNO = "";
        //showLoadingScreen();
        //popupTractPwd.show();
        //	showOTPPopup(lblText, refNo, mobNO, showCardActivationComplete, 3);
        //showTxnPasswordPopupActivateDebitCreditCard();
        validateCitizenId(frmCardActivationDetails.txtCititizenID.text);
    }
}

function validateCardNumber() {
    var cardNo = frmCardActivationDetails.txtCCNumbetInputThree.text;
    if (cardNo != null && cardNo != "") {
        var len = cardNo.length;
        var lastChar = len.charAt(len - 1);
        if (!kony.string.isNumeric(lastChar)) {
            frmCardActivationDetails.txtCCNumbetInputThree.text = lastChar.substring(0, len - 1);
        }
    }
}

function goBackToPreviousPage() {
    if (gblManageCardFlow == "frmMBCardList") {
        frmMBCardList.show();
    } else if (gblManageCardFlow == "frmMBListDebitCard") {
        frmMBListDebitCard.show();
    }
}

function initOnlineDebitPIN() {
    showDismissLoadingMBIB(true);
    var inputParam = {};
    var status = invokeServiceSecureAsync("InitOnlineATMPIN", inputParam, initOnlineDebitPINCallBack);
}

function initOnlineDebitPINCallBack(status, resulttable) {
    if (status == 400) {
        showDismissLoadingMBIB(false);
        if (resulttable["opstatus"] == 0) {
            gblPk = resulttable["pubKey"];
            gblRand = resulttable["serverRandom"];
            gblAlgorithm = resulttable["hashAlgorithm"];
        } else {
            gblPk = "";
            gblRand = "";
            gblAlgorithm = "";
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        }
    }
}
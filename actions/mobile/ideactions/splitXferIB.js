/*************************************************************************
 	Module	: splitAmountIB
	Author  : Kony
	Purpose : spliting Amount
****************************************************************************/

function splitAmountIB(Amount) {
	var minFee;
	var maxFee;
	gblsplitAmt = [];
	gblsplitFee = [];
	var transORFTSplitAmnt = parseFloat(gblLimitORFTPerTransaction);
	var transSMARTSplitAmnt = parseFloat(gblLimitSMARTPerTransaction);
	
	var prodCode = gblcwselectedData.prodCode;
	var remainingFee = gblcwselectedData.remainingFee;
	
	remainingFee = parseInt(remainingFee);
	if (gblTrasORFT == 1) {
		minFee = parseInt(gblXerSplitData[0].ORFTSPlitFeeAmnt1);
		maxFee = parseInt(gblXerSplitData[0].ORFTSPlitFeeAmnt2);
		var orftRange1Lower = parseInt(gblXerSplitData[0].ORFTRange1Lower);
		var orftRange2Lower = parseInt(gblXerSplitData[0].ORFTRange2Lower);
		var orftRange1High = parseInt(gblXerSplitData[0].ORFTRange1Higher);
		var orftRange2High = parseInt(gblXerSplitData[0].ORFTRange2Higher);
		var SplitCnt = parseInt(Amount / transORFTSplitAmnt);
		
		var indexdot = Amount.indexOf(".");
		var decimal = "";
		var remAmt = "";
		if (indexdot > 0) {
			decimal = Amount.substr(indexdot, 3);
			Amount = Amount.substr(0, indexdot)
			remAmt = Amount % transORFTSplitAmnt;
			remAmt = remAmt + decimal;
			
		} else {
			remAmt = Amount % transORFTSplitAmnt;
			remAmt = remAmt + ".00";
		}
		for (var i = 0; i < SplitCnt; i++) {
			gblsplitAmt.push(transORFTSplitAmnt + ".00");
			if(gblORFT_ALL_FREE_TRANS_CODES.indexOf(prodCode) >= 0){
				gblsplitFee.push("0");
			}else if (gblORFT_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
				if (gblPaynow && remainingFee > 0) {
					gblsplitFee.push("0");
					remainingFee = remainingFee - 1;
				} else {
					if (transORFTSplitAmnt > orftRange1High)
						gblsplitFee.push(maxFee);
					else
						gblsplitFee.push(minFee);
				}
			} else {
				if (transORFTSplitAmnt > orftRange1High)
					gblsplitFee.push(maxFee);
				else
					gblsplitFee.push(minFee);
			}
		}
		if (remAmt > orftRange1Lower) {
			gblsplitAmt.push(remAmt);
			if (remAmt <= orftRange1High) {
				if(gblORFT_ALL_FREE_TRANS_CODES.indexOf(prodCode) >= 0){
					gblsplitFee.push("0");
				}else if (gblORFT_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(minFee);
				} else
					gblsplitFee.push(minFee);
			} else {
				if(gblORFT_ALL_FREE_TRANS_CODES.indexOf(prodCode) >= 0){
					gblsplitFee.push("0");
				}else if (gblORFT_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(maxFee);
				} else
					gblsplitFee.push(maxFee);
			}
		}
 	} else if (gblTransSMART == 1) {
 		minFee = parseInt(gblXerSplitData[0].SMARTSPlitFeeAmnt1);
		maxFee = parseInt(gblXerSplitData[0].SMARTSPlitFeeAmnt2);
		var smartRange1Lower = parseInt(gblXerSplitData[0].SMARTRange1Lower);
		var smartRange2Lower = parseInt(gblXerSplitData[0].SMARTRange2Lower);
		var smartRange1High = parseInt(gblXerSplitData[0].SMARTRange1Higher);
		var smartRange2High = parseInt(gblXerSplitData[0].SMARTRange2Higher);
		var SplitCnt = parseInt(Amount / transSMARTSplitAmnt);
 		var indexdot = Amount.indexOf(".");
		var decimal = "";
		var remAmt = "";
		if (indexdot > 0) {
			decimal = Amount.substr(indexdot, 3);
			Amount = Amount.substr(0, indexdot)
			remAmt = Amount % transSMARTSplitAmnt;
			remAmt = remAmt + decimal;
 		} else {
			remAmt = Amount % transSMARTSplitAmnt;
			remAmt = remAmt + ".00"
		}
		for (var i = 0; i < SplitCnt; i++) {
			gblsplitAmt.push(transSMARTSplitAmnt + ".00");
			if (gblSMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
				if (gblPaynow && remainingFee > 0) {
					gblsplitFee.push("0");
					remainingFee = remainingFee - 1;
				} else {
					if (transSMARTSplitAmnt > smartRange1High)
						gblsplitFee.push(maxFee);
					else
						gblsplitFee.push(minFee);
				}
			} else {
				if (gblALL_SMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					gblsplitFee.push("0");
				} else if (transSMARTSplitAmnt > smartRange1High) {
					gblsplitFee.push(maxFee);
				} else {
					gblsplitFee.push(minFee);
				}
			}
		}
		if (remAmt > smartRange1Lower) {
			gblsplitAmt.push(remAmt);
			if (remAmt <= smartRange1High) {
				if (gblSMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(minFee);
				} 
				else if (gblALL_SMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					gblsplitFee.push("0");
				} else {
					gblsplitFee.push(minFee);
				}
			} else {
				if (gblSMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					if (gblPaynow && remainingFee > 0) {
						gblsplitFee.push("0");
						remainingFee = remainingFee - 1;
					} else
						gblsplitFee.push(maxFee);
				} else if (gblALL_SMART_FREE_TRANS_CODES.indexOf(prodCode) >= 0) {
					gblsplitFee.push("0");
				} else {
					gblsplitFee.push(maxFee);
				}
			}
		}
		
	}
}

  

 

function xferIBORFT() {
	if (gblTrasORFT == 0) {
 		frmIBTranferLP.btnXferSMART.skin = btnFeeSmart;
		frmIBTranferLP.btnXferSMART.focusSkin = btnFeeSmart;
		frmIBTranferLP.btnXferORFT.skin = btnFeeOrftFocus;
		gblTrasORFT = gblTrasORFT + 1;
		gblTransSMART = 0;
	} else {
 		frmIBTranferLP.btnXferORFT.skin = btnFeeOrft;
		gblTrasORFT = 0;
	}
} //end of function

function xferIBSmart() {
	if (gblTransSMART == 0) {
 		frmIBTranferLP.btnXferSMART.skin = btnFeeSmartFocus;
		frmIBTranferLP.btnXferORFT.skin = btnFeeOrft;
		frmIBTranferLP.btnXferORFT.focusSkin = btnFeeOrft;
		gblTransSMART = gblTransSMART + 1;
		gblTrasORFT = 0;
	} else {
 		frmIBTranferLP.btnXferSMART.skin = btnFeeSmart;
		gblTransSMART = 0;
	}
} //end of function

function XferSMS() {
	var toRecipient = "";
	if(gblSelTransferMode == 1){
		toRecipient = frmIBTranferLP.lblXferToNameRcvd.text;
	}else if(gblSelTransferMode == 3 && frmIBTranferLP.hbxSelMobileRecipient.isVisible){
		toRecipient = frmIBTranferLP.lblXferToContactRcvdMobile.text;
	}else{
		toRecipient = "";
	}
	frmIBTranferLP.tbxEmail.text="";
	
	
	if (toRecipient.length == 0) {
		
 		frmIBTranferLP.hbxSMS.setVisibility(false);
 		frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
		//frmIBTranferLP.lineOne.setVisibility(false);
		frmIBTranferLP.lineTwo.setVisibility(false);
	}
	if ((gblTrasSMS == 0) && (toRecipient.length != 0)) {
		
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(true);
		frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
		frmIBTranferLP.lineOne.setVisibility(true);
		frmIBTranferLP.lineTwo.setVisibility(true);
		frmIBTranferLP.hbxSMS.setVisibility(true);
		frmIBTranferLP.hbxEmail.setVisibility(false);
		var smsMobile = onEditMobileNumberP2P(gblXferPhoneNo);
		if(smsMobile.length == 12){
			frmIBTranferLP.txtTransLndSmsNEmail.text = smsMobile;
		}else{
			frmIBTranferLP.txtTransLndSmsNEmail.text = "";
		}
		frmIBTranferLP.txtTransLndSmsNEmail.placeholder = kony.i18n.getLocalizedString("TransferMobileNo");
		frmIBTranferLP.btnXferSMS.skin = btnIBSMSfocusleft;
		//frmIBTranferLP.btnXferSMS.hoverSkin = btnIBSMSfocusleft;
		frmIBTranferLP.btnXferSMS.focusSkin = btnIBSMSfocusleft;
		frmIBTranferLP.btnXferEmail.skin = btnIBemailRoundedCorner;
		gblTrasSMS = gblTrasSMS + 1;
		gblTransEmail = 0;
		frmIBTranferLP.tbxXferNTR.text = "";
		frmIBTranferLP.textRecNoteEmail.text = "";		
	} else {
		frmIBTranferLP.hbxSMS.setVisibility(false);
		frmIBTranferLP.btnXferSMS.skin = btnIbSMSRoundedCorner;
		//frmIBTranferLP.btnXferSMS.skin = btnIBSMSfocusleft;
		frmIBTranferLP.btnXferSMS.focusSkin = btnIBSMSfocusleft;
		frmIBTranferLP.btnXferEmail.skin = btnIBemailRoundedCorner;
		frmIBTranferLP.txtTransLndSmsNEmail.text = "";
		frmIBTranferLP.tbxXferNTR.text = "";
		frmIBTranferLP.textRecNoteEmail.text = "";
		frmIBTranferLP.hbxEmail.setVisibility(false);
		frmIBTranferLP.lineTwo.setVisibility(false);
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
		frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
		//frmIBTranferLP.lineOne.setVisibility(false);
		gblTrasSMS = 0;
	}
} // end of SMS Function

function XferSMSP2P() {
	var toRecipient = "";
	if(frmIBTranferLP.hbxSelMobileRecipient.isVisible){
		toRecipient = frmIBTranferLP.lblXferToContactRcvdMobile.text;
	}
	if (toRecipient.length == 0) {
 		frmIBTranferLP.hbxSMS.setVisibility(false);
 		frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
		frmIBTranferLP.lineTwo.setVisibility(false);
	}
	if (gblTrasSMS == 0  && toRecipient.length != 0) {
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(true);
		frmIBTranferLP.lineThree.setVisibility(false);
		frmIBTranferLP.lineOne.setVisibility(true);
		frmIBTranferLP.lineTwo.setVisibility(true);
		frmIBTranferLP.hbxSMS.setVisibility(true);
		if(frmIBTranferLP.txtXferMobileNumber.text.length == 12){
			frmIBTranferLP.txtTransLndSmsNEmail.text = frmIBTranferLP.txtXferMobileNumber.text;
		}else{
			frmIBTranferLP.txtTransLndSmsNEmail.text = "";
		}
		frmIBTranferLP.txtTransLndSmsNEmail.placeholder = kony.i18n.getLocalizedString("TransferMobileNo");
		frmIBTranferLP.btnXferSMSP2P.skin = btnIBSMSBothRoundConerBlue;
		frmIBTranferLP.btnXferSMSP2P.focusSkin = btnIBSMSBothRoundConerBlue;
		gblTrasSMS = gblTrasSMS + 1;
		gblTransEmail = 0;
		frmIBTranferLP.tbxXferNTR.text = "";
		frmIBTranferLP.textRecNoteEmail.text = "";		
	} else {
		frmIBTranferLP.hbxSMS.setVisibility(false);
		frmIBTranferLP.btnXferSMSP2P.skin = btnIBSMSBothRoundConerGrey;
		frmIBTranferLP.btnXferSMSP2P.focusSkin = btnIBSMSBothRoundConerBlue;
		frmIBTranferLP.tbxXferNTR.text = "";
		frmIBTranferLP.textRecNoteEmail.text = "";		
		frmIBTranferLP.lineTwo.setVisibility(false);
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
		frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
		gblTrasSMS = 0;
	}
} // end of SMS Function

function XferEMAIL() {
	var toRecipient = "";
	if(gblSelTransferMode == 1){
		toRecipient = frmIBTranferLP.lblXferToNameRcvd.text;
	}else if(gblSelTransferMode == 3 && frmIBTranferLP.hbxSelMobileRecipient.isVisible){
		toRecipient = frmIBTranferLP.lblXferToContactRcvdMobile.text;
	}else{
		toRecipient = "";
	}
	frmIBTranferLP.txtTransLndSmsNEmail.text=""
	
	if (toRecipient.length == 0) {
		
 		frmIBTranferLP.hbxSMS.setVisibility(false);
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
		frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
		//frmIBTranferLP.lineOne.setVisibility(false);
		frmIBTranferLP.lineTwo.setVisibility(false);
	}
	if ((gblTransEmail == 0 || gblTrasSMS == 1) && (toRecipient.length != 0)) {
		
		frmIBTranferLP.hbxEmail.setVisibility(true);
		frmIBTranferLP.hbxSMS.setVisibility(false);
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
		frmIBTranferLP.hbxRecNoteEmail.setVisibility(true);
		//frmIBTranferLP.lineOne.setVisibility(true);
		frmIBTranferLP.lineTwo.setVisibility(true);
		frmIBTranferLP.tbxEmail.text = gblXferEmail;
		frmIBTranferLP.tbxEmail.placeholder = kony.i18n.getLocalizedString("transferEmailID");
		frmIBTranferLP.btnXferEmail.skin = btnIBEmailRgtFocus;
 		frmIBTranferLP.btnXferEmail.focusSkin = btnIBEmailRgtFocus;
		frmIBTranferLP.btnXferSMS.skin = btnIbSMSRoundedCorner	
		gblTransEmail = gblTransEmail + 1
		gblTrasSMS = 0;
		frmIBTranferLP.tbxXferNTR.text="";
		frmIBTranferLP.textRecNoteEmail.text="";
	} else {
		
		frmIBTranferLP.txtTransLndSmsNEmail.text = ""
		frmIBTranferLP.tbxEmail.text="";
		frmIBTranferLP.tbxXferNTR.text="";
		frmIBTranferLP.textRecNoteEmail.text="";
		frmIBTranferLP.btnXferEmail.skin = btnIBemailRoundedCorner;
		//frmIBTranferLP.btnXferEmail.skin = btnIBEmailRgtFocus;
		frmIBTranferLP.btnXferEmail.focusSkin = btnIBEmailRgtFocus;
		frmIBTranferLP.btnXferSMS.skin = btnIbSMSRoundedCorner;
		frmIBTranferLP.hbxSMS.setVisibility(false);
		frmIBTranferLP.hbxEmail.setVisibility(false);
		frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
		frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
		//frmIBTranferLP.lineOne.setVisibility(false);
		frmIBTranferLP.lineTwo.setVisibility(false);
		gblTransEmail = 0;
	}
} // end of Email Function

 
 

/*************************************************************************

	Module	: onTransferLndngNextIB
	Author  : Kony
	Purpose : Onclick of Confirm button in TranferLP

***************************************************************************/
function onTransferLndngNextIB() {
	gblSplitAckImg = [];
	gblsplitAmt = [];
		/** check for Empty Fields on Tranfer LP ****/
		if (!validateTransferFieldsIB()) {
			return false;
		}
		/** check for max transfer limits for ORFT & SMART ****/
		
			//if (!checkMaxLimitForTransferIB()) {
			//	return false;
			//}
	gblNofeeVarIB="";
	var availableBal;
	if (gblcwselectedData == 0) {
		var fromData = gblcwselectedData;
		availableBal = fromData[0].lblBalance;
	} else {
		availableBal = gblcwselectedData.lblBalance; //To be retrived from service
	}
	gblNofeeVarIB = gblcwselectedData.prodCode;
	availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	availableBal = availableBal.replace(/,/g, "");
	availableBal = parseFloat(availableBal.trim());
	var enteredAmount = frmIBTranferLP.txbXferAmountRcvd.text;
		enteredAmount =  enteredAmount.replace(/,/g, "");
		enteredAmount=parseFloat(enteredAmount.trim());
	var fee ="0";
    if (gblisTMB == gblTMBBankCD) {
		inputParam["gblisTMB"]=gblTransSMART;
		inputParam["gblTMBBankCD"]=gblTransSMART;
	} else {
			if (gblTransSMART == 1) {
 				fee = frmIBTranferLP.btnXferSMART.text;
				var indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
				fee = fee.substring(0, indexOfB);
			} else if (gblTrasORFT == 1) {
			   	fee = frmIBTranferLP.btnXferORFT.text;
				var indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
				fee = fee.substring(0, indexOfB);
			} else {
				fee = "0";
			}
		}
	fee = parseFloat(fee.trim());
	
	//ENH_129 SMART Transfer Add Date & Time
	if (gblTransSMART == 1 && gblPaynow) {
		frmIBTransferNowConfirmation.lblSmartTrfrDateVal.text = smartDate;
		frmIBTransferNowCompletion.lblSmartTrfrDateVal.text = smartDate;
			
		frmIBTransferNowConfirmation.hbxSmartTrfrDate.setVisibility(true);
		frmIBTransferNowCompletion.hbxSmartTrfrDate.setVisibility(true);
		
		frmIBTransferNowConfirmation.lblSmartDateSplitVal.text = smartDate;
		frmIBTransferNowCompletion.lblSmartDateSplitVal.text = smartDate;
		
		frmIBTransferNowConfirmation.hbxSmartDateSplit.setVisibility(true);
		frmIBTransferNowCompletion.hbxSmartDateSplit.setVisibility(true);
		
		frmIBTransferNowCompletion.hbox101458964925304.skin="hboxLightGrey320px";
		frmIBTransferNowCompletion.hbxTransNotif.skin="hbox320pxpadding";
		frmIBTransferNowCompletion.hbxTransNtr.skin="hboxLightGrey320px";
		frmIBTransferNowCompletion.hbox101458964925306.skin="hbox320pxpadding";
	} else {
		frmIBTransferNowConfirmation.lblSmartTrfrDateVal.text = "";
		frmIBTransferNowCompletion.lblSmartTrfrDateVal.text = "";
		
		frmIBTransferNowConfirmation.hbxSmartTrfrDate.setVisibility(false);
		frmIBTransferNowCompletion.hbxSmartTrfrDate.setVisibility(false);
		
		frmIBTransferNowConfirmation.lblSmartDateSplitVal.text = "";
		frmIBTransferNowCompletion.lblSmartDateSplitVal.text = "";
		
		frmIBTransferNowConfirmation.hbxSmartDateSplit.setVisibility(false);
		frmIBTransferNowCompletion.hbxSmartDateSplit.setVisibility(false);
		
		frmIBTransferNowCompletion.hbox101458964925304.skin="hbox320pxpadding";
		frmIBTransferNowCompletion.hbxTransNotif.skin="hboxLightGrey320px";
		frmIBTransferNowCompletion.hbxTransNtr.skin="hbox320pxpadding";
		frmIBTransferNowCompletion.hbox101458964925306.skin="hboxLightGrey320px";
	}
	
	if ((availableBal < (enteredAmount+fee)) && gblPaynow) {
		alert(kony.i18n.getLocalizedString("keyenteredamountexceedsavailablebalance"));
		return false;
		}		
		//For TD Transfer below hboxs are enable true otherwisr false DEF 5007
		frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfIntAmt.setVisibility(false);
		frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfTaxAmt.setVisibility(false);
		frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfPenaltyAmt.setVisibility(false);
		frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfNetAmt.setVisibility(false);
		frmIBTransferNowConfirmation.hbxScheduleDetails.setVisibility(false)
		
		frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpIntAmt.setVisibility(false);
		frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpTaxAmt.setVisibility(false);
		frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpPenaltyAmt.setVisibility(false);
		frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpNetAmt.setVisibility(false);
		frmIBTransferNowCompletion.hbxScheduleDetails.setVisibility(false);
		
		gblTDDateFlag=false;
		
		if(gblshotcutToACC == false) {
			var accName="";
			if(recipientAddFromTransfer || gblTransferFromRecipient){
				if(gblTransferFromRecipient){
					accName=accountData["lblAccountName"];
				}	
				else{
					accName=frmIBMyReceipentsAddContactManually.segmentRcAccounts.data[0]["lblAccountName"].trim();
				}	
				
			}else
			{
					accName = gblSmartAccountName;
			}
			if(accName ==null || accName =="null" || accName == undefined || accName =="NONE" || accName == "NA")
					accName="";
			frmIBTransferNowConfirmation.lblXferToAccTyp.text=accName.trim();
			frmIBTransferNowCompletion.lblXferToAccTyp.text=accName.trim();
		}
		if(gblPaynow){
			frmIBTransferNowConfirmation.hbxScheduleDetails.setVisibility(false);
			frmIBTransferNowCompletion.hbxScheduleDetails.setVisibility(false);
		}
		if(!gblPaynow && gblTransSMART == 1){
			var dateTemp = GBL_SMART_DATE_FT;
			dateTemp = GBL_SMART_DATE_FT.split("/");
			
			// wirting the specfic code here to the limitation of IE8 browser for parse int
			//parsed as an octal number. Use the radix parameter in parseInt
			var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
			if(isIE8)
			{
				var monthTemp = parseInt(dateTemp[1],10)
			}
			else
			{
        		var monthTemp = parseInt(dateTemp[1])
        	}
			var d2 = new Date(dateTemp[2],monthTemp-1,dateTemp[0])
			if(d2.getDay() == 6 || d2.getDay() == 0){
				alert(kony.i18n.getLocalizedString("keyFTSMARTOnWeekend"));
				return;
			}
			var dateToSend = dateTemp[2]+"/"+dateTemp[1]+"/"+dateTemp[0];
			srvHolydayChkFutureTransSet(dateToSend);
		}else{
			checkCrmProfileInqIB();
			//transferTokenExchangeService();
		}
		
		/* added code for Token Switching by revanth */
		/*
		if(gblSwitchToken == false && gblTokenSwitchFlag == false){
			checkTokenFlag();
		} */
		
		/*
			
			#DEFECT MIB-625 Transfer to own TMB accounts system should not require OTP from token
		
		*/
		if(gblTransEmail == "1" && gblTrasSMS == "1" && gblisTMB == gblTMBBankCD){
				frmIBTransferNowConfirmation.hbxToken.setVisibility(false);
				frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(false);
		}else{
		
			if(gblTokenSwitchFlag && gblSwitchToken == false){
			
				frmIBTransferNowConfirmation.hbxToken.setVisibility(true);
				frmIBTransferNowConfirmation.tbxToken.setFocus(true);
				frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(false);
			} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
				frmIBTransferNowConfirmation.hbxToken.setVisibility(false);
				frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(true);
			}
		}
		frmIBTransferNowConfirmation.hbxOtpBox.setVisibility(false);
		/* End of TokenSwitching Code*/
		
		gblsplitAmt = [];
		gblsplitFee = [];
		gblSplitAckImg = [];
		gblSplitCnt = 0;
		gblSplitStatusInfo = [];
/*		var activityTypeID = ""
		var activityFlexValues5 = "";
		var activityFlexValues1 = "";
		var fee = ""
		if (gblisTMB == gblTMBBankCD) {
			activityTypeID = "023"
			activityFlexValues5 = ""
			fee = 0;
		} else {
			if (gblTransSMART == 1) {
				activityTypeID = "025"
				activityFlexValues5 = "SMART"
				fee = frmIBTranferLP.btnXferSMART.text;
				indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
				fee = fee.substring(0, indexOfB);
			} else if (gblTrasORFT == 1) {
				activityTypeID = "025"
				activityFlexValues5 = "ORFT"
				fee = frmIBTranferLP.btnXferORFT.text;
				indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
				fee = fee.substring(0, indexOfB);
			} else {
				activityTypeID = "024"
				activityFlexValues5 = ""
				fee = 0
			}
		}  */
		if((gblTransEmail == "0" && gblTrasSMS == "0")||(gblTransEmail == "1" && gblTrasSMS == "1")){
		
			frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
			frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
			frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);
			frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
			frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(false);
			frmIBTransferNowConfirmation.hbxSplitNtr.setVisibility(false);
			frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
			frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);
			frmIBTransferNowConfirmation.hbox101458964925294.setVisibility(true);
	        frmIBTransferNowCompletion.hbox101458964925304.setVisibility(true);
			
		
		}else{
			frmIBTransferNowCompletion.hbxTransNotif.setVisibility(true);
			frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(true);
			frmIBTransferNowCompletion.hbxTransNtr.setVisibility(true);
			frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(true);	
			frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(true);	
			frmIBTransferNowConfirmation.hbxSplitNtr.setVisibility(true);
			frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(true);
			frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(true);
			frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(true);
		}		
/*		var frmID = frmIBTranferLP.lblTranLandFromNum.text;
		activityFlexValues1 = replaceCommon(frmID, "-", "");
		var toNum = frmIBTranferLP.lblXferToContactRcvd.text;
		toNum = replaceCommon(toNum, "-", "");
		var activityFlexValues2 = toNum ;
		var activityFlexValues3= getBankShortNameTransferMB(gblBANKREF);
		var amt=replaceCommon(frmIBTranferLP.txbXferAmountRcvd.text, ",", "");
		amt = parseFloat(amt);
		fee = parseFloat(fee);
		var activityFlexValues4 = amt +"+"+ fee;
		if(!gblPaynow){
			activityTypeID = "026";
			activityFlexValues1 = replaceCommon(frmID, "-", "");
			activityFlexValues2 = toNum = replaceCommon(toNum, "-", "");
			activityFlexValues3 = getBankShortNameTransferMB(gblBANKREF);
			activityFlexValues4 = amt +"+"+ fee;
		}
	*/
		//activityLogServiceCall(activityTypeID, "", "00", "", activityFlexValues1, activityFlexValues2, activityFlexValues3,	activityFlexValues4, activityFlexValues5, "")

}
	function checkTokenFlag() {
		var inputParam = [];
 		showLoadingScreenPopup();
		invokeServiceSecureAsync("tokenSwitching", inputParam, ibTokenFlagCallbackfunction);
	}
	function ibTokenFlagCallbackfunction(status,callbackResponse){
			if(status==400){
					if(callbackResponse["opstatus"] == 0){
						//dismissLoadingScreenPopup();
						
						if(callbackResponse["deviceFlag"].length==0){
							
							//return
						}
						if ( callbackResponse["deviceFlag"].length > 0 ){
								var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
								var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
								var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
								
								if ( tokenFlag=="Y" && ( mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02'){
 									gblTokenSwitchFlag = true;
								} else {
 									gblTokenSwitchFlag = false;
 									gblSwitchToken=true;
								}
							}
							onClickPreConfirmTrans();
						}
						 else {
						 	dismissLoadingScreenPopup();
						 	frmIBTransferNowConfirmation.hbxToken.setVisibility(false);
							frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(true);
							gblTokenSwitchFlag = false;
						 }
						 
						 
			}
			
	} 
/*************************************************************************

	Module	: checkTransferTypeIB
	Author  : Kony
	Purpose : checking for whether transfer type is split or normal

***************************************************************************/

function checkTransferTypeIB() {
	dismissLoadingScreenPopup();
	var transORFTSplitAmnt = parseFloat(gblTransORFTSplitAmnt)
	var transSMARTSplitAmnt = parseFloat(gblTransSMARTSplitAmnt);
 	var transferAmt = frmIBTranferLP.txbXferAmountRcvd.text + "";
 	transferAmt= replaceCommon(transferAmt, ",", "");
	if (gblisTMB != gblTMBBankCD && transferAmt > transORFTSplitAmnt && gblTrasORFT == 1) {
		splitAmountIB(transferAmt);
		frmIBTransferNowConfirmation.hbxSplitXfer.setVisibility(true);
		frmIBTransferNowConfirmation.hbxSplitMN.setVisibility(true);
		frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(true);
		frmIBTransferNowCompletion.hbxSplitMN.setVisibility(true);
		//DEF1176 fix
        frmIBTransferNowConfirmation.hbox101458964925296.setVisibility(false);
        frmIBTransferNowCompletion.hbox101458964925306.setVisibility(false);
        frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
		frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);
		frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
		frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);
		popSplitTransfers.show();
	} else if (gblisTMB != gblTMBBankCD && transferAmt > transSMARTSplitAmnt && gblTransSMART == 1) {
		splitAmountIB(transferAmt);
		frmIBTransferNowConfirmation.hbxSplitXfer.setVisibility(true);
		frmIBTransferNowConfirmation.hbxSplitMN.setVisibility(true);
		frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(true);
		frmIBTransferNowCompletion.hbxSplitMN.setVisibility(true);
		//DEF1176 fix
        frmIBTransferNowConfirmation.hbox101458964925296.setVisibility(false);
        frmIBTransferNowCompletion.hbox101458964925306.setVisibility(false);
        frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
		frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);
		frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
		frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);
		popSplitTransfers.show();
 	} 
 	else {
 		frmIBTransferNowConfirmation.hbxSplitMN.setVisibility(false);
 		frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(false);
 		frmIBTransferNowCompletion.hbxSplitMN.setVisibility(false);
 		//DEF1176 fix
        frmIBTransferNowConfirmation.hbox101458964925296.setVisibility(true);
        frmIBTransferNowCompletion.hbox101458964925306.setVisibility(true);
		normalTransactionIB();
 	}
}
/*************************************************************************

	Module	: validateTransferFields
	Author  : Kony
	Purpose : validation on Tranfer Fields

***************************************************************************/
function validateTransferFieldsIB() {
     if (frmIBTranferLP.lblXferToNameRcvd.text == "") {
        alert(kony.i18n.getLocalizedString("keyPleaseSelectToAccount"));
         return false;
    }
    
    if(frmIBTranferLP.hbxMaturity.isVisible){
    	if(frmIBTranferLP.cmbxMaturityDetails.selectedkey == "a,b,0"){
   		 	alert(kony.i18n.getLocalizedString("keySelectMaturityDate"));
			return false;
		}
    }
    if (frmIBTranferLP.txbXferAmountRcvd.text == "") {
        alert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"))
        return false;
    }
    //MIB-4884-Allow special characters for My Note and Note to recipient field
	/*
    if (isNotBlank(frmIBTranferLP.txtArMn.text)){
		if(!MyNoteValid(frmIBTranferLP.txtArMn.text)){		
			alert(kony.i18n.getLocalizedString("MIB_TRkeyMynoteInvalid"));			
			return false;
		}		
	}
	*/
	if (isNotBlank(frmIBTranferLP.txtArMn.text)){
		if(checkSpecialCharMyNote(frmIBTranferLP.txtArMn.text)){		
			alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
			frmIBTranferLP.txtArMn.setFocus(true);			
			return false;
		}		
	}
    if (gblisTMB != gblTMBBankCD && gblTrasORFT == 0 && gblTransSMART == 0) {
        alert(kony.i18n.getLocalizedString("keyPleaseSelectTransferType"));
        return false;
    }
    
    if(!gblPaynow &&  gblTransSMART == true && DailyClicked == true){
	   alert(kony.i18n.getLocalizedString("keyFTSmartDailyNotAllowed"))
       return false;
	}
     if(frmIBTranferLP.hbxSMS.isVisible == true && frmIBTranferLP.hbxEmail.isVisible == true ){
        if (frmIBTranferLP.txtTransLndSmsNEmail.text == "" && frmIBTranferLP.tbxEmail.text == "") {
            if (gblTransEmail == 1 && gblTrasSMS == 1) {
                return true;
            } else if (gblTransEmail == 1 || gblTrasSMS == 1) {
                alert(kony.i18n.getLocalizedString("keyPleaseEnterMobileNumEmail"))
                return false
            }
        }
	}else{
        var emailNSms="";
        if (gblTransEmail == 1) {
            if (gblTrasSMS == 1) {
                return true
            } else {
               //DEF1103 Fix
                if (frmIBTranferLP.tbxEmail.text == null || frmIBTranferLP.tbxEmail.text == ""){
                   alert(kony.i18n.getLocalizedString("transferEmailID"));
                   frmIBTranferLP.tbxEmail.text = "";
                   return false
                  }
                 
                var isValidEmail = validateEmail(frmIBTranferLP.tbxEmail.text);
                emailNSms = kony.i18n.getLocalizedString("email") + " [" + frmIBTranferLP.tbxEmail.text + "]";
                if (isValidEmail == false) {
                    alert(kony.i18n.getLocalizedString("keyPleaseEnterValidEMAILID"));
                    frmIBTranferLP.tbxEmail.text = "";
                    return false
                }
                if (frmIBTranferLP.textRecNoteEmail.text == "") {
                    alert(kony.i18n.getLocalizedString("keyPleaseEnterRcipientNote"))
                    return false;
                }
                //MIB-4884-Allow special characters for My Note and Note to recipient field
				/*
                if (isNotBlank(frmIBTranferLP.textRecNoteEmail.text)){
					if(!MyNoteValid(frmIBTranferLP.textRecNoteEmail.text)){		
						alert(kony.i18n.getLocalizedString("MIB_TRkeyRecNoteInvalid"));
						return false;
					}		
				}
				*/
				if (isNotBlank(frmIBTranferLP.textRecNoteEmail.text)){
					if(checkSpecialCharMyNote(frmIBTranferLP.textRecNoteEmail.text)){		
						alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
						frmIBTranferLP.textRecNoteEmail.setFocus(true);
						return false;
					}		
				}
                frmIBTransferNowConfirmation.lblTranNotifyModeVal.text = emailNSms;
                frmIBTransferNowConfirmation.lblSplitModTranVal.text = emailNSms;
            }
        } else if (gblTrasSMS == 1) {
            if (gblTransEmail == 1) {
                return true
            } else {
                emailNSms = frmIBTranferLP.txtTransLndSmsNEmail.text;
		        if(emailNSms.length== 0){
		          	alert(kony.i18n.getLocalizedString("keyEnterMobileNum"));
		          	return false;
				}else{
					if (!recipientMobileVal(removeHyphenIB(emailNSms))){
						alert(kony.i18n.getLocalizedString("keyEnteredMobileNumberisnotvalid"));
						return false;
					}
				}
                if (frmIBTranferLP.tbxXferNTR.text == "") {
                    alert(kony.i18n.getLocalizedString("keyPleaseEnterRcipientNote"))
                    return false;
                }
                //MIB-4884-Allow special characters for My Note and Note to recipient field
				/*
                if (isNotBlank(frmIBTranferLP.tbxXferNTR.text)){
					if(!MyNoteValid(frmIBTranferLP.tbxXferNTR.text)){		
						alert(kony.i18n.getLocalizedString("MIB_TRkeyRecNoteInvalid"));
						return false;
					}		
				} 
				*/  
				if (isNotBlank(frmIBTranferLP.tbxXferNTR.text)){
					if(checkSpecialCharMyNote(frmIBTranferLP.tbxXferNTR.text)){		
						alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
						frmIBTranferLP.tbxXferNTR.setFocus(true);
						return false;
					}		
				}              				
            }
            var strMobNo = removeHyphenIB(emailNSms);
		    frmIBTransferNowConfirmation.lblTranNotifyModeVal.text = kony.i18n.getLocalizedString("SMS") + " [xxx-xxx-"+ strMobNo.substring(6, 10) +"]";
			frmIBTransferNowConfirmation.lblSplitModTranVal.text = emailNSms;
        } //end of else if
        return true;
    }
    return true;
}

/*************************************************************************

	Module	: CheckMaxLimitForTransfer
	Author  : Kony
	Purpose : validate the Max Transfer Amnt to Other Banks

***************************************************************************/
function checkMaxLimitForTransferIB(transferAmt) {
	var maxTransferORFT = parseFloat(gblMaxTransferORFT).toFixed(2);
	var maxTransferSMART = parseFloat(gblMaxTransferSMART).toFixed(2);
	var transferAmt = parseFloat(frmIBTranferLP.txbXferAmountRcvd.text.replace(/,/g, ''));
	
	if (gblisTMB != gblTMBBankCD && transferAmt > maxTransferORFT && gblTrasORFT == 1) {
		var megaExceedLimitMsg =  kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
		megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}",  commaFormatted(maxTransferORFT+""));	
		alert(megaExceedLimitMsg);
		return false;
	}
	if (gblisTMB != gblTMBBankCD && transferAmt > maxTransferSMART && gblTransSMART == 1) {
		var megaExceedLimitMsg =  kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
		megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}",  commaFormatted(maxTransferSMART+""));	
		alert(megaExceedLimitMsg);
		return false;
	}
	
	return true;
}
/*
************************************************************************

	Module	: onClickConfirmTransferIB
	Author  : Kony
	Purpose : Onclick of Confirm button in TransferNowConfirmation
	
****/

function onClickConfirmTransferIB() {
	gblshotcutToACC = false;
	if(frmIBTransferNowConfirmation.hbxOtpBox.isVisible){
		
		if (frmIBTransferNowConfirmation.hbxOTPEntry.isVisible == true && frmIBTransferNowConfirmation.txtBxOTP.text == "") {
	 		alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
			frmIBTransferNowConfirmation.hbxToken.setVisibility(false);
			return false;
		} else if (frmIBTransferNowConfirmation.hbxToken.isVisible == true && frmIBTransferNowConfirmation.tbxToken.text == "" ){
	 			alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
				return false;
		}else {
			gblVerifyOTP = gblVerifyOTP + 1
		}
	}
	
	checkVerifyOTPIB();
}

function verifyTransferIB() {
    var inputParam = {};
    inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    if (gblTokenSwitchFlag == true) {
        inputParam["password"] = frmIBTransferNowConfirmation.tbxToken.text;
        inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    } else {
        inputParam["password"] = frmIBTransferNowConfirmation.txtBxOTP.text;
        inputParam["retryCounterVerifyOTP"] = gblVerifyOTP;
    }
    //future transfer
    var fromAcctID = frmIBTranferLP.lblTranLandFromNum.text;
    var transferAmt = frmIBTranferLP.txbXferAmountRcvd.text + ""
    transferAmt=replaceCommon(transferAmt, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    transferAmt = transferAmt.replace(/,/g, "");
    
    if (kony.string.containsChars(transferAmt, ["."])) transferAmt = transferAmt;
    else transferAmt = transferAmt + ".00";
    var frmAcct = fromAcctID.replace(/-/g, "");
    if (frmAcct.length == 14) {
        frmAcct = frmAcct.substring(4, 14);
    }
    
    var toAcctid = gblp2pAccountNumber;
    var toAcct = toAcctid.replace(/-/g, "");
    var frequency = frmIBTransferNowConfirmation.lblRepeatValue.text;
    //inputParam["freq"] = frequency;
    
    if (frequency == undefined || frequency == null) {
        frequency = "once";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
        inputParam["freq"] = "Daily";
    } else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
        inputParam["freq"] = "Weekly";
    } else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
        inputParam["freq"] = "Monthly";
    } else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
        inputParam["freq"] = "Annually";
    } else {
        inputParam["freq"] = "once";
    }
   	if (times == "1") {
		inputParam["NumInsts"] = frmIBTransferNowConfirmation.lblExecuteValue.text;
	} else if (times == "2") {
		inputParam["FinalDueDt"] = changeDateFormatForService(frmIBTransferNowConfirmation.lblEnDONValue.text);
	} else if (times == "0"){
		//inputParam["NumInsts"] = "99";
	}
    var fourthDigit;	
	if(frmAcct.length ==10 )
	  fourthDigit = frmAcct.charAt(3);
	else
	  fourthDigit = frmAcct.charAt(7);
 
 	 if (fourthDigit == "3") {
 	  	var comboDataKey = frmIBTranferLP.cmbxMaturityDetails.selectedKey;
		var dateaAmount=comboDataKey.split(",");
		var selTD = dateaAmount[2];//frmIBTranferLP.cmbxMaturityDetails.selectedKey;
		var depositeNo = gblTDDepositNo[selTD - 1];
		inputParam["depositeNo"] = depositeNo;
  	}	
	
	if(!gblPaynow){
	
		var splitResult=[];
		//if split transfer 
		if(frmIBTransferNowConfirmation.hbxSplitXfer.isVisible == true){
		    	var segData = frmIBTransferNowConfirmation.segSplitDet.data;
				for (i = 0; i < segData.length; i++) {
					var splitAmtVal = commaFormatted(parseFloat(removeCommos(segData[i]["lblSplitAmtVal"])).toFixed(2));
					var splitFeeVal = commaFormatted(parseFloat(removeCommos(segData[i]["lblSplitFeeVal"])).toFixed(2));
					splitAmtVal = splitAmtVal.replace(/,/g, "");
					splitFeeVal = splitFeeVal.replace(/,/g, "");
					
					var tempRec = {
						"amount" : commaFormatted(splitAmtVal) + kony.i18n.getLocalizedString("currencyThaiBaht"),
						"fee" : commaFormatted(splitFeeVal) + kony.i18n.getLocalizedString("currencyThaiBaht"),
						"transRefId" : segData[i]["lblSplitTRNVal"]
					}
					splitResult.push(tempRec);
				}
				inputParam["totalAmount"]=commaFormatted(parseFloat(removeCommos(frmIBTransferNowConfirmation.lblSplitTotAmtVal.text)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht");
				inputParam["totalFee"]=commaFormatted(parseFloat(removeCommos(frmIBTransferNowConfirmation.lblSplitTotFeeVal.text)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht");
				inputParam["splitResult"]=JSON.stringify(splitResult);
				//inputParam["myNote"]=frmIBTransferNowConfirmation.lblSplitMNVal.text;
				//inputParam["noteToRecipient"]=frmIBTransferNowConfirmation.lblSplitNTRVal.text;
				//inputParam["bankName"]=frmIBTransferNowConfirmation.lblSplitToBankNameVal.text;
			}
		    //if normal transfer
			else {
				var tempRec = {
						"amount" :commaFormatted(parseFloat(removeCommos(frmIBTransferNowConfirmation.lblAmtVal.text)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht"),
						"fee" :commaFormatted(parseFloat(removeCommos(frmIBTransferNowConfirmation.lblFeeVal.text)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht"),
						"transRefId" : frmIBTransferNowConfirmation.lblTRNVal.text
					}
					splitResult.push(tempRec);
					inputParam["splitResult"]=JSON.stringify(splitResult);
			}
	
		
		var transferSchedule = "";
	    if(frmIBTransferNowConfirmation.lblRepeatValue.text == "Once"){
	        transferSchedule = frmIBTransferNowConfirmation.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBTransferNowConfirmation.lblRepeatValue.text;
	      }else if(frmIBTransferNowConfirmation.lblExecuteValue.text == "-"){
	            transferSchedule = frmIBTransferNowConfirmation.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBTransferNowConfirmation.lblRepeatValue.text;
	      } else{
	             transferSchedule = frmIBTransferNowConfirmation.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmIBTransferNowConfirmation.lblEnDONValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBTransferNowConfirmation.lblRepeatValue.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmIBTransferNowConfirmation.lblExecuteValue.text+" "+kony.i18n.getLocalizedString("keyTimesIB");
	      }
		inputParam["PaymentSchedule"] = transferSchedule;
	}
	
    var fromFIIdent = gblcwselectedData.lblDummy;
    inputParam["fromAcctNo"] = frmAcct; //future
    inputParam["toAcctNo"] = toAcct; //future
	if(!gblPaynow) {
	    inputParam["dueDate"] = changeDateFormatForService(frmIBTransferNowConfirmation.lblStartOnValue.text); //future
	} else if(gblTransSMART == 1){
		//ENH_129 SMART Transfer Add Date & Time
		inputParam["dueDate"]=frmIBTransferNowConfirmation.lblSmartTrfrDateVal.text;
	}
	if(gblSelTransferMode == 3 || gblSelTransferMode == 2){
		inputParam["merchantNo"] = removeHyphenIB(frmIBTransferNowConfirmation.lblXferAccNo.text);
	}
	inputParam["dueDateForEmail"]=frmIBTransferNowConfirmation.lblStartOnValue.text;
    inputParam["memo"] = frmIBTranferLP.txtArMn.text; //future
    inputParam["transferAmt"] = transferAmt; //future
    inputParam["gblBANKREF"] = gblBANKREF; //future
    inputParam["gblTrasSMS"] = gblTrasSMS; //future
    inputParam["gblTransEmail"] = gblTransEmail; //future
    inputParam["gblTransSMART"] = gblTransSMART; //future
    inputParam["gblTrasORFT"] = gblTrasORFT; //future
    inputParam["gblisTMB"] = gblisTMB; //future
    inputParam["toAcctBank"] = gblBANKREF;
    inputParam["bankShortName"]=getBankShortName(gblisTMB); // to fix issue of blank or incorrect bank name in Note to recipient for Any ID Xfer using mobile number. 
    inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag; //future
    inputParam["gblPaynow"] = gblPaynow; //future
    inputParam["recipientEmailAddr"] = frmIBTranferLP.tbxEmail.text; //future
    inputParam["recipientMobileNbr"] = removeHyphenIB(frmIBTranferLP.txtTransLndSmsNEmail.text); //future
    inputParam["recipientMobile"] = removeHyphenIB(frmIBTranferLP.txtTransLndSmsNEmail.text); //future
    inputParam["receipientNote"] = frmIBTranferLP.tbxXferNTR.text; //future
	if(gblTrasSMS ==0 && gblTransEmail==1){
	    inputParam["receipientNote"] = frmIBTranferLP.textRecNoteEmail.text;
	}
	inputParam["RecipentName"]=gblSelectedRecipentName;
    inputParam["times"] = times;
//    inputParam["toFIIdent"] = fiident; //billPment		
    inputParam["frmFiident"] = fromFIIdent; //billPment	
    inputParam["toFIIdent"] = gblisTMB; //orft 		
    inputParam["acctTitle"] = frmIBTransferNowConfirmation.lblXferToName.text //orft 
    inputParam["transferDate"] = gblTransferDate; //TransferAdd	
    inputParam["transferFee"] = frmIBTransferNowConfirmation.lblFeeVal.text;
    var toNickName = frmIBTransferNowConfirmation.lblXferToName.text + "";
    var enterMobNo = "";
    //if(!frmIBTransferNowConfirmation.lblXferToName.isVisible && gblSelTransferMode == 2){
	if(gblSelTransferMode == 2){ // Always send Mobile Number
    	toNickName = kony.i18n.getLocalizedString("MIB_P2PMob"); 
    	enterMobNo = "yes";
    }else if(gblSelTransferMode == 3){
    	toNickName = kony.i18n.getLocalizedString("MIB_P2PCiti"); 
    	enterMobNo = "yes";
    }else {
     if(isNotBlank(toNickName)) {
	    toNickName = toNickName.substr(0, 20);
	 } 
	    enterMobNo = "no";
    }
	inputParam["enterMobNo"] = enterMobNo; //financial
    inputParam["finTxnMemo"] = frmIBTranferLP.txtArMn.text + ""; //financial		
    inputParam["fromAcctName"] = frmIBTransferNowConfirmation.lblName.text + ""; //financial		
    inputParam["fromAcctNickname"] = frmIBTransferNowConfirmation.lblName.text + ""; //financial		
    inputParam["toAcctName"] = frmIBTransferNowConfirmation.lblXferToAccTyp.text + ""; //financial		
    inputParam["toAcctNickname"] = toNickName; //financial		
    inputParam["gblBalAfterXfer"] = gblBalAfterXfer; //financial		
    inputParam["endDate"] = frmIBTransferNowConfirmation.lblEnDONValue.text;
    inputParam["transferOrderDate"] = frmIBTransferNowConfirmation.lblTransferVal.text;
    var frequency = frmIBTransferNowConfirmation.lblRepeatValue.text;
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
		inputParam["freq"] = "Daily";
		inputParam["recurring"] = "keyDaily";
	} else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
		inputParam["freq"] = "Weekly";
		inputParam["recurring"] = "keyWeekly";
	} else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
		inputParam["freq"] = "Monthly";
		inputParam["recurring"] = "keyMonthly";
	} else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
		inputParam["freq"] = "Annually";
		inputParam["recurring"] = "keyYearly";
	} else {
		inputParam["freq"] = "once";
		inputParam["recurring"] = "keyOnce";
	}		
	inputParam["locale"] = kony.i18n.getCurrentLocale();
	
    //inputParam["recurring"] = frmIBTransferNowConfirmation.lblRepeatValue.text;
    inputParam["Recp_category"] = Recp_category;
    //MIB-2096 - Adding log for tracking one time transfer, Date: 23-Jun-16
	if(!frmIBTranferLP.hbxSelMobileRecipient.isVisible) {
		inputParam["oneTimeTransfer"] = "MobileNumber";
	}
    
	showLoadingScreenPopup();
 		if (gblTokenSwitchFlag == true)
        frmIBTransferNowConfirmation.tbxToken.text="";
   		else 
        frmIBTransferNowConfirmation.txtBxOTP.text="";
    invokeServiceSecureAsync("executeTransfer", inputParam, callbackVerifyTransferIB)
}

function callbackVerifyTransferIB(status, resultTable) {
    if (status == 400) {
    	if (resultTable["opstatus"] == 0) 
		{
			//alert("resultTablefreetransCount"+resultTable["freetransCount"]);
			//frmIBTransferNowCompletion.lblremFreeTranVAlue.text=resultTable["freetransCount"];

		   if(gblisTMB==gblTMBBankCD)
		   {
				frmIBTransferNowCompletion.lblremFreeTranVAlue.text=resultTable["remainingFree"];
		   }
		   if(resultTable["StatusCode"] == 0 || (resultTable["Transfer"] != undefined && resultTable["Transfer"].length > 0)){
		   		if(resultTable["repeatCallTransferAdd"]!= null && kony.string.equalsIgnoreCase(resultTable["repeatCallTransferAdd"],"yes")){
					alert( kony.i18n.getLocalizedString("TRErr_ORFTDuplicate"));
					dismissLoadingScreenPopup();
					return false;
				}
		   		
			   	frmIBTransferNowCompletion.show();
			   	dismissLoadingScreenPopup();
			    frmIBTransferNowConfirmation.lblSplitXferDtVal.text = resultTable["serverDate"];
				frmIBTransferNowConfirmation.lblTransferVal.text = resultTable["serverDate"];
				frmIBTransferNowCompletion.lblTransferVal.text = resultTable["serverDate"];
				frmIBTransferNowCompletion.lblSplitXferDtVal.text = resultTable["serverDate"];
			   	if (!gblPaynow) {
					frmIBTransferNowCompletion.hbox101461944018201.setVisibility(false);
					frmIBTransferNowCompletion.lblBal.setVisibility(false);
				}
				
				var fee = 0;
				var transferResultList=resultTable["Transfer"];
				for (var i = 0; i < transferResultList.length; i++) {
					if(transferResultList[i]["status"] == 0){
				    	var transferFee = transferResultList[i]["fee"]+"";
				    	fee = fee + parseFloat(transferFee);
				    }
				}				
				/*
				fee = frmIBTransferNowConfirmation.lblFeeVal.text;
				if (fee == undefined) {
					fee = 0;
				} else {
					var indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
					fee = parseFloat(fee.substring(0, indexOfB));
				}
				*/
				frmIBTransferNowCompletion.lblName.text = frmIBTranferLP.lblXferFromNameRcvd.text;
				frmIBTransferNowCompletion.lblAccountNo.text = frmIBTranferLP.lblTranLandFromNum.text;
				
				if(gblSelTransferMode == 2){
					frmIBTransferNowCompletion.lblXferAccNo.text = maskMobileNumber(frmIBTransferNowConfirmation.lblXferAccNo.text);
				}else if(gblSelTransferMode == 3){
					frmIBTransferNowCompletion.lblXferAccNo.text = maskCitizenID(frmIBTransferNowConfirmation.lblXferAccNo.text);
				}else{
					frmIBTransferNowCompletion.lblXferAccNo.text = frmIBTransferNowConfirmation.lblXferAccNo.text;
				}
				frmIBTransferNowCompletion.lblXferToBankName.text = getBankNameCurrentLocaleTransfer(gblisTMB);
				frmIBTransferNowCompletion.image2101554296111612.src = frmIBTransferNowConfirmation.image247327596554550.src;
				frmIBTransferNowCompletion.lblProductName.text = frmIBTranferLP.lblTranLandFromAccntType.text;
				frmIBTransferNowCompletion.lblXferToAccTyp.text = frmIBTransferNowConfirmation.lblXferToAccTyp.text;
				frmIBTransferNowCompletion.lblSplitToBankNameVal.text = frmIBTranferLP.lblXferToBankNameRcvd.text;
				frmIBTransferNowCompletion.lblSplitTotAmtVal.text = frmIBTranferLP.txbXferAmountRcvd.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				//frmIBTransferNowCompletion.lblSplitTotFeeVal.text = frmIBTransferNowConfirmation.lblSplitTotFeeVal.text;
				frmIBTransferNowCompletion.lblSplitTotFeeVal.text = commaFormatted(fee + "") +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
				//frmIBTransferNowCompletion.lblSplitXferDt.text=kony.i18n.getLocalizedString("keyXferTD");
				frmIBTransferNowCompletion.lblSplitXferDtVal.text = frmIBTransferNowConfirmation.lblSplitXferDtVal.text;
				frmIBTransferNowCompletion.label101640690016505.text = frmIBTransferNowConfirmation.lblTranNotifyModeVal.text;
				frmIBTransferNowCompletion.label101640690016289.text = frmIBTransferNowConfirmation.lblSplitModTranVal.text;
				frmIBTransferNowCompletion.lblFeeVal.text = commaFormatted(fee + "") +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
				//MIB-4884-Allow special characters for My Note and Note to recipient field
				frmIBTransferNowCompletion.lblSplitMNVal.text = replaceHtmlTagChars(frmIBTranferLP.txtArMn.text);
				frmIBTransferNowCompletion.lblSplitNTRVal.text = replaceHtmlTagChars(frmIBTransferNowConfirmation.lblSplitNTRVal.text);
				frmIBTransferNowCompletion.lblAmtVal.text = frmIBTranferLP.txbXferAmountRcvd.text +" " +kony.i18n.getLocalizedString("currencyThaiBaht");
				//frmIBTransferNowCompletion.lblFeeVal.text = frmIBTransferNowConfirmation.lblFeeVal.text;
				//MIB-4884-Allow special characters for My Note and Note to recipient field
				frmIBTransferNowCompletion.lblMNVal.text = replaceHtmlTagChars(frmIBTransferNowConfirmation.lblMNVal.text);
				frmIBTransferNowCompletion.lblNTRVal.text = replaceHtmlTagChars(frmIBTransferNowConfirmation.lblNTRVal.text);
				frmIBTransferNowCompletion.lblTRNVal.text = frmIBTransferNowConfirmation.lblTRNVal.text;
				
				if (gblTDDateFlag) {
					frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpIntAmt.setVisibility(true);
					frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpTaxAmt.setVisibility(true);
					frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpPenaltyAmt.setVisibility(true);
					frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpNetAmt.setVisibility(true);
					frmIBTransferNowCompletion.lblfrmIBTransferNowCmpIntAmtVal.text = frmIBTransferNowConfirmation.lblfrmIBTransferNowCnfIntAmtVal.text
					frmIBTransferNowCompletion.lblfrmIBTransferNowCmpPenaltyAmtVal.text = frmIBTransferNowConfirmation.lblfrmIBTransferNowCnfPenaltyAmtVal.text
					frmIBTransferNowCompletion.lblfrmIBTransferNowCmpTaxAmtVal.text = frmIBTransferNowConfirmation.lblfrmIBTransferNowCnfTaxAmtVal.text
					frmIBTransferNowCompletion.lblfrmIBTransferNowCmpNetAmtVal.text = frmIBTransferNowConfirmation.lblfrmIBTransferNowCnfNetAmtVal.text
					gblTDDateFlag = false;
				}
				
                if (resultTable["availBal"] != undefined) {
                    var availableBal = resultTable["availBal"] + ""
                    availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
                    availableBal = replaceCommon(availableBal, ",", "");
                    gblBalAfterXfer = availableBal;
                }
                else {
                	var availableBal = frmIBTransferNowConfirmation.lblBeforBal.text;
                    availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
                    availableBal = replaceCommon(availableBal, ",", "");
                    gblBalAfterXfer = availableBal;
                }
				frmIBTransferNowCompletion.lblBal.text = commaFormatted(gblBalAfterXfer) + kony.i18n.getLocalizedString("currencyThaiBaht");
				if (!gblPaynow) {
					frmIBTransferNowCompletion.hbxScheduleDetails.setVisibility(true)
					frmIBTransferNowCompletion.hbxScheduleDetails.isVisible = true;
					frmIBTransferNowCompletion.lblRepeatValue.text = frmIBTransferNowConfirmation.lblRepeatValue.text;
					frmIBTransferNowCompletion.lblExecuteValue.text = frmIBTransferNowConfirmation.lblExecuteValue.text;
					frmIBTransferNowCompletion.lblStartOnValue.text = frmIBTransferNowConfirmation.lblStartOnValue.text;
					frmIBTransferNowCompletion.lblEnDONValue.text = frmIBTransferNowConfirmation.lblEnDONValue.text;
				}
				enableSegmentTransferIB(resultTable)
				if(!gblPaynow){
 					frmIBTransferNowCompletion.hbox101461944018201.setVisibility(false);
					frmIBTransferNowCompletion.lblBal.setVisibility(false);
					if(Recp_category == "2"){
						frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
						frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
					}
				}
        }
		else
		{
        	dismissLoadingScreenPopup();
        	//error case
        	if(resultTable["errMsg"] != undefined)
        	{
        		alert(resultTable["errMsg"]);
        	}
        	else
        	{
        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        	}		
		}//statuscode !=0
	  }//opstatus
	  else if (resultTable["opstatus"] == 1){
	  		dismissLoadingScreenPopup();
	  		if (resultTable["ServerStatusCode"] == "XB240071" || resultTable["ServerStatusCode"] == "XB240072") {
	  			alert(kony.i18n.getLocalizedString("MIB_TRErrNoDest"));	  			
	  		}else{
	  			if (resultTable["ServerStatusCode"] != undefined){
	  				alert(kony.i18n.getLocalizedString("ECGenericError")+ " (" + resultTable["ServerStatusCode"] + ")");
	  			}else{
	  				alert(kony.i18n.getLocalizedString("ECGenericError"));
	  			}	  			
	  		}	  		
	  }//opstatus	
	  else if (resultTable["opstatus"] == 8005) {
 			dismissLoadingScreenPopup();
 			if (resultTable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTP = resultTable["retryCounterVerifyOTP"];
				if(gblTokenSwitchFlag == true  && gblSwitchToken == false) {
                   // alert(kony.i18n.getLocalizedString("wrongOTP")); 
                    frmIBTransferNowConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBTransferNowConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBTransferNowConfirmation.hbxOTPincurrect.isVisible = true;
                    frmIBTransferNowConfirmation.hbox476047582127699.isVisible = false;
                    frmIBTransferNowConfirmation.hbxOTPsnt.isVisible = false; 
                    frmIBTransferNowConfirmation.tbxToken.text = "";    
                    frmIBTransferNowConfirmation.hbxToken.setVisibility(true);
					frmIBTransferNowConfirmation.tbxToken.setFocus(true);
				} else {
				    //alert(kony.i18n.getLocalizedString("wrongOTP"));//commented by swapna
                    frmIBTransferNowConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBTransferNowConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBTransferNowConfirmation.hbxOTPincurrect.isVisible = true;
                    frmIBTransferNowConfirmation.hbox476047582127699.isVisible = false;
                    frmIBTransferNowConfirmation.hbxOTPsnt.isVisible = false; 
                    frmIBTransferNowConfirmation.txtBxOTP.text = "";    
                    frmIBTransferNowConfirmation.txtBxOTP.setFocus(true);    
                  
				}
                    return false;
           }else if (resultTable["errCode"] == "VrfyOTPErr00002") {
                    dismissLoadingScreenPopup();
                    //startRcCrmUpdateProIB("04");
					handleOTPLockedIB(resultTable);
                    return false;
           	}
           	else if (resultTable["errCode"] == "VrfyOTPErr00004") {
                    dismissLoadingScreenPopup();
                    //startRcCrmUpdateProIB("04");
					showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
                    return false;
           	}
           	else if (resultTable["errCode"] == "VrfyOTPErr00005") {
                    dismissLoadingScreenPopup();
                    //startRcCrmUpdateProIB("04");
					showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
                    return false;
           	}else if (resultTable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	}else if(resultTable["errCode"] == "VrfyOTPErr00006"){
           	
           	        frmIBTransferNowConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBTransferNowConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBTransferNowConfirmation.hbxOTPincurrect.isVisible = true;
                    frmIBTransferNowConfirmation.hbox476047582127699.isVisible = false;
                    frmIBTransferNowConfirmation.hbxOTPsnt.isVisible = false; 
                    frmIBTransferNowConfirmation.tbxToken.text = "";    
                    frmIBTransferNowConfirmation.hbxToken.setVisibility(true);
					frmIBTransferNowConfirmation.tbxToken.setFocus(true);          	
             	}else {
                    frmIBTransferNowConfirmation.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBTransferNowConfirmation.lblPlsReEnter.text = " "; 
                    frmIBTransferNowConfirmation.hbxOTPincurrect.isVisible = false;
                    frmIBTransferNowConfirmation.hbox476047582127699.isVisible = true;
                    frmIBTransferNowConfirmation.hbxOTPsnt.isVisible = true; 
                    frmIBTransferNowConfirmation.txtBxOTP.setFocus(true);           	
           	
           	}
 	}else
	   {
 	      	dismissLoadingScreenPopup();
 	      	//error case
        	if(resultTable["errMsg"] != undefined)
        	{
        		alert(resultTable["errMsg"]);
        	}
        	else
        	{
        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        	}	
	   }
    }//status 40
}

function enableSegmentTransferIB(resultTable) {

	var transferAckListTbl=resultTable["Transfer"]
	
	var gblRemainingFreeTransactionCount = 0;	
	var isAtleastOneSuccess = false;
	var isAtleastOneFail = false;
	for (var i = 0; i < transferAckListTbl.length; i++) {
			if (transferAckListTbl[i]["image"] == "wrong.png") {
				isAtleastOneFail = true;
			} else {
				isAtleastOneSuccess =true;
			}
		}
		
		if(isAtleastOneFail){
				frmIBTransferNowCompletion.hbxFail.setVisibility(true);
				frmIBTransferNowCompletion.hbxSuccess.setVisibility(false);
				frmIBTransferNowCompletion.lblTrans.setVisibility(true);
				frmIBTransferNowCompletion.lblComp.setVisibility(false);
				frmIBTransferNowCompletion.hbxAdv.setVisibility(false);
		}else{
				frmIBTransferNowCompletion.hbxFail.setVisibility(false);
				frmIBTransferNowCompletion.hbxSuccess.setVisibility(true);
				frmIBTransferNowCompletion.lblTrans.setVisibility(false);
				frmIBTransferNowCompletion.lblComp.setVisibility(true);
				frmIBTransferNowCompletion.hbxAdv.setVisibility(true);
		}
		
		
	if (transferAckListTbl.length > 1) {
		
		if( (gblTransEmail == "0" && gblTrasSMS == "0") || (gblTransEmail == "1" && gblTrasSMS == "1")){
			frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
			frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
		
		}else{
			frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(true);
			frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(true);
		}
		frmIBTransferNowCompletion.hbxDetails.setVisibility(false);	
		frmIBTransferNowCompletion.segSplitDet.widgetDataMap = {
			lblSplitAmt: "lblSplitAmt",
			lblSplitAmtVal: "lblSplitAmtVal",
			lblSplitFee: "lblSplitFee",
			lblSplitFeeVal: "lblSplitFeeVal",
			lblSplitTRN: "lblSplitTRN",
			lblSplitTRNVal: "lblSplitTRNVal",
			imgFail: "imgFail"
		}
		var transferNoToDiaplay = "";
		var tempData = [];
		var image="";
		for (var i = 0; i < transferAckListTbl.length; i++) {
			transferNoToDiaplay =transferAckListTbl[i]["refId"];
			if(transferAckListTbl[i]["status"] == 0){
		    	image="completeico_sm.png";
				gblRemainingFreeTransactionCount++;
		    }
		    else
		    {
		   		image="failico_sm.png";
		    }
 			var data = {
				lblSplitTRN: gblPaynow ? kony.i18n.getLocalizedString("keyTransactionRefNo") : kony.i18n.getLocalizedString("keySchedule"),
				lblSplitAmt: kony.i18n.getLocalizedString("amount"),
				lblSplitFee: kony.i18n.getLocalizedString("keyFee"),
				lblSplitTRNVal: transferNoToDiaplay,
				lblSplitAmtVal: commaFormatted(transferAckListTbl[i]["amount"]) +" "+ kony.i18n.getLocalizedString("currencyThaiBaht"),
				lblSplitFeeVal: commaFormatted(transferAckListTbl[i]["fee"]+"") + " "+kony.i18n.getLocalizedString("currencyThaiBaht"),
				imgFail: image
			}
			/**{lblSegTrans:"Transaction Ref. No:",lblSegAmount:"Amount:",lblSegFee:"Fee:",lblSegTransRefNo:"3894756",lblsegAmountDes:"45670 "+"\u0E3F",lblsegFeeDes:"50 "+"\u0E3F",image2101271281126570:"wrong.png"
		}]**/
			tempData.push(data);
		}
		
		frmIBTransferNowCompletion.segSplitDet.setData(tempData);
		frmIBTransferNowCompletion.segSplitDet.setVisibility(true); 
	} else {
		frmIBTransferNowCompletion.segSplitDet.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(false);
		frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitMN.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
		frmIBTransferNowCompletion.hbxDetails.setVisibility(true);
		if(gblisTMB==gblTMBBankCD)
		   {
		   		//frmIBTransferNowCompletion.lblremFreeTranVAlue.text
				gblRemainingFreeTransactionCount=resultTable["remainingFree"];
		   }else if (transferAckListTbl[0]["status"] == 0) {
             	gblRemainingFreeTransactionCount++;
        	 }
		if( (gblTransEmail == "0" && gblTrasSMS == "0") || (gblTransEmail == "1" && gblTrasSMS == "1")){
			frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
			frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);		
		}else{
			frmIBTransferNowCompletion.hbxTransNotif.setVisibility(true);
			frmIBTransferNowCompletion.hbxTransNtr.setVisibility(true);
		}
		if (gblisTMB == gblTMBBankCD) {
			fee = 0;
		} else {
			var fee = frmIBTransferNowConfirmation.lblFeeVal.text + "";
			var indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
			fee = parseFloat(fee.substring(0, indexOfB));
		} 
	}
	frmIBTransferNowCompletion.lblXferToName.text = frmIBTransferNowConfirmation.lblXferToName.text;
	frmIBTransferNowCompletion.lblremFreeTranVAlue.text = "";
	if(!isAtleastOneSuccess && gblisTMB==gblTMBBankCD){
		frmIBTransferNowCompletion.lblremFreeTranVAlue.text = frmIBTransferNowConfirmation.lblremFreeTranVAlue.text;
	}else if(gblisTMB==gblTMBBankCD && resultTable["remainingFree"]!=undefined &&  resultTable["remainingFree"]!=null)
		frmIBTransferNowCompletion.lblremFreeTranVAlue.text=resultTable["remainingFree"];
	else if(gblTrasORFT && resultTable["RemainingFeeORFT"]!=undefined && resultTable["RemainingFeeORFT"]!=null)
	{
		frmIBTransferNowCompletion.lblremFreeTranVAlue.text = resultTable["RemainingFeeORFT"];
	}
	else if(resultTable["RemainingFeeSmart"] != undefined && resultTable["RemainingFeeSmart"] != null)
		frmIBTransferNowCompletion.lblremFreeTranVAlue.text=resultTable["RemainingFeeSmart"];	
}
 
function checkVerifyOTPIB() {
	
	verifyTransferIB();
}
 
/**************************************************************************************
		Module	: toggleTDMaturityCombobox
		Author  : Kony
		Date    : May 05, 2013
		Purpose : displaying TDmaturityDetails combobox if selected from account is TD
*****************************************************************************************/

function toggleTDMaturityComboboxIB(gbltdFlag) {
	var temptdFlag  = "CDA"
	if (gbltdFlag == temptdFlag) {
		frmIBTranferLP.hbxMaturity.setVisibility(true);
		frmIBTranferLP.line1011851788556.setVisibility(true);
		frmIBTranferLP.txbXferAmountRcvd.setEnabled(false);
		var amountTD = gblcwselectedData.lblBalance;
		amountTD = amountTD.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
		amountTD = amountTD.replace(",", "")
		amountTD = parseFloat(amountTD.trim());
		btnTermDeposit();
		getTDAccountIB();
	} else {
		frmIBTranferLP.txbXferAmountRcvd.setEnabled(true);
		frmIBTranferLP.hbxMaturity.setVisibility(false);
		frmIBTranferLP.line1011851788556.setVisibility(true);
		frmIBTransferNowConfirmation.hbxMatDate.setVisibility(false);
		frmIBTransferNowCompletion.hbxMaturityDat.setVisibility(false);
		//Added to make the service call only incase of DreamAccount
		frmIBTranferLP.btnXferShowContact.setVisibility(true);
		if(gblcwselectedData.prodCode == 206)
		{
					depositAccountInqDreamSav();//checking for dream savings account
		}
	}
}

function funReqOTPIB() {
	frmIBTransferNowConfirmation.btnOTPReq.skin = btnIB158active
	frmIBTransferNowConfirmation.btnOTPReq.setEnabled(true);
	kony.timer.cancel("OtpTimerID1");
}
/**************************************************************************************
		Module	: transferMaturityDateIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : displaying TDmaturityDetails afer selecting the combobox 
*****************************************************************************************/

function transferMaturityDateIB() {
	frmIBTranferLP.label1209235936217894.text = kony.i18n.getLocalizedString("keyMaturityDate")
	frmIBTranferLP.label1209235936217896.text = kony.i18n.getLocalizedString("keyPrincipal")
	var comboDataKey = frmIBTranferLP.cmbxMaturityDetails.selectedKey;
	var dateaAmount=comboDataKey.split(",");
	var completeDate=dateaAmount[0];
	var amountTD=dateaAmount[1];
	var keyVal = dateaAmount[2];
	if(keyVal !=0){
		if (frmIBTranferLP.hbxMaturityResults.isVisible == false) {
			frmIBTranferLP.hbxMaturityResults.setVisibility(true);
			var comboData = frmIBTranferLP.cmbxMaturityDetails.selectedKeyValue;
			
			frmIBTranferLP.lblTDDatVal.text = completeDate;
			frmIBTransferNowConfirmation.lblMatDateVal.text = completeDate;
			frmIBTransferNowCompletion.lblMatDateVal.text = completeDate;
			frmIBTranferLP.lblTDAmtVal.text = commaFormatted(amountTD);
			gblTDDateIB = completeDate;
			//gbTDAmtIB = amountTD;
			
			var amountXferTD = gblXferTDAmt[keyVal - 1];
			frmIBTranferLP.txbXferAmountRcvd.text = commaFormatted(amountXferTD);
			frmIBTranferLP.btnTD.setVisibility(true);
			frmIBTranferLP.hbxInsideMaturity.setVisibility(false);
			frmIBTranferLP.txbXferAmountRcvd.setEnabled(false);
			frmIBTransferNowConfirmation.hbxMatDate.setVisibility(true);
			frmIBTransferNowCompletion.hbxMaturityDat.setVisibility(true);
			//getTransferFeeTDIB();
		
		} else {
			frmIBTransferNowConfirmation.hbxMatDate.setVisibility(false);
			frmIBTransferNowCompletion.hbxMaturityDat.setVisibility(false);
			frmIBTranferLP.hbxMaturityResults.setVisibility(false);
			frmIBTranferLP.btnTD.setVisibility(false);
			frmIBTranferLP.hbxInsideMaturity.setVisibility(true);
			frmIBTranferLP.txbXferAmountRcvd.setEnabled(true);
		}
	}
}

function btnTermDeposit() {
	frmIBTranferLP.hbxMaturityResults.setVisibility(false);
	frmIBTranferLP.hbxInsideMaturity.setVisibility(true);
	//frmIBTranferLP.txbXferAmountRcvd.setEnabled(true);
	frmIBTranferLP.btnTD.setVisibility(false);
}
/**************************************************************************************
		Module	: customWidgetSelectEventIBTransfer
		Author  : Kony
		Date    : May 05, 2013
		Purpose : onClick of Custom widget this method is called
*****************************************************************************************/
var productCode="";
function customWidgetSelectEventIBTransfer() {
	showLoadingScreenPopup();
	var selectedItem = gblCWSelectedItem;
	gblcwselectedData = frmIBTransferCustomWidgetLP.custom1016496273208904.data[gblCWSelectedItem];
	//TMBUtil.DestroyForm(frmIBTranferLP);
	dismissLoadingScreenPopup();
	frmIBTranferLP.show();
	setSelTabBankAccountOrMobile();
	frmIBTranferLP.imgXferToImage.src="";
	frmIBTranferLP.lblXferToNameRcvd.text="";
	frmIBTranferLP.lblXferToContactRcvd.text="";
	frmIBTranferLP.lblXferToBankNameRcvd.text="";
	frmIBTranferLP.imgXferToImage.setVisibility(false);
	closeRightPanelTransfer();
	clearValueIBTransferLP();
	if(recipientAddFromTransfer || gblTransferFromRecipient){
		var x=frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text;
		var y=gblcwselectedData.accountWOF;
		if(x!= undefined && y != undefined && x != null && y != null && x != y){
			//gblBANKREF="TMB";
			//gblisTMB=gblTMBBankCD
			//gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +"tmb" + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
			frmIBTranferLP.imgXferToImage.src=frmIBTransferCustomWidgetLP.imgXferToImage.src;
			frmIBTranferLP.lblXferToNameRcvd.text=frmIBTransferCustomWidgetLP.lblXferToNameRcvd.text;
			frmIBTranferLP.lblXferToContactRcvd.text=frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text;
			frmIBTranferLP.lblXferToBankNameRcvd.text=frmIBTransferCustomWidgetLP.lblXferToBankNameRcvd.text;
			frmIBTranferLP.imgXferToImage.setVisibility(true);
			if(frmIBTransferCustomWidgetLP.hbxNotifyRecipent.isvisible) {
				frmIBTranferLP.hbxNotifyRecipent.setVisibility(true);
				frmIBTranferLP.lineOne.setVisibility(true);
			} else {
				frmIBTranferLP.hbxNotifyRecipent.setVisibility(false);
				frmIBTranferLP.lineOne.setVisibility(false);
			}
		}
	//}else if(gblshotcutToACC && (gblCWSelectedItem == 0)){
	}else if(gblshotcutToACC){
		var x=frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text;
		var y=gblcwselectedData.accountWOF;
		if(x!= undefined && y != undefined && x != null && y != null && x != y){
			gblBANKREF="TMB";
			gblisTMB=gblTMBBankCD
			gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +"tmb" + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
			frmIBTranferLP.imgXferToImage.src=gblMyProfilepic;
			frmIBTranferLP.lblXferToNameRcvd.text=frmIBTransferCustomWidgetLP.lblXferToNameRcvd.text;
			frmIBTranferLP.lblXferToContactRcvd.text=frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text;
			frmIBTranferLP.lblXferToBankNameRcvd.text=frmIBTransferCustomWidgetLP.lblXferToBankNameRcvd.text;
			frmIBTranferLP.imgXferToImage.setVisibility(true);
			/* to disable note to recipient fields in all screens as we are showing these own accounts in to field*/
				frmIBTranferLP.hbxNotifyRecipent.setVisibility(false);
				frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
				frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
				frmIBTranferLP.hbxEmail.setVisibility(false);
				frmIBTranferLP.hbxSMS.setVisibility(false);
				frmIBTranferLP.lineOne.setVisibility(false);
				frmIBTranferLP.lineThree.setVisibility(false);
				frmIBTranferLP.hboxXferlblFee.setVisibility(false);
				frmIBTranferLP.vbox47679117772050.skin = "vboxWhiteBg";
				frmIBTransferNowConfirmation.hbxSplitNtr.setVisibility(false);
				frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(false);
				frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
				frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);		
				frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
				frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
				frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
				frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);	
				gblTrasORFT = 0;
				gblTransSMART = 0;
				gblTransEmail = 1;
				gblTrasSMS = 1;
		}
	}
	if (gblcwselectedData == undefined) {
		frmIBTranferLP.lblTranLandFromNum.text = "undefined";
	} else {
		frmIBTransferNowConfirmation.image247327596551482.src = gblcwselectedData.img1;
		frmIBTransferNowCompletion.image247327596551482.src = gblcwselectedData.img1;
		frmIBTransferNowConfirmation.lblBeforBal.text = gblcwselectedData.lblBalance;
		frmIBTranferLP.lblXferFromNameRcvd.text = gblcwselectedData.lblCustName;
		frmIBTranferLP.lblTranLandFromNum.text = gblcwselectedData.accountWOF;
		
		frmIBTranferLP.lblTranLandFromAccntType.text=gblcwselectedData.custAcctName; //gblcwselectedData.lblProductVal;
		frmIBTranferLP.lblTranLandFromBalanceAmtVal.text=gblcwselectedData.lblBalance;
		frmIBTranferLP.imageXferFrmLP.src=gblcwselectedData.img1;
		if(gblSMART_FREE_TRANS_CODES.indexOf(gblcwselectedData.prodCode) >= 0 ){
			frmIBTranferLP.hbxFreeTransactions.setVisibility(true);
			frmIBTransferNowConfirmation.hbxFreeTransactions.setVisibility(true);
			frmIBTransferNowCompletion.hbxFreeTransactions.setVisibility(true);		
			frmIBTranferLP.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree");
			frmIBTransferNowConfirmation.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree");
			frmIBTransferNowCompletion.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree");
			frmIBTranferLP.lblremFreeTranVAlue.text=gblcwselectedData.remainingFee;
			frmIBTransferNowConfirmation.lblremFreeTranVAlue.text=gblcwselectedData.remainingFee;
			frmIBTransferNowCompletion.lblremFreeTranVAlue.text=(parseFloat(gblcwselectedData.remainingFee-1))<0 ? 0:(parseFloat(gblcwselectedData.remainingFee) -1).toString();
		}
		else{
			frmIBTranferLP.hbxFreeTransactions.setVisibility(false);
			frmIBTransferNowConfirmation.hbxFreeTransactions.setVisibility(false);
			frmIBTransferNowCompletion.hbxFreeTransactions.setVisibility(false);		
		}
		
		
		gbltdFlag = gblcwselectedData.lblSliderAccN2;
		productCode =fromData[selectedItem].prodCode;
		
		
		
		if(kony.i18n.getCurrentLocale() != "th_TH")
				frmIBTranferLP.btnMenuTransfer.skin = "btnIBMenuTransferFocus"
			else
				frmIBTranferLP.btnMenuTransfer.skin = "btnIBMenuTransferFocusThai"	
			
		toggleTDMaturityComboboxIB(gbltdFlag);
	}
}

/*************************************************************************

	Module	: normalTransactionIB
	Author  : Kony
	Purpose : Setting data into Transfer confirm screens

****/

function normalTransactionIB() {
	frmIBTransferNowConfirmation.show();
	frmIBTransferNowConfirmation.txtBxOTP.text = "";
	frmIBTransferNowConfirmation.lblName.text = frmIBTranferLP.lblXferFromNameRcvd.text;
	frmIBTransferNowConfirmation.lblAccountNo.text = gblcwselectedData.accountWOF;
	frmIBTransferNowConfirmation.lblXferToBankName.text=getBankNameCurrentLocaleTransfer(gblisTMB);
	
	if(gblSelTransferMode == 1) {// Normal Transfer(ToAccount)
		frmIBTransferNowConfirmation.image247327596554550.src = frmIBTranferLP.imgXferToImage.src;
		frmIBTransferNowConfirmation.lblXferToName.text = frmIBTranferLP.lblXferToNameRcvd.text;
		frmIBTransferNowConfirmation.lblXferAccNo.text = frmIBTranferLP.lblXferToContactRcvd.text;
	} else { //Mobile Transfer(AnyID)
			frmIBTransferNowConfirmation.lblXferAccNo.text = frmIBTranferLP.txtXferMobileNumber.text;
	}
	
	frmIBTransferNowCompletion.image2101554296111612.text = frmIBTransferNowConfirmation.image247327596554550.src ;
	frmIBTransferNowCompletion.lblXferToName.text = frmIBTransferNowConfirmation.lblXferToName.text;
	frmIBTransferNowCompletion.lblXferToName.setVisibility(false);
	if(frmIBTransferNowConfirmation.lblXferToName.isVisible){
		frmIBTransferNowCompletion.lblXferToName.setVisibility(true);
	}
	if(gblSelTransferMode == 2){
		frmIBTransferNowCompletion.lblXferAccNo.text = maskMobileNumber(frmIBTransferNowConfirmation.lblXferAccNo.text);
	}else if(gblSelTransferMode == 3){
		frmIBTransferNowCompletion.lblXferAccNo.text = maskCitizenID(frmIBTransferNowConfirmation.lblXferAccNo.text);
	}else{
		frmIBTransferNowCompletion.lblXferAccNo.text = frmIBTransferNowConfirmation.lblXferAccNo.text;
	}
	
	//MIB-4884-Allow special characters for My Note and Note to recipient field
 	frmIBTransferNowConfirmation.lblMNVal.text = replaceHtmlTagChars(frmIBTranferLP.txtArMn.text);
 	var notetoRec=frmIBTranferLP.tbxXferNTR.text;
	if(gblTrasSMS ==0 && gblTransEmail==1){
		notetoRec=frmIBTranferLP.textRecNoteEmail.text;
	}
	frmIBTransferNowConfirmation.lblNTRVal.text = replaceHtmlTagChars(notetoRec);
	frmIBTransferNowConfirmation.lblSplitNTRVal.text=replaceHtmlTagChars(notetoRec);
	frmIBTransferNowCompletion.lblSplitNTRVal.text=replaceHtmlTagChars(notetoRec);
	frmIBTransferNowConfirmation.lblProductName.text = frmIBTranferLP.lblTranLandFromAccntType.text;
 
	//for TD account remove the comma in amount feild
	var amtRcvd = replaceCommon(frmIBTranferLP.txbXferAmountRcvd.text, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	
	amtRcvd = amtRcvd.replace(/ /g, "");

	frmIBTransferNowConfirmation.lblAmtVal.text = amtRcvd +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
 	frmIBTransferNowConfirmation.hbxDetails.setVisibility(true);
	if(gblPaynow){
		frmIBTransferNowConfirmation.lblTRNVal.text = "NT" + gblTransferRefNo + "00";
	}else{
		frmIBTransferNowConfirmation.lblTRNVal.text = "ST" + gblTransferRefNo + "00";
			
			//To show date in 24 hour format for future transfer
			var requiredDate =returnDateForFT();
			
	}
	frmIBTransferNowConfirmation.hbxSplitXfer.setVisibility(false);
	frmIBTransferNowCompletion.hbxDetails.setVisibility(true);
	frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(false);
	frmIBTransferNowConfirmation.hbxSplitMN.setVisibility(false);
	frmIBTransferNowConfirmation.hbxSplitNtr.setVisibility(false);
	frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(false);
	
	frmIBTransferNowConfirmation.lblSmartTrfrDate.text = kony.i18n.getLocalizedString("keyTransferBy");
	frmIBTransferNowCompletion.lblSmartTrfrDate.text = kony.i18n.getLocalizedString("keyTransferBy");
	
	if(gblPaynow){
		if(gblTransSMART == 1) {
			frmIBTransferNowConfirmation.lblTransfer.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
			frmIBTransferNowCompletion.lblTransfer.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
		} else {
			frmIBTransferNowConfirmation.lblTransfer.text = kony.i18n.getLocalizedString("keyXferTD");
			frmIBTransferNowCompletion.lblTransfer.text = kony.i18n.getLocalizedString("keyXferTD");
		}	
	} else {
		frmIBTransferNowConfirmation.lblTransfer.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
		frmIBTransferNowCompletion.lblTransfer.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
	}
	if (!gblPaynow) {
		var textDates = frmIBTranferLP.lblXferTransferRange.text;
		var textTimes = frmIBTranferLP.lblXferInterval.text;
		var tokenDate = textDates.split(" ");
		var tokenTimes = textTimes.split(" ");
		frmIBTransferNowCompletion.hbxScheduleDetails.setVisibility(true);
 		frmIBTransferNowConfirmation.hbxScheduleDetails.isVisible = true;
		frmIBTransferNowConfirmation.lblRepeatValue.text = tokenTimes[1];
		frmIBTransferNowConfirmation.lblExecuteValue.text = tokenTimes[2];
		frmIBTransferNowConfirmation.lblStartOnValue.text = tokenDate[0];
		frmIBTransferNowConfirmation.lblEnDONValue.text = tokenDate[2];
		if (tokenTimes[2] == undefined) {
		  undefinedTransactionTimesforIB();
		 }
		 }
	var fee;
	var indexOfB;
	if (gblisTMB != gblTMBBankCD && gblTrasORFT == 1) {
		fee = frmIBTranferLP.btnXferORFT.text;
		indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
		fee = fee.substring(0, indexOfB);
		frmIBTransferNowConfirmation.lblFeeVal.text = fee + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmIBTransferNowCompletion.lblFeeVal.text = frmIBTransferNowConfirmation.lblFeeVal.text
	} else if (gblisTMB != gblTMBBankCD && gblTransSMART == 1) {
		fee = frmIBTranferLP.btnXferSMART.text
		indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
		fee = fee.substring(0, indexOfB);
		frmIBTransferNowConfirmation.lblFeeVal.text = fee + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmIBTransferNowCompletion.lblFeeVal.text = frmIBTransferNowConfirmation.lblFeeVal.text;
	}
		frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(true);
		frmIBTransferNowConfirmation.hbxButn.setVisibility(false);
		frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(false);
	
	if((gblisTMB == gblTMBBankCD && gblTransEmail == 1 && gblTrasSMS == 1) || ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && isOwnAccountP2P)){
    	frmIBTransferNowConfirmation.hbxButn.setVisibility(true);
    	frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(false);
    }else{
   		frmIBTransferNowConfirmation.hbxButn.setVisibility(false);
    	frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(true);
    }
}

/*************************************************************************

	Module	: Normal and splitingTransaction
	Author  : Kony
	Purpose : undefined times for normal and splitingTransaction(FutureTransfer)

****/


function undefinedTransactionTimesforIB(){
	if (DailyClicked == true) {
		frmIBTransferNowConfirmation.lblExecuteValue.text="-"
		frmIBTransferNowConfirmation.lblEnDONValue.text ="-"
		frmIBTransferNowConfirmation.lblRepeatValue.text=kony.i18n.getLocalizedString("Transfer_Daily");
  	} 
	else if (weekClicked == true) {
		frmIBTransferNowConfirmation.lblExecuteValue.text="-"
	  	frmIBTransferNowConfirmation.lblEnDONValue.text ="-"
		frmIBTransferNowConfirmation.lblRepeatValue.text=kony.i18n.getLocalizedString("Transfer_Weekly");
   	}
	else if (monthClicked == true) {
		frmIBTransferNowConfirmation.lblExecuteValue.text="-"
		frmIBTransferNowConfirmation.lblEnDONValue.text ="-"
		frmIBTransferNowConfirmation.lblRepeatValue.text=kony.i18n.getLocalizedString("Transfer_Monthly");
  	}
	else if (YearlyClicked == true) {
		frmIBTransferNowConfirmation.lblExecuteValue.text="-"
		frmIBTransferNowConfirmation.lblEnDONValue.text ="-"
		frmIBTransferNowConfirmation.lblRepeatValue.text=kony.i18n.getLocalizedString("Transfer_Yearly");
   	}else {
		frmIBTransferNowConfirmation.lblExecuteValue.text="1 times"
		frmIBTransferNowConfirmation.lblEnDONValue.text =frmIBTransferNowConfirmation.lblStartOnValue.text;
		frmIBTransferNowConfirmation.lblRepeatValue.text="Once";
  	}
}

/*************************************************************************

	Module	: splitingTransaction
	Author  : Kony
	Purpose : Setting data into Transfer confirm screens

****/

function splitingTransactionIB() {
    frmIBTransferNowConfirmation.hbox101458964925294.setVisibility(false);
	frmIBTransferNowCompletion.hbox101458964925304.setVisibility(false)
	frmIBTransferNowConfirmation.show();
	frmIBTransferNowConfirmation.txtBxOTP.text = "";
	frmIBTransferNowConfirmation.lblName.text = frmIBTranferLP.lblXferFromNameRcvd.text;
	//MIB-4884-Allow special characters for My Note and Note to recipient field
	frmIBTransferNowConfirmation.lblSplitMNVal.text = replaceHtmlTagChars(frmIBTranferLP.txtArMn.text);
	frmIBTransferNowConfirmation.lblAccountNo.text = gblcwselectedData.accountWOF;
	frmIBTransferNowConfirmation.lblSplitNTRVal.text = replaceHtmlTagChars(frmIBTranferLP.tbxXferNTR.text);
	if(gblTrasSMS ==0 && gblTransEmail==1){
		frmIBTransferNowConfirmation.lblSplitNTRVal.text = replaceHtmlTagChars(frmIBTranferLP.textRecNoteEmail.text);
	}
	frmIBTransferNowConfirmation.lblXferToName.text = frmIBTranferLP.lblXferToNameRcvd.text;
	frmIBTransferNowConfirmation.lblXferToBankName.text=getBankNameCurrentLocaleTransfer(gblisTMB);
	frmIBTransferNowConfirmation.lblProductName.text = frmIBTranferLP.lblTranLandFromAccntType.text;
	
	frmIBTransferNowConfirmation.lblSplitToBankNameVal.text = frmIBTranferLP.lblXferToBankNameRcvd.text;
	frmIBTransferNowConfirmation.lblXferAccNo.text = frmIBTranferLP.lblXferToContactRcvd.text;
	frmIBTransferNowConfirmation.lblSplitTotAmtVal.text = frmIBTranferLP.txbXferAmountRcvd.text + " " +kony.i18n.getLocalizedString(
		"currencyThaiBaht");
	frmIBTransferNowConfirmation.hbxDetails.setVisibility(false);
	frmIBTransferNowConfirmation.segSplitDet.widgetDataMap = {
		lblSplitAmt: "lblSplitAmt",
		lblSplitAmtVal: "lblSplitAmtVal",
		lblSplitFee: "lblSplitFee",
		lblSplitFeeVal: "lblSplitFeeVal",
		lblSplitTRN: "lblSplitTRN",
		lblSplitTRNVal: "lblSplitTRNVal"
	}
	var splitAmtLen = gblsplitAmt.length;
	
	var tempData = [];
	var totalFee = 0;
	var transferNoToDiaplay = "";
	for (var i = 0; i < splitAmtLen; i++) {
	    if(gblsplitFee[i] != 0)
		 totalFee += gblsplitFee[i];
		var pad = "";
			if (i < 9)
				pad = "0" + (i + 1);
			else
				pad = (i + 1);
				
			var refNoToDisplay = "";
			if(gblPaynow){
				transferNoToDiaplay = "NT"+ gblTransferRefNo +pad 
			}else{
				transferNoToDiaplay = "ST"+ gblTransferRefNo +pad 
				frmIBTransferNowConfirmation.lblSplitXferDtVal.text =returnDateForFT()
			}

		temp = {
			lblSplitAmt: kony.i18n.getLocalizedString("amount"),
			lblSplitAmtVal: commaFormatted(gblsplitAmt[i]) + " " +kony.i18n.getLocalizedString("currencyThaiBaht"),
			lblSplitFee: kony.i18n.getLocalizedString("keyFee"),
			lblSplitFeeVal: commaFormatted(gblsplitFee[i]+"") +" " + kony.i18n.getLocalizedString("currencyThaiBaht"),
			lblSplitTRN: gblPaynow ? kony.i18n.getLocalizedString("keyTransactionRefNo") : kony.i18n.getLocalizedString("keySchedule"),
			lblSplitTRNVal: transferNoToDiaplay
		}
		tempData.push(temp);
	}
	frmIBTransferNowConfirmation.lblSplitTotFeeVal.text = commaFormatted(totalFee+"") + " " +  kony.i18n.getLocalizedString("currencyThaiBaht");
	frmIBTransferNowConfirmation.segSplitDet.data = tempData;
	if( gblTransEmail == "0" && gblTrasSMS == "0"){
		frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(false);
		frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitMN.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
	
	}else{
		frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(true);
		frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(true);
		frmIBTransferNowCompletion.hbxSplitMN.setVisibility(true);
		frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(true);
	
	}
	frmIBTransferNowCompletion.hbxDetails.setVisibility(false);	
	frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(true);

 
	if (!gblPaynow) {
		var textDates = frmIBTranferLP.lblXferTransferRange.text;
		var textTimes = frmIBTranferLP.lblXferInterval.text;
		var textArray = textTimes.split(" ");
		frmIBTransferNowConfirmation.hbxScheduleDetails.setVisibility(true)
		frmIBTransferNowConfirmation.hbxScheduleDetails.isVisible = true;
		frmIBTransferNowConfirmation.lblRepeatValue.text = textArray[1];
		frmIBTransferNowConfirmation.lblExecuteValue.text = textArray[2];
		frmIBTransferNowConfirmation.lblStartOnValue.text = textDates.substring(0, 10);
		frmIBTransferNowConfirmation.lblEnDONValue.text = textDates.substring(13, 24);
		
		if (textArray[2] == undefined) {
		  undefinedTransactionTimesforIB();
		 }
	}
	
	if(gblPaynow){
	
		if(gblTransSMART == 1) {
			frmIBTransferNowConfirmation.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
			frmIBTransferNowCompletion.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
		} else {
		frmIBTransferNowConfirmation.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyXferTD");
		frmIBTransferNowCompletion.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyXferTD");
		}
	} else {
		frmIBTransferNowConfirmation.hbxSplitMN.setVisibility(true);
		frmIBTransferNowConfirmation.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
		frmIBTransferNowCompletion.lblSplitXferDt.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
	}
	frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(true);
	frmIBTransferNowConfirmation.hbxButn.setVisibility(false);
	frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(false);
}
/*************************************************************************

	Module	: enableSegmentTransferCompl
	Author  : Kony
	Purpose : Setting data into Transfer confirm screens

****/

function enableSegmentTransferCompl() {
	
	if (gblsplitAmt.length > 0) {
		for (var i = 0; i < gblsplitAmt.length; i++) {
			if (gblSplitAckImg[i] == "wrong.png") {
				frmIBTransferNowCompletion.hbxFail.setVisibility(true);
				frmIBTransferNowCompletion.hbxSuccess.setVisibility(false);
				frmIBTransferNowCompletion.lblTrans.setVisibility(true);
				frmIBTransferNowCompletion.lblComp.setVisibility(false);
				frmIBTransferNowCompletion.hbxAdv.setVisibility(false);
			} else {
				frmIBTransferNowCompletion.hbxFail.setVisibility(false);
				frmIBTransferNowCompletion.hbxSuccess.setVisibility(true);
				frmIBTransferNowCompletion.lblTrans.setVisibility(false);
				frmIBTransferNowCompletion.lblComp.setVisibility(true);
				frmIBTransferNowCompletion.hbxAdv.setVisibility(true);
			}
		}
		
		if( gblTransEmail == "0" && gblTrasSMS == "0"){
			frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(false);
			frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
			frmIBTransferNowCompletion.hbxSplitMN.setVisibility(false);
			frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
		
		}else{
			frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(true);
			frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(true);
			frmIBTransferNowCompletion.hbxSplitMN.setVisibility(true);
			frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(true);
 		
		}
		frmIBTransferNowCompletion.hbxDetails.setVisibility(false);	


		frmIBTransferNowCompletion.segSplitDet.widgetDataMap = {
			lblSplitAmt: "lblSplitAmt",
			lblSplitAmtVal: "lblSplitAmtVal",
			lblSplitFee: "lblSplitFee",
			lblSplitFeeVal: "lblSplitFeeVal",
			lblSplitTRN: "lblSplitTRN",
			lblSplitTRNVal: "lblSplitTRNVal",
			imgFail: "imgFail"
		}
		var transferNoToDiaplay = "";
		var tempData = [];
		for (var i = 0; i < gblsplitAmt.length; i++) {
			if(gblPaynow){
				transferNoToDiaplay = "NT" + gblTransferRefNo + "0" + (i + 1) ;
			}else{
				transferNoToDiaplay = "ST"+ gblTransferRefNo + "0" + (i + 1) ;
			}
			var data = {
				lblSplitTRN: gblPaynow ? kony.i18n.getLocalizedString("keyTransactionRefNo") : kony.i18n.getLocalizedString("keySchedule"),
				lblSplitAmt: kony.i18n.getLocalizedString("amount"),
				lblSplitFee: kony.i18n.getLocalizedString("keyFee"),
				lblSplitTRNVal: transferNoToDiaplay,
				lblSplitAmtVal: commaFormatted(gblsplitAmt[i]) + kony.i18n.getLocalizedString("currencyThaiBaht"),
				lblSplitFeeVal: commaFormatted(gblsplitFee[i]+"") + kony.i18n.getLocalizedString("currencyThaiBaht"),
				imgFail: gblSplitAckImg[i]
			}
			/**{lblSegTrans:"Transaction Ref. No:",lblSegAmount:"Amount:",lblSegFee:"Fee:",lblSegTransRefNo:"3894756",lblsegAmountDes:"45670 "+"\u0E3F",lblsegFeeDes:"50 "+"\u0E3F",image2101271281126570:"wrong.png"
		}]**/
			tempData.push(data);
		}
		
		frmIBTransferNowCompletion.segSplitDet.setData(tempData);
		frmIBTransferNowCompletion.segSplitDet.setVisibility(true);
		
		var j = 0;
		for (var i = 0; i < gblsplitAmt.length; i++) {
			var finTxnRefId = "" + j + (i + 1);
			var finTxnStatus = "";
			if (gblSplitStatusInfo[i] == 0)
				finTxnStatus = "01"
			else
				finTxnStatus = "02"
			if ((i % 10) == 0)
				j++;
		}
	} else {
		frmIBTransferNowCompletion.segSplitDet.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitXfer.setVisibility(false);
		frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitMN.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
		frmIBTransferNowCompletion.hbxDetails.setVisibility(true);
		if (gblisTMB == gblTMBBankCD) {
			fee = 0;
		} else {
			var fee = frmIBTransferNowConfirmation.lblFeeVal.text + "";
			var indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
			fee = parseFloat(fee.substring(0, indexOfB));
		}
		
	}
}

function transAmountOnClickIB() {
	if(frmIBTranferLP.txbXferAmountRcvd.text !=undefined && frmIBTranferLP.txbXferAmountRcvd.text.trim() != "" ){
			frmIBTranferLP.txbXferAmountRcvd.text = replaceCommon(frmIBTranferLP.txbXferAmountRcvd.text, ",", "");
	}
}

/*
************************************************************************

	Module	: transAmountOnDone
	Author  : Kony
	Purpose : Calculating the fee for ORFT and SMART based on the amount entered by the user

****/

function transAmountOnDoneIB() {
	var availableBal;
	if (gblcwselectedData == 0) {
		var fromData = gblcwselectedData;
		availableBal = fromData[0].lblBalance;
	} else {
		availableBal = gblcwselectedData.lblBalance; //To be retrived from service
	}
	
	availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	availableBal = availableBal.replace(/,/g, "");
	availableBal = parseFloat(availableBal.trim());
	var findDot;
	var isCrtFormt;
	var isSyntaxCorrect;
	var dotcount;
	var isValdFormt;
	
	enteredAmount = frmIBTranferLP.txbXferAmountRcvd.text;
	isCrtFormt = amtValidationIB(enteredAmount);
	dotcount = checkAmountSyntax(enteredAmount);
	enteredAmount =  enteredAmount.replace(/,/g, "");
	//frmIBTranferLP.txbXferAmountRcvd.text = enteredAmount;
	// enteredAmount = parseFloat(enteredAmount);
	
	if (dotcount >= 2) {
		isSyntaxCorrect = false;
	} else {
		isSyntaxCorrect = true;
	}
	var transOWNLimit = parseFloat(gblXerSplitData[0].WithinBankOwnAccLimitTransaction);
	var transOWNLimitPerDay = parseFloat(gblXerSplitData[0].WithinBankOwnAccLimit);
	if(gblisTMB == gblTMBBankCD )
	{
		if (transOWNLimit > transOWNLimitPerDay) 
		{
		
			alert(kony.i18n.getLocalizedString("keyConfigurationError"));
		}
		if (enteredAmount  > transOWNLimitPerDay ){
			alert(kony.i18n.getLocalizedString("keyLimitExceededforOwnAccounts"));
			frmIBTranferLP.txbXferAmountRcvd.text = "";
		   	return false
		}
	}
	if (!isCrtFormt || !isSyntaxCorrect) {
		alert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"));
		frmIBTranferLP.txbXferAmountRcvd.text = "";
		return false
	}
	
	reCheckSelTransferMode(removeHyphenIB(frmIBTranferLP.txtXferMobileNumber.text));
	if(gblSelTransferMode == 2){
		var mobileNumber = removeHyphenIB(frmIBTranferLP.txtXferMobileNumber.text);
		if(!isNotBlank(frmIBTranferLP.lblMobileNumberTemp.text)){
			frmIBTranferLP.lblMobileNumberTemp.text = "";
		}
		if(!isNotBlank(frmIBTranferLP.txtXferMobileNumber.text)){
			showAlert(kony.i18n.getLocalizedString("IB_P2PNoToValue_ERR"), kony.i18n.getLocalizedString("info"));
			frmIBTranferLP.txtXferMobileNumber.setFocus(true);
			return false;
		}
		if((isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) > 0)
			&& isNotBlank(mobileNumber)){
			frmIBTranferLP.txbXferAmountRcvd.text = commaFormatted(fixedToTwoDecimal(enteredAmount));
			if(!kony.string.equalsIgnoreCase(frmIBTranferLP.lblMobileNumberTemp.text,frmIBTranferLP.txtXferMobileNumber.text)){
				if (!recipientMobileVal(mobileNumber)){
					showAlert(kony.i18n.getLocalizedString("MIB_P2PkeyEnteredMobileNumberisnotvalid"), kony.i18n.getLocalizedString("info"));
					frmIBTranferLP.txtXferMobileNumber.setFocus(true);
					return false;
				}else{
					gblp2pAccountNumber = "";
					checkCallCheckOnUsPromptPayinqServiceIB();
				}
			}else{
				if(isValidateFromToAccountIB(gblp2pAccountNumber) == false){
			        showAlert(kony.i18n.getLocalizedString("MIB_P2PErrMsgMobileNoLinkFromAcct"), kony.i18n.getLocalizedString("info"));
			        frmIBTranferLP.txtXferMobileNumber.setFocus(true);
			        return false;        
			    }
				checkDisplayForHbxSelRecipient();
    			calcuateITMXFeeIB();
			}
		}else{
			displayITMXFeeP2P(false);
		}
	}else if(gblSelTransferMode == 3){
		var citizenID = removeHyphenIB(frmIBTranferLP.txtXferMobileNumber.text);
		if(!isNotBlank(frmIBTranferLP.lblMobileNumberTemp.text)){
			frmIBTranferLP.lblMobileNumberTemp.text = "";
		}
		if(!isNotBlank(frmIBTranferLP.txtXferMobileNumber.text)){
			showAlert(kony.i18n.getLocalizedString("IB_P2PNoToValue_ERR"), kony.i18n.getLocalizedString("info"));
			frmIBTranferLP.txtXferMobileNumber.setFocus(true);
			return false;
		}
		if((isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) > 0)
			&& isNotBlank(citizenID)){
			frmIBTranferLP.txbXferAmountRcvd.text = commaFormatted(fixedToTwoDecimal(enteredAmount));
			if(!kony.string.equalsIgnoreCase(frmIBTranferLP.lblMobileNumberTemp.text,frmIBTranferLP.txtXferMobileNumber.text)){
				if (!checkCitizenID(citizenID)){
					showAlert(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"), kony.i18n.getLocalizedString("info"));
					frmIBTranferLP.txtXferMobileNumber.setFocus(true);
					return false;
				}else{
					gblp2pAccountNumber = "";
					checkCallCheckOnUsPromptPayinqServiceIB();
				}
			}else{
				if(isValidateFromToAccountIB(gblp2pAccountNumber) == false){
			        showAlert(kony.i18n.getLocalizedString("MIB_P2PErrMsgCILinkFromAcct"), kony.i18n.getLocalizedString("info"));
			        frmIBTranferLP.txtXferMobileNumber.setFocus(true);
			        return false;        
			    }
				checkDisplayForHbxSelRecipient();
    			calcuateITMXFeeIB();
			}
		}else{
			displayITMXFeeP2P(false);
		}
	}
	
	if(!isNotBlank(gblisTMB)){
		return false;
	}
	if (gblisTMB == gblTMBBankCD) {
		frmIBTranferLP.hbxFee.setVisibility(false);
		frmIBTranferLP.hbxFeeBtns.setVisibility(false);
		frmIBTranferLP.lineBelowFee.setVisibility(false);
		frmIBTransferNowConfirmation.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmIBTransferNowCompletion.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
		frmIBTranferLP.txbXferAmountRcvd.text = commaFormatted(fixedToTwoDecimal(enteredAmount));
	} else if(isNotBlank(frmIBTranferLP.txbXferAmountRcvd.text)){
		//get the orft & smart fee for non-tmb transfers.
		frmIBTranferLP.txbXferAmountRcvd.text = commaFormatted(fixedToTwoDecimal(enteredAmount));
		if(gblSelTransferMode == 2 || gblSelTransferMode == 3){
			return;
		}
		if (frmIBTranferLP.lblXferToNameRcvd.text == "") {
         	return;
    	}
		frmIBTranferLP.hbxFeeBtns.setVisibility(true);
		frmIBTranferLP.btnXferSMART.setVisibility(true)
		frmIBTranferLP.btnXferORFT.setVisibility(true)
		frmIBTranferLP.lineBelowFee.setVisibility(true);
		getTransferFeeIB();
	}
}
/*************************************************************************

	Module	: onChangeToRecip
	Author  : Kony
	Purpose : clearing field on selction of to recipeints

*************************************************************************/

function onChangeToRecip() {
	frmIBTranferLP.tbxXferNTR.text = "";
	frmIBTranferLP.textRecNoteEmail.text="";
    frmIBTranferLP.txbXferAmountRcvd.text = "";
    frmIBTranferLP.txtArMn.text = "";
	//var date=new Date(GLOBAL_TODAY_DATE);
	var todaysDate = new Date(GLOBAL_TODAY_DATE);
	var year = todaysDate.getFullYear();
	var date = todaysDate.getDate();
	if ((date.toString()
		.length) == 1) {
		date = "0" + date;
	}
	var month = todaysDate.getMonth() + 1;
	var TransferDate = date + "/" + month + "/" + year;
	frmIBTranferLP.lblXferTransferRange.text=TransferDate;
	frmIBTranferLP.lblXferInterval.text = "";
    frmIBTranferLP.hboxXferlblFee.setVisibility(false);
    frmIBTranferLP.hbxFee.setVisibility(false);
    frmIBTranferLP.hbxFeeBtns.setVisibility(false);
    frmIBTranferLP.btnXferSMART.setVisibility(false)
    frmIBTranferLP.btnXferORFT.setVisibility(false)
    frmIBTranferLP.lineBelowFee.setVisibility(false);
    frmIBTranferLP.lineTwo.setVisibility(false);
    frmIBTranferLP.lineThree.setVisibility(false);
    frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
    frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
    frmIBTranferLP.hbxSMS.setVisibility(false);
    frmIBTranferLP.hbxEmail.setVisibility(false);
    displayITMXFeeP2P(false);
    frmIBTranferLP.btnXferEmail.skin = btnIBemailRoundedCorner;
    frmIBTranferLP.btnXferSMS.skin = btnIbSMSRoundedCorner
}
 
/*************************************************************************

	Module	: amountValidationIB
	Author  : Kony
	Purpose : Checking if the enter data contains only "0","1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."

*************************************************************************/

function amtValidationIB(amount) {
	if (amount != null && amount != "") {
		if (kony.string.containsOnlyGivenChars(amount, ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."])) {
			amount = amount.replace(/,/g, "");
			amount = amount.replace(".", "");
			//ADDED os.tonumber(amount)== nil TO CHECK IF THE USER ONLY ENTER "." or ","
			if (kony.os.toNumber(amount) <= 0 || kony.os.toNumber(amount) == null) {
				return false;
			} else
				return true;
		} else
			return false;
	}else if(amount == ""){
			  return true;
		  }else {
			  return false;
		  }
}

function checkAmountSyntax(amount) {
	var dotArray = [];
	for (var i = 0; i < amount.length; i++) {
		if (amount[i] == ".") {
			dotArray.push(amount[i]);
		}
	}
	return dotArray.length;
}
/**************************************************************************************
		Module	: TransferRecipientSearch
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function search in dictionay view
*****************************************************************************************/

function searchTransferToRecipientIB(eventObject, searchType) {
	
	if(gblSelTransferMode == 2){
		searchP2PContactsIB();
	} else {
		var textLength;
		
		if (searchType == "1") {
			textLength = 0;
		} else {
			textLength = 0;
		}
		var sProduct = eventObject.text;
		var favimg="";
		if (sProduct.length <= textLength) {
			frmIBTranferLP.lblMsg.setVisibility(false);
			frmIBTranferLP.segXferRecipentsList.setVisibility(true);
			frmIBTranferLP.segXferRecipentsList.data = gblTransferToRecipientCache;
			gblTransferToRecipientData = gblTransferToRecipientCache;
		} else {
			final_Product = [];
			var check = "";
			for (var i = 0;
				((gblTransferToRecipientCache) != null) && i < gblTransferToRecipientCache.length; i++) {
				val = gblTransferToRecipientCache[i].lblXferRecipentsName;
				check = val.toLowerCase().indexOf(sProduct) > -1
				
				//check = kony.string.startsWith(val, sProduct, true);  
				if (check == true) {
					favimg = gblTransferToRecipientCache[i].imgfav;
					final_Product.push({
						lblXferRecipentsName: gblTransferToRecipientCache[i].lblXferRecipentsName,
						lblXferRecipentsAccount: gblTransferToRecipientCache[i].lblXferRecipentsAccount,
						personalizedId: gblTransferToRecipientCache[i].personalizedId,
						btnXferMobile:gblTransferToRecipientCache[i].btnXferMobile,
						imgURL:gblTransferToRecipientCache[i].imgURL,
						btnXferFb:gblTransferToRecipientCache[i].btnXferFb,
						isnonTMBAccount:"false",
						imgXferArrow: "navarrow3.png",
						imgfav:favimg,
						imgXferRecipentsImage: gblTransferToRecipientCache[i].imgXferRecipentsImage,
						mobileNo: gblTransferToRecipientCache[i].mobileNo,
						fbNo: gblTransferToRecipientCache[i].fbNo,
						category: "main",
						template: hbxXferSegmentInitial,
						recepientID: gblTransferToRecipientCache[i].recepientID
					});
				}
			}
			frmIBTranferLP.segXferRecipentsList.data = final_Product;
			gblTransferToRecipientData = final_Product;
			
			if (final_Product.length == 0) {
				frmIBTranferLP.lblMsg.setVisibility(true);
				frmIBTranferLP.segXferRecipentsList.setVisibility(false);
				//frmIBTranferLP.lblMsg.text = "No results found";
				
			} else {
				frmIBTranferLP.lblMsg.setVisibility(false);
				frmIBTranferLP.segXferRecipentsList.setVisibility(true);
			}
		}
	}
}
/**************************************************************************************
		Module	: ResetTransferHomePageIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : On click generates OTP
*****************************************************************************************/

function ResetTransferHomePageIB() {
	//frmIBTranferLP.destroy(); IE 8 specific issue occur when we use destroy on IE 8
	//frmIBTransferNowCompletion.destroy();
	//frmIBTransferNowConfirmation.destroy();
 	gblOrftAddRs = [];
	gblsplitAmt = [];
	gblsplitFee = [];
	gblSplitAckImg = [];
	gblRetryCountRequestOTP = 0;
	gblVerifyOTPCounter = 0;
	gblVerifyOTP = 0;
	gblSplitCnt = 0;
	gblTransSMART = 0;
	gblTrasORFT = 0;
	gblPaynow = true;
	Recp_category = 0;
	gblTransferFromRecipient=false;
	recipientAddFromTransfer=false;
}
/**************************************************************************************
		Module	: OTP Generation module
		Author  : Kony
		Date    : May 05, 2013
		Purpose : On click generates OTP
*****************************************************************************************/

function requestButtonForOTPIB() {
        frmIBTransferNowConfirmation.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBTransferNowConfirmation.lblPlsReEnter.text = " "; 
        frmIBTransferNowConfirmation.hbxOTPincurrect.isVisible = false;
        //frmIBTransferNowConfirmation.hbox476047582127699.isVisible = true;
        //frmIBTransferNowConfirmation.hbxOTPsnt.isVisible = true; 
        frmIBTransferNowConfirmation.txtBxOTP.setFocus(true); 
           
		frmIBTransferNowConfirmation.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		frmIBTransferNowConfirmation.lblOTPKey.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBTransferNowConfirmation.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		frmIBTransferNowConfirmation.txtBxOTP.text = "";
		tokenFlag = false;
		startRequestOTPApplyServicesTransferIB();
}
/**************************************************************************************
		Module	: locale change code
		Author  : Kony
		Date    : May 05, 2013
		Purpose : On click locale change is done
*****************************************************************************************/

function syncIBTransferLocale() {
	frmIBFTrnsrView.lblFee.text=kony.i18n.getLocalizedString("keyXferFee");
	var currentFormId = kony.application.getCurrentForm()
		.id;
	if (currentFormId == "frmIBTransferCustomWidgetLP") {
		getFromXferAccountsIB();
	} else if (currentFormId == "frmIBTranferLP") {
	
		frmIBTranferLP.txbXferSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
		frmIBTranferLP.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree");
		frmIBTranferLP.lblMsg.text=kony.i18n.getLocalizedString("keybillernotfound");
			if(gblSelTransferMode == 1){
			
						if (frmIBTranferLP.hbxXferTo.isVisible){
							frmIBTranferLP.TMBImage.isVisible=false;
							//getToRecipientsNewIB(gblcwselectedData.prodCode, removeHyphenIB(gblcwselectedData.accountWOF));
							//frmIBTranferLP.lblXferToBankNameRcvd.text=getBankNameCurrentLocaleTransfer(gblisTMB);
						}
					var combomaturity=frmIBTranferLP.cmbxMaturityDetails.masterData;
					var x=frmIBTranferLP.cmbxMaturityDetails.selectedKey;
					frmIBTranferLP.label1209235936217894.text = kony.i18n.getLocalizedString("keyMaturityDate")
					frmIBTranferLP.label1209235936217896.text = kony.i18n.getLocalizedString("keyPrincipal")
					if(combomaturity != null && combomaturity != undefined && combomaturity.length > 1){
							combomaturity[0][1] = kony.i18n.getLocalizedString("keySelectMaturityDate");
						for(var i=1;i<combomaturity.length;i++){
							var comboDataKey = combomaturity[i][0]//frmIBTranferLP.cmbxMaturityDetails.selectedKey;
							var dateaAmount=comboDataKey.split(",");
							var completeDate=dateaAmount[0]
							var princAmount=dateaAmount[1]
							var keyVal = dateaAmount[2];
							combomaturity[i][1]=kony.i18n.getLocalizedString("keyMaturityDate") + completeDate +" "+ kony.i18n.getLocalizedString("keyPrincipal") + princAmount + " " 
									+kony.i18n.getLocalizedString("currencyThaiBaht") ;
						}
					frmIBTranferLP.cmbxMaturityDetails.masterData=combomaturity;	
					frmIBTranferLP.cmbxMaturityDetails.selectedKey = x;
					var today = new Date();
				    var dd = today.getDate();
				    var mm = today.getMonth() + 1;
				    var yyyy = today.getFullYear();
				    if (dd < 10) {
				        dd = '0' + dd
				    }
				    if (mm < 10) {
				        mm = '0' + mm
				    }
				    today = dd + '/' + mm + '/' + yyyy;
					if(((frmIBTranferLP.CalendarXferDate.formattedDate)!="")&&(frmIBTranferLP.CalendarXferDate.formattedDate!=null)&&(parseDate(frmIBTranferLP.CalendarXferDate.formattedDate) - parseDate(today) > 0))
						onSaveClick();
					}
		}
		
	} else if (currentFormId == "frmIBTransferNowConfirmation" || currentFormId == "frmIBTransferNowCompletion"){
	
	frmIBTransferNowConfirmation.lblSmartTrfrDate.text = kony.i18n.getLocalizedString("keyTransferBy");
	frmIBTransferNowCompletion.lblSmartTrfrDate.text = kony.i18n.getLocalizedString("keyTransferBy");
    if (frmIBTransferNowCompletion.lblBal.isVisible)
    {
      frmIBTransferNowCompletion.lblHide.text= kony.i18n.getLocalizedString("keyHideIB");
    }
    else
    {
     frmIBTransferNowCompletion.lblHide.text=kony.i18n.getLocalizedString("keyUnhide");
    }
 		
 		if(gblPaynow){
 			if(gblTransSMART == 1) {
				frmIBTransferNowConfirmation.lblTransfer.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
				frmIBTransferNowCompletion.lblTransfer.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
			} else {
				frmIBTransferNowConfirmation.lblTransfer.text = kony.i18n.getLocalizedString("keyXferTD");
				frmIBTransferNowCompletion.lblTransfer.text = kony.i18n.getLocalizedString("keyXferTD");
			}			
		} else {
			frmIBTransferNowConfirmation.lblTransfer.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
			frmIBTransferNowCompletion.lblTransfer.text = kony.i18n.getLocalizedString("keyOrderDAteFTIB");
		}
		frmIBTransferNowConfirmation.lblXferToBankName.text=getBankNameCurrentLocaleTransfer(gblisTMB);
		frmIBTransferNowCompletion.lblXferToBankName.text=getBankNameCurrentLocaleTransfer(gblisTMB);
		frmIBTransferNowConfirmation.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
		frmIBTransferNowConfirmation.lblKeyotpmg.text = kony.i18n.getLocalizedString("keyotpmsg");
		frmIBTransferNowConfirmation.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		frmIBTransferNowConfirmation.lblOTPKey.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBTransferNowConfirmation.lblToken.text = kony.i18n.getLocalizedString("keyToken");
		frmIBTransferNowCompletion.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree")
		frmIBTransferNowConfirmation.lblremFreeTran.text=kony.i18n.getLocalizedString("keyremainingFree")
		frmIBTransferNowConfirmation.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		frmIBTransferNowConfirmation.button506369893806238.text = kony.i18n.getLocalizedString("keySwitchToOTP");	
		if (monthClicked == true) {
            frmIBTransferNowConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
            frmIBTransferNowCompletion.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
        } else if (weekClicked == true) {
            frmIBTransferNowConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
            frmIBTransferNowCompletion.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
        } else if (DailyClicked == true) {
            frmIBTransferNowConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
            frmIBTransferNowCompletion.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
        } else if (YearlyClicked == true) {
            frmIBTransferNowConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
            frmIBTransferNowCompletion.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
        }	else{
        	frmIBTransferNowConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
            frmIBTransferNowCompletion.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
        
        }
	}
}

//function startdummycrmServiceTr() {
//	var inputParams = {
//		crmId: frmIBEnterCustomerIdLogin.txtUserId.text
//	};
//	gblcrmId = frmIBEnterCustomerIdLogin.txtUserId.text;
//	try {
//		invokeServiceSecureAsync("dummycrmGenerator", inputParams, callBackdummycrmService);
//	} catch (e) {
//		// todo: handle exception
//		invokeCommonIBLogger("Exception in invoking service");
//	}
//}
/*function startdummycrmServiceCmbBx() {
	var inputParams = {
		crmId: frmIBEnterCustomerIdLogin.crmCombo.selectedKeyValue
	};
	try {
		invokeServiceSecureAsync("dummycrmGenerator", inputParams, callBackdummycrmService);
	} catch (e) {
 		invokeCommonIBLogger("Exception in invoking service");
	}
}*/

/*function callBackdummycrmService(status, resultable) {
	if (status == 400) {
 		getFromXferAccountsIB();
	} else {
		if (status == 300) {
			alert("Error");
		}
	}
}*/

function onSavingSessionXferOTPGenerate() {
	if(gblTokenSwitchFlag && gblSwitchToken == false){
		dismissLoadingScreenPopup();
		frmIBTransferNowConfirmation.hbxToken.setVisibility(true);
		frmIBTransferNowConfirmation.tbxToken.setFocus(true);
		frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(false);
		frmIBTransferNowConfirmation.hbxOtpBox.setVisibility(true);
		frmIBTransferNowConfirmation.txtBxOTP.setFocus(true);
		frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(false);
		frmIBTransferNowConfirmation.hbxButn.setVisibility(true);
		frmIBTransferNowConfirmation.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
	} else{ 
		frmIBTransferNowConfirmation.hbxToken.setVisibility(false);
		frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(true);
		frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(false);
		frmIBTransferNowConfirmation.hbxButn.setVisibility(true);
		frmIBTransferNowConfirmation.hbxOtpBox.setVisibility(true);
		frmIBTransferNowConfirmation.txtBxOTP.setFocus(true);
		
		requestButtonForOTPIB();
	}
}
// modified the code due to issues on switc token from other modules
function executeTransferBeforeconfirmOne(){
	
	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		checkTokenFlag();
	} 
	
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
		frmIBTransferNowConfirmation.hbxToken.setVisibility(true);
		frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(false);
		frmIBTransferNowConfirmation.hbxOtpBox.setVisibility(true);
		frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(false);
		frmIBTransferNowConfirmation.hbxButn.setVisibility(true);
		frmIBTransferNowConfirmation.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
		frmIBTransferNowConfirmation.hbxToken.setVisibility(true);
		frmIBTransferNowConfirmation.tbxToken.setFocus(true);
	}
	else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
		frmIBTransferNowConfirmation.hbxToken.setVisibility(false);
		frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(true);
		frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(false);
		frmIBTransferNowConfirmation.hbxButn.setVisibility(true);
		frmIBTransferNowConfirmation.hbxOtpBox.setVisibility(true);
		frmIBTransferNowConfirmation.txtBxOTP.setFocus(true);
		onClickPreConfirmTrans();
	}
}


function onClickPreConfirmTrans() {
	var amount = frmIBTranferLP.txbXferAmountRcvd.text + "";
	var userName = "";
	var userNum = "";
	var temptdFlag;
	temptdFlag = kony.i18n.getLocalizedString("termDeposit")
	if(gbltdFlag == temptdFlag) {
	 userName = frmIBTransferNowConfirmation.lblName.text + "";
	 userNum = frmIBTransferNowConfirmation.lblAccountNo.text + "";
	}
	else {
	 userName = frmIBTranferLP.lblXferToNameRcvd.text + "";
	 userNum = gblp2pAccountNumber + "";
	}
	if(userNum.length>0) {
		userNum=removeHyphenIB(userNum);
		userNum="xxx-x-"+userNum.substring(userNum.length-6,userNum.length-1)+"-x";
	}
	if(!gblPaynow) {
		userName = frmIBTransferNowConfirmation.lblXferToName.text;
	}
    var bankShortName=""
	if(gblBANKREF == "TMB"){
	 bankShortName = gblBANKREF;
	}else{
     bankShortName = getBankShortNameTransferMB(gblBANKREF);
	}
	
	var module = "";
	if (gbltdFlag == "CDA") {
		module = "withdraw";
	} else {
		module = "execute";
	}
	if(gblToTDFlag){
		module = "deposit";
	}
	
	if(!gblPaynow) {
		module = "schedule";
	}
	
    execXferServiceParamsInSession(amount, userName, userNum, bankShortName, module);
}

/*function execXferServiceParamsCallBack(status, result){
    if (status == 400) {
		
		
		if (result["opstatus"] == 0) {
			
			onSavingSessionXferOTPGenerate();
		}
	}
}*/



function transferTokenExchangeService() {
 var inputParam = [];
 showLoadingScreenPopup();
 invokeServiceSecureAsync("tokenSwitching", inputParam, transferTokenExchangeServiceCallBack);
}
function transferTokenExchangeServiceCallBack(status,resulttable){
  if (status == 400) {
		   if(resulttable["opstatus"] == 0){
		   checkCrmProfileInqIB();
		   }else{
			    dismissLoadingScreenPopup();
			    alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
		   }
	   }
} 

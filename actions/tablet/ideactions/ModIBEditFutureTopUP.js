









/*function invokeCustomerBillInqForEditTPService(){

	var inputParam = {};
    invokeServiceSecureAsync("customerBillInquiry", inputParam, callBackCustomerBillInqForEditTPService)
}*/


/*function callBackCustomerBillInqForEditTPService(status,result){
if (status == 400) {
        if (result["opstatus"] == 0) {
			callPaymentInqServiceForEditTP();
        }else {
        	dismissLoadingScreenPopup();
            
        }
    }

}*/

function resetGolbalVariablesForEditTP(){
	gblAmountSelectedForTP = false;
	gblScheduleFreqChangedTP = false;
	gblScheduleButtonClickedTP = false;
	gblExecutionOnLoadTP ="";
	gblEndDateOnLoadTP ="";
	gblEndingFreqOnLoadIBTP = "";
	gblVerifyOTPEditTP = 0;
	onClickRepeatAsTP = "";
	onClickEndFreqTP = "";
	repeatAsTP = "";
	endFreqSaveTP = "";
	gblBillerStatusTP=0;
	frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,0,0]
	viewEditTopUpFrmReset();
}

function viewEditTopUpFrmReset(){
		if (frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.isVisible) {
        frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.setVisibility(false);
    }
    if(frmIBTopUpViewNEdit.hbxTPEditAmnt.isVisible){
		frmIBTopUpViewNEdit.hbxTPEditAmnt.setVisibility(false);
	}
	if(!frmIBTopUpViewNEdit.hbxTPVeiwAmnt.isVisible){
		frmIBTopUpViewNEdit.hbxTPVeiwAmnt.setVisibility(true);
	}
	if(frmIBTopUpViewNEdit.lblTPeditHrd.isVisible){
		frmIBTopUpViewNEdit.lblTPeditHrd.setVisibility(false);
	}
	if(!frmIBTopUpViewNEdit.lblTPViewHrd.isVisible){
		frmIBTopUpViewNEdit.lblTPViewHrd.setVisibility(true);
	}
	if(!frmIBTopUpViewNEdit.hbxTPViewHdr.isVisible){
		frmIBTopUpViewNEdit.hbxTPViewHdr.setVisibility(true);
	}
	//New Header Changes
	 if(!frmIBTopUpViewNEdit.buttonEdit.isVisible){
		 frmIBTopUpViewNEdit.buttonEdit.setVisibility(true);
	 }
	
	if(!frmIBTopUpViewNEdit.buttonDel.isVisible){
		frmIBTopUpViewNEdit.buttonDel.setVisibility(true);
	}
	//--
	//frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,20,0]
	if(frmIBTopUpViewNEdit.btnTPSchedule.isVisible){
		frmIBTopUpViewNEdit.btnTPSchedule.setVisibility(false);
		frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,0,0]
	}
	if(!frmIBTopUpViewNEdit.hbxbtnReturn.isVisible){
		frmIBTopUpViewNEdit.hbxbtnReturn.setVisibility(true);
	}
	if(frmIBTopUpViewNEdit.hbxEditBtns.isVisible){
		frmIBTopUpViewNEdit.hbxEditBtns.setVisibility(false);
	}

	if(frmIBTopUpViewNEdit.hboxOnlineTP.isVisible){
		frmIBTopUpViewNEdit.hboxOnlineTP.setVisibility(false);
	}
	//Schedule 
	if (frmIBTopUpViewNEdit.lblScheduleTopUp.isVisible) {
        frmIBTopUpViewNEdit.lblScheduleTopUp.setVisibility(false);
    }
    if (frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.isVisible) {
        frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.setVisibility(false);
    }
    
    frmIBTopUpViewNEdit.arrowTPScheduleField.setVisibility(false);

	//menuResetForEdit();

}
//Call Payment Inq service 
function callPaymentInqServiceForEditTP(scheID){
	resetGolbalVariablesForEditTP();
	var inputparam = [];
    inputparam["scheRefID"] = scheID;//"FU13000000000141"; 
    inputparam["rqUID"] = "";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("doPmtInqForEditBP", inputparam, callBackPaymenInqServiceForEditTP);

}

function callBackPaymenInqServiceForEditTP(status,result){



 if (status == 400) {
        if (result["opstatus"] == 0) {
        	/*
            var statusCode = result["StatusCode"];
            var severity = result["Severity"];
            var statusDesc = result["StatusDesc"];
            if (statusCode != 0) {
                
            } else { */
                if (result["PmtInqRs"].length > 0 && result["PmtInqRs"] != null) {
                    var amountTP = result["PmtInqRs"][0]["Amt"];
                    gblAmtFromServiceTP = amountTP;
                    gblFromAccNoTP = result["PmtInqRs"][0]["FromAccId"];
                    gblFromAccTypeTP = result["PmtInqRs"][0]["FromAccType"];
                    gblToAccNoTP = result["PmtInqRs"][0]["ToAccId"];
                    gblToAccTypeTP = result["PmtInqRs"][0]["ToAccType"];
                    var executionOnLoadTP = result["PmtInqRs"][0]["ExecutionTimes"];
                    var endDateOnLoadTP = result["PmtInqRs"][0]["EndDate"];
                    var onLoadRepeatAsIBTP = result["PmtInqRs"][0]["RepeatAs"];
                    var pmtOrderdtTP = result["PmtInqRs"][0]["InitiatedDt"]; // check this with TL
                    var scheduleRefNoTP = result["PmtInqRs"][0]["scheRefNo"];
                    var startOnTP = result["PmtInqRs"][0]["Duedt"];
                    var myNoteTP = result["PmtInqRs"][0]["MyNote"];
                    //var bankId = result["PmtInqRs"][0]["BankId"];
                    gblTransCodeTP = result["PmtInqRs"][0]["TransCode"];
                   // gblMobileNumber = result["PmtInqRs"][0]["RecipientMobileNbr"]; // check this with TL
                    gblCustPayIDTP = result["PmtInqRs"][0]["InternalBillerID"];
                    frmIBTopUpViewNEdit.lblTPAmtValue.text = commaFormatted(amountTP) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); // Amount 
                    frmIBTopUpViewNEdit.lblTPScheduleRefNoVal.text = scheduleRefNoTP;//.replace("S", "S"); // schedule ref Num	
                    frmIBTopUpViewNEdit.lblTPStartOnDateVal.text = dateFormatForDisplay(startOnTP);
					gblStartOnTP = dateFormatForDisplay(startOnTP);
                    
                    if(myNoteTP != undefined){
                    	frmIBTopUpViewNEdit.lblMyNoteVal.text = replaceHtmlTagChars(myNoteTP);
                    }else
                    	frmIBTopUpViewNEdit.lblMyNoteVal.text = "-";
                    
                    
                    frmIBTopUpViewNEdit.lblTPPaymentOrderValue.text = dateFormatForDisplayWithTimeStamp(pmtOrderdtTP);
					
                    if (onLoadRepeatAsIBTP != "" && onLoadRepeatAsIBTP != undefined && onLoadRepeatAsIBTP != null) {
                        if (onLoadRepeatAsIBTP == "Annually") {
                            onLoadRepeatAsIBTP = "Yearly";
                            gblOnLoadRepeatAsIBTP = onLoadRepeatAsIBTP;
                            frmIBTopUpViewNEdit.lblTPRepeatAsVal.text = onLoadRepeatAsIBTP;
                        } else {
                            gblOnLoadRepeatAsIBTP = onLoadRepeatAsIBTP;
                            frmIBTopUpViewNEdit.lblTPRepeatAsVal.text = onLoadRepeatAsIBTP;
                        }
                        if (endDateOnLoadTP != "" && endDateOnLoadTP != undefined && endDateOnLoadTP != null) {
                            gblEndingFreqOnLoadIBTP = "OnDate";
							gblEndDateOnLoadTP = dateFormatForDisplay(endDateOnLoadTP);
                            frmIBTopUpViewNEdit.lblEndOnDateVal.text = dateFormatForDisplay(endDateOnLoadTP);
                            var executiontimesTP = numberOfExecution(frmIBTopUpViewNEdit.lblTPStartOnDateVal.text, frmIBTopUpViewNEdit.lblEndOnDateVal.text, onLoadRepeatAsIBTP);
							gblExecutionOnLoadTP = executiontimesTP;
                            frmIBTopUpViewNEdit.lblTPExcuteVal.text = executiontimesTP;
                        } else if (executionOnLoadTP != "" && executionOnLoadTP != undefined && executionOnLoadTP != null) {
                            gblEndingFreqOnLoadIBTP = "After";
							gblExecutionOnLoadTP = executionOnLoadTP;
                            frmIBTopUpViewNEdit.lblTPExcuteVal.text = executionOnLoadTP;
                            var endOnDateTP = endOnDateCalculator(frmIBTopUpViewNEdit.lblTPStartOnDateVal.text, frmIBTopUpViewNEdit.lblTPExcuteVal.text , onLoadRepeatAsIBTP);
							gblEndDateOnLoadTP = endOnDateTP;
                            frmIBTopUpViewNEdit.lblEndOnDateVal.text = endOnDateTP;
                        } else {
                            gblEndingFreqOnLoadIBTP = "Never";
							frmIBTopUpViewNEdit.lblEndOnDateVal.text = "-";
							frmIBTopUpViewNEdit.lblTPExcuteVal.text = "-";
							gblExecutionOnLoadTP ="";
							gblEndDateOnLoadTP ="";
							
                        }
                    } else {
                        onLoadRepeatAsIBTP = "Once";
                        gblOnLoadRepeatAsIBTP = onLoadRepeatAsIBTP;
						
						frmIBTopUpViewNEdit.lblEndOnDateVal.text = dateFormatForDisplay(startOnTP);  //"-"
						frmIBTopUpViewNEdit.lblTPExcuteVal.text = "1"; //"-"
						frmIBTopUpViewNEdit.lblTPRepeatAsVal.text = onLoadRepeatAsIBTP;	
						gblExecutionOnLoadTP ="1"; //""
						gblEndDateOnLoadTP = dateFormatForDisplay(startOnTP);//"";	
						gblEndingFreqOnLoadIBTP="";
                    }
                    
                    if(result["PmtInqRs"][0]["ref1ValueB"] != null && result["PmtInqRs"][0]["ref1ValueB"] != undefined){
                    	frmIBTopUpViewNEdit.lblTPRef1Val.text = result["PmtInqRs"][0]["ref1ValueB"];
					} else {
						frmIBTopUpViewNEdit.lblTPRef1Val.text = "";
					}
					//Changed for CC Masking
					if(result["PmtInqRs"][0]["maskedRef1ValueB"] != null && result["PmtInqRs"][0]["maskedRef1ValueB"] != undefined){
                    	frmIBTopUpViewNEdit.lblTPRef1ValMasked.text = result["PmtInqRs"][0]["maskedRef1ValueB"];
                    	/*
                    	if(result["PmtInqRs"][0]["ref1ValueB"].length >= "10"){
                    		frmIBTopUpViewNEdit.lblTPRef1Val.text = addHyphenIB(result["PmtInqRs"][0]["ref1ValueB"]+"");
                    	}else{
                    		 frmIBTopUpViewNEdit.lblTPRef1Val.text = result["PmtInqRs"][0]["ref1ValueB"];
                    	}
                    	*/
                    }else{
                    	 frmIBTopUpViewNEdit.lblTPRef1ValMasked.text = "";
                    
                    }
                    gblBillerStatusTP = result["PmtInqRs"][0]["activeStatus"];
					
					if (result["PmtInqRs"][0]["billerNickName"] != null && result["PmtInqRs"][0]["billerNickName"] != "") {
						if (gblBillerStatusTP == 0) { // If status is inactive don't display Nick Name
								frmIBTopUpViewNEdit.lblTPBillerNickName.setVisibility(false);
						} else {
								frmIBTopUpViewNEdit.lblTPBillerNickName.text = result["PmtInqRs"][0]["billerNickName"];
						}		
					} else {
						//If Not avialble , show blank
						frmIBTopUpViewNEdit.lblTPBillerNickName.text = "";
					}
					        gblEditPaymentMethodTP = 	result["PmtInqRs"][0]["PmtMethod"]; //used for doPmtAdd service
                            gblEditBillMethodTP = result["PmtInqRs"][0]["billerMethod"];
                            gblEditBillerGroupTypeTP = result["PmtInqRs"][0]["billerGroupType"];
                            billerShortNameTP = result["PmtInqRs"][0]["billerShortName"];
                            gblBillerCompcodeTP = result["PmtInqRs"][0]["billerCompcode"];
                            billerNameTPTH=result["PmtInqRs"][0]["billerNameTH"];
                            var local = kony.i18n.getCurrentLocale();
                            
                       //     gblBPEditEffDtIBTP = result["PmtInqRs"][0]["effDt"];
                          //  gblBPEditEffDt  get this param also as we need in onlinepaymentinquiry
                            var billerNameTP = "";
                            labelReferenceNumber1TP = "";
                            if (local == "en_US") {
                                billerNameTP = result["PmtInqRs"][0]["billerNameEN"];
                                labelReferenceNumber1TP = result["PmtInqRs"][0]["labelReferenceNumber1EN"];
                            } else if (local == "th_TH") {
                                billerNameTP = result["PmtInqRs"][0]["billerNameTH"];
                                labelReferenceNumber1TP = result["PmtInqRs"][0]["labelReferenceNumber1TH"];
                            }
                           if(labelReferenceNumber1TP != ""){
                           		labelReferenceNumber1TP = findColonLastString(labelReferenceNumber1TP) ? labelReferenceNumber1TP + ":" : labelReferenceNumber1TP;
                           }
                            if (gblBillerStatusTP == "0") {
                                frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text = billerShortNameTP;
                                if(labelReferenceNumber1TP != ""){
                                	frmIBTopUpViewNEdit.lblTPRef1.text = labelReferenceNumber1TP;
                                }
                                frmIBTopUpViewNEdit.buttonEdit.setEnabled(false);
                                frmIBTopUpViewNEdit.buttonEdit.skin = "btnIBediticonsmall";
                            } else {
                                frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text = billerNameTP + " (" + gblBillerCompcodeTP + ") ";
                                if(labelReferenceNumber1TP != ""){
                                	frmIBTopUpViewNEdit.lblTPRef1.text = labelReferenceNumber1TP;
                                }
                            }
                            
                             gblRef1LblEN = result["PmtInqRs"][0]["labelReferenceNumber1EN"];
			                 gblRef1LblTH = result["PmtInqRs"][0]["labelReferenceNumber1TH"];
                            
                            
                            
                            
                            
                            //step amount 
							if(gblEditBillMethodTP == 1){
							if (result["StepAmount"].length > 0 && result["StepAmount"] != null){
								comboDataEditTP = [];
								var temp = [];
								var j = 1;	
                					for (var i = 0; i < result["StepAmount"].length; i++) {
                    						
                    						var tempAmt  = amountTP+".00";
                    						var resultAmt =  result["StepAmount"][i]["Amt"];
                    						//amountTP+".00" == result["StepAmount"][i]["Amt"]
                    						if(parseFloat(tempAmt.toString()) == parseFloat(resultAmt.toString())){
                    							temp = [ 0,amountTP+".00"];
                    							//j--;
                    						} else {
                    							temp = [ j , result["StepAmount"][i]["Amt"]];
                    							j++;
                    						}
                     						 
                   							 comboDataEditTP.push(temp);
                				}
									 
                				frmIBTopUpViewNEdit.comboboxOnlineAmt.masterData = comboDataEditTP;
                				//frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKey=0;
                				
							} 
							
							}
							var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblBillerCompcodeTP+ "&modIdentifier=MyBillers";
							//BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblBillerCompcodeTP;
							frmIBTopUpViewNEdit.imageBillerPic.src=imagesUrl;
							frmIBEditFutureTopUpPrecnf.image2448366816169765.src=imagesUrl;
                          //As per mail discussion don't call billpaymentInq service for TMB credit card and TMB Loan
							if (gblEditBillMethodTP == 0 || gblEditBillMethodTP ==1){
								forTPBillPmtInq();
							}else{
								frmIBTopUpViewNEdit.lblTPPaymentFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                				forTPCallCustomerAccountInq(); 
                				//frmIBTopUpViewNEdit.show();
               				 	//dismissLoadingScreenPopup();
							}					
					
                   
                } else {
                    dismissLoadingScreenPopup();
                    //alert(result["StatusDesc"]);
                    alert(result["AdditionalStatusDesc"]);
                }
           // }
        } else {
            dismissLoadingScreenPopup();
            //alert(result["errmsg"]);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }


}


/*
   **************************************************************************************
		Module	: forTPBillPmtInq
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function forTPBillPmtInq() {
   var inputParam = [];
     var tranCodeTP;
    if (gblFromAccTypeTP == "DDA") {
        tranCodeTP = "88" + "10";
    } else {
        tranCodeTP = "88" + "20";
    }
    inputParam["compCode"] = gblBillerCompcodeTP; //get from masterbillInq
    inputParam["tranCode"] = tranCodeTP; // from paymentInq //Transaction Code
    inputParam["fromAcctIdentValue"] =   gblFromAccNoTP;//From Account Number from paymentInq 
    inputParam["fromAcctTypeValue"] = gblFromAccTypeTP; //From Account Type from paymentInq (commented for testing) 
    inputParam["transferAmount"] = gblAmtFromServiceTP;  //BillPayment Amount from paymentInq 
    inputParam["pmtRefIdent"] = removeHyphenIB(frmIBTopUpViewNEdit.lblTPRef1Val.text); // Refernece 1 
    inputParam["postedDate"] = changeDateFormatForService(gblStartOnTP); //Payment Date
    inputParam["ePayCode"] = "EPYS"; //Fixed value if ePayment	"value""EPYS"" - ePayment"
	inputParam["invoiceNumber"] = "";
    inputParam["waiveCode"] = "I";
    inputParam["fIIdent"] = ""; //Financial Identity Information of To Account
    invokeServiceSecureAsync("billPaymentInquiry", inputParam, forTPBillPaymentInquiryServiceCallBack);
}
/*
   **************************************************************************************
		Module	: forBPBillPaymentInquiryServiceCallBack
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function forTPBillPaymentInquiryServiceCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            if (result["BillPmtInqRs"].length > 0 && result["BillPmtInqRs"] != null) {
            	if(result["BillPmtInqRs"][0]["WaiveFlag"] == "Y"){
            		frmIBTopUpViewNEdit.lblTPPaymentFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            	}else{
                	frmIBTopUpViewNEdit.lblTPPaymentFeeVal.text = commaFormatted(result["BillPmtInqRs"][0]["FeeAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                forTPCallCustomerAccountInq(); 
                //dismissLoadingScreenPopup();
            } else {
                dismissLoadingScreenPopup();
                alert("No records found");
            }
        } else {
            dismissLoadingScreenPopup();
            alert(result["errMsg"]);
        }
     
    }
}


/*
   **************************************************************************************
		Module	: forTPCallCustomerAccountInq
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function forTPCallCustomerAccountInq() {
    var inputparam = [];
	inputparam["billPayInd"]="billPayInd";
    invokeServiceSecureAsync("customerAccountInquiry", inputparam, forTPCusAccInqCallBackFunction);
}
/*
   **************************************************************************************
		Module	: forBPcusAccInqcallBackFunction
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function forTPCusAccInqCallBackFunction(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
        /*	var statusCode = result["statusCode"];
            var severity = result["severity"];
            var statusDesc = result["statusDesc"];
            if (statusCode != 0) {
                
            } else { */
            
            var ICON_ID = "";
            if (result["custAcctRec"].length > 0 && result["custAcctRec"] != null) {
            	var accFound = false;
                for (var i = 0; i < result["custAcctRec"].length; i++) {
                    var cusAccNoTP = result["custAcctRec"][i]["accId"];
                    var pmtFromAcctNoTP = gblFromAccNoTP;
                   /*
                    if(cusAccNoTP.length == "14"){
						cusAccNoTP = cusAccNoTP.substring(4, cusAccNoTP.length);
					}
					*/
                    
                    if(gblFromAccTypeTP == "SDA" || gblFromAccTypeTP == "CDA" ){
                    	if(pmtFromAcctNoTP.length == "10")
	                    	pmtFromAcctNoTP = "0000" + gblFromAccNoTP;
                     } else {
                     	pmtFromAcctNoTP = gblFromAccNoTP;
                     }
                     
                    if (cusAccNoTP == pmtFromAcctNoTP) {
                    accFound = true;
                        var fromAccNickNameTP = result["custAcctRec"][i]["acctNickName"];
                        var fromAcctStatusTP = result["custAcctRec"][i]["personalisedAcctStatusCode"];
                        var fromAccNameTP = result["custAcctRec"][i]["accountName"];
                        gblAvailBalTP = result["custAcctRec"][i]["availableBal"];
                        var productIDTP =result["custAcctRec"][i]["productID"];
                        ICON_ID = result["custAcctRec"][i]["ICON_ID"];
                        if (fromAcctStatusTP == "02") {
                            if (frmIBTopUpViewNEdit.hbxFromAccnt.isVisible) {
                                frmIBTopUpViewNEdit.hbxFromAccnt.setVisibility(false);
                            }
                            
                            if (!frmIBTopUpViewNEdit.richtextFromAccHidden.isVisible) {
                            	frmIBTopUpViewNEdit.hbox587850322128235.setVisibility(true);
                                frmIBTopUpViewNEdit.richtextFromAccHidden.setVisibility(true);
                            }
                            var delAccMessageTP = kony.i18n.getLocalizedString("S2S_DelAcctMsg") +
								"<a onclick= \"return onClickCallBackFunction();\" href = \"#\"  >" + kony.i18n.getLocalizedString(
									"S2S_MyAcctList") + "</a>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans"); //get this from i18n //"You already deleted this account. Please add the account under <a href=link>My Account List</a> before proceeding with this future bill payment!";
                           // "You already deleted this account. Please add the account under <a onclick= \"return onClickCallBackFunction();\" href = \"#\"  >My Account List</a> before proceeding with this future bill payment!";
                            /// hide the formAccount details
                            frmIBTopUpViewNEdit.richtextFromAccHidden.text = delAccMessageTP;
                            frmIBTopUpViewNEdit.richtextFromAccHidden.skin = lblIB18pxBlack;
                            frmIBTopUpViewNEdit.buttonEdit.setEnabled(false);
                            frmIBTopUpViewNEdit.buttonEdit.skin = "btnIBediticonsmall"; // check for the skin - To Do 
                        } else {
                        	if (!frmIBTopUpViewNEdit.hbxFromAccnt.isVisible) {
                                frmIBTopUpViewNEdit.hbxFromAccnt.setVisibility(true);
                            }
                            
                            if (frmIBTopUpViewNEdit.richtextFromAccHidden.isVisible) {
                            	frmIBTopUpViewNEdit.hbox587850322128235.setVisibility(false);
                                frmIBTopUpViewNEdit.richtextFromAccHidden.setVisibility(false);
                            }
                            frmIBTopUpViewNEdit.buttonEdit.setEnabled(true);
                            frmIBTopUpViewNEdit.buttonEdit.skin = "btnIBediticonsmall";
                            //If nickname not found, then we need to show product name +last 4-digits of the account number
                            //concatenated (default nickname). Ex: TMB No Fixed 1234 // In Tech Spec
                            if (fromAccNickNameTP != null && fromAccNickNameTP != "") {
                                frmIBTopUpViewNEdit.lblTPFromAccNickName.text = fromAccNickNameTP; // from Acc Nick Name
                            } else {
                                //for eng  :ProductNameEng
                                //for TH : ProductNameThai
                                var local = kony.i18n.getCurrentLocale();
                                var length = cusAccNoTP.length;
                                var accNum = cusAccNoTP.substring(length - 4, length); // last four digits of the account number
                                if (local == "en_US") {
                                    var productNameEng = result["custAcctRec"][i]["productNmeEN"];
                                    frmIBTopUpViewNEdit.lblTPFromAccNickName.text = productNameEng + " " + accNum;
                                } else if (local == "th_TH") {
                                    var productNameThai = result["custAcctRec"][i]["productNmeTH"];
                                    frmIBTopUpViewNEdit.lblTPFromAccNickName.text = productNameThai + " " + accNum;
                                }
                            }
                            frmIBTopUpViewNEdit.lblTPFromAccName.text = fromAccNameTP;
                            frmIBTopUpViewNEdit.lblTPFromAccNumber.text = addHyphenIB(cusAccNoTP); //from Acc Number
                        }
                        //forBPMasterBillInq();
						//forBPCustomerBillInquiry();
						//forTPCallOnlinePaymentInq();


          var frmProdIcon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+ICON_ID+"&modIdentifier=PRODICON";
  		 // var frmProdIcon = "https://vit.tau2904.com:443/tmb/ImageRender?crmId=&&personalizedId=&billerId="+ICON_ID+"&modIdentifier=PRODICON";
			frmIBTopUpViewNEdit.imageFromAccPic.src = frmProdIcon+".png";
	    	frmIBEditFutureTopUpPrecnf.image247502979411375.src = frmProdIcon+".png";

						//Assigning from Account images
						/*
							if(gblFromAccTypeTP == "SDA"){
									if(productIDTP == "222"){
										    	frmIBTopUpViewNEdit.imageFromAccPic.src = "prod_nofee.png";
										    	frmIBEditFutureTopUpPrecnf.image247502979411375.src = "prod_nofee.png";
									 }else if(productIDTP == "221"){
										    	frmIBTopUpViewNEdit.imageFromAccPic.src = "piggyblueico.png";
										    	frmIBEditFutureTopUpPrecnf.image247502979411375.src = "piggyblueico.png";
									 }else if(productIDTP == "203" || productIDTP == "206"){
											    frmIBTopUpViewNEdit.imageFromAccPic.src = "piggyblueico.png";
											    frmIBEditFutureTopUpPrecnf.image247502979411375.src = "piggyblueico.png";
									 }else{
										        frmIBTopUpViewNEdit.imageFromAccPic.src = "prod_currentac.png";
										        frmIBEditFutureTopUpPrecnf.image247502979411375.src = "prod_currentac.png";
									 }
							}else if(gblFromAccTypeTP == "DDA"){
										 	frmIBTopUpViewNEdit.imageFromAccPic.src = "prod_currentac.png";
										 	frmIBEditFutureTopUpPrecnf.image247502979411375.src = "prod_currentac.png";  
							}else if(gblFromAccTypeTP == "CDA"){
										    frmIBTopUpViewNEdit.imageFromAccPic.src = "prod_termdeposits.png";
										    frmIBEditFutureTopUpPrecnf.image247502979411375.src = "prod_termdeposits.png";
							}
						*/
						if(gblBillerCompcodeTP == 2151){
						 	forTPCallOnlinePaymentInq();
						}else{
							frmIBTopUpViewNEdit.hboxCustomerName.setVisibility(false);
							frmIBTopUpViewNEdit.show();
							dismissLoadingScreenPopup();
						}
						
                    } else {
                    	if(result["custAcctRec"].length == i+1){ 
								if( !accFound){
									dismissLoadingScreenPopup();
									alert("No Records Found for the given Account num");
								}
						}
                   	}
                }
                
            } else {
                dismissLoadingScreenPopup();
                
            }
           // }
        } else {
            dismissLoadingScreenPopup();
            alert(result["errMsg"]);
        }
    }
}

function forTPCallOnlinePaymentInq(){
	    var inputparam = [];
        inputparam["rqUID"] = "";
        inputparam["EffDt"] = "";
        inputparam["TrnId"] = ""; // change this value when integrated with calender / future transactions page
        inputparam["Amt"] = gblAmtFromServiceTP; /// for billpayment it is 0.00
        inputparam["compCode"] = gblBillerCompcodeTP;
        //inputparam["TranCode"] = gblTransCode;
        //inputparam["AcctId"] = gblFromAccNo;
        //inputparam["AcctTypeValue"] = gblFromAccType;
        inputparam["MobileNumber"] = removeHyphenIB(frmIBTopUpViewNEdit.lblTPRef1Val.text);
		inputparam["BankId"]="011";
		inputparam["BranchId"]= "0001";
        invokeServiceSecureAsync("onlinePaymentInqForEditBP", inputparam, forTPOnlinePaymentInquiryServiceCallBack);
}

function forTPOnlinePaymentInquiryServiceCallBack(status,result){
	if (status == 400) {
		if(result["opstatus"] == 0){
			if (result["OnlinePmtInqRs"] != undefined && result["OnlinePmtInqRs"].length > 0 && result["OnlinePmtInqRs"] != null) {
				if(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length >0 && result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"] != null){
					for(i=0; i < result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length ;i++ ){
					
						if(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "CustomerName"){
							frmIBTopUpViewNEdit.hboxCustomerName.setVisibility(true);
						    frmIBTopUpViewNEdit.labelCustomerNameValue.text = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"]
						    frmIBTopUpViewNEdit.show();
							dismissLoadingScreenPopup();									
						}  
					}
				}
			}else{
				dismissLoadingScreenPopup();
				alert(""+result["errMsg"]);			
			}
		
		}else{
			dismissLoadingScreenPopup();
			alert(""+result["errMsg"]);
		}
	}
} 
function onClickEditButtonViewPage() {

    if( gblEditBillMethodTP != 1){
    	if(!frmIBTopUpViewNEdit.hbxTPEditAmnt.isVisible){
			frmIBTopUpViewNEdit.hbxTPEditAmnt.setVisibility(true);
		}
	}else{
		if(frmIBTopUpViewNEdit.hbxTPEditAmnt.isVisible){
			frmIBTopUpViewNEdit.hbxTPEditAmnt.setVisibility(false);
		}
	}
	if(frmIBTopUpViewNEdit.hbxTPVeiwAmnt.isVisible){
		frmIBTopUpViewNEdit.hbxTPVeiwAmnt.setVisibility(false);
	}
	if(!frmIBTopUpViewNEdit.lblTPeditHrd.isVisible){
		frmIBTopUpViewNEdit.lblTPeditHrd.setVisibility(true);
	}

	if(frmIBTopUpViewNEdit.lblTPViewHrd.isVisible){
		frmIBTopUpViewNEdit.lblTPViewHrd.setVisibility(false);
	}
	if(frmIBTopUpViewNEdit.hbxTPViewHdr.isVisible){
		frmIBTopUpViewNEdit.hbxTPViewHdr.setVisibility(false);
	}
	//New Header Changes
	if(frmIBTopUpViewNEdit.buttonEdit.isVisible){
		frmIBTopUpViewNEdit.buttonEdit.setVisibility(false);
	}
	
	if(frmIBTopUpViewNEdit.buttonDel.isVisible){
		frmIBTopUpViewNEdit.buttonDel.setVisibility(false);
	}
	// --	
	
	if(!frmIBTopUpViewNEdit.btnTPSchedule.isVisible){
		frmIBTopUpViewNEdit.btnTPSchedule.setVisibility(true);
		frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,20,0]
	}
	if(frmIBTopUpViewNEdit.hbxbtnReturn.isVisible){
		frmIBTopUpViewNEdit.hbxbtnReturn.setVisibility(false);
	}
	if(!frmIBTopUpViewNEdit.hbxEditBtns.isVisible){
		frmIBTopUpViewNEdit.hbxEditBtns.setVisibility(true);
	}

	if(!frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.isVisible){
		 frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.setVisibility(true);
	}
    frmIBTopUpViewNEdit.lblTPEditAvailableBalanceVal.text = commaFormatted(gblAvailBalTP) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    if (gblEditBillMethodTP == 0 && gblEditBillerGroupTypeTP == 1) {
        frmIBTopUpViewNEdit.txtTPEditAmtValue.text = frmIBTopUpViewNEdit.lblTPAmtValue.text;
        frmIBTopUpViewNEdit.show();
        frmIBTopUpViewNEdit.txtTPEditAmtValue.setFocus(true)
        dismissLoadingScreenPopup();
    } else if (gblEditBillMethodTP == 1 && gblEditBillerGroupTypeTP == 1) {
        if(!frmIBTopUpViewNEdit.hboxOnlineTP.isVisible){
        	frmIBTopUpViewNEdit.hboxOnlineTP.setVisibility(true);
        }
        frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKey=0;
        frmIBTopUpViewNEdit.comboboxOnlineAmt.setVisibility(true);
        frmIBTopUpViewNEdit.show();
        dismissLoadingScreenPopup();
    } else if (gblEditBillMethodTP == 2 && gblEditBillerGroupTypeTP == 1) {
        frmIBTopUpViewNEdit.txtTPEditAmtValue.text = frmIBTopUpViewNEdit.lblTPAmtValue.text;
        frmIBTopUpViewNEdit.show();
        dismissLoadingScreenPopup();
    } else{
    	if( gblEditBillMethodTP != 1){
    		if(frmIBTopUpViewNEdit.hbxTPEditAmnt.isVisible){
			frmIBTopUpViewNEdit.hbxTPEditAmnt.setVisibility(false);
			}
		}
		if(!frmIBTopUpViewNEdit.hbxTPVeiwAmnt.isVisible){
			frmIBTopUpViewNEdit.hbxTPVeiwAmnt.setVisibility(true);
		}
		if(frmIBTopUpViewNEdit.lblTPeditHrd.isVisible){
			frmIBTopUpViewNEdit.lblTPeditHrd.setVisibility(false);
		}

		if(!frmIBTopUpViewNEdit.lblTPViewHrd.isVisible){
			frmIBTopUpViewNEdit.lblTPViewHrd.setVisibility(true);
		}
		if(!frmIBTopUpViewNEdit.hbxTPViewHdr.isVisible){
			frmIBTopUpViewNEdit.hbxTPViewHdr.setVisibility(true);
		}
		
		//New Header Changes
		if(!frmIBTopUpViewNEdit.buttonEdit.isVisible){
			frmIBTopUpViewNEdit.buttonEdit.setVisibility(true);
		}
		
		if(!frmIBTopUpViewNEdit.buttonDel.isVisible){
			frmIBTopUpViewNEdit.buttonDel.setVisibility(true);
		}
		//--
		
		if(frmIBTopUpViewNEdit.btnTPSchedule.isVisible){
			frmIBTopUpViewNEdit.btnTPSchedule.setVisibility(false);
			frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,0,0]
		}
		if(!frmIBTopUpViewNEdit.hbxbtnReturn.isVisible){
			frmIBTopUpViewNEdit.hbxbtnReturn.setVisibility(true);
		}
		if(frmIBTopUpViewNEdit.hbxEditBtns.isVisible){
			frmIBTopUpViewNEdit.hbxEditBtns.setVisibility(false);
		}

		if(frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.isVisible){
			 frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.setVisibility(false);
		}
    	dismissLoadingScreenPopup();
    	alert("you can't edit this transaction ");
    	
    }
    var activityTypeID = "068";
        var errorCode = "";
        var activityStatus = "00";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBTopUpViewNEdit.lblTPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = frmIBTopUpViewNEdit.lblTPStartOnDateVal.text; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = frmIBTopUpViewNEdit.lblTPRepeatAsVal.text; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmtFromServiceTP; //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
      frmIBTopUpViewNEdit.txtTPEditAmtValue.setFocus(true);
}

/*
   **************************************************************************************
		Module	: checkIBStatusForTpUp
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function checkIBStatusForTpUp() {
    var inputparam = [];
    inputparam["crmId"] = "";
    inputparam["rqUUId"] = "";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("crmProfileInq", inputparam, callBackCrmProfileInqForEditTP);
}
/*
   **************************************************************************************
		Module	: callBackCrmProfileInqForEditBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function callBackCrmProfileInqForEditTP(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var statusCode = result["statusCode"];
            var severity = result["Severity"];
            var statusDesc = result["StatusDesc"];
            if (statusCode != 0) {
                
            } else {

                var ibStat = result["ibUserStatusIdTr"]; 
                if (ibStat != null) {
                    var inputParam = [];
                    inputParam["ibStatus"] = ibStat;
                    invokeServiceSecureAsync("checkIBMBStatus", inputParam, callBackCheckStatusForEditTP);
                }
            }
        } else {
            dismissLoadingScreenPopup();
            alert(result["errMsg"]);
        }
    } else {
        //dismissLoadingScreenPopup();
        if (status == 300) {
            
        }
    }
}
/*
   **************************************************************************************
		Module	: callBackCheckStatusForEditTP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function callBackCheckStatusForEditTP(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            if (result["ibStatusFlag"] == "true") {
                    //alert("Your transaction has been locked, so you can't Edit Top Up Payment");
					//alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
					
                    dismissLoadingScreenPopup();
                    popIBBPOTPLocked.show();
					return false;
                } else {
                    onClickEditButtonViewPage();
                }
        } else {
            dismissLoadingScreenPopup();
            
        }
    } else {
        if (status == 300) {
            
        }
    }
    
}


function onClickNextBtnOnEditPage(){
	 var status = amtValidationonClickNextBtnOnEditPage();
    if (status == true) {
        //From Account 
        frmIBEditFutureTopUpPrecnf.lblTPFromAccNickName.text = frmIBTopUpViewNEdit.lblTPFromAccNickName.text;
        frmIBEditFutureTopUpPrecnf.lblTPFromAccName.text = frmIBTopUpViewNEdit.lblTPFromAccName.text;
        frmIBEditFutureTopUpPrecnf.lblTPFromAccNumber.text = frmIBTopUpViewNEdit.lblTPFromAccNumber.text;
        //To Biller
		if(frmIBEditFutureTopUpPrecnf.lblTPBillerNickName.isVisible){
        	frmIBEditFutureTopUpPrecnf.lblTPBillerNickName.text = frmIBTopUpViewNEdit.lblTPBillerNickName.text;
        }else{
        	frmIBEditFutureTopUpPrecnf.lblTPBillerNickName.setVisibility(false);
        }
        frmIBEditFutureTopUpPrecnf.lblTPBillNameAndCompCode.text = frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text;
        frmIBEditFutureTopUpPrecnf.lblTPRef1.text = frmIBTopUpViewNEdit.lblTPRef1.text;
        frmIBEditFutureTopUpPrecnf.lblTPRef1Val.text = frmIBTopUpViewNEdit.lblTPRef1Val.text;
        
        //Added for CC Masking
        frmIBEditFutureTopUpPrecnf.lblTPRef1ValMasked.text = frmIBTopUpViewNEdit.lblTPRef1ValMasked.text;
        
        //Payment Details
		if(gblEditBillMethodTP != 1){
			var amt = frmIBTopUpViewNEdit.txtTPEditAmtValue.text;
			if(amt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1){
				amt = amt + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			amt = replaceCommon(amt, ",", "");
        	frmIBEditFutureTopUpPrecnf.lblTPAmtValue.text = commaFormatted(amt);
       	}else {
       		var amt = frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKeyValue[1];
       		if(amt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1){
				amt = amt + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
       		
			amt = replaceCommon(amt, ",", "");
       		frmIBEditFutureTopUpPrecnf.lblTPAmtValue.text = amt;
       	}
        frmIBEditFutureTopUpPrecnf.lblTPPaymentFeeVal.text = frmIBTopUpViewNEdit.lblTPPaymentFeeVal.text;
        frmIBEditFutureTopUpPrecnf.lblTPPaymentOrderValue.text = frmIBTopUpViewNEdit.lblTPPaymentOrderValue.text;
        //schedule details 
        frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text = frmIBTopUpViewNEdit.lblTPStartOnDateVal.text;
        frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text = frmIBTopUpViewNEdit.lblTPRepeatAsVal.text;
        frmIBEditFutureTopUpPrecnf.lblEndOnDateVal.text = frmIBTopUpViewNEdit.lblEndOnDateVal.text;
        frmIBEditFutureTopUpPrecnf.lblTPExcuteVal.text = frmIBTopUpViewNEdit.lblTPExcuteVal.text;
        //MyNote
        frmIBEditFutureTopUpPrecnf.lblMyNoteVal.text = replaceHtmlTagChars(frmIBTopUpViewNEdit.lblMyNoteVal.text);
        //Schedule Ref. No.
        frmIBEditFutureTopUpPrecnf.lblTPScheduleRefNoVal.text = frmIBTopUpViewNEdit.lblTPScheduleRefNoVal.text;
        if (frmIBTopUpViewNEdit.lblScheduleTopUp.isVisible) {
            frmIBTopUpViewNEdit.lblScheduleTopUp.setVisibility(false);
        }
        if (frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.isVisible) {
            frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.setVisibility(false);
        }
        //frmIBEditFutureBillPaymentPrecnf.show();
    } else if (status == false) {
        frmIBTopUpViewNEdit.show();
        return false;
    }
	frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
	frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
	frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
    frmIBEditFutureTopUpPrecnf.hbxTPCmpletngHdr.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.imgComplete.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.hbxbtnUpdatedReturn.setVisibility(false);
   // frmIBEditFutureTopUpPrecnf.hbxTPCmpleAvilBal.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.hbxTPSave.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.hbxTPViewHdr.setVisibility(true);
    frmIBEditFutureTopUpPrecnf.hbxOTP.setVisibility(true);
    frmIBEditFutureTopUpPrecnf.txtBxOTP.setFocus(true);
    frmIBEditFutureTopUpPrecnf.hbxEditBtns.setVisibility(true);
    frmIBTopUpViewNEdit.arrowTPScheduleField.setVisibility(false);

}
//gobal variables :

gblAmountSelectedForTP = false;
gblScheduleFreqChangedTP = false;
gblScheduleButtonClickedTP = false;
gblScheduleFreqChangedTP = false;
gblExecutionOnLoadTP ="0";
gblEndDateOnLoadTP ="";
gblEndingFreqOnLoadIBTP = "";

function amtValidationonClickNextBtnOnEditPage() {
    var userEnteredAmtTP = "";
    var amtTP = 0;
    if (gblEditBillMethodTP != 1) {
       	amtTP = frmIBTopUpViewNEdit.txtTPEditAmtValue.text;
       userEnteredAmtTP = amtTP.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim(); 
    } else if (gblEditBillMethodTP == 1){
        amtTP = frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKeyValue[1];
       userEnteredAmtTP = amtTP;//replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim(); 
    }
	
    if (userEnteredAmtTP == "" || userEnteredAmtTP == "0" || userEnteredAmtTP == "0.0") {
        //alert("Please enter a valid amount");
		alert(kony.i18n.getLocalizedString("Error_Invalid_Amount"));
        gblAmountSelectedForTP = false;
        return false;
    } else {
        var amtvldTpUp = amtValidationIB(userEnteredAmtTP);
        if (amtvldTpUp == false) {
            //alert("Please enter a valid amount");
			alert(kony.i18n.getLocalizedString("Error_Invalid_Amount"));
            gblAmountSelectedForTP = false;
            return false;
        } else {
			var amount = frmIBTopUpViewNEdit.lblTPAmtValue.text;
			amount = amount.substring(0, amount.length - 1).trim();
			
            if (userEnteredAmtTP != amount) { // add online amt also
                gblAmountSelectedForTP = true;
            } else {
                gblAmountSelectedForTP = false;
            }
			gblUserEnteredAmtTP = userEnteredAmtTP;
        }
    }
    if(gblScheduleButtonClickedTP){
		gblScheduleButtonClickedTP = false;
		frmIBTopUpViewNEdit.btnTPSchedule.setEnabled(true);
		frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,20,0]
	}
	if(frmIBTopUpViewNEdit.arrowTPScheduleField.isVisible){
    	frmIBTopUpViewNEdit.arrowTPScheduleField.setVisibility(false);
    }
	
   if (gblAmountSelectedForTP == true || gblScheduleFreqChangedTP == true ) {
       // srvTokenSwitchingDummyEditTP();		//callCrmProfileInqServiceForEditTPAmt();
		validateBillerTopUp(userEnteredAmtTP);
   }else {
   		//alert("Please Edit Amount or Schedule Frequency to proceed Further")
		alert(kony.i18n.getLocalizedString("Error_NoEditingDoneBP"));	
   		return false
   }
    return true;
}


function validateBillerTopUp(userEnteredAmtTP){
		var changedAmount=userEnteredAmtTP;
	    var compCodeBillPay = gblBillerCompcodeTP;
        var ref1ValBillPay = frmIBTopUpViewNEdit.lblTPRef1Val.text;
        var ref2ValBillPay="";
        var scheduleDtBillPay = frmIBTopUpViewNEdit.lblTPStartOnDateVal.text;
        var billerType = "TopUp";
        var billerMethod=gblEditBillMethodTP;
        callBillerValidationEditService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, changedAmount, scheduleDtBillPay, billerType,billerMethod);
}
function callCrmProfileInqServiceForEditTPAmt(){
	var inputparam = [];
    inputparam["crmId"] = ""; // check this value as that service is not having preprocess
    inputparam["rqUUId"] = "";
    showLoadingScreenPopup();
    	var editedAmount = "";
		editedAmount = gblUserEnteredAmtTP;  //removeCommaIB(gblUserEnteredAmtTP);
	    editedAmount = editedAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	    
	    if (editedAmount.indexOf(",") != -1) {
       	    editedAmount = parseFloat(replaceCommon(editedAmount, ",", "")).toFixed(2);
   		}else
    	    editedAmount = parseFloat(editedAmount).toFixed(2);
	    
	   /* 
	    editedAmount = parseFloat(editedAmount.toString()); 
	    editedAmount = "" +editedAmount;
	    if(editedAmount.indexOf(".") != -1){
	      var tmp = editedAmount.split(".", 2);
	      if(tmp[1].length == 1){
	       editedAmount = editedAmount + "0";
	       }else if (tmp[1].length > 2){
	        tmp[1] = tmp[1].substring("0", "2")
	        editedAmount = ""
	        editedAmount = tmp[0] +"."+ tmp[1];
	       }
	    }
	    */
    inputparam["amtEdited"] = editedAmount;
	inputparam["billerName"] = frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text.substring(0, frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text.lastIndexOf("("));
    inputparam["billerRef1"] = frmIBTopUpViewNEdit.lblTPRef1Val.text;
     if ((gblAmountSelectedForTP == false && gblScheduleFreqChangedTP == true) || (gblAmountSelectedForTP == true && gblScheduleFreqChangedTP == true)){
     	inputparam["editEditBillPayFlag"] = "freqChangeEdit";
     	inputparam["scheID"] = frmIBTopUpViewNEdit.lblTPScheduleRefNoVal.text.replace("F", "S");
     }else if(gblAmountSelectedForTP == true && gblScheduleFreqChangedTP == false){
     	inputparam["editEditBillPayFlag"] = "amtChangeEdit";
     	inputparam["startOnDate"] = changeDateFormatForService(frmIBTopUpViewNEdit.lblTPStartOnDateVal.text);
     	inputparam["extTrnRefID"] = frmIBTopUpViewNEdit.lblTPScheduleRefNoVal.text.replace("F", "S");
     }
    invokeServiceSecureAsync("crmProfileInq", inputparam, forEditTPCrmProfileInqServiceCallBack);
}

function forEditTPCrmProfileInqServiceCallBack(status,result){
	//tokenFlag = false;
    if (status == 400) {
    	var activityTypeID = "068";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBTopUpViewNEdit.lblTPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = "";
		var activityFlexValues4 = "";
		var activityFlexValues5 = "";		
		if(gblScheduleFreqChangedTP == true){
			activityFlexValues3 = gblStartOnTP + "+"+ frmIBTopUpViewNEdit.lblTPStartOnDateVal.text; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsIBTP+"+"+frmIBTopUpViewNEdit.lblTPRepeatAsVal.text; //Old Frequency + New Frequency (Edit case)
		}else{
			activityFlexValues3 = gblStartOnTP; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsIBTP; //Old Frequency + New Frequency (Edit case) //gblOnLoadRepeatAsIB
		}
		if(gblAmountSelectedForTP == true){
			activityFlexValues5 = gblAmtFromServiceTP +"+"+ gblUserEnteredAmtTP; //Old Amount + New Amount (Edit case)
		}else{
			activityFlexValues5 = gblAmtFromServiceTP ; //Old Amount + New Amount (Edit case)
		}
				
        var logLinkageId = "";
        if (result["opstatus"] == 0) {
            var StatusCode = result["statusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
            	activityStatus = "00";
                var dailyChannelLimit = result["ebMaxLimitAmtCurrent"];
                var fee = frmIBTopUpViewNEdit.lblTPPaymentFeeVal.text;
                dailyChannelLimit = parseFloat(dailyChannelLimit.toString());
       //         var totalamt = parseFloat(gblUserEnteredAmtTP.replace(",","")) + parseFloat(fee.substring(0, fee.length -1).trim());
       			
       			var tmpAmt = gblUserEnteredAmtTP+"";
       			tmpAmt = replaceCommon(tmpAmt, ",", "");
       			
                var totalamt = parseFloat(tmpAmt);
       
                if (dailyChannelLimit < totalamt) {
                	dismissLoadingScreenPopup();
                    //alert("Entered Amount is exceeding the Daily Channel Limit");
					alert(kony.i18n.getLocalizedString("Error_Amount_Daily_Limit"));
                    return false;
                } else {
                	try {
							kony.timer.cancel("OTPTimerReq")
						} catch (e) {
							
						}
						frmIBEditFutureTopUpPrecnf.btnOTPReq.setEnabled(true);
						frmIBEditFutureTopUpPrecnf.txtBxOTP.text = "";
						frmIBEditFutureTopUpPrecnf.btnOTPReq.onClick = onclickRequestBtnEditTPIBForOTP;
						
						
						frmIBEditFutureTopUpPrecnf.hbxOTP.setVisibility(false);
						frmIBEditFutureTopUpPrecnf.hbxEditBtns.setVisibility(false);
						frmIBEditFutureTopUpPrecnf.btnNext.setVisibility(true);
					
						 
					/*	 
                    if (result["tokenDeviceFlag"] == "N") {
                        tokenFlag = false;
                        //gblOTPFlag = true;
                        //	
                        frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(true);
                        frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(true);
                        frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                        frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
                        frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
                        frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
                       // frmIBEditFutureTopUpPrecnf.btnOTPReq.onClick = onclickRequestBtnEditTPIBForOTP;
                    } else {
                        tokenFlag = true;
                        //gblOTPFlag = false;
                        //
                        frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(false);
                        frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(false);
                        frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
                        frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
                        frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
                        frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
                      //  frmIBEditFutureTopUpPrecnf.btnOTPReq.onClick = onclickRequestBtnEditTPIBForOTP;
                        
                    }
                    */
                    frmIBEditFutureTopUpPrecnf.show()
                }
            } else {
            	activityStatus = "02";
            	dismissLoadingScreenPopup();
                alert(" " + StatusDesc);
            }
        }else {
        	activityStatus = "02";
        	dismissLoadingScreenPopup();
        	alert(result["errMsg"]);
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);	
    }
}

/*
   **************************************************************************************
		Module	: onclickRequestBtnEditTPIBForOTP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function onclickRequestBtnEditTPIBForOTP() {
    if (tokenFlag == true) {
        frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
        frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        frmIBEditFutureTopUpPrecnf.txtBxOTP.text = "";
        tokenFlag = false;
    } else {
        callGenRefNoServiceForEditTP();
    }
}
function callGenRefNoServiceForEditTP() {
    var locale = kony.i18n.getCurrentLocale();
    var eventNotificationPolicy;
    var SMSSubject;
    var Channel;
     var currFrm = kony.application.getCurrentForm().id;
     if(currFrm == "frmIBEditFutureTopUpPrecnf"){
       //alert("in iff frmIBEditFutureTopUpPrecnf");
       frmIBEditFutureTopUpPrecnf.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
       frmIBEditFutureTopUpPrecnf.lblPlsReEnter.text = " "; 
       frmIBEditFutureTopUpPrecnf.hbxOTPincurrect.isVisible = false;
       frmIBEditFutureTopUpPrecnf.hboxBankRefNo.isVisible = true;
       frmIBEditFutureTopUpPrecnf.hboxOTPSent.isVisible = true;     
     }
   
    /*
    if (locale == "en_US") {
        eventNotificationPolicy = "MIB_MobileTopup_EN";
        SMSSubject = "MIB_MobileTopup_EN"; 
    } else {
        eventNotificationPolicy = "MIB_MobileTopup_TH";
        SMSSubject = "MIB_MobileTopup_TH";
    }*/
    Channel = "TopUpPayment";
    var inputParam = [];
    inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP; //check this count value
    inputParam["Channel"] = Channel;
	inputParam["locale"] = locale;
    invokeServiceSecureAsync("generateOTPWithUser", inputParam, callbackGenRefNoServiceForEditTP);
}
/*
   **************************************************************************************
		Module	: callbackGenRefNoServiceForEditTP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
//gblOTPLENGTHEdit
function callbackGenRefNoServiceForEditTP(status, result) {
    if (status == 400) {
    	  if (result["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
     		if (result["errCode"] == "GenOTPRtyErr00002") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
                return false;
            } 
    
        if (result["opstatus"] == 0) {
            if (result["errCode"] == "GenOTPRtyErr00001") {
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (result["errCode"] == "JavaErr00001") {
                showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            
			frmIBEditFutureTopUpPrecnf.hbxOTP.setVisibility(true);
			frmIBEditFutureTopUpPrecnf.txtBxOTP.setFocus(true);
			frmIBEditFutureTopUpPrecnf.hbxEditBtns.setVisibility(true);
			frmIBEditFutureTopUpPrecnf.btnNext.setVisibility(false);
			
            var reqOtpTimer = kony.os.toNumber(result["requestOTPEnableTime"]);
            gblRetryCountRequestOTP = kony.os.toNumber(result["retryCounterRequestOTP"]);
            gblOTPLENGTHEdit = kony.os.toNumber(result["otpLength"]);
            
            kony.timer.schedule("OTPTimerReq", OTPTimerCallBackEditTP, reqOtpTimer, false);
            frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(true); 
            frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(true);
            frmIBEditFutureTopUpPrecnf.labelBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
            var refVal="";
			for(var d=0;result["Collection1"].length;d++){
				if(result["Collection1"][d]["keyName"] == "pac"){
						refVal=result["Collection1"][d]["ValueString"];
						break;
				}
			}
            frmIBEditFutureTopUpPrecnf.labelBankRefVal.text = refVal;
            //frmIBEditFutureTopUpPrecnf.labelBankRefVal.text = result["bankRefNo"];
            frmIBEditFutureTopUpPrecnf.labelOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsg");
            frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
            frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
            frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
            
            frmIBEditFutureTopUpPrecnf.labelMobileNum.text = "xx-xxx-" + gblPHONENUMBER.substring(6, 10);
            frmIBEditFutureTopUpPrecnf.txtBxOTP.maxTextLength=gblOTPLENGTHEdit;
            //frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = btnIB125Disabled;
			frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotp";
            frmIBEditFutureTopUpPrecnf.btnOTPReq.setEnabled(false);
            
            dismissLoadingScreenPopup();
            frmIBEditFutureTopUpPrecnf.txtBxOTP.setFocus(true);
        } else {
           dismissLoadingScreenPopup();	
           alert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + result["errMsg"]);
        }
    }
}


function OTPTimerCallBackEditTP() {
    //frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = btnIB125active;
    frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
    frmIBEditFutureTopUpPrecnf.btnOTPReq.setEnabled(true);
    frmIBEditFutureTopUpPrecnf.btnOTPReq.onClick = callGenRefNoServiceForEditTP;
    try {
        kony.timer.cancel("OTPTimerReq")
    } catch (e) {
        
    }
}

gblVerifyOTPEditTP = 0;

function callBackVerifyOTPForEditTPIB(status, result) {
	dismissLoadingScreenPopup();
	if (status == 400) {
		if (result["opstatusVPX"] == 0) {
				
				gblVerifyOTPEditTP = "0";
				frmIBEditFutureTopUpPrecnf.txtBxOTP.text ="";
				frmIBEditFutureTopUpPrecnf.btnOTPReq.setEnabled(true);
				 try {
        			kony.timer.cancel("OTPTimerReq")
    			 } catch (e) {
                    
                 }
				editTopUPFlow(result);
			//}
		}else {
			frmIBEditFutureTopUpPrecnf.txtBxOTP.text ="";
			if(result["opstatusVPX"] == 8005){
				if (result["errCode"] == "VrfyOTPErr00001") {
					gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
					dismissLoadingScreenPopup();
					//showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);//commented by swapna.
                    frmIBEditFutureTopUpPrecnf.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBEditFutureTopUpPrecnf.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBEditFutureTopUpPrecnf.hbxOTPincurrect.isVisible = true;
                    frmIBEditFutureTopUpPrecnf.hboxBankRefNo.isVisible = false;
                    frmIBEditFutureTopUpPrecnf.hboxOTPSent.isVisible = false;
                    frmIBEditFutureTopUpPrecnf.txtBxOTP.text = "";
                    frmIBEditFutureTopUpPrecnf.txtBxOTP.setFocus(true);
					return false;
				}else if (result["errCode"] == "VrfyOTPErr00004") {
					dismissLoadingScreenPopup();
					showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
					
					return false;
				}
				else if (result["errCode"] == "VrfyOTPErr00002") {
					dismissLoadingScreenPopup();
					
					handleOTPLockedIB(result);
					return false;
				}else if (result["errCode"] == "EditPaymentErr001") {
					dismissLoadingScreenPopup();
					showAlert("Transaction Can Not Be Preocessed", null);
					return false;
				}
			
			}else {
			        frmIBEditFutureTopUpPrecnf.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBEditFutureTopUpPrecnf.lblPlsReEnter.text = " "; 
                    frmIBEditFutureTopUpPrecnf.hbxOTPincurrect.isVisible = false;
                    frmIBEditFutureTopUpPrecnf.hboxBankRefNo.isVisible = true;
                    frmIBEditFutureTopUpPrecnf.hboxOTPSent.isVisible = true;
				  dismissLoadingScreenPopup();
			   //showAlert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + result["errMsg"], null);
			   showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		     }

		}
	}
	else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			//showCommonAlertMYA("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		}
	}
}


/*function callBackVerifyTokenForEditTPIB(status,result){
if (status == 400) {
        if (result["opstatus"] == 0) {
			alert("To be implemented");
           }
    }
}*/

function editTopUPFlow(result){

	if (gblAmountSelectedForTP == true && gblScheduleFreqChangedTP == true){
  		callBackPmtCanServiceForEditTPIB(result);
  	}else if (gblAmountSelectedForTP == false && gblScheduleFreqChangedTP == true){
  		callBackPmtCanServiceForEditTPIB(result);
  	}else  if (gblAmountSelectedForTP == true && gblScheduleFreqChangedTP == false){
  		callBackPmtModServiceForEditTPIB(result);
  		gblAmountSelectedForTP = false;
  		gblScheduleFreqChangedTP = false;
  	}
}


function callBackPmtCanServiceForEditTPIB(result){

        if (result["opstatusCancel"] == 0) {
            var StatusCode = result["statusCodeCancel"];
            var StatusDesc = result["statusDescCancel"];
            if (StatusCode == 0) {
				callBackPmtAddServiceForEditTPIB(result);
                 var refNum = result["transRefNum"];
            	frmIBEditFutureTopUpPrecnf.lblTPScheduleRefNoVal.text = refNum ;
            } else {
                alert(" " + StatusDesc);
                return false;
            }
        } else {
            alert("  " + result["opstatus"]);
        }
}


function callBackPmtAddServiceForEditTPIB(result){
        gblScheduleFreqChangedTP = false;
        gblAmountSelectedForTP = false;
        if (result["opstatusPayAdd"] == 0) {
            var StatusCode = result["statusCodeAdd"];
            var StatusDesc = result["statusDescAdd"];
            if (StatusCode == 0) {
            	frmIBEditFutureTopUpPrecnf.lblTPPaymentOrderValue.text = result["currentTime"];
            	populateEditTopUpCompleteScreen();
            } else {
                alert("  " + StatusDesc);
                return false;
            }
        } else {
            alert(" " + result["opstatusPayAdd"]);
        }
}

function populateEditTopUpCompleteScreen(){
	//frmIBEditFutureTopUpPrecnf.lblTPBalAfterPayVal.text = commaFormatted(gblAvailBalTP) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");	
    frmIBEditFutureTopUpPrecnf.hbxTPCmpletngHdr.setVisibility(true);
    frmIBEditFutureTopUpPrecnf.imgComplete.setVisibility(true);
    frmIBEditFutureTopUpPrecnf.hbxbtnUpdatedReturn.setVisibility(true);
    //frmIBEditFutureTopUpPrecnf.hbxTPCmpleAvilBal.setVisibility(true);
    frmIBEditFutureTopUpPrecnf.hbxTPSave.setVisibility(true);
    frmIBEditFutureTopUpPrecnf.hbxTPViewHdr.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.hbxOTP.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.hbxEditBtns.setVisibility(false);
    
    frmIBEditFutureTopUpPrecnf.btnNext.setVisibility(false);
    
    //frmIBEditFutureTopUpPrecnf.hbxAdd.setVisibility(true);
    //hbxAdv
    campaginService("imgAd1","frmIBEditFutureTopUpPrecnf","I");
    frmIBEditFutureTopUpPrecnf.hbxAdv.setVisibility(true);
    
}



function callBackPmtModServiceForEditTPIB(result){
        if (result["opstatusBP"] == 0) {
            var StatusCode = result["StatusCode"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
            	populateEditTopUpCompleteScreen();
            } else {
                alert(" " + StatusDesc);
                return false;
            }
        } else {
            alert(" " + result["opstatusBP"]);
        }
}

/*
   **************************************************************************************
		Module	: repeatScheduleFrequencyForTP
		Author  : Kony
		Date    : 
		Purpose : To know which frequency button is clicked (Daily,Weekly,Monthly or Yearly)
	***************************************************************************************
*/
var onClickRepeatAsTP = "";
var onClickEndFreqTP = "";
function repeatScheduleFrequencyForTP(eventObject) {
    frmIBTopUpViewNEdit.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTopUpViewNEdit.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTopUpViewNEdit.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTopUpViewNEdit.btnYearly.skin = "btnIbTab4RightNrml";
    var btnEventId = eventObject.id;
    if (kony.string.equalsIgnoreCase(btnEventId, "btnDaily")) {
        eventObject.skin = "btnIBTab4LeftFocus";
        onClickRepeatAsTP = "Daily";
    } else if (kony.string.equalsIgnoreCase(btnEventId, "btnWeekly")) {
        eventObject.skin = "btnIBTab4MidFocus";
        onClickRepeatAsTP = "Weekly";
    } else if (kony.string.equalsIgnoreCase(btnEventId, "btnMonthly")) {
        eventObject.skin = "btnIBTab4MidFocus";
        onClickRepeatAsTP = "Monthly";
    } else if (kony.string.equalsIgnoreCase(btnEventId, "btnYearly")) {
        eventObject.skin = "btnIBTab4RightFocus";
        onClickRepeatAsTP = "Yearly";
    }
}
/*
   **************************************************************************************************
		Module	: endingScheduleForTP
		Author  : Kony
		Date    : 
		Purpose : To know which button is clicked and changing the skin accordingly(Never,After,EndOn)
	**************************************************************************************************
*/
function endingScheduleForTP(eventObject) {
    frmIBTopUpViewNEdit.btnTPNever.skin = "btnIBTab3LeftNrml";
    frmIBTopUpViewNEdit.btnTPAfter.skin = "btnIBTab3MidNrml";
    frmIBTopUpViewNEdit.btnTPOnDate.skin = "btnIBTab3RightNrml";
    var btnEventId = eventObject.id;
    if (kony.string.equalsIgnoreCase(btnEventId, "btnTPNever")) {
        eventObject.skin = "btnIBTab3LeftFocus";
        onClickEndFreqTP = "Never";
       // if (!frmIBTopUpViewNEdit.lineSchTPClose.isVisible) {
//           // frmIBTopUpViewNEdit.lineSchTPClose.setVisibility(true);
//        }
        if (frmIBTopUpViewNEdit.hbxTPexecutiontimes.isVisible) {
            frmIBTopUpViewNEdit.hbxTPexecutiontimes.setVisibility(false);
        }
        if (frmIBTopUpViewNEdit.hbxTPUntil.isVisible) {
            frmIBTopUpViewNEdit.hbxTPUntil.setVisibility(false);
        }
		//frmIBTopUpViewNEdit.calendarEndOn.date = currentDateForcalender();
       // frmIBTopUpViewNEdit.txtBPExecutiontimesval.text = "";
    } else if (kony.string.equalsIgnoreCase(btnEventId, "btnTPAfter")) {
        eventObject.skin = "btnIBTab3MidFocus";
        onClickEndFreqTP = "After";
        if (!frmIBTopUpViewNEdit.hbxTPexecutiontimes.isVisible) {
            frmIBTopUpViewNEdit.hbxTPexecutiontimes.setVisibility(true);
        }
        if (frmIBTopUpViewNEdit.hbxTPUntil.isVisible) {
            frmIBTopUpViewNEdit.hbxTPUntil.setVisibility(false);
        }
		var afterValue = frmIBTopUpViewNEdit.lblTPExcuteVal.text;
		if(afterValue != "-"){
			frmIBTopUpViewNEdit.txtTPExecutiontimesval.text  = frmIBTopUpViewNEdit.lblTPExcuteVal.text;
		}else{
            frmIBTopUpViewNEdit.txtTPExecutiontimesval.text  = "";
        }
    } else if (kony.string.equalsIgnoreCase(btnEventId, "btnTPOnDate")) {
        eventObject.skin = "btnIBTab3RightFocus";
        onClickEndFreqTP = "OnDate";
        if (frmIBTopUpViewNEdit.hbxTPexecutiontimes.isVisible) {
            frmIBTopUpViewNEdit.hbxBPexecutiontimes.setVisibility(false);
        }
        if (!frmIBTopUpViewNEdit.hbxTPUntil.isVisible) {
            frmIBTopUpViewNEdit.hbxTPUntil.setVisibility(true);
        }
		var onDateValue = frmIBTopUpViewNEdit.lblEndOnDateVal.text;
		if(onDateValue != "-"){
			 frmIBTopUpViewNEdit.calendarEndOn.date = frmIBTopUpViewNEdit.lblEndOnDateVal.text;
		}else{
            frmIBTopUpViewNEdit.calendarEndOn.date = currentDateForcalender();
        }
		
    }
}

//gblScheduleButtonClickedTP = false

function onClickScheduleButtonTP() {
	if(!gblScheduleButtonClickedTP){
		gblScheduleButtonClickedTP = true;
		frmIBTopUpViewNEdit.btnTPSchedule.setEnabled(false);
		frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,0,0]
	}
    frmIBTopUpViewNEdit.lblScheduleTopUp.setVisibility(true);
    frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.setVisibility(true);
    frmIBTopUpViewNEdit.arrowTPScheduleField.setVisibility(true);
	
	
	frmIBTopUpViewNEdit.calendarStartOn.validStartDate = currentDateForcalender();
	frmIBTopUpViewNEdit.calendarEndOn.validStartDate = currentDateForcalender();
	if(repeatAsTP == "" && endFreqSaveTP == ""){
		var d = frmIBTopUpViewNEdit.lblTPStartOnDateVal.text;
		d = d.split("/");
    	frmIBTopUpViewNEdit.calendarStartOn.dateComponents = [d[0],d[1],d[2]];   //frmIBTopUpViewNEdit.lblTPStartOnDateVal.text;
		onLoadShowRepeatFreqTP(gblOnLoadRepeatAsIBTP);
		onLoadShowEndFreqTP(gblEndingFreqOnLoadIBTP);
	}else{
		var d1 = frmIBTopUpViewNEdit.lblTPStartOnDateVal.text;
		d1 = d1.split("/");
		frmIBTopUpViewNEdit.calendarStartOn.dateComponents = [d1[0],d1[1],d1[2]];	//frmIBTopUpViewNEdit.lblTPStartOnDateVal.text;
		onLoadShowRepeatFreqTP(repeatAsTP);
		onLoadShowEndFreqTP(endFreqSaveTP);
	}
	//New
	gblTemprepeatAsTP = repeatAsTP;
	gblTempendFreqSaveTP = endFreqSaveTP;
}

function onLoadShowEndFreqTP(endingFreq) {
	var gblEndValTempTP;
    frmIBTopUpViewNEdit.btnTPNever.skin = "btnIBTab3LeftNrml";
    frmIBTopUpViewNEdit.btnTPAfter.skin = "btnIBTab3MidNrml";
    frmIBTopUpViewNEdit.btnTPOnDate.skin = "btnIBTab3RightNrml";
    if (endingFreq == "OnDate") {
         gblEndValTempTP = "OnDate"; //kony.i18n.getLocalizedString("Transfer_OnDate");
        frmIBTopUpViewNEdit.hbxTPUntil.setVisibility(true);
        frmIBTopUpViewNEdit.btnTPOnDate.skin = "btnIBTab3RightFocus";
        // get endOn date and display
		var d2 = frmIBTopUpViewNEdit.lblEndOnDateVal.text;
		d2 = d2.split("/");
        frmIBTopUpViewNEdit.calendarEndOn.dateComponents = [d2[0],d2[1],d2[2]]; //frmIBTopUpViewNEdit.lblEndOnDateVal.text;
        if (frmIBTopUpViewNEdit.hbxTPexecutiontimes.isVisible) {
            frmIBTopUpViewNEdit.hbxTPexecutiontimes.setVisibility(false);
        }
    } else if (endingFreq == "After") {
        gblEndValTempTP = "After"; // kony.i18n.getLocalizedString("Transfer_After");
        frmIBTopUpViewNEdit.hbxTPexecutiontimes.setVisibility(true);
        frmIBTopUpViewNEdit.lineSchTPClose.setVisibility(true);
        frmIBTopUpViewNEdit.btnTPAfter.skin = "btnIBTab3MidFocus";
        // get execution times and display
        frmIBTopUpViewNEdit.txtTPExecutiontimesval.text = frmIBTopUpViewNEdit.lblTPExcuteVal.text;
        if (frmIBTopUpViewNEdit.hbxTPUntil.isVisible) {
            frmIBTopUpViewNEdit.hbxTPUntil.setVisibility(false);
        }
    } else if (endingFreq == "Never") {
         gblEndValTempTP = "Never";//kony.i18n.getLocalizedString("Transfer_Never");
        frmIBTopUpViewNEdit.btnTPNever.skin = "btnIBTab3LeftFocus";
        if (frmIBTopUpViewNEdit.hbxTPexecutiontimes.isVisible) {
            frmIBTopUpViewNEdit.hbxTPexecutiontimes.setVisibility(false);
        }
        if (frmIBTopUpViewNEdit.hbxTPUntil.isVisible) {
            frmIBTopUpViewNEdit.hbxTPUntil.setVisibility(false);
        }

    }else{
		//if (frmIBTopUpViewNEdit.hbxTPUntil.isVisible) {
            frmIBTopUpViewNEdit.hbxTPUntil.setVisibility(false);
       // }
		//if (frmIBTopUpViewNEdit.hbxTPexecutiontimes.isVisible) {
            frmIBTopUpViewNEdit.hbxTPexecutiontimes.setVisibility(false);
      //  }
	
	}
}

function onLoadShowRepeatFreqTP(repeatFreq) {
    frmIBTopUpViewNEdit.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTopUpViewNEdit.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTopUpViewNEdit.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTopUpViewNEdit.btnYearly.skin = "btnIbTab4RightNrml";
    if (kony.string.equalsIgnoreCase(repeatFreq, "Daily")) {
        frmIBTopUpViewNEdit.lblTPEnding.setVisibility(true);
        frmIBTopUpViewNEdit.hbxTPEndingBtns.setVisibility(true);
        //frmIBTopUpViewNEdit.lineSchTPClose.setVisibility(true);
        frmIBTopUpViewNEdit.btnDaily.skin = "btnIBTab4LeftFocus";
    } else if (kony.string.equalsIgnoreCase(repeatFreq, "Weekly")) {
        frmIBTopUpViewNEdit.lblTPEnding.setVisibility(true);
        frmIBTopUpViewNEdit.hbxTPEndingBtns.setVisibility(true);
        //frmIBTopUpViewNEdit.lineSchTPClose.setVisibility(true);
        frmIBTopUpViewNEdit.btnWeekly.skin = "btnIBTab4MidFocus";
    } else if (kony.string.equalsIgnoreCase(repeatFreq, "Monthly")) {
        frmIBTopUpViewNEdit.lblTPEnding.setVisibility(true);
        frmIBTopUpViewNEdit.hbxTPEndingBtns.setVisibility(true);
       // frmIBTopUpViewNEdit.lineSchTPClose.setVisibility(true);
        frmIBTopUpViewNEdit.btnMonthly.skin = "btnIBTab4MidFocus";
    } else if (kony.string.equalsIgnoreCase(repeatFreq, "Yearly")) {
        frmIBTopUpViewNEdit.lblTPEnding.setVisibility(true);
        frmIBTopUpViewNEdit.hbxTPEndingBtns.setVisibility(true);
        //frmIBTopUpViewNEdit.lineSchTPClose.setVisibility(true);
        frmIBTopUpViewNEdit.btnYearly.skin = "btnIBTab4RightFocus";
    }else{
        frmIBTopUpViewNEdit.lblTPEnding.setVisibility(false);
        frmIBTopUpViewNEdit.hbxTPEndingBtns.setVisibility(false);
       // frmIBTopUpViewNEdit.lineSchTPClose.setVisibility(true);
         frmIBTopUpViewNEdit.hbxTPUntil.setVisibility(false);
         frmIBTopUpViewNEdit.hbxTPexecutiontimes.setVisibility(false);
	}
}


/*
   **************************************************************************************
		Module	: editBPscheduleSaveValidationsTP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/ /*
	gblscheduleRepeatasEditFlagTP = false ;
	gblscheduleEndingEditFlagTP = false ;	
	gblscheduleStartOnEditFlagTP = false;
	*/

	var repeatAsTP = "";
	var endFreqSaveTP = "";
function editScheduleSaveValidationsTP() {
	//gblscheduleRepeatasEditFlag = true ;
	//gblscheduleEndingEditFlag = true ;	
	var	scheduleRepeatasEditFlagTP = false ;
	var scheduleEndingEditFlagTP = false ;	
	var scheduleStartOnEditFlagTP = false;
	var startOnDateTP = frmIBTopUpViewNEdit.calendarStartOn.formattedDate;
	var executionNumberOftimesTP = "";
	var endOnDateTP = "";
	frmIBTopUpViewNEdit.imgTMB.setVisibility(false);

	if(gblScheduleButtonClickedTP){
		gblScheduleButtonClickedTP = false;
		frmIBTopUpViewNEdit.btnTPSchedule.setEnabled(true);
		frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,20,0]
	}
	
	//check if local variable is updated with the latest value else work with global variable
	//OnClickRepeatAs - repeat as (daily,weekly,monthly,yearly) -local variable
	//OnClickEndFreq - end freq (never,after and ondate) - local variable
	//gblOnLoadRepeatAsIB - repeat As - global variable
	//gblEndingFreqOnLoadIB - end freq - global variable
	
	if(onClickRepeatAsTP != gblOnLoadRepeatAsIBTP && onClickRepeatAsTP != "" ){
		scheduleRepeatasEditFlagTP = true;
	}else{
		scheduleRepeatasEditFlagTP  = false;
	}
	if(onClickEndFreqTP != gblEndingFreqOnLoadIBTP && onClickEndFreqTP != ""){
		scheduleEndingEditFlagTP = true;
	}else{
		scheduleEndingEditFlagTP = false;
	}
	if(onClickRepeatAsTP != ""){
		repeatAsTP = onClickRepeatAsTP;
	}else{
		repeatAsTP = gblOnLoadRepeatAsIBTP;
	}
	if(onClickEndFreqTP != ""){
		endFreqSaveTP = onClickEndFreqTP;
	}else {
		endFreqSaveTP = gblEndingFreqOnLoadIBTP;
	}
	if(repeatAsTP != "Once"){
		if(endFreqSaveTP == ""){
			//alert(" Please select the ending frequency");
			alert(kony.i18n.getLocalizedString("Error_NoEndingValueSelected"));
			repeatAsTP = "";
			scheduleRepeatasEditFlagTP = false ;
			scheduleEndingEditFlagTP = false ;	
			return false;
		}
	}

	frmIBTopUpViewNEdit.lblTPStartOnDateVal.text = startOnDateTP;
	
	if(repeatAsTP == "Once" && endFreqSaveTP == ""){
		frmIBTopUpViewNEdit.lblEndOnDateVal.text = frmIBTopUpViewNEdit.lblTPStartOnDateVal.text;
		frmIBTopUpViewNEdit.lblTPExcuteVal.text = "1";
		frmIBTopUpViewNEdit.lblTPRepeatAsVal.text = "Once"; 			
	}
	
	if(startOnDateTP == null || startOnDateTP == ""){
		//alert("Please select valid start date"); 
		alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
		return false;
	}else{
		var curDt = currentDate();
		if( parseDate(curDt) >= parseDate(startOnDateTP)){ 
			alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
			return false;
		}
	}
	
	if(startOnDateTP == gblStartOnTP){
		scheduleStartOnEditFlagTP = false;
	}else {
		var curDt = currentDate();
		if(parseDate(startOnDateTP) > parseDate(curDt)){ 
			scheduleStartOnEditFlagTP = true;
		}else {
			//alert("Start Date Should be Greater than Current Date");
			alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
			return false;
		}
	}
	
	if (kony.string.equalsIgnoreCase(endFreqSaveTP, "Never")) {

		if(!frmIBTopUpViewNEdit.hboxRepeatAs.isVisible){
			frmIBTopUpViewNEdit.hboxRepeatAs.setVisibility(true);
		}
		if(!frmIBTopUpViewNEdit.hboxExecute.isVisible){
    		frmIBTopUpViewNEdit.hboxExecute.setVisibility(true);
    	}
    	if(!frmIBTopUpViewNEdit.hboxEndOn.isVisible){
    		frmIBTopUpViewNEdit.hboxEndOn.setVisibility(true);
    	}
		frmIBTopUpViewNEdit.lblEndOnDateVal.text = "-";
    	frmIBTopUpViewNEdit.lblTPExcuteVal.text = "-";
	} else if (kony.string.equalsIgnoreCase(endFreqSaveTP, "After")) {
		executionNumberOftimesTP = frmIBTopUpViewNEdit.txtTPExecutiontimesval.text;
		if(executionNumberOftimesTP == "" || executionNumberOftimesTP == "0" || executionNumberOftimesTP == "0.00"){
			scheduleRepeatasEditFlagTP = false ;
			scheduleEndingEditFlagTP = false ;	
			//alert("please enter valid number of times");
			alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes"));
			endFreqSaveTP = "";
			repeatAsTP = "";
			return false;
		}
		
		if(executionNumberOftimesTP > 99){
			scheduleRepeatasEditFlagTP = false ;
			scheduleEndingEditFlagTP = false ;	
			//alert("please enter valid number of times");
			alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
			endFreqSaveTP = "";
			repeatAsTP = "";
			return false;
		}
		
		if(kony.string.isNumeric(executionNumberOftimesTP) == true){
		if(executionNumberOftimesTP > gblExecutionOnLoadTP || executionNumberOftimesTP < gblExecutionOnLoadTP  ){ //if selected execution times is greater than or less than the execution times from service then set the flag to true
			scheduleEndingEditFlagTP = true ;	
		}
		}else{
			alert(kony.i18n.getLocalizedString("keyIBOtpNumeric"));
			return false;
		}
		endOnDateTP = endOnDateCalculator(startOnDateTP,executionNumberOftimesTP,repeatAsTP);
		frmIBTopUpViewNEdit.lblEndOnDateVal.text = endOnDateTP;
		//frmIBTopUpViewNEdit.lblBPStartOnDateVal.text = startOnDateTP;
		frmIBTopUpViewNEdit.lblTPExcuteVal.text = executionNumberOftimesTP;
		if(!frmIBTopUpViewNEdit.hboxRepeatAs.isVisible){
			frmIBTopUpViewNEdit.hboxRepeatAs.setVisibility(true);
		}
		if(!frmIBTopUpViewNEdit.hboxExecute.isVisible){
    		frmIBTopUpViewNEdit.hboxExecute.setVisibility(true);
    	}
    	if(!frmIBTopUpViewNEdit.hboxEndOn.isVisible){
    		frmIBTopUpViewNEdit.hboxEndOn.setVisibility(true);
    	}
	} else if (kony.string.equalsIgnoreCase(endFreqSaveTP, "OnDate")) {
		endOnDateTP = frmIBTopUpViewNEdit.calendarEndOn.formattedDate;
		if(endOnDateTP == null || endOnDateTP == ""){
			//alert("Please select valid end date"); 
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));
			return false;
		}
		if (parseDate(endOnDateTP) <= parseDate(startOnDateTP)) {
			scheduleRepeatasEditFlagTP = false ;
			scheduleEndingEditFlagTP = false ;	
			//alert("End date should be greater than Start Date");
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));			
			return false;
		}
		if(parseDate(endOnDateTP)> parseDate(gblEndDateOnLoadTP) || parseDate(endOnDateTP)< parseDate(gblEndDateOnLoadTP)){//if selected end date is greater than or less than the end date from service then set the flag to true
			scheduleEndingEditFlagTP = true ;	
		}
		executionNumberOftimesTP = numberOfExecution(startOnDateTP, frmIBTopUpViewNEdit.calendarEndOn.formattedDate,repeatAsTP); 
		
		if (executionNumberOftimesTP > 99) {
			scheduleRepeatasEditFlagTP = false ;
			scheduleEndingEditFlagTP = false ;	
			//alert("End date should be greater than Start Date");
			alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));			
			return false;
		}
		
		frmIBTopUpViewNEdit.lblEndOnDateVal.text = endOnDateTP;
		//frmIBTopUpViewNEdit.lblBPStartOnDateVal.text = startOnDateTP;
		frmIBTopUpViewNEdit.lblTPExcuteVal.text = executionNumberOftimesTP;
		if(!frmIBTopUpViewNEdit.hboxRepeatAs.isVisible){
			frmIBTopUpViewNEdit.hboxRepeatAs.setVisibility(true);
		}
		if(!frmIBTopUpViewNEdit.hboxExecute.isVisible){
    		frmIBTopUpViewNEdit.hboxExecute.setVisibility(true);
    	}
    	if(!frmIBTopUpViewNEdit.hboxEndOn.isVisible){
    		frmIBTopUpViewNEdit.hboxEndOn.setVisibility(true);
    	}
	} 
	
	if (frmIBTopUpViewNEdit.lblScheduleTopUp.isVisible) {
        frmIBTopUpViewNEdit.lblScheduleTopUp.setVisibility(false);
    }
    if (frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.isVisible) {
        frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.setVisibility(false);
    }
    if(frmIBTopUpViewNEdit.arrowTPScheduleField.isVisible){
    	frmIBTopUpViewNEdit.arrowTPScheduleField.setVisibility(false);
    }
    
    frmIBTopUpViewNEdit.lblTPRepeatAsVal.text=repeatAsTP;
	
	
	
	
	if(scheduleRepeatasEditFlagTP == true || scheduleEndingEditFlagTP == true || scheduleStartOnEditFlagTP == true){
		gblScheduleFreqChangedTP = true;
	}else{
		gblScheduleFreqChangedTP = false;
	}
	
	frmIBTopUpViewNEdit.imgTMB.setVisibility(true);

 }
 
 
 
 /*
   **************************************************************************************
		Module	: cancelEditScheTP
		Author  : Kony
		Date    : 
		Purpose : To reset the editted future bill payment
	***************************************************************************************
*/
function cancelEditScheTP() {
	frmIBTopUpViewNEdit.imgTMB.setEnabled(false);
	if(gblScheduleButtonClickedTP){
		gblScheduleButtonClickedTP = false;
		frmIBTopUpViewNEdit.btnTPSchedule.setEnabled(true);
		frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,20,0]
	}
    if (frmIBTopUpViewNEdit.lblScheduleTopUp.isVisible) {
        frmIBTopUpViewNEdit.lblScheduleTopUp.setVisibility(false);
    }
    if (frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.isVisible) {
        frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.setVisibility(false);
    }
	if(!frmIBTopUpViewNEdit.hboxExecute.isVisible){
    	frmIBTopUpViewNEdit.hboxExecute.setVisibility(true);
    }
    if(!frmIBTopUpViewNEdit.hboxEndOn.isVisible){
    	frmIBTopUpViewNEdit.hboxEndOn.setVisibility(true);
    }
    if(frmIBTopUpViewNEdit.arrowTPScheduleField.isVisible){
    	frmIBTopUpViewNEdit.arrowTPScheduleField.setVisibility(false);
    }
   	
	
	
    if(repeatAsTP == "" || endFreqSaveTP == ""){
		//frmIBTopUpViewNEdit.lblTPStartOnDateVal.text= gblStartOnTP;
		if(gblEndingFreqOnLoadIBTP == "Never"){
			frmIBTopUpViewNEdit.lblTPStartOnDateVal.text= gblStartOnTP;
			frmIBTopUpViewNEdit.lblEndOnDateVal.text = "-";
			frmIBTopUpViewNEdit.lblTPExcuteVal.text = "-";
			frmIBTopUpViewNEdit.lblTPRepeatAsVal.text=gblOnLoadRepeatAsIBTP;
			onClickRepeatAsTP = "";
			onClickEndFreqTP = "";
		}else if(gblOnLoadRepeatAsIBTP == "Once"){
			onClickRepeatAsTP = repeatAsTP;					//New changes to handle the once dispaly
			onClickEndFreqTP = endFreqSaveTP;
		}else{
			frmIBTopUpViewNEdit.lblTPStartOnDateVal.text= gblStartOnTP;
			frmIBTopUpViewNEdit.lblEndOnDateVal.text = gblEndDateOnLoadTP;
			frmIBTopUpViewNEdit.lblTPExcuteVal.text = gblExecutionOnLoadTP;
			frmIBTopUpViewNEdit.lblTPRepeatAsVal.text=gblOnLoadRepeatAsIBTP;
			onClickRepeatAsTP = "";
			onClickEndFreqTP = "";
		}
		/*
		frmIBTopUpViewNEdit.lblTPRepeatAsVal.text=gblOnLoadRepeatAsIBTP;
		onClickRepeatAsTP = "";
		onClickEndFreqTP = "";
		*/
	}else{
		onClickRepeatAsTP = repeatAsTP;
		onClickEndFreqTP = endFreqSaveTP;
	}
	
	
	
	
}


/*
   **************************************************************************************
		Module	: cancelEditTPonEditPage
		Author  : Kony
		Date    : 
		Purpose : to cancel the editted future Top up on edit page
	***************************************************************************************
*/
function cancelEditTPonEditPage() {
	frmIBTopUpViewNEdit.comboboxOnlineAmt.setVisibility(false);
    if (frmIBTopUpViewNEdit.hbxTPEditAmnt.isVisible) {
        frmIBTopUpViewNEdit.hbxTPEditAmnt.setVisibility(false);
    }
    if(frmIBTopUpViewNEdit.hboxOnlineTP.isVisible){
    	frmIBTopUpViewNEdit.hboxOnlineTP.setVisibility(false);
    }
    if (!frmIBTopUpViewNEdit.hbxTPVeiwAmnt.isVisible) {
        frmIBTopUpViewNEdit.hbxTPVeiwAmnt.setVisibility(true);
    }
    if (frmIBTopUpViewNEdit.lblTPeditHrd.isVisible) {
        frmIBTopUpViewNEdit.lblTPeditHrd.setVisibility(false);
    }
    if (!frmIBTopUpViewNEdit.lblTPViewHrd.isVisible) {
        frmIBTopUpViewNEdit.lblTPViewHrd.setVisibility(true);
    }
    if (!frmIBTopUpViewNEdit.hbxTPViewHdr.isVisible) {
        frmIBTopUpViewNEdit.hbxTPViewHdr.setVisibility(true);
    }
    //New HeaderChanges
	if(!frmIBTopUpViewNEdit.buttonEdit.isVisible){
		frmIBTopUpViewNEdit.buttonEdit.setVisibility(true);
	}
	
	if(!frmIBTopUpViewNEdit.buttonDel.isVisible){
		frmIBTopUpViewNEdit.buttonDel.setVisibility(true);
	}
    //--
    
    if (frmIBTopUpViewNEdit.btnTPSchedule.isVisible) {
        frmIBTopUpViewNEdit.btnTPSchedule.setVisibility(false);
        frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,0,0]
    }
    if (!frmIBTopUpViewNEdit.hbxbtnReturn.isVisible) {
        frmIBTopUpViewNEdit.hbxbtnReturn.setVisibility(true);
    }
    if (frmIBTopUpViewNEdit.hbxEditBtns.isVisible) {
        frmIBTopUpViewNEdit.hbxEditBtns.setVisibility(false);
    }
    if (frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.isVisible) {
        frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.setVisibility(false);
    }
    if (!frmIBTopUpViewNEdit.hboxRepeatAs.isVisible) {
        frmIBTopUpViewNEdit.hboxRepeatAs.setVisibility(true);
    }
    if (!frmIBTopUpViewNEdit.hboxEndOn.isVisible) {
        frmIBTopUpViewNEdit.hboxEndOn.setVisibility(true);
    }
    if (!frmIBTopUpViewNEdit.hboxExecute.isVisible) {
        frmIBTopUpViewNEdit.hboxExecute.setVisibility(true);
    }
    if (frmIBTopUpViewNEdit.lblScheduleTopUp.isVisible) {
        frmIBTopUpViewNEdit.lblScheduleTopUp.setVisibility(false);
    }
    if (frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.isVisible) {
        frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.setVisibility(false);
    }
    if(frmIBTopUpViewNEdit.arrowTPScheduleField.isVisible){
    	frmIBTopUpViewNEdit.arrowTPScheduleField.setVisibility(false);
    }
	if(gblScheduleButtonClickedTP){
		gblScheduleButtonClickedTP = false;
		frmIBTopUpViewNEdit.btnTPSchedule.setEnabled(true);
		frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,20,0]
	}

		frmIBTopUpViewNEdit.lblTPStartOnDateVal.text= gblStartOnTP;
		if(gblEndingFreqOnLoadIBTP == "Never"){
			frmIBTopUpViewNEdit.lblEndOnDateVal.text = "-";
			frmIBTopUpViewNEdit.lblTPExcuteVal.text = "-";
		}else{
			frmIBTopUpViewNEdit.lblEndOnDateVal.text = gblEndDateOnLoadTP;
			frmIBTopUpViewNEdit.lblTPExcuteVal.text = gblExecutionOnLoadTP;
		}
		frmIBTopUpViewNEdit.lblTPRepeatAsVal.text=gblOnLoadRepeatAsIBTP;
		if(gblEditBillMethodTP != 1){
			frmIBTopUpViewNEdit.txtTPEditAmtValue.text = frmIBTopUpViewNEdit.lblTPAmtValue.text; //for amt display
		}else{
			frmIBTopUpViewNEdit.comboboxOnlineAmt.masterDataMap = comboDataEditTP;
			frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKey=0;
		}
		onClickRepeatAsTP = "";
		onClickEndFreqTP = "";
		endFreqSaveTP="";
		repeatAsTP="";
		gblAmountSelectedForTP = false;
		gblScheduleFreqChangedTP = false;
		
   		frmIBTopUpViewNEdit.show();
}

function cancelEditFutureTP() {
	if (frmIBTopUpViewNEdit.hbxTPEditAmnt.isVisible) {
		frmIBTopUpViewNEdit.hbxTPEditAmnt.setVisibility(false);
	}
	if (frmIBTopUpViewNEdit.hboxOnlineTP.isVisible) {
		frmIBTopUpViewNEdit.hboxOnlineTP.setVisibility(false);
	}
	if (!frmIBTopUpViewNEdit.hbxTPVeiwAmnt.isVisible) {
		frmIBTopUpViewNEdit.hbxTPVeiwAmnt.setVisibility(true);
	}
	if (frmIBTopUpViewNEdit.lblTPeditHrd.isVisible) {
		frmIBTopUpViewNEdit.lblTPeditHrd.setVisibility(false);
	}
	if (!frmIBTopUpViewNEdit.lblTPViewHrd.isVisible) {
		frmIBTopUpViewNEdit.lblTPViewHrd.setVisibility(true);
	}
	if (!frmIBTopUpViewNEdit.hbxTPViewHdr.isVisible) {
		frmIBTopUpViewNEdit.hbxTPViewHdr.setVisibility(true);
	}
	if (frmIBTopUpViewNEdit.btnTPSchedule.isVisible) {
		frmIBTopUpViewNEdit.btnTPSchedule.setVisibility(false);
		frmIBTopUpViewNEdit.lblTPStartOnDateVal.padding = [0,0,0,0]
	}
	//New header Changes
	if(!frmIBTopUpViewNEdit.buttonEdit.isVisible){
		frmIBTopUpViewNEdit.buttonEdit.setVisibility(true);
	}
	
	if(!frmIBTopUpViewNEdit.buttonDel.isVisible){
		frmIBTopUpViewNEdit.buttonDel.setVisibility(true);
	}
	//--	
	
	if (!frmIBTopUpViewNEdit.hbxbtnReturn.isVisible) {
		frmIBTopUpViewNEdit.hbxbtnReturn.setVisibility(true);
	}
	if (frmIBTopUpViewNEdit.hbxEditBtns.isVisible) {
		frmIBTopUpViewNEdit.hbxEditBtns.setVisibility(false);
	}
	if (frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.isVisible) {
		frmIBTopUpViewNEdit.hbxTPAfterEditAvilBal.setVisibility(false);
	}
	if (!frmIBTopUpViewNEdit.hboxRepeatAs.isVisible) {
		frmIBTopUpViewNEdit.hboxRepeatAs.setVisibility(true);
	}
	if (!frmIBTopUpViewNEdit.hboxEndOn.isVisible) {
		frmIBTopUpViewNEdit.hboxEndOn.setVisibility(true);
	}
	if (!frmIBTopUpViewNEdit.hboxExecute.isVisible) {
		frmIBTopUpViewNEdit.hboxExecute.setVisibility(true);
	}
	if (frmIBTopUpViewNEdit.lblScheduleTopUp.isVisible) {
		frmIBTopUpViewNEdit.lblScheduleTopUp.setVisibility(false);
	}
	if (frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.isVisible) {
		frmIBTopUpViewNEdit.hbxEditTPFutureSchedule.setVisibility(false);
	}
	if (frmIBTopUpViewNEdit.arrowTPScheduleField.isVisible) {
		frmIBTopUpViewNEdit.arrowTPScheduleField.setVisibility(false);
	}
	//frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
   // frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
   // frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");

	onClickRepeatAsTP = "";
	onClickEndFreqTP = "";
	endFreqSaveTP="";
	repeatAsTP="";
	frmIBTopUpViewNEdit.lblTPStartOnDateVal.text= gblStartOnTP;
	if(gblEndingFreqOnLoadIBTP == "Never"){
			frmIBTopUpViewNEdit.lblEndOnDateVal.text = "-";
			frmIBTopUpViewNEdit.lblTPExcuteVal.text = "-";
		}else{
			frmIBTopUpViewNEdit.lblEndOnDateVal.text = gblEndDateOnLoadTP;
			frmIBTopUpViewNEdit.lblTPExcuteVal.text = gblExecutionOnLoadTP;
		}
		frmIBTopUpViewNEdit.lblTPRepeatAsVal.text=gblOnLoadRepeatAsIBTP;
		if(gblEditBillMethodTP != 1){
			frmIBTopUpViewNEdit.txtTPEditAmtValue.text = frmIBTopUpViewNEdit.lblTPAmtValue.text; //for amt display
		}else{
			frmIBTopUpViewNEdit.comboboxOnlineAmt.setVisibility(false);
			frmIBTopUpViewNEdit.comboboxOnlineAmt.masterDataMap = comboDataEditTP;
			frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKey=0;
		}
	gblAmountSelectedForTP = false;
	gblScheduleFreqChangedTP = false;
	try {
		kony.timer.cancel("OTPTimerReq")
	} catch (e) {
		
	}
	frmIBEditFutureTopUpPrecnf.btnOTPReq.setEnabled(true);
	frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
}


function deleteFutureTPViewPage() {
	var inputparam = [];
	inputparam["RqUID"] = "";
	inputparam["PmtId"] = "";
	inputparam["scheID"] = frmIBTopUpViewNEdit.lblTPScheduleRefNoVal.text.replace("F", "S"); // get this value from calendar or future transactions

	inputparam["activityFlexValues1"] = "Cancel";
	inputparam["activityFlexValues2"] = frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBTopUpViewNEdit.lblTPRef1Val.text);
	inputparam["activityFlexValues3"] = gblStartOnTP;
	inputparam["activityFlexValues4"] = gblOnLoadRepeatAsIBTP;
	var amt = frmIBTopUpViewNEdit.lblTPAmtValue.text;
	amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	inputparam["activityFlexValues5"] = amt;
	inputparam["isCancelFlow"] = "true";
	inputparam["EditFTFlow"] = "EditTP";
	
	invokeServiceSecureAsync("doPmtCan", inputparam, callBackPmtCanServiceForEditTPIBViewPage)
}

function callBackPmtCanServiceForEditTPIBViewPage(status, result) {
	if (status == 400) {
		var activityTypeID = "068";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Cancel";
       	var activityFlexValues2 =  frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBTopUpViewNEdit.lblTPRef1Val.text);	
		var activityFlexValues3 = gblStartOnTP; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsIBTP; //Old Frequency + New Frequency (Edit case)//gblOnLoadRepeatAsIBTP
		var activityFlexValues5 = frmIBTopUpViewNEdit.lblTPAmtValue.text; //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var ServerStatusCode = result["ServerStatusCode"]
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				activityStatus = "01";
				//Commented as Financial Activity is not required 
				/*
				var finSchdRefId = frmIBTopUpViewNEdit.lblTPScheduleRefNoVal.text.replace("F", "S");
				setFinancialActivityLogFTDelete(finSchdRefId);
				*/
				//alert("Successfully Deleted ");
				alert(kony.i18n.getLocalizedString("ScheduledPayment_Delete"));
				if(gblActivitiesNvgn=="F"){
					invokeFutureInstructionService();
					/*
					frmIBMyActivities.show();
					if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
				    showCalendar(gsSelectedDate,frmIBMyActivities,1);
				    */
				    IBMyActivitiesReloadAndShowCalendar();
					
				}else if(gblActivitiesNvgn=="C"){
					
					/*
					frmIBMyActivities.show();
					if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
				    showCalendar(gsSelectedDate,frmIBMyActivities,1);
					*/
					IBMyActivitiesReloadAndShowCalendar();
				}
				
			} else {
				activityStatus = "02";
				alert("Couldn't delete");
				
			}
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	}
}
function onClickGeneratePDFImageForEditTopUpIB(filetype){
	var inputParam = {};
	//fileTypeTP = filetype;
	var scheduleDetails ="";
	if(frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text == "Once"){
	 	scheduleDetails = frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text + " "+ " Repeat " +frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text;
	 }else if(frmIBEditFutureTopUpPrecnf.lblTPExcuteVal.text == "-"){
	 	scheduleDetails = frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text + " "+ " Repeat " +frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text;
	 } else{
	 	scheduleDetails = frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text +" to " +frmIBEditFutureTopUpPrecnf.lblEndOnDateVal.text + " "+ " Repeat " +frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text +" for "+frmIBEditFutureTopUpPrecnf.lblTPExcuteVal.text+" times " ;
	 }
	 var AccountNo = frmIBEditFutureTopUpPrecnf.lblTPFromAccNumber.text;
	 if(AccountNo.indexOf("-") != -1)AccountNo = replaceCommon(AccountNo, "-", "");
	 var len = AccountNo.length;
	 if(len == 10){
	 	AccountNo = "XXX-X-"+AccountNo.substring(len-6, len-1)+"-X";
	 }else
	 	AccountNo = frmIBEditFutureTopUpPrecnf.lblTPFromAccNumber.text;
	 
	 var pdfImagedata = {
     "AccountNo" : AccountNo,
     "AccountName" : frmIBEditFutureTopUpPrecnf.lblTPFromAccName.text,
     "BillerName" : frmIBEditFutureTopUpPrecnf.lblTPBillNameAndCompCode.text,
     "Ref1Label" : frmIBEditFutureTopUpPrecnf.lblTPRef1.text,
	 "Ref1Value" : frmIBEditFutureTopUpPrecnf.lblTPRef1Val.text,
	 "Amount" : frmIBEditFutureTopUpPrecnf.lblTPAmtValue.text,
	 "Fee" : frmIBEditFutureTopUpPrecnf.lblTPPaymentFeeVal.text,	
	 "PaymentOrderDate"	: frmIBEditFutureTopUpPrecnf.lblTPPaymentOrderValue.text,
	 "PaymentSchedule":scheduleDetails,
	 "MyNote": frmIBEditFutureTopUpPrecnf.lblMyNoteVal.text,
	 "TransactionRefNo" : frmIBEditFutureTopUpPrecnf.lblTPScheduleRefNoVal.text,
	 "localeId":kony.i18n.getCurrentLocale()
    }
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "FutureTopUpPaymentComplete";
   // inputParam["localeId"]= kony.i18n.getCurrentLocale();
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    inputParam["outputtemplatename"] =  "Future_Top_Up_Set_Details_"+kony.os.date("ddmmyyyy");
    
    
    //var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext+"/GenericPdfImageServlet";
	
	//invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
 	
 	//kony.net.invokeServiceAsync(url, inputParam, callbackfunctionForPDFImageTP);
	
	saveFuturePDF(filetype, "068", frmIBEditFutureTopUpPrecnf.lblTPScheduleRefNoVal.text);
}


function shareonFBEditFutureTP() {
	//alert("To be implemented");
	window.open('https://www.facebook.com/sharer/sharer.php?u=www.tmbbank.com', 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
	return false;
	
}


function checkIBStatusForDelTP() {
	var inputparam = [];
	inputparam["crmId"] = "";
	inputparam["rqUUId"] = "";
	showLoadingScreenPopup();
	invokeServiceSecureAsync("crmProfileInq", inputparam, callBackCrmProfileInqForDelTP);
}
/*
   **************************************************************************************
		Module	: callBackCrmProfileInqForDelBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackCrmProfileInqForDelTP(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var statusCode = result["statusCode"];
			var severity = result["Severity"];
			var statusDesc = result["StatusDesc"];
			if (statusCode != 0) {
				
			} else {
				var ibStat = result["ibUserStatusIdTr"];
				if (ibStat != null) {
					var inputParam = [];
					inputParam["ibStatus"] = ibStat;
					invokeServiceSecureAsync("checkIBMBStatus", inputParam, callBackCheckIBStatusForTPDele);
				}
			}
		} else {
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
		}
	} else {
		//dismissLoadingScreenPopup();
		if (status == 300) {
			
		}
	}
}


function callBackCheckIBStatusForTPDele(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["ibStatusFlag"] == "true") {
				//alert("Your transaction has been locked, so you can't Delete ");
				alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
				dismissLoadingScreenPopup();
				return false;
			} else {
				dismissLoadingScreenPopup();
				popIBFutureTPDelete.show(); //popIBFutureTPDelete.show();
			}
		} else {
			dismissLoadingScreenPopup();
			
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
	
}

//Token Changes

function tokenHandlerEditTP(){
	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		srvTokenSwitchingEditTP();
	}else if(gblTokenSwitchFlag ==false && gblSwitchToken == true){
		frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(true);
		frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(true);
		frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
	    dismissLoadingScreenPopup();
	    callGenRefNoServiceForEditTP()
	}else if(gblTokenSwitchFlag ==true && gblSwitchToken == false){
		frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(false);
		frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(false);
		frmIBEditFutureTopUpPrecnf.txtBxOTP.setFocus(true);
		
		frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
		frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keySwitchToSMS");
		frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
	    dismissLoadingScreenPopup();
	    
	    frmIBEditFutureTopUpPrecnf.btnNext.setVisibility(false);
	    frmIBEditFutureTopUpPrecnf.hbxOTP.setVisibility(true);
	    frmIBEditFutureTopUpPrecnf.txtBxOTP.setFocus(true);
	    frmIBEditFutureTopUpPrecnf.hbxEditBtns.setVisibility(true);
	    
	}else if(gblTokenSwitchFlag ==true && gblSwitchToken == true){
		frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(true);
		frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(true);
		frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
	    dismissLoadingScreenPopup();
	    callGenRefNoServiceForEditTP()
	}
}

function srvTokenSwitchingEditTP(){
	var inputParam = [];
	inputParam["crmId"] = gblcrmId;
	showLoadingScreenPopup();
	invokeServiceSecureAsync("tokenSwitching", inputParam, srvTokenSwitchingEditTPCallBack);
}

function srvTokenSwitchingEditTPCallBack(status,callbackResponse){
   if(status  == 400){
   	  if(callbackResponse["opstatus"] == 0){
		//dismissLoadingScreen();
		if(callbackResponse["deviceFlag"].length==0){
			
			//return
		}
		    if(callbackResponse["deviceFlag"].length > 0 ){
				var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
				var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
				var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
				
				if ( tokenFlag=="Y" && ( mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02' ){
					//tokenFlag = true;
					frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(false);
					frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(false);
					frmIBEditFutureTopUpPrecnf.txtBxOTP.setFocus(true);
					frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
					frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
					frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keySwitchToSMS");
					frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
					
					 
				    frmIBEditFutureTopUpPrecnf.btnNext.setVisibility(false);
				    frmIBEditFutureTopUpPrecnf.hbxOTP.setVisibility(true);
				    frmIBEditFutureTopUpPrecnf.txtBxOTP.setFocus(true);
				    frmIBEditFutureTopUpPrecnf.hbxEditBtns.setVisibility(true);
					
					dismissLoadingScreenPopup();
					gblTokenSwitchFlag = true;
					
				} else {
					//tokenFlag = false; // same flag is being used in the flow alredy
					frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(true);
					frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(true);
					frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
					frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
					frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
					
					gblTokenSwitchFlag = false;
					callGenRefNoServiceForEditTP()
					//dismissLoadingScreenPopup();
				}
			}else{
				frmIBEditFutureTopUpPrecnf.hboxBankRefNo.setVisibility(true);
				frmIBEditFutureTopUpPrecnf.hboxOTPSent.setVisibility(true);
				frmIBEditFutureTopUpPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
				frmIBEditFutureTopUpPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
				frmIBEditFutureTopUpPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
				frmIBEditFutureTopUpPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
				
				gblTokenSwitchFlag = false;
				callGenRefNoServiceForEditTP()
			
			}
		}else {
		 	dismissLoadingScreenPopup();
		 }
    }

}


gblVerifyOTPEditTP = 0;
function onClickConfirmEditTPIB() {
	 var otpText = frmIBEditFutureTopUpPrecnf.txtBxOTP.text;
	 var otpLength = otpText.length;
	 var isNumOTP = kony.string.isNumeric(otpText);
	 if (frmIBEditFutureTopUpPrecnf.btnOTPReq.text == kony.i18n.getLocalizedString("keyRequest")){
	  	if(otpText == "" || isNumOTP == false || otpLength != gblOTPLENGTHEdit){
	  		dismissLoadingScreenPopup();
	  		alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;	
	  	}
	  
	 }else if(frmIBEditFutureTopUpPrecnf.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")){
	 	if(otpText == "" || isNumOTP == false){
	 		dismissLoadingScreenPopup();
	 		alert(kony.i18n.getLocalizedString("emptyToken"));
		 	return false;
	 	}
	 }
	
	gblVerifyOTPEditTP = gblVerifyOTPEditTP + 1	 
    srvVerifyPasswordExEditTP();
}
function srvVerifyPasswordExEditTP(){
	showLoadingScreenPopup();
	var inputParam = {};
        var activityFlexValues1 = "";
        var activityFlexValues2 = "";
		var activityFlexValues3 = "";
		var activityFlexValues4 = "";
		var activityFlexValues5 ="";
		inputParam["retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
		inputParam["password"] = frmIBEditFutureTopUpPrecnf.txtBxOTP.text;
	//gblTokenSwitchFlag == false
	if(frmIBEditFutureTopUpPrecnf.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")) {
		inputParam["OTP_TOKEN_FLAG"] = "TOKEN";
	}
	else{
		inputParam["OTP_TOKEN_FLAG"] = "OTP";
		
	}
	 if ((gblAmountSelectedForTP == false && gblScheduleFreqChangedTP == true) || (gblAmountSelectedForTP == true && gblScheduleFreqChangedTP == true)){
	  	inputParam["scheID"] = frmIBEditFutureTopUpPrecnf.lblTPScheduleRefNoVal.text.replace("F", "S");
	   var edAmtTP = frmIBEditFutureTopUpPrecnf.lblTPAmtValue.text;  //removeCommaIB(frmIBEditFutureTopUpPrecnf.lblTPAmtValue.text);
	   // edAmtTP = edAmtTP.substring("0",edAmtTP.length-1);
	    
	    if (edAmtTP.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) 
            edAmtTP = edAmtTP.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	    
	    if (edAmtTP.indexOf(",") != -1) {
       	    edAmtTP = parseFloat(replaceCommon(edAmtTP, ",", "")).toFixed(2);
   		}else
    	    edAmtTP = parseFloat(edAmtTP).toFixed(2);
	    
	    
	  /*  
	    edAmtTP = parseFloat(edAmtTP.toString()); 
	    edAmtTP = "" +edAmtTP;
	    if(edAmtTP.indexOf(".") != -1){
	      var tmp = edAmtTP.split(".", 2);
	      if(tmp[1].length == 1){
	       edAmtTP = edAmtTP + "0";
	       }else if (tmp[1].length > 2){
	        tmp[1] = tmp[1].substring("0", "2")
	        edAmtTP = ""
	        edAmtTP = tmp[0] +"."+ tmp[1];
	       }
	    }
	    */
	    inputParam["amt"] = edAmtTP; 
	    inputParam["fromAcct"] = gblFromAccNoTP; // from Account ID (from PaymentInq)
	    inputParam["fromAcctType"] = gblFromAccTypeTP; // from Account Type (from PaymentInq)
	    inputParam["toAcct"] = gblToAccNoTP; // TO Biller No (from PaymentInq)
	    inputParam["toAccTType"] = gblToAccTypeTP; // To Biller Type ((from PaymentInq)
	    inputParam["dueDate"] = changeDateFormatForService(frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text); // Start On Date
	    inputParam["pmtRefNo1"] =removeHyphenIB(frmIBEditFutureTopUpPrecnf.lblTPRef1Val.text); // Ref 1 value
	    inputParam["custPayeeId"] =gblCustPayIDTP; 
	    inputParam["transCode"] = gblTransCodeTP; //TransCode (from PaymentInq)
	    inputParam["memo"] = frmIBEditFutureTopUpPrecnf.lblMyNoteVal.text; // My Note 
		var ref1TP = frmIBEditFutureTopUpPrecnf.lblTPRef1Val.text;
		ref1TP = replaceCommon(ref1TP, "-", "");
	  
	   if(gblEditBillMethodTP == "1"){
	     inputParam["PmtMiscType"] = "MOBILE";
	     inputParam["MiscText"] = ref1TP;
	   }
		inputParam["billMethod"] = gblEditBillMethodTP;
	    inputParam["pmtMethod"] = gblEditPaymentMethodTP; // get this value from paymentInq
	    inputParam["extTrnRefId"] = frmIBEditFutureTopUpPrecnf.lblTPScheduleRefNoVal.text.replace("F", "S"); // Updated Schedule Ref No	
		inputParam["endFreqSave"] = endFreqSaveTP;	
	    if (endFreqSaveTP == "After") {
	        inputParam["NumInsts"] = frmIBEditFutureTopUpPrecnf.lblTPExcuteVal.text; // Execution times 
	    }
	    if (endFreqSaveTP == "OnDate") {
	        inputParam["FinalDueDt"] = changeDateFormatForService(frmIBEditFutureTopUpPrecnf.lblEndOnDateVal.text); // End On Date
	    }
	    inputParam["frequencyCode"] = "0";
	    var frequency = frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text;
	    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
	        inputParam["dayOfWeek"] = "";
	        inputParam["frequencyCode"] = "1";
	    }
	    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
	        inputParam["dayOfMonth"] = "";
	        inputParam["frequencyCode"] = "2";
	    }
	    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
	        frequency = "Annually";
	        inputParam["dayofMonth"] = "";
	        inputParam["monthofYear"] = "";
	        inputParam["frequencyCode"] = "3";
	    }
	    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
			inputParam["dayOfWeek"] = "";
			inputParam["frequencyCode"] = "4";
		}
	    if(kony.string.equalsIgnoreCase(frequency, "Once")){
			inputParam["freq"] = "once";
		}else{
			inputParam["freq"] = frequency;
		}
   		 inputParam["selectedEditOption"] = "Frequency";
   		  inputParam["transRefType"] = "SU";
   		  var feeAmt = frmIBEditFutureTopUpPrecnf.lblTPPaymentFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0",lenFee-1);
        feeAmt = parseFloat(feeAmt.toString());
         activityFlexValues1 = "Edit";
         activityFlexValues2 = frmIBTopUpViewNEdit.lblTPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBTopUpViewNEdit.lblTPRef1Val.text);    //Biller Name + Ref1
		 activityFlexValues3 ="";
		 activityFlexValues4 ="";
		 activityFlexValues5 ="";
		if(gblScheduleFreqChangedTP == true){
			activityFlexValues3 = gblStartOnTP +"+"+ frmIBTopUpViewNEdit.lblTPStartOnDateVal.text; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsIBTP+"+"+frmIBTopUpViewNEdit.lblTPRepeatAsVal.text; //Old Frequency + New Frequency (Edit case)
		}else{
			activityFlexValues3 = gblStartOnTP; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsIBTP; //Old Frequency + New Frequency (Edit case)
		}
		if(gblAmountSelectedForTP == true){
			 activityFlexValues5 = gblAmtFromServiceTP +"+"+ gblUserEnteredAmtTP; //Old Amount + New Amount (Edit case)
		}else{
			 activityFlexValues5 = gblAmtFromServiceTP; //Old Amount + New Amount (Edit case)
		}
  	}else  if (gblAmountSelectedForTP == true && gblScheduleFreqChangedTP == false){
  		inputParam["selectedEditOption"] = "Amount";
  	    var editedAmount = "";
		editedAmount = frmIBEditFutureTopUpPrecnf.lblTPAmtValue.text;		//removeCommaIB(frmIBEditFutureTopUpPrecnf.lblTPAmtValue.text);
	    
	    if (editedAmount.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) 
            editedAmount = editedAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	    
	    if (editedAmount.indexOf(",") != -1) {
       	    editedAmount = parseFloat(replaceCommon(editedAmount, ",", "")).toFixed(2);
   		}else
    	    editedAmount = parseFloat(editedAmount).toFixed(2);
	    
	    inputParam["extTrnRefID"] = frmIBEditFutureTopUpPrecnf.lblTPScheduleRefNoVal.text.replace("F", "S");
	    inputParam["Amt"] = editedAmount;
	    inputParam["Starton"] = changeDateFormatForService(frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text);
	    var feeAmt = frmIBEditFutureTopUpPrecnf.lblTPPaymentFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0",lenFee-1);
        feeAmt = parseFloat(feeAmt.toString());
         activityFlexValues1 = "Edit";
         activityFlexValues2 = frmIBEditFutureTopUpPrecnf.lblTPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBEditFutureTopUpPrecnf.lblTPRef1Val.text);    //Biller Name + Ref1
		 activityFlexValues3 = frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text; //Old Future Date + New Future Date (Edit case)
		 activityFlexValues4 = frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text; //Old Frequency + New Frequency (Edit case)
		 activityFlexValues5 = gblAmtFromServiceTP +"+"+ gblUserEnteredAmtTP; //Old Amount + New Amount (Edit case)		
  	}
  		inputParam["activityTypeID"] = "068";
		inputParam["activityFlexValues1"] = activityFlexValues1;
		inputParam["activityFlexValues2"] = activityFlexValues2;
		inputParam["activityFlexValues3"] = activityFlexValues3;
		inputParam["activityFlexValues4"] = activityFlexValues4;
		inputParam["activityFlexValues5"] = activityFlexValues5;
	inputParam["notificationType"] = "Email"; // check this : which value we have to pass
	inputParam["phoneNumber"] = gblPHONENUMBER;
	inputParam["emailId"] = gblEmailId;
	inputParam["customerName"] = frmIBEditFutureTopUpPrecnf.lblTPFromAccNickName.text; 
	var fromAccount = frmIBEditFutureTopUpPrecnf.lblTPFromAccNumber.text;
	if(fromAccount.indexOf("-") != -1){
	    fromAccount = replaceCommon(fromAccount, "-", "");
	}
	inputParam["fromAccount"] = fromAccount; 
	inputParam["fromAcctNick"] = frmIBEditFutureTopUpPrecnf.lblTPFromAccNickName.text;
	inputParam["fromAcctName"] = frmIBEditFutureTopUpPrecnf.lblTPFromAccName.text;
	inputParam["billerNick"] = frmIBEditFutureTopUpPrecnf.lblTPBillerNickName.text 
	inputParam["billerName"] = frmIBEditFutureTopUpPrecnf.lblTPBillNameAndCompCode.text 
	inputParam["billerNameTH"]=billerNameTPTH;
	inputParam["ref1EN"] = gblRef1LblEN+" : " + frmIBEditFutureTopUpPrecnf.lblTPRef1Val.text;
    inputParam["ref1TH"] = gblRef1LblTH+" : " + frmIBEditFutureTopUpPrecnf.lblTPRef1Val.text;
	inputParam["ref2EN"] = "";
    inputParam["ref2TH"] = "";
	inputParam["amount"] = frmIBEditFutureTopUpPrecnf.lblTPAmtValue.text; 
	inputParam["fee"] = frmIBEditFutureTopUpPrecnf.lblTPPaymentFeeVal.text; 
	inputParam["initiationDt"] =NormalDate;
	inputParam["paymentOrderDateEmailPDF"] = frmIBEditFutureTopUpPrecnf.lblTPPaymentOrderValue.text;
	var td = new Date();
	var currMonth =  td.getMonth()+1;
	inputParam["todayDate"] = td.getDate() + "/" + currMonth + "/" + td.getFullYear(); 
	inputParam["todayTime"] = "11:59:59 PM"; 
	inputParam["recurring"] = frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text 
	inputParam["startDt"] = frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text;
	inputParam["endDt"] = frmIBEditFutureTopUpPrecnf.lblEndOnDateVal.text 
	inputParam["refID"] = frmIBEditFutureTopUpPrecnf.lblTPScheduleRefNoVal.text
	inputParam["mynote"] = frmIBEditFutureTopUpPrecnf.lblMyNoteVal.text;
	inputParam["memo"] = frmIBEditFutureTopUpPrecnf.lblMyNoteVal.text;
	inputParam["channelId"] = "IB";
	inputParam["source"] = "FutureBillPaymentAndTopUp";
	inputParam["Locale"] = kony.i18n.getCurrentLocale();
	
	
	var scheduleDetails ="";
	if(frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text == "Once"){
	 	scheduleDetails = frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text;
	 }else if(frmIBEditFutureTopUpPrecnf.lblTPExcuteVal.text == "-"){
	 	scheduleDetails = frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text;
	 } else{
	 	scheduleDetails = frmIBEditFutureTopUpPrecnf.lblTPViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmIBEditFutureTopUpPrecnf.lblEndOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBEditFutureTopUpPrecnf.lblTPRepeatAsVal.text +" for "+frmIBEditFutureTopUpPrecnf.lblTPExcuteVal.text + " " + kony.i18n.getLocalizedString("keyTimesIB");
	 }
	
	inputParam["PaymentSchedule"] = scheduleDetails;
	
	
	invokeServiceSecureAsync("FutureBillPaymentEditExecute", inputParam, callBackVerifyOTPForEditTPIB);  

}



function srvTokenSwitchingDummyEditTP() {
    var inputParam = [];
    //showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, srvTokenSwitchingDummyCallBackEditTP);
}

function srvTokenSwitchingDummyCallBackEditTP(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
        	callCrmProfileInqServiceForEditTPAmt();
        }
	}
}


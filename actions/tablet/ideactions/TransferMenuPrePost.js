function frmTransferLandingMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmTransferLanding_frmTransferLanding_preshow.call(this);
	}
}

function frmTransferLandingMenuPostshow() {
	if(gblCallPrePost)
	{
		ehFrmTransferLanding_frmTransferLanding_postshow.call(this);
	}
	assignGlobalForMenuPostshow();
}

function frmTransferConfirmMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmTransferConfirm_frmTransferConfirm_preshow.call(this);
	}
}

function frmTransferConfirmMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmTransfersAckMenuPreshow() {
	if(gblCallPrePost)
	{
		initialFrmTransfersAckPreShow.call(this);
    	ehFrmTransfersAck_frmTransfersAck_preshow.call(this);
	}
}

function frmTransfersAckMenuPostshow() {
	if(gblCallPrePost)
	{
		ehFrmTransfersAck_frmTransfersAck_postshow.call(this);
	}
	assignGlobalForMenuPostshow();
}

function frmTransfersAckCalendarMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmTransfersAckCalendar_frmTransfersAckCalendar_preshow.call(this);
	}
}

function frmTransfersAckCalendarMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmTransferToRecipentsMobileMenuPreshow() {
	if(gblCallPrePost)
	{
		frmTranfersToRecipentsMobilePreshow.call(this);
	}
}

function frmTransferToRecipentsMobileMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmPreTransferMBMenuPreshow() {
	if(gblCallPrePost)
	{
		preShowfrmPreTransferMB.call(this);
	}
}

function frmPreTransferMBMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmTranfersToRecipentsMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmTranfersToRecipents_frmTranfersToRecipents_preshow.call(this);
	}
}

function frmTranfersToRecipentsMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMBFTViewMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFTView_frmMBFTView_preshow.call(this);
	}
}

function frmMBFTViewMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMBFTEditCmpleteMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFTEditCmplete_frmMBFTEditCmplete_preshow.call(this);
	}
}

function frmMBFTEditCmpleteMenuPostshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFTEditCmplete_frmMBFTEditCmplete_postshow.call(this);
	}
	assignGlobalForMenuPostshow();
}

function frmMBFTEditCnfrmtnMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFTEditCnfrmtn_frmMBFTEditCnfrmtn_preshow.call(this);
	}
}

function frmMBFTEditCnfrmtnMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMBFTEditMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFTEdit_frmMBFTEdit_preshow.call(this);
	}
}

function frmMBFTEditMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMBFtScheduleMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFtSchedule_frmMBFtSchedule_preshow.call(this);
	}
}

function frmMBFtScheduleMenuPostshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFtSchedule_frmMBFtSchedule_postshow.call(this);
	}
	assignGlobalForMenuPostshow();
}


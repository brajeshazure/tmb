//Form JS File
/*

function currentFormId_currentFormId_preshow_seq0(eventobject, neworientation) {
    currentFormId.txtUserId.setFocus(true);
};

function currentFormId_currentFormId_postshow_seq0(eventobject, neworientation) {
    currentFormId.txtUserId.setFocus(true);
    if (gblFirstSiteAccess) {
        langchange("th_TH");
        gblFirstSiteAccess = false;
    }
    gblLoggedIn = false;
};

function currentFormId_currentFormId_onhide_seq0(eventobject, neworientation) {
    currentFormId.txtCaptchaText.text = ""
    currentFormId.txtPassword.text = ""
    currentFormId.txtUserId.text = ""
    currentFormId.hboxCaptcha.setVisibility(false);
    currentFormId.hboxCaptchaText.setVisibility(false);
    hbox59324032046854.isVisible = false
};


*/
function currentFormId_txtUserId_onDone_seq0(eventobject, changedtext) {
    if (currentFormId.txtUserId.text != "") {
        if (currentFormId.txtPassword.text != "") {
            if (currentFormId.hboxCaptchaText.isVisible) {
                if (currentFormId.txtCaptchaText.text != "") {
                    captchaValidation.call(this, currentFormId.txtCaptchaText.text);
                } else {
                    var alert_seq8_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                    currentFormId.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            var alert_seq4_act0 = kony.ui.Alert({
                "message": kony.i18n.getLocalizedString("keyPasswordRequired"),
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "",
                "alertIcon": "",
                "alertHandler": null
            }, {});
            currentFormId.txtPassword.setFocus(true);
        }
    } else {
        var alert_seq2_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("keyLoginRequired"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
        currentFormId.txtUserId.setFocus(true);
    }
};

function currentFormId_txtUserId_onBeginEditing_seq0(eventobject, changedtext) {
    currentFormId.hbxUserId.skin = "hbxFocusInPreLogin"
};

function currentFormId_txtUserId_onEndEditing_seq0(eventobject, changedtext) {
    currentFormId.hbxUserId.skin = "hboxWhiteback"
};

function currentFormId_txtPassword_onDone_seq0(eventobject, changedtext) {
    if (currentFormId.txtUserId.text != "") {
        if (currentFormId.txtPassword.text != "") {
            if (currentFormId.hboxCaptchaText.isVisible) {
                if (currentFormId.txtCaptchaText.text != "") {
                    captchaValidation.call(this, currentFormId.txtCaptchaText.text);
                } else {
                    var alert_seq8_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                    currentFormId.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            var alert_seq4_act0 = kony.ui.Alert({
                "message": kony.i18n.getLocalizedString("keyPasswordRequired"),
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "",
                "alertIcon": "",
                "alertHandler": null
            }, {});
            currentFormId.txtPassword.setFocus(true);
        }
    } else {
        var alert_seq2_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("keyLoginRequired"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
        currentFormId.txtUserId.setFocus(true);
    }
};

function currentFormId_txtPassword_onBeginEditing_seq0(eventobject, changedtext) {
    currentFormId.hbxPassword.skin = "hbxFocusInPreLogin"
};

function currentFormId_txtPassword_onEndEditing_seq0(eventobject, changedtext) {
    currentFormId.hbxPassword.skin = "hboxWhiteback"
};

function currentFormId_txtCaptchaText_onDone_seq0(eventobject, changedtext) {
    if (currentFormId.txtUserId.text != "") {
        if (currentFormId.txtPassword.text != "") {
            if (currentFormId.hboxCaptchaText.isVisible) {
                if (currentFormId.txtCaptchaText.text != "") {
                    captchaValidation.call(this, currentFormId.txtCaptchaText.text);
                } else {
                    var alert_seq8_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                    currentFormId.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            var alert_seq4_act0 = kony.ui.Alert({
                "message": kony.i18n.getLocalizedString("keyPasswordRequired"),
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "",
                "alertIcon": "",
                "alertHandler": null
            }, {});
            currentFormId.txtPassword.setFocus(true);
        }
    } else {
        var alert_seq2_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("keyLoginRequired"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
        currentFormId.txtUserId.setFocus(true);
    }
};

function currentFormId_txtCaptchaText_onBeginEditing_seq0(eventobject, changedtext) {
    currentFormId.txtCaptchaText.skin = "txtBottomBorderFocus"
};

function currentFormId_txtCaptchaText_onEndEditing_seq0(eventobject, changedtext) {
    currentFormId.txtCaptchaText.skin = "txtBottomBorder"
};

function currentFormId_btnLogIn_onClick_seq0(eventobject) {
    if (!/^\s*$/.test(currentFormId.txtUserId.text)) {
        if (currentFormId.txtPassword.text != "") {
            if (currentFormId.hboxCaptchaText.isVisible) {
                if (currentFormId.txtCaptchaText.text != "") {
                    captchaValidation.call(this, currentFormId.txtCaptchaText.text);
                } else {
                    var alert_seq8_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                    currentFormId.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            var alert_seq4_act0 = kony.ui.Alert({
                "message": kony.i18n.getLocalizedString("keyPasswordRequired"),
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "",
                "alertIcon": "",
                "alertHandler": null
            }, {});
            currentFormId.txtPassword.setFocus(true);
        }
    } else {
        var alert_seq2_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("keyLoginRequired"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
        currentFormId.txtUserId.setFocus(true);
    }
};

function currentFormId_link502735421204405_onClick_seq0(eventobject) {
    popupIBForgotPassword.show();
};

function addIBLogin() {
    currentFormId = kony.application.getCurrentForm();
    var isIE8N = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    var useridplaceholder = kony.i18n.getLocalizedString("keyUserIDPreLogin");
    var passwordplaceholder = kony.i18n.getLocalizedString("keyPasswordPlaceHolder");
    if (isIE8N) {
        useridplaceholder = null;
        passwordplaceholder = null;
    }
    var lblCustomerLogin = new kony.ui.Label({
        "id": "lblCustomerLogin",
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyIBCustomerLogin"),
        "skin": "lblWhite24",
        "i18n_text": "kony.i18n.getLocalizedString('keyIBCustomerLogin')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 4],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 8
    }, {
        "toolTip": null
    });
    var image2502735421204232 = new kony.ui.Image2({
        "id": "image2502735421204232",
        "isVisible": true,
        "src": "icon_user_id.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "referenceWidth": null,
        "referenceHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 8
    }, {
        "toolTip": null
    });
    var txtUserId = new kony.ui.TextBox2({
        "id": "txtUserId",
        "isVisible": true,
        "text": null,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "maxTextLength": 20,
        "placeholder": useridplaceholder,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "onDone": currentFormId_txtUserId_onDone_seq0,
        "skin": "txtIB20pxBlack",
        "focusSkin": "txtIB20pxBlack"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 2, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 92
    }, {
        "onBeginEditing": currentFormId_txtUserId_onBeginEditing_seq0,
        "onEndEditing": currentFormId_txtUserId_onEndEditing_seq0,
        "autoCorrect": false,
        "autoComplete": false
    });
    var hbxUserId = new kony.ui.Box({
        "id": "hbxUserId",
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxWhiteback",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 9,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [6, 2, 2, 2],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxUserId.add(image2502735421204232, txtUserId);
    var line449290367432081 = new kony.ui.Line({
        "id": "line449290367432081",
        "isVisible": true,
        "skin": "lineGreyLogin"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var vbox449290367432080 = new kony.ui.Box({
        "id": "vbox449290367432080",
        "isVisible": true,
        "skin": "vbxBgWhite",
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [3, 0, 3, 0],
        "vExpand": false,
        "hExpand": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbox449290367432080.add(line449290367432081);
    var hbox502735421204401 = new kony.ui.Box({
        "id": "hbox502735421204401",
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxWhiteback",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 11,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbox502735421204401.add(vbox449290367432080);
    var image2502735421204263 = new kony.ui.Image2({
        "id": "image2502735421204263",
        "isVisible": true,
        "src": "icon_password.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "referenceWidth": null,
        "referenceHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 8
    }, {
        "toolTip": null
    });
    var txtPassword = new kony.ui.TextBox2({
        "id": "txtPassword",
        "isVisible": true,
        "text": null,
        "secureTextEntry": true,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "placeholder": passwordplaceholder,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "onDone": currentFormId_txtPassword_onDone_seq0,
        "skin": "txtIB20pxBlack",
        "focusSkin": "txtIB20pxBlack"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 2, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 92
    }, {
        "onBeginEditing": currentFormId_txtPassword_onBeginEditing_seq0,
        "onEndEditing": currentFormId_txtPassword_onEndEditing_seq0,
        "autoCorrect": false,
        "autoComplete": false
    });
    var hbxPassword = new kony.ui.Box({
        "id": "hbxPassword",
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxWhiteback",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 9,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [6, 2, 2, 2],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxPassword.add(image2502735421204263, txtPassword);
    var line449200994471993 = new kony.ui.Line({
        "id": "line449200994471993",
        "isVisible": true,
        "skin": "lineGreyLogin"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var vbox449200994471991 = new kony.ui.Box({
        "id": "vbox449200994471991",
        "isVisible": true,
        "skin": "vbxBgWhite",
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [3, 0, 3, 0],
        "vExpand": false,
        "hExpand": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbox449200994471991.add(line449200994471993);
    var hboxLine2 = new kony.ui.Box({
        "id": "hboxLine2",
        "isVisible": false,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxWhiteback",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 5,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hboxLine2.add(vbox449200994471991);
    var imgcaptcha = new kony.ui.Image2({
        "id": "imgcaptcha",
        "isVisible": true,
        "src": "captcha.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
        "referenceWidth": null,
        "referenceHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {
        "toolTip": null
    });
    var hboxCaptcha = new kony.ui.Box({
        "id": "hboxCaptcha",
        "isVisible": false,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxWhiteback",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 11,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [2, 2, 2, 2],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hboxCaptcha.add(imgcaptcha);
    var label449200994471754 = new kony.ui.Label({
        "id": "label449200994471754",
        "isVisible": true,
        "text": "Please type the characters you see \nfrom above box",
        "skin": "lblIB18pxBlack"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 59
    }, {
        "toolTip": null
    });
    var txtCaptchaText = new kony.ui.TextBox2({
        "id": "txtCaptchaText",
        "isVisible": true,
        "text": null,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "onDone": currentFormId_txtCaptchaText_onDone_seq0,
        "skin": "txtBottomBorder",
        "focusSkin": "txtBottomBorderFocus"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 41
    }, {
        "onBeginEditing": currentFormId_txtCaptchaText_onBeginEditing_seq0,
        "onEndEditing": currentFormId_txtCaptchaText_onEndEditing_seq0,
        "autoCorrect": false,
        "autoComplete": false
    });
    var vbox449200994471731 = new kony.ui.Box({
        "id": "vbox449200994471731",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "vExpand": false,
        "hExpand": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbox449200994471731.add(label449200994471754, txtCaptchaText);
    var hboxCaptchaText = new kony.ui.Box({
        "id": "hboxCaptchaText",
        "isVisible": false,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxWhiteback",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 16,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hboxCaptchaText.add(vbox449200994471731);
    var btnLogIn = new kony.ui.Button({
        "id": "btnLogIn",
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyIBPreloginLoginButton"),
        "skin": "btnIB224",
        "focusSkin": "btnIB224",
        "onClick": currentFormId_btnLogIn_onClick_seq0,
        "i18n_text": "kony.i18n.getLocalizedString('keyIBPreloginLoginButton')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": true,
        "hExpand": true,
        "margin": [0, 4, 0, 0],
        "padding": [0, 3, 0, 3],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 12
    }, {
        "hoverskin": "btnIB289active",
        "toolTip": null
    });
    var imgCall = new kony.ui.Image2({
        "id": "imgCall",
        "isVisible": true,
        "src": "icon_phone.png",
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "referenceWidth": null,
        "referenceHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 7
    }, {
        "toolTip": null
    });
    var link502735421204405 = new kony.ui.Link({
        "id": "link502735421204405",
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("forPass"),
        "skin": "linkWhitesize18",
        "focusSkin": "linkWhiteSize18Underline",
        "onClick": currentFormId_link502735421204405_onClick_seq0,
        "i18n_text": "kony.i18n.getLocalizedString('forPass')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 93
    }, {
        "hoverSkin": "linkWhiteSize18Underline",
        "toolTip": null
    });
    var hbox502735421204402 = new kony.ui.Box({
        "id": "hbox502735421204402",
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 8,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 4, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbox502735421204402.add(imgCall, link502735421204405);
    var vbox = new kony.ui.Box({
        "id": "vbox",
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 3, 0, 2],
        "padding": [6, 3, 6, 4],
        "vExpand": false,
        "hExpand": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbox.add(lblCustomerLogin, hbxUserId, hbox502735421204401, hbxPassword, hboxLine2, hboxCaptcha, hboxCaptchaText, btnLogIn, hbox502735421204402);
    var hboxCustomerLogin = new kony.ui.Box({
        "id": "hboxCustomerLogin",
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxCmnDrkBlue",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 71,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hboxCustomerLogin.add(vbox);
    if (kony.i18n.getCurrentLocale() == "en_US") {
        var bannerSrc = "aw_banner01.jpg";
    } else {
        var bannerSrc = "aw_banner02.jpg";
    }
    var imgActivate = new kony.ui.Image2({
        "id": "imgActivate",
        "isVisible": true,
        "src": bannerSrc,
        "imageWhenFailed": null,
        "imageWhileDownloading": null
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "referenceWidth": null,
        "referenceHeight": null,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {
        "toolTip": null
    });
    var vbox503629151205725 = new kony.ui.Box({
        "id": "vbox503629151205725",
        "isVisible": true,
        "skin": "vboxCursor",
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "hExpand": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbox503629151205725.add(imgActivate);
    var hboxActivateNow = new kony.ui.Box({
        "id": "hboxActivateNow",
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxIBLightBlue",
        "onClick": frmIBPreLogin_hbxActivateImage_onClick_seq0,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 22,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hboxActivateNow.add(vbox503629151205725);
    while (currentFormId.vbxTotalMenu.removeAt(0) != undefined) {
        currentFormId.vbxTotalMenu.removeAt(0);
    }
    currentFormId.vbxTotalMenu.add(hboxCustomerLogin, hboxActivateNow);
};
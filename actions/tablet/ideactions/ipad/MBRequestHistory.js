var totalRequestDataMB = [];

function viewRequestHistoryMB(type) {
    var inputParam = [];
    inputParam["crmId"] = gblcrmId;
    var d1 = dateFormatChange(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1));
    var d2 = dateFormatChange(new Date(new Date().getFullYear() - 1, new Date().getMonth(), new Date().getDate()));
    //var d = new Date();
    //var dformat = [d.getDate(),d.getMonth()+1,d.getFullYear()].join('-')+' ' +[d.getHours(),d.getMinutes(),d.getSeconds()].join(':');
    inputParam["todate"] = d1
    inputParam["fromdate"] = d2;
    if (type == 1) {
        inputParam["sortType"] = "ASC";
    } else {
        inputParam["sortType"] = "DESC";
    }
    showLoadingScreen();
    invokeServiceSecureAsync("requestHistory", inputParam, mbRequestHistoryCallbackfunction);
}

function mbRequestHistoryCallbackfunction(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            dismissLoadingScreen();
            if (callBackResponse["requestHistory"].length == 0) {
                alert(kony.i18n.getLocalizedString("NoRecsFound"));
                return false;
            }
            // Code change for IOS9	
            gblRequData = callBackResponse["requestHistory"];
            setRequestHistData();
            frmMyProfileReqHistory.show();
            /*frmMyProfileReqHistory.segRequestHistory.removeAll();
            var tempData =[];
            for (var i=0;i<callBackResponse["requestHistory"].length;i++){
            	var reqHistoryData={
            					lblReqNo :kony.i18n.getLocalizedString("keyReqNumber")+": "+ callBackResponse["requestHistory"][i]["SR_LOG_ID"],
            					lblTimeBank :callBackResponse["requestHistory"][i]["SERVICE_REQUEST_DATE"],
            					lblTime:"["+callBackResponse["requestHistory"][i]["SERVICE_REQUEST_TIME"]+"]",
            					lblReqDesc :callBackResponse["requestHistory"][i]["ACTIVITY_FLEX_VALUES"],
            					lblStatus:" "+kony.i18n.getLocalizedString("keyStatus")+": ",
            					lblStatusValue:callBackResponse["requestHistory"][i]["STATUS_DESC"]
            					
            	};
            	tempData.push(reqHistoryData);
            }	
            totalRequestDataMB = tempData;
            frmMyProfileReqHistory.segRequestHistory.addAll(tempData);
            if(tempData.length == 1) {
            	frmMyProfileReqHistory.combobox508395714279435.setEnabled(false);
            }*/
            //			 			//var sortedTotalData = loadSortedData(frmAccountStatementMB.segcredit.data);
            //						//frmIBCMMyProfile.segRequestHistory.setData(sortedTotalData);
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("NoRecsFound"));
            return false;
        }
    }
}

function setRequestHistData() {
    frmMyProfileReqHistory.segRequestHistory.removeAll();
    var tempData = [];
    for (var i = 0; i < gblRequData.length; i++) {
        var reqHistoryData = {
            lblReqNo: kony.i18n.getLocalizedString("keyReqNumber") + ": " + gblRequData[i]["SR_LOG_ID"],
            lblTimeBank: gblRequData[i]["SERVICE_REQUEST_DATE"],
            lblTime: "[" + gblRequData[i]["SERVICE_REQUEST_TIME"] + "]",
            lblReqDesc: gblRequData[i]["ACTIVITY_FLEX_VALUES"],
            lblStatus: " " + kony.i18n.getLocalizedString("keyStatus") + ": ",
            lblStatusValue: gblRequData[i]["STATUS_DESC"]
        };
        tempData.push(reqHistoryData);
    }
    totalRequestDataMB = tempData;
    frmMyProfileReqHistory.segRequestHistory.addAll(tempData);
    if (tempData.length == 1) {
        frmMyProfileReqHistory.combobox508395714279435.setEnabled(false);
    }
}
//Changing the width 22em to 20 em to fix the Nexus five issue
hzRoundAbout = {
    initializeWidget: function(parentNode, widgetModel) {
        var roundAbout = '<style type="text/css">' + '@font-face ' + '{' + 'font-family:"DB Ozone X";' + 'src: url("./spaandroid/DBOzoneX.woff");' + '}' + '@font-face ' + '{' + 'font-family:"DB Ozone X UltraLi";' + 'src: url("./spaandroid/DBOzoneX-UltraLight.woff");' + '}' + '@font-face ' + '{' + 'font-family:"DB Ozone X Med";' + 'src: url("./spaandroid/DBOzoneX-Medium.woff");' + '}' + '@font-face ' + '{' + 'font-family:"DB Ozone X Bold";' + 'src: url("./spaandroid/DBOzoneX-Bold.woff");' + '}' + 'ul.roundabout-holder {' + 'list-style: none;' + 'padding: 10;' + 'margin: 0 auto;' + 'width: 20em;' + 'height: 14em;' + '}' + '.roundabout-holder li {' + 'margin:1em;' + 'height:13em;' + 'width: 17em;' + 'text-align: left;' + 'cursor: pointer;' + '}	' + '.roundabout-holder li span {' + 'margin-left: 1em;' + 'margin-right: 1em;' + 'margin-top: -1em;' + 'display: block;' + 'padding-top: 1em;' + 'overflow: hidden;' + 'height:11em;' + 'width:19em;' + '}' + '.roundabout-holder { padding: 0; height: 100%; width: 100%; margin: 0 auto; list-style: none;overflow: hidden;}' + '</style>' + '<table style="width:100%;height:15em;font-family:DB Ozone X;color:#333333;">' + '<tr>' + '<td width="8%" align="left">';
        numberOfAccounts = widgetModel["data"].length;
        if (numberOfAccounts != 1) {
            roundAbout += '<a href="#" Class="previous" z-index:-1><img src = "./spaiphone/images/l_arrow.png" alt="Image not found" onError="this.onerror=null;this.src="./spaiphone/images/l_arrow.png";" style="float:left"/></a>';
        }
        roundAbout += '</td>' + '<td width="84%" align="center">' + '<ul id="container" class="roundabout-holder">';
        if (gblcarouselwidgetflow == "Savings") {
            var colorArray = ["#76AED6", "#76AED6", "#76AED6", "#76AED6", "#76AED6"];
            numberOfAccounts = widgetModel["data"].length;
            for (var i = 0, j = 0; i < numberOfAccounts; i++, ((j < colorArray.length - 1) ? (j++) : (j = 0))) {
                roundAbout += '<li><span style="padding-top:20px; background-color:' + colorArray[j] + '"><table cellpadding=2><tr><td rowspan="2"><img src="spaiphone/images/' + widgetModel["data"][i]["img1"] + '"/></td><td style="font:normal 24px DB Ozone X Med; color:#FFFFFF;">' + widgetModel["data"][i]["lblCustName"] + '</td></tr></table> </span></li>';
            }
            gblcarouselwidgetflow = "Normal"
        } else {
            var colorArray = ["#EE8080", "#6FB3D6", "#C1DB57", "#F4ED26", "#E28A3C"];
            numberOfAccounts = widgetModel["data"].length;
            for (var i = 0, j = 0; i < numberOfAccounts; i++, ((j < colorArray.length - 1) ? (j++) : (j = 0))) {
                roundAbout += '<li><span style="background-color:' + colorArray[j] + ';padding:0 10px;"><table cellpadding="0" cellspacing="0"><tr><td rowspan="2"><img style = "height:55px; width:55px" src=' + widgetModel["data"][i]["img1"] + '/></td><td><font style="font:normal 20px DB Ozone X Med;">' + widgetModel["data"][i]["lblCustName"] + '</font><br><font style="font:normal 24px DB Ozone X;">' + widgetModel["data"][i]["lblBalance"] + '</font></td></tr></table>' + '<font style="font:normal 16px DB Ozone X;">' + widgetModel["data"][i]["lblACno"] + widgetModel["data"][i]["lblActNoval"] + ' <br/>' + widgetModel["data"][i]["lblSliderAccN1"] + widgetModel["data"][i]["lblSliderAccN2"] + '  <br/>' + widgetModel["data"][i]["lblRemainFee"] + widgetModel["data"][i]["lblRemainFeeValue"] + '</font></span></li>';
            }
        }
        roundAbout += '</ul>' + '</td>' + '<td width="8%" align="right">';
        if (numberOfAccounts != 1) {
            roundAbout += '<a href="#" class="next" ><img src = "./spaiphone/images/r_arrow.png" alt="Image not found" onError="this.onerror=null;this.src="./spaiphone/images/r_arrow.png";" style="float:right" /></a>';
        }
        '</td>' + '</tr>' + '</table>';
        parentNode.innerHTML = roundAbout;
        $(document).ready(function() {
            $('ul').roundabout({
                shape: 'square',
                maxScale: 1,
                minScale: 0.2,
                btnNext: ".next",
                btnPrev: ".previous",
                enableDrag: true,
                startingChild: gblspaSelIndex,
                clickToFocus: true,
                triggerFocusEvents: true,
                clickToFocusCallback: function($item) {
                    //alert("[0,"+$('ul').roundabout("getChildInFocus")+"]");
                    var temp = [];
                    var spa = "";
                    temp[0] = "0";
                    temp[1] = $('#container').roundabout("getChildInFocus");
                    widgetModel["selectedItem"] = temp;
                    gbltranFromSelIndex = temp;
                    if (gblmbSpatransflow == true) {
                        //alert("inside the conditions");
                        spa = gbltranFromSelIndex[1];
                        gbltdFlag = widgetModel["data"][spa].tdFlag;
                        //alert("the final value of flag is"+gbltdFlag);
                        // toggleTDMaturityCombobox(gbltdFlag);
                        //	resetTransferLandingBasedOnToAccount();
                    }
                    //alert("the value 1"+gbltranFromSelIndex);
                    //alert("the value 2"+temp);
                    //widgetModel["onSelect"].call();
                },
                btnNextCallback: function($item) {
                    //alert("[0,"+$('ul').roundabout("getChildInFocus")+"]");
                    var temp = [];
                    var spa = "";
                    temp[0] = "0";
                    temp[1] = $('#container').roundabout("getChildInFocus");
                    widgetModel["selectedItem"] = temp;
                    gbltranFromSelIndex = temp;
                    if (gblmbSpatransflow == true) {
                        //alert("inside the conditions");
                        spa = gbltranFromSelIndex[1];
                        gbltdFlag = widgetModel["data"][spa].tdFlag;
                        //alert("the final value of flag is"+gbltdFlag);
                        frmTransferLanding.txtTranLandAmt.setEnabled(true);
                        //frmTransferLanding.btnSchedTo.onClick = ehFrmTransferLanding_btnSchedTo_onClick;
                        //toggleTDMaturityCombobox(gbltdFlag);
                        // resetTransferLandingBasedOnToAccount();
                    }
                    //alert("the value 1"+gbltranFromSelIndex);
                    //alert("the value 2"+temp);
                    //widgetModel["onSelect"].call();
                },
                btnPrevCallback: function($item) {
                    //alert("[0,"+$('ul').roundabout("getChildInFocus")+"]");
                    var temp = [];
                    var spa = "";
                    temp[0] = "0";
                    temp[1] = $('#container').roundabout("getChildInFocus");
                    widgetModel["selectedItem"] = temp;
                    gbltranFromSelIndex = temp;
                    if (gblmbSpatransflow == true) {
                        //alert("inside the conditions");
                        spa = gbltranFromSelIndex[1];
                        gbltdFlag = widgetModel["data"][spa].tdFlag;
                        //alert("the final value of flag is"+gbltdFlag);
                        enableTransferTextBoxAmount(true);
                        //frmTransferLanding.btnSchedTo.onClick = ehFrmTransferLanding_btnSchedTo_onClick;
                        toggleTDMaturityCombobox(gbltdFlag);
                        resetTransferLandingBasedOnToAccount();
                    }
                    //alert("the value 1"+gbltranFromSelIndex);
                    //alert("the value 2"+temp);
                    //widgetModel["onSelect"].call();
                },
                dropCallback: function($item) {
                    //alert("[0,"+$('ul').roundabout("getChildInFocus")+"]");
                    var temp = [];
                    var spa = "";
                    temp[0] = "0";
                    temp[1] = $('#container').roundabout("getChildInFocus");
                    widgetModel["selectedItem"] = temp;
                    gbltranFromSelIndex = temp;
                    if (gblmbSpatransflow == true) {
                        //alert("inside the conditions");
                        spa = gbltranFromSelIndex[1];
                        gbltdFlag = widgetModel["data"][spa].tdFlag;
                        //alert("the final value of flag is"+gbltdFlag);
                        // toggleTDMaturityCombobox(gbltdFlag);
                        //	resetTransferLandingBasedOnToAccount();
                    }
                    //alert("the value 1"+gbltranFromSelIndex);
                    //alert("the value 2"+temp);
                    //widgetModel["onSelect"].call();
                }
            });
        });
    },
    modelChange: function(widgetModel, propertyChanged, propertyValue) {}
}
//var gblSelTransferMode = 1; // 1=bank account, 2=Mobile
function setTabBankAccountOrMobile(isInit){
	if(!isNotBlank(isInit)) isInit = false;
	if(isInit || gblSelTransferMode == 1){
		focusOnUsBankAccountIB("frmIBTransferCustomWidgetLP");
		resetOnUsMobileNumberIB("frmIBTransferCustomWidgetLP");
		displayBtnSchedule("frmIBTransferCustomWidgetLP", true);
		displayHbxNotifyRecipent("frmIBTransferCustomWidgetLP", true);
	}else{
		focusOnUsMobileNumberIB("frmIBTransferCustomWidgetLP");
		resetOnUsBankAccountIB("frmIBTransferCustomWidgetLP");
		displayBtnSchedule("frmIBTransferCustomWidgetLP", false);
		displayHbxNotifyRecipent("frmIBTransferCustomWidgetLP", false);
	}
}

function setSelTabBankAccountOrMobile(){
	var currentForm = "frmIBTranferLP";
	if(gblSelTransferMode == 1){
		focusOnUsBankAccountIB(currentForm);
		resetOnUsMobileNumberIB(currentForm);
		displayTxtMobileP2P(false);
		displayHbxTo(true);
		displayBtnSchedule(currentForm, true);
		displayHbxNotifyRecipent(currentForm, true);
	}else{
		focusOnUsMobileNumberIB(currentForm);
		resetOnUsBankAccountIB(currentForm);
		if(frmIBTranferLP.hbxSelMobileRecipient.isVisible){
			displayHbxSelMobileRecipientP2P(true);
		}else{
			displayTxtMobileP2P(true);
		}
		displayHbxTo(false);
		displayBtnSchedule(currentForm, false);
		displayHbxNotifyRecipent(currentForm, false);
	}
}

function displayHbxTo(isShow){
	frmIBTranferLP.hbxTo.isVisible = isShow;
}

function clearValueHbxToWidgetLP(){
	frmIBTransferCustomWidgetLP.imgXferToImage.src = "transparent.png";
	frmIBTransferCustomWidgetLP.lblXferToNameRcvd.text = "";
	frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text = "";
	frmIBTransferCustomWidgetLP.lblXferToBankNameRcvd.text = "";
}

function clearValueHbxToLP(){
	frmIBTranferLP.imgXferToImage.src = "transparent.png";
	frmIBTranferLP.lblXferToNameRcvd.text = "";
	frmIBTranferLP.lblXferToContactRcvd.text = "";
	frmIBTranferLP.lblXferToBankNameRcvd.text = "";
}

function clearValueHbxToP2PLP(){
	frmIBTranferLP.imgXferToImageP2P.src = "transparent.png";
	frmIBTranferLP.lblToMobileNo.text = "";
	frmIBTranferLP.lblXferToContactRcvdMobile.text = "";
}

function displayTxtMobileP2P(isShow){
	frmIBTranferLP.hbxToMobile.isVisible = isShow;
	//if (gblSelTransferMode == 2){
	//	frmIBTranferLP.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("MIB_P2PTREnter_MobNo");
	//}else{
		frmIBTranferLP.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("IB_P2PNoToValue");
	//}
	
	frmIBTranferLP.txtXferMobileNumber.isVisible = isShow;
	frmIBTranferLP.hbxSelMobileRecipient.isVisible = !isShow;
}

function displayITMXFeeP2P(isShow){
	frmIBTranferLP.hbxP2PFee.setVisibility(isShow);
	frmIBTranferLP.lineP2PFee.setVisibility(isShow);
}

function displayHbxSelMobileRecipientP2P(isShow){
	frmIBTranferLP.hbxToMobile.isVisible = isShow;
	frmIBTranferLP.txtXferMobileNumber.isVisible = !isShow;
	frmIBTranferLP.hbxSelMobileRecipient.isVisible = isShow;
}

function resetOnUsBankAccountIB(currentForm){
	var locale = kony.i18n.getCurrentLocale();
	if(currentForm == "frmIBTransferCustomWidgetLP"){
		frmIBTransferCustomWidgetLP.vbxTabBankAccount.skin = "vbxBGGrey2";
		frmIBTransferCustomWidgetLP.vbxTabBankAccount.focusSkin = "vbxBGGrey2";
		if(locale == 'en_US'){
			frmIBTransferCustomWidgetLP.imgTabBankAccount.src = "en_account.png";
		}else{
			frmIBTransferCustomWidgetLP.imgTabBankAccount.src = "th_account.png";
		}
	}else{
		frmIBTranferLP.vbxTabBankAccount.skin = "vbxBGGrey2";
		frmIBTranferLP.vbxTabBankAccount.focusSkin = "vbxBGGrey2";
		if(locale == 'en_US'){
			frmIBTranferLP.imgTabBankAccount.src = "en_account.png";
		}else{
			frmIBTranferLP.imgTabBankAccount.src = "th_account.png";
		}
	}
}
function resetOnUsMobileNumberIB(currentForm){
	var locale = kony.i18n.getCurrentLocale();
	if(currentForm == "frmIBTransferCustomWidgetLP"){
		frmIBTransferCustomWidgetLP.vbxTabMobile.skin = "vbxBGGrey2";
		frmIBTransferCustomWidgetLP.vbxTabMobile.focusSkin = "vbxBGGrey2";
		if(locale == 'en_US'){
			frmIBTransferCustomWidgetLP.imgTabMobile.src = "en_mobile.png";
		}else{
			frmIBTransferCustomWidgetLP.imgTabMobile.src = "th_mobile.png";
		}
	}else{
		frmIBTranferLP.vbxTabMobile.skin = "vbxBGGrey2";
		frmIBTranferLP.vbxTabMobile.focusSkin = "vbxBGGrey2";
		if(locale == 'en_US'){
			frmIBTranferLP.imgTabMobile.src = "en_mobile.png";
		}else{
			frmIBTranferLP.imgTabMobile.src = "th_mobile.png";
		}
	
	}
	
}
function focusOnUsBankAccountIB(currentForm){
	var locale = kony.i18n.getCurrentLocale();
	gblSelTransferMode = 1;
	if(currentForm == "frmIBTransferCustomWidgetLP"){
		frmIBTransferCustomWidgetLP.vbxTabBankAccount.skin = "vbxTabFocus";
		frmIBTransferCustomWidgetLP.vbxTabBankAccount.focusSkin = "vbxTabFocus";
		if(locale == 'en_US'){
			frmIBTransferCustomWidgetLP.imgTabBankAccount.src = "en_account_active.png";
		}else{
			frmIBTransferCustomWidgetLP.imgTabBankAccount.src = "th_account_active.png";
		}
	}else{
		frmIBTranferLP.vbxTabBankAccount.skin = "vbxTabFocus";
		frmIBTranferLP.vbxTabBankAccount.focusSkin = "vbxTabFocus";
		if(locale == 'en_US'){
			frmIBTranferLP.imgTabBankAccount.src = "en_account_active.png";
		}else{
			frmIBTranferLP.imgTabBankAccount.src = "th_account_active.png";
		}
		displayITMXFeeP2P(false);
		
	}
}
function focusOnUsMobileNumberIB(currentForm){
	var locale = kony.i18n.getCurrentLocale();
	gblSelTransferMode = 2;
	var P2PData = removeHyphenIB(frmIBTranferLP.txtXferMobileNumber.text);
	if(isNotBlank(P2PData) && P2PData.length > 0){
		reCheckSelTransferMode(P2PData);
	}
	gblTrasORFT = 0;
	gblTransSMART = 0;
	if(currentForm == "frmIBTransferCustomWidgetLP"){
		frmIBTransferCustomWidgetLP.vbxTabMobile.skin = "vbxTabFocus";
		frmIBTransferCustomWidgetLP.vbxTabMobile.focusSkin = "vbxTabFocus";
		if(locale == 'en_US'){
			frmIBTransferCustomWidgetLP.imgTabMobile.src = "en_mobile_active.png";
		}else{
			frmIBTransferCustomWidgetLP.imgTabMobile.src = "th_mobile_active.png";
		}
	}else{
		frmIBTranferLP.vbxTabMobile.skin = "vbxTabFocus";
		frmIBTranferLP.vbxTabMobile.focusSkin = "vbxTabFocus";
		if(locale == 'en_US'){
			frmIBTranferLP.imgTabMobile.src = "en_mobile_active.png";
		}else{
			frmIBTranferLP.imgTabMobile.src = "th_mobile_active.png";
		}
	}
}

function onClickBankAccountNumberIB(){
	var currentForm = kony.application.getCurrentForm().id;
	clearValueHbxToWidgetLP();
	clearValueHbxToLP();
	if(currentForm == "frmIBTransferCustomWidgetLP"){
		if(frmIBTransferCustomWidgetLP.vbxTabBankAccount.skin != "vbxTabFocus"){
			focusOnUsBankAccountIB(currentForm);	
			resetOnUsMobileNumberIB(currentForm);
			getFromXferAccountsIB();
		}
	}else{
		if(frmIBTranferLP.vbxTabBankAccount.skin != "vbxTabFocus"){
			focusOnUsBankAccountIB(currentForm);	
			resetOnUsMobileNumberIB(currentForm);
			displayTxtMobileP2P(false);
			displayHbxTo(true);
		}
	}
	displayBtnSchedule(currentForm, true);
	displayHbxNotifyRecipent(currentForm, true);
}

function onClickOnUsMobileNumberIB(){
	gblPrevLen = 0;
	clearValueHbxToWidgetLP();
	clearValueHbxToLP();
	displayITMXFeeP2P(false);
	frmIBTranferLP.lblMobileNumberTemp.text = "";
	var currentForm = kony.application.getCurrentForm().id;
	if(currentForm == "frmIBTransferCustomWidgetLP"){
		if(frmIBTransferCustomWidgetLP.vbxTabMobile.skin != "vbxTabFocus"){
			resetOnUsBankAccountIB(currentForm)
			focusOnUsMobileNumberIB(currentForm);
			getFromXferAccountsIB();
		}
	}else{
		if(frmIBTranferLP.vbxTabMobile.skin != "vbxTabFocus"){
			resetOnUsBankAccountIB(currentForm)
			focusOnUsMobileNumberIB(currentForm);
			displayTxtMobileP2P(true);
			displayHbxTo(false);
		}
	}
	displayBtnSchedule(currentForm, false);
	displayHbxNotifyRecipent(currentForm, false);
}

function displayBtnSchedule(currentForm, isShow){
	if(currentForm == "frmIBTransferCustomWidgetLP"){
		frmIBTransferCustomWidgetLP.hbxXferTransferRange.isVisible = isShow;
		frmIBTransferCustomWidgetLP.lblXferTransferRangeP2P.isVisible = !isShow;
		frmIBTransferCustomWidgetLP.lblXferTransferRangeP2P.text = frmIBTransferCustomWidgetLP.lblXferTransferRange.text;
	}else{
		frmIBTranferLP.hbxXferTransferRange.isVisible = isShow;
		frmIBTranferLP.hbxXferTransferRangeP2P.isVisible = !isShow;
		frmIBTranferLP.lblXferTransferRangeP2P.text = frmIBTranferLP.lblXferTransferRange.text;
	}
}

function displayHbxNotifyRecipent(currentForm, isShow){
	if(currentForm == "frmIBTransferCustomWidgetLP"){
		frmIBTransferCustomWidgetLP.hbxNotifyRecipent.isVisible = isShow;
		frmIBTransferCustomWidgetLP.hbxNotifyRecipentP2P.isVisible = !isShow;
	}else{
		frmIBTranferLP.hbxNotifyRecipent.isVisible = isShow;
		frmIBTranferLP.hbxNotifyRecipentP2P.isVisible = !isShow;
	}
}

function calGetRecipientsForOnUsIB(){
		frmIBTranferLP.lblMsg.text = kony.i18n.getLocalizedString("keybillernotfound");
		frmIBTranferLP.txbXferSearch.text = "";
		frmIBTranferLP.lblMsg.setVisibility(false);
		var inputParam = {};
		showLoadingScreenPopup();
		frmIBTranferLP.segXferRecipentsList.removeAll();
		frmIBTranferLP.segXferRecipentsList.setVisibility(false);
		frmIBTranferLP.button1011732624601.setVisibility(false);
		invokeServiceSecureAsync("getMibRecipientsForOnUs", inputParam, calGetRecipientsForOnUsCallBackIB);
}

function calGetRecipientsForOnUsCallBackIB(status, resulttable){
	//success responce
	if (status == 400) {
		var mobVal = getOnlyDigitMobileNumber(gblPHONENUMBER);
		if ((resulttable["opstatus"] == 0 && resulttable["RecepientsList"].length>0) || (isNotBlank(mobVal) && mobVal.length == 10  && recipientMobileVal(mobVal))) {
			var recipientList = resulttable["RecepientsList"];
			var recSegmentData = [];
			var favimg ="";
			var randomnum = Math.floor((Math.random()*10000)+1); 
			gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" +randomnum;
			
			var contactsToService = [];
			var mobVal = getOnlyDigitMobileNumber(gblPHONENUMBER);
			if(isNotBlank(mobVal) && mobVal.length == 10  && recipientMobileVal(mobVal)){
				contactsToService.push(gblPHONENUMBER);
				recSegmentData.push({
					lblXferRecipentsName: gblCustomerName,
					lblXferRecipentsAccount: onEditMobileNumberP2P(gblPHONENUMBER),
					imgXferArrow:  "empty.png",
					imgURL: "",
					imgfav: "",
					onUsLogo : "",
					imgXferRecipentsImage: gblMyProfilepic,
					btnXferMobile: "empty.png",
					category: "own",
					personalizedId: "",
					template: hbxXferSegmentInitial
               	 	});
            }
            
			for (var i = 0; i < recipientList.length; i++) {
				
				if (recipientList[i].personalizedFavFlag != null &&
					recipientList[i].personalizedFavFlag=="Y") {
					favimg = "starblue.png";
				} else {
					favimg = "";
				}
				
				var rcImg = "";
				var profilePic = recipientList[i].personalizedPicId;
				var XferRcURL ="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +	"/ImageRender?billerId=&crmId=Y&personalizedId=";
				var randomnum = Math.floor((Math.random()*10000)+1); 
				
				if (profilePic == null || profilePic == "") {
					rcImg = XferRcURL + recipientList[i].personalizedId +"&rr="+randomnum;
				} else {
					var check = kony.string.endsWith(profilePic + "", "nouserimg.jpg", true)
					if (check == true) {
						rcImg = XferRcURL + recipientList[i].personalizedId +"&rr="+randomnum;
					} else {
						var check = kony.string.startsWith(profilePic + "", "http", true)
						if (check == true) {
							rcImg = profilePic;
						} else {
							rcImg = XferRcURL + recipientList[i].personalizedId +"&rr="+randomnum;									
						}
					}
				}
				
                var mobVal = getOnlyDigitMobileNumber(recipientList[i].personalizedMobileNum.trim());
				var temp = 	{
							btnXferFb: "empty.png",
							btnXferMobile: "empty.png",
							imgXferRecipentsImage: rcImg,
							onUsLogo: favimg,
							imgXferArrow: "empty.png",							
							lblXferRecipentsName: recipientList[i].personalizedName,
							lblXferRecipentsAccount: onEditMobileNumberP2P(mobVal),
							personalizedId: recipientList[i].personalizedId,
							imgURL: "",
							category: "main",
							template: hbxXferSegmentInitial
                        };
				if(isNotBlank(mobVal) && mobVal.length == 10  && recipientMobileVal(mobVal)){
					contactsToService.push(mobVal);
					recSegmentData.push(temp);
				}
			}
			gblTransferToRecipientData = recSegmentData;
			checkEligibilityForP2PIB(contactsToService);
			
		}else{
			if (resulttable["RecepientsList"].length==0){
				showAlert(kony.i18n.getLocalizedString("MIB_P2PTRErr_NoToMobMIB"), kony.i18n.getLocalizedString("info"));
			}				
			dismissLoadingScreenPopup();
		}		
	}
}

function showHideNotifyRecipientP2PIB(isShow){
	frmIBTranferLP.hbxNotifyRecipentP2P.setVisibility(isShow);
	frmIBTranferLP.lineOne.setVisibility(isShow);
}

function showHideNotifyRecipientP2PCIIB(isShow){
	frmIBTranferLP.hbxNotifyRecipent.setVisibility(isShow);
	frmIBTranferLP.lineOne.setVisibility(isShow);
}

function onClickP2pRecipientSegment(){
	resetContentsIB();
	onChangeToRecip();
	
	if(frmIBTranferLP.segXferRecipentsList.selectedItems[0].category == "own"){
		isOwnAccountP2P = true;
	}else{
		isOwnAccountP2P = false;
	}
	resetHbxNotifyRecAndSenderNoteP2P();
	gblSelTransferMode = 2;
	checkDisplayNotifyP2PMOorCI();
	frmIBTranferLP.txtXferMobileNumber.text = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsAccount;
	frmIBTranferLP.lblXferToNameRcvd.text = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsName;
	frmIBTransferNowConfirmation.lblXferToName.text = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsName;
	frmIBTransferNowConfirmation.lblXferToName.setVisibility(true);
	checkCallCheckOnUsPromptPayinqServiceIB();
}


function showP2PTextBoxIB(flag){
	frmIBTranferLP.hbxSelMobileRecipient.setVisibility(!flag);
	frmIBTranferLP.txtXferMobileNumber.setVisibility(flag);
	
}

function enableP2PTxnIB(setyourIDTransfer){
	if(setyourIDTransfer != null && setyourIDTransfer == "true"){
		frmIBTranferLP.hbxBankMobileTransfer.setVisibility(true);
		frmIBTransferCustomWidgetLP.hbxBankMobileTransfer.setVisibility(true);
	}else{
		frmIBTranferLP.hbxBankMobileTransfer.setVisibility(false);
		frmIBTransferCustomWidgetLP.hbxBankMobileTransfer.setVisibility(false);
	}
}

function searchP2PContactsIB() {
	var searchText = frmIBTranferLP.txbXferSearch.text;
	searchText = searchText.toLowerCase();
	
	var recipientsSegData = gblTransferToRecipientData;
	
	var  searchList = searchResultOfP2PContactsIB(recipientsSegData, searchText);
	
	if(frmIBTranferLP.segXferRecipentsList.data != null && frmIBTranferLP.segXferRecipentsList.data != undefined){
		if(frmIBTranferLP.segXferRecipentsList.data.length != searchList.length){
			frmIBTranferLP.segXferRecipentsList.setData(searchList);
		}
	}else{
		frmIBTranferLP.segXferRecipentsList.setData(searchList);
	}
	
	if(searchList.length <= 0) {
		frmIBTranferLP.segXferRecipentsList.setVisibility(false);
		frmIBTranferLP.lblMsg.setVisibility(true);
	} else {
		frmIBTranferLP.segXferRecipentsList.setVisibility(true);
		frmIBTranferLP.lblMsg.setVisibility(false);
	}
}


function searchResultOfP2PContactsIB(recipientsSegData, searchText) {
	var searchList = [];
	var recipientsSegDataLength = recipientsSegData.length;
	var recipientName = "";
	
	if (recipientsSegDataLength > 0) { // Atleast one recipient should be avilable

		if (searchText.length >= 1) { //Atleast 3 characters should be entered in search box

			for (j = 0, i = 0; i < recipientsSegDataLength; i++) {
				recipientName = recipientsSegData[i].lblXferRecipentsName;
				recipientNumber = removeHyphenIB(recipientsSegData[i].lblXferRecipentsAccount);
				if (recipientName.toLowerCase().indexOf(searchText) > -1) {
					searchList[j] = recipientsSegData[i];
					j++;
				}else if(recipientNumber.indexOf(searchText) > -1) {
					searchList[j] = recipientsSegData[i];
					j++;
				}
			}//For Loop End

		} else {//searchText condition End
			//If you Clear the Search Text then show All Transactions
				searchList = gblTransferToRecipientData;
		}
	}//recipientsSegDataLength condition End
	
	return searchList;
}

function onClickLabelMobileP2P(){
	frmIBTranferLP.txtXferMobileNumber.text = frmIBTranferLP.lblToMobileNo.text;
	frmIBTranferLP.txtXferMobileNumber.isVisible = true;
	frmIBTranferLP.txbXferAmountRcvd.text = "";
	hideHbxP2PFee();
	frmIBTranferLP.txtArMn.text = "";
	frmIBTranferLP.txtXferMobileNumber.setFocus(true);
	frmIBTranferLP.hbxSelMobileRecipient.isVisible = false;
	checkDisplayNotifyP2PMOorCI();
	resetXferSMS();
	resetHbxNotifyRecAndSenderNoteP2P();
}

function onTextChangeToMobileNoP2PIB(txt){
	//Max length of Mobile Number	
	var maxlength = 12;
	frmIBTranferLP.txtXferMobileNumber.maxTextLength = maxlength;
	
	var curr_form = frmIBTranferLP;
	if (txt == null) return false;
	var numChars = txt.length;
	if(kony.string.isNumeric(txt.charAt(numChars - 1))){
		//OK
	}
	else{
		curr_form.txtXferMobileNumber.text = txt.substring(0, numChars - 1);
		txt.substring(0, numChars - 1);
		numChars = numChars-1;
	}
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 5) {
				iphenText += '-';
			}
		}
		if(iphenText.length > 12){
        	iphenText = iphenText.substring(0, 12);
        }
		curr_form.txtXferMobileNumber.text = iphenText;
	}
	gblPrevLen = currLen;
	var mobileNumber = removeHyphenIB(curr_form.txtXferMobileNumber.text);
	if(!isNotBlank(frmIBTranferLP.lblMobileNumberTemp.text)){
		frmIBTranferLP.lblMobileNumberTemp.text = "";
	}
	if(mobileNumber.length == 10){
		frmIBTransferNowConfirmation.lblXferToName.text = "";
		if(!kony.string.equalsIgnoreCase(frmIBTranferLP.lblMobileNumberTemp.text,frmIBTranferLP.txtXferMobileNumber.text)){
			isOwnAccountP2P = false;
			if (!recipientMobileVal(mobileNumber)){
				showAlert(kony.i18n.getLocalizedString("MIB_P2PkeyEnteredMobileNumberisnotvalid"), kony.i18n.getLocalizedString("info"));
			}else{
				var enteredAmt = curr_form.txbXferAmountRcvd.text;
				if(isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0 ){
					checkCallCheckOnUsPromptPayinqServiceIB();
				}
			}
		}else{
			displayPrevValueForSameEnteredMobile();
		}
	}
}



function displayPrevValueForSameEnteredMobile(){
	frmIBTranferLP.hbxSelMobileRecipient.setVisibility(true);
	frmIBTranferLP.txtXferMobileNumber.setVisibility(false);
}

function onTextChangeToSMSMobileNoP2PIB(txt){
	var curr_form = frmIBTranferLP;
	if (txt == null) return false;
	var numChars = txt.length;
	if(kony.string.isNumeric(txt.charAt(numChars - 1))){
		//OK
	}
	else{
		curr_form.txtTransLndSmsNEmail.text = txt.substring(0, numChars - 1);
		txt.substring(0, numChars - 1);
		numChars = numChars-1;
	}
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 5) {
				iphenText += '-';
			}
		}
		if(iphenText.length > 12){
        	iphenText = iphenText.substring(0, 12);
        }
		curr_form.txtTransLndSmsNEmail.text = iphenText;
	}
	gblPrevLen = currLen;
}

function onTransferLndngNextP2PIB(){
	gblSplitAckImg = [];
	gblsplitAmt = [];
	/** check for Empty Fields on Tranfer LP ****/
	if (!validateTransferFieldsP2PIB()) {
		return false;
	}
	
	var availableBal;
	if (gblcwselectedData == 0) {
		var fromData = gblcwselectedData;
		availableBal = fromData[0].lblBalance;
	} else {
		availableBal = gblcwselectedData.lblBalance; //To be retrived from service
	}
	gblNofeeVarIB = gblcwselectedData.prodCode;
	availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	availableBal = availableBal.replace(/,/g, "");
	var availableBalFloat = parseFloat(availableBal.trim());
	//callCheckOnUsPromptPayinqServiceIB();
	var fee = frmIBTransferNowConfirmation.lblFeeVal.text;
	fee =  fee.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	var enteredAmountFloat = parseFloat(enteredAmount.trim());
	if ((availableBalFloat < (enteredAmountFloat + parseFloat(fee)))) {
		alert(kony.i18n.getLocalizedString("keyenteredamountexceedsavailablebalance"));
		return false;
	}
	if(isValidateFromToAccountIB(gblp2pAccountNumber) == false){
		dismissLoadingScreenPopup();
		if(gblSelTransferMode == 2){
			showAlert(kony.i18n.getLocalizedString("MIB_P2PErrMsgMobileNoLinkFromAcct"), kony.i18n.getLocalizedString("info"));
		}else{
			showAlert(kony.i18n.getLocalizedString("MIB_P2PErrMsgCILinkFromAcct"), kony.i18n.getLocalizedString("info"));
		}
        return false;        
    }
	goToNextafterCheckP2PEligibleIB();
}

/*************************************************************************

	Module	: validateTransferFieldsP2PIB
	Author  : Kony
	Purpose : validation on Tranfer Fields for P2P

***************************************************************************/
function validateTransferFieldsP2PIB() {
    if(!isNotBlank(frmIBTranferLP.txtXferMobileNumber.text)) {
        alert(kony.i18n.getLocalizedString("IB_P2PNoToValue_ERR"));
        frmIBTranferLP.txtXferMobileNumber.setFocus(true);
        return false;
    }else{
    	if(gblSelTransferMode == 2){
			if (!recipientMobileVal(removeHyphenIB(frmIBTranferLP.txtXferMobileNumber.text))){
				alert(kony.i18n.getLocalizedString("MIB_P2PkeyEnteredMobileNumberisnotvalid"));
				frmIBTranferLP.txtXferMobileNumber.setFocus(true);
				return false;
			}
		}else{
			if (!checkCitizenID(removeHyphenIB(frmIBTranferLP.txtXferMobileNumber.text))){
				alert(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"));
				frmIBTranferLP.txtXferMobileNumber.setFocus(true);
				return false;
			}
		}
	}
    if (!isNotBlank(frmIBTranferLP.txbXferAmountRcvd.text)) {
        alert(kony.i18n.getLocalizedString("MIB_P2PkeyPleaseEnterAmount"))
        return false;
    }
    // End #MIB-2274
	//MIB-4884-Allow special characters for My Note and Note to recipient field
	/*
    if (isNotBlank(frmIBTranferLP.txtArMn.text)){
		if(!MyNoteValid(frmIBTranferLP.txtArMn.text)){		
			alert(kony.i18n.getLocalizedString("MIB_TRkeyMynoteInvalid"));			
			return false;
		}		
	}
	*/
	if (isNotBlank(frmIBTranferLP.txtArMn.text)){
		if(checkSpecialCharMyNote(frmIBTranferLP.txtArMn.text)){		
			alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
			frmIBTranferLP.txtArMn.setFocus(true);			
			return false;
		}		
	}
	
    if (gblTrasSMS == 1) {
        var txtLen = frmIBTranferLP.txtTransLndSmsNEmail.text;
         if(txtLen.length== 0){
          	alert(kony.i18n.getLocalizedString("keyEnterMobileNum"));
          	return false;
		}else{
			if (!recipientMobileVal(removeHyphenIB(txtLen))){
				alert(kony.i18n.getLocalizedString("keyEnteredMobileNumberisnotvalid"));
				return false;
			}
		}
	    if (!isNotBlank(frmIBTranferLP.tbxXferNTR.text)) {
	        alert(kony.i18n.getLocalizedString("MIB_P2PkeyPleaseEnterRcipientNote"))
	        return false;
	    }
	    // End #MIB-2274
		//MIB-4884-Allow special characters for My Note and Note to recipient field
		/*
	    if (isNotBlank(frmIBTranferLP.tbxXferNTR.text)){
			if(!MyNoteValid(frmIBTranferLP.tbxXferNTR.text)){		
				alert(kony.i18n.getLocalizedString("MIB_TRkeyRecNoteInvalid"));
				return false;
			}		
		}
		*/
		
		if (isNotBlank(frmIBTranferLP.tbxXferNTR.text)){
			if(checkSpecialCharMyNote(frmIBTranferLP.tbxXferNTR.text)){		
				alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
				frmIBTranferLP.tbxXferNTR.setFocus(true);
				return false;
			}		
		}
		
	    var strMobNo = removeHyphenIB(txtLen);
	    frmIBTransferNowConfirmation.lblTranNotifyModeVal.text = kony.i18n.getLocalizedString("SMS") + " [xxx-xxx-"+ strMobNo.substring(6, 10) +"]";
		frmIBTransferNowConfirmation.lblSplitModTranVal.text = replaceHtmlTagChars(frmIBTranferLP.tbxXferNTR.text);
    }
    if(gblSelTransferMode == 3){
    	if (gblTransEmail == 1){
	    	if (!isNotBlank(frmIBTranferLP.tbxEmail.text)){
               	alert(kony.i18n.getLocalizedString("transferEmailID"));
               	frmIBTranferLP.tbxEmail.text = "";
               	return false
			}
             
            var isValidEmail = validateEmail(frmIBTranferLP.tbxEmail.text);
            emailNSms = kony.i18n.getLocalizedString("email") + " [" + frmIBTranferLP.tbxEmail.text + "]";
            if (isValidEmail == false) {
                alert(kony.i18n.getLocalizedString("keyPleaseEnterValidEMAILID"));
                frmIBTranferLP.tbxEmail.text = "";
                return false
            }
            if (!isNotBlank(frmIBTranferLP.textRecNoteEmail.text)) {
                alert(kony.i18n.getLocalizedString("keyPleaseEnterRcipientNote"))
                return false;
            }
            // End #MIB-2274
			//MIB-4884-Allow special characters for My Note and Note to recipient field
			/*
            if (isNotBlank(frmIBTranferLP.textRecNoteEmail.text)){
				if(!MyNoteValid(frmIBTranferLP.textRecNoteEmail.text)){		
					alert(kony.i18n.getLocalizedString("MIB_TRkeyRecNoteInvalid"));
					return false;
				}		
			}
			*/
			if (isNotBlank(frmIBTranferLP.textRecNoteEmail.text)){
				if(checkSpecialCharMyNote(frmIBTranferLP.textRecNoteEmail.text)){		
					alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
					frmIBTranferLP.textRecNoteEmail.setFocus(true);
					return false;
				}		
			}
			
            frmIBTransferNowConfirmation.lblTranNotifyModeVal.text = emailNSms;
            frmIBTransferNowConfirmation.lblSplitModTranVal.text = emailNSms;
	    }
    }
    return true;
}




function checkEligibilityForP2PIB(contactsToService){
		var inputParam = {}
		inputParam["numbersList"] = removeHyphenIB(contactsToService.toString());
		showLoadingScreenPopup();
		invokeServiceSecureAsync("checkEligibilityOfP2P", inputParam, checkEligibilityForP2PCallBackIB);

}

function checkEligibilityForP2PCallBackIB(status, resulttable){
	//success responce
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0 ){
			var anyIdDataSet = resulttable["CheckP2PResultDS"];
			if(anyIdDataSet.length > 0){
					var tempContacts = [];
					for(var i=0;i<anyIdDataSet.length;i++){
					
							gblTransferToRecipientData[i]["AcctIdentValue"] =anyIdDataSet[i]["AcctIdentValue"];
							gblTransferToRecipientData[i]["OnUsFlag"] = anyIdDataSet[i]["OnUsFlag"];
							gblTransferToRecipientData[i]["AcctTitle"] = anyIdDataSet[i]["AcctTitle"];
							gblTransferToRecipientData[i]["ITMXFlag"] = anyIdDataSet[i]["ITMXFlag"];
							gblTransferToRecipientData[i]["imgfav"] = "";
							if(anyIdDataSet[i]["OnUsFlag"] == "Y"){
								gblTransferToRecipientData[i]["imgfav"] = "tmb_recipient.png";
								tempContacts.push(gblTransferToRecipientData[i]);
							}else if(ITMX_TRANSFER_ENABLE == "true"){
								tempContacts.push(gblTransferToRecipientData[i]);
							}
					}
					
					gblTransferToRecipientData= tempContacts;
			frmIBTranferLP.segXferRecipentsList.widgetDataMap = {
				btnXferMobile: "btnXferMobile",
				btnXferFb: "btnXferFb",
				imgXferRecipentsImage: "imgXferRecipentsImage",
				imgXferArrow: "imgXferArrow",
				lblXferRecipentsName: "lblXferRecipentsName",
				lblXferRecipentsAccount: "lblXferRecipentsAccount",
				lbl3: "lbl3",
				lbl4: "lbl4",
				lbl5: "lbl5",
				lblBnkName: "lblBnkName",
				img2: "img2",
				img3: "img3",
				imgfav:"imgfav",
				onUsLogo:"onUsLogo"
			}						
				if(gblTransferToRecipientData.length > 0){
					frmIBTranferLP.segXferRecipentsList.data = gblTransferToRecipientData;
					frmIBTranferLP.segXferRecipentsList.setVisibility(true);
					showToField();
				}else{
					showAlert(kony.i18n.getLocalizedString("MIB_P2PErrNoLinkAcct"), kony.i18n.getLocalizedString("info"));
				}
				
			}
		}
		dismissLoadingScreenPopup();
	}
}

function isValidateFromToAccountIB(toAcctField){
    var formAcctNum = removeHyphenIB(frmIBTranferLP.lblTranLandFromNum.text);
    var toAcctNum = removeHyphenIB(toAcctField);
    if (formAcctNum == toAcctNum && gblisTMB == gblTMBBankCD){
        return false;
    }else{
        return true;
    }
}

function goToNextafterCheckP2PEligibleIB(){
	gblNofeeVarIB = "";
	if (gblTransSMART == 1 && gblPaynow) {
		frmIBTransferNowConfirmation.lblSmartTrfrDateVal.text = smartDate;
		frmIBTransferNowCompletion.lblSmartTrfrDateVal.text = smartDate;
			
		frmIBTransferNowConfirmation.hbxSmartTrfrDate.setVisibility(true);
		frmIBTransferNowCompletion.hbxSmartTrfrDate.setVisibility(true);
		
		frmIBTransferNowConfirmation.lblSmartDateSplitVal.text = smartDate;
		frmIBTransferNowCompletion.lblSmartDateSplitVal.text = smartDate;
		
		frmIBTransferNowConfirmation.hbxSmartDateSplit.setVisibility(true);
		frmIBTransferNowCompletion.hbxSmartDateSplit.setVisibility(true);
		
		frmIBTransferNowCompletion.hbox101458964925304.skin="hboxLightGrey320px";
		frmIBTransferNowCompletion.hbxTransNotif.skin="hbox320pxpadding";
		frmIBTransferNowCompletion.hbxTransNtr.skin="hboxLightGrey320px";
		frmIBTransferNowCompletion.hbox101458964925306.skin="hbox320pxpadding";
	} else {
		frmIBTransferNowConfirmation.lblSmartTrfrDateVal.text = "";
		frmIBTransferNowCompletion.lblSmartTrfrDateVal.text = "";
		
		frmIBTransferNowConfirmation.hbxSmartTrfrDate.setVisibility(false);
		frmIBTransferNowCompletion.hbxSmartTrfrDate.setVisibility(false);
		
		frmIBTransferNowConfirmation.lblSmartDateSplitVal.text = "";
		frmIBTransferNowCompletion.lblSmartDateSplitVal.text = "";
		
		frmIBTransferNowConfirmation.hbxSmartDateSplit.setVisibility(false);
		frmIBTransferNowCompletion.hbxSmartDateSplit.setVisibility(false);
		
		frmIBTransferNowCompletion.hbox101458964925304.skin="hbox320pxpadding";
		frmIBTransferNowCompletion.hbxTransNotif.skin="hboxLightGrey320px";
		frmIBTransferNowCompletion.hbxTransNtr.skin="hbox320pxpadding";
		frmIBTransferNowCompletion.hbox101458964925306.skin="hboxLightGrey320px";
	}
	frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfIntAmt.setVisibility(false);
	frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfTaxAmt.setVisibility(false);
	frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfPenaltyAmt.setVisibility(false);
	frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfNetAmt.setVisibility(false);
	frmIBTransferNowConfirmation.hbxScheduleDetails.setVisibility(false)
	
	frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpIntAmt.setVisibility(false);
	frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpTaxAmt.setVisibility(false);
	frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpPenaltyAmt.setVisibility(false);
	frmIBTransferNowCompletion.hbxfrmIBTransferNowCmpNetAmt.setVisibility(false);
	frmIBTransferNowCompletion.hbxScheduleDetails.setVisibility(false);
		
	gblTDDateFlag = false;
	if(gblPaynow){
		frmIBTransferNowConfirmation.hbxScheduleDetails.setVisibility(false);
		frmIBTransferNowCompletion.hbxScheduleDetails.setVisibility(false);
	}
	checkCrmProfileInqIB();
	if(gblTokenSwitchFlag && gblSwitchToken == false){
	
		frmIBTransferNowConfirmation.hbxToken.setVisibility(true);
		frmIBTransferNowConfirmation.tbxToken.setFocus(true);
		frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(false);
	} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
		frmIBTransferNowConfirmation.hbxToken.setVisibility(false);
		frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(true);
	}
	frmIBTransferNowConfirmation.hbxOtpBox.setVisibility(false);
	
	gblsplitAmt = [];
	gblsplitFee = [];
	gblSplitAckImg = [];
	gblSplitCnt = 0;
	gblSplitStatusInfo = [];
	if((gblTransEmail == "0" && gblTrasSMS == "0")||(gblTransEmail == "1" && gblTrasSMS == "1")){
		frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
		frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
		frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);
		frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
		frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(false);
		frmIBTransferNowConfirmation.hbxSplitNtr.setVisibility(false);
		frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
		frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);
		frmIBTransferNowConfirmation.hbox101458964925294.setVisibility(true);
        frmIBTransferNowCompletion.hbox101458964925304.setVisibility(true);
	}else{
		frmIBTransferNowCompletion.hbxTransNotif.setVisibility(true);
		frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(true);
		frmIBTransferNowCompletion.hbxTransNtr.setVisibility(true);
		frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(true);	
		frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(true);	
		frmIBTransferNowConfirmation.hbxSplitNtr.setVisibility(true);
		frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(true);
		frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(true);
		frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(true);
	}
}

function resetHbxNotifyRecAndSenderNoteP2P(){
	frmIBTranferLP.btnXferSMSP2P.skin = btnIBSMSBothRoundConerGrey;
    frmIBTranferLP.btnXferSMSP2P.focusSkin = btnIBSMSBothRoundConerBlue;
    frmIBTranferLP.hbxSMS.setVisibility(false);
    frmIBTranferLP.txtTransLndSmsNEmail.text = "";
    frmIBTranferLP.txtTransLndSmsNEmail.placeholder =  kony.i18n.getLocalizedString("TransferMobileNo");
    frmIBTranferLP.tbxXferNTR.text = "";
    frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
    frmIBTranferLP.lineTwo.setVisibility(false);
    frmIBTranferLP.lineThree.setVisibility(false);
    gblTrasSMS = 0;
}

function formatAmountOnTextChangeIB(){
	var enteredAmount = frmIBTranferLP.txbXferAmountRcvd.text;
	if(isNotBlank(enteredAmount)) {
		enteredAmount = kony.string.replace(enteredAmount, ",", "");
		frmIBTranferLP.txbXferAmountRcvd.text = commaFormattedTransfer(enteredAmount);
	}else{
		displayITMXFeeP2P(false);
	}
	var enteredAmt = frmIBTranferLP.txbXferAmountRcvd.text;
	if((gblSelTransferMode == 2 || gblSelTransferMode == 3) && !isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) <= 0){
		hideHbxP2PFee();
	}
}

function hideHbxP2PFee(){
	frmIBTranferLP.lblP2PFeeVal.text ="";
	displayITMXFeeP2P(false);
}

function callBackP2PIBAmountFields(response){
	if (response == true) {
		frmIBTranferLP.txbXferAmountRcvd.setFocus(true);
	}
}

function clearValueIBTransferLP() {
	clearValueHbxToLP();
	clearValueHbxToP2PLP();
	showP2PTextBoxIB(true);
	resetXferSMS();
    frmIBTranferLP.txbXferAmountRcvd.text = "";
    frmIBTranferLP.txtArMn.text = "";
    frmIBTranferLP.txtXferMobileNumber.text = "";
    frmIBTranferLP.lblXferInterval.text = "";
    frmIBTranferLP.lblXferInterval.setVisibility(false);
    frmIBTranferLP.hboxXferlblFee.setVisibility(false);
    frmIBTranferLP.hbxFee.setVisibility(false);
    frmIBTranferLP.hbxFeeBtns.setVisibility(false);
    frmIBTranferLP.lineBelowFee.setVisibility(false);
}

function resetXferSMS() {
	frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
	frmIBTranferLP.hbxSMS.setVisibility(false);
	frmIBTranferLP.hbxEmail.setVisibility(false);
	frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
	frmIBTranferLP.tbxXferNTR.text = "";
	frmIBTranferLP.txtTransLndSmsNEmail.text = "";
	frmIBTranferLP.tbxEmail.text = "";
	frmIBTranferLP.textRecNoteEmail.text = "";
	frmIBTranferLP.lineTwo.setVisibility(false);
	frmIBTranferLP.btnXferSMS.skin = btnIbSMSRoundedCorner;
	frmIBTranferLP.btnXferEmail.skin = btnIBemailRoundedCorner;
	frmIBTranferLP.btnXferSMSP2P.skin = btnIBSMSBothRoundConerGrey;
    frmIBTranferLP.btnXferSMSP2P.focusSkin = btnIBSMSBothRoundConerBlue;
	gblTrasSMS = 0;
	gblTransEmail = 0;
}



function checkpromptPayInqIB(amt) {
	var inputParam = {}
	var fromAcctID;
	fromAcctID = gblcwselectedData.accountWOF;
	fromAcctID = replaceCommon(fromAcctID,"-", "");
	var toAcctID = frmIBTranferLP.txtXferMobileNumber.text;
	var prodCode = gblcwselectedData.prodCode;
	toAcctID = replaceCommon(toAcctID, "-", "");
	if (kony.string.containsChars(amt, ["."]))
		amt = amt;
	else
		amt = amt + ".00";
	inputParam["fromAcctNo"] = fromAcctID;		
	inputParam["toAcctNo"] = toAcctID;
	inputParam["toFIIdent"] = gblisTMB;		
	inputParam["transferAmt"] = amt;
	inputParam["prodCode"] = prodCode;
	if (gblSelTransferMode == 3){
		inputParam["mobileOrCI"] = "01";
	}else if (gblSelTransferMode == 2){
		inputParam["mobileOrCI"] = "02";
	}
	showLoadingScreenPopup();
	invokeServiceSecureAsync("promptPayInq", inputParam, callBackpromptPayInqIB);
 }
 
 function callBackpromptPayInqIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var StatusCode = resulttable["StatusCode"];
			if (StatusCode != "0") {
				dismissLoadingScreenPopup();
				if(resulttable["errMsg"] != undefined){
		        	showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
		        	return false;
		        }else{
		        	showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
		        }	return false;
			} else {
				var ToAccountName = resulttable["acctTitle"];
				if(gblisTMB != gblTMBBankCD){
					ToAccountName = resulttable["toAcctName"];
				}
				if(!isNotBlank(ToAccountName) && gblisTMB != gblTMBBankCD){
					if(gblSelTransferMode == 2){
						showAlert(kony.i18n.getLocalizedString("MIB_P2PNullAcctName"), kony.i18n.getLocalizedString("info"));
					}else if(gblSelTransferMode == 3){
						showAlert(kony.i18n.getLocalizedString("MIB_P2PNullAcctName2"), kony.i18n.getLocalizedString("info"));
					}
					frmIBTranferLP.txtXferMobileNumber.text = "";
					showP2PTextBoxIB(true);
					dismissLoadingScreen();
					return false;
				}
				
				gblSmartAccountName = ToAccountName;
                gblp2pAccountNumber = resulttable["toAcctNo"];
                gblisTMB = resulttable["DestBankCode"];
                if(!isNotBlank(gblisTMB)){
					showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
					dismissLoadingScreenPopup();
					return false;
				}
				
                frmIBTransferNowConfirmation.lblFeeVal.text = resulttable["itmxFee"] + " " +kony.i18n.getLocalizedString("currencyThaiBaht");
				frmIBTransferNowConfirmation.lblXferToAccTyp.text = "";
				//frmIBTransferNowConfirmation.lblXferToName.text = frmIBTranferLP.lblXferToContactRcvdMobile.text;
				frmIBTransferNowConfirmation.lblXferToAccTyp.text = ToAccountName;
				frmIBTransferNowConfirmation.image247327596554550.src = frmIBTranferLP.imgXferToImageP2P.src;
				/** checking if transfer is happeninf from TD account(i.e AccountPreWithDrawInq  */
				if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
					checkAccountWithdrawInqIB();
				} else {
					checkTransferTypeIB();
				}
				dismissLoadingScreenPopup();
			}
		} else {
			if (resulttable["promptPayFlag"] == "1"){
				var errCode = resulttable["errCode"];
				if (!isNotBlank(errCode)){
					showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				
				}else if(errCode == 'XB240066'){
					if(gblSelTransferMode == 2){
						showAlert(kony.i18n.getLocalizedString("MIB_P2PNullAcctName"), kony.i18n.getLocalizedString("info"));
					}else if(gblSelTransferMode == 3){
						showAlert(kony.i18n.getLocalizedString("MIB_P2PnullAcctName2"), kony.i18n.getLocalizedString("info"));
					}								
				}else{
					var errorText = "";
					if(isNotBlank(resulttable["errB24Msg"])){
						if(gblSelTransferMode == 2){
							errorText = resulttable["errB24Msg"];
				            errorText = errorText.replace("{mobile_no}", frmIBTranferLP.txtXferMobileNumber.text);
						}else if(gblSelTransferMode == 3){
							errorText = displayNotEligibleCitizenIDIB();
						}
					}else{
						errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode +")";
					}	
					showAlert(errorText, kony.i18n.getLocalizedString("info"));
				}
			}else{
				var errorText = "";
				if(gblSelTransferMode == 2){
					errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrMobtNoInvalid");
		            errorText = errorText.replace("{mobile_no}", frmIBTranferLP.txtXferMobileNumber.text);	            
				}else if(gblSelTransferMode == 3){
					errorText = displayNotEligibleCitizenIDIB();            
				}				
	            showAlert(errorText, kony.i18n.getLocalizedString("info"));
			}
            frmIBTranferLP.txtXferMobileNumber.text = "";
            showP2PTextBoxIB(true);
			dismissLoadingScreenPopup();
			return false;
		}
	}
}

function checkCallCheckOnUsPromptPayinqServiceIB(){
	var enteredAmt = frmIBTranferLP.txbXferAmountRcvd.text;
	if(isNotBlank(enteredAmt) && parseFloat(enteredAmt, 10) > 0 ){
		callCheckOnUsPromptPayinqServiceIB();
		checkDisplayNotifyP2PMOorCI();
	}else{
		displayTxtMobileP2P(true);
		closeRightPanelTransfer();
	}
}

function callCheckOnUsPromptPayinqServiceIB(){
	var inputParam = {}
	var fromAcctID;
	fromAcctID = gblcwselectedData.accountWOF;
	fromAcctID = replaceCommon(fromAcctID,"-", "");
	var toAcctID = frmIBTranferLP.txtXferMobileNumber.text;
	var prodCode = gblcwselectedData.prodCode;
	var locale = kony.i18n.getCurrentLocale();		
	toAcctID = replaceCommon(toAcctID, "-", "");
	inputParam["fromAcctNo"] = fromAcctID;		
	inputParam["toAcctNo"] = toAcctID;
	inputParam["toFIIdent"] = gblisTMB;		
	inputParam["transferAmt"] = enteredAmount;
	inputParam["prodCode"] = prodCode;
	inputParam["locale"] = locale;
	if (gblSelTransferMode == 3){
		inputParam["mobileOrCI"] = "01";
	}else if (gblSelTransferMode == 2){
		inputParam["mobileOrCI"] = "02";
	}	

	showLoadingScreenPopup();
	invokeServiceSecureAsync("checkOnUsPromptPayinq", inputParam, callBackCheckOnUsPromptPayinqServiceIB);
}

function callBackCheckOnUsPromptPayinqServiceIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {			
			gblisTMB = resulttable["destBankCode"];			
			if(!isNotBlank(gblisTMB)){
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreenPopup();
				return false;
			}
			var ToAccountName = resulttable["toAccTitle"];
			if(gblisTMB != gblTMBBankCD){
				ToAccountName = resulttable["toAcctName"];
			}
			if(!isNotBlank(ToAccountName) && gblisTMB != gblTMBBankCD){
			
				if(gblSelTransferMode == 2){
					showAlert(kony.i18n.getLocalizedString("MIB_P2PNullAcctName"), kony.i18n.getLocalizedString("info"));
				}else if(gblSelTransferMode == 3){
					showAlert(kony.i18n.getLocalizedString("MIB_P2PnullAcctName2"), kony.i18n.getLocalizedString("info"));
				}								
				frmIBTranferLP.txtXferMobileNumber.text = "";
				showP2PTextBoxIB(true);
				dismissLoadingScreenPopup();
				return false;
			}
			var isOtherBank =gblcwselectedData.isOtherBankAllowed;
			var isOtherTmb = gblcwselectedData.isOtherTMBAllowed;
			var isAllowedSA = gblcwselectedData.isAllowedSA;
			var isAllowedCA = gblcwselectedData.isAllowedCA;
			var isAllowedTD = gblcwselectedData.isAllowedTD;
			var fromAccNumber = gblcwselectedData.accountNum;
			var prodCode = gblcwselectedData.prodCode;
			if(isOtherBank != "Y" && gblisTMB != gblTMBBankCD){
				if (gblSelTransferMode == 2){
					showAlert(kony.i18n.getLocalizedString("MIB_P2PErrFromToNotMatch"), kony.i18n.getLocalizedString("info"));
				}else if (gblSelTransferMode == 3){
					showAlert(kony.i18n.getLocalizedString("MIB_P2PErrFromToNotMatch2"), kony.i18n.getLocalizedString("info"));
				}
							
				frmIBTranferLP.txtXferMobileNumber.text = "";
				frmIBTranferLP.txbXferAmountRcvd.text = "";
				dismissLoadingScreenPopup();
				return false;
			}
			if(frmIBTransferNowConfirmation.lblXferToName.text == ""){
				if (gblSelTransferMode == 2){
					frmIBTransferNowConfirmation.lblXferToName.text = kony.i18n.getLocalizedString("MIB_P2PMob"); //getBankShortName(gblisTMB);
				}else if (gblSelTransferMode == 3){
					frmIBTransferNowConfirmation.lblXferToName.text = kony.i18n.getLocalizedString("MIB_P2PCiti");
				}				
				frmIBTransferNowConfirmation.lblXferToName.setVisibility(false);
			}
			isOwnAccountP2P = false;
            if(resulttable["isOwn"] == "Y"){
                isOwnAccountP2P = true;
            }
			gblSelectedRecipentName = frmIBTransferNowConfirmation.lblXferToName.text;
			gblp2pAccountNumber = resulttable["toAcctNo"];
			if(ITMX_TRANSFER_ENABLE == "true"){
				frmIBTransferNowConfirmation.lblFeeVal.text = resulttable["itmxFee"] + " " +kony.i18n.getLocalizedString("currencyThaiBaht");
			}else{
				frmIBTransferNowConfirmation.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}			
			frmIBTranferLP.lblXferToContactRcvdMobile.text =  ToAccountName;
			gblSelectedRecipentName = ToAccountName;
			frmIBTranferLP.lblMobileNumberTemp.text = frmIBTranferLP.txtXferMobileNumber.text;
			frmIBTranferLP.lblToMobileNo.text = frmIBTranferLP.txtXferMobileNumber.text;
			frmIBTranferLP.lblBankName.text = getBankNameIB(gblisTMB);
			if (gblSelTransferMode == 2){
				frmIBTranferLP.imgXferToImageP2P.src =  getBankLogoURL("toMobile");
			}else if(gblSelTransferMode == 3){
				frmIBTranferLP.imgXferToImageP2P.src =  getBankLogoURL("toCitizen");
			}			
			frmIBTransferNowConfirmation.image247327596554550.src = frmIBTranferLP.imgXferToImageP2P.src;
			showHideBankDetails(false);
			if(isValidateFromToAccountIB(gblp2pAccountNumber) == false){
				if (gblSelTransferMode == 2){
		        	showAlert(kony.i18n.getLocalizedString("MIB_P2PErrMsgMobileNoLinkFromAcct"), kony.i18n.getLocalizedString("info"));
		        }else if (gblSelTransferMode == 3){
		        	showAlert(kony.i18n.getLocalizedString("MIB_P2PErrMsgCILinkFromAcct"), kony.i18n.getLocalizedString("info"));
		        }
		        dismissLoadingScreenPopup();
		        return false;        
		    }
		    resetHbxNotifyRecAndSenderNoteP2P();
		   	checkDisplayNotifyP2PMOorCI();
			calcuateITMXFeeIB();
			showP2PTextBoxIB(false);
			closeRightPanelTransfer();
			dismissLoadingScreenPopup();
		} else {
			if (resulttable["promptPayFlag"] == "1"){
				var errCode = resulttable["errCode"];
				var errorText = "";
				if (!isNotBlank(errCode)){
					showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));				
				}else if(errCode == 'XB240048'){
					if(gblSelTransferMode == 2){
						errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrMobtNoInvalid");
				        errorText = errorText.replace("{mobile_no}", frmIBTranferLP.txtXferMobileNumber.text);
					}else if(gblSelTransferMode == 3){
				        errorText = displayNotEligibleCitizenIDIB();	
				        
					}
					showAlert(errorText, kony.i18n.getLocalizedString("info"));
				}else if(errCode == 'XB240066'){
					if(gblSelTransferMode == 2){
						showAlert(kony.i18n.getLocalizedString("MIB_P2PNullAcctName"), kony.i18n.getLocalizedString("info"));
					}else if(gblSelTransferMode == 3){
						showAlert(kony.i18n.getLocalizedString("MIB_P2PnullAcctName2"), kony.i18n.getLocalizedString("info"));
					}
				}else if(errCode == 'XB240088'){
					if(gblSelTransferMode == 2){
						errorText = kony.i18n.getLocalizedString("MIB_P2PAccInActive");
						showAlert(errorText, kony.i18n.getLocalizedString("info"));
					}else if(gblSelTransferMode == 3){
						errorText = kony.i18n.getLocalizedString("MIB_P2PAccInActive");
						showAlert(errorText, kony.i18n.getLocalizedString("info"));
					}								
				}else if(errCode == 'XB240067'){
					if(gblSelTransferMode == 2){
						errorText = kony.i18n.getLocalizedString("MIB_P2PCutOffTime");
						showAlert(errorText, kony.i18n.getLocalizedString("info"));
					}else if(gblSelTransferMode == 3){
						errorText = kony.i18n.getLocalizedString("MIB_P2PCutOffTime");
						showAlert(errorText, kony.i18n.getLocalizedString("info"));
					}								
				}else if(errCode == 'XB240072'){
					if(gblSelTransferMode == 2){
						errorText = kony.i18n.getLocalizedString("MIB_P2PCommunicationErr");
						showAlert(errorText, kony.i18n.getLocalizedString("info"));
					}else if(gblSelTransferMode == 3){
						errorText = kony.i18n.getLocalizedString("MIB_P2PCommunicationErr");
						showAlert(errorText, kony.i18n.getLocalizedString("info"));
					}								
				}else if(errCode == 'X8899'){					
					errorText = kony.i18n.getLocalizedString("MIB_P2PCloseBranchErr");
					showAlert(errorText, kony.i18n.getLocalizedString("info"));													
				}else{
					errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode +")";
					showAlert(errorText, kony.i18n.getLocalizedString("info"));
				}
			}else{
				var errorText = "";
				if(gblSelTransferMode == 2){
					errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrMobtNoInvalid");
		            errorText = errorText.replace("{mobile_no}", frmIBTranferLP.txtXferMobileNumber.text);	            
				}else if(gblSelTransferMode == 3){
		            errorText = displayNotEligibleCitizenIDIB();	            
				}				
	            showAlert(errorText, kony.i18n.getLocalizedString("info"));
			}
            frmIBTranferLP.txtXferMobileNumber.text = "";
            showP2PTextBoxIB(true);
			dismissLoadingScreenPopup();
			return false;
		}
	}
}

function getBankLogoURL(bankCode){
	var randomnum = Math.floor((Math.random()*10000)+1);
	return "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+bankCode+"&modIdentifier=BANKICON&rr="+randomnum;
}


function calcuateITMXFeeIB(){
	displayITMXFeeP2P(true);
    if(gblisTMB == gblTMBBankCD){
    	frmIBTranferLP.lblP2PFeeVal.text = kony.i18n.getLocalizedString("keyFreeTransfer");
    }else{
    	var enteredAmount = frmIBTranferLP.txbXferAmountRcvd.text;
	   	enteredAmount = kony.string.replace(enteredAmount, ",", "");
	   	frmIBTranferLP.txbXferAmountRcvd.text = commaFormattedTransfer(enteredAmount);
	   	var fee = getITMXTransferFee(enteredAmount);
	   	if(parseFloat(fee) == 0){
	   		frmIBTranferLP.lblP2PFeeVal.text = kony.i18n.getLocalizedString("keyFreeTransfer");
	   	}else{
	   		frmIBTranferLP.lblP2PFeeVal.text = fee + " " +kony.i18n.getLocalizedString("currencyThaiBaht");
	   	}
    }
}



function checkDisplayForHbxSelRecipient(){
	if(kony.string.equalsIgnoreCase(frmIBTranferLP.lblMobileNumberTemp.text,frmIBTranferLP.txtXferMobileNumber.text)){
		displayPrevValueForSameEnteredMobile();
		calcuateITMXFeeIB();
	}
}

function checkDisplayNotifyP2PMOorCI(){
	if(isOwnAccountP2P){
		showHideNotifyRecipientP2PIB(false);
		showHideNotifyRecipientP2PCIIB(false);
	}else{
		if(gblSelTransferMode == 2){
			showHideNotifyRecipientP2PCIIB(false);
			showHideNotifyRecipientP2PIB(true);
		}else{
			showHideNotifyRecipientP2PIB(false);
			showHideNotifyRecipientP2PCIIB(true);
			
		}
	}
}

function displayNotEligibleCitizenIDIB(){
	var errorText = "";
	if(ITMX_TRANSFER_ENABLE == "true"){ 
		errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrCINotRegisTurnOn");
	} else {
		errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrCINotRegisTurnOff");
	}
    return errorText.replace("{citizenID}", frmIBTranferLP.txtXferMobileNumber.text);
}
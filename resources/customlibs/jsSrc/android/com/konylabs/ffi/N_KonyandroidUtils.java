package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.geolocation.GeoLocation;
import com.kony.downloadfiles.KonyDM;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_KonyandroidUtils extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[2];
 libs[0] = new GeoLocation();
 libs[1] = new KonyDM();
 return libs;
 }



	public N_KonyandroidUtils(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "KonyandroidUtils";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class GeoLocation extends JSLibrary {

 
 
	public static final String getLocation = "getLocation";
 
	String[] methods = { getLocation };

	public Object createInstance(final Object[] params) {
 return new com.kony.geolocation.GeoLocation(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 2 || paramLen > 3){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function successCB0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 successCB0 = (com.konylabs.vm.Function)params[0+inc];
 }
 com.konylabs.vm.Function errorCB0 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 errorCB0 = (com.konylabs.vm.Function)params[1+inc];
 }
 ret = this.getLocation(params[0]
 ,successCB0
 ,errorCB0
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "GeoLocation";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] getLocation( Object self ,com.konylabs.vm.Function inputKey0
 ,com.konylabs.vm.Function inputKey1
 ){
 
		Object[] ret = null;
 ((com.kony.geolocation.GeoLocation)self).getGeoLocation( (com.konylabs.vm.Function)inputKey0
 , (com.konylabs.vm.Function)inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}



class KonyDM extends JSLibrary {

 
 
	public static final String downloadFile = "downloadFile";
 
	String[] methods = { downloadFile };

	public Object createInstance(final Object[] params) {
 return new com.kony.downloadfiles.KonyDM(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 2 || paramLen > 3){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 java.lang.String url0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 url0 = (java.lang.String)params[0+inc];
 }
 com.konylabs.vm.Function callbackDownload0 = null;
 if(params[1+inc] != null && params[1+inc] != LuaNil.nil) {
 callbackDownload0 = (com.konylabs.vm.Function)params[1+inc];
 }
 ret = this.downloadFile(params[0]
 ,url0
 ,callbackDownload0
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "KonyDM";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] downloadFile( Object self ,java.lang.String inputKey0
 ,com.konylabs.vm.Function inputKey1
 ){
 
		Object[] ret = null;
 ((com.kony.downloadfiles.KonyDM)self).downloadFile( inputKey0
 , (com.konylabs.vm.Function)inputKey1
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};

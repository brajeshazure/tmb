package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kony.barcodescaner.ScanBarcode;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_barcode extends JSLibrary {

 
	String[] methods = { };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[1];
 libs[0] = new BarScan();
 return libs;
 }



	public N_barcode(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "barcode";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 


class BarScan extends JSLibrary {

 
 
	public static final String scanbarcode = "scanbarcode";
 
 
	public static final String imagereader = "imagereader";
 
	String[] methods = { scanbarcode, imagereader };

	public Object createInstance(final Object[] params) {
 return new com.kony.barcodescaner.ScanBarcode(
 );
 }


	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function callback0 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 callback0 = (com.konylabs.vm.Function)params[0+inc];
 }
 ret = this.scanbarcode(params[0]
 ,callback0
 );
 
 			break;
 		case 1:
 if (paramLen < 1 || paramLen > 2){ return new Object[] {new Double(100),"Invalid Params"};}
 inc = 1;
 
 com.konylabs.vm.Function callback1 = null;
 if(params[0+inc] != null && params[0+inc] != LuaNil.nil) {
 callback1 = (com.konylabs.vm.Function)params[0+inc];
 }
 ret = this.imagereader(params[0]
 ,callback1
 );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "BarScan";
	}

	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] scanbarcode( Object self ,com.konylabs.vm.Function inputKey0
 ){
 
		Object[] ret = null;
 ((com.kony.barcodescaner.ScanBarcode)self).scanBarcode( (com.konylabs.vm.Function)inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] imagereader( Object self ,com.konylabs.vm.Function inputKey0
 ){
 
		Object[] ret = null;
 ((com.kony.barcodescaner.ScanBarcode)self).barcodeImageReader( (com.konylabs.vm.Function)inputKey0
 );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
}

};

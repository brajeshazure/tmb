function p2kwiet2012247631214_txtEditAmnt_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    if (frmMBFTEdit.txtEditAmnt.text == "") {
        frmMBFTEdit.txtEditAmnt.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmMBFTEdit.txtEditAmnt.text = commaFormatted(parseFloat(removeCommos(frmMBFTEdit.txtEditAmnt.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}
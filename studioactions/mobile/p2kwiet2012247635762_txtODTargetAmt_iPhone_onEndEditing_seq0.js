function p2kwiet2012247635762_txtODTargetAmt_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    var entAmt = frmOpenAcDreamSaving.txtODTargetAmt.text;
    var isCrtFormt;
    isCrtFormt = amountValidationMB(entAmt);
    if (!isCrtFormt) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        return false
    }
    var indexdot = entAmt.indexOf(".");
    var decimal = "";
    var remAmt = "";
    kony.print("indexdot" + indexdot);
    if (indexdot > 0) {
        decimal = entAmt.substr(indexdot);
        kony.print("decimal@@" + decimal);
        if (decimal.length > 3) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    frmOpenAcDreamSaving.txtODTargetAmt.skin = "txtNormalBG"
    var AddOpenBahtDreamOpen = frmOpenAcDreamSaving.txtODTargetAmt.text;
    frmOpenAcDreamSaving.txtODTargetAmt.text = commaFormattedOpenAct(AddOpenBahtDreamOpen) + kony.i18n.getLocalizedString("currencyThaiBaht")
}
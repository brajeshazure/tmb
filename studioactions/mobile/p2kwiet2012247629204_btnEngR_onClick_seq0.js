function p2kwiet2012247629204_btnEngR_onClick_seq0(eventobject) {
    //#ifdef spaip
    //#define preprocessdecision_onClick_80121201224767582_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define preprocessdecision_onClick_80121201224767582_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define preprocessdecision_onClick_80121201224767582_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define preprocessdecision_onClick_80121201224767582_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define preprocessdecision_onClick_80121201224767582_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define preprocessdecision_onClick_80121201224767582_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef preprocessdecision_onClick_80121201224767582_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    if ((kony.i18n.getCurrentLocale() != "en_US")) {
        gblLang_flag = "en_US";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeEngMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        //gblLang_flag = "en_US";
        //kony.i18n.setCurrentLocaleAsync("en_US", onSuccessLocaleChange, onFailureLocaleChange, "");
        //frmMBTnC.lblTandCSpa.text="";
        setLocaleEng();
        frmFATCAQuestionnaire1.btnEngR.skin = btnOnFocus;
        frmFATCAQuestionnaire1.btnThaiR.skin = btnOffFocus;
        if (gblFATCAPage == 1) frmFATCAQuestionnaire1PreShow();
        else frmFATCAQuestionnaire2PreShow();
    } else {}
    //#endif
    //#ifdef android
    //#define preprocessdecision_onClick_97268201224763278_android_iphone
    //#endif
    //#ifdef iphone
    //#define preprocessdecision_onClick_97268201224763278_android_iphone
    //#endif
    //#ifdef preprocessdecision_onClick_97268201224763278_android_iphone
    if ((kony.i18n.getCurrentLocale() != "en_US")) {
        gblLang_flag = "en_US";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeEngMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        showLoadingScreen();
        setLocaleEng();
        frmFATCAQuestionnaire1.btnEngR.skin = btnOnFocus;
        frmFATCAQuestionnaire1.btnThaiR.skin = btnOffFocus;
    } else {}
    //#endif
}
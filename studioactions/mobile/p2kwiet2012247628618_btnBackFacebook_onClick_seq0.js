function p2kwiet2012247628618_btnBackFacebook_onClick_seq0(eventobject) {
    var form = kony.application.getPreviousForm();
    var condition = null;
    if (form.id.search("MyRecipient") > 0) {
        condition = "1";
    } else if (form.id == "frmeditMyProfile") {
        condition = "2";
    } else {
        condition = "3";
    }
    gblPOWstateTrack = true;
    if ((condition == "1")) {
        getFriendListFromFacebook.call(this, eventobject);
    } else {
        if ((condition == "2")) {
            gblMyFBdelinkTrack = true;
            fbprofileviewServiceCall.call(this);
        } else {
            postOnWallAfter.call(this);
        }
    }
}
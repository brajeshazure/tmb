function p2kwiet2012247626847_btnSpaNext_onClick_seq0(eventobject) {
    function alert_onClick_60726201224769670_True() {}

    function alert_onClick_46560201224767753_True() {}
    frmAddTopUpToMB.txbNickName.skin = txtNormalBG;
    frmAddTopUpToMB.txbNickName.focusSkin = txtFocusBG;
    frmAddTopUpToMB.txtRef1.skin = txtNormalBG;
    frmAddTopUpToMB.txtRef1.focusSkin = txtFocusBG;
    if ((gblMyBillerTopUpBB == 2)) {
        if ((isMenuShown == false)) {
            if ((gblMyBillerTopUpBB == 2)) {
                onClickNextMBApplyBB.call(this);
            } else {
                if ((checkDuplicateNicknameOnAddTopUp())) {
                    nicknameRef1Ref2ValidationCheck.call(this);
                } else {
                    function alert_onClick_46560201224767753_Callback() {
                        alert_onClick_46560201224767753_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": "Duplicate Nickname !",
                        "alertHandler": alert_onClick_46560201224767753_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                }
            }
        } else {
            frmBBPaymentApply.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    } else {
        if ((isMenuShown == false)) {
            if ((frmAddTopUpToMB.txbNickName.text.length > 2 && isBillerSelected())) {
                if ((checkDuplicateNicknameOnAddTopUp())) {
                    tokenExchangeBeforeSaveParamsInSessSPA.call(this);
                } else {
                    function alert_onClick_60726201224769670_Callback() {
                        alert_onClick_60726201224769670_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Ok",
                        "noLabel": "No",
                        "message": kony.i18n.getLocalizedString("Valid_DuplicateNickname"),
                        "alertHandler": alert_onClick_60726201224769670_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                }
            } else {
                if (!isBillerSelected()) {
                    alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
                } else {
                    frmAddTopUpToMB.txbNickName.skin = txtErrorBG;
                    frmAddTopUpToMB.txbNickName.focusSkin = txtErrorBG;
                    alert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"));
                }
            }
        } else {
            frmAddTopUpToMB.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    }
}
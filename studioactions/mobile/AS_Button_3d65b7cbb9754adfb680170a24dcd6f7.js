function AS_Button_3d65b7cbb9754adfb680170a24dcd6f7(eventobject) {
    //#ifdef spaip
    //#define CHANNEL_CONDITION_ide_onClick_dea42b9de35246ccafa6b92f4c213d56_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define CHANNEL_CONDITION_ide_onClick_dea42b9de35246ccafa6b92f4c213d56_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define CHANNEL_CONDITION_ide_onClick_dea42b9de35246ccafa6b92f4c213d56_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define CHANNEL_CONDITION_ide_onClick_dea42b9de35246ccafa6b92f4c213d56_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define CHANNEL_CONDITION_ide_onClick_dea42b9de35246ccafa6b92f4c213d56_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define CHANNEL_CONDITION_ide_onClick_dea42b9de35246ccafa6b92f4c213d56_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef CHANNEL_CONDITION_ide_onClick_dea42b9de35246ccafa6b92f4c213d56_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    if ((kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH")) {
        gblLang_flag = "th_TH";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeThaiMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        frmMBTnC.lblTandCSpa.text = "";
        setLocaleTH();
        //gblLang_flag = "th_TH";
        //kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChange, onFailureLocaleChange, "");
        frmMBTnC.btnEngR.skin = btnOnNormal;
        frmMBTnC.btnThaiR.skin = btnOffNorm;
        frmMBTnCPreShow();
    } else {}
    //#endif
    //#ifdef android
    //#define CHANNEL_CONDITION_ide_onClick_b7e834e303724f8493e68c70aec6d35b_android_iphone
    //#endif
    //#ifdef iphone
    //#define CHANNEL_CONDITION_ide_onClick_b7e834e303724f8493e68c70aec6d35b_android_iphone
    //#endif
    //#ifdef CHANNEL_CONDITION_ide_onClick_b7e834e303724f8493e68c70aec6d35b_android_iphone
    if ((kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH")) {
        gblLang_flag = "th_TH";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeThaiMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        showLoadingScreen();
        frmMBTnC.lblTandCTh.text = "";
        frmMBTnC.hbxTandCEng.setVisibility(false);
        frmMBTnC.hbxTandCTh.setVisibility(true);
        setLocaleTH();
        frmMBTnC.btnEngR.skin = btnOnNormal;
        frmMBTnC.btnThaiR.skin = btnOffNorm;
        frmMBTnCPreShow();
    } else {}
    //#endif
}
function p2kwiet2012247635762_txtODTargetAmt_onTextChange_seq0(eventobject, changedtext) {
    if (frmOpenAcDreamSaving.txtODTargetAmt.text == "0" || frmOpenAcDreamSaving.txtODTargetAmt.text == "" || frmOpenAcDreamSaving.txtODTargetAmt.text == null) {
        frmOpenAcDreamSaving.hbxODMnthSavAmt.setEnabled(false);
        frmOpenAcDreamSaving.hbxODMyDream.setEnabled(false);
    } else {
        frmOpenAcDreamSaving.hbxODMnthSavAmt.setEnabled(true);
        frmOpenAcDreamSaving.hbxODMyDream.setEnabled(true);
    }
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = "";
    frmOpenAcDreamSaving.txtODMyDream.text = "";
}
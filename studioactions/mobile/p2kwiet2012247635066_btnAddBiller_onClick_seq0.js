function p2kwiet2012247635066_btnAddBiller_onClick_seq0(eventobject, context) {
    function alert_onClick_71708201224762741_True() {}

    function alert_onClick_9444201224766567_True() {}
    if ((gblMyBillerTopUpBB == 2)) {
        TMBUtil.DestroyForm(frmAddTopUpToMB);
        //if(flowSpa)
        //{
        //frmAddTopUpToMBGlobals();
        //}
        onSelectSugBillerBB.call(this);
        showAccountListBBMB.call(this);
    } else {
        if ((gblBlockBillerTopUpAdd)) {
            kony.print("Adding biller is disabled");
        } else {
            //#ifdef j2me
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef bb
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef bb10
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef winphone8
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef palm
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef android
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef iphone
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef winmobile
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef symbian
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef winmobile6x
            //#define preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            //#endif
            //#ifdef preprocessdecision_onClick_99707201224768265_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
            if ((kony.string.equals(gblTranxLockedForBiller, "03"))) {
                //alert(kony.i18n.getLocalizedString("keyTranxPwdLocked"))
                popTransferConfirmOTPLock.show();
            } else {
                if ((checkMaxBillerCountMB())) {
                    onclickOfSuggestedBillersAdd.call(this);
                } else {
                    function alert_onClick_9444201224766567_Callback() {
                        alert_onClick_9444201224766567_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Ok",
                        "noLabel": "No",
                        "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                        "alertHandler": alert_onClick_9444201224766567_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                }
            }
            //#endif
            //#ifdef spaip
            //#define preprocessdecision_onClick_16352201224765948_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
            //#endif
            //#ifdef spabbnth
            //#define preprocessdecision_onClick_16352201224765948_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
            //#endif
            //#ifdef spawinphone8
            //#define preprocessdecision_onClick_16352201224765948_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
            //#endif
            //#ifdef wap
            //#define preprocessdecision_onClick_16352201224765948_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
            //#endif
            //#ifdef spaan
            //#define preprocessdecision_onClick_16352201224765948_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
            //#endif
            //#ifdef spabb
            //#define preprocessdecision_onClick_16352201224765948_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
            //#endif
            //#ifdef spawindows
            //#define preprocessdecision_onClick_16352201224765948_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
            //#endif
            //#ifdef preprocessdecision_onClick_16352201224765948_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
            if ((kony.string.equals(gblIBFlowStatus, "04"))) {
                alert(kony.i18n.getLocalizedString("Receipent_OTPLocked"))
            } else {
                if ((checkMaxBillerCountMB())) {
                    onclickOfSuggestedBillersAdd.call(this);
                } else {
                    function alert_onClick_71708201224762741_Callback() {
                        alert_onClick_71708201224762741_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Ok",
                        "noLabel": "No",
                        "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                        "alertHandler": alert_onClick_71708201224762741_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                }
            }
            //#endif
        }
    }
}
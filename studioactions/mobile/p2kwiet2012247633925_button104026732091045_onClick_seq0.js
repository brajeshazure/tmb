function p2kwiet2012247633925_button104026732091045_onClick_seq0(eventobject) {
    if (gblIBFlowStatus == "04") {
        popTransferConfirmOTPLock.show();
        kony.print("IB_USER_STATUS_ID is 04");
        return false;
    } else {
        chngUseridspa = true
        if (spaCaptchaisVisible) {
            frmCMChgAccessPin.hboxCaptcha.setVisibility(true);
            frmCMChgAccessPin.hboxCaptchaText.setVisibility(true);
        } else {
            frmCMChgAccessPin.hboxCaptcha.setVisibility(false);
            frmCMChgAccessPin.hboxCaptchaText.setVisibility(false);
        }
    }
    frmCMChgAccessPin.show();
}
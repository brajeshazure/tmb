function p2kwiet2012247637118_btnOnDate_onClick_seq0(eventobject) {
    frmScheduleBillPayEditFuture.hboxUntil.setVisibility(true);
    frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
    frmScheduleBillPayEditFuture.hboxtimes.setVisibility(false);
    frmScheduleBillPayEditFuture.lblNumberOfTimes.setVisibility(false);
    frmScheduleBillPayEditFuture.linetimes.setVisibility(false);
    endingScheduleFrequencyMB.call(this, eventobject);
}
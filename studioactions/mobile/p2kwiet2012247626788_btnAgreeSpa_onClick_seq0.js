function p2kwiet2012247626788_btnAgreeSpa_onClick_seq0(eventobject) {
    if ((checkMaxBillerCountOnConfirmMB())) {
        onMytopUpConfirm.call(this);
    } else {
        var segData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
        var listData = myTopupListMB;
        var maxBillAllowed = parseInt(GLOBAL_MAX_BILL_COUNT);
        var TotalBillerCanBeAdded = maxBillAllowed - listData.length;
        alert(kony.i18n.getLocalizedString("Valid_CantAddMore") + TotalBillerCanBeAdded + kony.i18n.getLocalizedString("keyBillersIB"));
    }
}
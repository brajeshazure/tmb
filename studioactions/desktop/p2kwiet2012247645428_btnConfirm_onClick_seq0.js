function p2kwiet2012247645428_btnConfirm_onClick_seq0(eventobject) {
    if ((checkMaxBillerCountOnConfirmTopupIB())) {
        mapSegTopUpComplete.call(this);
        verifyOTPBillTopUp.call(this);
    } else {
        var BillerList = myTopupList;
        var ConfirmationList = frmIBMyTopUpsHome.segBillersConfirm.data;
        var maxBillAllowed = parseInt(GLOBAL_MAX_BILL_COUNT);
        var TotalBillerCanBeAdded = maxBillAllowed - BillerList.length;
        alert(kony.i18n.getLocalizedString("Valid_CantAddMore") + TotalBillerCanBeAdded + kony.i18n.getLocalizedString("keyBillersIB"));
    }
}
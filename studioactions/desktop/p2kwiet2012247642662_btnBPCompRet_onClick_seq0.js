function p2kwiet2012247642662_btnBPCompRet_onClick_seq0(eventobject) {
    if (frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
        frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
    }
    if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
        //frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
        frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
    }
    if (!frmIBBillPaymentView.hbxBPVeiwAmnt.isVisible) {
        frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(true);
    }
    if (frmIBBillPaymentView.lblBPeditHrd.isVisible) {
        frmIBBillPaymentView.lblBPeditHrd.setVisibility(false);
    }
    if (!frmIBBillPaymentView.lblBPViewHrd.isVisible) {
        frmIBBillPaymentView.lblBPViewHrd.setVisibility(true);
    }
    if (!frmIBBillPaymentView.hbxBPViewHdr.isVisible) {
        frmIBBillPaymentView.hbxBPViewHdr.setVisibility(true);
    }
    if (frmIBBillPaymentView.btnBPSchedule.isVisible) {
        frmIBBillPaymentView.btnBPSchedule.setVisibility(false);
    }
    if (!frmIBBillPaymentView.hbxbtnReturn.isVisible) {
        frmIBBillPaymentView.hbxbtnReturn.setVisibility(true);
    }
    if (frmIBBillPaymentView.hbxEditBtns.isVisible) {
        frmIBBillPaymentView.hbxEditBtns.setVisibility(false);
    }
    if (frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
        frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
    }
    if (frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
        frmIBBillPaymentView.hbxOnlinePayment.setVisibility(false);
    }
    if (frmIBBillPaymentView.hboxOnlinePayPenalty.isVisible) {
        frmIBBillPaymentView.hboxOnlinePayPenalty.setVisibility(false);
    }
    gsFormId = frmIBMyActivities;
    if (gblActivitiesNvgn == "F") {
        invokeFutureInstructionService();
        /*
        frmIBMyActivities.show();
        if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
        showCalendar(gsSelectedDate,frmIBMyActivities,1);*/
        IBMyActivitiesReloadAndShowCalendar();
    } else if (gblActivitiesNvgn == "C") {
        kony.print("Calendar");
        /*frmIBMyActivities.show();
        if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
        showCalendar(gsSelectedDate,frmIBMyActivities,1);*/
        IBMyActivitiesReloadAndShowCalendar();
    }
    frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
}
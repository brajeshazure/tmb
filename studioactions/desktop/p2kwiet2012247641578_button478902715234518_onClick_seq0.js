function p2kwiet2012247641578_button478902715234518_onClick_seq0(eventobject) {
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    IBProfileUpdateOTPRequestService();
    frmIBCMEditMyProfile.hbxTokenEntry.setVisibility(false);
    frmIBCMEditMyProfile.hbxOTPEntry.setVisibility(true);
    frmIBCMEditMyProfile.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBCMEditMyProfile.txtBxOTP.setFocus(true);
}
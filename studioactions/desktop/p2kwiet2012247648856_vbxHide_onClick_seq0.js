function p2kwiet2012247648856_vbxHide_onClick_seq0(eventobject) {
    if (frmIBTopUpExecutedTxn.lblBBPaymentValue.isVisible) {
        frmIBTopUpExecutedTxn.lblBBPaymentValue.setVisibility(false);
        frmIBTopUpExecutedTxn.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBTopUpExecutedTxn.lblBBPaymentValue.setVisibility(true);
        frmIBTopUpExecutedTxn.lblHide.text = kony.i18n.getLocalizedString("Hide");
    }
}
function p2kwiet2012247645184_frmIBMyReceipentsAddContactManuallyConf_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    setDatatoReceipentsList.call(this, "8");
    pagespecificSubMenu.call(this);
    doRcCacheRefresh.call(this);
    if (gblRC_QA_TEST_VAL == 0) {
        setToAddDatatoManualConfirmationSeg();
    }
    campaginService.call(this, "imgAddRecManually", "frmIBMyReceipentsAddContactManuallyConf", "I");
}
function p2kwiet2012247643574_button445417506939912_onClick_seq0(eventobject) {
    frmIBFTrnsrView.lblViewStartOnDateVal.text = formatDateFT(gblFTViewStartOnDate);
    frmIBFTrnsrView.lblRepeatAsVal.text = gblFTViewRepeatAsVal
    frmIBFTrnsrView.lblRepeatAsValView.text = gblFTViewRepeatAsVal;
    var tmpEndDate = gblFTViewEndDate;
    if (gblFTViewEndDate == "-") {
        frmIBFTrnsrView.lblEndOnDateVal.text = "-";
    } else {
        if (tmpEndDate.indexOf("-", 0) != -1) {
            tmpEndDate = formatDateFT(gblFTViewEndDate);
        } else frmIBFTrnsrView.lblEndOnDateVal.text = tmpEndDate;
    }
    frmIBFTrnsrView.lblExcuteVal.text = gblFTViewExcuteVal
    frmIBFTrnsrView.lblRemainingVal.text = gblFTViewExcuteremaing + " )"
    gblIsEditAftr = gblIsEditAftrFrmService;
    gblIsEditOnDate = gblIsEditOnDateFrmService;
    gblScheduleRepeatFT = gblFTViewRepeatAsVal;
    gblScheduleEndFT = gblFTViewEndVal;
    gblEditFTSchdule = false;
    frmIBFTrnsrView.lblFeeVal.text = gblFTFeeView;
    if (gblViewRepeatAsLS == "Daily") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyDaily");
    } else if (gblViewRepeatAsLS == "Weekly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyWeekly");
    } else if (gblViewRepeatAsLS == "Monthly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyMonthly");
    } else if (gblViewRepeatAsLS == "Yearly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyYearly");
    } else frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyOnce");
    gblRepeatAsforLS = gblViewRepeatAsLS;
    frmIBFTrnsrView.lblToAccntNickName.text = gblToAccntNick;
    frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(false);
    frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(true);
    frmIBFTrnsrView.lblFTEditHdr.setVisibility(false);
    frmIBFTrnsrView.btnFTEdit.setVisibility(false);
    frmIBFTrnsrView.hbxFTViewHdr.setVisibility(true);
    //frmIBFTrnsrView.hbxbtnReturn.setVisibility(true);
    frmIBFTrnsrView.btnReturnView.setVisibility(true)
    frmIBFTrnsrView.hbxEditBtns.setVisibility(false);
    frmIBFTrnsrView.hbxFTAfterEditAvilBal.setVisibility(false);
    frmIBFTrnsrView.hbxEditFT.setVisibility(false);
    //New
    frmIBFTrnsrView.hbxTrnsfrDetails2.setVisibility(false);
    frmIBFTrnsrView.hbxTrnsfrDetails2ForView.setVisibility(true);
    frmIBFTrnsrView.btnFTEditFlow.setVisibility(true);
    frmIBFTrnsrView.btnFTDel.setVisibility(true);
    frmIBFTrnsrView.show();
}
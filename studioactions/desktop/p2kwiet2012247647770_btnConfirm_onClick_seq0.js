function p2kwiet2012247647770_btnConfirm_onClick_seq0(eventobject) {
    frmIBSSSDetails.hbxViewTransferToNoFix.setVisibility(true);
    frmIBSSSDetails.hbxViewTrnFrmNoFxd.setVisibility(true);
    frmIBSSSDetails.hbxEditSSSFrom.setVisibility(false);
    frmIBSSSDetails.hbxEditSSSTo.setVisibility(false);
    frmIBSSSDetails.hbxEditCCBtns.setVisibility(false);
    frmIBSSSDetails.btnViewBack.setVisibility(true);
    //frmIBSSSDetails.lblAmntLmtMax.text = frmIBSSSDetails.txtAmntMax.text;
    //frmIBSSSDetails.lblAmntLmtMin.text = frmIBSSSDetails.txtAmntMin.text;
    var maxAmount = frmIBSSSDetails.lblAmntLmtMax.text;
    var minAmount = frmIBSSSDetails.lblAmntLmtMin.text;
    var lmtValid = validAmntLmtIB(maxAmount, minAmount);
    frmIBSSApplyCnfrmtn.hbxToken.setVisibility(false);
    frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(false);
    if (lmtValid) {
        gblSSServieHours = "modifyS2S";
        checkBusinessHoursForIBS2SEditDel();
        // Below commented code is only to test UI
        // frmSSServiceED.lblAmntLmtMax.text = frmSSService.txtBalMax.text;//+"\u0E3F";
        // frmSSServiceED.lblAmntLmtMin.text = frmSSService.txtBalMin.text;
        // frmSSServiceED.show();
    }
}
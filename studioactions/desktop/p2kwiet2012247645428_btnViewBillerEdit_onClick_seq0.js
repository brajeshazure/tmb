function p2kwiet2012247645428_btnViewBillerEdit_onClick_seq0(eventobject) {
    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxOtpBox.setVisibility(false)
    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(true);
    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyTopUpsHome.txtEditBillerNickName.skin = txtBoxTransParentGrey;
    frmIBMyTopUpsHome.txtEditBillerNickName.text = frmIBMyTopUpsHome.lblViewBillerName.text;
    frmIBMyTopUpsHome.txtEditBillerNickName.setFocus(true)
}
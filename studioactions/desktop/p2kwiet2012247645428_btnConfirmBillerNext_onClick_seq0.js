function p2kwiet2012247645428_btnConfirmBillerNext_onClick_seq0(eventobject) {
    frmIBMyTopUpsHome.txtotp.text = "";
    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(true);
    frmIBMyTopUpsHome.hbxOtpBox.setVisibility(true)
    frmIBMyTopUpsHome.hbxCanConfBtnContainer.setVisibility(true);
    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyTopUpsHome.btnConfirmBillerNext.setVisibility(false);
    onAddBillTopUpNextIB.call(this);
    frmIBMyTopUpsHome.txtotp.setFocus(true);
}
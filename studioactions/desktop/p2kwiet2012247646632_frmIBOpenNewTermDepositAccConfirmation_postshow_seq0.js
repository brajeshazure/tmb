function p2kwiet2012247646632_frmIBOpenNewTermDepositAccConfirmation_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    segConvenientServicesLoad.call(this);
    pagespecificSubMenu.call(this);
    var stringstr = frmIBOpenNewTermDepositAccConfirmation.lblOATDAmtVal.text;
    if (stringstr == null || stringstr == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyTargetAmt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    var dotcount = stringstr.split(".");
    var lendot = dotcount.length - 1;
    if (lendot > 1) {
        showAlertIB(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    // var decimal= dotcout[1];
    var stringstr = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var name = frmIBOpenNewTermDepositAccConfirmation.lblOATDAmtVal.text;
    var charExists;
    if (charExists = (name.indexOf("à¸¿") >= 0)) {} else {
        frmIBOpenNewTermDepositAccConfirmation.lblOATDAmtVal.text = commaFormattedOpenAct(stringstr) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    //frmIBOpenNewTermDepositAccConfirmation.lblInterestVal.text=frmIBOpenNewTermDepositAccConfirmation.lblInterestVal.text+" %"
    addNumberCheckListner.call(this, "txtOTP");
    addNumberCheckListner.call(this, "tbxToken");
}
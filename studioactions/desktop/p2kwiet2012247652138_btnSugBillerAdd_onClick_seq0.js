function p2kwiet2012247652138_btnSugBillerAdd_onClick_seq0(eventobject, context) {
    function alert_onClick_34458201224766682_True() {}

    function alert_onClick_7233201224765763_True() {}
    if ((kony.string.equals(gblOTPLockedForBiller, "04"))) {
        alert(kony.i18n.getLocalizedString("ECVrfyOTPErr"))
    } else {
        if ((checkIfBarcodeOnlyIBBiller())) {
            if ((checkMaxBillerCount())) {
                frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
                frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
                frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
                frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
                frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
                frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
                frmIBMyBillersHome.txtAddBillerNickName.setEnabled(true);
                frmIBMyBillersHome.txtAddBillerRef1.setEnabled(true);
                loadAddBillerValues.call(this);
            } else {
                function alert_onClick_34458201224766682_Callback() {
                    alert_onClick_34458201224766682_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertHandler": alert_onClick_34458201224766682_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        } else {
            function alert_onClick_7233201224765763_Callback() {
                alert_onClick_7233201224765763_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": "Ok",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("Valid_BarcodeOnly"),
                "alertHandler": alert_onClick_7233201224765763_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
        }
    }
}
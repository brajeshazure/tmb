function p2kwiet2012247651202_btnThaiE8_onClick_seq0(eventobject) {
    langchange.call(this, "th_TH");
    hbxiE8.btnEngE8.skin = "btnEngLangOff";
    hbxiE8.btnThaiE8.skin = "btnThaiLangOn";
    hbxiE8.btnEngE8.skin = "btnEngLangOff";
    hbxiE8.btnThaiE8.skin = "btnThaiLangOn";
    if (!kony.appinit.isIE8) {
        frmIBPreLogin.txtUserId.placeholder = kony.i18n.getLocalizedString("keyUserIDPreLogin");
        frmIBPreLogin.txtPassword.placeholder = kony.i18n.getLocalizedString("keyPasswordPlaceHolder");
    }
    hbxiE8.lnkChrome.text = kony.i18n.getLocalizedString("Chrome");
    hbxiE8.lnkSafari.text = kony.i18n.getLocalizedString("Safari");
    hbxiE8.lnkMozilla.text = kony.i18n.getLocalizedString("Mozilla");
    hbxiE8.lnkE.text = kony.i18n.getLocalizedString("Internet");
    hbxClose.btnClose.text = kony.i18n.getLocalizedString("Close");
    hbxiE8.lblDesc.text = kony.i18n.getLocalizedString("ie8Desc");
}
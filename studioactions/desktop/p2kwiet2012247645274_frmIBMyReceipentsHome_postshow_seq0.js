function p2kwiet2012247645274_frmIBMyReceipentsHome_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    setDatatoReceipentsList.call(this, "1");
    pagespecificSubMenu.call(this);
    doRcCacheRefresh.call(this);
    startInquiryCustomerAccountsService.call(this);
    destroyAllForms();
    //Reset state
    Add_Receipent_State = 0;
    if (navigator.userAgent.match(/iPad/i)) {
        frmIBMyReceipentsHome.lblMyReceipents.setFocus(true);
    } else {
        frmIBMyReceipentsHome.tbxSearch.setFocus(true);
    }
}
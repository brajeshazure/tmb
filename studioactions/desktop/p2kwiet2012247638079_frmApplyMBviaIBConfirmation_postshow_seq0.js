function p2kwiet2012247638079_frmApplyMBviaIBConfirmation_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    handleMessageConfPreShow.call(this);
    if ((gblMode == 0)) {
        segConvenientServicesLoad.call(this);
    } else {
        segAboutMeLoad.call(this);
    }
    pagespecificSubMenu.call(this);
    campaginService.call(this, 'imgAdv', 'frmApplyMBviaIBConfirmation', 'I');
}
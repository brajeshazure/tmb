function p2kwiet2012247649549_btnHide_onClick_seq0(eventobject) {
    if (frmIBTransferNowCompletion.lblBal.isVisible) {
        frmIBTransferNowCompletion.lblBal.setVisibility(false);
        frmIBTransferNowCompletion.lblHide.text = "Unhide";
    } else {
        frmIBTransferNowCompletion.lblBal.setVisibility(true);
        frmIBTransferNowCompletion.lblHide.text = "Hide";
    }
}
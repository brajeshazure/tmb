function p2kwiet2012247642265_frmIBDateSlider_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBDateSlider.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocusThai";
    } else {
        frmIBDateSlider.btnMenuMyActivities.skin = "btnIBMenuMyActivitiesFocus";
    }
    document.getElementById("frmIBDateSlider_vbxcombo2").className = " kcell kwt28 ";
    document.getElementById("frmIBDateSlider_vbxcombo3").className = " ktable kwt100 ";
    document.getElementById("frmIBDateSlider_vboxCalDayView").className = " ktable kwt94 ";
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
        if (isIE8) {
            frmIBDateSlider.vbox588717954792719.containerWeight = 14;
        }
    }
    columnHeaderInDGHistory.call(this);
    getMonthCycleIB.call(this);
}
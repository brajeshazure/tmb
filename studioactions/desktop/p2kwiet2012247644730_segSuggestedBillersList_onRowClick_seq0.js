function p2kwiet2012247644730_segSuggestedBillersList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    function alert_onRowClick_45997201224765551_True() {}

    function alert_onRowClick_81593201224763580_True() {}
    if ((kony.string.equals(gblOTPLockedForBiller, "04"))) {
        alert(kony.i18n.getLocalizedString("ECVrfyOTPErr"))
    } else {
        if ((checkIfBarcodeOnlyIBBiller())) {
            if ((checkMaxBillerCount())) {
                gblAddBillerButtonClicked = false;
                frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
                frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
                frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
                frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
                frmIBMyBillersHome.txtAddBillerNickName.text = "";
                frmIBMyBillersHome.txtAddBillerRef1.text = "";
                frmIBMyBillersHome.txtAddBillerRef2.text = "";
                frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
                frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
                frmIBMyBillersHome.txtAddBillerNickName.setEnabled(true);
                frmIBMyBillersHome.txtAddBillerRef1.setEnabled(true);
                loadAddBillerValues.call(this);
            } else {
                function alert_onRowClick_45997201224765551_Callback() {
                    alert_onRowClick_45997201224765551_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertHandler": alert_onRowClick_45997201224765551_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        } else {
            function alert_onRowClick_81593201224763580_Callback() {
                alert_onRowClick_81593201224763580_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": "Ok",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("Valid_BarcodeOnly"),
                "alertHandler": alert_onRowClick_81593201224763580_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
        }
    }
}
function p2kwiet2012247648059_txtCaptchaText_onDone_seq0(eventobject, changedtext) {
    function alert_onDone_36207201224764579_True() {}

    function alert_onDone_21596201224765745_True() {}

    function alert_onDone_73054201224769349_True() {}
    if ((frmIBPreLogin.txtUserId.text != "")) {
        if ((frmIBPreLogin.txtPassword.text != "")) {
            if ((frmIBPreLogin.hboxCaptchaText.isVisible)) {
                if ((frmIBPreLogin.txtCaptchaText.text != "")) {
                    captchaValidation.call(this, frmIBPreLogin.txtCaptchaText.text);
                } else {
                    function alert_onDone_36207201224764579_Callback() {
                        alert_onDone_36207201224764579_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": "Field is mandatory",
                        "alertHandler": alert_onDone_36207201224764579_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                    frmIBPreLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            function alert_onDone_21596201224765745_Callback() {
                alert_onDone_21596201224765745_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "No",
                "message": "Field is mandatory",
                "alertHandler": alert_onDone_21596201224765745_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBPreLogin.txtPassword.setFocus(true);
        }
    } else {
        function alert_onDone_73054201224769349_Callback() {
            alert_onDone_73054201224769349_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": "Field is mandatory",
            "alertHandler": alert_onDone_73054201224769349_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBPreLogin.txtUserId.setFocus(true);
    }
}
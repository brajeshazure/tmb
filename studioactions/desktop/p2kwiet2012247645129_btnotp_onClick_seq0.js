function p2kwiet2012247645129_btnotp_onClick_seq0(eventobject) {
    frmIBMyReceipentsAddContactManually.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBMyReceipentsAddContactManually.lblPlsReEnter.text = " ";
    frmIBMyReceipentsAddContactManually.hbxOTPincurrect.isVisible = false;
    frmIBMyReceipentsAddContactManually.hbox101089555968194.isVisible = true;
    frmIBMyReceipentsAddContactManually.hbox101089555968200.isVisible = true;
    if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
        frmIBMyReceipentsAddContactManually.otpmsglbl.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBMyReceipentsAddContactManually.lblOTP.text = "OTP:";
        frmIBMyReceipentsAddContactManually.btnotp.text = kony.i18n.getLocalizedString("keyRequest");
        frmIBMyReceipentsAddContactManually.txtotp.text = "";
        // frmIBMyReceipentsAddContactManually.txtotp.placeholder=kony.i18n.getLocalizedString("enterOTP");
        gblTokenSwitchFlag = false;
        gblSwitchToken = true;
        startBackGroundOTPRequestService();
        frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
    } else {
        startOTPRequestService();
        frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
    }
}
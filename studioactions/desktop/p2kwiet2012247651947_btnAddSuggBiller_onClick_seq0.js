function p2kwiet2012247651947_btnAddSuggBiller_onClick_seq0(eventobject, context) {
    function alert_onClick_2257201224768047_True() {}
    if ((checkMaxBillerCount())) {
        gblAddBillerFromPay = true;
        gblSuggestedBiller = false;
        frmIBMyTopUpsHome.show()
    } else {
        function alert_onClick_2257201224768047_Callback() {
            alert_onClick_2257201224768047_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
            "alertHandler": alert_onClick_2257201224768047_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
}
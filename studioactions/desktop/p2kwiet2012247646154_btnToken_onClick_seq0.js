function p2kwiet2012247646154_btnToken_onClick_seq0(eventobject) {
    frmIBOpenNewSavingsAccConfirmation.hbxToken.setVisibility(false);
    frmIBOpenNewSavingsAccConfirmation.hbxTokenMessage.setVisibility(false);
    frmIBOpenNewSavingsAccConfirmation.hbox866854059250283.setVisibility(true);
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    frmIBOpenNewSavingsAccConfirmation.hbox866854059301598.setVisibility(true);
    frmIBOpenNewSavingsAccConfirmation.hbox866854059301638.setVisibility(true);
    frmIBOpenNewSavingsAccConfirmation.hbox866854059301656.setVisibility(true);
    IBreqOTPMYNewAccnts();
    frmIBOpenNewSavingsAccConfirmation.txtOTP.setFocus(true);
}
function p2kwiet2012247640409_btnBPConfirm_onClick_seq0(eventobject) {
    populateEditBillPaymentConfirmScreen.call(this);
    frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
    frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
    frmIBEditFutureBillPaymentPrecnf.hbxBPCmpletngHdr.setVisibility(false);
    frmIBEditFutureBillPaymentPrecnf.imgComplete.setVisibility(false);
    frmIBEditFutureBillPaymentPrecnf.hbxbtnUpdatedReturn.setVisibility(false);
    //frmIBEditFutureBillPaymentPrecnf.hbxBPCmpleAvilBal.setVisibility(false);
    frmIBEditFutureBillPaymentPrecnf.hbxBPSave.setVisibility(false);
    frmIBEditFutureBillPaymentPrecnf.hbxBPViewHdr.setVisibility(true);
    frmIBEditFutureBillPaymentPrecnf.hbxOTP.setVisibility(true);
    frmIBEditFutureBillPaymentPrecnf.hbxEditBtns.setVisibility(true);
    frmIBBillPaymentView.arrowBPScheduleField.setVisibility(false);
    //frmIBEditFutureBillPaymentPrecnf.show();
}
function p2kwiet2012247647721_frmIBSSApplyCnfrmtn_preshow_seq0(eventobject, neworientation) {
    frmIBSSApplyCnfrmtn.hbxDecisionOTP.setVisibility(false);
    frmIBSSApplyCnfrmtn.hbxOTP.setVisibility(false);
    frmIBSSApplyCnfrmtn.hbxCancelConf.setVisibility(true)
    frmIBSSApplyCnfrmtn.hbxToken.setVisibility(false);
    frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(false);
    if (gblSSServieHours == "S2SEdit" || gblSSServieHours == "dummyABX") {
        kony.print("inside s2sEdit... ");
        gblSSServieHours = "dummyABX";
        getTokenStatus();
    }
    kony.print("tokenValueForS2S... " + tokenValueForS2S);
    if (tokenValueForS2S != null) {
        kony.print("tokenValueForS2S not null...");
        if (tokenValueForS2S == "N") {
            kony.print("tokenValueForS2S value N..");
            gblOTPFlag = true;
            tokenFlagForS2S = false;
            kony.print("gblSSServieHours:" + gblSSServieHours);
            kony.print("tokenFlagForS2S:" + tokenFlagForS2S + "gblOTPFlag:" + gblOTPFlag);
            frmIBSSApplyCnfrmtn.hboxBankRef.setVisibility(true);
            frmIBSSApplyCnfrmtn.hbxOTPsnt.setVisibility(true);
            dismissLoadingScreenPopup();
        } else {
            kony.print("else tokenValueForS2S value Y..");
            gblOTPFlag = false;
            tokenFlagForS2S = true;
            kony.print("gblSSServieHours:" + gblSSServieHours);
            kony.print("tokenFlagForS2S:" + tokenFlagForS2S + "gblOTPFlag:" + gblOTPFlag);
            frmIBSSApplyCnfrmtn.hboxBankRef.setVisibility(false);
            frmIBSSApplyCnfrmtn.hbxOTPsnt.setVisibility(false);
            //frmIBSSApplyCnfrmtn.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("S2S_tokenId");
            //frmIBSSApplyCnfrmtn.lblOTPKey.text = kony.i18n.getLocalizedString("S2S_Token");
            //frmIBSSApplyCnfrmtn.btnOTPReq.text = kony.i18n.getLocalizedString("S2S_SwitchtoOTP");
            dismissLoadingScreenPopup();
        }
    } else {
        kony.print("else tokenValueForS2S Null..");
        gblOTPFlag = true;
        tokenFlagForS2S = false;
        kony.print("tokenFlagForS2S:" + tokenFlagForS2S + "gblOTPFlag:" + gblOTPFlag);
        kony.print("gblSSServieHours:" + gblSSServieHours);
        frmIBSSApplyCnfrmtn.hboxBankRef.setVisibility(true);
        frmIBSSApplyCnfrmtn.hbxOTPsnt.setVisibility(true);
        //frmIBSSApplyCnfrmtn.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        //frmIBSSApplyCnfrmtn.lblOTPKey.text = kony.i18n.getLocalizedString("keyOTP");
        //frmIBSSApplyCnfrmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        dismissLoadingScreenPopup();
    }
    /* End of preshow of frmIBSSApplyCnfrmtn */
}
function p2kwiet2012247645428_frmIBMyTopUpsHome_preshow_seq0(eventobject, neworientation) {
    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(true);
    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
    frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyTopUpsHome.txtotp.maxTextLength = 6;
    frmIBMyTopUpsHome.lblNoBills.setVisibility(false);
    frmIBMyTopUpsHome.lblErrorMsg.setVisibility(false);
    frmIBMyTopUpsHome.hbxTokenEntry.setVisibility(false);
    gblRetryCountRequestOTP = 0;
    frmIBMyTopUpsHome.lblMyBills.margin = [0, 0, 0, 0];
    getLocaleBillerIBMyTopUp.call(this);
}
function p2kwiet2012247645129_segmentReceipentListing_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    //Store the selected Rc id
    gblselectedRcId = frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["receipentID"];
    if (checkActualRcSegItemIndex()) {
        //Index done in method
    } else {
        globalSeletedRcIndex = frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedIndex[1];
    }
    showLoadingScreenPopup();
    frmIBMyReceipentsAccounts.show();
    frmIBMyReceipentsAccounts.hboxMyReceipents.setVisibility(true);
    frmIBMyReceipentsAccounts.hboxReceipentAccount.setVisibility(false);
    //frmIBMyReceipentsAccounts.hboxTMBWelcome.setVisibility(false);
    //Code to set selected item name
    if (globalRcData[0][globalSeletedRcIndex].lblReceipentName == null) {
        frmIBMyReceipentsAccounts.lblRcName.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcName.text = globalRcData[0][globalSeletedRcIndex].lblReceipentName;
    }
    if (frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["mobileNumber"] == "undefined" || frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["mobileNumber"] == undefined || frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["mobileNumber"] == null || frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["mobileNumber"] == "No Number") {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["mobileNumber"];
    }
    if (frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["emailId"] == null || frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["emailId"] == "No Mail") {
        frmIBMyReceipentsAccounts.lblRcEmail.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcEmail.text = frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["emailId"];
    }
    if (frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["facebookId"] == null) {
        frmIBMyReceipentsAccounts.lblRcFbId.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcFbId.text = frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["facebookId"];
    }
    if (frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["imgReceipentPic"] == null) {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = "nouserimg.jpg";
    } else {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = frmIBMyReceipentsAddContactManually.segmentReceipentListing.selectedItems[0]["imgReceipentPic"];
    }
}
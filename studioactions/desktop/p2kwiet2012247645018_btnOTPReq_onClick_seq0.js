function p2kwiet2012247645018_btnOTPReq_onClick_seq0(eventobject) {
    frmIBMyReceipentsAddContactFB.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBMyReceipentsAddContactFB.lblPlsReEnter.text = " ";
    frmIBMyReceipentsAddContactFB.hbxOTPincurrect.isVisible = false;
    frmIBMyReceipentsAddContactFB.hbox476047582127699.isVisible = true;
    frmIBMyReceipentsAddContactFB.hbxOTPsnt.isVisible = true;
    if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
        frmIBMyReceipentsAddContactFB.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBMyReceipentsAddContactFB.label476047582115284.text = "OTP:";
        frmIBMyReceipentsAddContactFB.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        frmIBMyReceipentsAddContactFB.textbox247428873338513.text = "";
        frmIBMyReceipentsAddContactFB.textbox247428873338513.placeholder = kony.i18n.getLocalizedString("enterOTP");
        gblTokenSwitchFlag = false;
        gblSwitchToken = true;
        startBackGroundOTPRequestService();
    } else {
        startOTPRequestService();
    }
}
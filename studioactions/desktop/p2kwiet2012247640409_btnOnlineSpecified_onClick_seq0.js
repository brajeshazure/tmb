function p2kwiet2012247640409_btnOnlineSpecified_onClick_seq0(eventobject) {
    frmIBBillPaymentView.btnOnlineSpecified.skin = btnIBgreyActive;
    frmIBBillPaymentView.btnOnlineFull.skin = btnIBgreyInactive;
    frmIBBillPaymentView.btnOnlineMin.skin = btnIBgreyInactive;
    frmIBBillPaymentView.textboxOnlineAmtvalue.setEnabled(true);
    frmIBBillPaymentView.textboxOnlineAmtvalue.text = frmIBBillPaymentView.lblBPAmtValue.text;
}
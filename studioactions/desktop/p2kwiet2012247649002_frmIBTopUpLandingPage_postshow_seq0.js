function p2kwiet2012247649002_frmIBTopUpLandingPage_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    if (kony.i18n.getCurrentLocale() != "th_TH") frmIBTopUpLandingPage.btnMenuTopUp.skin = "btnIBMenuTopUpFocus";
    else frmIBTopUpLandingPage.btnMenuTopUp.skin = "btnIBMenuTopUpFocusThai";
    //frmIBTopUpLandingPage.CalendarXferDate.dateEditable=false;
    //frmIBTopUpLandingPage.calendarXferUntil.dateEditable=false;
    //ticket #28094
    try {
        document.getElementById("CalendarXferDate").setAttribute('readonly', "readonly");
        document.getElementById("calendarXferUntil").setAttribute('readonly', "readonly");
    } catch (e) {}
    addNumberCheckListner.call(this, "txtAmount");
}
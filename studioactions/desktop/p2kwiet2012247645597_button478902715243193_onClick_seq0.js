function p2kwiet2012247645597_button478902715243193_onClick_seq0(eventobject) {
    frmIBOpenAccountContactKYC.hbxTokenEntry.setVisibility(false);
    frmIBOpenAccountContactKYC.hbxOTPEntry.setVisibility(true);
    frmIBOpenAccountContactKYC.txtBxOTP.setFocus(true);
    frmIBOpenAccountContactKYC.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBOpenAccountContactKYC.txtBxOTP.setFocus(true);
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    if ((frmIBOpenAccountContactKYC.hbxcnf2.isVisible)) {
        IBProfileUpdateOTPRequestService.call(this);
    } else {
        IBNewMobNoOTPRequestService1.call(this);
    }
}
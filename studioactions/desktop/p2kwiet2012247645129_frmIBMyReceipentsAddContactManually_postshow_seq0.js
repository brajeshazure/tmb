function p2kwiet2012247645129_frmIBMyReceipentsAddContactManually_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    setDatatoReceipentsList.call(this, "4");
    pagespecificSubMenu.call(this);
    doRcCacheRefresh.call(this);
    frmIBMyReceipentsAddContactManually.hbxotpreconfrm.setVisibility(false)
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (!isIE8) {
        frmIBMyReceipentsAddContactManually.txtRecipientName.placeholder = kony.i18n.getLocalizedString("Receipent_ReceipentName");
    } else {
        frmIBMyReceipentsAddContactManually.txtRecipientName.placeholder = "";
    }
    frmIBMyReceipentsAddContactManually.txtRecipientName.setFocus(true);
}
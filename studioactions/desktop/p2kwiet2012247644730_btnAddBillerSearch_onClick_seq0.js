function p2kwiet2012247644730_btnAddBillerSearch_onClick_seq0(eventobject) {
    frmIBMyBillersHome.segBillersList.setVisibility(false);
    frmIBMyBillersHome.lblMyBills.setVisibility(false);
    frmIBMyBillersHome.hbxMore.setVisibility(false);
    frmIBMyBillersHome.segSuggestedBillersList.removeAll();
    frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
    isSelectBillerEnabled = true;
    gblTopUpMore = 0;
    gblCustTopUpMore = 0;
    getMyBillSuggestListIB.call(this);
}
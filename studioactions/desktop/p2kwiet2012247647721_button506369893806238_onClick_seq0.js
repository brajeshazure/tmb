function p2kwiet2012247647721_button506369893806238_onClick_seq0(eventobject) {
    frmIBSSApplyCnfrmtn.hbxToken.setVisibility(false);
    frmIBSSApplyCnfrmtn.keyEnterToken.setVisibility(false);
    frmIBSSApplyCnfrmtn.hbxOTP.setVisibility(true);
    frmIBSSApplyCnfrmtn.hbox50639980413174.setVisibility(false);
    startReqOTPApplyServicesS2SIB();
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    frmIBSSApplyCnfrmtn.txtBxOTP.setFocus(true);
}
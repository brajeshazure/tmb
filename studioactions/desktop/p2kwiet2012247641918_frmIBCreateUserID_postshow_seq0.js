function p2kwiet2012247641918_frmIBCreateUserID_postshow_seq0(eventobject, neworientation) {
    document.getElementById("frmIBCreateUserID_hbxPasswdStrength").style.width = "320px";
    if (gblSetPwd) {
        frmIBCreateUserID.label50285458139.setVisibility(false);
        frmIBCreateUserID.hbxUserId.setVisibility(false);
    } else {
        frmIBCreateUserID.label50285458139.setVisibility(true);
        frmIBCreateUserID.hbxUserId.setVisibility(true);
    }
}
function p2kwiet2012247644730_btnEditBillerCancel_onClick_seq0(eventobject) {
    frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
    frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyBillersHome.hbxOtpBox.setVisibility(false)
    frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(true);
}
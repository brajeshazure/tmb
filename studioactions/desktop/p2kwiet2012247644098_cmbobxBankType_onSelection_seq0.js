function p2kwiet2012247644098_cmbobxBankType_onSelection_seq0(eventobject) {
    srvGetBankDetailIB.call(this, frmIBMyAccnts.cmbobxBankType.selectedKeyValue[0]);
    gblBankCD = frmIBMyAccnts.cmbobxBankType.selectedKeyValue[0]
    var banktype = frmIBMyAccnts.cmbobxBankType.selectedKeyValue[1];
    if (banktype == kony.i18n.getLocalizedString("keyTMBBank")) {
        frmIBMyAccnts.cmbobxAccntType.selectedKey = "key2";
        frmIBMyAccnts.cmbobxAccntType.setVisibility(true);
        frmIBMyAccnts.lineComboAccntype.setVisibility(true);
        frmIBMyAccnts.txtNickNAme.text = ""
        frmIBMyAccnts.txtbxOtherAccnt.text = ""
            //frmIBMyAccnts.hbxCreditCardNum.setVisibility(false);
            //frmIBMyAccnts.hbxAccntNum.setVisibility(true);
            //TMB Bank
    } else {
        frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
        frmIBMyAccnts.lineComboAccntype.setVisibility(false);
        frmIBMyAccnts.lblsuffix.setVisibility(false);
        frmIBMyAccnts.txtbxSuffix.setVisibility(false);
        frmIBMyAccnts.lblCreditCardNum.setVisibility(false);
        frmIBMyAccnts.hbxCreditCardNum.setVisibility(false);
        frmIBMyAccnts.lblAccntNum.setVisibility(true);
        frmIBMyAccnts.hbxAccntNum.setVisibility(false);
        frmIBMyAccnts.txtbxOtherAccnt.setVisibility(true);
        frmIBMyAccnts.txtbxOtherAccnt.text = "";
        frmIBMyAccnts.txtNickNAme.text = ""
    }
}
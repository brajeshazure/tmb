function p2kwiet2012247646755_button478902715234518_onClick_seq0(eventobject) {
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    IBProfileUpdateOTPRequestService();
    frmIBOpenProdMailAddressEdit.hbxTokenEntry.setVisibility(false);
    frmIBOpenProdMailAddressEdit.hbxOTPEntry.setVisibility(true);
    frmIBOpenProdMailAddressEdit.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBOpenProdMailAddressEdit.txtBxOTP.setFocus(true);
}
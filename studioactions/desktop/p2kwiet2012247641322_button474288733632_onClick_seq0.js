function p2kwiet2012247641322_button474288733632_onClick_seq0(eventobject) {
    editbuttonflag = "profile";
    showLoadingScreenPopup();
    editConfirm = false;
    gblAddressFlag = 1;
    gblMyProfileAddressFlag = "state";
    frmIBCMEditMyProfile.hbxRequest.setVisibility(false);
    frmIBCMEditMyProfile.arrowrequest.setVisibility(false);
    frmIBCMEditMyProfile.label475124774164.setVisibility(true);
    frmIBCMEditMyProfile.hboxEdit.setVisibility(true);
    frmIBCMEditMyProfile.hbxCancelSave.setVisibility(true);
    getIBEditProfileStatus.call(this);
}
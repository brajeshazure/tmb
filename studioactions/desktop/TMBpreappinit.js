function TMBpreappinit(params) {
    kony.license.disableMetricReporting();
    kony.application.setApplicationBehaviors({
        skipEscapeHtml: true
    });
    defineTMBGlobals.call(this);
    initializeMFSDKIB();
}
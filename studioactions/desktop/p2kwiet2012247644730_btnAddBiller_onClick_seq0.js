function p2kwiet2012247644730_btnAddBiller_onClick_seq0(eventobject) {
    function alert_onClick_92469201224767839_True() {}
    if ((getCRMLockStatus())) {
        curr_form = kony.application.getCurrentForm().id;
        popIBBPOTPLocked.show();
    } else {
        if ((kony.string.equals(gblOTPLockedForBiller, "04"))) {
            alert(kony.i18n.getLocalizedString("ECVrfyOTPErr"))
        } else {
            if ((checkMaxBillerCount())) {
                gblAddToMyBill = false;
                gblAddBillerButtonClicked = true;
                frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
                frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
                frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
                frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
                frmIBMyBillersHome.lblAddBillerName.text = "";
                frmIBMyBillersHome.txtAddBillerNickName.text = "";
                frmIBMyBillersHome.txtAddBillerRef1.text = "";
                frmIBMyBillersHome.lblAddBillerRef1.text = kony.i18n.getLocalizedString("keyRef1");
                frmIBMyBillersHome.lblAddBillerRef2.text = kony.i18n.getLocalizedString("keyRef2");
                frmIBMyBillersHome.txtAddBillerRef2.text = "";
                frmIBMyBillersHome.imgAddBillerLogo.src = "empty.png"
                frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
                frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
                frmIBMyBillersHome.txtAddBillerNickName.setEnabled(false);
                frmIBMyBillersHome.txtAddBillerRef1.setEnabled(false);
                frmIBMyBillersHome.txtAddBillerRef2.setEnabled(false);
            } else {
                function alert_onClick_92469201224767839_Callback() {
                    alert_onClick_92469201224767839_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertHandler": alert_onClick_92469201224767839_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        }
    }
}
function p2kwiet2012247641918_txtUserID_DESKTOPWEB_onBeginEditing_seq0(eventobject, changedtext) {
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBCreateUserID.image2448396637310883.src = "userid.png"
        frmIBCreateUserID.image2447324161940655.src = "password_rules.png"
    } else {
        frmIBCreateUserID.image2448396637310883.src = "userid_thai.png"
        frmIBCreateUserID.image2447324161940655.src = "password_rules_thai.png"
    }
    frmIBCreateUserID.hbxUserId.skin = "hbxIBbottomborderfocus";
    frmIBCreateUserID.imgarrow1.setVisibility(false);
    frmIBCreateUserID.imgarrow2.setVisibility(false);
    frmIBCreateUserID.hbxPassRules.setVisibility(false);
    frmIBCreateUserID.hbxUserIdRules.setVisibility(false);
    //frmIBCreateUserID.vbxRules.setVisibility(true)
    frmIBCreateUserID.vbxRules.skin = ""
    frmIBCreateUserID.hbxPasswordRulesBubble.setVisibility(false)
    frmIBCreateUserID.vbxRules.margin = [0, 0, 0, 0];
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) {
        document.getElementById("frmIBCreateUserID_hbxUserIDBubble").style.margin = "-5% 0% 0% 0%";
        frmIBCreateUserID.vbxRules.margin = [0, 0, 0, 0];
    }
    var countUserID = frmIBCreateUserID.txtUserID.text.length;
    if (countUserID <= 0) {
        frmIBCreateUserID.vbxRules.setVisibility(true);
        frmIBCreateUserID.hbxUserIDBubble.setVisibility(true);
    } else {
        frmIBCreateUserID.vbxRules.setVisibility(false);
        frmIBCreateUserID.hbxUserIDBubble.setVisibility(false);
    }
}
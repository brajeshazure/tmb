function p2kwiet2012247652016_btnSugBillerAdd_onClick_seq0(eventobject, context) {
    function alert_onClick_70064201224761185_True() {}

    function alert_onClick_5881220122476845_True() {}
    if ((kony.string.equals(gblOTPLockedForBiller, "04"))) {
        alert(kony.i18n.getLocalizedString("ECVrfyOTPErr"))
    } else {
        if ((checkIfBarcodeOnlyIBTopUp())) {
            if ((checkMaxBillerCountTopupIB())) {
                frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(true);
                frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxCanConfBtnContainer.setVisibility(false);
                frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(true);
                frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
                frmIBMyTopUpsHome.txtAddBillerNickName.text = "";
                frmIBMyTopUpsHome.txtAddBillerRef1.text = "";
                frmIBMyTopUpsHome.txtAddBillerNickName.setEnabled(true);
                frmIBMyTopUpsHome.txtAddBillerRef1.setEnabled(true);
                loadAddTopupValues.call(this);
            } else {
                function alert_onClick_70064201224761185_Callback() {
                    alert_onClick_70064201224761185_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "No",
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertHandler": alert_onClick_70064201224761185_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        } else {
            function alert_onClick_5881220122476845_Callback() {
                alert_onClick_5881220122476845_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "",
                "yesLabel": "Ok",
                "noLabel": "No",
                "message": kony.i18n.getLocalizedString("Valid_BarcodeOnly"),
                "alertHandler": alert_onClick_5881220122476845_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
        }
    }
}
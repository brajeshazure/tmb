function p2kwiet2012247642662_frmIBEditFutureBillPaymentPrecnf_preshow_seq0(eventobject, neworientation) {
    frmIBEditFutureBillPaymentPrecnf.lblOTPinCurr.text = "";
    frmIBEditFutureBillPaymentPrecnf.lblPlsReEnter.text = "";
    frmIBEditFutureBillPaymentPrecnf.hbxAdv.setVisibility(false);
    frmIBEditFutureBillPaymentPrecnf.hbxOTP.setVisibility(false);
    frmIBEditFutureBillPaymentPrecnf.hbxEditBtns.setVisibility(false);
    frmIBEditFutureBillPaymentPrecnf.hbxbtnUpdatedReturn.setVisibility(false)
    frmIBEditFutureBillPaymentPrecnf.btnNext.setVisibility(true);
    var curFrm = kony.application.getCurrentForm().id;
    if (curFrm == "frmIBFBLogin") {
        frmIBEditFutureBillPaymentPrecnf.hbxBPCmpletngHdr.setVisibility(true);
        frmIBEditFutureBillPaymentPrecnf.imgComplete.setVisibility(true);
        frmIBEditFutureBillPaymentPrecnf.hbxbtnUpdatedReturn.setVisibility(true);
        frmIBEditFutureBillPaymentPrecnf.hbxBPSave.setVisibility(true);
        frmIBEditFutureBillPaymentPrecnf.hbxBPViewHdr.setVisibility(false);
        frmIBEditFutureBillPaymentPrecnf.hbxOTP.setVisibility(false);
        frmIBEditFutureBillPaymentPrecnf.hbxEditBtns.setVisibility(false);
        frmIBEditFutureBillPaymentPrecnf.hbxAdv.setVisibility(true);
        frmIBEditFutureBillPaymentPrecnf.btnNext.setVisibility(false);
    }
}
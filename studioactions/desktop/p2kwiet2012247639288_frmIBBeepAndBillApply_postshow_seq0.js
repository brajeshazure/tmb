function p2kwiet2012247639288_frmIBBeepAndBillApply_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    segConvenientServicesLoad.call(this);
    pagespecificSubMenu.call(this);
    populatefrmBBSelect(gblmasterBillerAndTopupBB);
    frmIBBeepAndBillApply.imgAddBillerLogo.isVisible = false;
    frmIBBeepAndBillApply.lblAddBillerCompCode.isVisible = false;
    populateOnFrmIBBeepAndBillList.call(this);
}
function p2kwiet2012247645129_btnAddAccntNext_onClick_seq0(eventobject) {
    if (frmIBMyReceipentsAddContactManually.hbxotpreconfrm.isVisible) {
        if (gblAdd_Receipent_State == gblEXISTING_RC_EDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM) {
            validateSecurityForTransactionRc(frmIBMyReceipentsAddContactManually);
        }
        frmIBMyReceipentsAddContactManually.txtotp.setFocus(true)
    } else {
        //onEditReceipentNextIB();
        saveRecipientDeatils();
        frmIBMyReceipentsAddContactManually.hbxotpreconfrm.setVisibility(true)
        frmIBMyReceipentsAddContactManually.txtotp.setFocus(true);
    }
}
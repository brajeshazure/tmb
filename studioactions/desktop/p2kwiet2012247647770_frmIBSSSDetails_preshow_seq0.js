function p2kwiet2012247647770_frmIBSSSDetails_preshow_seq0(eventobject, neworientation) {
    frmIBSSSDetails.lblCurrencyLogo1.text = "\u0E3F";
    frmIBSSSDetails.lblCurrencyLogo2.text = "\u0E3F";
    //frmIBSSSDetails.lblCurrencyLogo3.text = "\u0E3F";
    //frmIBSSSDetails.lblCurrencyLogo4.text = "\u0E3F";
    /*
     * Below if-else condition is to check if No Fixed Account is hidden or not 
     */
    kony.print("gblNFHidden.. " + gblNFHidden);
    kony.print("gblSSLinkedStatus.. " + gblSSLinkedStatus);
    gblSSApply = "false";
    /*
     * Below if-else condition is to check if No Fixed Account is hidden or not 
     */
    kony.print("gblNFHidden.. " + gblNFHidden);
    kony.print("gblSSLinkedStatus.. " + gblSSLinkedStatus);
    gblSSApply = "false";
    if (gblNFHidden == "02") {
        //gblNFHidden== "02" ;; mean no fixed account is deleted/hidden
        kony.print("in if gblNFHidden.. ");
        frmIBSSSDetails.hbxSaveTo.setVisibility(false);
        frmIBSSSDetails.richTxtNoFixdAcctHidden.setVisibility(true);
        frmIBSSSDetails.button447476809947.onClick = null;
        gblNFHidden = "noFixedAccDeleted";
        frmIBSSSDetails.richTxtNoFixdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg") + "<a onclick= \"return onClickCallBackFunctionIB();\" href = \"#\" >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
    } else {
        frmIBSSSDetails.hbxSaveTo.setVisibility(true);
        frmIBSSSDetails.richTxtNoFixdAcctHidden.setVisibility(false);
    }
    /*
     * Below if-else condition is to check if linked account is hidden or not & we have to set the value of variable 'gblSSLinkAcc' after checking service call
     */
    if (gblSSLinkedStatus == "02") {
        //gblSSLinkedStatus == "02" ;; mean linked account is deleted/hidden
        kony.print("in if gblSSLinkedStatus.. ");
        frmIBSSSDetails.hbxSendFrom.setVisibility(false);
        frmIBSSSDetails.richTxtLnkdAcctHidden.setVisibility(true);
        gblSSLinkedStatus = "linkedAccDeleted";
        frmIBSSSDetails.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg") + "<a onclick= \"return onClickCallBackFunctionIB();\" href = \"#\" >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
    } else {
        frmIBSSSDetails.hbxSendFrom.setVisibility(true);
        frmIBSSSDetails.richTxtLnkdAcctHidden.setVisibility(false);
    }
}
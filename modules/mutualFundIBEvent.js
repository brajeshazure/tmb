function onClickRedeemIB(){
  var APchar = gblUnitHolderNumber.substring(0, 2);
  kony.print("APchar  "+APchar);
  kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
  var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
  if(AS_prefix == APchar){
	alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
    return;
  }
  if(getCRMLockStatus()){
    curr_form = kony.application.getCurrentForm().id;
    dismissLoadingScreenPopup();
    popIBBPOTPLocked.show();
  }
  else{
    showLoadingScreenPopup();
    gblMFOrder = {};
    gblMFEventFlag = MF_EVENT_REDEEM;
    gblMFOrder["orderType"] = MF_ORD_TYPE_REDEEM;
    gblMFOrder["orderFlow"] = MF_FUNDDETAIL_FLOW;
    gblMFOrder["fundNameTH"] = gblMFDetailsResulttable["FundNameTH"];
    gblMFOrder["fundNameEN"] = gblMFDetailsResulttable["FundNameEN"];
    gblMFOrder["fundShortName"] = gblFundShort;
    IBcallMutualFundRulesValidation();
  } 
}

function onClickPurchaseIB(){
  var APchar = gblUnitHolderNumber.substring(0, 2);
  kony.print("APchar  "+APchar);
  kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
  var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
  if(AS_prefix == APchar){
	alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
    return;
  }
  if(getCRMLockStatus()){
    curr_form = kony.application.getCurrentForm().id;
    dismissLoadingScreenPopup();
    popIBBPOTPLocked.show();
  }
  else{
    showLoadingScreenPopup();
    gblMFOrder = {};
    gblMFEventFlag = MF_EVENT_PURCHASE;
    gblMFOrder["orderType"] = MF_ORD_TYPE_PURCHASE;
    gblMFOrder["orderFlow"] = MF_FUNDDETAIL_FLOW;
    gblMFOrder["fundNameTH"] = gblMFDetailsResulttable["FundNameTH"];
    gblMFOrder["fundNameEN"] = gblMFDetailsResulttable["FundNameEN"];
    gblMFOrder["fundShortName"] = gblFundShort;
    IBcallMutualFundRulesValidation();
  }
}

function preShowFrmIBMFTnC(){ 
  var accountRiskLevel = 0;
  var fundRiskLevel = 0;
  var allowPurchaseRedeem = 'N';
	var locale = kony.i18n.getCurrentLocale();
  frmIBMFTnC.ImageStep.src = (locale == "th_TH") ? "step2th.png" : "step2.png";
 
  

  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFTnC.lblMFHeader.text = kony.i18n.getLocalizedString("MF_Purchase_01");
    frmIBMFTnC.LinkPurchase.skin = "linkBlack114RightBroder"; //MKI, references: MIB 9343
    frmIBMFTnC.LinkPurchase.hoverSkin = "linkBlack114RightBroder"; //MKI, references: MIB 9343
	frmIBMFTnC.LinkRedeem.skin = "linkBlue114RightBorder"; //MKI, references: MIB 9343
    frmIBMFTnC.LinkRedeem.hoverSkin = "linkBlue114RightBorderUnderlineFont"; //MKI, references: MIB 9343
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmIBMFTnC.lblMFHeader.text = kony.i18n.getLocalizedString("MF_RDM_1");
    frmIBMFTnC.LinkPurchase.skin = "linkBlue114RightBorder"; //MKI, references: MIB 9343
    frmIBMFTnC.LinkPurchase.hoverSkin = "linkBlue114RightBorderUnderlineFont"; //MKI, references: MIB 9343
    frmIBMFTnC.LinkRedeem.skin = "linkBlack114RightBroder"; //MKI, references: MIB 9343
    frmIBMFTnC.LinkRedeem.hoverSkin = "linkBlack114RightBroder"; //MKI, references: MIB 9343
  }
  frmIBMFTnC.LinkPortfolio.text = kony.i18n.getLocalizedString("MU_ACC_InvestVal");
  frmIBMFTnC.LinkPurchase.text = kony.i18n.getLocalizedString("MU_Purchase");
  frmIBMFTnC.LinkRedeem.text = kony.i18n.getLocalizedString("MU_Redeem");
  frmIBMFTnC.LinkSutability.text = kony.i18n.getLocalizedString("MU_SB_Suitability");

  frmIBMFTnC.lblRiskWarning.text = kony.i18n.getLocalizedString("MF_Disclaimer");

  frmIBMFTnC.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmIBMFTnC.btnNext.text = kony.i18n.getLocalizedString("keyAgreeButton");
  
  if(gblFundRulesData["fxRisk"] == "Y"){
    allowPurchaseRedeem = 'N';
  } else {
    allowPurchaseRedeem = 'Y';
  }
  
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFTnC.HBoxFundFact.setVisibility(true);
    frmIBMFTnC.LabelInform.text = kony.i18n.getLocalizedString("MF_Purchase_46");
    frmIBMFTnC.LabelLinkFundFactSheet.text = kony.i18n.getLocalizedString("MF_Purchase_47");
    var accountRiskLevel = parseInt(gblFundRulesData["suitabilityScore"]);
    var fundRiskLevel = parseInt(gblFundRulesData["riskRate"]);
    var custAge = gblFundRulesData["overAge"];

    accountRiskLevel = getAccountScoreMapping(accountRiskLevel); //MKI, MIB-11354
    if(accountRiskLevel < fundRiskLevel) {
      if (custAge == 'Y') {
      	gblMFOrder["AcceptTerm1"] = false;
	  	gblMFOrder["AcceptTerm2"] = false;
	  } else {
      	gblMFOrder["AcceptTerm1"] = false;
	  	gblMFOrder["AcceptTerm2"] = true;
	  }
    } else {
      	gblMFOrder["AcceptTerm1"] = true;
	  	gblMFOrder["AcceptTerm2"] = true;
    }
	
	if(allowPurchaseRedeem == "N"){
        gblMFOrder["AcceptTerm3"] = false;
    } else {
        gblMFOrder["AcceptTerm3"] = true;
    }
	
    if(gblMFOrder["AcceptTerm1"] && gblMFOrder["AcceptTerm2"] && gblMFOrder["AcceptTerm3"]){
      flagEnableWarningContent = false;
      frmIBMFTnC.btnNext.setEnabled(true);
    } else {
      flagEnableWarningContent = true;
      frmIBMFTnC.btnNext.skin = "btnIB158disabled";
      frmIBMFTnC.btnNext.setEnabled(false);
    }

    frmIBMFTnC.hbxsecurityoptions.setVisibility(flagEnableWarningContent);
    frmIBMFTnC.hbxSecurity1.setVisibility(!(gblMFOrder["AcceptTerm1"]));
    frmIBMFTnC.hbxSecurity2.setVisibility(!(gblMFOrder["AcceptTerm2"]));
	frmIBMFTnC.hbxSecurity3.setVisibility(!(gblMFOrder["AcceptTerm3"]));
    if(frmIBMFTnC.hbxSecurity1.isVisible){
      frmIBMFTnC.btnAgreeCheck1.skin = "btnCheck";	
    } else {
      frmIBMFTnC.btnAgreeCheck1.skin = "btnCheckFoc";	
    }
    if(frmIBMFTnC.hbxSecurity2.isVisible){
      frmIBMFTnC.btnAgreeCheck2.skin = "btnCheck";	
    } else {
      frmIBMFTnC.btnAgreeCheck2.skin = "btnCheckFoc";	
    }
	if(frmIBMFTnC.hbxSecurity3.isVisible){
      frmIBMFTnC.btnAgreeCheck3.skin = "btnCheck";	
    } else {
      frmIBMFTnC.btnAgreeCheck3.skin = "btnCheckFoc";	
    }

    if(!gblMFOrder["AcceptTerm2"]) {
		frmIBMFTnC.lblWarning1.text = kony.i18n.getLocalizedString("MF_Warning1");
        frmIBMFTnC.lblWarningVal1.text = kony.i18n.getLocalizedString("MF_WarningBox1");
		
		frmIBMFTnC.lblWarning2.text = kony.i18n.getLocalizedString("MF_Warning2");
        frmIBMFTnC.lblWarningVal2.text = kony.i18n.getLocalizedString("MF_WarningBox2");
	}else if(!gblMFOrder["AcceptTerm1"]){
		frmIBMFTnC.lblWarning1.text = kony.i18n.getLocalizedString("MF_Warning1");
        frmIBMFTnC.lblWarningVal1.text = kony.i18n.getLocalizedString("MF_WarningBox1");
	}
	
    if(!gblMFOrder["AcceptTerm3"]){
      frmIBMFTnC.lblWarning3.text = kony.i18n.getLocalizedString("MF_Warning3");
      frmIBMFTnC.lblWarningVal3.text = kony.i18n.getLocalizedString("MF_WarningBox3");
    }
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    frmIBMFTnC.HBoxFundFact.setVisibility(false);
    frmIBMFTnC.hbxsecurityoptions.setVisibility(false);
    frmIBMFTnC.btnNext.setEnabled(true);
    if(allowPurchaseRedeem != "Y"){
      frmIBMFTnC.hbxsecurityoptions.setVisibility(false); //MKI, MIB-12691 start
      frmIBMFTnC.hbxSecurity1.setVisibility(false);
      frmIBMFTnC.hbxSecurity2.setVisibility(true);
	  frmIBMFTnC.hbxSecurity3.setVisibility(false);
      gblMFOrder["AcceptTerm1"] = true;
      gblMFOrder["AcceptTerm2"] = true; //MKI, MIB-12691 start
	  gblMFOrder["AcceptTerm3"] = true;
      frmIBMFTnC.btnNext.skin = "btnIB158"; //MKI, MIB-12691 start
      frmIBMFTnC.btnNext.setEnabled(true);
      frmIBMFTnC.lblWarning2.text = kony.i18n.getLocalizedString("MF_Warning2");
      frmIBMFTnC.lblWarningVal2.text = kony.i18n.getLocalizedString("MF_WarningBox2");
      frmIBMFTnC.btnAgreeCheck2.skin = "btnCheck";	

    } else {
      frmIBMFTnC.hbxsecurityoptions.setVisibility(false);
      frmIBMFTnC.btnNext.setEnabled(true);	
      frmIBMFTnC.btnNext.skin = "btnIB158";
    }
  }
  getMutualFundTnC();
} 
function frmIBMFTnC_setEnableNext(){
  if(gblMFOrder["AcceptTerm1"] && gblMFOrder["AcceptTerm2"] && gblMFOrder["AcceptTerm3"]) { //MKI, MIB-11354 
    frmIBMFTnC.btnNext.setEnabled(true);
    frmIBMFTnC.btnNext.skin = "btnIB158";
  } else {
    frmIBMFTnC.btnNext.setEnabled(false);
    frmIBMFTnC.btnNext.skin = "btnIB158disabled";
  }
}
function frmIBMFTnC_onClickAcceptWarning1(){
  if(frmIBMFTnC.btnAgreeCheck1.skin == "btnCheck") {
    frmIBMFTnC.btnAgreeCheck1.skin = "btnCheckFoc";	
    gblMFOrder["AcceptTerm1"] = true;
  } else {
    frmIBMFTnC.btnAgreeCheck1.skin = "btnCheck";	
    gblMFOrder["AcceptTerm1"] = false;
  }
  frmIBMFTnC_setEnableNext();

}
function frmIBMFTnC_onClickAcceptWarning2(){
  if(frmIBMFTnC.btnAgreeCheck2.skin == "btnCheck") {
    frmIBMFTnC.btnAgreeCheck2.skin = "btnCheckFoc";	
    gblMFOrder["AcceptTerm2"] = true;
  } else {
    frmIBMFTnC.btnAgreeCheck2.skin = "btnCheck";	
    gblMFOrder["AcceptTerm2"] = false;
  }
  frmIBMFTnC_setEnableNext();
}
function frmIBMFTnC_onClickAcceptWarning3(){ //MKI, MIB-11354  start
  if(frmIBMFTnC.btnAgreeCheck3.skin == "btnCheck") {
    frmIBMFTnC.btnAgreeCheck3.skin = "btnCheckFoc";	
    gblMFOrder["AcceptTerm3"] = true;
  } else {
    frmIBMFTnC.btnAgreeCheck3.skin = "btnCheck";	
    gblMFOrder["AcceptTerm3"] = false;
  }
  frmIBMFTnC_setEnableNext();

} //MKI, MIB-11354 End
function frmIBMFTnC_onClickNext(){
  IBcallMutualFundRulesValidationForAcceptTnc();
}
function frmIBMFTnC_onClickBack(){
  callMutualFundsSummary();
}

function frmIBMFTnC_onClickCheckBox(){
  if(frmIBMFTnC.btnAgreeCheck1.skin == "btnCheck"){
    frmIBMFTnC.btnAgreeCheck1.skin = "btnCheckFoc";
    frmIBMFTnC.btnNext.setEnabled(true);
  } else if(frmIBMFTnC.btnAgreeCheck1.skin == "btnCheckFoc"){
    frmIBMFTnC.btnAgreeCheck1.skin = "btnCheck";
    frmIBMFTnC.btnNext.setEnabled(false);
  }
}

function preShowFrmIBMFSelRedeemFund(){
  var locale = kony.i18n.getCurrentLocale();
  var investmentValue = "";
  var unitHolder = "";

  var fundHouseCode = "";

  var randomnum = Math.floor((Math.random() * 10000) + 1);
  var logo = "";
	
  frmIBMFSelRedeemFund.ImageStep.src = (locale == "th_TH") ? "step1th.png" : "step1.png"; // MKI, MIB-8529
  syncIBMFSelRedeemFund();

  if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
    frmIBMFSelRedeemFund.HBoxSelectFund.setVisibility(false);
    frmIBMFSelRedeemFund.HBoxSelectedFund.setVisibility(true);
    frmIBMFSelRedeemFund.VBoxArrow.setVisibility(false);

    frmIBMFSelRedeemFund.HBoxDetail.setVisibility(true);
    frmIBMFSelRedeemFund.HBoxTranDate.setVisibility(true);
    frmIBMFSelRedeemFund.btnNext.setEnabled(true);
    frmIBMFSelRedeemFund.btnNext.skin = "btnIB320";//btnIB320dis
    frmIBMFSelRedeemFund.btnNext.setVisibility(true);

    frmIBMFSelRedeemFund.HBoxFundList.setVisibility(false);
    frmIBMFSelRedeemFund.hbxImage.setVisibility(true);


    fundHouseCode = gblMFDetailsResulttable["FundHouseCode"];
    logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
      "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
    frmIBMFSelRedeemFund.ImageFundHouse.src = logo;
    frmIBMFSelRedeemFund.lblFundCode.text = gblMFOrder["fundShortName"];
    frmIBMFSelRedeemFund.lblUnitHolderNo.text = gblMFDetailsResulttable["UnitHolderNo"];

    redeemableUnit = commaFormatted(numberFormat(gblFundRulesData["totalRedeemInProcess"], 4));

    var unitF = parseFloat(numberFormat(gblMFDetailsResulttable["Unit"], 4));
    var ordUnitF = numberFormat(gblFundRulesData["totalRedeemInProcess"], 4);
    var redeemableUnitF = unitF - ordUnitF;

    gblMFOrder["redeemableUnit"] = numberFormat(redeemableUnitF, 4);
    frmIBMFSelRedeemFund.lblRedeemUnitVals.text =  commaFormatted(numberFormat(unitF,4));//commaFormatted(gblMFOrder["redeemableUnit"]); //MKI,MIB-12562

    investmentValue = commaFormatted(gblMFDetailsResulttable["InvestmentValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

    frmIBMFSelRedeemFund.lblBalanceVal.text = investmentValue;

    frmIBMFSelRedeemFund.btnAll.skin = "btnIB18pxWhiteFontBlueBG";
    frmIBMFSelRedeemFund.btnAll.focusSkin = "btnIB18pxBlueFontWhiteBG";

    frmIBMFSelRedeemFund.txtAmount.text = gblMFOrder["redeemableUnit"]; 
    frmIBMFSelRedeemFund.lblTranOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);

    if(gblMFOrder["redeemUnit"] == undefined || gblMFOrder["redeemUnit"] == ORD_UNIT_ALL){
      frmIBMFSelRedeemFund_onClickBtnAll();
    } else {
      if(gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT){
        frmIBMFSelRedeemFund_onClickBtnUnit();
        frmIBMFSelRedeemFund.txtAmount.text = onDoneEditingUnitValue(gblMFOrder["amount"]); 
      } else if(gblMFOrder["redeemUnit"] == ORD_UNIT_BAHT){
        frmIBMFSelRedeemFund_onClickBtnBaht();
        frmIBMFSelRedeemFund.txtAmount.text = onDoneEditingAmountValue(gblMFOrder["amount"]); 
      }
    }

  } else if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    if(gblFundRulesData["fundCode"] != undefined && gblFundRulesData["fundCode"] != ""){
      frmIBMFSelRedeemFund.HBoxSelectFund.setVisibility(false);
      frmIBMFSelRedeemFund.HBoxSelectedFund.setVisibility(true);
      frmIBMFSelRedeemFund.VBoxArrow.setVisibility(true);

      frmIBMFSelRedeemFund.HBoxDetail.setVisibility(true);
      frmIBMFSelRedeemFund.HBoxTranDate.setVisibility(true);
      frmIBMFSelRedeemFund.btnNext.setEnabled(true);
      frmIBMFSelRedeemFund.btnNext.skin = "btnIB320";//btnIB320dis
      frmIBMFSelRedeemFund.btnNext.setVisibility(true);

      frmIBMFSelRedeemFund.HBoxFundList.setVisibility(false);
      frmIBMFSelRedeemFund.hbxImage.setVisibility(true);

      fundHouseCode = gblMFOrder["fundHouseCode"];
      logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
        "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
      frmIBMFSelRedeemFund.ImageFundHouse.src = logo;
      frmIBMFSelRedeemFund.lblFundCode.text = gblMFOrder["fundShortName"];
      frmIBMFSelRedeemFund.lblUnitHolderNo.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);

      redeemableUnit = commaFormatted(numberFormat(gblFundRulesData["totalRedeemInProcess"], 4));

      var unitF = parseFloat(numberFormat(gblMFDetailsResulttable["Unit"], 4));
      var ordUnitF = numberFormat(gblFundRulesData["totalRedeemInProcess"], 4);
      var redeemableUnitF = unitF - ordUnitF;

      gblMFOrder["redeemableUnit"] = numberFormat(redeemableUnitF, 4);
      frmIBMFSelRedeemFund.lblRedeemUnitVals.text = commaFormatted(numberFormat(unitF,4));//commaFormatted(gblMFOrder["redeemableUnit"]); //MKI,MIB-12562

      investmentValue = commaFormatted(gblMFDetailsResulttable["InvestmentValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

      frmIBMFSelRedeemFund.lblBalanceVal.text = investmentValue;

      frmIBMFSelRedeemFund.btnAll.skin = "btnIB18pxWhiteFontBlueBG";
      frmIBMFSelRedeemFund.btnAll.focusSkin = "btnIB18pxBlueFontWhiteBG";

      frmIBMFSelRedeemFund.txtAmount.text = commaFormatted(gblMFOrder["redeemableUnit"]);
      frmIBMFSelRedeemFund.lblTranOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);

      if(gblMFOrder["redeemUnit"] == undefined || gblMFOrder["redeemUnit"] == ORD_UNIT_ALL){
        frmIBMFSelRedeemFund_onClickBtnAll();
      } else {
        if(gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT){
          frmIBMFSelRedeemFund_onClickBtnUnit();
          frmIBMFSelRedeemFund.txtAmount.text = onDoneEditingUnitValue(gblMFOrder["amount"]); 
        } else if(gblMFOrder["redeemUnit"] == ORD_UNIT_BAHT){
          frmIBMFSelRedeemFund_onClickBtnBaht();
          frmIBMFSelRedeemFund.txtAmount.text = onDoneEditingAmountValue(gblMFOrder["amount"]); 
        }
      }
    }
    else {
      frmIBMFSelRedeemFund.HBoxSelectFund.setVisibility(true);
      frmIBMFSelRedeemFund.HBoxSelectedFund.setVisibility(false);
      frmIBMFSelRedeemFund.VBoxArrow.setVisibility(true);

      frmIBMFSelRedeemFund.HBoxDetail.setVisibility(false);
      frmIBMFSelRedeemFund.HBoxTranDate.setVisibility(false);
      frmIBMFSelRedeemFund.btnNext.setVisibility(false);

      frmIBMFSelRedeemFund.HBoxFundList.setVisibility(true);
      frmIBMFSelRedeemFund.hbxImage.setVisibility(false);

      setFundListForRedeemIB();
    }

  }
}
function frmIBMFSelRedeemFund_SelectFund(){
  if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    frmIBMFSelRedeemFund.hbxImage.setVisibility(false);
    frmIBMFSelRedeemFund.HBoxFundList.setVisibility(true);
    frmIBMFSelRedeemFund.HBoxDetail.setVisibility(false);
    frmIBMFSelRedeemFund.HBoxTranDate.setVisibility(false);
    frmIBMFSelRedeemFund.btnNext.setVisibility(false);
    frmIBMFSelRedeemFund.VBoxArrow.setVisibility(true);
    frmIBMFSelRedeemFund.HBoxSelectedFund.setVisibility(false);
    frmIBMFSelRedeemFund.HBoxSelectFund.setVisibility(true);
  }
}
function frmIBMFSelRedeemFund_SegSelectFund(){
  if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    callIBMutualFundsDetailsForSelectFund();
  }
}
function frmIBMFSelRedeemFund_onClickBtnAll(){
  gblMFOrder["redeemUnit"] = ORD_UNIT_ALL;
  frmIBMFSelRedeemFund.txtAmount.text = commaFormatted(gblMFOrder["redeemableUnit"]);
  frmIBMFSelRedeemFund.txtAmount.setEnabled(false);
  frmIBMFSelRedeemFund.txtAmount.setVisibility(true);
  frmIBMFSelRedeemFund.btnAll.skin = "btnIB18pxWhiteFontBlueBG";
  frmIBMFSelRedeemFund.btnBaht.skin = "btnIB18pxBlueFontWhiteBG";
  frmIBMFSelRedeemFund.btnUnit.skin = "btnIB18pxBlueFontWhiteBG";
}

function frmIBMFSelRedeemFund_onClickBtnBaht(){
  gblMFOrder["redeemUnit"] = ORD_UNIT_BAHT;
  frmIBMFSelRedeemFund.txtAmount.text = "";
  frmIBMFSelRedeemFund.txtAmount.setEnabled(true);
  frmIBMFSelRedeemFund.btnAll.skin = "btnIB18pxBlueFontWhiteBG";
  frmIBMFSelRedeemFund.btnBaht.skin = "btnIB18pxWhiteFontBlueBG";
  frmIBMFSelRedeemFund.btnUnit.skin = "btnIB18pxBlueFontWhiteBG";
  frmIBMFSelRedeemFund.txtAmount.placeholder = "0.00";
  frmIBMFSelRedeemFund.txtAmount.setFocus(true);
}

function frmIBMFSelRedeemFund_onClickBtnUnit(){
  gblMFOrder["redeemUnit"] = ORD_UNIT_UNIT;
  frmIBMFSelRedeemFund.txtAmount.text = "";
  frmIBMFSelRedeemFund.txtAmount.setEnabled(true);
  frmIBMFSelRedeemFund.btnAll.skin = "btnIB18pxBlueFontWhiteBG";
  frmIBMFSelRedeemFund.btnBaht.skin = "btnIB18pxBlueFontWhiteBG";
  frmIBMFSelRedeemFund.btnUnit.skin = "btnIB18pxWhiteFontBlueBG";
  frmIBMFSelRedeemFund.txtAmount.placeholder = "0.0000";
  frmIBMFSelRedeemFund.txtAmount.setFocus(true);
}

function frmIBMFSelRedeemFundAmountOnEndEditing(){
  if(gblMFOrder["redeemUnit"] == ORD_UNIT_BAHT){
    if (kony.string.containsChars(frmIBMFSelRedeemFund.txtAmount.text, ["."]))
      frmIBMFSelRedeemFund.txtAmount.text = frmIBMFSelRedeemFund.txtAmount.text;
    else
      frmIBMFSelRedeemFund.txtAmount.text = frmIBMFSelRedeemFund.txtAmount.text + ".00";
    frmIBMFSelRedeemFund.txtAmount.text = onDoneEditingAmountValue(frmIBMFSelRedeemFund.txtAmount.text);
  } else if(gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT){
    if (kony.string.containsChars(frmIBMFSelRedeemFund.txtAmount.text, ["."]))
      frmIBMFSelRedeemFund.txtAmount.text = frmIBMFSelRedeemFund.txtAmount.text;
    else
      frmIBMFSelRedeemFund.txtAmount.text = frmIBMFSelRedeemFund.txtAmount.text + ".0000";
    frmIBMFSelRedeemFund.txtAmount.text = onDoneEditingUnitValue(frmIBMFSelRedeemFund.txtAmount.text);
  }
}

function frmIBMFSelRedeemFundAmountOnKeyUp(){
  var enteredAmount = frmIBMFSelRedeemFund.txtAmount.text;
  if(isNotBlank(enteredAmount)) {
    enteredAmount = kony.string.replace(enteredAmount, ",", "");
    if(!(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0)){
      frmIBMFSelRedeemFund.txtAmount.text = commaFormattedTransfer(enteredAmount);
    }
  }
}

function frmIBMFSelRedeemFund_onClickNext(){
  checkValidAmountIB();
}

function frmIBMFSelRedeemFund_onClickBack(){
  callMutualFundsSummary();
}
//function preShowFrmIBMFComplete(){
 
//}

function preShowFrmIBMFConfirm(){
  var locale = kony.i18n.getCurrentLocale();
  var fundHouseCode = gblMFOrder["fundHouseCode"];

  var randomnum = Math.floor((Math.random() * 10000) + 1);
  var logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
      "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;

  frmIBMFConfirm.button506369893806238.onClick = frmIBMFConfirmOnClickSwitchToMobileOTP;
	frmIBMFConfirm.ImageStep.src = (locale == "th_TH") ? "step3th.png" : "step3.png"; // MKI, MIB-9082
  syncIBMFConfirm();

    if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFConfirm.imgMFicon.src = frmIBMFSelPurchaseFundsLP.ImageFundHouse.src;
    frmIBMFConfirm.LinkPurchase.skin = "linkBlack114RightBroder"; //MKI, references: MIB 9343
	frmIBMFConfirm.LinkPurchase.hoverSkin = "linkBlack114RightBroder"; //MKI, references: MIB 9343
	frmIBMFConfirm.LinkRedeem.skin = "linkBlue114RightBorder"; //MKI, references: MIB 9343
	frmIBMFConfirm.LinkRedeem.hoverSkin = "linkBlue114RightBorderUnderlineFont"; //MKI, references: MIB 9343
  } else if (gblMFEventFlag == MF_EVENT_REDEEM) {
    frmIBMFConfirm.imgMFicon.src = frmIBMFSelRedeemFund.ImageFundHouse.src;
    frmIBMFConfirm.LinkPurchase.skin = "linkBlue114RightBorder"; //MKI, references: MIB 9343
	frmIBMFConfirm.LinkPurchase.hoverSkin = "linkBlue114RightBorderUnderlineFont"; //MKI, references: MIB 9343
	frmIBMFConfirm.LinkRedeem.skin = "linkBlack114RightBroder"; //MKI, references: MIB 9343
	frmIBMFConfirm.LinkRedeem.hoverSkin = "linkBlack114RightBroder"; //MKI, references: MIB 9343
  }
  
  
  frmIBMFConfirm.lblunitholderval.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);
  frmIBMFConfirm.lblfundcodeval.text = gblMFOrder["fundShortName"];
  frmIBMFConfirm.lblfundnamefullval.text = (locale == "th_TH") ? gblMFOrder["fundNameTH"] : gblMFOrder["fundNameEN"];

  if(gblMFOrder["redeemUnit"] == ORD_UNIT_ALL || gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT) {
    amountUnit = kony.i18n.getLocalizedString("MF_RD_lbl_unit");
  } else {
    amountUnit = kony.i18n.getLocalizedString("currencyThaiBaht");
  }
  frmIBMFConfirm.lblAmountVal.text = commaFormatted(gblMFOrder["amount"]) + " " + amountUnit;

  frmIBMFConfirm.lblTranOrdDateVal.text = formatDateMF(GLOBAL_TODAY_DATE);

  tempDate = gblFundRulesData["effectiveDate"].substring(0,2)
  tempMonth = gblFundRulesData["effectiveDate"].substring(3,5);
  tempYear = gblFundRulesData["effectiveDate"].substring(6,10);
  effDate = tempMonth + "/" + tempDate + "/" + tempYear;
  frmIBMFConfirm.lblEffDateVal.text = formatDateMF(effDate);

  frmIBMFConfirm.lblCutOffTimeVal.text = gblFundRulesData["timeNotAllow"].substring(0,5) + ":00";
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFConfirm.HBox0f8605226cf804c.setVisibility(true);
    frmIBMFConfirm.lblSelectAcctVal.text = (locale == "th_TH") ? gblMFOrder["acctNickNameTH"] : gblMFOrder["acctNickNameEN"];
    frmIBMFConfirm.HBoxMFSettleDate.setVisibility(false);
  } else {
    frmIBMFConfirm.HBox0f8605226cf804c.setVisibility(false);
    frmIBMFConfirm.HBoxMFSettleDate.setVisibility(true);
    frmIBMFConfirm.LabelSettleDateVal.text = "T + " + gblFundRulesData["timeToRed"];
  }
}

function frmIBMFConfirm_onClickBack(){
  callMutualFundsSummary();
}

function frmIBMFConfirm_onClickConfirm(){
  confirmMFOrderCompositeJavaService();
}

function preShowFrmIBMFSelPurchaseFundsLP(){
  
  var locale = kony.i18n.getCurrentLocale();
  kony.print("MKi preShowFrmIBMFSelPurchaseFundsLP locale="+locale);
  frmIBMFSelPurchaseFundsLP.ImageStep.src = (locale == "th_TH") ? "step1th.png" : "step1.png"; // MKI, MIB-9082
  syncIBMFSelPurchaseFundsLP();

  var fundHouseCode = "";
  var randomnum = Math.floor((Math.random() * 10000) + 1);
  var logo = "";

  if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
    frmIBMFSelPurchaseFundsLP.HBoxSelectFund.setVisibility(false);
    frmIBMFSelPurchaseFundsLP.HBoxSelectedFund.setVisibility(true);
    frmIBMFSelPurchaseFundsLP.VBoxArrowUnitHolder.setVisibility(false);

    frmIBMFSelPurchaseFundsLP.HBoxSelectUnitHolderNo.setVisibility(false);
    frmIBMFSelPurchaseFundsLP.HBoxSelectedUnitHolderNo.setVisibility(true);
    frmIBMFSelPurchaseFundsLP.VBoxArrowFund.setVisibility(false);

    frmIBMFSelPurchaseFundsLP.HBoxSelectAmount.setVisibility(false);
    frmIBMFSelPurchaseFundsLP.HBoxTranDate.setVisibility(false);
    frmIBMFSelPurchaseFundsLP.hbxBut0otn.setVisibility(false);

    fundHouseCode = gblMFDetailsResulttable["FundHouseCode"]; 
    logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
      "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
    frmIBMFSelPurchaseFundsLP.ImageFundHouse.src = logo;
    frmIBMFSelPurchaseFundsLP.lblunitholderval.text = gblMFDetailsResulttable["UnitHolderNo"];
    frmIBMFSelPurchaseFundsLP.lblfundcodeval.text = gblMFOrder["fundShortName"];

  } else if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    frmIBMFSelPurchaseFundsLP.VBoxArrowUnitHolder.setVisibility(true);
    frmIBMFSelPurchaseFundsLP.VBoxArrowFund.setVisibility(true);
    frmIBMFSelPurchaseFundsLP.HBoxSelectAmount.setVisibility(false);
    frmIBMFSelPurchaseFundsLP.HBoxTranDate.setVisibility(false);
    frmIBMFSelPurchaseFundsLP.hbxBut0otn.setVisibility(false);

    fundHouseCode = gblMFOrder["fundHouseCode"];
    logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
      "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
    frmIBMFSelPurchaseFundsLP.ImageFundHouse.src = logo;
    frmIBMFSelPurchaseFundsLP.lblunitholderval.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);
    frmIBMFSelPurchaseFundsLP.lblfundcodeval.text = gblMFOrder["fundShortName"];


  }
}

function preShowFrmIBMFSelPurchaseFundsEntry(){
  var locale = kony.i18n.getCurrentLocale();
  frmIBMFSelPurchaseFundsEntry.ImageStep.src = (locale == "th_TH") ? "step1th.png" : "step1.png";
  syncIBMFSelPurchaseFundsEntry();
  frmIBMFSelPurchaseFundsEntry.HBoxErrorUnitHolderNo.setVisibility(false);
  if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
    frmIBMFSelPurchaseFundsEntry.HBoxSelectFund.setVisibility(false);
    frmIBMFSelPurchaseFundsEntry.HBoxSelectedFund.setVisibility(true);
    frmIBMFSelPurchaseFundsEntry.HBoxSelectUnitHolderNo.setVisibility(false);
    frmIBMFSelPurchaseFundsEntry.HBoxSelectedUnitHolderNo.setVisibility(true);
    frmIBMFSelPurchaseFundsEntry.HBoxDetail.setVisibility(true);
    frmIBMFSelPurchaseFundsEntry.HBoxSelectAccount.setVisibility(false);
    frmIBMFSelPurchaseFundsEntry.HBoxSelectedAccount.setVisibility(true);
    frmIBMFSelPurchaseFundsEntry.hbxBut0otn.skin = "btnIB320";
    frmIBMFSelPurchaseFundsEntry.HBoxSelectAmount.setVisibility(true);
    frmIBMFSelPurchaseFundsEntry.HBoxTranDate.setVisibility(true);
    frmIBMFSelPurchaseFundsEntry.hbxBut0otn.setVisibility(true);
    frmIBMFSelPurchaseFundsEntry.VBoxArrowUnitHolder.setVisibility(false);
    frmIBMFSelPurchaseFundsEntry.VBoxArrowFund.setVisibility(false);
    frmIBMFSelPurchaseFundsEntry.HBoxSegSelectFund.setVisibility(false);
    frmIBMFSelPurchaseFundsEntry.HBoxSegSelectUnitHol.setVisibility(false);
    frmIBMFSelPurchaseFundsEntry.hbxImage.setVisibility(true);

    frmIBMFSelPurchaseFundsEntry.ImageFundHouse.src = frmIBMFSelPurchaseFundsLP.ImageFundHouse.src;

    frmIBMFSelPurchaseFundsEntry.lblfundcodeval.text = gblMFOrder["fundShortName"];

    frmIBMFSelPurchaseFundsEntry.lblunitholderval.text = frmIBMFSelPurchaseFundsLP.lblunitholderval.text;

    if(gblFundRulesData["effectiveDate"] != undefined) {
      frmIBMFSelPurchaseFundsEntry.lbltransdateval.text = formatDateMF(GLOBAL_TODAY_DATE); //MKI, MIB-10653
    } else {
      frmIBMFSelPurchaseFundsEntry.lbltransdateval.text = "";
    }
    

    if(gblMFOrder["avaiAmount"] != undefined && gblMFOrder["avaiAmount"] != 0.00){
      frmIBMFSelPurchaseFundsEntry.lblAccountNameVal.text = (locale == "th_TH") ? gblMFOrder["acctNickNameTH"] : gblMFOrder["acctNickNameEN"];	
      frmIBMFSelPurchaseFundsEntry.lblAccountbalval.text = commaFormatted(numberFormat(gblMFOrder["avaiAmount"], 2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
      if(gblMFOrder["amount"] != undefined && gblMFOrder["amount"] != 0.00){
        frmIBMFSelPurchaseFundsEntry.txtAmountVal.text = commaFormatted(numberFormat(gblMFOrder["amount"], 2));
      } else {
        frmIBMFSelPurchaseFundsEntry.txtAmountVal.text = "";
        frmIBMFSelPurchaseFundsEntry.txtAmountVal.text.placeholder = "0.00";
      }
    } else {
      frmIBMFSelPurchaseFundsEntry.lblAccountbalval.text = "";
      frmIBMFSelPurchaseFundsEntry.txtAmountVal.text.placeholder = "0.00";
    }
    frmIBMFSelPurchaseFundsEntry.txtAmountVal.setFocus(true);

  } else if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    frmIBMFSelPurchaseFundsEntry.VBoxArrowUnitHolder.setVisibility(true);
    frmIBMFSelPurchaseFundsEntry.VBoxArrowFund.setVisibility(true);
    if(gblMFOrder["fundCode"] == undefined || gblMFOrder["fundCode"] == ""){
      frmIBMFSelPurchaseFundsEntry.HBoxSelectFund.setVisibility(true);
      frmIBMFSelPurchaseFundsEntry.HBoxSelectedFund.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxSelectUnitHolderNo.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxSelectedUnitHolderNo.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxSegSelectFund.setVisibility(true);
      frmIBMFSelPurchaseFundsEntry.hbxImage.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxDetail.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxSelectAccount.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxSelectedAccount.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxSelectAmount.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxTranDate.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.hbxBut0otn.setVisibility(false);
      frmIBMFSelPurchaseFundsEntry.HBoxSegSelectUnitHol.setVisibility(false);
      setFundListForPurchase("","");
      frmIBMFSelPurchaseFundsEntryOnClickFeatured();

    } else if(gblMFOrder["fundCode"] != ""){
      if(gblMFOrder["unitHolderNo"] == undefined || gblMFOrder["unitHolderNo"] == ""){
        randomnum = Math.floor((Math.random() * 10000) + 1);
        fundHouseCode = gblMFOrder["fundHouseCode"];
        logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
          "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;

        frmIBMFSelPurchaseFundsEntry.ImageFundHouse.src = logo;
        frmIBMFSelPurchaseFundsEntry.lblfundcodeval.text = gblMFOrder["fundShortName"];

        frmIBMFSelPurchaseFundsEntry.HBoxSelectFund.setVisibility(false);
        frmIBMFSelPurchaseFundsEntry.HBoxSelectedFund.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.HBoxSelectUnitHolderNo.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.HBoxSelectedUnitHolderNo.setVisibility(false);

        var noOfUnitHolderNo = setUnitHolderNumberListPurchase(gblMFOrder["fundHouseCode"], gblMFOrder["fundCode"], gblMFOrder["allotType"]);
        if(noOfUnitHolderNo > 1){
          frmIBMFSelPurchaseFundsEntry.HBoxSegSelectFund.setVisibility(false);
          frmIBMFSelPurchaseFundsEntry.HBoxSegSelectUnitHol.setVisibility(true);
          frmIBMFSelPurchaseFundsEntry.hbxImage.setVisibility(false);
          frmIBMFSelPurchaseFundsEntry.HBoxDetail.setVisibility(false);
          frmIBMFSelPurchaseFundsEntry.HBoxSelectAccount.setVisibility(false);
          frmIBMFSelPurchaseFundsEntry.HBoxSelectedAccount.setVisibility(false);
          frmIBMFSelPurchaseFundsEntry.HBoxSelectAmount.setVisibility(false);
          frmIBMFSelPurchaseFundsEntry.HBoxTranDate.setVisibility(false);
          frmIBMFSelPurchaseFundsEntry.hbxBut0otn.setVisibility(false);
        } else if(noOfUnitHolderNo == 0){
          frmIBMFSelPurchaseFundsEntry.HBoxSegSelectFund.setVisibility(false);
          frmIBMFSelPurchaseFundsEntry.HBoxErrorUnitHolderNo.setVisibility(true);
          frmIBMFSelPurchaseFundsEntry.HBoxSegSelectUnitHol.setVisibility(false);
          gblMFOrder["fundCode"] = "";
        } else if(noOfUnitHolderNo == 1){
          var firstIndex = [0, 0];
          frmIBMFSelPurchaseFundsEntry.SegmentUnitHolderList.selectedIndex = firstIndex;
          frmIBMFSelPurchaseFundsEntryOnClickOnSegmentUnitHolder();
        }
      } else if(gblMFOrder["fundCode"] != "" && gblMFOrder["unitHolderNo"] != ""){
        frmIBMFSelPurchaseFundsEntry.HBoxSelectFund.setVisibility(false);
        frmIBMFSelPurchaseFundsEntry.HBoxSelectedFund.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.HBoxSelectUnitHolderNo.setVisibility(false);
        frmIBMFSelPurchaseFundsEntry.HBoxSelectedUnitHolderNo.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.HBoxDetail.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.HBoxSelectAccount.setVisibility(false);
        frmIBMFSelPurchaseFundsEntry.HBoxSelectedAccount.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.hbxBut0otn.skin = "btnIB320";
        frmIBMFSelPurchaseFundsEntry.HBoxSelectAmount.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.HBoxTranDate.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.hbxBut0otn.setVisibility(true);
        frmIBMFSelPurchaseFundsEntry.HBoxSegSelectFund.setVisibility(false);
        frmIBMFSelPurchaseFundsEntry.HBoxSegSelectUnitHol.setVisibility(false);
        frmIBMFSelPurchaseFundsEntry.hbxImage.setVisibility(true);

        if(gblFundRulesData["effectiveDate"] != undefined){
          frmIBMFSelPurchaseFundsEntry.lbltransdateval.text = formatDateMF(GLOBAL_TODAY_DATE); //MKI, MIB-10653
        } else {
          frmIBMFSelPurchaseFundsEntry.lbltransdateval.text = "";
        }
        

        frmIBMFSelPurchaseFundsEntry.lblAccountbalval.text = commaFormatted(numberFormat(gblMFOrder["avaiAmount"], 2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

        frmIBMFSelPurchaseFundsEntry.lblunitholderval.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);
        if(gblMFOrder["amount"] != undefined && gblMFOrder["amount"] != 0.00){
          frmIBMFSelPurchaseFundsEntry.txtAmountVal.text = commaFormatted(numberFormat(gblMFOrder["amount"], 2));
        } else {
          frmIBMFSelPurchaseFundsEntry.txtAmountVal.text = "";
          frmIBMFSelPurchaseFundsEntry.txtAmountVal.text.placeholder = "0.00";
        }
      }
    }
  }

}

function frmIBMFSelPurchaseFundsEntry_onClickConfirm(){ 
  checkValidAmountIB();

}
function callingEmptyFunction()
{}
function preShowfrmIBMFCompleteNew(){ 
   var locale = kony.i18n.getCurrentLocale(); // MKI, MIB-9082/11148
   frmIBMFComplete.ImageStep.src = (locale == "th_TH") ? "step4th.png" : "step4.png"; // MKI, MIB-9082/11148
}
function preShowfrmIBMFComplete(){
  var locale = kony.i18n.getCurrentLocale();
  syncIBMFComplete();
  
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    frmIBMFComplete.ImageFund.src = frmIBMFSelPurchaseFundsLP.ImageFundHouse.src;
    frmIBMFComplete.HBoxMFSettleDate.setVisibility(false);
    
    frmIBMFComplete.LinkPurchase.skin = "linkBlack114RightBroder"; 
    frmIBMFComplete.LinkPurchase.hoverSkin = "linkBlack114RightBroder"; 
	frmIBMFComplete.LinkRedeem.skin = "linkBlue114RightBorder"; 
	frmIBMFComplete.LinkRedeem.hoverSkin = "linkBlue114RightBorderUnderlineFont";
    
    frmIBMFComplete.LinkRedeem.onClick = onClickRedeemIBMenu ; //MKI, references: MIB 9805
    frmIBMFComplete.LinkPurchase.onClick = callingEmptyFunction; //MKI, references: MIB 9805
  } else if (gblMFEventFlag == MF_EVENT_REDEEM) {
    frmIBMFComplete.ImageFund.src = frmIBMFSelRedeemFund.ImageFundHouse.src;
    frmIBMFComplete.HBoxMFSettleDate.setVisibility(true);
    frmIBMFComplete.LabelSettleDateVal.text = "T + " + gblFundRulesData["timeToRed"];
    
    frmIBMFComplete.LinkPurchase.skin = "linkBlue114RightBorder"; 
	frmIBMFComplete.LinkPurchase.hoverSkin = "linkBlue114RightBorderUnderlineFont"; 
	frmIBMFComplete.LinkRedeem.skin = "linkBlack114RightBroder"; 
	frmIBMFComplete.LinkRedeem.hoverSkin = "linkBlack114RightBroder"; 
    
    frmIBMFComplete.LinkRedeem.onClick = callingEmptyFunction ; //MKI, references: MIB 9805
    frmIBMFComplete.LinkPurchase.onClick = onClickPurchaseIBMenu ; //MKI, references: MIB 9805
  }
 
  frmIBMFComplete.lblunitholderval.text = formatUnitHolderNumer(gblMFOrder["unitHolderNo"]);
  frmIBMFComplete.lblfundcodeval.text = gblMFOrder["fundShortName"];
  frmIBMFComplete.lblfunamefullval.text = locale == "th_TH" ? gblMFOrder["fundNameTH"] : gblMFOrder["fundNameEN"];

  if(gblMFOrder["redeemUnit"] == ORD_UNIT_ALL || gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT) {
    amountUnit = kony.i18n.getLocalizedString("MF_RD_lbl_unit");
  } else {
    amountUnit = kony.i18n.getLocalizedString("currencyThaiBaht");
  }
  frmIBMFComplete.lblAmountVal.text = commaFormatted(gblMFOrder["amount"]) + " " + amountUnit;

  if(gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM){
    frmIBMFComplete.HBox0f8605226cf804c.setVisibility(false);
    if(GBL_MF_TEMENOS_ENABLE_FLAG != "ON" && gblMFOrder["fundHouseCode"] == "TMBAM") {
      frmIBMFComplete.HBoxPaymentMethod.setVisibility(true);
      if(gblMFOrder["paymentType"] != undefined || gblMFOrder["paymentType"] != ""){
        if(gblMFOrder["paymentType"] == "TR") {
          frmIBMFComplete.lblpaymentmethodval.text = kony.i18n.getLocalizedString("RD_lbl_transfer") + gblMFOrder["paymentDetail"];
        }else if (gblMFOrder["paymentType"] == "CQ"){
          frmIBMFComplete.lblpaymentmethodval.text = kony.i18n.getLocalizedString("RD_lbl_cheque");
        }
      } else {
        frmIBMFComplete.HBoxPaymentMethod.setVisibility(false);
      }
    } else {
      frmIBMFComplete.HBoxPaymentMethod.setVisibility(false);
    }
  } else if(gblMFOrder["orderType"] == MF_ORD_TYPE_PURCHASE){
    frmIBMFComplete.HBox0f8605226cf804c.setVisibility(true);
    frmIBMFComplete.HBoxPaymentMethod.setVisibility(false);
  }

  frmIBMFComplete.lblTransRefVal.text = gblMFOrder["refID"];
  frmIBMFComplete.lblTranOrdDateVal.text = formatDateMF(gblMFOrder["orderDate"]) + " " + gblMFOrder["orderTime"];
  frmIBMFComplete.lblEffDateVal.text = formatDateMF(gblMFOrder["effectiveDate"]);
}

function onClickViewPortfolio(){
  callMutualFundsSummary();
}
function onSelectFromAccntMufutalFundIB() {
  showLoadingScreen();
  var inputParam = {}
  inputParam["billPayInd"] = "billPayInd";
  invokeServiceSecureAsync("customerAccountInquiry", inputParam, mutualFundCustomerAccountCallBackIB);
}


function mutualFundCustomerAccountCallBackIB(status, resulttable) {
  if (status == 400) //success responce
  {
    if (resulttable["opstatus"] == 0) {
      /** checking for Soap status below  */
      //	frmIBMFSelPurchaseFundsLP.hbxImage.setVisibility(false);

      frmIBMFSelPurchaseFundsLP.segselAccnt.setVisibility(true);

      var StatusCode = resulttable["statusCode"];
      var fromData = []
      var j = 1
      gbltranFromSelIndex = [0, 0];
      if (resulttable.custAcctRec != null) {
        gblNoOfFromAcs = resulttable.custAcctRec.length;
        var nonCASAAct = 0;
        for (var i = 0; i < resulttable.custAcctRec.length; i++) {
          //
          //fiident	
          var accountStatus = resulttable["custAcctRec"][i].acctStatus;
          if (accountStatus.indexOf("Active") == -1) {
            nonCASAAct = nonCASAAct + 1;

          }
          if (accountStatus.indexOf("Active") >= 0) {
            if (resulttable.custAcctRec[i].personalisedAcctStatusCode == "02") {

              continue;
            }
            var icon = "";
            icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable.custAcctRec[i]["ICON_ID"] + "&modIdentifier=PRODICON";
            if (j == 1) {
              var temp = createSegmentRecordIB(resulttable.custAcctRec[i], icon);
            } else if (j == 2) {
              var temp = createSegmentRecordIB(resulttable.custAcctRec[i], icon);
            } else if (j == 3) {
              var temp = createSegmentRecordIB(resulttable.custAcctRec[i], icon);
              j = 0;
            }
            j++;
            var jointActXfer = resulttable.custAcctRec[i].partyAcctRelDesc;
            if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {} else {
              kony.table.insert(fromData, temp[0])
            }
          }
          //
        }
      } //for
      //frmIBBillPaymentCW.customIBBPFromAccount.data = fromData;
      if (nonCASAAct == resulttable.custAcctRec.length) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
        return false;
      }
      if (fromData.length == 0) {
        //alert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"));
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
        dismissLoadingScreenPopup();
        return;
      } else {
        frmIBMFSelPurchaseFundsLP.segselAccnt.data = fromData;
      }
      //frmIBBillPaymentCW.customIBBPFromAccount.data = fromData;
      dismissLoadingScreenPopup();

      frmIBMFSelPurchaseFundsLP.segselAccnt.onSelect=customWidgetSelectEventMutualFundIB;

      preShowFrmIBMFSelPurchaseFundsLP();

      frmIBMFSelPurchaseFundsLP.show();



    } else if (resulttable["opstatus"] == 1) {
      showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
    } else {
      dismissLoadingScreenPopup();
      alert(" " + resulttable["errMsg"]);
    }
  } //status 400
}

function customWidgetSelectEventMutualFundIB() {
  var locale = kony.i18n.getCurrentLocale();
  var selectedItem = frmIBMFSelPurchaseFundsLP.segselAccnt.selectedItem;
  var selectedData = frmIBMFSelPurchaseFundsLP.segselAccnt.data[gblCWSelectedItem];
  //frmIBMFSelPurchaseFundsLP.lblBPFromNameRcvd.text = selectedData.lblCustName;
  accntNme = selectedData.AccountName
  gblNickNameEN = selectedData.lblAccountNickNameEN;
  gblNickNameTH = selectedData.lblAccountNickNameTH;
  gblProductNameEN = selectedData.lblProductValEN;
  gblProductNameTH = selectedData.lblProductValTH;
  gblAccountNoval = selectedData.lblActNoval;
  gblAccountType=selectedData.lblSliderAccN2;

  gblMFOrder["acctNickNameEN"] = gblNickNameEN;
  gblMFOrder["acctNickNameTH"] = gblNickNameTH;

  avaiLength = selectedData.lblBalance.length;
  avaiAmount = selectedData.lblBalance.substring(0, avaiLength - 2);
  gblMFOrder["avaiAmount"] = parseFloat(removeCommos(avaiAmount));
  preShowFrmIBMFSelPurchaseFundsEntry();
  frmIBMFSelPurchaseFundsEntry.show();
}


function masterBillerInqMF() {
  showIBLoadingScreen();
  var billerGroupType = "";
  billerGroupType = "2";
  var inputParams = {
    IsActive: "1",
    BillerGroupType: billerGroupType,
    clientDate: getCurrentDate(),
    flagBillerList : "IB"
  };

  invokeServiceSecureAsync("masterBillerInquiry", inputParams, masterBillerInqMFCallback);
}

MFtoAccountKey="";
MFBillerCompcode="";
function masterBillerInqMFCallback(status, callBackResponse) {
  var toAccountKey="";
  var BillerCompcode="";
  if (status == 400) {
    if (callBackResponse["opstatus"] == "0") {

      var myBillList = callBackResponse["MasterBillerInqRs"];
      if (myBillList.length > 0) {
        var fundHouseCode = gblMFOrder["fundHouseCode"];
        if(GBL_MF_TEMENOS_ENABLE_FLAG != "ON" && fundHouseCode == "TMBAM"){ // mki, MIB-11710
          fundHouseCode = gblMFOrder["fundCode"];
        }	
        for (var i = 0; i < myBillList.length; i++) {

          if ((myBillList[i]["BillerShortName"] == fundHouseCode) && (myBillList[i]["BillerNameTH"] == fundHouseCode)) {
            MFtoAccountKey = myBillList[i]["ToAccountKey"];
            MFBillerCompcode = myBillList[i]["BillerCompcode"];
            gblMFOrder["MFtoAccountKey"] = myBillList[i]["ToAccountKey"];
            gblMFOrder["MFBillerCompcode"] = myBillList[i]["BillerCompcode"];
            break;
          }
        }
        //alert("vaules are"+toAccountKey+" "+BillerCompcode);
        //frmIBMFConfirm.show();
        callMFOrderValidation();
        dismissIBLoadingScreen();
      } else {		
        dismissIBLoadingScreen();
      }
    } else {
      dismissIBLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  } else {
    if (status == 300) {
      dismissIBLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  }
}
function frmIBMFConfirm_onEditMFOrder(){
  if(gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM){
    frmIBMFSelRedeemFund.show();


  } else if(gblMFOrder["orderType"] == MF_ORD_TYPE_PURCHASE){
    frmIBMFSelPurchaseFundsEntry.show();
  }
}


function frmIBMFSelPurchaseFundsEntry_EnterAmnt_onBeginEditing(eventobject, changedtext) {
  var enteredAmount = frmIBMFSelPurchaseFundsEntry.txtAmountVal.text;
  if (isNotBlank(enteredAmount)) {
    enteredAmount = kony.string.replace(enteredAmount, ",", "");
    if (!(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0)) {
      frmIBMFSelPurchaseFundsEntry.txtAmountVal.text = commaFormattedTransfer(enteredAmount);
    }
  }
  //frmIBMFSelPurchaseFundsEntry.hbxenteramnt.skin = "hbxProperFocus40px";
}

function frmIBMFSelPurchaseFundsLPClickNext(){
  showAlert(kony.i18n.getLocalizedString("MF01_002"), kony.i18n.getLocalizedString("info"));
}

function loadChartIB() {
  var tarray = [];
  var locale = kony.i18n.getCurrentLocale();
  var index = 0;
  var lblHead = "";
  if(gblMFSummaryData["FundClassDS"] == undefined || gblMFSummaryData["FundClassDS"].length == 0) {
    return;
  }
  var colorInx = 0;
  var colorChart = [];
  for (i = 100; i <= 900; i = i+100) {  //MKI, MIB-11485

    if(gblFundClassPercentageData[i] != 0){
      for (j = 0; j < gblMFSummaryData["FundClassDS"].length; j++) { 
        if(gblMFSummaryData["FundClassDS"][j]["FundClassCode"] == i){
          if(locale == "th_TH") {
            lblHead = gblMFSummaryData["FundClassDS"][j]["FundClassNameTH"]
          } else {
            lblHead = gblMFSummaryData["FundClassDS"][j]["FundClassNameEN"]
          }
        }
      }

      if(i == 100)
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#BFBFBF';//'#6F529B';
      }
      else if(i == 200)
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#7F7F7F';//'#30A4B1';
      }
      else if(i == 300)
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#B4C7E7';//'#272838';
      }
      else if(i == 400)
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#0070C0';//'#989FCE';
      }
      else if(i == 500)
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#F19E65';//'#347FC4';
      }
      else if(i == 600)
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#C55A11';//'#91AEC1';
      }
      else if(i == 700)
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#FFD966';
      }
       else if(i == 800) //MKI, MIB-11485 start
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#000066';
      }
      else if(i == 900)
      {
        tarray.push({
          name: lblHead,
          percent: gblFundClassPercentageData[i]});
        colorChart[colorInx++] = '#0f45bd'; //MKI, MIB-11485 End
      }
    }
  } 
  for (i = 0; i < tarray.length ;  i++) { 

    if(i == 0)
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    }
    else if(i == 1)
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    }
    else if(i == 2)
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    }
    else if(i == 3)
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    }
    else if(i == 4)
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    }
    else if(i == 5)
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    }
    else if(i == 6)
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    }
    else if(i == 7) //MKI, MIB-11485 start
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    }
    else if(i == 8)
    {
      tarray[i]["name"] = tarray[i]["name"].substring(0, 20);
    } //MKI, MIB-11485 End

  } 
  var t = tarray,
      e = d3.layout.pie().value(function(t) {
        return t.percent
      }).sort(null).padAngle(.01),
      n = 300,
      a = 300,
      r = n / 2,
      l = 100,
      i = d3.scale.ordinal().range(colorChart);
  //i = d3.scale.category10(),
  d = d3.svg.arc().outerRadius(r).innerRadius(l),
    o = d3.select("#chart").append("svg").attr({
    width: n,
    height: a,
    "class": "shadow"
  }).append("g").attr({
    transform: "translate(" + n / 2 + "," + a / 2 + ")"
  }),
    s = o.selectAll("path").data(e(t)).enter().append("path").attr({
    d: d,
    fill: function(t, e) {
      return i(t.data.name)
    }
  });
  s.transition().duration(1e3).attrTween("d", function(t) {
    var e = d3.interpolate({
      startAngle: 0,
      endAngle: 0
    }, t);
    return function(t) {
      return d(e(t))
    }
  });
  var c = function() {
    var n = (o.selectAll("text").data(e(t)).enter().append("text").transition().duration(200).attr("transform", function(t) {
      return "translate(" + d.centroid(t) + ")"
    }).attr("dy", ".4em").attr("text-anchor", "middle").text(function(t) {
      return t.data.percent != 0 ? t.data.percent + "%" : ""
    }).style({
      fill: "#fff",
      "font-size": "18px",
      "font-family":'DB Ozone X'
    }), 11),
        a = 7,
        r = n + a,
        l = o.selectAll(".legend").data(i.domain()).enter().append("g").attr({
          "class": "legend",
          transform: function(t, e) {
            return "translate(-55," + (e * r - 70) + ")"
          }
        });
    l.append("rect").attr({
      width: 10,
      height: 10,
      rx: 0,
      ry: 0
    }).style({
      fill: i,
      stroke: i
    }), l.append("text").attr({
      x: 20,
      y: 10
    }).text(function(t) {
      return t
    }).style({
      fill: "#929DAF",
      "font-size": "14px",
      "font-family":'DB Ozone X'
    })
  };
  setTimeout(c, 1e3)
}

function deleteTagforChartIBMutualFund() {
  if (document.getElementById('frmIBMutualFundsPortfolio_hbxChart')) {
    var existingelement = document.getElementById('frmIBMutualFundsPortfolio_hbxChart');
    var existingchart = document.getElementById('chart');
    if (existingchart == null) {} else {
      existingelement.removeChild(existingchart);
    }
    var newNode = document.createElement('div');
    newNode.id = 'chart';
    newNode.className = 'chart-container';
    existingelement.appendChild(newNode);
    var scriptTag = document.createElement('script');
    scriptTag.onload = loadChartIB();
    newNode.appendChild(scriptTag);
    var col_wrapper = document.getElementById("frmIBMutualFundsPortfolio_hbxChart").getElementsByTagName("div");
    var elementsToRemove = [];
    for (var i = 0; i < col_wrapper.length; i++) {
      if (col_wrapper[i].className.toLowerCase() == "kcell kwt100") {
        elementsToRemove.push(col_wrapper[i]);
      }
    }
    for (var i = 0; i < elementsToRemove.length; i++) {
      elementsToRemove[i].parentNode.removeChild(elementsToRemove[i]);
    }
  }
}

function checkValidAmountIB(){
  if(gblMFEventFlag == MF_EVENT_PURCHASE){
    checkValidPurchaseAmountIB();
  } else if(gblMFEventFlag == MF_EVENT_REDEEM){
    checkValidRedeemAmountIB();
  }
}

function checkValidPurchaseAmountIB(){
  var amount = 0.0;
  var avaiAmount = 0.0;
  var errMsg = "";
  if(gblMFOrder["fromAccountNo"] == ""){
    showAlert(kony.i18n.getLocalizedString("MF01_002"), kony.i18n.getLocalizedString("info"));
  } else {
    if(frmIBMFSelPurchaseFundsEntry.txtAmountVal.text != ""){
      amount = parseFloat(removeCommos(frmIBMFSelPurchaseFundsEntry.txtAmountVal.text));
    }
    avaiAmount = gblMFOrder["avaiAmount"];

    if(amount > 0.0){
      errMsg = validatePurchaseOrder(amount, avaiAmount);

      if(errMsg != ""){
        showAlert(errMsg, kony.i18n.getLocalizedString("info"));
        frmIBMFSelPurchaseFundsEntry.txtAmountVal.text = "";
        frmIBMFSelPurchaseFundsEntry.txtAmountVal.setFocus(true);
      } else {

        gblMFOrder["amount"] = numberFormat(amount, 2);
        gblChannel = "IB";
        if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
          checkValidFundRuleIB();
        } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
          frmIBMFTnC.show();
        }
      }
    } else {
      showAlert(kony.i18n.getLocalizedString("MF01_003"), kony.i18n.getLocalizedString("info"));
    }
  }
}

function checkValidRedeemAmountIB(){
  var orderUnit = 0.00; 
  var nav = 0.0;
  var errMsg = "";
  var avaiUnit = parseFloat(gblMFOrder["redeemableUnit"]);
  var avaiAmount = parseFloat(gblMFDetailsResulttable["InvestmentValue"]);

  if(frmIBMFSelRedeemFund.txtAmount.text == ""){
    orderUnit = 0.00;
  } else {
    orderUnit = parseFloat(removeCommos(frmIBMFSelRedeemFund.txtAmount.text));
  }

  if(gblMFDetailsResulttable["Nav"] != ""){
    nav = parseFloat(gblMFDetailsResulttable["Nav"]);
    avaiAmount = parseFloat(gblMFOrder["redeemableUnit"]) * nav;
  }

  if(gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT){
    gblMFOrder["amount"] = numberFormat(orderUnit, 4);
    errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_UNIT,nav);
  } else if(gblMFOrder["redeemUnit"] == ORD_UNIT_BAHT){
    gblMFOrder["amount"] = numberFormat(orderUnit, 2);
    errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_BAHT,nav);
  } else{
    gblMFOrder["amount"] = numberFormat(orderUnit, 4);
    errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_ALL,nav);
  }

  if(errMsg == ""){
    gblChannel = "IB";
    if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
      checkValidFundRuleIB();
    } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
      frmIBMFTnC.show();
    }
  } else {
    showAlert(errMsg, kony.i18n.getLocalizedString("info"));
  }
}
function checkValidFundRuleIB(){ 
  //MF_FUNDSUMMARY_FLOW
  var amount = 0.0;
  if(gblFundRulesData["effectiveDate"] != undefined && gblFundRulesData["transactionDate"] != undefined) {
    effectiveDate = gblFundRulesData["effectiveDate"].substring(0,10);
    transactionDate = gblFundRulesData["transactionDate"].substring(0,10);
  }
  if(gblMFEventFlag == MF_EVENT_PURCHASE && gblFundRulesData["suitabilityExpireFlag"] == 'Y') {
    showAlert(kony.i18n.getLocalizedString("MF01_001"), kony.i18n.getLocalizedString("info"));
  } else if(gblFundRulesData["allowTransFlag"] == '3'){ //MKI, MIB-12551
        showAlert(kony.i18n.getLocalizedString("MF_ERR_Overcutofftime"), kony.i18n.getLocalizedString("info")); //MKI, MIB-12551
  } 
  else if(gblFundRulesData["allowTransFlag"] != '0'){
    showAlert(kony.i18n.getLocalizedString("MF_ERR_cutofftime"), kony.i18n.getLocalizedString("info"));
  } else if(gblMFEventFlag == MF_EVENT_REDEEM && 
            (gblFundRulesData["allotType"] == '2' || gblFundRulesData["allotType"] == '3')){
    showAlert(kony.i18n.getLocalizedString("MF_ERR_notallow_order"), kony.i18n.getLocalizedString("info"));
  } else if(gblFundRulesData["fundCode"] == "") {
    showAlert("Cannot check fund suitability", kony.i18n.getLocalizedString("info"));
  } else if(effectiveDate != transactionDate){ 
    popupConfirmMutualFundOrderIB();
  } else {  
    gblChannel = "IB";
    frmIBMFTnC.show();
  }
}

function frmIBMFSelPurchaseFundsEntryOnClickSearchFund(){
  frmIBMFSelPurchaseFundsEntry.TextFieldSearch.text = "";
  frmIBMFSelPurchaseFundsEntry.HBoxSearchBox.setVisibility(true);
  frmIBMFSelPurchaseFundsEntry.LabelSearch.skin = "lblIB20pxBlack";
  frmIBMFSelPurchaseFundsEntry.LineSearch.skin = "lineBlack";
  frmIBMFSelPurchaseFundsEntry.LabelFeatured.skin = "lblIB20pxBlue";
  frmIBMFSelPurchaseFundsEntry.LineFeatured.skin = "lineBlue";
  frmIBMFSelPurchaseFundsEntry.HBoxSegment.setVisibility(false);
  frmIBMFSelPurchaseFundsEntry.TextFieldSearch.setFocus(true);
  frmIBMFSelPurchaseFundsEntry.LineFeatured.thickness = 2;
  frmIBMFSelPurchaseFundsEntry.LineSearch.thickness = 3;
}
function frmIBMFSelPurchaseFundsEntryOnClickFeatured(){
  var fundHouseCode = "";
  if(gblMFOrder["fundHouseCode"] == undefined || gblMFOrder["fundHouseCode"] == ""){
    fundHouseCode = "";
  } else {
    fundHouseCode = gblMFOrder["fundHouseCode"];
  }
  setFundListForPurchase(fundHouseCode, "");
  frmIBMFSelPurchaseFundsEntry.TextFieldSearch.text = "";
  frmIBMFSelPurchaseFundsEntry.HBoxSearchBox.setVisibility(false);
  frmIBMFSelPurchaseFundsEntry.LabelSearch.skin = "lblIB20pxBlue";
  frmIBMFSelPurchaseFundsEntry.LineSearch.skin = "lineBlue";
  frmIBMFSelPurchaseFundsEntry.LabelFeatured.skin = "lblIB20pxBlack";
  frmIBMFSelPurchaseFundsEntry.LineFeatured.skin = "lineBlack";
  frmIBMFSelPurchaseFundsEntry.HBoxSegment.setVisibility(true);
  frmIBMFSelPurchaseFundsEntry.LineFeatured.thickness = 3;
  frmIBMFSelPurchaseFundsEntry.LineSearch.thickness = 2;
}

function frmIBMFSelPurchaseFundsEntryOnClickOnSegment(){

  var selectedIndex = frmIBMFSelPurchaseFundsEntry.SegmentFundList.selectedIndex[1];
  var fundDataObject = frmIBMFSelPurchaseFundsEntry.SegmentFundList.data[selectedIndex];
  var unitholder = "";
  var fundCode = "";
  var funClassCode = "";

  gblMFOrder["fundCode"] = fundDataObject.FundCode;
  gblMFOrder["fundHouseCode"] = fundDataObject.FundHouseCode;
  gblMFOrder["fundNickName"] = fundDataObject.LabelFundFullName;
  gblMFOrder["fundNameTH"] = fundDataObject.FundNameTH;
  gblMFOrder["fundNameEN"] = fundDataObject.FundNameEN;
  gblMFOrder["fundShortName"] = fundDataObject.FundShortName;
  gblMFOrder["allotType"] = fundDataObject.AllotType;

  gblMFOrder["unitHolderNo"] = "";

  frmIBMFSelPurchaseFundsEntry.show();
}


function frmIBMFSelPurchaseFundsEntryOnClickOnSegmentUnitHolder(){
  var selectedIndex = frmIBMFSelPurchaseFundsEntry.SegmentUnitHolderList.selectedIndex[1];
  var fundDataObject = frmIBMFSelPurchaseFundsEntry.SegmentUnitHolderList.data[selectedIndex];
  var unitholder = "";

  unitholder = fundDataObject.LabelUnitHolderNo.replace(/-/gi, "");
  gblMFOrder["unitHolderNo"] = unitholder;
  
  var APchar = unitholder.substring(0, 2);
  kony.print("APchar  "+APchar);
  kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
  var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
  if(AS_prefix == APchar){
	alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
    return;
  }
  
  getFundRulePurchaseOnSelectedIndex();
}

function frmIBMFSelPurchaseFundsEntryOnClickSelectFund(){
  if(frmIBMFSelPurchaseFundsEntry.VBoxArrowFund.isVisible){
    gblMFOrder["fundCode"] = "";
    gblMFOrder["fundHouseCode"] = "";
    frmIBMFSelPurchaseFundsEntry.show();
  }

}

function frmIBMFSelPurchaseFundsEntryOnClickSelectUnitHolder(){
  if(frmIBMFSelPurchaseFundsEntry.VBoxArrowUnitHolder.isVisible){
    gblMFOrder["unitHolderNo"] = "";
    frmIBMFSelPurchaseFundsEntry.show();
  }
}

function frmIBMFSelPurchaseFundsOnSearchFund(){
  var searchText = frmIBMFSelPurchaseFundsEntry.TextFieldSearch.text;
  var fundHouseCode = gblMFOrder["fundHouseCode"];
  if(fundHouseCode == kony.i18n.getLocalizedString("MF_RDM_20") || fundHouseCode == "" || fundHouseCode == undefined){
    fundHouseCode = "";
  }
  if(searchText.length >= 3){
    setFundListForPurchase(fundHouseCode, searchText);
    frmIBMFSelPurchaseFundsEntry.HBoxSegment.setVisibility(true)
  } 
}
function frmIBMFSelPurchaseFundsEntryOnClickClear(){
  frmIBMFSelPurchaseFundsEntry.TextFieldSearch.text = "";
  frmIBMFSelPurchaseFundsEntry.SegmentFundList.removeAll();
}

function frmIBMFSelPurchaseFundsEntryOnSelectFundHouse(){
  setFundHouseListForPurchase();
  popupIBSelectFundHouseMF.show();
}

function popupIBSelectFundHouseMFOnSelectFundHouse(){
  var locale = kony.i18n.getCurrentLocale();
  var selectedIndex = popupIBSelectFundHouseMF.SegmentFundHouse.selectedIndex[1];
  var fundDataObject = popupIBSelectFundHouseMF.SegmentFundHouse.data[selectedIndex];
  var fundHouseName = fundDataObject.LabelFundHouse;
  var fundHouseCode = fundDataObject.fundHouseCode;
  var fundimg = fundDataObject.ImageFund;

  if(fundimg != "") {
    gblMFOrder["fundHouseCode"] = fundHouseCode;
    setFundListForPurchase(fundHouseCode,"");
    frmIBMFSelPurchaseFundsEntry.LabelFundHouseVal.text = fundHouseName;
  } else {
    gblMFOrder["fundHouseCode"] = "";
    setFundListForPurchase("","");
    frmIBMFSelPurchaseFundsEntry.LabelFundHouseVal.text = kony.i18n.getLocalizedString("MF_RDM_20");
  }

  popupIBSelectFundHouseMF.dismiss();
}

function frmIBMFSelRedeemFundInit(){
  frmIBMFSelRedeemFund.txtAmount.onKeyUp = frmIBMFSelRedeemFundAmountOnKeyUp;
  frmIBMFSelRedeemFund.txtAmount.onEndEditing = frmIBMFSelRedeemFundAmountOnEndEditing;
}

function frmIBMFTncOnClickLinkFundFact(){
  kony.application.openURL(kony.i18n.getLocalizedString("keyMFFundFactSheet"));
}

function updateFundListNameForPurchase(){
  var locale = kony.i18n.getCurrentLocale();
  var dataFund=[];
  if(frmIBMFSelPurchaseFundsEntry.SegmentFundList.data.length > 0){
    for(var i in frmIBMFSelPurchaseFundsEntry.SegmentFundList.data){
      var fundNickName = "";
      var temp = frmIBMFSelPurchaseFundsEntry.SegmentFundList.data[i];
      if(locale == "th_TH") {
        fundNickName = frmIBMFSelPurchaseFundsEntry.SegmentFundList.data[i].FundNameTH;
      } else {
        fundNickName = frmIBMFSelPurchaseFundsEntry.SegmentFundList.data[i].FundNameEN;
      }
      temp["LabelFundFullName"] = fundNickName;

      dataFund.push(temp);
    }
    frmIBMFSelPurchaseFundsEntry.SegmentFundList.removeAll();
    frmIBMFSelPurchaseFundsEntry.SegmentFundList.setData(dataFund);
  }
}

/*----- This service called for Redeem to work 
when suitability status expired MIB-14501 ----*/
function callMutualFundsSummaryReedem(){  
  showLoadingScreenPopup();
  var inputParam = {};
  currMFselectedInd = 1;
  invokeServiceSecureAsync("MFUHAccountSummaryInq", inputParam, callMutualFundsSummaryCallBackRedeem);
}
function callMutualFundsSummaryCallBackRedeem(status,resulttable){
  if (status == 400) {
    if (resulttable["opstatus"] == 0){
		dismissLoadingScreenPopup();
		gblMFSummaryData=resulttable;
	}
  }
}
function onClickIBRedeem(){
  callMutualFundsSummaryReedem();
	var endTime = gblMFSummaryData["MF_EndTime"];
    var startTime = gblMFSummaryData["MF_StartTime"];
    if (gblMFSummaryData["MF_BusinessHrsFlag"] != null && gblMFSummaryData["MF_BusinessHrsFlag"] == "false") {
        var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
        messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
        messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
        showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
    } else {
      	 kony.print("Inside Normal flow ");
      	if(gblIsFundCodeNull){ // mki, MIB- 10003 = start
          showAlert( kony.i18n.getLocalizedString("MF_P_ERR_01001"), kony.i18n.getLocalizedString("info"));
        }
      	else {
        	onClickRedeemIBMenu();
        }// mki, MIB- 10003 = End
    }
}
function onClickIBPurchase(){
	var endTime = gblMFSummaryData["MF_EndTime"];
    var startTime = gblMFSummaryData["MF_StartTime"];
    if (gblMFSummaryData["MF_BusinessHrsFlag"] != null && gblMFSummaryData["MF_BusinessHrsFlag"] == "false") {
        kony.print("Inside Business fail flow");
        var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
        messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
        messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
        showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
    } else {
        kony.print("Inside Normal flow ");
      	if(gblIsFundCodeNull){ // mki, MIB- 10003 = start
          showAlert( kony.i18n.getLocalizedString("MF_P_ERR_01001"), kony.i18n.getLocalizedString("info"));
        }
      	else {
        	onClickPurchaseIBMenu();
        }// mki, MIB- 10003 = End
    }
}
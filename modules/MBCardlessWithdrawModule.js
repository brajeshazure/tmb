var gblCardlessFromSelIndex = "";
var gblCardlessFromSelAcctId = "";
function loadFromAccountForCardlessWD(AcctId) {
	gblSelTransferMode = 1;
	showLoadingScreen();
	if(checkMBUserStatus()){
		isMenuShown = false;
		gblRetryCountRequestOTP = "0";
		gblPaynow = true;
		GBLFINANACIALACTIVITYLOG = {}
		var inputParam = {}
		inputParam["cardLessFlag"] = "true";
		gblCardlessFromSelAcctId = "";
      	if(typeof(AcctId) != "undefined"){
			gblCardlessFromSelAcctId = 	AcctId;
        }
        invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackCardlessWD);

    }
}

function getCardlessWithdrawIndex() {}
	getCardlessWithdrawIndex.prototype.paginationSwitch = function (sectionIndex, rowIndex) {
	var segdata = frmCardlessWithdrawSelectAccount.segNSSlider.data;
	rowIndex = parseFloat(rowIndex);
	frmCardlessWithdrawSelectAccount.segNSSlider.selectedIndex = [0, rowIndex];
	gblCardlessFromSelIndex = frmCardlessWithdrawSelectAccount.segNSSlider.selectedIndex;
	gblCardlessWithDrawFromAcct = frmCardlessWithdrawSelectAccount.segNSSlider.selectedItems[0];
}

function callBackCardlessWD(status, resulttable){ //1224 ModAccountSummaryCallService
	if (status == 400) {	
		if (resulttable["opstatus"] == 0) {
			deviceContactRefreshServerValue = resulttable["DEVICE_CONTACT_REFRESH_VALUE"];
			var setyourIDTransfer = resulttable["setyourIDTransfer"];
			GLOBAL_TODAY_DATE = resulttable["TODAY_DATE"];
			var fromData = [];
			if(resulttable.custAcctRec.length == undefined ){
				dismissLoadingScreen();
				alert("Accounts are not eligible for Transfer or either Hidden");
				return;
			}
			gblSelectedRecipentName=gblCustomerName;
			var j = 1
			gblXerSplitData = [];
			var temp1 = [];
			gblXerSplitData = temp1;
			var nonCASAAct = 0;
			gblCardlessWithdrawOtherAccounts = [];
			for (var i = 0; i < resulttable.custAcctRec.length; i++) {
				var accountStatus = resulttable["custAcctRec"][i].acctStatus;
				var anyIDEligible = true;

				if(accountStatus.indexOf("Active") == -1){
					nonCASAAct = nonCASAAct + 1;
				}
				var joinType = resulttable.custAcctRec[i].partyAcctRelDesc;
				var AccStatCde = resulttable.custAcctRec[i].personalisedAcctStatusCode;
				
				if (AccStatCde == undefined || AccStatCde == "01" ||  AccStatCde == ""){
					if (joinType == "PRIJNT" || joinType == "SECJAN" || joinType == "OTHJNT" || joinType == "SECJNT") {

					} else {
						if(anyIDEligible && accountStatus.indexOf("Active") >= 0){
							var icon = "";
							var iconcategory = "";	
							var temp;						
							icon="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+"NEW_"+resulttable.custAcctRec[i]["ICON_ID"]+"&modIdentifier=PRODICON";	
							iconcategory=resulttable.custAcctRec[i]["ICON_ID"];
							// added new From account widget
							if (iconcategory=="ICON-01"||iconcategory=="ICON-02") {
								var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew1, icon);
							} else if (iconcategory=="ICON-03") {
								var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew2, icon);
							} else if (iconcategory=="ICON-04") {
								var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew3, icon);
							  
							}
							kony.table.insert(fromData, temp[0]);
						}
					}
				}
			}
			if(nonCASAAct==resulttable.custAcctRec.length)
			{
				showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
				return false;
			}
			if(gblDeviceInfo["name"] == "android"){
				if(fromData.length == 1){
					frmCardlessWithdrawSelectAccount.segNSSlider.viewConfig = {
						"coverflowConfig": {
							"rowItemRotationAngle": 0,
							"isCircular": false,
							"spaceBetweenRowItems": 10,
							"projectionAngle": 90,
							"rowItemWidth": 80
						}
					};
				}else{
					frmCardlessWithdrawSelectAccount.segNSSlider.viewConfig = {
					"coverflowConfig": {
						"rowItemRotationAngle": 0,
						"isCircular": true,
						"spaceBetweenRowItems": 10,
						"projectionAngle": 90,
						"rowItemWidth": 80
						}
					};
				}
			}
			frmCardlessWithdrawSelectAccount.segNSSlider.widgetDataMap = {
				lblACno: "lblACno",
				lblAcntType: "lblAcntType",
				img1: "img1",
				lblCustName: "lblCustName",
				lblBalance: "lblBalance",
				lblActNoval: "lblActNoval",
				lblDummy: "lblDummy",
				lblSliderAccN1: "lblSliderAccN1",
				lblSliderAccN2: "lblSliderAccN2",
				lblRemainFee: "lblRemainFee",
				lblRemainFeeValue: "lblRemainFeeValue"
			}
			gblNoOfFromAcs = fromData.length;
			if(gblNoOfFromAcs == 0){
			   showAlert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"));
			   dismissLoadingScreen();
			   return false;
			}			
			var selectedIndex = 0;
			if(glb_accId.length==14){
				glb_accId=glb_accId.substring(4);
			}
			if(isNotBlank(glb_accId)){
				gblCardlessFromSelAcctId = glb_accId;
			}	
			frmCardlessWithdrawSelectAccount.segNSSlider.data = fromData;
          	if(gblCardlessFromSelAcctId == ""){
            	gblCardlessFromSelIndex = [0, 0];
                if(gblCardlessFromSelIndex == ""){
                    gblCardlessFromSelIndex = [0, 0];
                }
                frmCardlessWithdrawSelectAccount.segNSSlider.selectedIndex = gblCardlessFromSelIndex;
                gblCardlessWithDrawFromAcct = frmCardlessWithdrawSelectAccount.segNSSlider.selectedItems[0];
              	dismissLoadingScreen();
              	gblCardlessFlowCurrentForm = frmCardlessWithdrawSelectAccount;
                frmCardlessWithdrawSelectAccount.show();
                gbltdFlag = fromData[selectedIndex].tdFlag;
            }else{
              	var found = false;
            	for (var i = 0; i < gblNoOfFromAcs; i++) {
                	if(fromData[i]["accountNum"].replace(/-/g,"") == gblCardlessFromSelAcctId){
                    	gblCardlessWithDrawFromAcct = fromData[i];
                      	gblCardlessFromSelIndex = [0,i];
                      	frmCardlessWithdrawSelectAccount.segNSSlider.selectedIndex = gblCardlessFromSelIndex;
                      	found = true;
                      	break;
                    }
                }
              	dismissLoadingScreen();
              	if(found){
                  frmCardlessWithdrawSelectAccount.show();
                }
            }
		} else {
			dismissLoadingScreen();
			if(resulttable["errMsg"] == "No Valid Accounts Available for Cardless Withdraw"){
				alert(kony.i18n.getLocalizedString("TRErr_NoFromAcc"));
			}else{
				alert(" " + resulttable["errMsg"]);			
			}
		}
	}
}
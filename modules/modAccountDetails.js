allowApplySoGoood = "";

function onClickOfAccountDetailsMore() {
  if (frmAccountDetailsMB.linkMore.text == kony.i18n.getLocalizedString("More")) {
    frmAccountDetailsMB.linkMore.text = kony.i18n.getLocalizedString("Hide");
    frmAccountDetailsMB.hbxUseful7.isVisible = true;
    frmAccountDetailsMB.hbxUseful8.isVisible = true;
    if(!(frmAccountDetailsMB.lblUsefulValue9.text==undefined || frmAccountDetailsMB.lblUsefulValue9.text == "")){
      frmAccountDetailsMB.hbxUseful9.isVisible = true;
    }
    frmAccountDetailsMB.hbxUseful10.isVisible = true;
    if (glb_viewName != "PRODUCT_CODE_NOFIXED") {
      if(!(frmAccountDetailsMB.lblUsefulValue11.text==undefined || frmAccountDetailsMB.lblUsefulValue11.text == "")){
        frmAccountDetailsMB.hbxUseful11.isVisible = true;
      }
    }
    if (glb_viewName == "PRODUCT_CODE_NOFIXED" && (frmAccountDetailsMB.lblUsefulValue2.text).length == 0) {

      //frmAccountDetailsMB.hboxApplySendToSave.isVisible = true;
    }
    frmAccountDetailsMB.hbxUseful12.isVisible = true;
    frmAccountDetailsMB.hbxUseful13.isVisible = true;
    if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName ==
        "PRODUCT_CODE_NOFEESAVING_TABLE"||glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {

      frmAccountDetailsMB.segDebitCard.setVisibility(true);
      if (GLOBAL_DEBITCARD_TABLE != null && GLOBAL_DEBITCARD_TABLE != "") {
        var temp = [];
        var imageForDisplay="";

        for (var i = 0; i < GLOBAL_DEBITCARD_TABLE.length; i++) {
          imageForDisplay="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+GLOBAL_DEBITCARD_TABLE[i]["cardLogo"]+"&modIdentifier=AccountDetails";

          //imageForDisplay="debitcard.png"

          var segTable = {
            imgCreditCard: imageForDisplay,
            lblCreditCardNo: GLOBAL_DEBITCARD_TABLE[i]["maskedCardNo"],
            lblExpiryDate: kony.i18n.getLocalizedString("ExpiryDate") + "  " +GLOBAL_DEBITCARD_TABLE[i]["expiryDate"]
          }
          temp.push(segTable);
          //frmAccountDetailsMB.segDebitCard.setData(temp);
          //					frmAccountDetailsMB.segDebitCard.setVisibility(true);
          //					frmAccountDetailsMB.hbxLinkedTo.isVisible = true;
        }
        frmAccountDetailsMB.segDebitCard.setData(temp);
        frmAccountDetailsMB.segDebitCard.setVisibility(true);
        frmAccountDetailsMB.hbxLinkedTo.isVisible = true;
      }
    }
  } else {
    frmAccountDetailsMB.linkMore.text = kony.i18n.getLocalizedString("More");
    frmAccountDetailsMB.hbxUseful7.isVisible = false;
    frmAccountDetailsMB.hbxUseful8.isVisible = false;
    frmAccountDetailsMB.hbxUseful9.isVisible = false;
    frmAccountDetailsMB.hbxUseful10.isVisible = false;
    frmAccountDetailsMB.hbxUseful11.isVisible = false;
    frmAccountDetailsMB.hbxUseful12.isVisible = false;
    frmAccountDetailsMB.hbxUseful13.isVisible = false;
    frmAccountDetailsMB.segDebitCard.setVisibility(false);
    frmAccountDetailsMB.hbxLinkedTo.isVisible = false;
    frmAccountDetailsMB.hboxApplySendToSave.isVisible = false;
    frmAccountDetailsMB.hbxUseful11.isVisible = false;
    if (glb_viewName == "PRODUCT_CODE_NOFIXED" && (frmAccountDetailsMB.lblUsefulValue2.text).length == 0) {

      //frmAccountDetailsMB.hboxApplySendToSave.isVisible = true;
    }
  }
}

/*function onClickOfAccountDetailsBack() {    
   closeAllFlaps();
  var curr_form_name = kony.application.getCurrentForm().id;		//Added to fix blank screen issue on clicking of alert popups in Menu
	if(curr_form_name != "frmAccountSummaryLanding")
		TMBUtil.DestroyForm(frmAccountSummaryLanding);
	gaccType = "";
	glb_viewName = "";
	gblAccountTable = "";
	GLOBAL_DEBITCARD_TABLE = ""
	if(flowSpa){
	 isMenuShown = false
	 //frmAccountSummaryLanding = null;
//     frmAccountSummaryLandingGlobals();    
	 TMBUtil.DestroyForm(frmAccountSummaryLanding);
     gblAccountTable = "";
  	}
  	callCustomerAccountService()
	frmAccountSummaryLanding.lblAccntHolderName.text = gblCustomerName;
   kony.print("inside onClickOfAccountDetailsBack: "+displayRTPAnnoucement);
  if(frmAccountSummaryLanding.FlexMain.isVisible === false){
    kony.print("main is invisible @@");
  	frmAccountSummaryLanding.FlexMain.setVisibility(true);
  }
  if(frmAccountSummaryLanding.flxPopup.isVisible === true){
    kony.print("popup is visible @@");
  	frmAccountSummaryLanding.flxPopup.setVisibility(false);
  }
  if(displayRTPAnnoucement) {
    startUpRTPDisplay();
  } else {
    animation = true;
    frmAccountSummaryLanding.show();
    TMBUtil.DestroyForm(frmAccountDetailsMB);
  }

}*/

function simpleAccountFunction() {
  frmAccountDetailsMB.lblUseful8.text = "";
  frmAccountDetailsMB.lblUseful9.text = "";
  frmAccountDetailsMB.lblUseful10.text = "";
  frmAccountDetailsMB.lblUseful11.text = "";
  frmAccountDetailsMB.lblUseful12.text = "";
  frmAccountDetailsMB.lblUseful13.text = "";
  frmAccountDetailsMB.lblUseful14.text = "";
  frmAccountDetailsMB.lblUseful15.text = "";
  //Setting the links and lines
  frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
  frmAccountDetailsMB.flexLineStatement.setVisibility(true);
  frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
  //frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
  //frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
  //frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
  frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
  //frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
  frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
  frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
  //
  frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
  frmAccountDetailsMB.flexBenefiList.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  //
  frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
  frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
  
  var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
  frmAccountDetailsMB.lblUsefulValue3.text = selectedRow["lblAccountNumber"];  
  frmAccountDetailsMB.lblUsefulValue3.textCopyable=true;
  frmAccountDetailsMB.lblUsefulValue8.text = "";
  frmAccountDetailsMB.lblUsefulValue9.text = "";
  frmAccountDetailsMB.lblUsefulValue10.text = "";
  frmAccountDetailsMB.lblUsefulValue11.text = "";
  frmAccountDetailsMB.lblUsefulValue12.text = "";
  frmAccountDetailsMB.lblUsefulValue13.text = "";
  frmAccountDetailsMB.lblUsefulValue14.text = "";
  frmAccountDetailsMB.lblUsefulValue15.text = "";
  //frmAccountDetailsMB.lblLinkedDebit.text = kony.i18n.getLocalizedString("LinkedTo");
  moreAccountDetails();
}

function TDAccountFunction() {
  frmAccountDetailsMB.lblUseful10.text = "";
  frmAccountDetailsMB.lblUseful11.text = "";
  frmAccountDetailsMB.lblUseful12.text = "";
  frmAccountDetailsMB.lblUseful13.text = "";
  frmAccountDetailsMB.lblUseful14.text = "";
  frmAccountDetailsMB.lblUseful15.text = "";
  //frmAccountDetailsMB.lblLinkedDebit.text = "";
  //frmAccountDetailsMB.line4740217943851.setVisibility(true);
  //frmAccountDetailsMB.hbxSegHeader.isVisible = true;
  //frmAccountDetailsMB.lblDepositDetails.setVisibility(true);
  //Setting the links and lines
  frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
  frmAccountDetailsMB.flexLineStatement.setVisibility(true);
  frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
 // frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
  frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
  //frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
  frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
  frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
  //
  frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(true);
  frmAccountDetailsMB.flexMaturityDisplay.setVisibility(true);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(true);
  frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(true);
  //
  frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
  frmAccountDetailsMB.flexBenefiList.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  //
  //this is savings care container
  frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
  //
  //frmAccountDetailsMB.segDebitCard.setVisibility(false);
  //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_termdeposits.png";
  //var imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"]+"&modIdentifier=PRODICON";
  //frmAccountDetailsMB.imgAccountDetailsPic.src=imageName;
  frmAccountDetailsMB.lblUsefulValue4.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
  frmAccountDetailsMB.lblUsefulValue4.textCopyable=true;
  frmAccountDetailsMB.lblUsefulValue10.text = "";
  frmAccountDetailsMB.lblUsefulValue11.text = "";
  frmAccountDetailsMB.lblUsefulValue12.text = "";
  frmAccountDetailsMB.lblUsefulValue13.text = "";
  frmAccountDetailsMB.lblUsefulValue14.text = "";
  frmAccountDetailsMB.lblUsefulValue15.text = "";
  moreAccountDetails();

}

function noFeeSavingAccount() {
try{
  	 //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_nofee.png";
  frmAccountDetailsMB.lblUseful9.text = "";
  frmAccountDetailsMB.lblUseful10.text = "";
  frmAccountDetailsMB.lblUseful11.text = "";
  frmAccountDetailsMB.lblUseful12.text = "";
  frmAccountDetailsMB.lblUseful13.text = "";
  frmAccountDetailsMB.lblUseful14.text = "";
  frmAccountDetailsMB.lblUseful15.text = "";
  frmAccountDetailsMB.lblUsefulValue14.text = "";
  frmAccountDetailsMB.lblUsefulValue15.text = "";
  //Setting the links and lines
  frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
  frmAccountDetailsMB.flexLineStatement.setVisibility(true);
  frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
  //frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
  //frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
  //frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
  frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
  //frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
  frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
  frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
  //
  frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
  frmAccountDetailsMB.flexBenefiList.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  //
  frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
  frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
  //
  var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
  kony.print("selectedRow>>"+selectedRow);
  frmAccountDetailsMB.lblUsefulValue2.text = selectedRow.remainingFee;
    //gblAccountTable["custAcctRec"][gblIndex]["remainingFee"];
  frmAccountDetailsMB.lblUsefulValue4.text = selectedRow.lblAccountNumber
 
    //gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
  frmAccountDetailsMB.lblUsefulValue4.textCopyable=true;
  moreAccountDetails();
}catch(e){
  kony.print("Exception in noFeeSavingAccount >>"+e);
 }
 
}

function dreamSavingAccount() {

   //Setting the links and lines
  frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
  frmAccountDetailsMB.flexLineStatement.setVisibility(true);
  frmAccountDetailsMB.flexLineSavingsCare.setVisibility(true);
  frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(true);
  frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
 // frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
  frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
  //frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
  frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
  frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
  //
  frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
  frmAccountDetailsMB.flexBenefiList.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  //
  frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
  frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
  //
  frmAccountDetailsMB.lblUsefulValue7.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
  frmAccountDetailsMB.lblUsefulValue7.textCopyable=true;
  moreAccountDetails();
}

function noFixedAccount() {

  frmAccountDetailsMB.lblUseful12.text = "";
  frmAccountDetailsMB.lblUseful13.text = "";
  frmAccountDetailsMB.lblUseful14.text = "";
  frmAccountDetailsMB.lblUseful15.text = "";
  frmAccountDetailsMB.lblUsefulValue14.text = "";
  frmAccountDetailsMB.lblUsefulValue15.text = "";
  
  //Setting the links and lines
  frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
  frmAccountDetailsMB.flexLineStatement.setVisibility(true);
  frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
 // frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
  frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
  //frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
  frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
  frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
  //
  frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
  frmAccountDetailsMB.flexBenefiList.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);

  //
  frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
  var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
  frmAccountDetailsMB.lblUsefulValue5.text = selectedRow["lblAccountNumber"];
  frmAccountDetailsMB.lblUsefulValue5.textCopyable=true;
  moreAccountDetails();
}

function creditCardAccount() {
 
  frmAccountDetailsMB.lblUseful10.text = "";
  frmAccountDetailsMB.lblUseful11.text = "";
  frmAccountDetailsMB.lblUseful12.text = "";
  frmAccountDetailsMB.lblUseful13.text = "";
  //hiding the links and lines
  frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(false);
  frmAccountDetailsMB.flexLineStatement.setVisibility(false);
  frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);	
 
  frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
  frmAccountDetailsMB.flexBenefiList.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
  
  frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
  
  frmAccountDetailsMB.lblUsefulValue10.text = "";
  frmAccountDetailsMB.lblUsefulValue11.text = "";
  frmAccountDetailsMB.lblUsefulValue12.text = "";
  frmAccountDetailsMB.lblUsefulValue13.text = "";
  moreAccountDetails();
}

function cash2GoAccount() {
  //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_cash2go.png";
  frmAccountDetailsMB.lblUseful10.text = "";
  frmAccountDetailsMB.lblUseful11.text = "";
  frmAccountDetailsMB.lblUseful12.text = "";
  frmAccountDetailsMB.lblUseful13.text = "";
  //frmAccountDetailsMB.lblLinkedDebit.text = "";

  //rmAccountDetailsMB.lblLinkedDebit.text = "";
  //frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
  //frmAccountDetailsMB.flexBenefiList.setVisibility(false);

  //hiding the links and lines
  frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(false);
  frmAccountDetailsMB.flexLineStatement.setVisibility(false);
  frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
 // frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
  frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
  //frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
  frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
  frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
  //
  frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
  frmAccountDetailsMB.flexBenefiList.setVisibility(false);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
  frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
  frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
  //
  if (gblLoanAccountTable["loanAccounNo"] != "" && gblLoanAccountTable["loanAccounNo"] != null) {
    frmAccountDetailsMB.lblUsefulValue2.text = formatAccountNo(gblLoanAccountTable["loanAccounNo"].substring(17, 27));
    frmAccountDetailsMB.lblUsefulValue3.text = gblLoanAccountTable["loanAccounNo"].substring(27);
  }
  frmAccountDetailsMB.lblUsefulValue4.text = gblLoanAccountTable["accountName"]
  frmAccountDetailsMB.lblUsefulValue5.text = reformatDate(gblLoanAccountTable["nextPmtDt"]);
  if (gblLoanAccountTable["regPmtCurAmt"] != "" && gblLoanAccountTable["regPmtCurAmt"] != null) {
    frmAccountDetailsMB.lblUsefulValue6.text = commaFormatted(gblLoanAccountTable["regPmtCurAmt"]) + kony.i18n.getLocalizedString(
      "currencyThaiBaht");
  }
  if (gblLoanAccountTable["debitAccount"].length != 0) {
    frmAccountDetailsMB.lblUsefulValue9.text = formatAccountNo(gblLoanAccountTable["debitAccount"].substring(4));
  }
  else
  {
    frmAccountDetailsMB.lblUseful9.isVisible = false;
  }
  frmAccountDetailsMB.lblUsefulValue7.text = ""
  frmAccountDetailsMB.lblUsefulValue8.text = ""
  frmAccountDetailsMB.lblUsefulValue10.text = "";
  frmAccountDetailsMB.lblUsefulValue11.text = "";
  frmAccountDetailsMB.lblUsefulValue12.text = "";
  frmAccountDetailsMB.lblUsefulValue13.text = "";
  moreAccountDetails();
}

function homeLoanAccount() {
  //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_homeloan.png";
  frmAccountDetailsMB.lblUseful8.text = "";
  frmAccountDetailsMB.lblUseful9.text = "";	 
  frmAccountDetailsMB.lblUseful10.text = "";
  frmAccountDetailsMB.lblUseful11.text = "";
  frmAccountDetailsMB.lblUseful12.text = "";
  frmAccountDetailsMB.lblUseful13.text = "";
  frmAccountDetailsMB.lblUseful14.text = "";
  frmAccountDetailsMB.lblUseful15.text = "";
  frmAccountDetailsMB.lblUsefulValue8.text = "";
  frmAccountDetailsMB.lblUsefulValue9.text = "";	 
  frmAccountDetailsMB.lblUsefulValue10.text = "";
  frmAccountDetailsMB.lblUsefulValue11.text = "";
  frmAccountDetailsMB.lblUsefulValue12.text = "";
  frmAccountDetailsMB.lblUsefulValue13.text = "";
  frmAccountDetailsMB.lblUsefulValue14.text = "";
  frmAccountDetailsMB.lblUsefulValue15.text = "";
  //hiding the links
  frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(false);
  frmAccountDetailsMB.flexLineStatement.setVisibility(false);
  frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
  frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
  frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
  frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
 // frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
  frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
  //frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
  frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
  frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);	
  //
  frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(true);
  frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(true);
  frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(true);
  frmAccountDetailsMB.flexMaturityDisplay.setVisibility(true);
  frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
  frmAccountDetailsMB.flexBenefiList.setVisibility(false);
  frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
  //
  frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
  //
  if (gblLoanAccountTable["loanAccounNo"] != "" && gblLoanAccountTable["loanAccounNo"] != null) {
    frmAccountDetailsMB.lblUsefulValue2.text = formatAccountNo(gblLoanAccountTable["loanAccounNo"].substring(17, 27));
    frmAccountDetailsMB.lblUsefulValue3.text = gblLoanAccountTable["loanAccounNo"].substring(27);
  }
  frmAccountDetailsMB.lblUsefulValue4.text = gblLoanAccountTable["accountName"];
  frmAccountDetailsMB.lblUsefulValue5.text = reformatDate(gblLoanAccountTable["nextPmtDt"]);
  frmAccountDetailsMB.lblUsefulValue6.text = commaFormatted(gblLoanAccountTable["regPmtCurAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
  if (gblLoanAccountTable["debitAccount"].length != 0) {
    frmAccountDetailsMB.hbxUseful7.isVisible = false;
    frmAccountDetailsMB.lblUsefulValue7.text = formatAccountNo(gblLoanAccountTable["debitAccount"].substring(4));
  }
  else
  {
    frmAccountDetailsMB.lblUseful7.isVisible = false;
  }
  moreAccountDetails();
}
function clearAllSelectedAccDetails(){
  frmAccountDetailsMB.lblUseful1.text="";
  frmAccountDetailsMB.lblUseful2.text="";
  frmAccountDetailsMB.lblUseful3.text="";
  frmAccountDetailsMB.lblUseful4.text="";
  frmAccountDetailsMB.lblUseful5.text="";
  frmAccountDetailsMB.lblUseful6.text="";
  frmAccountDetailsMB.lblUseful7.text="";
  frmAccountDetailsMB.lblUseful8.text="";
  frmAccountDetailsMB.lblUseful9.text="";
  frmAccountDetailsMB.lblUseful10.text="";
  frmAccountDetailsMB.lblUseful11.text="";
  frmAccountDetailsMB.lblUseful12.text="";
  frmAccountDetailsMB.lblUseful13.text="";
  frmAccountDetailsMB.lblUseful14.text="";
  frmAccountDetailsMB.lblUseful15.text="";
  frmAccountDetailsMB.lblUsefulValue1.text="";
  frmAccountDetailsMB.lblUsefulValue2.text="";
  frmAccountDetailsMB.lblUsefulValue3.text="";
  frmAccountDetailsMB.lblUsefulValue4.text="";
  frmAccountDetailsMB.lblUsefulValue5.text="";
  frmAccountDetailsMB.lblUsefulValue6.text="";
  frmAccountDetailsMB.lblUsefulValue7.text="";
  frmAccountDetailsMB.lblUsefulValue8.text="";
  frmAccountDetailsMB.lblUsefulValue9.text="";
  frmAccountDetailsMB.lblUsefulValue10.text="";
  frmAccountDetailsMB.lblUsefulValue11.text="";
  frmAccountDetailsMB.lblUsefulValue12.text="";
  frmAccountDetailsMB.lblUsefulValue13.text="";
  frmAccountDetailsMB.lblUsefulValue14.text="";
  frmAccountDetailsMB.lblUsefulValue15.text="";
}
function showAccountDetails(eventObject) {
  try{
    kony.print("@@@ In showAccountDetails() @@@");
    //Flag to check if flow is running under cache.
    if(allowOtherOpr == false)
      return;
    gblCallPrePost = true;
    clearAllSelectedAccDetails();
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    gblASSelectedRowIndex = frmAccountSummary.segAccountDetails.selectedRowIndex;
    if (isNotBlank(selectedRow)){
      var viewName =  selectedRow["viewName"];
      kony.print("@@@ ViewName:::"+viewName);
      gaccType = selectedRow["accType"];
      
      if(viewName == "PRODUCT_CODE_NOFIXED"){
        frmAccountDetailsMB.hbxUseful2.isVisible = false;
        frmAccountDetailsMB.hbxUseful3.isVisible = false;
      }
      if (viewName == "PRODUCT_CODE_DREAM_SAVING") {
        dreamSavingCallService();
      } else if (viewName == "PRODUCT_CODE_NOFIXED") {
        depositAccInqCallServiceForNoFixed();
      } else if (viewName == "PRODUCT_CODE_NOFEESAVING_TABLE") {
        callDepositAccountInquiryServiceNofee();  //debitcardinq called from this methods callback
      } else if (viewName == "PRODUCT_CODE_SAVING_TABLE" || viewName == "PRODUCT_CODE_CURRENT_TABLE" ||
                 viewName == "PRODUCT_CODE_SAVINGCARE" || viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {
        callDepositAccountInquiryService();
      } else if (viewName == "PRODUCT_CODE_CREDITCARD_TABLE") {
        creditCardCallService();
      } else if (viewName == "PRODUCT_CODE_OLDREADYCASH_TABLE") {
        creditCardCallService();
      } else if (viewName == "PRODUCT_CODE_TD_TABLE") {
        callTimeDepositDetailService();
      } else if (viewName == "PRODUCT_CODE_LOAN_CASH2GO") {
        loanAccountCallService(cash2GoAccountCallBck);
      } else if (viewName == "PRODUCT_CODE_LOAN_HOMELOAN") {
        loanAccountCallService(homeLoanAccountCallBck);
      } else if (viewName == "PRODUCT_CODE_READYCASH_TABLE") {
        creditCardCallService();
      }
    }
  }catch(e){
    kony.print("@@@ In showAccountDetails() Exception:::"+e);
  }
}

function onclickgetTransferFromAccountsFromAccDetails() {

  if(checkMBUserStatus()){
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    if (isNotBlank(selectedRow)){
      var accId = selectedRow.accId;
      var accountStatus = selectedRow.acctStatus;
      var jointActXfer  =selectedRow.partyAcctRelDesc;
      if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
        dismissLoadingScreen();
        alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
        return;
      }

      if(accountStatus.indexOf("Active") == -1){
        dismissLoadingScreen();
        alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
        return;
      }

      var noCASAAct = noActiveActs();
      if(noCASAAct)
      {
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
        return false;
      }
      resetTransferRTPPush();
      //getTransferFromAccountsFromAccSmry(accId);
      glb_accId = accId
	  if (glb_accId.length == 10)
		glb_accId = "0000" + glb_accId;
      recipientAddFromTransfer = false;
	  gblTransferFromRecipient =false;
      showPreTransferLandingPage();
    }
  }
}

function onclickgetBillPaymentFromAccountsFromAccDetails() {

  if(checkMBUserStatus()){
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
 	if (isNotBlank(selectedRow)){

      glb_accId = selectedRow.accId;
      var jointActXfer  = selectedRow.partyAcctRelDesc;

      if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
        alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
        return;
      }
      var accountStatus = selectedRow.acctStatus;

      if(accountStatus.indexOf("Active") == -1){
        alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
        return;

      }

      var noCASAAct = noActiveActs();
      if(noCASAAct)
      {
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
        return false;
      }

      gblMyBillerTopUpBB = 0;
      GblBillTopFlag = true;
      gblBillPayShortCut = true;
	  loadFunctionalModuleSync("eDonationModule");
      callMasterBillerService();
    }
  }

}

function onclickgetTopUpFromAccountsFromAccDetails() {
  if(checkMBUserStatus()){
   	 var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
 	 if (isNotBlank(selectedRow)){

            glb_accId = selectedRow.accId;
            var jointActXfer  = selectedRow.partyAcctRelDesc;

            if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
              alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
              return;
            }
            var accountStatus = selectedRow.acctStatus;

            if(accountStatus.indexOf("Active") == -1){
              alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
              return;
            }

            var noCASAAct = noActiveActs();
            if(noCASAAct)
            {
              showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
              return false;
            }
            gblMyBillerTopUpBB = 1;
            GblBillTopFlag = false;
            callMasterBillerService(); // For Top Up Redesign PBI
     }
  }

}

function onclickgetCardlessWithdrawFromAccountsFromAccDetails() {
   loadFunctionalModuleSync("cardlessModule");
  	if(checkMBUserStatus()){
   		var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
		if (isNotBlank(selectedRow)){
          showLoadingScreen();
          glb_accId = selectedRow.accId;
          var accountStatus = selectedRow.acctStatus;
          var jointActXfer  = selectedRow.partyAcctRelDesc;
          if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
            dismissLoadingScreen();	
            alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
            return;
          }
          if(accountStatus.indexOf("Active") == -1){
            dismissLoadingScreen();
            alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
            return;
          }
          var noCASAAct = noActiveActs();
          if(noCASAAct)
          {
            dismissLoadingScreen();
            showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
            return false;
          }
          gblCardlessFlowCurrentForm = kony.application.getCurrentForm();

          //6. Setting up VTAP SDK
          setUpVTapSDK();
          frmCardlessWithdrawSelectAccount.txtAmountVal.text = "";
          if(gblUVregisterMB == "Y"){
            checkUVActiveCode();
          }else{
            callUVMaintInqService(callBackUVMaintInqServiceForCardLessWDShortcut);
          }
        //callMasterBillerService(); // For Top Up Redesign PBI
      }
    }
}

function showAccountSummaryFromMenu() {
  try{
    kony.print("@@@ In showAccountSummaryFromMenu() @@@");
    //Method to close all opened flaps on AS page.
    frmAccountSummary.show();
    closePayAlert();
    closeAllFlaps();
    isMenuShown = false;
    gblFlagMenu = "";
    gblfromCalender =false;
    resetTransferRTPPush();
    showAccuntSummaryScreen();
    /*
    kony.print("@@@ Current Form:::"+kony.application.getPreviousForm().id);
    if (kony.application.getPreviousForm().id != "frmAccountSummary"){
      gblRetryCountRequestOTP = "0";
      //added this condtion to not show AS screen before service call is success in both scenarios
      if(gblFromSwitchLanguage == false && !isNotBlank(gbl3dTouchAction) && gblforcedCleanAS == false)  {
        //frmAccountSummaryLanding.show();
        showAccountSummaryNew();
      }
      showAccuntSummaryScreen();
    }else{
      showAccountSummaryNew();
    }

    //To Refresh account summary page from menu only after gap of some interval of seconds.
    currentTime = Math.round(new Date().getTime()/1000);

    refreshTime = GLOBAL_ACCOUNT_SUMMARY_REFRESH_INTERVAL;
    if(gblforcedCleanAS === true || cacheBasedActivationFlow === true || gblLastRefresh === null || refreshTime === null || currentTime - gblLastRefresh > refreshTime){
      gblAccountTable = "";
      gblAccountStatus="";
      GLOBAL_DEBITCARD_TABLE = "";
      showAccuntSummaryScreen();
      gblLastRefresh = currentTime;
      glb_accountId = "";
      glb_accId = "";
    }else{
      showAccountSummaryWithoutRefresh();
    } */
  }catch(e){
    kony.print("@@@ In showAccountSummaryFromMenu() Exception:::"+e);
  }
}

function accountDetailsClear() {
  frmAccountDetailsMB.lblUseful1.text = ""
  frmAccountDetailsMB.lblUseful2.text = ""
  frmAccountDetailsMB.lblUseful3.text = ""
  frmAccountDetailsMB.lblUseful4.text = ""
  frmAccountDetailsMB.lblUseful5.text = ""
  frmAccountDetailsMB.lblUseful6.text = ""
  frmAccountDetailsMB.lblUseful7.text = ""
  frmAccountDetailsMB.lblUseful8.text = ""
  frmAccountDetailsMB.lblUseful9.text = ""
  frmAccountDetailsMB.lblUseful10.text = ""
  frmAccountDetailsMB.lblUseful11.text = ""
  frmAccountDetailsMB.lblUseful12.text = ""
  frmAccountDetailsMB.lblUseful13.text = ""
  frmAccountDetailsMB.lblUseful14.text = ""
  frmAccountDetailsMB.lblUseful15.text = "" 
  frmAccountDetailsMB.segMaturityDisplay.removeAll();
  frmAccountDetailsMB.lblUsefulValue1.text = ""
  frmAccountDetailsMB.lblUsefulValue2.text = ""
  frmAccountDetailsMB.lblUsefulValue3.text = ""
  frmAccountDetailsMB.lblUsefulValue4.text = ""
  frmAccountDetailsMB.lblUsefulValue5.text = ""
  frmAccountDetailsMB.lblUsefulValue6.text = ""
  frmAccountDetailsMB.lblUsefulValue7.text = ""
  frmAccountDetailsMB.lblUsefulValue8.text = ""
  frmAccountDetailsMB.lblUsefulValue9.text = ""
  frmAccountDetailsMB.lblUsefulValue10.text = ""
  frmAccountDetailsMB.lblUsefulValue11.text = ""
  frmAccountDetailsMB.lblUsefulValue12.text = ""
  frmAccountDetailsMB.lblUsefulValue13.text = ""
  frmAccountDetailsMB.lblUsefulValue14.text = ""
  frmAccountDetailsMB.lblUsefulValue15.text = ""  
  frmAccountDetailsMB.hbxUseful7.isVisible = false;
  frmAccountDetailsMB.hbxUseful8.isVisible = false;
  frmAccountDetailsMB.hbxUseful9.isVisible = false;
  frmAccountDetailsMB.hbxUseful10.isVisible = false;
  frmAccountDetailsMB.hbxUseful11.isVisible = false;
  frmAccountDetailsMB.hbxUseful12.isVisible = false;
  frmAccountDetailsMB.hbxUseful13.isVisible = false;
  frmAccountDetailsMB.hbxUseful14.isVisible = false;
  frmAccountDetailsMB.hbxUseful15.isVisible = false;    
  frmAccountDetailsMB.hbxUseful2.isVisible = true;
  frmAccountDetailsMB.hbxUseful3.isVisible = true;
  if(!(frmAccountDetailsMB.lblUsefulValue11.text==undefined || frmAccountDetailsMB.lblUsefulValue11.text == "")){
    frmAccountDetailsMB.hbxUseful11.isVisible = true;
  }

  frmAccountDetailsMB.hbxUseful4.isVisible = true;
  frmAccountDetailsMB.hbxUseful5.isVisible = true;
  frmAccountDetailsMB.lblUseful9.isVisible = true;
  gblAccountStatus="";
}
/*function crmCallBackFunctionacc(status, result) {

	if (status == 400) {


		showAccuntSummaryScreen();
	} else if (status == 300) {
		alert("status is not 300");
	}
}*/


function moreAccountDetails() {

  frmAccountDetailsMB.hbxUseful7.isVisible = false;
  frmAccountDetailsMB.hbxUseful8.isVisible = false;
  frmAccountDetailsMB.hbxUseful9.isVisible = false;
  frmAccountDetailsMB.hbxUseful10.isVisible = false;
  frmAccountDetailsMB.hbxUseful11.isVisible = false;
  frmAccountDetailsMB.hbxUseful12.isVisible = false;
  frmAccountDetailsMB.hbxUseful13.isVisible = false;
  frmAccountDetailsMB.hbxUseful14.isVisible = false;
  frmAccountDetailsMB.hbxUseful15.isVisible = false;	
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue1.text)){
    frmAccountDetailsMB.hbxUseful1.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue2.text)){
    frmAccountDetailsMB.hbxUseful2.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue3.text)){
    frmAccountDetailsMB.hbxUseful3.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue4.text)){
    frmAccountDetailsMB.hbxUseful4.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue5.text)){
    frmAccountDetailsMB.hbxUseful5.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue6.text)){
    frmAccountDetailsMB.hbxUseful6.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue7.text)){
    frmAccountDetailsMB.hbxUseful7.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue8.text)){
    frmAccountDetailsMB.hbxUseful8.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue9.text)){
    frmAccountDetailsMB.hbxUseful9.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue10.text)){
    frmAccountDetailsMB.hbxUseful10.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue11.text)){
    frmAccountDetailsMB.hbxUseful11.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue12.text)){
    frmAccountDetailsMB.hbxUseful12.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue13.text)){
    frmAccountDetailsMB.hbxUseful13.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue14.text)){
    frmAccountDetailsMB.hbxUseful14.isVisible = true;
  }
  if(isNotBlank(frmAccountDetailsMB.lblUsefulValue15.text)){
    frmAccountDetailsMB.hbxUseful15.isVisible = true;
  }



  /*
		if (glb_viewName != "PRODUCT_CODE_NOFIXED") {
			if(isNotBlank(frmAccountDetailsMB.lblUsefulValue11.text)){
				frmAccountDetailsMB.hbxUseful11.isVisible = true;
			}
		}
		if (glb_viewName == "PRODUCT_CODE_NOFIXED" && (frmAccountDetailsMB.lblUsefulValue2.text).length == 0) {

			//frmAccountDetailsMB.hboxApplySendToSave.isVisible = true;
		}

		if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName ==
			"PRODUCT_CODE_NOFEESAVING_TABLE"||glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {

			//frmAccountDetailsMB.segDebitCard.setVisibility(true);
			if (GLOBAL_DEBITCARD_TABLE != null && GLOBAL_DEBITCARD_TABLE != "") {
				var temp = [];
				var imageForDisplay="";
				var imageStatusDisplay = "";
				for (var i = 0; i < GLOBAL_DEBITCARD_TABLE.length; i++) {
				imageForDisplay="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+GLOBAL_DEBITCARD_TABLE[i]["cardLogo"]+"&modIdentifier=AccountDetails";
                imageStatusDisplay = imageForDisplay;
				  //imageForDisplay="debitcard.png"

				//ENH_092 - Debit Card Activation
				var cardStatus = GLOBAL_DEBITCARD_TABLE[i]["cardStatusCode"];   
				    var linkActivate = "";
				    var imgActivateCard = "";

				    if (cardStatus == "" || cardStatus == "Active") {
				    	// Active - Activated
						cardStatus = kony.i18n.getLocalizedString("keyCardActivated");
						imageStatusDisplay = "";
				    } else if (cardStatus == "Pin Assign(P)") {
				    	// Pin Assign(P) - Waiting for Activate
						cardStatus = kony.i18n.getLocalizedString("keyCardWaitForActivated");
						linkActivate = kony.i18n.getLocalizedString('keyActivateCard');
				    } else {
						cardStatus = "";						
				    }

					var segTable = {
						imgCreditCard: imageForDisplay,
						lblCreditCardNo: GLOBAL_DEBITCARD_TABLE[i]["maskedCardNo"],
						lblExpiryDate: kony.i18n.getLocalizedString("ExpiryDate") + "  " +GLOBAL_DEBITCARD_TABLE[i]["expiryDate"],
						lblCardStatus: cardStatus,	
						linkActivateCard: linkActivate,
						imgActivateLinkImg: imageStatusDisplay
					}
					temp.push(segTable);
					//frmAccountDetailsMB.segDebitCard.setData(temp);
					//frmAccountDetailsMB.segDebitCard.setVisibility(true);
					//frmAccountDetailsMB.hbxLinkedTo.isVisible = true;
				}
				frmAccountDetailsMB.segDebitCard.setData(temp);
				frmAccountDetailsMB.segDebitCard.setVisibility(true);
				frmAccountDetailsMB.hbxLinkedTo.isVisible = true;
			}
		} */
  if (gblAccountTable!= null && gblAccountTable != undefined && gblIndex!= null && gblIndex!= undefined && gblAccountTable["custAcctRec"] != null){
    //moreAccountDetails();
    frmAccountDetailsMB.show();
  }
}

function shortCutActDetailsPage()
{
  if(!flowSpa)
  {
    /*
		currForm = frmAccountDetailsMB;
		var deviceHght = kony.os.deviceInfo().deviceHeight;
		var osType = kony.os.deviceInfo().name;
		var btnskin = "btnToDoIcon";
		var btnFocusSkin = "btnToDoIconFoc";

		frmAccountDetailsMB.hbxOnHandClick.isVisible = true;
		frmAccountDetailsMB.btnOptions.skin = btnFocusSkin;
		frmAccountDetailsMB.imgHeaderMiddle.src = "arrowtop.png";
		frmAccountDetailsMB.imgHeaderRight.src = "empty.png";		

		if (frmAccountDetailsMB.hbxOnHandClick.isVisible) {
			frmAccountDetailsMB.hbxOnHandClick.isVisible = false;
			frmAccountDetailsMB.btnOptions.skin = btnskin;
			frmAccountDetailsMB.imgHeaderMiddle.src = "arrowtop.png";
			frmAccountDetailsMB.imgHeaderRight.src = "empty.png";
		} else {
			frmAccountDetailsMB.hbxOnHandClick.isVisible = true;
			frmAccountDetailsMB.btnOptions.skin = btnFocusSkin;
			frmAccountDetailsMB.imgHeaderMiddle.src = "arrowtop.png";
			frmAccountDetailsMB.imgHeaderRight.src = "empty.png";
		}
		*/
  }
  else
  {
    toDo();
  }
}

function postShowOfAccountSummary(){
  gblMBLoggingIn = false;

  deviceInfo = kony.os.deviceInfo();
  if (deviceInfo["name"] == "thinclient" & deviceInfo["type"]=="spa")
  {
    isMenuRendered = false;
    isMenuShown = false;
    //frmAccountSummaryLanding.scrollboxMain.scrollToEnd();
  }
  destroyCardlessWithDrawForms();
}
function canIssueNewCard(productCode) {
  var nocard = 0;
  var maxDebit = 0;
  var allowList = gblDebitCardResult["allowIssueNewCardProductList"];

  if(allowList.indexOf(productCode) < 0) { //not in the list
    return false;
  }

  if(gblDebitCardResult["allowIssueNewCard"] != 'N') {
    return true;
  }

  return false;
}
function checkShowDebitCardMenu(selectedRow){
  kony.print("inside checkShowDebitCardMenu function gblenableDebitCardProductList>>"+gblenableDebitCardProductList);
  kony.print("checkShowDebitCardMenu - gblenableDebitCardAccountStatus: "+gblenableDebitCardAccountStatus);
  kony.print("checkShowDebitCardMenu - selectedRow>>"+selectedRow["productID"]);
  if (isNotBlank(selectedRow["productID"]) &&  isNotBlank(gblenableDebitCardProductList) && isNotBlank(gblenableDebitCardAccountStatus)) {
    var allowList = gblenableDebitCardProductList;  //gblDebitCardResult["enableDebitCardProductList"];
    var productCode = selectedRow["productID"];
    var allowAccountStatusList =  gblenableDebitCardAccountStatus; //gblDebitCardResult["enableDebitCardAccountStatus"];
    var acctStatus = selectedRow["acctStatus"];
    var idx = acctStatus.indexOf("|");
    if(idx > -1) {
      acctStatus = acctStatus.substring(0,idx);
      acctStatus = acctStatus.trim();
    }
    kony.print("Product code:"+productCode+" acctStatus: "+acctStatus);
    if (allowList.indexOf(productCode) > -1 && allowAccountStatusList.indexOf(acctStatus) > -1) {
      kony.print("product code allowed 11");
      kony.print("before setting Debit Card text");
      frmAccountDetailsMB.btnlinkDebitCard.text = kony.i18n.getLocalizedString('DBA01_Title');
      frmAccountDetailsMB.flexLineDebitCards.setVisibility(true);
      frmAccountDetailsMB.flexLinkDebitCards.setVisibility(true);
    }
    else
    {
      kony.print("inside not allowed acctStatus flow: "+acctStatus);
      frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
      frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    }
  }
}

function moveCradFlexforDetails()
{
  if(frmAccountDetailsMB.flxCardImage.isVisible)
  {
    if(frmAccountDetailsMB.flxCardImage.top=="-85dp")
    {
      frmAccountDetailsMB.flxCardImage.animate(
        kony.ui.createAnimation({"100":{"top":"0dp","stepConfig":{"timingFunction":kony.anim.EASE}}}),
        {"delay":0,"iterationCount":1,"fillMode":kony.anim.FILL_MODE_FORWARDS,"duration":0.25},
        {"animationEnd" : animateCallbackCardFlxmove});
    }
  }

}

function animateCallbackCardFlxmove()
{
  //do nothing here
}

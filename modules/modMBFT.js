EDIT_RECEIPIENT_ENABLE = false;







function endDateCalMB(date,times){
    var times = times;
    var startDate = date;  //date should be formatteddate
    var startDate = parseDateIB(startDate);
    frmMBFTView.lblEndOnDateVal.text = endDateCalculatorFTMB(startDate, times);
}

function completeProcessMB(){
    frmMBFTEditCmplete.lblFrmAccntName.text = frmMBFTEditCnfrmtn.lblFrmAccntName.text;
	frmMBFTEditCmplete.lblFrmAccntNum.text = frmMBFTEditCnfrmtn.lblFrmAccntNum.text;
	frmMBFTEditCmplete.lblFrmAccntNickName.text = frmMBFTEditCnfrmtn.lblFrmAccntNickName.text;
	frmMBFTEditCmplete.lblToAccntNickName.text = frmMBFTEditCnfrmtn.lblToAccntNickName.text;
	frmMBFTEditCmplete.lblToAccntNum.text = frmMBFTEditCnfrmtn.lblToAccntNum.text;
	frmMBFTEditCmplete.lblToAccntName.text = frmMBFTEditCnfrmtn.lblToAccntName.text;
	frmMBFTEditCmplete.lblBankNameVal.text = frmMBFTEditCnfrmtn.lblBankNameVal.text;
	frmMBFTEditCmplete.lblFTViewAmountVal.text = frmMBFTEditCnfrmtn.lblFTViewAmountVal.text
	frmMBFTEditCmplete.lblFeeVal.text = "("+frmMBFTEditCnfrmtn.lblFeeVal.text+")";
	frmMBFTEditCmplete.lblViewStartOnDateVal.text = frmMBFTEditCnfrmtn.lblViewStartOnDateVal.text;
	frmMBFTEditCmplete.lblRepeatAsVal.text = frmMBFTEditCnfrmtn.lblRepeatAsVal.text;
	frmMBFTEditCmplete.lblEndOnDateVal.text = frmMBFTEditCnfrmtn.lblEndOnDateVal.text;
	frmMBFTEditCmplete.lblExcuteVal.text = frmMBFTEditCnfrmtn.lblExcuteVal.text + " "+  kony.i18n.getLocalizedString("keyTimesMB");
	frmMBFTEditCmplete.lblNotifyRecipientVal.text = frmMBFTEditCnfrmtn.lblNotifyRecipientVal.text;
	frmMBFTEditCmplete.lblNoteToRecipientVal.text = frmMBFTEditCnfrmtn.lblNoteToRecipientVal.text;
	frmMBFTEditCmplete.lblMyNoteVal.text = frmMBFTEditCnfrmtn.lblMyNoteVal.text;
	frmMBFTEditCmplete.lblRemainingVal.text = frmMBFTEditCnfrmtn.lblRemainingVal.text;
	frmMBFTEditCmplete.lblMethodName.text = frmMBFTEditCnfrmtn.lblMethodName.text;
	dismissLoadingScreen();
  	EDIT_RECEIPIENT_ENABLE = true;
	frmMBFTEditCmplete.show();
}

//Call before saving the schdule details

function validateSchduleMB(){
	//validating edited start date
	var today = new Date();
	var date =  today.getDate();
	var month = today.getMonth()+1;
	var year = today.getFullYear();
	//var dateArray = [date,month,year];
	//dateChek(dateArray , frmIBFTrnsrView.calStartDate.dateComponents);
	var d1 = date+"/"+ month + "/" + year;
	var d2 = frmMBFtSchedule.calScheduleStartDate.formattedDate;
	
	//Should handle when tomorrows date is selectd,today's time is b4 11pm or not
	if(parseDateIB(d1) >= parseDateIB(d2)){
	 	//alert("Start On Date should be greater than todays date");
	 	alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));	
	 	return false;
	} 
	frmMBFTEdit.lblViewStartOnDateVal.text = getFormattedDate(frmMBFtSchedule.calScheduleStartDate.formattedDate,"en_US");
	
	//kony.i18n.getLocalizedString("keyOnce") // Once
	if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyOnce")){
	   	if(gblScheduleRepeatFTMB != "Once"){
			if(gblScheduleEndFTMB == "none"){
				alert("Please select Ending Value");
				return false;
			}else if(gblScheduleEndFTMB == kony.i18n.getLocalizedString("keyAfter")){
				if(frmMBFtSchedule.tbxAfterTimes.text == ""){
					//alert("Enter No. of times");
					alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes"));
					return false;
				}else{
				   	var temp = frmMBFtSchedule.tbxAfterTimes.text;
				   	if(kony.string.isNumeric(temp) == false){
				   	  	frmMBFtSchedule.tbxAfterTimes.skin = txtErrorBG;
				   	  	alert("Enter Valid Input");  //This case is handled by keyboard disable
				      	//alert(kony.i18n.getLocalizedString("keyIBOtpNumeric"));   //Only Numeric input allowed
						return false;
					}
				}
			}
		}else if(gblScheduleRepeatFTMB == "Once" && gblScheduleEndFTMB != "none"){
			alert("Please Select Repeat As Value");
			return false;
		}else if(gblScheduleRepeatFTMB == "Once" && gblScheduleEndFTMB == "none"){
			frmMBFTEdit.lblEndOnDateVal.text = frmMBFTEdit.lblViewStartOnDateVal.text;
			frmMBFTEdit.lblExcuteVal.text = "1";
			frmMBFTEdit.lblRepeatAsVal.text = "Once";
			frmMBFTEdit.lblRemainingVal.text = "1";
			gblRepeatAsCopy="once";
		}
	}
	if(gblScheduleEndFTMB == kony.i18n.getLocalizedString("keyOnDate")) {
		// Date validations
		if(parseDateIB(frmMBFtSchedule.calScheduleStartDate.formattedDate) >= parseDateIB(frmMBFtSchedule.calScheduleEndDate.formattedDate)){
	    	//alert("End Date should be greater than the start Date");
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));	
		 	return false;				 
		}	
			
	 	//frmMBFTEdit.lblViewStartOnDateVal.text = frmMBFtSchedule.calScheduleStartDate.formattedDate;
	 	//frmMBFTEdit.lblEndOnDateVal.text = frmMBFtSchedule.calScheduleEndDate.formattedDate;
	 	frmMBFTEdit.lblViewStartOnDateVal.text = getFormattedDate(frmMBFtSchedule.calScheduleStartDate.formattedDate,"en_US");
	 	frmMBFTEdit.lblEndOnDateVal.text = getFormattedDate(frmMBFtSchedule.calScheduleEndDate.formattedDate, "en_US");
		 
	 	var tempNumTimes = numberOfExecutionFT(frmMBFtSchedule.calScheduleStartDate.formattedDate, frmMBFtSchedule.calScheduleEndDate.formattedDate,gblScheduleRepeatFTMB);
	 	if(tempNumTimes > 99){
	    	alert("Recurring should be less than 99");
        	return false;
		}
		
		// frmMBFTEdit.lblExcuteVal.text = tempNumTimes+1; //Including the ondate excution value
    	frmMBFTEdit.lblExcuteVal.text = tempNumTimes;
	  	gblIsEditOnDateMB = true;
	  	gblIsEditAftrMB = false;
	}
	//if after is changed, have to show the end date in view page
	if(gblScheduleEndFTMB == kony.i18n.getLocalizedString("keyAfter")){
	   	if(frmMBFtSchedule.tbxAfterTimes.text != ""){
	        var temp = frmMBFtSchedule.tbxAfterTimes.text; 
	        if(kony.string.isNumeric(temp == false)){
	        	frmMBFtSchedule.tbxAfterTimes.skin = txtErrorBG;
	         	alert("Enter Valid Input");
	         	//alert(kony.i18n.getLocalizedString("keyIBOtpNumeric"));   //Only Numeric input allowed
	         	return false;
	         }else{
	         	if(temp > 99){
		           alert("Recurring should be less than 99");
		           return false;
	         	}
				frmMBFTEdit.lblExcuteVal.text = frmMBFtSchedule.tbxAfterTimes.text;				         
	         }
		}else {
		   alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes"));   //alert("Enter No. of Times");
		   return false;
		}	
	  	//frmMBFTEdit.lblEndOnDateVal.text =  endDateMapMB(frmMBFtSchedule.tbxAfterTimes.text,frmMBFtSchedule.calScheduleStartDate.formattedDate);;
	  	var endDate =  endDateMapMB(frmMBFtSchedule.tbxAfterTimes.text, frmMBFtSchedule.calScheduleStartDate.formattedDate);
	  	frmMBFTEdit.lblEndOnDateVal.text = getFormattedDate(endDate, "en_US");
	  
	  	gblIsEditOnDateMB = false;
	  	gblIsEditAftrMB = true;
		
	} 
	
	if(gblScheduleEndFTMB == kony.i18n.getLocalizedString("keyNever")){
	  	frmMBFTEdit.lblEndOnDateVal.isVisible = false;
        frmMBFTEdit.lblToTxt.isVisible = false;
		frmMBFTEdit.lblExcuteVal.isVisible = false;
		frmMBFTEdit.lblRemainingVal.isVisible = false;
     	frmMBFTEdit.flexRepeatSection.isVisible = false;
        frmMBFTEdit.lblPayBillOn.isVisible = false;
        frmMBFTEdit.flxRecurringDetail.isVisible = false;
	  	gblIsEditOnDateMB = false;
	  	gblIsEditAftrMB = false;
	}else{
      	frmMBFTEdit.lblEndOnDateVal.isVisible = true;
        frmMBFTEdit.lblToTxt.isVisible = true;
		frmMBFTEdit.lblExcuteVal.isVisible = true;
		frmMBFTEdit.lblRemainingVal.isVisible = true;
        frmMBFTEdit.flexRepeatSection.isVisible = true;
        frmMBFTEdit.lblPayBillOn.isVisible = true;
        frmMBFTEdit.flxRecurringDetail.isVisible = true;
	  	
      
    }
	 
	if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyDaily")){
	   frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyDaily");
	   	gblRepeatAsCopy="Daily";
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyWeekly")){
	    frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
	    gblRepeatAsCopy = "Weekly"
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyMonthly")){
	    frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
	    gblRepeatAsCopy = "Monthly"
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyYearly")){
	    frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");
	    gblRepeatAsCopy = "Yearly"
	}else if(gblScheduleRepeatFTMB == "Once"){
	    frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyOnce");
	    frmMBFTEdit.lblEndOnDateVal.text = frmMBFTEdit.lblViewStartOnDateVal.text;
	    frmMBFTEdit.lblExcuteVal.text = "1";
		frmMBFTEdit.lblRemainingVal.text = "1";
		gblRepeatAsCopy="once";
	}
	 //Logic if only date or num of times is edited without clicking repeatas btn and ending btn
	 if(gblIsEditOnDateMB == true && gblIsEditAftrMB == false && gblScheduleRepeatFTMB != "Once" ){
	    if(parseDateIB(frmMBFtSchedule.calScheduleStartDate.formattedDate) >= parseDateIB(frmMBFtSchedule.calScheduleEndDate.formattedDate)){
			//alert("End Date should be greater than the start Date");
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));
			return false;				 
		}
		var d2 = frmMBFTEdit.lblEndOnDateVal.text;
		var d3 = frmMBFtSchedule.calScheduleEndDate.formattedDate;
		 
		if(parseDateIB(d2) != parseDateIB(d3)){
		  	gblIsEditAftrMB = false;
		  	gblIsEditOnDateMB = true;
		}
		//var i = numberOfExecution(frmMBFtSchedule.calScheduleStartDate.formattedDate, frmMBFtSchedule.calScheduleEndDate.formattedDate, gblScheduleRepeatFTMB);
		frmMBFTEdit.lblExcuteVal.text = numberOfExecution(frmMBFtSchedule.calScheduleStartDate.formattedDate, frmMBFtSchedule.calScheduleEndDate.formattedDate, gblScheduleRepeatFTMB);
	}
	if(gblIsEditAftrMB == true && gblIsEditOnDateMB == false && gblScheduleRepeatFTMB != "Once"){
		var n1 = frmMBFtSchedule.tbxAfterTimes.text;
		var n2 = frmMBFTEdit.lblExcuteVal.text;
		//Only if repeatas and ending value are not changed	
	
		if(gblScheduleRepeatFTMB == frmMBFTEdit.lblRepeatAsVal.text){
			if(n1 != n2){
			    var endDate = endDateMapMB(frmMBFtSchedule.tbxAfterTimes.text,frmMBFtSchedule.calScheduleStartDate.formattedDate);
			    frmMBFTEdit.lblEndOnDateVal.text = getFormattedDate(endDate, "en_US");
				gblIsEditAftrMB = true;
			    gblIsEditOnDateMB = false;
		    }else{
		    	var endDate =  endDateMapMB(frmMBFtSchedule.tbxAfterTimes.text, frmMBFtSchedule.calScheduleStartDate.formattedDate);
		    	frmMBFTEdit.lblEndOnDateVal.text = getFormattedDate(endDate, "en_US");
		    }
		}	 
	}
	     
   	frmMBFTEdit.lblRemainingVal.text = frmMBFTEdit.lblRemainingVal.text;  
   	if(gblpmtMethodFTMB == "SMART"){
    	var dd = frmMBFtSchedule.calScheduleStartDate.dateComponents;
	    var monthTemp = parseInt(dd[1],10);  
	    var d2 = new Date(dd[2],monthTemp-1,dd[0]);			//new Date(dd[2],dd[1]-1,dd[0])
	
		if(d2.getDay() == 6 || d2.getDay() == 0){
		   alert(kony.i18n.getLocalizedString("keyFTSMARTOnWeekend"));
		   return;
		}
    	var date = dd[2]+"/"+dd[1]+"/"+dd[0]; //"2013/11/11"
		srvHolydayChkMB(date);
   } else {
     if(parseInt(frmMBFTEdit.lblFeeVal.text) == 0){
       gblShowFeeText = true;
       frmMBFTEdit.lblFeeText.setVisibility(true);
       frmMBFTEdit.lblFeeVal.setVisibility(false);
     }else{
       gblShowFeeText = false;
       frmMBFTEdit.lblFeeText.setVisibility(false);
       frmMBFTEdit.lblFeeVal.setVisibility(true);
     }
   		frmMBFTEdit.show();
   }
}

//End date caluculation for if the After is clicked
function endDateMapMB(times,date){
    var times = times;
	var startDate = date;
    var startDate = parseDateIB(startDate);
    var endDate = endDateCalculatorFTMB(startDate, times)
    return endDate;
}

function endDateCalculatorFTMB(staringDateFromCalendar, repeatTimesTextBoxValue) {

	var endingDate = staringDateFromCalendar;
	var numberOfDaysToAdd = 0;
	if (kony.string.equalsIgnoreCase(gblScheduleRepeatFTMB, kony.i18n.getLocalizedString("keyDailyNT")) || kony.string.equalsIgnoreCase(gblScheduleRepeatFTMB, kony.i18n.getLocalizedString("keyDailyNE"))) {
		numberOfDaysToAdd = repeatTimesTextBoxValue * 1;
		numberOfDaysToAdd = numberOfDaysToAdd - 1;
		endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
	} else if (kony.string.equalsIgnoreCase(gblScheduleRepeatFTMB, kony.i18n.getLocalizedString("keyWeeklyNT")) || kony.string.equalsIgnoreCase(gblScheduleRepeatFTMB, kony.i18n.getLocalizedString("keyWeeklyNE"))) {
		numberOfDaysToAdd = ((repeatTimesTextBoxValue - 1) * 7);
		endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
	} else if (kony.string.equalsIgnoreCase(gblScheduleRepeatFTMB, kony.i18n.getLocalizedString("keyMonthlyNT")) || kony.string.equalsIgnoreCase(gblScheduleRepeatFTMB, kony.i18n.getLocalizedString("keyMonthlyNE"))) {
		       	endingDate.setDate(endingDate.getDate());
                var dd = endingDate.getDate();
                
    			var mm = endingDate.getMonth() + 1;
    			
    			var newmm = parseFloat(mm.toString())+ parseFloat(repeatTimesTextBoxValue - 1);
    			
    			var newmmadd = newmm % 12;
    			if(newmmadd == 0){
    				newmmadd = 12;
    			}
    			
    			var yearAdd = Math.floor((newmm / 12));
    			
   				var y = endingDate.getFullYear();
   				if(newmmadd == 12){
   					y = parseFloat(y )+ parseFloat(yearAdd)-1;
   				}else
   				    y = parseFloat(y )+ parseFloat(yearAdd);
   				    
   				mm = parseFloat(mm.toString())+ newmmadd;
   				
   				if(newmmadd == 2){
   					if(dd > 28){
   						dd = 28;
   					}
   				}
   				if(newmmadd == 4 || newmmadd == 6 || newmmadd == 9 || newmmadd == 11){
   					if(dd > 30){
   						dd = 30;
   					}
   				}
   				if (dd < 10) 
		        	dd = '0' + dd;
   				
   				if (newmmadd < 10) {
					newmmadd = '0' + newmmadd;
				}
   				var someFormattedDate = dd + '/' + newmmadd + '/' + y;
   				return someFormattedDate;
		} 
	
	var dd = endingDate.getDate();
	var mm = endingDate.getMonth() + 1;
	var y = endingDate.getFullYear();
	
	if(kony.string.equalsIgnoreCase(gblScheduleRepeatFTMB, kony.i18n.getLocalizedString("keyYearlyNT")) || kony.string.equalsIgnoreCase(gblScheduleRepeatFTMB, kony.i18n.getLocalizedString("keyYearlyNE"))) {
		y = parseFloat(y)+ parseFloat(repeatTimesTextBoxValue - 1) ;
	}
	
	if (dd < 10) {
		dd = '0' + dd;
	}
	if (mm < 10) {
		mm = '0' + mm;
	}
	var someFormattedDate = dd + '/' + mm + '/' + y;
	return someFormattedDate;
}


function showEditScheduleMB(){

	frmMBFtSchedule.calScheduleStartDate.validStartDate = currentDateForcalender();
	frmMBFtSchedule.calScheduleEndDate.validStartDate = currentDateForcalender();

	if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyMonthly")){
	
		frmMBFtSchedule.lblEnd.setVisibility(true);
		gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyMonthly");
		
		frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
		setFocusOnMonthlyEditFT();
	
		if(gblIsEditOnDateMB == true && gblIsEditAftrMB == false){
		    gblEndValTempMB = kony.i18n.getLocalizedString("keyOnDate"); 
		    
		    frmMBFtSchedule.lblEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(true);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(true);
			frmMBFtSchedule.hbxEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(true);
			setFocusOnOnDateBtnEditFT();
		   
		    frmMBFtSchedule.calScheduleEndDate.dateComponents = dateFormatForDateComp(frmMBFTEdit.lblEndOnDateVal.text);
		} else if(gblIsEditAftrMB == true && gblIsEditOnDateMB == false ){
			gblEndValTempMB =kony.i18n.getLocalizedString("keyAfter");
			//gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyAfter");	  
		    frmMBFtSchedule.lblEndAfter.setVisibility(true);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(true);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(true);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(true)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
			frmMBFtSchedule.hbxEndAfter.setVisibility(true);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
			setFocusOnAfterBtnEditFT();
			frmMBFtSchedule.tbxAfterTimes.text = frmMBFTEdit.lblExcuteVal.text;
	    
 		}else {
		    gblEndValTempMB = kony.i18n.getLocalizedString("keyNever");
		    frmMBFtSchedule.lblEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
			frmMBFtSchedule.hbxEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
			setFocusOnNeverBtnEditFT();
		 }

	}else if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyDaily")){
		gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyDaily");
		frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
	
		frmMBFtSchedule.lblEnd.setVisibility(true);
		setFocusOnDailyEditFT();
	
	 	if(gblIsEditOnDateMB == true && gblIsEditAftrMB == false){
    		gblEndValTempMB = kony.i18n.getLocalizedString("keyOnDate"); 

		    frmMBFtSchedule.calScheduleEndDate.dateComponents = dateFormatForDateComp(frmMBFTEdit.lblEndOnDateVal.text);
		  
		    frmMBFtSchedule.lblEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(true);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(true);
			frmMBFtSchedule.hbxEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(true);
			setFocusOnOnDateBtnEditFT();
 		} else if(gblIsEditAftrMB == true && gblIsEditOnDateMB == false ){
		   	gblEndValTempMB =kony.i18n.getLocalizedString("keyAfter");
		   	gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyAfter");
		   
		    frmMBFtSchedule.lblEndAfter.setVisibility(true);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(true);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(true);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(true)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
			frmMBFtSchedule.hbxEndAfter.setVisibility(true);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
			
			setFocusOnAfterBtnEditFT();
			frmMBFtSchedule.tbxAfterTimes.text = frmMBFTEdit.lblExcuteVal.text;
   
 		}else {
		    gblEndValTempMB = kony.i18n.getLocalizedString("keyNever");
		   
		    frmMBFtSchedule.lblEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
			frmMBFtSchedule.hbxEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
			setFocusOnNeverBtnEditFT();
    
 		}
	}else if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyWeekly")){
	   	gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyWeekly");
	   
	    frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
		frmMBFtSchedule.lblEnd.setVisibility(true)
		setFocusOnWeeklyEditFT();
		
	  	if(gblIsEditOnDateMB == true && gblIsEditAftrMB == false){
		    gblEndValTempMB = kony.i18n.getLocalizedString("keyOnDate"); 
		    
			frmMBFtSchedule.calScheduleEndDate.dateComponents = dateFormatForDateComp(frmMBFTEdit.lblEndOnDateVal.text);
		
		    frmMBFtSchedule.lblEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(true);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(true);
			frmMBFtSchedule.hbxEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(true);
			setFocusOnOnDateBtnEditFT();
	 	}else if(gblIsEditAftrMB == true && gblIsEditOnDateMB == false ){
		    gblEndValTempMB =kony.i18n.getLocalizedString("keyAfter");
		    gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyAfter");
		    
		    frmMBFtSchedule.lblEndAfter.setVisibility(true);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(true);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(true);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(true)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
			frmMBFtSchedule.hbxEndAfter.setVisibility(true);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
			setFocusOnAfterBtnEditFT();
			frmMBFtSchedule.tbxAfterTimes.text = frmMBFTEdit.lblExcuteVal.text;
    
		}else {
		    gblEndValTempMB = kony.i18n.getLocalizedString("keyNever");
		    frmMBFtSchedule.lblEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
			frmMBFtSchedule.hbxEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
			setFocusOnNeverBtnEditFT();
		    
		 }

	}else if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyYearly")){
		frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
	
	    gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyYearly");
	    frmMBFtSchedule.lblEnd.setVisibility(true)
		setFocusOnYearlyEditFT();
	 
		if(gblIsEditOnDateMB == true && gblIsEditAftrMB == false){
		    gblEndValTempMB = kony.i18n.getLocalizedString("keyOnDate"); 
		    frmMBFtSchedule.calScheduleEndDate.dateComponents = dateFormatForDateComp(frmMBFTEdit.lblEndOnDateVal.text);
		    frmMBFtSchedule.lblEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(true);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(true);
			frmMBFtSchedule.hbxEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(true);
			setFocusOnOnDateBtnEditFT();
		 }else if(gblIsEditAftrMB == true && gblIsEditOnDateMB == false ){
		    gblEndValTempMB =kony.i18n.getLocalizedString("keyAfter");
		    gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyAfter");
		    
		    frmMBFtSchedule.lblEndAfter.setVisibility(true);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(true);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(true);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(true)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
			frmMBFtSchedule.hbxEndAfter.setVisibility(true);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
			
			setFocusOnAfterBtnEditFT();
			frmMBFtSchedule.tbxAfterTimes.text = frmMBFTEdit.lblExcuteVal.text;
		    
		 }else {
		    gblEndValTempMB = kony.i18n.getLocalizedString("keyNever");
		    frmMBFtSchedule.lblEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
			frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
			frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
			frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
			frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
			frmMBFtSchedule.hbxEndAfter.setVisibility(false);
			frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
			setFocusOnNeverBtnEditFT();
		 }
	}
  	//frmMBFtSchedule.btnDaily.skin =  btnScheduleEndLeftDisabled;	
	if(gblpmtMethodFTMB == "SMART"){
	 	frmMBFtSchedule.btnDaily.skin =  btnScheduleEndDisabled;
	 	frmMBFtSchedule.btnDaily.focusSkin =  btnScheduleEndDisabled;
	}	
	frmMBFtSchedule.show();
}


//frm level mapping from view to Edit

/*
function navigateToEditFrm(){
    
     frmMBFTEdit.lblFrmAccntName.text = frmMBFTView.lblFrmAccntName.text;
     frmMBFTEdit.lblFrmAccntNickName.text = frmMBFTView.lblFrmAccntNickName.text;
     frmMBFTEdit.lblFrmAccntNum.text = frmMBFTView.lblFrmAccntNum.text;
     frmMBFTEdit.lblToAccntName.text = frmMBFTView.lblToAccntName.text;
     frmMBFTEdit.lblToAccntNickName.text = frmMBFTView.lblToAccntNickName.text;
     frmMBFTEdit.lblToAccntNum.text = frmMBFTView.lblToAccntNum.text;
     frmMBFTEdit.lblBankNameVal.text = frmMBFTView.lblBankNameVal.text;
     frmMBFTEdit.lblFeeVal.text = frmMBFTView.lblFeeVal.text; 
     frmMBFTEdit.lblOrderDateVal.text = frmMBFTView.lblOrderDateVal.text;
     frmMBFTEdit.lblViewStartOnDateVal.text = frmMBFTView.lblViewStartOnDateVal.text;
     frmMBFTEdit.lblRepeatAsVal.text = frmMBFTView.lblRepeatAsVal.text;
     frmMBFTEdit.lblRemainingVal.text = frmMBFTView.lblRemainingVal.text;
     frmMBFTEdit.lblEndOnDateVal.text = frmMBFTView.lblEndOnDateVal.text;
     frmMBFTEdit.lblExcuteVal.text = frmMBFTView.lblExcuteVal.text; 
     frmMBFTEdit.lblNotifyRecipientVal.text = frmMBFTView.lblNotifyRecipientVal.text;
     frmMBFTEdit.lblNoteToRecipientVal.text = frmMBFTView.lblNoteToRecipientVal.text;
     frmMBFTEdit.lblMyNoteVal.text = frmMBFTView.lblMyNoteVal.text;
     frmMBFTEdit.lblMethodName.text = frmMBFTView.lblMethodName.text;
     frmMBFTEdit.lblScheduleRefNoVal.text = frmMBFTView.lblScheduleRefNoVal.text;
     //frmMBFTEdit.txtEditAmnt.text = frmMBFTView.lblFTViewAmountVal.text;
     
     var tmpAmt = frmMBFTView.lblFTViewAmountVal.text;
	 tmpAmt = tmpAmt.substring(0, tmpAmt.length-1)
	 frmMBFTEdit.txtEditAmnt.text = tmpAmt;

}
*/

// Validation for Amount
function validateAmtMB(){
  	if(gblpmtMethodFTMB == "SMART" && (frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyDaily") || frmMBFTEdit.lblRepeatAsVal.text == "Daily")){
      showAlert(kony.i18n.getLocalizedString("keyFTSmartDailyNotAllowed"), kony.i18n.getLocalizedString("info"));
      return false;
    }
	var tempAmt = frmMBFTView.lblFTViewAmountVal.text
	tempAmt = tempAmt.substring(0, tempAmt.length-1)
	if(isNotBlank(tempAmt) && tempAmt.indexOf(",") != -1){
		tempAmt = kony.string.replace(tempAmt, ",", "");
	}
			
	tempAmt = parseFloat(tempAmt.toString())
	var tempAmt1 = frmMBFTEdit.txtEditAmnt.text;
		
	if(isNotBlank(tempAmt1) && tempAmt1.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
		tempAmt1 = replaceCommon(tempAmt1, kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	}
		
	if(tempAmt1.length == 0 || tempAmt1 == "." || tempAmt1 == ","){
		dismissLoadingScreen();
		frmMBFTEdit.txtEditAmnt.skin = txtErrorBG;
      	showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterAmount"),kony.i18n.getLocalizedString("info"),setFocusOnAmountField);
		return false;
	}
	tempAmt1 = tempAmt1.trim(); 
	if(isNotBlank(tempAmt1) && tempAmt1.indexOf(",") != -1){
		tempAmt1 = kony.string.replace(tempAmt1, ",", "");
    }
		
	var pat1= /[^0-9.,]/g
	var  pat2 = /[\s]/g
	var isAlpha = pat1.test(tempAmt1);
	var containsSpace = pat2.test(tempAmt1);  
	if(isAlpha == true || containsSpace == true){
		  dismissLoadingScreen();
		  frmMBFTEdit.txtEditAmnt.skin = txtErrorBG;
		  //alert("Enter Valid Amount!");
		  alert(kony.i18n.getLocalizedString("Error_MinXfrAmountAllowed"));
		  return false;
	}
		
	tempAmt1 = parseFloat(tempAmt1.toString());
	if(tempAmt1 < 0.01){
		dismissLoadingScreen();
		 //alert("Min. Tranfer Amount Should be 0.01"+kony.i18n.getLocalizedString("currencyThaiBaht"));
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterAmount"),kony.i18n.getLocalizedString("info"),setFocusOnAmountField);
		return false;
	}
	var compare = (tempAmt == tempAmt1);
	if(compare== true && gblEditFTSchduleMB == false){
		dismissLoadingScreen();
		  //alert("Change Either Amount or Schedule");
		alert(kony.i18n.getLocalizedString("Error_NoEditingDone"));
		return false;
	}
		
	var maxTransactnLimt = "";
	if(gblpmtMethodFTMB == "ORFT")
		maxTransactnLimt = gblORFTPerTransAmntLimtMB;   //"50000"; //Configurable //Get it from gblORFTPerTransAmntLimt 
	if(gblpmtMethodFTMB == "SMART")
		maxTransactnLimt = gblSMARTTransAmntMB;   //"100000"; //Configurable // Get it From gblSMARTTransAmnt
  if(gblpmtMethodFTMB == "PROMPTPAY_ACCOUNT")
		maxTransactnLimt = gbltoAccPromptPayRange2Higher; //50000 configurable transaction limit for promptpay
			
	maxTransactnLimt = parseFloat(maxTransactnLimt.toString());
    frmMBFTEditCnfrmtn.lblFeeVal.text = frmMBFTEdit.lblFeeVal.text;
    if(tempAmt1 > maxTransactnLimt ){
      	dismissLoadingScreen(); 
      	var amounttext = kony.i18n.getLocalizedString("TRErr_MaxLimitExceedLimitType");
       	amounttext = amounttext.replace("{MaxLimitperTypeAmount}", maxTransactnLimt);
      	showAlertWithCallBack(amounttext,kony.i18n.getLocalizedString("info"),setFocusOnAmountField);
      	return false;
    }else{
       if(gblpmtMethodFTMB == "ORFT"){
       		if (isNotBlank(gblORFT_ALL_FREE_TRANS_CODES) && gblORFT_ALL_FREE_TRANS_CODES.indexOf(gblFrmaccntProdCodeMB) >= 0) {
				frmMBFTEditCnfrmtn.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht"); //"0"
        	}else{
       	   		var temp = ""+gblORFT_FEE_RANGE_MB;
       	   		temp = temp.split("-");
       	   		temp[0] = parseFloat(temp[0].toString());
       	   		if(tempAmt1 >= temp[0]){
   	   		  		frmMBFTEditCnfrmtn.lblFeeVal.text = eval(gblORFT_FEE_AMNT2_MB).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
       	   		}else
       	   	  		frmMBFTEditCnfrmtn.lblFeeVal.text = eval(gblORFT_FEE_AMNT1_MB).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
       	 	}
       	}else if (gblpmtMethodFTMB == "PROMPTPAY_ACCOUNT") {
                    if (isNotBlank(gblTOACC_PP_FREE_PROD_CODES) && gblTOACC_PP_FREE_PROD_CODES.indexOf(gblFrmaccntProdCodeMB) >= 0) {
                     	frmMBFTEditCnfrmtn.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht"); //"0"
                    }else{
                        if( tempAmt1 > gbltoAccPromptPayRange1Lower && tempAmt1 <= gbltoAccPromptPayRange1Higher){
                            frmMBFTEditCnfrmtn.lblFeeVal.text = eval(gbltoAccPromptPaySPlitFeeAmnt1).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                        }else if (tempAmt1 > gbltoAccPromptPayRange2Lower && tempAmt1 <= gbltoAccPromptPayRange2Higher) {
                           frmMBFTEditCnfrmtn.lblFeeVal.text = eval(gbltoAccPromptPaySPlitFeeAmnt2).toFixed(2)+ kony.i18n.getLocalizedString("currencyThaiBaht");
                        }
                }
    		}	
    }
	getFeeCrmProfileInqMB();
}

function setFocusOnAmountField(){
  frmMBFTEdit.txtEditAmnt.setFocus(true);
}	
function checkeditfutureTransferSavingSPA() {
	var inputParam = [];
	showLoadingScreen();
	invokeServiceSecureAsync("tokenSwitching", inputParam, editfutureTransferSavingSPACallback);
}

function editfutureTransferSavingSPACallback(status,resulttable){
	 if (status == 400) {
		 if(resulttable["opstatus"] == 0){
			 getFeeCrmProfileInqMB();
		 }else{
			 dismissLoadingScreen();
			 alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
			}
		 }
}
function dateFormatForDisplayWithTimeStampFTMB(datetimestamp) {
    var year = datetimestamp.substring(0, 4);
    var month = datetimestamp.substring(5, 7);
    var date = datetimestamp.substring(8, 10);
    var TimeofActivation = datetimestamp.substring(11, 19);
    var formatedDatewithTimeStamp = date + "/" + month + "/" + year + " " + TimeofActivation;
    return formatedDatewithTimeStamp;
}

function isEditScheduleMB(){
    var tmpDate = formatDateFT(gblFTViewStartOnDateMB);
	if(tmpDate != frmMBFTEdit.lblViewStartOnDateVal.text){
      gblEditFTSchduleMB = true;
    }else{
      gblEditFTSchduleMB = false; 

      if(gblFTViewRepeatAsValMB != gblScheduleRepeatFTMB){
        gblEditFTSchduleMB = true ;  

      }else{
        gblEditFTSchduleMB = false;
        if(gblFTViewEndValMB != gblScheduleEndFTMB) {
          gblEditFTSchduleMB = true; 

        }else {
          gblEditFTSchduleMB = false; 
          if(gblFTViewEndValMB == kony.i18n.getLocalizedString("keyOnDate") || gblFTViewEndValMB == "OnDate"){
            var temp = gblFTViewEndDateMB;
            if(temp.indexOf("-",0) != -1){
              temp = formatDateFT(gblFTViewEndDateMB);
            }
            if(temp != frmMBFTEdit.lblEndOnDateVal.text)
              gblEditFTSchduleMB = true;
          }else if(gblFTViewEndValMB == kony.i18n.getLocalizedString("keyAfter") || gblFTViewEndValMB == "After"){
            if(parseFloat(gblFTViewExcuteValMB) != parseFloat(frmMBFTEdit.lblExcuteVal.text))
              gblEditFTSchduleMB = true;
          } 
        }
      } 
    }
	validateAmtMB();
}

/*function addReceipentFTMB(){
  isChMyRecipientsRs = true;
  frmMyRecipients.show();
}*/

function addMyAccntFTMB(){
  frmMyAccountList.show();
}


function formatDateFTMB(date){
 
 date = ""+date;
	var d = [];
    d = date.split("-");
	return d[2]+ "/" +d[1]+ "/" + d[0];
}

function checkEnteredAccountPresentEditInRecipient(productId,accountNo){
	var inputParam = {}
	inputParam["prodId"]=productId;
	inputParam["accId"]=accountNo;
invokeServiceSecureAsync("getRecipientsForTransfer", inputParam, callBackCheckEnteredAccountPresentEditInRecipient)
}

function callBackCheckEnteredAccountPresentEditInRecipient(status, resulttable) {
 	if (status == 400) //success responce
	{
		if (resulttable["opstatus"] == 0 || resulttable["OwnAccounts"].length>0) {
		EDIT_RECEIPIENT_ENABLE = true;
		}else{
			dismissLoadingScreen();
			if(kony.string.equalsIgnoreCase("No Records Found", resulttable["errMsg"])){
				EDIT_RECEIPIENT_ENABLE = false;
			}
			return false;
		}
	}else{
		EDIT_RECEIPIENT_ENABLE = false;
		dismissLoadingScreen();
		return false;
	}
}
function navigateToLoanKYC(){
  frmMBLoanKYC.show();
  
}

//Type your code here
function frmMBLoanKYCInit(){
  frmMBLoanKYC.preShow = frmMBLoanKYCPreShow;
  frmMBLoanKYC.btnBack.onClick = btnBackClickForMBLoanKYC;
  frmMBLoanKYC.onDeviceBack = onDeviceSpaback;
  frmMBLoanKYC.btnCancel.onClick = btnCancelClickForMBLoanKYC;
  frmMBLoanKYC.btnNext.onClick = navigateToCheckList;
  
  
}


// Called on Pre show of the form-- should be placed in preshowforallforms.js
function frmMBLoanKYCPreShow(){
  kony.print("gblCustomerFullNameEn @@: "+gblCustomerFullNameEn);
  kony.print("gblCustomerFullNameTh @@: "+gblCustomerFullNameTh);
  kony.print("gblCustomerDob @@: "+gblCustomerDob);
  kony.print("gblCustomerIDType @@: "+gblCustomerIDType);
  kony.print("gblCustomerIDValue @@: "+gblCustomerIDValue);
  kony.print("gblCustomerPassport @@ 1: "+gblCustomerPassport);
  
  frmMBLoanKYC.lblKYCHeading.text = kony.i18n.getLocalizedString("KYC_Heading");
  frmMBLoanKYC.lblTitle.text = kony.i18n.getLocalizedString("KYC_Title");
  frmMBLoanKYC.lblName.text = kony.i18n.getLocalizedString("KYC_Name");
  frmMBLoanKYC.lblDob.text = kony.i18n.getLocalizedString("KYC_DateofBirth");
  frmMBLoanKYC.lblCustId.text = kony.i18n.getLocalizedString("CitizenID");
  frmMBLoanKYC.lblCustIdValue.text = kony.i18n.getLocalizedString("KYC_Remark");
  frmMBLoanKYC.lblRemark.text = kony.i18n.getLocalizedString("KYC_Remark");
  frmMBLoanKYC.btnCancel.text = kony.i18n.getLocalizedString("KYC_Incorrect");
  frmMBLoanKYC.btnNext.text = kony.i18n.getLocalizedString("KYC_Correct");
  
  
  if(kony.i18n.getCurrentLocale() == "en_US"){
    frmMBLoanKYC.lblNameValue.text = gblCustomerFullNameEn;
  }else{
    frmMBLoanKYC.lblNameValue.text = gblCustomerFullNameTh;
  }
  if(isNotBlank(gblCustomerDob)){
     //var month = gblCustomerDob.substring(4,5);
     //var day = gblCustomerDob.substring(0,2);
     //var year = gblCustomerDob.substring(6,10);
     //var customerdob = day+"/"+month+"/"+year;
    frmMBLoanKYC.lblDobValue.text = formatDateFullDesc(gblCustomerDob);
  }else{
      frmMBLoanKYC.lblDobValue.text = "";
  }
  if (gblCustomerIDType == "CI") {
  	frmMBLoanKYC.lblCustIdValue.text = formatCitizenID(gblCustomerIDValue);
  }else{
    frmMBLoanKYC.lblCustIdValue.text = gblCustomerPassport;
  }
}

//This function checks whether the entered customer ID 
//matches with that from the service.
function navigateToCheckList(){
 showfrmCheckList();
}


function btnBackClickForMBLoanKYC(){
  kony.print("Back clicked @@");
  frmLoanProductDetails.show();
}

function btnCancelClickForMBLoanKYC(){
  kony.print("Cancel clicked @@");
  //alert("Cancel clicked @@");
  frmLoanProductList.show();
}

function callBackOfLoanKYConAlert(){
  kony.print("callBackOfLoanKYConAlert function");
}
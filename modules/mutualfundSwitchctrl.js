//mutual funds business rules and UI wiring


function frmMFSwitchConfirmMBInitiatization(){
  try{
  frmMFSwitchConfirmMB.preShow = frmMFSwitchConfirmMBPreShow;
  frmMFSwitchConfirmMB.postShow = addAccessPinKeypad(frmMFSwitchConfirmMB);
  frmMFSwitchConfirmMB.btnConfirm.onClick = frmSwitchMFConfirmMBOnClickConfirm;
  frmMFSwitchConfirmMB.btnBack.onClick = frmSwitchMFConfirmMBOnClickBack;
  frmMFSwitchConfirmMB.btnCancel.onClick = onClickCancelSwitchMF;
  frmMFSwitchConfirmMB.onDeviceBack = disableBackButton;
  }catch(e){
    kony.print("Exception in frmMFSwitchConfirmMBInitiatization "+e);
  }
}


function frmMFSwitchCompleteMBInitiatization(){
  try{
  frmMFSwitchCompleteMB.preShow = frmMFSwitchCompleteMBPreShow;
  frmMFSwitchCompleteMB.postShow = addAccessPinKeypad(frmMFSwitchConfirmMB);
  frmMFSwitchCompleteMB.btnConfirm.onClick = frmMFSwitchCompleteMBOnClickConfirm;
  frmMFSwitchCompleteMB.btnCancel.onClick = onClickCancelSwitchMF;
  frmMFSwitchCompleteMB.onDeviceBack = disableBackButton;
  }catch(e){
    kony.print("Exception in frmMFSwitchCompleteMBInitiatization "+e);
  }
}

function frmMFSwitchFailedMBInitiatization(){
  try{
  frmMFSwitchFailedMB.preShow = frmMFSwitchFailedMBPreShow;
  frmMFSwitchFailedMB.postShow = addAccessPinKeypad(frmMFSwitchFailedMB);
  frmMFSwitchFailedMB.btnConfirm.onClick = frmMFSwitchFailedMBOnClickConfirm;
  frmMFSwitchFailedMB.btnCancel.onClick = onClickCancelSwitchMF;
  frmMFSwitchFailedMB.onDeviceBack = disableBackButton;
  }catch(e){
    kony.print("Exception in frmMFSwitchFailedMBInitiatization");
  }
}


function frmMFSwitchConfirmMBPreShow(){

  // set all i18n and copy data
  try{
    var amountUnit = "";
    var locale = kony.i18n.getCurrentLocale();
    frmMFSwitchConfirmMB.lblHeader.text = kony.i18n.getLocalizedString("MF_SW_04-Confirm");
    frmMFSwitchConfirmMB.btnCancel.text = kony.i18n.getLocalizedString("MF_SW_04-Cancel");
    frmMFSwitchConfirmMB.btnConfirm.text = kony.i18n.getLocalizedString("MF_SW_04-Confirm");
    frmMFSwitchConfirmMB.lblEffectiveDateText.text = kony.i18n.getLocalizedString("MF_SW_04-Effdate");
    frmMFSwitchConfirmMB.lblTransDateText.text = kony.i18n.getLocalizedString("MF_SW_04-Transdate");
    frmMFSwitchConfirmMB.lblSwitchingMethodText.text = kony.i18n.getLocalizedString("MF_SW_04-SwitchMethod");
    frmMFSwitchConfirmMB.lblTargetFundText.text = kony.i18n.getLocalizedString("MF_SW_04-Target");
    frmMFSwitchConfirmMB.lblCutOffTime.text = kony.i18n.getLocalizedString("MF_SW_04-Cutoff");
    frmMFSwitchConfirmMB.lblFundSourceTxt.text = kony.i18n.getLocalizedString("MF_SW_04-FundSource");
    frmMFSwitchConfirmMB.lblBalance.text = kony.i18n.getLocalizedString("MF_SW_04-Unit");
     
    copyDataToConfirmScreen();

  }catch(e){
    kony.print("error in frmMFSwitchConfirmMBPreShow "+e);
  }

}

function frmSwitchMFConfirmMBOnClickConfirm(){
try{
  saveToSessionSwitchMFOrder();
  showSwitchMutualFundsPwdPopupForProcess();
}catch(e){
  kony.print("Exception in frmSwitchMFConfirmMBOnClickConfirm");
}
}

function frmSwitchMFConfirmMBOnClickBack(){
  try{
    kony.print("inside frmSwitchMFConfirmMBOnClickBack");
    frmMFSwitchLanding.show();

  }catch(e){
    kony.print("error in frmSwitchMFConfirmMBOnClickBack "+e);
  }


}

function frmMFSwitchCompleteMBPreShow(){

  // set all i18n and copy data
  try{
    var amountUnit = "";
    var locale = kony.i18n.getCurrentLocale();
    frmMFSwitchCompleteMB.lblHeader.text = kony.i18n.getLocalizedString("MF_SW_04-Finish");
    frmMFSwitchCompleteMB.btnCancel.text = kony.i18n.getLocalizedString("MF_SW_04-Home");
    frmMFSwitchCompleteMB.btnConfirm.text = kony.i18n.getLocalizedString("MF_SW_04-MyMutualFunds");
    frmMFSwitchCompleteMB.lblFundCompleted.text = kony.i18n.getLocalizedString("MF_SW_04-Msg");
    frmMFSwitchCompleteMB.lblEffectiveDateText.text = kony.i18n.getLocalizedString("MF_SW_04-Effdate");
    frmMFSwitchCompleteMB.lbltranDatetext.text = kony.i18n.getLocalizedString("MF_SW_04-Transdate");
    frmMFSwitchCompleteMB.lblSwitchingMethod.text = kony.i18n.getLocalizedString("MF_SW_04-SwitchMethod");
    frmMFSwitchCompleteMB.lblTargetFundText.text = kony.i18n.getLocalizedString("MF_SW_04-Target");
    frmMFSwitchCompleteMB.lblCutOffTime.text = kony.i18n.getLocalizedString("MF_SW_04-Cutoff");
    frmMFSwitchCompleteMB.lblFundSourceText.text = kony.i18n.getLocalizedString("MF_SW_04-FundSource");
    frmMFSwitchCompleteMB.lblBalance.text = kony.i18n.getLocalizedString("MF_SW_04-Unit");
    frmMFSwitchCompleteMB.lblBalance.text = kony.i18n.getLocalizedString("MF_SW_04-Unit");
    copyDataToCompleteScreen();
    

  }catch(e){
    kony.print("error in frmMFSwitchConfirmMBPreShow "+e);
  }

}

function frmMFSwitchCompleteMBOnClickConfirm(){
  try{
    kony.print("inside frmMFSwitchCompleteMBOnClickConfirm");
    MBcallMutualFundsSummary();

  }catch(e){
    kony.print("error in frmMFSwitchCompleteMBOnClickConfirm "+e);
  }


}

function frmMFSwitchCompleteMBOnClickHome(){
  try{
    kony.print("inside frmMFSwitchCompleteMBOnClickHome");
showAccountSummaryFromMenu();

  }catch(e){
    kony.print("error in frmMFSwitchCompleteMBOnClickHome "+e);
  }


}


function frmMFSwitchFailedMBPreShow(){

  // set all i18n and copy data
  try{
    frmMFSwitchFailedMB.lblHeader.text = kony.i18n.getLocalizedString("MF_SW_04-Failed");
    frmMFSwitchFailedMB.btnCancel.text = kony.i18n.getLocalizedString("MF_SW_04-Home");
    frmMFSwitchFailedMB.btnConfirm.text = kony.i18n.getLocalizedString("MF_SW_04-MyMutualFunds");
    frmMFSwitchFailedMB.lblFundCompleted.text = kony.i18n.getLocalizedString("MF_SW_04-FailedMessage");  
    animateIdAndSelfieImagesMF(frmMFSwitchFailedMB.flxArrow, 45);

  }catch(e){
    kony.print("error in frmMFSwitchFailedMBPreShow "+e);
  }

}

function frmMFSwitchFailedMBOnClickConfirm(){
  try{
    kony.print("inside frmMFSwitchFailedMBOnClickConfirm");
    MBcallMutualFundsSummary();

  }catch(e){
    kony.print("error in frmMFSwitchFailedMBOnClickConfirm "+e);
  }


}

function frmMFSwitchFailedMBOnClickHome(){
  try{
    kony.print("inside frmMFSwitchFailedMBOnClickHome");

showAccountSummaryFromMenu();
  }catch(e){
    kony.print("error in frmMFSwitchFailedMBOnClickHome "+e);
  }


}

function onClickCancelSwitchMF(){

  try{
    kony.print("inside onClickCancelSwitchMF");
    MBcallMutualFundsSummary();

  }catch(e){
    kony.print("error in onClickCancelSwitchMF "+e);
  }
}


function copyDataToConfirmScreen(){
  try{
    frmMFSwitchConfirmMB.lblPortfolioName.text =frmMFSwitchLanding.lblPortfolioName.text ;
    frmMFSwitchConfirmMB.lblPortfolioCode.text =frmMFSwitchLanding.lblPortfolioCode.text ;
    
    frmMFSwitchConfirmMB.lblTargetFundType.text =frmMFSwitchLanding.lblTargetFundType.text ;
    
    frmMFSwitchConfirmMB.lbltransactionDate.text =transactionDate ;
    frmMFSwitchConfirmMB.lblEffectiveDate.text = effectiveDate;
    if(gblMFSwitchOrder["redeemUnit"]== ORD_UNIT_BAHT)
    	frmMFSwitchConfirmMB.lblSwitchingUnits.text =gblMFSwitchOrder["amount"]+" "+kony.i18n.getLocalizedString("MF_SW_03-Baht") ;
    else
      frmMFSwitchConfirmMB.lblSwitchingUnits.text =gblMFSwitchOrder["amount"]+" "+kony.i18n.getLocalizedString("MF_SW_03-Units") ;
    frmMFSwitchConfirmMB.lblCutOffTime.text =frmMFSwitchConfirmMB.lblCutOffTime.text +" "+frmMFSwitchLanding.lblCutOffTime.text ;
    frmMFSwitchConfirmMB.lblBalance.text =frmMFSwitchConfirmMB.lblBalance.text.replace("XXX",frmMFSwitchLanding.lblBalance.text) ;

  }catch(e){
    kony.print("error in copyDataToConfirmScreen "+e);
  }
}


function copyDataToCompleteScreen(){
  try{
    frmMFSwitchCompleteMB.lblPortfolioName.text =frmMFSwitchLanding.lblPortfolioName.text ;
    frmMFSwitchCompleteMB.lblPortfolioCode.text =frmMFSwitchLanding.lblPortfolioCode.text ;
    
    frmMFSwitchCompleteMB.lblTargetFundType.text =frmMFSwitchLanding.lblTargetFundType.text ;
    frmMFSwitchCompleteMB.lbltransactionDate.text =transactionDate ;
    frmMFSwitchCompleteMB.lblEffectiveDate.text = effectiveDate;
    
     if(gblMFSwitchOrder["redeemUnit"]== ORD_UNIT_BAHT)
    	frmMFSwitchCompleteMB.lblSwitchingUnits.text =gblMFSwitchOrder["amount"]+" "+kony.i18n.getLocalizedString("MF_SW_03-Baht") ;
    else
      frmMFSwitchCompleteMB.lblSwitchingUnits.text =gblMFSwitchOrder["amount"]+" "+kony.i18n.getLocalizedString("MF_SW_03-Units") ;
    frmMFSwitchCompleteMB.lblCutOffTime.text =frmMFSwitchConfirmMB.lblCutOffTime.text +" "+frmMFSwitchLanding.lblCutOffTime.text ;
    frmMFSwitchCompleteMB.lblBalance.text =frmMFSwitchConfirmMB.lblBalance.text.replace("XXX",frmMFSwitchLanding.lblBalance.text) ;
    

  }catch(e){
    kony.print("error in copyDataToCompleteScreen "+e);
  }
}

function checkValidSwitchAmountMB(){
  try{
    kony.print("checkValidSwitchAmountMB ::: start");
    var orderUnit = 0.00;
    var errMsg = "";
    var avaiUnit = parseFloat(gblMFSwitchOrder["redeemableUnit"]);
    var avaiAmount = parseFloat(removeCommos(gblMFSwitchOrder["InvestmentValue"]));

    if(frmMFSwitchLanding.txtSwitchBalance.text == ""){
      orderUnit = 0.00;
    } else {
      orderUnit = parseFloat(removeCommos(frmMFSwitchLanding.txtSwitchBalance.text));
    }

    if(gblMFSwitchOrder["Nav"] != ""){
      nav = parseFloat(removeCommos(gblMFSwitchOrder["Nav"]));
      avaiAmount = parseFloat(gblMFSwitchOrder["redeemableUnit"]) * nav;
    }  

    if(frmMFSwitchLanding.btnUnits.skin == "btnBlueBGMF"){
      gblMFSwitchOrder["amount"] = numberFormat(orderUnit, 4);
      errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_UNIT,nav);
      gblMFSwitchOrder["redeemUnit"] = ORD_UNIT_UNIT;
    } else if(frmMFSwitchLanding.btnBaht.skin == "btnBlueBGMF"){
      gblMFSwitchOrder["amount"] = numberFormat(orderUnit, 2);
      errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_BAHT,nav);
      gblMFSwitchOrder["redeemUnit"] = ORD_UNIT_BAHT;
    } else{
      gblMFSwitchOrder["amount"] = numberFormat(orderUnit, 4);
      errMsg = validateRedeemOrder(avaiUnit,avaiAmount, orderUnit, ORD_UNIT_ALL,nav);
      gblMFSwitchOrder["redeemUnit"] = ORD_UNIT_ALL;
    }
    if(errMsg != ""){
      kony.print("checkValidSwitchAmountMB ::: shwoing alert");
      showAlert(errMsg, kony.i18n.getLocalizedString("info"));
      frmMFSwitchLanding.txtSwitchBalance.text = ""; //mki, MIB 9746
      frmMFSwitchLanding.txtSwitchBalance.setFocus(true);
    } else {
    
       kony.print(" transactionDate ", gblMFSwitchData["transactionDate"] + "effectiveDate  " +gblMFSwitchData["effectiveDate"] + "allowTransFlag " +gblMFSwitchData["allowTransFlag"] )

       if(gblMFSwitchData["allowTransFlag"] == '4'){
          // showAlert(kony.i18n.getLocalizedString("MFSW01-001"), kony.i18n.getLocalizedString("info"));
         
        var basicConf = {
            message: kony.i18n.getLocalizedString("MFSW01-001"),
            alertType: constants.ALERT_TYPE_INFO,
            alertTitle: kony.i18n.getLocalizedString("info"),
            yesLabel: okk,
            noLabel: "",
            alertHandler: handle2
        };
        var pspConf = {};
        var infoAlert = kony.ui.Alert(basicConf, pspConf);

        function handle2(response) {
          MBcallMutualFundsSummary();
        }
        return;

         
       }

      getMutualFundTnC();
    }
    kony.print("checkValidSwitchAmountMB ::: end");
  }catch(e){
    kony.print("error in checkValidSwitchAmountMB "+e);
  }
}





function showSwitchMutualFundsPwdPopupForProcess() {
  try{
  var lblText = kony.i18n.getLocalizedString("transPasswordSub");
  var refNo = "";
  var mobNO = "";
  if(gblAuthAccessPin == true){
    showAccesspinPopup();
  }else{
    showLoadingScreen();
    showOTPPopup(lblText, refNo, mobNO, onClickSwitchMutualFundsConfirmPop, 3);  
  }}catch(e){
    kony.print("error in showSwitchMutualFundsPwdPopupForProcess "+e);
  }

}

function onClickSwitchMutualFundsConfirmPop(tranPassword) {
  try{
  if (isNotBlank(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text)) {
    showLoadingScreen(); 
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
    popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;

    MBconfirmSwitchMFOrder(tranPassword);	


  } else{
    setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
    return false;
  }
  }catch(e){
    kony.print("error in onClickSwitchMutualFundsConfirmPop "+e);
  }
}

function MBconfirmSwitchMFOrder(tranPassword) {
try{
  if (tranPassword == null || tranPassword == '') {
    setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
    return false;
  }
  else
  {
    //showLoadingScreen();

    popupTractPwd.show();
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
    popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;

    MBconfirmSwitchMFOrderCompositeJavaService(tranPassword);
  }
}catch(e){
  kony.print("error in MBconfirmSwitchMFOrder "+e);
}
}


function callBackOnConfirmOverTimeOrderSwitch(){
  try{
  popupConfirmation.dismiss();

  MutualFundTnC();
  }catch(e){
    kony.print("error in callBackOnConfirmOverTimeOrderSwitch "+e);
  }
}



function animateIdAndSelfieImagesMF(frmName, rotationAngle){
  try{
                var trans100 = kony.ui.makeAffineTransform();
    trans100.rotate(rotationAngle);
    frmName.animate(
    kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.001
    }, {
        "animationEnd": null
    });
  }catch(e){
    kony.print("error in animateIdAndSelfieImagesMF "+e);
  }
}






function frmTransferLandingMenuPreshow() { 
  loadFunctionalModuleSync("myRecipientsModule");
  if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator" || gblDeviceInfo["name"] == "iPad"){
    frmTransferLanding.segTransFrm.viewConfig = {coverflowConfig: {isCircular: true}};
  }
	if(gblCallPrePost)
	{
		ehFrmTransferLanding_frmTransferLanding_preshow.call(this);
	}
}

function frmTransferLandingMenuPostshow() {
    gblTransferNextClick = false; 
	if(gblCallPrePost)
	{
       
		ehFrmTransferLanding_frmTransferLanding_postshow.call(this);
     //#ifdef iphone
       if(frmTransferLanding.hbxCitizenID.isVisible){
          frmTransferLanding.txtCitizenID.setFocus(true)
          }
      else if(frmTransferLanding.hbxOnUsInputMobileNo.isVisible){
         // frmTransferLanding.txtOnUsMobileNo.setFocus(true)
          }
       //#endif
      }
    
	assignGlobalForMenuPostshow();
    
}

function frmTransferConfirmMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmTransferConfirm_frmTransferConfirm_preshow.call(this);
	}
}

function frmTransferConfirmMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmTransfersAckMenuPreshow() {
	if(gblCallPrePost)
	{
		initialFrmTransfersAckPreShow.call(this);
    	ehFrmTransfersAck_frmTransfersAck_preshow.call(this);
	}
}

function frmTransfersAckMenuPostshow() {
	if(gblCallPrePost)
	{
		ehFrmTransfersAck_frmTransfersAck_postshow.call(this);
	}
	assignGlobalForMenuPostshow();
}

function frmTransfersAckCalendarMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmTransfersAckCalendar_frmTransfersAckCalendar_preshow.call(this);
	}
}

function frmTransfersAckCalendarMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmTransferToRecipentsMobileMenuPreshow() {
	if(gblCallPrePost)
	{
		frmTranfersToRecipentsMobilePreshow.call(this);
	}
}

function frmTransferToRecipentsMobileMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmPreTransferMB2MenuPreshow() {
	if(gblCallPrePost)
	{
		preShowfrmPreTransferMB2.call(this);
	}
}

function frmPreTransferMBMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmTranfersToRecipentsMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmTranfersToRecipents_frmTranfersToRecipents_preshow.call(this);
	}
}

function frmTranfersToRecipentsMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMBFTViewMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFTView_frmMBFTView_preshow.call(this);
	}
}

function frmMBFTViewMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMBFTEditCmpleteMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFTEditCmplete_frmMBFTEditCmplete_preshow.call(this);
	}
}

function frmMBFTEditCmpleteMenuPostshow() {
	if(gblCallPrePost)
	{
        frmMBFTEditCmplete.lblAmountValueHeader.text = frmMBFTEditCnfrmtn.lblFTViewAmountVal.text;
           frmMBFTEditCmplete.lblFeeValueHeader.text = "("+frmMBFTEditCnfrmtn.lblFeeVal.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim()+" "+kony.i18n.getLocalizedString("currencyThaiBaht")+")"
           frmMBFTEditCmplete.lblFTViewAmountVal.text = frmMBFTEditCnfrmtn.lblFTViewAmountVal.text + frmMBFTEditCmplete.lblFeeValueHeader.text
		   ehFrmMBFTEditCmplete_frmMBFTEditCmplete_postshow.call(this);
           frmMBFTEditCmplete.imgFrmAccnt.src =frmMBFTEdit.imgFrmAccnt.src; 
      	   frmMBFTEditCmplete.imgToAccnt.src =frmMBFTEdit.imgToAccnt.src;
	}
	assignGlobalForMenuPostshow();
}

function frmMBFTEditCmpleteMenuInit(){
  		frmMBFTEditCmplete.btnReturn1.onClick = MBMyActivitiesReloadAndShowCalendar;
  		frmMBFTEditCmplete.btnTransferMore.onClick = makeAnotherTransfer;
  	 	frmMBFTEditCmplete.hbxImageAddRecipient.onClick =  onClickAddToMyRecipient;
     	frmMBFTEditCmplete.btnRight.onClick = onClickshareFTCompleteScreen;
    	frmMBFTEditCmplete.vbxImage.onClick = onClickShareImageFTCompleteScreen;
  		frmMBFTEditCmplete.vbxMessenger.onClick = onClickMessengerImageFTCompleteScreen;
  		frmMBFTEditCmplete.vbxLine.onClick = onClickShareLineFTCompleteScreen;
  		frmMBFTEditCmplete.vbxOthers.onClick = onClickshareOthersFTCompleteScreen;
        frmMBFTEditCmplete.btnHdrMenu.onClick = handleMenuBtn;
    
}
function onClickShareImageFTCompleteScreen(){
  //#ifdef iphone
  		screenShotCall("screenshot");
  //#endif 
  //#ifdef android
      	shareIntentmoreCallandroid("screenshot","Transfer");
  //#endif 
  
}

function onClickMessengerImageFTCompleteScreen(){
  shareIntentCall("facebook","Transfer");
  
  
}


function onClickShareLineFTCompleteScreen(){
  shareIntentCall("line","Transfer");
  
  
}

function onClickshareOthersFTCompleteScreen(){
  //#ifdef iphone
  	shareIntentmoreCall("more", "Transfer");
   //#endif
  //#ifdef android
  	shareIntentmoreCallandroid("more", "Transfer");
    //#endif
}
 
function onClickshareFTCompleteScreen (){
  	if(frmMBFTEditCmplete.imgComplete.src == "iconnotcomplete.png" || frmMBFTEditCmplete.imgComplete.src == "mes2.png")
	{
		alert(kony.i18n.getLocalizedString("Err_ShareFailed"));
		return;
	}
	else
	{
		if(frmMBFTEditCmplete.hbxShareOption.isVisible){
			frmMBFTEditCmplete.hbxShareOption.isVisible = true;
			frmMBFTEditCmplete.imgHeaderMiddle.src = "arrowtop.png";
		}else{
			frmMBFTEditCmplete.hbxShareOption.isVisible = true;
			frmMBFTEditCmplete.imgHeaderMiddle.src = "empty.png";
		}	
	}	
  
}
function frmMBFTEditCnfrmtnMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFTEditCnfrmtn_frmMBFTEditCnfrmtn_preshow.call(this);
	}
}

function frmMBFTEditCnfrmtnMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMBFTEditMenuPreshow() {
  	//#ifdef android    
  	//mki, mib-10789 Start
	// The below code snippet is to fix the whilte label on some screens
	frmMBFTEdit.FlexScrollContainer0eda206efda9d44.showFadingEdges  = false;
	//#endif  
	//mki, mib-10789 End
	if(gblCallPrePost)
	{
		ehFrmMBFTEdit_frmMBFTEdit_preshow.call(this);
	}
}

function frmMBFTEditMenuPostshow() {
	//assignGlobalForMenuPostshow();
    addAccessPinKeypad(frmMBFTEdit);
}

function frmMBFtScheduleMenuPreshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFtSchedule_frmMBFtSchedule_preshow.call(this);
	}
}

function frmMBFtScheduleMenuPostshow() {
	if(gblCallPrePost)
	{
		ehFrmMBFtSchedule_frmMBFtSchedule_postshow.call(this);
	}
	assignGlobalForMenuPostshow();
}


function onClickSaveimageTransCal(eventobject){
   if(frmTransfersAckCalendar.lblhidden.text == "QR"){
      gblTransferCompleteShareFlow = "saveimage"
      generateQRforTransRefIdTransCal();
   }else{
     onClickSaveimageTransCalGenCode(eventobject);
   }
}
function onClickMesTransCal(eventobject){
   if(frmTransfersAckCalendar.lblhidden.text == "QR"){
       gblTransferCompleteShareFlow = "messenger"
      generateQRforTransRefIdTransCal();
   }else{
     onClickMesTransCalGenCode(eventobject);
   }
}
function onClickLineTransCal(eventobject){
   if(frmTransfersAckCalendar.lblhidden.text == "QR"){
       gblTransferCompleteShareFlow = "line"
      generateQRforTransRefIdTransCal();
   }else{
     onClickLineTransCalGenCode(eventobject);
   }
}
function onClickOthersTransCal(eventobject){
   if(frmTransfersAckCalendar.lblhidden.text == "QR"){
       gblTransferCompleteShareFlow = "others"
      generateQRforTransRefIdTransCal();
   }else{
     onClickOthersTransCalGenCode(eventobject);
   }
}

function onClickSaveimageTransCalGenCode(eventobject){
    //#ifdef iphone
    //#define preprocessdecision_onClick_81173201224764339_iphone
    //#endif
    //#ifdef preprocessdecision_onClick_81173201224764339_iphone
    screenShotCall.call(this, "screenshot");
    //#endif
    //#ifdef android
    //#define preprocessdecision_onClick_20447201224762862_android
    //#endif
    //#ifdef preprocessdecision_onClick_20447201224762862_android
    shareIntentmoreCallandroid.call(this, "screenshot", "Transfer");
    //#endif
}

function onClickMesTransCalGenCode(eventobject){
    return shareIntentCall.call(this, "facebook", "Transfer");

}

function onClickLineTransCalGenCode(eventobject){
    return shareIntentCall.call(this, "line", "Transfer");
  

}

function onClickOthersTransCalGenCode(eventobject){
    //#ifdef iphone
    //#define preprocessdecision_onClick_17922201224762730_iphone
    //#endif
    //#ifdef preprocessdecision_onClick_17922201224762730_iphone
    shareIntentmoreCall.call(this, "more", "Transfer");
    //#endif
    //#ifdef android
    //#define preprocessdecision_onClick_69210201224767855_android
    //#endif
    //#ifdef preprocessdecision_onClick_69210201224767855_android
    shareIntentmoreCallandroid.call(this, "more", "Transfer");
    //#endif

}

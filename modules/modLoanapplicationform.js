/*
************************************************************************
            Name    : Handle Loan Apllication form
            Author  : Brajesh Kumar
            Date    : March 014, 2018
            Purpose : Loan Apllication form information
************************************************************************
 */


//gblPersonalInfo.savePI = false;
function initfrmLoanApplication(){
  
  frmLoanApplicationForm.preShow=preshowfrmLoanApplication;
  frmLoanApplicationForm.onDeviceBack=onDeviceSpaback;
  frmLoanApplicationForm.flexPersonalInfo.onClick = navigateToLoanPersonalInfo;
}
  
function preshowfrmLoanApplication() {
    try {
        frmLoanApplicationForm.btnBack.onClick = onClickbackloanAppform;
        frmLoanApplicationForm.btnCancel.onClick = navigateBackToSubProducts;
        frmLoanApplicationForm.btnBack.onClick = onClickbackloanAppform;
        frmLoanApplicationForm.btnCancel.onClick = navigateBackToSubProducts;
        frmLoanApplicationForm.flxPersonalLblCircle.skin = "flexWhiteBG";
		frmLoanApplicationForm.flxWorkingCirle.skin = "flexWhiteBG";
		frmLoanApplicationForm.flxIncomeCirle.skin = "flexWhiteBG";
		frmLoanApplicationForm.flxFacilityCirle.skin = "flexWhiteBG";
		
		frmLoanApplicationForm.lblWorkingCircle.skin = "skingreyCircle";
		frmLoanApplicationForm.lblIncomeCircle.skin = "skingreyCircle";
		frmLoanApplicationForm.lblFacilityCircle.skin = "skingreyCircle";
		
		frmLoanApplicationForm.lblWorkInfo.skin = "lblgrey24Px9b9b9b";
		frmLoanApplicationForm.lblIncomeInfo.skin = "lblgrey24Px9b9b9b";
		frmLoanApplicationForm.lblFacilityInfo.skin = "lblgrey24Px9b9b9b";
		
		frmLoanApplicationForm.imgIconWorkingInfo.skin = "skingreyArrowright";
		frmLoanApplicationForm.imgArrowIncome.skin = "skingreyArrowright";
		frmLoanApplicationForm.imgArrowFacility.skin = "skingreyArrowright";
		
		frmLoanApplicationForm.btnNext.onClick = onDeviceSpaback;
		frmLoanApplicationForm.btnNext.skin = "btnGreyBGNoRoundLoan";
		// gblPersonalInfo.savePI=true;
		if (!gblPersonalInfo.savePI) {
			frmLoanApplicationForm.lblPersonalCircle.skin = "skinpiorangeLineLone";
			frmLoanApplicationForm.flexWorkInfo.onClick = onDeviceSpaback;
			frmLoanApplicationForm.flexIncomeInfo.onClick = onDeviceSpaback;
			frmLoanApplicationForm.flexFacilityInfo.onClick = onDeviceSpaback;
		
			gblPersonalInfo.saveWI = false;
			gblPersonalInfo.saveIncome = false;
			gblPersonalInfo.saveFacility = false;
		} else if (!gblPersonalInfo.saveWI) {
			frmLoanApplicationForm.lblPersonalCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.lblWorkingCircle.skin = "skinpiorangeLineLone";
			
			frmLoanApplicationForm.flexWorkInfo.onClick = openWorkingInfoFlex;
			frmLoanApplicationForm.flexIncomeInfo.onClick = onDeviceSpaback;
			frmLoanApplicationForm.flexFacilityInfo.onClick = onDeviceSpaback;
			frmLoanApplicationForm.lblWorkInfo.skin = "lblblue24px007abc";
			frmLoanApplicationForm.imgIconPersonalInfo.skin = "skinblueArrowright";
			frmLoanApplicationForm.imgIconWorkingInfo.skin = "skinblueArrowright";
			gblPersonalInfo.saveIncome = false;
			gblPersonalInfo.saveFacility = false;
		} else if (!gblPersonalInfo.saveIncome) {
			frmLoanApplicationForm.lblPersonalCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.flxPersonalLblCircle.skin = "flxGreenCircleLoan";
			
			frmLoanApplicationForm.lblWorkingCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.lblIncomeCircle.skin = "skinpiorangeLineLone";
			
			frmLoanApplicationForm.lblWorkInfo.skin = "lblblue24px007abc";
			frmLoanApplicationForm.lblIncomeInfo.skin = "lblblue24px007abc";
			
			frmLoanApplicationForm.imgIconWorkingInfo.skin = "skinblueArrowright";
			frmLoanApplicationForm.imgArrowIncome.skin = "skinblueArrowright";
			
		
			frmLoanApplicationForm.flexWorkInfo.onClick = openWorkingInfoFlex;
			frmLoanApplicationForm.flexIncomeInfo.onClick = onClickIncomeInformation;
			frmLoanApplicationForm.flexFacilityInfo.onClick = onDeviceSpaback;
		
			if (gblPersonalInfo.saveFacility) {
				frmLoanApplicationForm.lblFacilityCircle.skin = "skinpiGreenLineLone";
			}
			frmLoanApplicationForm.imgArrowIncome.skin = "skinblueArrowright";
		} else if (!gblPersonalInfo.saveFacility) {
			frmLoanApplicationForm.lblPersonalCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.flxPersonalLblCircle.skin = "flxGreenCircleLoan";
			
			frmLoanApplicationForm.lblWorkingCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.lblIncomeCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.lblFacilityCircle.skin = "skinpiorangeLineLone";
			frmLoanApplicationForm.lblWorkInfo.skin = "lblblue24px007abc";
			frmLoanApplicationForm.lblIncomeInfo.skin = "lblblue24px007abc";
			frmLoanApplicationForm.lblFacilityInfo.skin = "lblblue24px007abc";
			frmLoanApplicationForm.imgIconWorkingInfo.skin = "skinblueArrowright";
			frmLoanApplicationForm.imgArrowIncome.skin = "skinblueArrowright";
			frmLoanApplicationForm.imgArrowFacility.skin = "skinblueArrowright";
			
			
			frmLoanApplicationForm.flexWorkInfo.onClick = openWorkingInfoFlex;
			frmLoanApplicationForm.flexIncomeInfo.onClick = onClickIncomeInformation;
			frmLoanApplicationForm.flexFacilityInfo.onClick = onClickFacilityInfo;
			
		} else if (gblPersonalInfo.savePI && gblPersonalInfo.saveWI && gblPersonalInfo.saveIncome && gblPersonalInfo.saveFacility) {
			frmLoanApplicationForm.lblPersonalCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.lblWorkingCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.lblIncomeCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.lblFacilityCircle.skin = "skinpiGreenLineLone";
			frmLoanApplicationForm.lblWorkInfo.skin = "lblblue24px007abc";
			frmLoanApplicationForm.lblIncomeInfo.skin = "lblblue24px007abc";
			frmLoanApplicationForm.lblFacilityInfo.skin = "lblblue24px007abc";
			frmLoanApplicationForm.imgIconWorkingInfo.skin = "skinblueArrowright";
			frmLoanApplicationForm.imgArrowIncome.skin = "skinblueArrowright";
			frmLoanApplicationForm.imgArrowFacility.skin = "skinblueArrowright";
			frmLoanApplicationForm.btnNext.skin = "btnBlueBGNoRound";
			
			frmLoanApplicationForm.flexWorkInfo.onClick = openWorkingInfoFlex;
			frmLoanApplicationForm.flexIncomeInfo.onClick = onClickIncomeInformation;
			frmLoanApplicationForm.flexFacilityInfo.onClick = onClickFacilityInfo;
			frmLoanApplicationForm.btnNext.onClick = showUploadDcumentPage;
		}
		frmLoanApplicationForm.lblApplicationForm.text = kony.i18n.getLocalizedString("ApplicationForm_Title");
		kony.print("Test poing 4");
		frmLoanApplicationForm.lblPeronsalInfo.text = kony.i18n.getLocalizedString("Personal_Info");
		frmLoanApplicationForm.lblWorkInfo.text = kony.i18n.getLocalizedString("Working_Info");
		frmLoanApplicationForm.lblIncomeInfo.text = kony.i18n.getLocalizedString("Income_Info");
		frmLoanApplicationForm.lblFacilityInfo.text = kony.i18n.getLocalizedString("Facility_Info");
		frmLoanApplicationForm.btnCancel.text = kony.i18n.getLocalizedString("MF_RDM_33");
		frmLoanApplicationForm.btnNext.text = kony.i18n.getLocalizedString("MF_RDM_6");
		kony.print("gblPersonalInfo : " + JSON.stringify(gblPersonalInfo));
	} catch (e) {
		kony.print("Exception  " + e);
		//alert("Exception in preshowfrmLoanApplication "+e);
	}
}

function showfrmLoanApplication(){
  gblPersonalInfo = {};
  frmLoanWorkInfo.destroy();
  frmLoanPersonalInfo.destroy();
  getPersonalInfoFields();
}

function onClickIncomeInformation(){
  navigateToLoanInformationInfo();
}
function onClickFacilityInfo(){
  onClickOfImgArrow();
  //frmFacilityInformation.show();
}
function onClickbackloanAppform(){
  if(frmLetterConsent.richtxtPersonalInfo.text != ""|| frmLetterConsent.richtxtPersonalInfo.text != null ){
    navigateBackToSubProducts();
  }else{
     frmLetterConsent.show();
  }
}

function getPersonalInfoFields(){
  	inputParam = {};
 	//Need to delete below line after testing
  	//gblCaId = "2018060601018288";
  	inputParam["caId"] = gblCaId;
	showLoadingScreen();
	invokeServiceSecureAsync("getPersonalInfoFields", inputParam, callbackOfgetPersonalInfoFields);
}

function callbackOfgetPersonalInfoFields(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {
        dismissLoadingScreen();
        var locale = kony.i18n.getCurrentLocale();
        kony.print("  result.personalInfoSavedFlag "+ result.personalInfoSavedFlag );
        kony.print("  result.personalInfoSavedFlag "+ result.empInfoSavedFlag );
        kony.print("  result.personalInfoSavedFlag "+ result.incomeInfoSavedFlag );
        kony.print("  result.facOrCardInfoSavedFlag "+ result.facOrCardInfoSavedFlag);
        gblPersonalInfo.savePI = (result.personalInfoSavedFlag == "Y" ? true : false);
        gblPersonalInfo.saveWI = (result.empInfoSavedFlag == "Y" ? true : false);
        gblPersonalInfo.saveIncome = (result.incomeInfoSavedFlag == "Y" ? true : false);
       gblPersonalInfo.saveFacility=(result.facOrCardInfoSavedFlag == "Y" ? true : false);; 
       var cardImg="sofast.png"; 
        if(result.product =="VP"){
          cardImg="sofast.png"; 
        }else if(result.product=="MS"){
          cardImg="sochill.png"; 
        }else if(result.product == "VM"){
          cardImg="sosmart.png"; 
        }else if(result.product =="VT"){
          cardImg="topbrass.png"; 
        }else if(result.product== "RC"){
          cardImg="ready.png"; 
        }else if(result.product == "C2G"){
          cardImg="cashtogo.png"; 
        }
        
      	frmLoanApplicationForm.Image0h3636ea173d74e.src = cardImg;
        frmLoanPersonalInfo.lblResidentStatusVal.text = result.residentTypeTH;
        frmLoanPersonalInfo.lblSalVal.text = result.thaiSalTH !== "" ? result.thaiSalTH : "- -";
        if("th_TH" === locale){
          frmLoanApplicationForm.lblSubProdName.text = result.productDescTH;
          frmLoanApplicationForm.lblSubPrdDesc.text = result.subProductDescTH;
          
         // frmLoanPersonalInfo.lblTitleTypeVal.text = result.titleTypeTH !== "" ? result.titleTypeTH : "-";

          frmLoanPersonalInfo.lblSourceCountry.text = result.countryOfIncomeTH;
          //frmLoanPersonalInfo.lblResidentStatusVal.text = result.residentTypeTH;
        }else{
          frmLoanApplicationForm.lblSubProdName.text = result.productDescEN;
          frmLoanApplicationForm.lblSubPrdDesc.text = result.subProductDescEN;
         // frmLoanPersonalInfo.lblSalVal.text = result.thaiSalEN !== "" ? result.thaiSalEN : "- -";
         // frmLoanPersonalInfo.lblTitleTypeVal.text = result.titleTypeEN !== "" ? result.titleTypeEN : "-";

          frmLoanPersonalInfo.lblSourceCountry.text = result.countryOfIncomeEN;
         
        }
        
        //frmLoanPersonalInfo.lblTitleTypeEntryCode.text = result.titleTypeCode;
        frmLoanPersonalInfo.lblSalutationEntryCode.text = result.thaiSalutationCode;
        frmLoanPersonalInfo.lblResidenceCode.text = result.residentTypeCode;
        frmLoanPersonalInfo.lblSrcIncomCountryCode.text = result.countryOfIncomeCode;
        
        gblPersonalInfo.thaiSalTH = result.thaiSalTH !== "" ? result.thaiSalTH : "- -";
        gblPersonalInfo.thaiSalutationCode = result.thaiSalutationCode;
        
        gblPersonalInfo.residentTypeTH = result.residentTypeTH;
        gblPersonalInfo.residentTypeCode = result.residentTypeCode;
        
        gblPersonalInfo.countryOfIncomeCode = result.countryOfIncomeCode;
        gblPersonalInfo.countryOfIncomeTH = result.countryOfIncomeTH;
 		gblPersonalInfo.countryOfIncomeEN = result.countryOfIncomeEN;
        
        gblPersonalInfo.emailStatementFlag = result.eStatement;
        
        var nameTh= gblCustomerNameTh.split(" ");
        var firstNameTh=nameTh[0];
        var lastNameTh=""
        for (var i=1; i<nameTh.length; i++){
          lastNameTh+=" "+nameTh[i];
        }
        var nameEn= gblCustomerName.split(" ");
        var firstNameEn=nameEn[0];
        var lastNameEn=""
        for (var i=1; i<nameEn.length; i++){
          lastNameEn+=" "+nameEn[i];
        }
            
        frmLoanPersonalInfo.lblThaiNameVal.text = firstNameTh.trim();
        frmLoanPersonalInfo.lblThaiSurNameVal.text =lastNameTh.trim();
        
        frmLoanPersonalInfo.lblEngNameVal.text = firstNameEn.trim();
        frmLoanPersonalInfo.lblEngSurNameVal.text =lastNameEn.trim();
        
        //frmLoanPersonalInfo.lblEmailVal.text = result.custEmail;
        //frmLoanPersonalInfo.lblMobileNo.text = result.custMobileNo;
        
        frmLoanPersonalInfo.lblEmailWithData.text = result.custEmail;
        frmLoanPersonalInfo.lblMobileNoInfo.text = "xxx-xxx-" + result.custMobileNo.substring(6, 10);
        kony.print("## totalAddr : "+result.totalAddr);
        var addr = result.totalAddr;
        var addr1 = addr.replace(/~/g , " ");
        kony.print("## addr1 : "+addr1);
      	frmLoanWorkInfo.lblWorkAddHidden.text = "";
        gblChosenTab = "";
        if (undefined != result["OCCUPATION"] && null !== result["OCCUPATION"] && result["OCCUPATION"].length > 0) {
                    gblPersonalInfo.OCCUPATION = result["OCCUPATION"][0];
                    frmLoanWorkInfo.lblEmpCode.text = gblPersonalInfo.OCCUPATION.refEntryCode;
                    frmLoanWorkInfo.lblOccupationCode.text = gblPersonalInfo.OCCUPATION.entryCode;
        }
        if (undefined != result["EMPLOYMENT_STATUS"] && null !== result["EMPLOYMENT_STATUS"] && result["EMPLOYMENT_STATUS"].length > 0) {
          var tempdata=[];
          for(var i=0; i<result["EMPLOYMENT_STATUS"].length; i++){
            if(result["EMPLOYMENT_STATUS"][i].entryCode=="01"){
              tempdata.push(result["EMPLOYMENT_STATUS"][i]);
            }
            else if(result["EMPLOYMENT_STATUS"][i].entryCode=="02"){
              tempdata.push(result["EMPLOYMENT_STATUS"][i]);
            }
          }
          gblPersonalInfo.EMPLOYMENT_STATUS =tempdata;
        }
        
        gblPersonalInfo.saveWI = result.empInfoSavedFlag == "Y" ? true : false ;
        if(gblPersonalInfo.saveWI !== undefined && gblPersonalInfo.saveWI === true){
	      	frmLoanApplicationForm.lblWorkingCircle.skin = "skinpiGreenLineLone";
      		frmLoanApplicationForm.flxWorkingCirle.skin="flxGreenCircleLoan";
       		frmLoanApplicationForm.flxWorkingCirle.setVisibility(true);
    	}
        clearDataSelected();
        if(result.empInfoSavedFlag == "Y"){
          if(frmLoanWorkInfo.lblEmpCode.text == "01"){
            gblChosenTab = "01";
            frmLoanIncomeInfo_btnEmployeeDeselect_onClick();
          }else if(frmLoanWorkInfo.lblEmpCode.text == "02"){
            gblChosenTab = "02";
            frmLoanIncomeInfo_btnSelfEmployed_onClick();
          }else{
            gblChosenTab = "";
            frmLoanIncomeInfo_btnEmployeeDeselect_onClick();
          }

          gblPersonalInfo["totalOfficeAddr"]=result.totalOfficeAddr;
          
          
          gblPersonalInfo["profCode"] = result.profCode;
          gblPersonalInfo["profCodeEN"]=result.profCodeTH;
          gblPersonalInfo["profCodeTH"]=result.profCodeTH;
          
          gblPersonalInfo["empMonth"] = result.empMonth;
          gblPersonalInfo["empYear"] = result.empYear;
          gblPersonalInfo["empDept"] = result.empDept;
          gblPersonalInfo["contractEmpStartDate"] = "";
          gblPersonalInfo["contractEmpEndDate"] = "";
          if("Y" == result.contractEmployedFlag){
            gblPersonalInfo["contractEmpStartDate"]=result.contractStartDateFmt;
          	gblPersonalInfo["contractEmpEndDate"]=result.contractEndDateFmt;
          }
          
          //gblPersonalInfo["empTeleDirectNo"]=result.empTeleDirectNo;
          
          gblPersonalInfo["empTelephoneNo"] = result.empTelephoneNo;
          gblPersonalInfo["empTelephoneExtNo"] = result.empTelephoneExtNo;
          
          frmLoanWorkInfo.lblProfessionalCode.text = result.profCode;
          frmLoanWorkInfo.lblEmpMonth.text = result.empMonth;
          frmLoanWorkInfo.lblEmpYear.text = result.empYear;
          frmLoanWorkInfo.lblLengthOfCurrentEmployee.text = gblPersonalInfo.empYear+" "+getLocalizedString("Years_textbox") +"  "+ gblPersonalInfo.empMonth+" "+getLocalizedString("Months_textbox");
          frmLoanWorkInfo.tbxDept.text = result.empDept;
		  frmLoanWorkInfo.tbxOfficeNo.text = result.empTelephoneNo;
		  frmLoanWorkInfo.tbxExtNo.text = result.empTelephoneExtNo;
          gblPersonalInfo.contractEmployedFlag = result.contractEmployedFlag;
          
          //if(locale == "th_TH"){
            frmLoanWorkInfo.lblEmploymentStatusDesc.text = result.empStatusTH;
            frmLoanWorkInfo.lblOccupationDesc.text = result.empOccupationTH;
            frmLoanWorkInfo.lblProfessionalDesc.text = result.profCodeTH;
//           }else{
//             frmLoanWorkInfo.lblEmploymentStatusDesc.text = result.empStatusEN;
//             frmLoanWorkInfo.lblOccupationDesc.text = result.empOccupationEN;
//             frmLoanWorkInfo.lblProfessionalDesc.text = result.profCodeEN;
//           }
        }
        //alert("gblPersonalInfo : "+JSON.stringify(gblPersonalInfo));
        frmLoanPersonalInfo.lblContactAddressInfo.text = addr1;
        
        var estatementVal = getLocalizedString("keyYes");
        if(result.eStatement !== undefined){
           if("N" === result.eStatement || "No" === result.eStatement || "NO" === result.eStatement){
              estatementVal = getLocalizedString("keyNo");
          }
        }
        kony.print("###  estatementVal : "+estatementVal);
        frmLoanPersonalInfo.lblEStatementVal.text = estatementVal
        //frmLoanPersonalInfo.lblEStatementVal.text = result.emailStatementFlag !== undefined ? result.emailStatementFlag : "Yes";
        
//         if(undefined != result["SALUTATION"] && null !== result["SALUTATION"] && result["SALUTATION"].length > 0){
// 			gblPersonalInfo.SALUTATION = result["SALUTATION"];
//           kony.print("## : "+gblPersonalInfo.SALUTATION);
         
//         }
        
//         if(undefined != result["TITLE_TYP"] && null !== result["TITLE_TYP"] && result["TITLE_TYP"].length > 0){
//           gblPersonalInfo.TITLE_TYP = result["TITLE_TYP"];
//         }
        
//         if(undefined != result["SCI_COUNTRY"] && null !== result["SCI_COUNTRY"] && result["SCI_COUNTRY"].length > 0){
//           gblPersonalInfo.SCI_COUNTRY = result["SCI_COUNTRY"];
//         }
        
        if(undefined != result["RESIDENT_TYP"] && null !== result["RESIDENT_TYP"] && result["RESIDENT_TYP"].length > 0){
			gblPersonalInfo.RESIDENT_TYP = result["RESIDENT_TYP"];
           kony.print("RESIDENT_TYP"+gblPersonalInfo.RESIDENT_TYP);
        }
        
        if(undefined != result["DIS_BANK"] && null !== result["DIS_BANK"] && result["DIS_BANK"].length > 0){
			gblPersonalInfo.DIS_BANK = result["DIS_BANK"];
        }if(undefined != result["PYMT_CRITERIA"] && null !== result["PYMT_CRITERIA"] && result["PYMT_CRITERIA"].length > 0){
			gblPersonalInfo.PYMT_CRITERIA = result["PYMT_CRITERIA"];
        }
         if(undefined != result["MAIL_PREFERENCE"] && null !== result["MAIL_PREFERENCE"] && result["MAIL_PREFERENCE"].length > 0){
			gblPersonalInfo.MAIL_PREFERENCE = result["MAIL_PREFERENCE"];
        }
         if(undefined != result["CARD_SEND_PREFERENCE"] && null !== result["CARD_SEND_PREFERENCE"] && result["CARD_SEND_PREFERENCE"].length > 0){
			gblPersonalInfo.CARD_SEND_PREFERENCE = result["CARD_SEND_PREFERENCE"];
        }if(undefined != result["TENURE"] && null !== result["TENURE"] && result["TENURE"].length > 0){
			gblPersonalInfo.TENURE = result["TENURE"];
        }

        if(result.incomeInfoSavedFlag == "Y"){
          gblLoanIncomeInformation = {};
          gblPersonalInfo.saveIncome = true;
          gblLoanIncomeInformation["incomeBankName"] = result.incomeBankName;
          gblLoanIncomeInformation["incomeType"] = result.incomeType;
          frmLoanIncomeInfo.lblcircle.skin = "skinpiGreenLineLone";
          frmLoanIncomeInfo.btnNext.skin = "btnBlue28pxLoan2";
          frmLoanIncomeInfo.btnNext.focusSkin="btnBlue200GreyBG";
          frmLoanIncomeInfo.btnNext.onClick = frmLoanIncomeInfo_btnNext_onClick;
          getBankNameByID();
          var accountNumber = result.incomeBankAccoutNumber;
          try{
            accountNumber = convertAccontNumberFormat(accountNumber);
            if(isNotBlank(result.incomeBasicSalary) && gblChosenTab != "02"){
            gblLoanIncomeInformation["txtFieldAccountNumber"] = accountNumber;
            gblLoanIncomeInformation["txtFieldFixedIncome"] =  isNotBlank(result.incomeBasicSalary) ? commaFormattedTransfer(result.incomeBasicSalary) : "";
            gblLoanIncomeInformation["txtFieldCOLAIncome"] = isNotBlank(result.incomeOtherIncome) ? commaFormattedTransfer(result.incomeOtherIncome) : "";
            gblLoanIncomeInformation["txtFieldBonusYearly"] = isNotBlank(result.bonusYearly) ? commaFormattedTransfer(result.bonusYearly) : "";
            gblLoanIncomeInformation["txtFieldOtherIncome"] = isNotBlank(result.incomeExtraOther) ? commaFormattedTransfer(result.incomeExtraOther) : "";
            gblLoanIncomeInformation["lblConsentValue"] = result.ncbConsentFlag;
          }
          else if(isNotBlank(result.incomeDeclared) && gblChosenTab != "01"){
            gblLoanIncomeInformation["txtFieldAccountNumberSelf"] = accountNumber;
            gblLoanIncomeInformation["txtFieldlDeclaredIncomeSelf"] = isNotBlank(result.incomeDeclared) ? commaFormattedTransfer(result.incomeDeclared) : "";
            gblLoanIncomeInformation["txtFieldCashFlow"] = isNotBlank(result.incometotalLastMthCreditAcct1) ? commaFormattedTransfer(result.incometotalLastMthCreditAcct1) : "";
            gblLoanIncomeInformation["txtFieldShareHolderSelf"] = isNotBlank(result.incomeSharedHolderPercent) ? commaFormattedTransfer(result.incomeSharedHolderPercent) : "";
            gblLoanIncomeInformation["lblConsentValueSelf"] = result.ncbConsentFlag;
          }else{
            gblChosenTab = "";
            gblLoanIncomeInformation = {};
            gblPrevLen = 0;
            MAX_ACC_LEN_LIST = "";
            gblIncomeInvalidField = "";
            gblPersonalInfo.saveIncome = false;
            frmLoanIncomeInfo.lblcircle.skin = "skinpiorangeLineLone";
            frmLoanIncomeInfo.btnNext.onClick = disableBackButton;
            frmLoanIncomeInfo.btnNext.skin = "btnGreyBGNoRoundLoan";
            frmLoanIncomeInfo.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
          }
          }catch(e){
            accountNumber = "";
            gblChosenTab = "";
            gblLoanIncomeInformation = {};
            gblPrevLen = 0;
            MAX_ACC_LEN_LIST = "";
            gblIncomeInvalidField = "";
            gblPersonalInfo.saveIncome = false;
            frmLoanIncomeInfo.lblcircle.skin = "skinpiorangeLineLone";
            frmLoanIncomeInfo.btnNext.onClick = disableBackButton;
            frmLoanIncomeInfo.btnNext.skin = "btnGreyBGNoRoundLoan";
            frmLoanIncomeInfo.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
          }
        }else{
          gblLoanIncomeInformation = {};
          gblPrevLen = 0;
          MAX_ACC_LEN_LIST = "";
          gblIncomeInvalidField = "";
          gblPersonalInfo.saveIncome = false;
          frmLoanIncomeInfo.lblcircle.skin = "skinpiorangeLineLone";
          frmLoanIncomeInfo.btnNext.onClick = disableBackButton;
          frmLoanIncomeInfo.btnNext.skin = "btnGreyBGNoRoundLoan";
          frmLoanIncomeInfo.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
        }
        frmLoanApplicationForm.show();
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false ;
      }
    }catch(e){
    alert("Exception in callbackOfgetPersonalInfoFields, e : "+e.message());
      dismissLoadingScreen();
  }
  }
  dismissLoadingScreen();
}

function IBverifyOTPMyAccounts() {
	showLoadingScreenPopup()
	var tmp_array = [];
	var inputParam = {};
	var collection = gblMyAccntAddTmpData;
	for (var i = 0; i < collection.length; i++) {
		var accntNo = collection[i].lblAccntNumVal.replace(/-/g, "");
		if (collection[i].lblASufx != null && collection[i].lblASufx != undefined && collection[i].lblASufx != ""){
				accntNo = "0" + accntNo + collection[i].lblASufxValue;
			}
		var acccname=collection[i].lblAccntNameVal;
		if(acccname == null || acccname == undefined || acccname==""){
				acccname = "null";
			}
		var tmpArray = ["", gblPersonalizeID, collection[i].bankCD, accntNo.trim(), collection[i].lblNNValue.trim(), "added", acccname.trim()];
		tmp_array.push(tmpArray)
	}
	  inputParam["loginModuleId"] = "IB_HWTKN";
      inputParam["userStoreId"] = "DefaultStore";
      inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
      inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
      inputParam["userId"] = gblUserName;
      if(gblTokenSwitchFlag == true ) 
      {
      	  inputParam["password"] = frmIBAddMyAccnt.txtToken.text;
	      if( frmIBAddMyAccnt.txtToken.text==null || frmIBAddMyAccnt.txtToken.text==""){
					dismissLoadingScreenPopup();
					alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
					return false;
			}
      }
      else
      {
	      if(frmIBAddMyAccnt.txtOTP.text=="" || frmIBAddMyAccnt.txtOTP.text == undefined || frmIBAddMyAccnt.txtOTP.text==null){
				dismissLoadingScreenPopup();
				alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
				return false;
		   }
      	inputParam["password"] = frmIBAddMyAccnt.txtOTP.text;
      }
      
      inputParam["sessionVal"] = "";
      inputParam["segmentId"] = "segmentId";
      inputParam["segmentIdVal"] = "MIB";
      inputParam["channel"] = "InterNet Banking";
      inputParam["personalizedAccList"] = tmp_array.toString();
      
      //inputParam["personalizedAccList"] = encMyAccountData(tmp_array.toString()); 
      
      inputParam["gblTokenSwitchFlag"]=gblTokenSwitchFlag;
      inputParam["gblBANKREF"]=gblBANKREF;
      inputParam["gblEditTMBhiddenAcctName"]=gblEditTMBhiddenAcctName;
       inputParam["gblEditTMBpersonalizedAcctId1"]= "ravi";
      inputParam["gblAccntType"]=gblAccntType;
      frmIBAddMyAccnt.txtOTP.text="";
      frmIBAddMyAccnt.txtToken.text="";
      invokeServiceSecureAsync("ExecuteMyAccountAddService", inputParam, AddCompleteProcessCallBackIB)
}

function AddCompleteProcessCallBackIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) 
		{
			dismissLoadingScreenPopup();
			frmIBAddMyAccnt.hbxCnrmtnStep1.setVisibility(false);
			frmIBAddMyAccnt.hbxConfrmtnStep2.setVisibility(false);
			frmIBAddMyAccnt.hbxCompleteAccnt.setVisibility(true);
			frmIBMyAccnts.hbxViewAccnt.isVisible=false;
			frmIBAddMyAccnt.hbxTokenUser.setVisibility(false);
			frmIBAddMyAccnt.show();
		} 
		else if (resulttable["opstatus"] == 8005) 
		{
 		//	dismissLoadingScreenPopup();
 			if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTP = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
				if(gblTokenSwitchFlag == true  && gblSwitchToken == false) 
				{
                   //  alert(kony.i18n.getLocalizedString("wrongOTP"));
                    frmIBAddMyAccnt.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBAddMyAccnt.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBAddMyAccnt.hbxOTPincurrect.isVisible = true;
                    frmIBAddMyAccnt.hbox450035142444916.isVisible = false;
                    frmIBAddMyAccnt.hbox44747680933513.isVisible = false;
					frmIBAddMyAccnt.hbxTokenEntry.setVisibility(true);
					
					frmIBAddMyAccnt.txtToken.text ="";
					frmIBAddMyAccnt.txtToken.setFocus(true);
				} else {
					//alert(kony.i18n.getLocalizedString("wrongOTP"));//commented by swapna
                    frmIBAddMyAccnt.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBAddMyAccnt.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBAddMyAccnt.hbxOTPincurrect.isVisible = true;
                    frmIBAddMyAccnt.hbox450035142444916.isVisible = false;
                    frmIBAddMyAccnt.hbox44747680933513.isVisible = false;
                    frmIBAddMyAccnt.txtOTP.text = "";
                    /*
                 frmIBAddMyAccnt.txtToken.text ="";
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
                      frmIBAddMyAccnt.txtToken.setFocus(true);
                    }else
                    */
                      frmIBAddMyAccnt.txtOTP.setFocus(true);
                    
				}
                    return false;
           }else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                    dismissLoadingScreenPopup();
					handleOTPLockedIB(resulttable);
                    return false;
           }else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           }else if (resulttable["errCode"] == "VrfyOTPErr00004") {
			    dismissLoadingScreenPopup();
                //showAlertIB(kony.i18n.getLocalizedString("ECVrfyOTPErr00006"), kony.i18n.getLocalizedString("info"));
				if (resulttable["errMsg"] != undefined)
                  alert(" " + resulttable["errMsg"]);
                else
                  alert(kony.i18n.getLocalizedString("ECVrfyOTPErr00006"));
                return false;
			}else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                    dismissLoadingScreenPopup();
                    frmIBAddMyAccnt.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBAddMyAccnt.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");; 
                    frmIBAddMyAccnt.hbxOTPincurrect.isVisible = true;
                    frmIBAddMyAccnt.hbox450035142444916.isVisible = false;
                    frmIBAddMyAccnt.hbox44747680933513.isVisible = false;
                    frmIBAddMyAccnt.txtOTP.text = " ";
                    frmIBAddMyAccnt.txtToken.text ="";
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
                      frmIBAddMyAccnt.txtToken.setFocus(true);
                    }else{
                      frmIBAddMyAccnt.txtOTP.setFocus(true);
                    }
           } 
           
           else {
                    frmIBAddMyAccnt.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBAddMyAccnt.lblPlsReEnter.text = " "; 
                    frmIBAddMyAccnt.hbxOTPincurrect.isVisible = false;
                    frmIBAddMyAccnt.hbox450035142444916.isVisible = true;
                    frmIBAddMyAccnt.hbox44747680933513.isVisible = true;  
               }      	
          }	else {
			dismissLoadingScreenPopup();
			 if(resulttable["errMsg"]!=undefined)
				alert(" " + resulttable["errMsg"]);
			 else
				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
		}
	}
}


function foucTabAcnt1() {
	dontAllowNonNeumaric(frmIBMyAccnts.txtAccNum1);
	var txt = frmIBMyAccnts.txtAccNum1.text;
	
	if (txt.length == 3)
		frmIBMyAccnts.txtAccNum2.setFocus(true);
}

function foucTabAcnt2() {
	dontAllowNonNeumaric(frmIBMyAccnts.txtAccNum2);
	var txt = frmIBMyAccnts.txtAccNum2.text;
	
	if (txt.length == 1)
		frmIBMyAccnts.txtAccNum3.setFocus(true);
		
		if(txt.length==0)
		frmIBMyAccnts.txtAccNum1.setFocus(true);
}

function foucTabAcnt3() {
	dontAllowNonNeumaric(frmIBMyAccnts.txtAccNum3);
	var txt = frmIBMyAccnts.txtAccNum3.text;
	
	if (txt.length == 5)
		frmIBMyAccnts.txtAccNum4.setFocus(true);
		if(txt.length==0)
		frmIBMyAccnts.txtAccNum2.setFocus(true);
}

function foucTabAcnt4() {
dontAllowNonNeumaric(frmIBMyAccnts.txtAccNum4);
	var txt = frmIBMyAccnts.txtAccNum4.text;
	
	if (txt.length == 1)
		frmIBMyAccnts.txtbxSuffix.setFocus(true);
		if(txt.length==0)
		frmIBMyAccnts.txtAccNum3.setFocus(true);
}

function foucTabCC1() {
	dontAllowNonNeumaric(frmIBMyAccnts.txtCCNum1);
	var txt = frmIBMyAccnts.txtCCNum1.text;
	
	if (txt.length == 4)
		frmIBMyAccnts.txtCCNum2.setFocus(true);
}

function foucTabCC2() {
dontAllowNonNeumaric(frmIBMyAccnts.txtCCNum2);
	var txt = frmIBMyAccnts.txtCCNum2.text;
	
	if (txt.length == 4)
		frmIBMyAccnts.txtCCNum3.setFocus(true);
	if(txt.length==0)
	frmIBMyAccnts.txtCCNum1.setFocus(true);
}

function foucTabCC3() {
	dontAllowNonNeumaric(frmIBMyAccnts.txtCCNum3);
	var txt = frmIBMyAccnts.txtCCNum3.text;
	
	if (txt.length == 4)
		frmIBMyAccnts.txtCCNum4.setFocus(true);
		if(txt.length==0)
	frmIBMyAccnts.txtCCNum2.setFocus(true);
}
function spacesCheck(){
//  alert("in spacesCheck ");
  var txt = frmIBMyAccnts.txtNickNAme.text;
  if(txt.charAt(0) == " "){
    //alert("in ifffff ");
    frmIBMyAccnts.txtNickNAme.text =  txt.trim();
 }

}
function spacesCheckRece(){
  //alert("in spacesCheck ");
  var txt = frmIBMyReceipentsAddContactManually.txtRecipientName.text;
  if(txt.charAt(0) == " "){
    //alert("in ifffff ");
    frmIBMyReceipentsAddContactManually.txtRecipientName.text = txt.trim();
 }

}
function DisableCopyPaste(e) {
	var key;
	var isCtrl;
	var forbiddenKeys = new Array('a', 'p', 'c', 'x', 'v');
	//for disabling poping of context menu on right click
	document.oncontextmenu = function () {
		return false;
	}
	//Event for disable the crtl + any normal key combination  
	if (window.event) {
		key = window.event.keyCode; //IE
		if (window.event.ctrlKey)
			isCtrl = true;
		else
			isCtrl = false;
	} else {
		var key = e.which; //firefox
		
		if (e.ctrlKey)
			isCtrl = true;
		else
			isCtrl = false;
	}
	
	//if ctrl is pressed check if other key is in forbidenKeys array
	if (isCtrl) {
		for (i = 0; i < forbiddenKeys.length; i++) {
			//case-insensitive comparation
			if (forbiddenKeys[i].toLowerCase() == String.fromCharCode(key)
				.toLowerCase()) {
				
				// alert('Key combination CTRL + ' +String.fromCharCode(key)  +' has been disabled.');
				return false;
			}
		}
	}
	return true;
}

function disableRightClick() {
	document.onkeydown = DisableCopyPaste
}
//View account Service Calls
// Service call when the segment of TMB accounts is clicked (segment on click)

function viewAccountTMBIB(index, accNo, segID) {
	showLoadingScreenPopup();
	frmIBEditMyAccounts.hbxViewBeneficiary.isVisible=false;
	frmIBEditMyAccounts.hbxTermsAndConditions.isVisible=false;
	var accType = index;
	
	var isTMB = (segID == "segTMBAccntsList") ? true : false;
	if (isTMB) {
		frmIBMyAccnts.imgViewAccntDetails.src = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].imgAccountPicture;
		frmIBMyAccnts.lblViewNickNameHdr.text = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].lblSegTMBAccntDetails;
		frmIBMyAccnts.hbxAccNickName.setVisibility(false);
		frmIBMyAccnts.hbxViewAccName.setVisibility(true);
		
		frmIBEditMyAccounts.imgViewAccntDetails.src = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].imgAccountPicture;
		frmIBEditMyAccounts.lblViewNickNameHdr.text = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].lblSegTMBAccntDetails;
		frmIBEditMyAccounts.hbxAccNickName.setVisibility(false);
		frmIBEditMyAccounts.hbxViewAccName.setVisibility(true);
	} else {
		frmIBMyAccnts.imgViewAccntDetails.src = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].imgotherBankAcntPic;
		frmIBMyAccnts.lblViewNickNameHdr.text = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].lblSegOtherBnkAccntDetails;
		frmIBMyAccnts.hbxAccNickName.setVisibility(false);
		frmIBMyAccnts.hbxViewAccName.setVisibility(true);
		
		frmIBEditMyAccounts.imgViewAccntDetails.src = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].imgotherBankAcntPic;
		frmIBEditMyAccounts.lblViewNickNameHdr.text = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].lblSegOtherBnkAccntDetails;
		frmIBEditMyAccounts.hbxAccNickName.setVisibility(false);
		frmIBEditMyAccounts.hbxViewAccName.setVisibility(true);
	}
	if (isTMB) {
		var accType = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAcctType;
		gblAccntType = accType;
		if (accType == "DDA") {
			var accntName = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAcctName
			var accVal = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;
			var accValLength = accVal.length;
			accVal = accVal.substring((accValLength - 10), (accValLength - 7)) + "-" + accVal.substring((accValLength - 7), (
				accValLength - 6)) + "-" + accVal.substring((accValLength - 6), (accValLength - 1)) + "-" + accVal.substring((
				accValLength - 1), accValLength);
			setAccountViewIB(kony.i18n.getLocalizedString("keyAccountNoIB"), accVal, kony.i18n.getLocalizedString("AccountName"),
				accntName, "", "");
		} else if (accType == "SDA" || accType == "CDA") {
 			if (frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID == "206"){
				gblAccountSummaryFlag="false"
				clearDreamDataMaint()
  			}else{
				myAccountViewDepositeServiceIB();
				if(frmIBMyAccnts.segTMBAccntsList.selectedItems != null && undefined != frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID && gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID) >= 0){
					getBeneficaryDetailsMyAccountsIB();
				}
			}
 		} else if (accType.toUpperCase() == "CCA") {
			
			// service call to get Credit Card details to show
			myAccountViewCreditCardServiceIB();
		} else if (accType.toUpperCase() == "LOC") {
			
			//service call to get loan details to show
			myAccountViewLoanAccServiceIB();
		} else {
			dismissLoadingScreenPopup();
			alert("Account type unknown");
		}
	} else {
		setAccountViewIB(kony.i18n.getLocalizedString("keyAccountNoIB"), encodeAccntNumbers(frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0]
			.hiddenAccountNo), kony.i18n.getLocalizedString("AccountName"), frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].hiddenAcctName, "", "");
	}
}
// Called form viewAccountTMBIB to get the deposite details


function myAccountViewDepositeServiceIB() {
	var inputparam = {};
	
	inputparam["acctId"] = "" + frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;
//	gblAccntIDIB =  frmIBMyAccnts.segTMBAccntsList.selectedItems[0].fiident + frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;
 	gblAccntTypeIB = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAcctType;
 	var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, myAccountViewDepositeCallBackIB);
}

function myAccountViewDepositeCallBackIB(status, resulttable) {
 	if (status == 400) {
 		if (resulttable["opstatus"] == 0) {
 			gblMyAccountFlowFlag = 01;
			var accVal = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;
			var accValLength = accVal.length;
 			accVal = accVal.substring((accValLength - 10), (accValLength - 7)) + "-" + accVal.substring((accValLength - 7), (
					accValLength - 6)) + "-" + accVal.substring((accValLength - 6), (accValLength - 1)) + "-" + accVal.substring((
					accValLength - 1), accValLength);
			setAccountViewIB(kony.i18n.getLocalizedString("keyAccountNoIB"), accVal, kony.i18n.getLocalizedString("AccountName"),
				resulttable["accountTitle"], "", "");
 			frmIBMyAccnts.hbxViewAccSuffix.setVisibility(false);
 			frmIBEditMyAccounts.hbxViewAccSuffix.setVisibility(false);
		} else {
			dismissLoadingScreenPopup();
			if(resulttable["errMsg"] != undefined)
	        	{
	        		alert(resulttable["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}	
	  		}
	} 
}
// Called form viewAccountTMBIB to get the Credit Card details

function myAccountViewCreditCardServiceIB() {
	var inputparam = {};
	var toDayDate = getTodaysDate();
	inputparam["cardId"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;
	inputparam["waiverCode"] = "";
	inputparam["tranCode"] = TRANSCODEUN;
	inputparam["postedDt"] = toDayDate;
	inputparam["rqUUId"] = "";
	
	var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, myAccountViewCreditCardCallBackIB);
}

function myAccountViewCreditCardCallBackIB(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			
			
			gblMyAccountFlowFlag = 03;
			var cardName = resulttable["fromAcctName"];
			var cardNo = resulttable["cardNo"];
			//var accVal = resulttable["cardNo"];
			// var accValLength = accVal.length;
			//accVal = accVal.substring((accValLength-16), (accValLength-12)) + "-" + accVal.substring((accValLength-12), (accValLength-8)) + "-" + accVal.substring((accValLength-8), (accValLength-4)) + "-" + accVal.substring((accValLength-4), accValLength);
			
			setAccountViewIB(kony.i18n.getLocalizedString("keyCardNumberIB"), cardNo, kony.i18n.getLocalizedString(
				"keyCardHolderNameIB"), cardName, "", "");
			frmIBMyAccnts.hbxViewAccSuffix.setVisibility(false);
			frmIBEditMyAccounts.hbxViewAccSuffix.setVisibility(false);
		} else {
			dismissLoadingScreenPopup();
			
			alert(" " + resulttable["errMsg"]);
		}
	} else if (status == 300) {
		dismissLoadingScreenPopup();
		
	}
}
// Called form viewAccountTMBIB to get the loan details

function myAccountViewLoanAccServiceIB() {
	var inputparam = {};
	inputparam["acctId"] = "" + frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;
	inputparam["acctType"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAcctType;
	inputparam["rqUUId"] = "";
	var status = invokeServiceSecureAsync("doLoanAcctInq", inputparam, myAccountViewLoanAccCallBackIB);
}

function myAccountViewLoanAccCallBackIB(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			
			
			
			gblMyAccountFlowFlag = 02;
			var accntNo = resulttable["loanAccounNo"];
			var suffix = accntNo.substring(accntNo.length, accntNo.length - 3);
			var accVal = resulttable["loanAccounNo"];
			var accValLength = accVal.length - 3;
			accVal = accVal.substring((accValLength - 10), (accValLength - 7)) + "-" + accVal.substring((accValLength - 7), (
				accValLength - 6)) + "-" + accVal.substring((accValLength - 6), (accValLength - 1)) + "-" + accVal.substring((
				accValLength - 1), accValLength);
			frmIBMyAccnts.hbxViewAccSuffix.setVisibility(true);
			frmIBEditMyAccounts.hbxViewAccSuffix.setVisibility(true);
			setAccountViewIB(kony.i18n.getLocalizedString("keyAccountNoIB"), accVal, kony.i18n.getLocalizedString("keySuffixIB") +
				":", suffix, kony.i18n.getLocalizedString("AccountName"), resulttable["accountName"]);
		} else {
			dismissLoadingScreenPopup();
			
			if(resulttable["errMsg"] != undefined)
	        	{
	        		alert(resulttable["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}	
	       	}
	} else if (status == 300) {
		dismissLoadingScreenPopup();
		
	}
}
// Called from myAccountViewDepositeCallBackIB, myAccountViewCreditCardCallBackIB, myAccountViewLoanAccCallBackIB , viewAccountTMBIB for setting details in frmIBMyAccnts.hbxViewAccnt

/*function setAccountViewIB(lblAccountNo, lblAccntNoValue, lblAccountName, lblAccntNameValue, lblAccntSuffix,
	lblAccontSuffixValue) {
	frmIBMyAccnts.lblViewAccntNum.text = lblAccountNo;
	frmIBMyAccnts.lblViewAccntNumVal.text = lblAccntNoValue;
	frmIBMyAccnts.lblViewAccntName.text = lblAccountName;
	if(lblAccntNameValue == "" ||  lblAccntNameValue == null || lblAccntNameValue == "null" || lblAccntNameValue == undefined)
	lblAccntNameValue="";
	frmIBMyAccnts.lblViewAccntNameVal.text = lblAccntNameValue;
	frmIBMyAccnts.lblViewAccntSuffix.text = lblAccntSuffix;
	frmIBMyAccnts.lblViewAccntSuffixVal.text = lblAccontSuffixValue;
	frmIBMyAccnts.hbxViewCCNum.setVisibility(false);
	frmIBMyAccnts.hbxViewAccnt.setVisibility(true);
	frmIBMyAccnts.hboxAddNewAccnt.setVisibility(false);
	frmIBMyAccnts.hbxTMBImg.setVisibility(false);
	//frmIBMyAccnts.imgArrow.setVisibility(false);
	frmIBMyAccnts.hbxEditAccntNN.setVisibility(false);
	dismissLoadingScreenPopup();
}*/
function setAccountViewIB(lblAccountNo, lblAccntNoValue, lblAccountName, lblAccntNameValue, lblAccntSuffix,
	lblAccontSuffixValue) {
	frmIBEditMyAccounts.lblViewAccntNum.text = lblAccountNo;
	frmIBEditMyAccounts.lblViewAccntNumVal.text = lblAccntNoValue;
	frmIBEditMyAccounts.lblViewAccntName.text = lblAccountName;
	if(lblAccntNameValue == "" ||  lblAccntNameValue == null || lblAccntNameValue == "null" || lblAccntNameValue == undefined)
	lblAccntNameValue="";
	frmIBEditMyAccounts.lblViewAccntNameVal.text = lblAccntNameValue;
	frmIBEditMyAccounts.lblViewAccntSuffix.text = lblAccntSuffix;
	frmIBEditMyAccounts.lblViewAccntSuffixVal.text = lblAccontSuffixValue;
	frmIBEditMyAccounts.hbxViewCCNum.setVisibility(false);
	frmIBEditMyAccounts.hbxViewAccnt.setVisibility(true);
	frmIBEditMyAccounts.hbxTMBImg.setVisibility(true);
	//if(gblEditClickFromAccountDetails && kony.application.getPreviousForm().id == "frmIBAccntSummary"){
	//	frmIBEditMyAccounts.hbxTermsAndConditions.isVisible=true;
	//	frmIBEditMyAccounts.hbxViewAccnt.isVisible=false;
	//}
	//frmIBEditMyAccounts.imgArrow.setVisibility(false);
	frmIBEditMyAccounts.hbxEditAccntNN.setVisibility(false);
	if(frmIBMyAccnts.segTMBAccntsList.selectedItems != null && undefined != frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID && gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID) < 0){
		dismissLoadingScreenPopup();
	}
	frmIBEditMyAccounts.show();
}
// Service for Edit Nickname
// Called after the new nick name is entered and clicked on confirm button in frmIBMyaccnts

function showfrmViewAccntIB() {

	showLoadingScreenPopup();
	var bankCde="";
	
	if (gblEditNTMBbankCD == "" && gblsegID == "segOtherBankAccntsList") {
		bankCde=frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankCD;
	}
	else{
		bankCde=gblEditNTMBbankCD;
	}
	var textformat=NickNameValid(frmIBEditMyAccounts.txtbxEditAccntNN.text);
	if(textformat== false) {
		dismissLoadingScreenPopup(); //def734
		showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	
	var newNickName = frmIBEditMyAccounts.txtbxEditAccntNN.text;
	
	var oldNickName = frmIBEditMyAccounts.lblViewNickNameHdr.text;
	if(gblsegID == "segOtherBankAccntsList" && getORFTFlagIBMyAcc(bankCde) == "N"){
			if (AccountNameValid(frmIBEditMyAccounts.tbxEditAccName.text) == false) {
			dismissLoadingScreenPopup();
	  		showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
	
	gblIBEditNickNameValue = oldNickName + " + " + newNickName;
	if ((newNickName == null) || (newNickName == "")) {
		
		dismissLoadingScreenPopup();
		showAlert(kony.i18n.getLocalizedString("keyEmptyNickName"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	if (newNickName != oldNickName) {
		for (var i = 0; i < gblAccntData.length; i++) {
		    if (gblAccntData[i].accntStatus != "02") 
		    {
		        if (gblAccntData[i].nickName == newNickName) 
		        {
		            dismissLoadingScreenPopup();
		            //alert("Nick name already exists");
		            showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
		            return false;
		        }
		    }
		}
		myAccountEditServiceIBNew("edit");
	} else {
	if(gblsegID == "segOtherBankAccntsList" && getORFTFlagIBMyAcc(bankCde) == "N"){
		if(frmIBEditMyAccounts.tbxEditAccName.text == frmIBEditMyAccounts.lblViewAccntNameVal.text){
			dismissLoadingScreenPopup();
			gblAddAccntVarIB();
			frmIBEditMyAccounts.hbxEditAccntNN.setVisibility(false);
			frmIBEditMyAccounts.hbxViewAccnt.setVisibility(true);
			frmIBEditMyAccounts.hbxTMBImg.setVisibility(false);
			frmIBEditMyAccounts.hbxEditAccntNN.setVisibility(false);
			if(gblEditClickFromAccountDetails){
				onClickCancelPointRedemptionIB();
			}
			//frmIBMyAccnts.show();
		}
		else{
				myAccountEditServiceIBNew("edit");
		}
		
	}else{
			dismissLoadingScreenPopup();
			gblAddAccntVarIB();
			frmIBEditMyAccounts.hbxEditAccntNN.setVisibility(false);
			frmIBEditMyAccounts.hbxViewAccnt.setVisibility(true);
			frmIBEditMyAccounts.hbxTMBImg.setVisibility(false);
			frmIBEditMyAccounts.hbxEditAccntNN.setVisibility(false);
			//frmIBMyAccnts.show();
	}
	}
	//dismissLoadingScreenPopup();
	//updating new value in form data
	// No need to update in gblMyAccntNickName becouse that will be updated when user will go to My Account List
}
// Called from showfrmViewAccntIB

function myAccountEditServiceIBNew(flow){
var myAccountEditService_inputparam = {};
	myAccountEditService_inputparam["personalizedId"] =gblPersonalizeID;
	myAccountEditService_inputparam["acctNickName"] = frmIBEditMyAccounts.txtbxEditAccntNN.text;

		
	
	if (gblsegID == "segTMBAccntsList") {
		if(frmIBMyAccnts.segTMBAccntsList.selectedItems == null || frmIBMyAccnts.segTMBAccntsList.selectedItems == undefined){
 
 		}else{ 
			gblMyAccountSelectedIndex =frmIBMyAccnts.segTMBAccntsList.selectedindex[1];
 		}
		
		if (gblEditTMBpersonalizedAcctId == "") {
			myAccountEditService_inputparam["personalizedAcctId"] = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.data[gblMyAccountSelectedIndex].personalizedAcctId);
			gblEditTMBpersonalizedAcctId = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.data[gblMyAccountSelectedIndex].personalizedAcctId);
		} else {
			myAccountEditService_inputparam["personalizedAcctId"] = gblEditTMBpersonalizedAcctId;
		}
		if (gblEditTMBpersonalizedId == "") {
			myAccountEditService_inputparam["recordPersonalizedId"] = frmIBMyAccnts.segTMBAccntsList.data[gblMyAccountSelectedIndex].personalizedId;
			gblEditTMBpersonalizedId = frmIBMyAccnts.segTMBAccntsList.data[gblMyAccountSelectedIndex].personalizedId;
		} else {
			myAccountEditService_inputparam["recordPersonalizedId"] = gblEditTMBpersonalizedId;
		}
	
		myAccountEditService_inputparam["bankCD"] = "11";
		
				
	} else {
		if(frmIBMyAccnts.segOtherBankAccntsList.selectedItems == null || frmIBMyAccnts.segOtherBankAccntsList.selectedItems == undefined){
 		}else{
			gblMyAccountSelectedIndex =frmIBMyAccnts.segOtherBankAccntsList.selectedindex[1];
		}
		
		if (gblEditNTMBpersonalizedAcctId == "") {
			myAccountEditService_inputparam["personalizedAcctId"] = frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].personalizedAcctId;
			gblEditNTMBpersonalizedAcctId = frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].personalizedAcctId;
		} else {
			myAccountEditService_inputparam["personalizedAcctId"] = gblEditNTMBpersonalizedAcctId;
		}
		if (gblEditNTMBpersonalizedId == "") {
			myAccountEditService_inputparam["recordPersonalizedId"] = frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].personalizedId;
			gblEditNTMBpersonalizedId = frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].personalizedId;
		} else {
			myAccountEditService_inputparam["recordPersonalizedId"] = gblEditNTMBpersonalizedId;
		}
		if (gblEditNTMBbankCD == "") {
			myAccountEditService_inputparam["bankCD"] = frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].bankCD;
			gblEditNTMBbankCD = frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].bankCD;
		} else {
			myAccountEditService_inputparam["bankCD"] = gblEditNTMBbankCD;
		}
		gblEditNTMBbankName = frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].bankNameEng;
	}
	//Logging activity  DEF3226
	var Number = ""
	var NickName = ""
	var BankName = ""
	if (gblsegID == "segTMBAccntsList") {
		Number =getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.data[gblMyAccountSelectedIndex].personalizedAcctId);
		NickName=frmIBEditMyAccounts.txtbxEditAccntNN.text;
		BankName="TMB";
	}
	else
	{
		Number = myAccountEditService_inputparam["personalizedAcctId"];
	    NickName = myAccountEditService_inputparam["acctNickName"];
		BankName =  frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].bankNameEng;
	}
	gblEditTMBpersonalizedAcctId= Number;
	myAccountEditService_inputparam["acctName"]=frmIBEditMyAccounts.lblViewAccntNameVal.text;
	myAccountEditService_inputparam["acctStatus"] = "";
	myAccountEditService_inputparam["BankName"] = BankName; 
    myAccountEditService_inputparam["Number"] = Number; 
    myAccountEditService_inputparam["gblIBEditNickNameValue"] = gblIBEditNickNameValue;
	if(flow=="edit"){
	
	if(gblsegID == "segOtherBankAccntsList" && getORFTFlagIBMyAcc(myAccountEditService_inputparam["bankCD"]) == "N")
		myAccountEditService_inputparam["AccountNameEdit"] = frmIBEditMyAccounts.tbxEditAccName.text;
	else
		myAccountEditService_inputparam["AccountNameEdit"] = frmIBEditMyAccounts.lblViewAccntNameVal.text;
 		invokeServiceSecureAsync("MyAccountEditService", myAccountEditService_inputparam, myAccountEditServiceCallBackIB);
	}
	else if(flow=="delete")
	{
		if(kony.application.getCurrentForm() == frmIBDreamSavingMaintenance){
			myAccountEditService_inputparam["acctNickName"]=frmIBDreamSavingMaintenance.lblAccountNicknameValue.text
			}
		else{
			myAccountEditService_inputparam["acctNickName"]=frmIBEditMyAccounts.lblViewNickNameHdr.text;
			}
		invokeServiceSecureAsync("MyAccountDeleteService", myAccountEditService_inputparam, myAccountDelServiceCallBackIB);
	}

}



//Called from myAccountEditServiceIB and srvChekAccntExistCallBackEdit for the service crmAccountModKony.
// As we have to refresh the segment data everytime we change the nickname by showing the view accnt in the same page,Saving the selected values (personalizedAcctId,personalizedId,bankCD) to   (gblEditTMBpersonalizedAcctId,gblEditTMBpersonalizedId,gblEditTMBbankCD),so that we can edit nickname any number of times by showing refreshed data in frmIBMyaccnts page.

function myAccountEditService_innerIB() {
	var myAccountEditService_inputparam = {};
	myAccountEditService_inputparam["acctNickName"] = frmIBMyAccnts.txtbxEditAccntNN.text;
	if (gblsegID == "segTMBAccntsList") {
		if (gblEditTMBpersonalizedAcctId == "") {
			myAccountEditService_inputparam["personalizedAcctId"] = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[
				0].personalizedAcctId);
			gblEditTMBpersonalizedAcctId = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId);
		} else {
			myAccountEditService_inputparam["personalizedAcctId"] = gblEditTMBpersonalizedAcctId;
		}
		if (gblEditTMBpersonalizedId == "") {
			myAccountEditService_inputparam["personalizedId"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedId;
			gblEditTMBpersonalizedId = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedId;
		} else {
			myAccountEditService_inputparam["personalizedId"] = gblEditTMBpersonalizedId;
		}
		if (gblEditTMBbankCD == "") {
			myAccountEditService_inputparam["bankCD"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankCD;
			gblEditTMBbankCD = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankCD;
		} else {
			myAccountEditService_inputparam["bankCD"] = gblEditTMBbankCD;
		}
		gblEditTMBbankCD = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankNameEng;
		myAccountEditService_inputparam["bankName"] = gblEditTMBbankCD
	} else {
		if (gblEditNTMBpersonalizedAcctId == "") {
			myAccountEditService_inputparam["personalizedAcctId"] = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].personalizedAcctId;
			gblEditNTMBpersonalizedAcctId = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].personalizedAcctId;
		} else {
			myAccountEditService_inputparam["personalizedAcctId"] = gblEditNTMBpersonalizedAcctId;
		}
		if (gblEditNTMBpersonalizedId == "") {
			myAccountEditService_inputparam["personalizedId"] = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].personalizedId;
			gblEditNTMBpersonalizedId = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].personalizedId;
		} else {
			myAccountEditService_inputparam["personalizedId"] = gblEditNTMBpersonalizedId;
		}
		if (gblEditNTMBbankCD == "") {
			myAccountEditService_inputparam["bankCD"] = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankCD;
			gblEditNTMBbankCD = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankCD;
		} else {
			myAccountEditService_inputparam["bankCD"] = gblEditNTMBbankCD;
		}
		gblEditNTMBbankName = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankNameEng;
		myAccountEditService_inputparam["bankName"] = gblEditNTMBbankName
	}
	//Logging activity  DEF3226
	var Number = ""
	var NickName = ""
	var BankName = ""
	if (gblsegID == "segTMBAccntsList") {
		Number =getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId);
		NickName=frmIBMyAccnts.txtbxEditAccntNN.text;
		BankName="TMB";
	}
	else{
	
		Number = myAccountEditService_inputparam["personalizedAcctId"];
	    NickName = myAccountEditService_inputparam["acctNickName"];
		
		BankName = myAccountEditService_inputparam["bankName"];
	}
	gblEditTMBpersonalizedAcctId= Number;
	
	activityLogServiceCall("055", "", "01", "", "Edit", gblIBEditNickNameValue, BankName, Number, "", "");
	myAccountEditService_inputparam["acctStatus"] = "";
	
	invokeServiceSecureAsync("crmAccountModKony", myAccountEditService_inputparam, myAccountEditServiceCallBackIB);
}
//Called from myAccountEditServiceIB and myAccountDelServiceIB to check whether the account exist in account relation table or not

function srvChekAccntExist(flag) {
	var myAccountList_inputparam = {};
	myAccountList_inputparam["personalizedId"] = gblPersonalizeID;
	if (gblEditTMBpersonalizedAcctId == "") {
		myAccountList_inputparam["personalizedAcctId"] = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId);
		gblEditTMBpersonalizedAcctId = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId);
	} else {
		myAccountList_inputparam["personalizedAcctId"] = gblEditTMBpersonalizedAcctId;
	}
	//myAccountList_inputparam["personalizedAcctId"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId;
	//TMB_BANK_CD is saved globally, which is response of getBankList called in the postshow of the frmIBMYaccnts
	myAccountList_inputparam["bankCD"] = TMB_BANK_CD;
	
	if (flag == "01")
		invokeServiceSecureAsync("checkAccountExistAccnt", myAccountList_inputparam, srvChekAccntExistCallBackEdit);
	else
		invokeServiceSecureAsync("checkAccountExistAccnt", myAccountList_inputparam, srvChekAccntExistCallBackDelete);
}

function srvChekAccntExistCallBackEdit(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			//return resulttable["accExist"];
			var accountFound = resulttable["accExist"];
			if (resulttable["accExist"].trim() == "false") {
				// if account is not found, have to add to 1st before editing
				var inputparam = {};
				inputparam["personalizedAccList"] = [];
				if (gblEditTMBpersonalizedAcctId1 == "") {
					gblEditTMBpersonalizedAcctId1 = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId)
				}
				if (gblEditTMBhiddenAcctName == "") {
					gblEditTMBhiddenAcctName = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAcctName
				}
				//Logging activity  DEF3226
				var Number = gblEditTMBpersonalizedAcctId1;
				//var NickName = myAccountEditService_inputparam["acctNickName"];
				var BankName = getBankName(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankCD);
				
				gblBANKREF="TMB";
				activityLogServiceCall("055", "", "01", "", "Edit", gblIBEditNickNameValue, BankName, Number, "", "");
				var tmpArray = ["", gblPersonalizeID, TMB_BANK_CD, gblEditTMBpersonalizedAcctId1, frmIBMyAccnts.txtbxEditAccntNN.text,"added", " "];
				inputparam["personalizedAccList"] = tmpArray.toString();
				invokeServiceSecureAsync("receipentAddBankAccntService", inputparam, AddCompleteProcessCallBackIB);
			} else {
				myAccountEditService_innerIB();
			}
		} else {
			dismissLoadingScreenPopup();
			alert("checkAccountExistAccnt_RelTable service Not returning opstatus 0 ");
		}
	}
}

function srvChekAccntExistCallBackDelete(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			gblAccExist=resulttable["accExist"].trim();
			if (resulttable["accExist"].trim() == "false") {
				gblDELAccntAdd = true;
				showLoadingScreenPopup();
				var inputparam = {};
				inputparam["personalizedAccList"] = [];
				if (gblEditTMBpersonalizedAcctId1 == "") {
					gblEditTMBpersonalizedAcctId1 = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId)
				}
				if (gblEditTMBhiddenAcctName == "") {
					gblEditTMBhiddenAcctName = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAcctName
				}
				var Number = ""
				var NickName = ""
				var BankName = ""
				if (gblsegID == "segTMBAccntsList") {
					Number =getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId);
					NickName=frmIBMyAccnts.txtbxEditAccntNN.text;
					BankName="TMB";
				}
				else{
				
					Number = gblEditTMBpersonalizedAcctId1;
				    NickName = frmIBMyAccnts.txtbxEditAccntNN.text;
					BankName = getBankName(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankCD)
				}
				
				
				activityLogServiceCall("055", "", "01", "", "Delete", NickName, BankName, Number, "", "");
				var tmpArray = ["", gblPersonalizeID, TMB_BANK_CD, gblEditTMBpersonalizedAcctId1, frmIBMyAccnts.lblViewNickNameHdr.text,
					"Deleted", "null"];
				inputparam["personalizedAccList"] = tmpArray.toString();
				invokeServiceSecureAsync("receipentAddBankAccntService", inputparam, AddCompleteProcessCallBackIB);
			}
			else{
			myAccountDelService_innerIB();
			}
		} else {
			dismissLoadingScreenPopup();
			alert("checkAccountExistAccnt_RelTable service Not returning opstatus 0 ");
		}
	}
}

function myAccountDelServiceIB() {
	//showLoadingScreenPopup();
	if (gblsegID == "segTMBAccntsList") {
		if (gblTMBBankCd == "") {
			gblTMBBankCd = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankCD;
		}
		if (TMB_BANK_CD == gblTMBBankCd) {
			if (gblEditTMBpersonalizedAcctId == "")
				gblEditTMBpersonalizedAcctId = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId);
			if (gblEditTMBpersonalizedId == "")
				gblEditTMBpersonalizedId = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedId;
			if (gblEditTMBbankCD == "")
				gblEditTMBbankCD = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankCD;
			var flag = "02"; //Delete flow
			srvChekAccntExist(flag);
		}
	} else {
		myAccountDelService_innerIB();
	}
}
//Called from myAccountDelServiceIB and srvChekAccntExistCallBackDelete for the service crmAccountModKony.
// As we have to refresh the segment data everytime we change the nickname by showing the view accnt in the same page,Saving the selected values (personalizedAcctId,personalizedId,bankCD) to   (gblEditTMBpersonalizedAcctId,gblEditTMBpersonalizedId,gblEditTMBbankCD),so that we can edit nickname any number of times by showing refreshed data in frmIBMyaccnts page.

function myAccountDelService_innerIB() {
	var myAccountDelService_inputparam = {};
	if (gblsegID == "segTMBAccntsList") {
		if (gblEditTMBpersonalizedAcctId == "") {
			myAccountDelService_inputparam["personalizedAcctId"] = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0]
				.personalizedAcctId);
			gblEditTMBpersonalizedAcctId = getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId);
		} else {
			myAccountDelService_inputparam["personalizedAcctId"] = gblEditTMBpersonalizedAcctId;
		}
		if (gblEditTMBpersonalizedId == "") {
			myAccountDelService_inputparam["personalizedId"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedId;
			gblEditTMBpersonalizedId = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedId;
		} else {
			myAccountDelService_inputparam["personalizedId"] = gblEditTMBpersonalizedId;
		}
		if (gblEditTMBbankCD == "") {
			myAccountDelService_inputparam["bankCD"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankCD;
			gblEditTMBbankCD = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].bankCD;
		} else {
			myAccountDelService_inputparam["bankCD"] = gblEditTMBbankCD;
		}
	} else {
		if (gblEditNTMBpersonalizedAcctId == "") {
			myAccountDelService_inputparam["personalizedAcctId"] = getAccountNoIB(frmIBMyAccnts.segOtherBankAccntsList.selectedItems[
				0].personalizedAcctId);
			gblEditNTMBpersonalizedAcctId = getAccountNoIB(frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].personalizedAcctId);
		} else {
			myAccountDelService_inputparam["personalizedAcctId"] = gblEditNTMBpersonalizedAcctId;
		}
		if (gblEditNTMBpersonalizedId == "") {
			myAccountDelService_inputparam["personalizedId"] = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].personalizedId;
			gblEditNTMBpersonalizedId = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].personalizedId;
		} else {
			myAccountDelService_inputparam["personalizedId"] = gblEditNTMBpersonalizedId;
		}
		if (gblEditNTMBbankCD == "") {
			myAccountDelService_inputparam["bankCD"] = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankCD;
			gblEditNTMBbankCD = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankCD;
		} else {
			myAccountDelService_inputparam["bankCD"] = gblEditNTMBbankCD;
		}
	}
	myAccountDelService_inputparam["acctStatus"] = "Deleted";
	//Logging activity DEF3226
	var Number = ""
	var NickName = ""
	var BankName = ""
	if (gblsegID == "segTMBAccntsList") {
		Number =getAccountNoIB(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].personalizedAcctId);
		NickName=frmIBMyAccnts.lblViewNickNameHdr.text;
		BankName="TMB";
	}
	else{
	
		Number = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].personalizedId;
	    NickName =frmIBMyAccnts.lblViewNickNameHdr.text; 
		
		BankName = frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankNameEng;
	}

	
	
	 gblEditTMBpersonalizedAcctId1 =Number;
	 gblEditTMBhiddenAcctName=NickName
	 gblAccntType="Other Bank Account";
	 gblBANKREF=BankName;
	activityLogServiceCall("055", "", "01", "", "Delete", NickName, BankName, Number, "", "");
	// 
	invokeServiceSecureAsync("crmAccountModKony", myAccountDelService_inputparam, myAccountDelServiceCallBackIB);
}
// Service call to populate the segments @ frmIBMyaccnts and frmIbAddMyAccnt with the TMB n NON TMB accoutns

function myAccountListServiceIB() {
	showLoadingScreenPopup();
	var myAccountList_inputparam = {};
	myAccountList_inputparam["accountsFlag"] = "true";
	var status = invokeServiceSecureAsync("customerAccountInquiry", myAccountList_inputparam, myAccountListCallBackAltIB);
}

function myAccountListCallBackAltIB(status, resulttable) {
 	if (status == 400) {
 		if (resulttable["opstatus"] == 0) {
 			/**below code merged from Acctype code List***/
 			 TOTAL_NON_TMB_AC = 0;
   			 LoadMasterConfigurationsForMyAccounts(resulttable);
   			 LoadBankListforMyAccounts(resulttable);
 			/** Till here ***/
  			if (resulttable["statusCode"] == 0) {
 				//Resetting TMB segData
				frmIBMyAccnts.lblTmbHeader.isVisible = false;
				frmIBAddMyAccnt.lblTmbHeader.isVisible = false;
				frmIBMyAccnts.segTMBAccntsList.removeAll();
				frmIBAddMyAccnt.segTMBAccntListFAA.removeAll();
				frmIBAddMyAccnt.hbox44557018542145.isVisible = false;
				//Resetting NON-TMB segData
				frmIBMyAccnts.lblOtherBankHdr.isVisible = false;
				frmIBAddMyAccnt.lblAddOtherBankHdr.isVisible = false;
				frmIBMyAccnts.segOtherBankAccntsList.removeAll();
				frmIBAddMyAccnt.segOtherBListFAA.removeAll();
				frmIBAddMyAccnt.hbox44557018542299.isVisible = false
				var rawDataTMB = [];
				var str = "";
				var imageName="";
		//		gblResultTableCustomer = resulttable.custAcctRec;
				MAX_OTHER_BANK = resulttable["max_non_tmb_count"];
				MAX_ACC_ADD = resulttable["max_acc_add"]; // Max no. of accnt that can be added at a time
				TMB_BANK_CD = resulttable["tmb_bank_cd"]; //TMB bannk ID value
				HIDDEN_ACCT_VAL = resulttable["hidden_acct_val"];
				if(resulttable["ownProfile"]==1){
					gblPersonalizeID = resulttable["PersonalizedID"];
				}
 				if (resulttable.custAcctRec == null || resulttable.custAcctRec.length == 0) {
					dismissLoadingScreenPopup();
  					//showAlert(kony.i18n.getLocalizedString("keyAllAccntHidden"), kony.i18n.getLocalizedString("info"));
					frmIBMyAccnts.lblOtherBankHdr.setVisibility(true);
					frmIBMyAccnts.lblOtherBankHdr.text=kony.i18n.getLocalizedString("keyAllAccntHidden");
				} else {
					populateMyAccountsListScreenIB(resulttable);
 				}
 			} else {
				dismissLoadingScreenPopup();
 				alert(resulttable["errMsg"]);
			}
		} else {
			dismissLoadingScreenPopup();
 			alert(resulttable["errMsg"]);
		}//opstatus
	} 
}

// Add accountflow : @ on Next button in frmIBMyaccnts after entring new account details while adding

function showConfirmationAddaccountIB() {

	if (IB_USER_STATUS_ID == "04") {
		showAlert(kony.i18n.getLocalizedString("keyErrTxnPasslock"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	var bank = frmIBMyAccnts.cmbobxBankType.selectedKeyValue[1]
	var accType = frmIBMyAccnts.cmbobxAccntType.selectedKeyValue[1]
	var accntNo = "";
	var nickName = frmIBMyAccnts.txtNickNAme.text;
	var suffix = "";
	var serach_accnt_no = "";
	if(frmIBMyAccnts.cmbobxBankType.selectedKeyValue[1]+":"== kony.i18n.getLocalizedString("keyBank")&&frmIBMyAccnts.hbox447417227842.isVisible==true){
		showAlert(kony.i18n.getLocalizedString("keySelectBank"), kony.i18n.getLocalizedString("info"))
		return false;
	}
	
	var searchFunction = 01; //01 = Loan,02 = TD(term deposite)(first 10 digit) ,03 = Credit card,SA,CA
	// If TMB Bank, get accnt no. based on accnt type
	// i18 for combobox data has to be added
	var isTMB = (bank == kony.i18n.getLocalizedString("keyTMBBank")) ? true : false;
	// If selected bank is TMB, check account type
	if (isTMB && accType == kony.i18n.getLocalizedString("keyIBAccounttype")) {
		showAlert(kony.i18n.getLocalizedString("keyIBPleaseSelectAccountType"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	// If TMB Bank, get accnt no. based on accnt type
	if (isTMB) {
		if (accType == kony.i18n.getLocalizedString("keylblDeposit")) {
			accntNo = "" + frmIBMyAccnts.txtAccNum1.text + "-" + frmIBMyAccnts.txtAccNum2.text + "-" + frmIBMyAccnts.txtAccNum3.text +
				"-" + frmIBMyAccnts.txtAccNum4.text;
			serach_accnt_no = accntNo.replace(/-/g, "");
			searchFunction = 02;
		} else if (accType == kony.i18n.getLocalizedString("keylblLoan")) {
			suffix = frmIBMyAccnts.txtbxSuffix.text
			
			accntNo = "0" + frmIBMyAccnts.txtAccNum1.text + "-" + frmIBMyAccnts.txtAccNum2.text + "-" + frmIBMyAccnts.txtAccNum3
				.text + "-" + frmIBMyAccnts.txtAccNum4.text +
				"-" + suffix;
			serach_accnt_no = accntNo.replace(/-/g, "");
			searchFunction = 01;
		} else if (accType == kony.i18n.getLocalizedString("keyCreditCardIB")) {
			accntNo = "" + frmIBMyAccnts.txtCCNum1.text + "-" + frmIBMyAccnts.txtCCNum2.text + "-" + frmIBMyAccnts.txtCCNum3.text +
				"-" + frmIBMyAccnts.txtCCNum4.text;
			serach_accnt_no = accntNo.replace(/-/g, "");
			searchFunction = 03;
		} else {
			accntNo = "" + frmIBMyAccnts.txtAccNum1.text + "-" + frmIBMyAccnts.txtAccNum2.text + "-" + frmIBMyAccnts.txtAccNum3.text +
				"-" + frmIBMyAccnts.txtAccNum4.text;
			serach_accnt_no = accntNo.replace(/-/g, "");
			searchFunction = 03;
		}
	} else {
		if (MAX_ACC_LEN == 10 && MAX_ACC_LEN_LIST.indexOf(",")<0) {
			accntNo = "" + frmIBMyAccnts.txtAccNum1.text + "-" + frmIBMyAccnts.txtAccNum2.text + "-" + frmIBMyAccnts.txtAccNum3.text +
				"-" + frmIBMyAccnts.txtAccNum4.text;
		} else
			accntNo = frmIBMyAccnts.txtbxOtherAccnt.text;
	}
	
	// Validate bank name
	if (kony.i18n.getLocalizedString("keylblBankName") == bank) {
	showAlert(kony.i18n.getLocalizedString("keyIBPleaseSelectBank"), kony.i18n.getLocalizedString("info"));
		return false;
			}
	
	// Printing max allowed accnt no. length and ORFT status. These value are set in call back of "getBankDetails" service every time when we
	// select bank from bank list
	
	var local_accnt_no = accntNo;
	local_accnt_no = local_accnt_no.replace(/-/g, "");
	
	var len = local_accnt_no.length;
	// Allowing only numeric value for accnt no.
	if(local_accnt_no == null || local_accnt_no == "") {
		showAlert(kony.i18n.getLocalizedString("keyErrAccntNoLen"), kony.i18n.getLocalizedString("info"))
		return false;
	}
	if (kony.string.isNumeric(local_accnt_no) == false) {
		showAlert(kony.i18n.getLocalizedString("keyErrAccntNoFormat"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	// validate accnt. no. length for NON TMB
	var x=MAX_ACC_LEN_LIST;
	var y=x.split(",");
	var z=parseInt(y[0]);
	if (!isTMB && (MAX_ACC_LEN_LIST.indexOf(len) < 0 || len < z)) {
		showAlert(kony.i18n.getLocalizedString("keyErrAccntNoLen"), kony.i18n.getLocalizedString("info"));
		return false;
	}

	
	for (var i = 0; i < gblMyAccntAddTmpData.length; i++) {
		var tmpAccntNo2 = gblMyAccntAddTmpData[i].lblAccntNumVal.replace(/-/g, "");
		if (accType == kony.i18n.getLocalizedString("keylblLoan")) {
			tmpAccntNo2 = "0" + tmpAccntNo2 + gblMyAccntAddTmpData[i].lblAccntsuffixValue;
		}
		
		if (tmpAccntNo2 == local_accnt_no || gblMyAccntAddTmpData[i].lblNNValue == nickName) {
			
			showAlert(kony.i18n.getLocalizedString("keyErrDataExistTmp"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
	var isFound = false;
	if (isTMB) {
		if (searchFunction == 01) {
			
			for (var i = 0; i < gblAccntData.length; i++) {
				if (gblAccntData[i].accntNo.replace(/-/g, "") == serach_accnt_no) {
					if (gblAccntData[i].accntStatus != HIDDEN_ACCT_VAL) {
						
						//alert("This Account no. is already added")
						showAlert(kony.i18n.getLocalizedString("keyErrDataExist"), kony.i18n.getLocalizedString("info"));
						return false;
					}
					gblAccntName = gblAccntData[i].accntName;
					isFound = true;
					break;
				}
			}
		} else if (searchFunction == 02) {
			
			for (var i = 0; i < gblAccntData.length; i++) {
				var accnLength = gblAccntData[i].accntNo.length;
				var existingAccnts =  gblAccntData[i].accntNo;
				if(accnLength == 14 ){
					existingAccnts = existingAccnts.substring(4, 14);
				}else if(accnLength == 13){
					existingAccnts = existingAccnts;
				}else{
					existingAccnts = existingAccnts;
				}
				if (existingAccnts == serach_accnt_no) {
					if (gblAccntData[i].accntStatus != HIDDEN_ACCT_VAL) {
						
						//alert("This Account no. is already added")
						showAlert(kony.i18n.getLocalizedString("keyErrDataExist"), kony.i18n.getLocalizedString("info"));
						return false;
					}
					gblAccntName = gblAccntData[i].accntName;
					isFound = true;
					break;
				}
			}
		} else {
			var len = serach_accnt_no.length;
			var index = -10;
			if (len == 16)
				index = -16;
			var tacc="";
			for (var i = 0; i < gblAccntData.length; i++) {
			tacc=gblAccntData[i].accntNo;
			if(tacc.length >= 16)
			tacc=tacc.substring(tacc.length-16, tacc.length)
				if (tacc.substr(index) == serach_accnt_no) {				
					if (gblAccntData[i].accntStatus != HIDDEN_ACCT_VAL) {
						showAlert(kony.i18n.getLocalizedString("keyErrDataExist"), kony.i18n.getLocalizedString("info"));
						return false;
					}
					gblAccntName = gblAccntData[i].accntName;
					isFound = true;
					break;
				}
			}
		}
		if (isFound == false) {
			showAlert(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	} else {
		for (var i = 0; i < gblAccntData.length; i++) {
			if (gblAccntData[i].accntNo.replace(/-/g, "") == local_accnt_no  &&  gblBankCD==gblAccntData[i]["bankCD"]) {
				
				showAlert(kony.i18n.getLocalizedString("keyErrDataExist"), kony.i18n.getLocalizedString("info"));
				return false;
			}
		}
	}

	//--------------for nick name & acc name validations 
	var textformat=NickNameValid(frmIBMyAccnts.txtNickNAme.text);
	if(textformat== false) {
	showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
	return false;
	}
	var textformat1=AccountNameValid(frmIBMyAccnts.tbxAccountName.text);
	if(textformat1== false&&frmIBMyAccnts.hbxAccountName.isVisible==true) {
	showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
	return false;
	}
	// Other BANK -  ORFTA account inquiry
	if (!isTMB && IS_ORFT == 'Y') {
		srvOrftAccountInqIB(local_accnt_no);
	} else {
		addToCacheLastStepIB();
	}
}

function srvOrftAccountInqIB(acctNo) {
	showLoadingScreenPopup();
	var inputParam = {}
	inputParam["toAcctNo"] = acctNo;
	inputParam["toFIIdent"] = gblBankCD;
	invokeServiceSecureAsync("ORFTInq", inputParam, orftAccountInqCallBackIB)
}

function orftAccountInqCallBackIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			if (resulttable["StatusCode"] == 0) {
				///fetching To Account Name for ORFT Inq
				gblAccntName = resulttable["ORFTTrnferInqRs"][0]["toAcctName"];
				dismissLoadingScreenPopup()
				addToCacheLastStepIB();
			} else {
				dismissLoadingScreenPopup()
			if (resulttable["errMsg"] != undefined ) alert(" " + resulttable["errMsg"]);
			else alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));			}
		} else {
			dismissLoadingScreenPopup()
			if (resulttable["errMsg"] != undefined ) alert(" " + resulttable["errMsg"]);
			else alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
		}
	}
}

function addToCacheLastStepIB() {
	// Validating nickName (alphanumeric and 1 to 20 length)
	var nickName = frmIBMyAccnts.txtNickNAme.text;
	var isTMB = (bank == kony.i18n.getLocalizedString("keyTMBBank")) ? true : false;
	if (NickNameValid(nickName) == false) {
  		showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	if(IS_ORFT != "Y" && !isTMB){
		var AccountName = frmIBMyAccnts.tbxAccountName.text;
		gblAccntName = AccountName;
		if (AccountNameValid(AccountName) == false) {
  		showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	}
	// Verify duplicate nick name and account no (if previously added in both My account list and my receipient list)
	for (var i = 0; i < gblAccntData.length; i++) {
		if (gblAccntData[i].nickName == nickName && gblAccntData[i].accntStatus != "02") {
  			showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
 			return false;
		}
	}

	var bank = frmIBMyAccnts.cmbobxBankType.selectedKeyValue[1];
 	if (!isTMB) {
		if ((NON_TMB_ADD + TOTAL_NON_TMB_AC) >= MAX_OTHER_BANK) {
 			showAlert(kony.i18n.getLocalizedString("keyErrMaxNonTMBAccnt"), kony.i18n.getLocalizedString("info"));
			return false;
		} else
			NON_TMB_ADD++;
	}
 	TOTAL_AC_ADD++;
	addAccountIB();
}

function addAccountIB() {
	showLoadingScreenPopup();
	//var newAccount = {};
	//var bank = frmMyAccntAddAccount.btnBanklist.text;
	var bank = frmIBMyAccnts.cmbobxBankType.selectedKeyValue[1];
	var bnkName =frmIBMyAccnts.cmbobxBankType.selectedKeyValue[1];
	var bnkCode =frmIBMyAccnts.cmbobxBankType.selectedKeyValue[0];
	gblBANKREF=bnkName;
	var bnkLogoURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +
				"/Bank-logo" + "/bank-logo-";
				
	var bankLogo = "";
	bankLogo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+bnkCode+"&modIdentifier=BANKICON";	
	bank = (bank == kony.i18n.getLocalizedString("keyTMBBank")) ? true : false;
	
	//Other bank
	if (bank == false) {
		var accntNo;
		if (MAX_ACC_LEN == 10 && MAX_ACC_LEN_LIST.indexOf(",")<0) {
			accntNo = "" + frmIBMyAccnts.txtAccNum1.text + "-" + frmIBMyAccnts.txtAccNum2.text + "-" + frmIBMyAccnts.txtAccNum3.text +
				"-" + frmIBMyAccnts.txtAccNum4.text;
		} else {
			accntNo = frmIBMyAccnts.txtbxOtherAccnt.text;
		}
		if (true) {
			gblMyAccntAddTmpData.push({
				"lblNN": kony.i18n.getLocalizedString("keyAddAccountNickname"),
				"lblNNValue": frmIBMyAccnts.txtNickNAme.text,
				"lblAccntNum": kony.i18n.getLocalizedString("keyAccountNoIB"),
				"lblAccntNumVal": accntNo,
				"lblAccntName": kony.i18n.getLocalizedString("AccountName"),
				"lblAccntNameVal": gblAccntName,
				"btnDelete": "DEL",
				"isTMB": "false",
				"bankCD": gblBankCD,
				"bankLogo":bankLogo,
				"lblBankName":bnkName
			});
		} else {
			gblMyAccntAddTmpData.push({
				"lblNN": kony.i18n.getLocalizedString("keyAddAccountNickname"),
				"lblNNValue": frmIBMyAccnts.txtNickNAme.text,
				"lblAccntNum": kony.i18n.getLocalizedString("keyAccountNoIB"),
				"lblAccntNumVal": accntNo,
				"btnDelete": "DEL",
				"isTMB": "false",
				"bankCD": gblBankCD,
				"bankLogo":bankLogo,
				"lblBankName":bnkName
			});
		}
	} else if (frmIBMyAccnts.cmbobxAccntType.selectedKeyValue[1] == kony.i18n.getLocalizedString("keylblLoan")) {
		
		gblMyAccntAddTmpData.push({
			"lblNN": kony.i18n.getLocalizedString("keyAddAccountNickname"),
			"lblNNValue": frmIBMyAccnts.txtNickNAme.text,
			"lblAccntNum": kony.i18n.getLocalizedString("keyAccountNoIB"),
			"lblAccntNumVal": frmIBMyAccnts.txtAccNum1.text + "-" + frmIBMyAccnts.txtAccNum2.text + "-" + frmIBMyAccnts.txtAccNum3
				.text + "-" + frmIBMyAccnts.txtAccNum4.text,
			"lblAccntName": kony.i18n.getLocalizedString("AccountName"),
			"lblAccntNameVal":gblAccntName ,
			"lblASufx": kony.i18n.getLocalizedString("keySuffixIB"),
			"lblASufxValue": frmIBMyAccnts.txtbxSuffix.text,
			"btnDelete": "DEL",
			"isTMB": "true",
			"bankCD": gblBankCD,
			"bankLogo":bankLogo,
			"lblBankName":bnkName
		});
	} else if (frmIBMyAccnts.cmbobxAccntType.selectedKeyValue[1] == kony.i18n.getLocalizedString("keylblDeposit") ||
		frmIBMyAccnts.cmbobxAccntType.selectedKeyValue[1] == kony.i18n.getLocalizedString("keyCurrentIB") ||
		frmIBMyAccnts.cmbobxAccntType.selectedKeyValue[1] == kony.i18n.getLocalizedString("keySavingIB")) {
		
		gblMyAccntAddTmpData.push({
			"lblNN": kony.i18n.getLocalizedString("keyAddAccountNickname"),
			"lblNNValue": frmIBMyAccnts.txtNickNAme.text,
			"lblAccntNum": kony.i18n.getLocalizedString("keyAccountNoIB"),
			"lblAccntNumVal": "" + frmIBMyAccnts.txtAccNum1.text + "-" + frmIBMyAccnts.txtAccNum2.text + "-" + frmIBMyAccnts.txtAccNum3
				.text + "-" + frmIBMyAccnts.txtAccNum4
				.text,
			"btnDelete": "DEL",
			"lblAccntName": kony.i18n.getLocalizedString("AccountName"),
			"lblAccntNameVal": gblAccntName,
			"isTMB": "true",
			"bankCD": gblBankCD,
			"bankLogo":bankLogo,
			"lblBankName":bnkName
		});
	} else if (frmIBMyAccnts.cmbobxAccntType.selectedKeyValue[1] == kony.i18n.getLocalizedString("keyCreditCardIB")) {
		
		gblMyAccntAddTmpData.push({
			"lblNN": kony.i18n.getLocalizedString("keyAddAccountNickname"),
			"lblNNValue": frmIBMyAccnts.txtNickNAme.text,
			"lblAccntNum": kony.i18n.getLocalizedString("keyCardNumberIB") + ":",
			"lblAccntNumVal": "" + frmIBMyAccnts.txtCCNum1.text + "-" + frmIBMyAccnts.txtCCNum2.text + "-" + frmIBMyAccnts.txtCCNum3
				.text + "-" + frmIBMyAccnts.txtCCNum4
				.text,
			"btnDelete": "DEL",
			"lblAccntName": kony.i18n.getLocalizedString("AccountName"),
			"lblAccntNameVal": gblAccntName,
			"isTMB": "true",
			"bankCD": gblBankCD,
			"bankLogo":bankLogo,
			"lblBankName":bnkName
		});
	} else {
		gblMyAccntAddTmpData.push({
			"lblNN": kony.i18n.getLocalizedString("keyAddAccountNickname"),
			"lblNNValue": frmIBMyAccnts.txtNickNAme.text,
			"lblAccntNum": kony.i18n.getLocalizedString("keyCardNumberIB") + ":",
			"lblAccntNumVal": "" + frmIBMyAccnts.txtAccNum1.text + "-" + frmIBMyAccnts.txtAccNum2.text + "-" + frmIBMyAccnts.txtAccNum3
				.text + "-" + frmIBMyAccnts.txtAccNum4
				.text,
			"btnDelete": "DEL",
			"lblAccntName": kony.i18n.getLocalizedString("AccountName"),
			"lblAccntNameVal": gblAccntName,
			"isTMB": "true",
			"bankCD": gblBankCD,
			"bankLogo":bankLogo,
			"lblBankName":bnkName
		})
	}
	frmIBAddMyAccnt.hbxCnrmtnStep1.setVisibility(true);
	frmIBAddMyAccnt.hbxConfrmtnStep2.setVisibility(false);
	frmIBAddMyAccnt.hbxCompleteAccnt.setVisibility(false);
	frmIBAddMyAccnt.hbxTokenUser.setVisibility(false);
	frmIBAddMyAccnt.show();
	dismissLoadingScreenPopup();
	frmIBAddMyAccnt.segAccntDetails.setVisibility(true);
	frmIBAddMyAccnt.segAccntDetails.setData(gblMyAccntAddTmpData);
	frmIBAddMyAccnt.line44747680925127.setVisibility(true);
}

// Service called when the bank  type is slecetf from the combobox while adding new account in frmIBMYaccnt.

function srvGetBankDetailIB(bankCode) {
	showLoadingScreenPopup();
	var inputparam = {};
	inputparam["bankCode"] = bankCode;
	invokeServiceSecureAsync("getBankDetails", inputparam, getBankDetailServiceCallBackIB);
}

function getBankDetailServiceCallBackIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			MAX_ACC_LEN_LIST=resulttable.Results[0].bankAccntLengths
			MAX_ACC_LEN = resulttable.Results[0].bankAccntLen;
			if (MAX_ACC_LEN == 10 && MAX_ACC_LEN_LIST.indexOf(",")<0) {
				frmIBMyAccnts.lblCreditCardNum.setVisibility(false);
				frmIBMyAccnts.lblsuffix.setVisibility(false);
				frmIBMyAccnts.txtbxSuffix.setVisibility(false);
				frmIBMyAccnts.label448396481135159.setVisibility(false);
				frmIBMyAccnts.hbxCreditCardNum.setVisibility(false);
				frmIBMyAccnts.hbxAccntNum.setVisibility(true);
				frmIBMyAccnts.lblAccntNum.setVisibility(true);
				frmIBMyAccnts.txtbxOtherAccnt.setVisibility(false);
				frmIBMyAccnts.txtAccNum1.text = "";
				frmIBMyAccnts.txtAccNum2.text = "";
				frmIBMyAccnts.txtAccNum3.text = "";
				frmIBMyAccnts.txtAccNum4.text = "";
				frmIBMyAccnts.tbxAccountName.text=""
			} else {
				frmIBMyAccnts.txtbxOtherAccnt.maxTextLength = parseInt(MAX_ACC_LEN);
				frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
				frmIBMyAccnts.lineComboAccntype.setVisibility(false);
				frmIBMyAccnts.lblsuffix.setVisibility(false);
				frmIBMyAccnts.txtbxSuffix.setVisibility(false);
				frmIBMyAccnts.lblCreditCardNum.setVisibility(false);
				frmIBMyAccnts.hbxCreditCardNum.setVisibility(false);
				frmIBMyAccnts.lblAccntNum.setVisibility(true);
				frmIBMyAccnts.hbxAccntNum.setVisibility(false);
				frmIBMyAccnts.txtbxOtherAccnt.setVisibility(true);
				frmIBMyAccnts.txtbxOtherAccnt.text = "";
				frmIBMyAccnts.txtNickNAme.text = "";
				frmIBMyAccnts.tbxAccountName.text="";
				
			}
			IS_ORFT = resulttable.Results[0].orftFlag;
			if(IS_ORFT == "Y"){
				frmIBMyAccnts.hbxAccountName.setVisibility(false);
				frmIBMyAccnts.line363582120273807.setVisibility(false);
			}else{
				frmIBMyAccnts.hbxAccountName.setVisibility(true);
				frmIBMyAccnts.line363582120273807.setVisibility(true);
			}
			
			
			dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			
			if(resulttable["errMsg"] != undefined)
	        	{
	        		alert(resulttable["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}	
	       		}
	} else if (status == 300) {
		dismissLoadingScreenPopup();
		
	}
}
//Called form frmIbAddmyaccnt after entring the otp


// Servie for the completion of add account

function srvAddCompleteProcessIB() {
	var inputparam = {};
	inputparam["personalizedAccList"] = [];
	var tmp_array = [];
	gblPersonalizeID = gblPersonalizeID;
	var collection = gblMyAccntAddTmpData;
	for (var i = 0; i < collection.length; i++) {
		var accntNo = collection[i].lblAccntNumVal.replace(/-/g, "");
		if (collection[i].lblASufx != null && collection[i].lblASufx != undefined && collection[i].lblASufx != "")
			accntNo = "0" + accntNo + collection[i].lblASufxValue;
		var tmpArray = ["", gblPersonalizeID, collection[i].bankCD, accntNo, collection[i].lblNNValue, "added", "null"];
		tmp_array.push(tmpArray)
		var Number = accntNo;
		var NickName = collection[i].lblNNValue;
		var BankName = collection[i].lblBankName;
		gblEditTMBpersonalizedAcctId1=Number;
	    gblEditTMBhiddenAcctName=NickName;
	    
		activityLogServiceCall("055", "", "01", "", "Add", NickName, BankName, Number, "", "");
	}
	gblAddAccnt = true;
	inputparam["personalizedAccList"] = tmp_array.toString();
	var status = invokeServiceSecureAsync("receipentAddBankAccntService", inputparam, AddCompleteProcessCallBackIB);
}



function IBreqOTPMYAccnts() {
    showLoadingScreenPopup();
    var accDetailMsg = "";
    var locale = kony.i18n.getCurrentLocale();
    var tem = frmIBAddMyAccnt.segCnfrmtnStep2.data;
    var count1 = 0;
    var count2 = 0;
    var oCount = [];
    var obn="";
    var accString = kony.i18n.getLocalizedString("Receipent_Account");
    
    frmIBAddMyAccnt.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBAddMyAccnt.lblPlsReEnter.text = " "; 
    frmIBAddMyAccnt.hbxOTPincurrect.isVisible = false;
    frmIBAddMyAccnt.hbox450035142444916.isVisible = true;
    frmIBAddMyAccnt.hbox44747680933513.isVisible = true;  
    
    for (var f = 0; f < gblBankList.length; f++) {
        oCount.push(0);
    }
    for (var i = 0; i < tem.length; i++) {
        var bnName = tem[i]["lblBankName"];
        var isTmb = (bnName == kony.i18n.getLocalizedString("keyTMBBank")) ? true : false;
        if (isTmb) {
            count1++;
        } else {
            count2++;
            
            for (var g = 0; g < gblBankList[0].length; g++) {
                
                if (bnName == gblBankList[0][g][1] && bnName != kony.i18n.getLocalizedString("keyTMBBank")) {
                    oCount[g]=kony.os.toNumber(oCount[g]);
					 
					obn=gblBankList[0][g][2]
							oCount[g]++;
                    
                }
            }
        }
    }
    var tcount = count1 + count2;
    var accDetailMsgt = ""
    if(tcount>1){
	    for (var h = 0; h < oCount.length; h++) {
	        if (oCount[h] > 0) {
	            
	            accDetailMsgt = accDetailMsgt + oCount[h] + ":" + gblBankList[0][h][2] + ", "
	        }
	    }
    }
    else{
    		if(count2==0){
    				accDetailMsg= "1 "+accString+" (TMB)";
    		}
    		else{
    				accDetailMsg="1 "+accString+" ("+obn+")";
    		}   
    }
    if(accDetailMsgt.length>1)
    {
	  accDetailMsgt=accDetailMsgt.substring(0, accDetailMsgt.length - 2);
	}
    if (count1 > 0 && count2 > 0) {
        accDetailMsg = tcount +" "+ accString +" (" + count1 + ":TMB," + accDetailMsgt + ")";
    } else if (count1 == 0 && count2 > 1) {
        accDetailMsg = tcount + " "+ accString +" (" + accDetailMsgt + ")";
    } else if (count1 > 1 && count2 == 0) {
        accDetailMsg = tcount + " "+ accString +" (" + count1 + ":TMB)";
    }
    
    var inputParams = {};
    var locale = kony.i18n.getCurrentLocale();
    /*
    if (kony.i18n.getCurrentLocale() == "en_US") {
        inputParams["eventNotificationPolicyId"] = "MIB_AddMyAcc_EN";
        inputParams["SMS_Subject"] = "MIB_AddMyAcc_EN";
    } else {
        inputParams["eventNotificationPolicyId"] = "MIB_AddMyAcc_TH";
        inputParams["SMS_Subject"] = "MIB_AddMyAcc_TH";
    }*/
    inputParams["Channel"] = "AddMyAccount";
    inputParams["locale"] = "locale";
    inputParams["AccDetailMsg"] = ""; //accDetailMsg;
	invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackIBreqOTPIBMYA);

}

function callBackIBreqOTPIBMYA(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
			dismissLoadingScreenPopup();
			showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
			dismissLoadingScreenPopup();
			showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
			return false;
		}		
		if (callBackResponse["opstatus"] == 0) 
		{
			frmIBAddMyAccnt.btnOTPReq.skin = btnIBREQotp;
			frmIBAddMyAccnt.btnOTPReq.setEnabled(false)
			kony.timer.schedule("MYTimerID2", funOTP, 60, false);
			dismissLoadingScreenPopup();
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			gblOTPReqLimit=kony.os.toNumber(callBackResponse["otpRequestLimit"]);
			gblOTPRequestCounter++;
			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++)
			{
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
			frmIBAddMyAccnt.hbxOTP.setVisibility(true);
			frmIBAddMyAccnt.txtOTP.setFocus(true);
			frmIBAddMyAccnt.hbox44747680935256.setVisibility(true);	
			frmIBAddMyAccnt.lblRefNum.setVisibility(true);
			frmIBAddMyAccnt.lblRefNumValue.setVisibility(true);
			frmIBAddMyAccnt.lblOTPMSG.setVisibility(true);
			frmIBAddMyAccnt.lblOTPMblNum.setVisibility(true);
			frmIBAddMyAccnt.txtOTP.maxTextLength = gblOTPLENGTH
			frmIBAddMyAccnt.lblRefNumValue.text =refVal ;   // callBackResponse["Collection1"][8]["ValueString"]; //callBackResponse["pac"]; //callBackResponse["bankRefNo"];
			frmIBAddMyAccnt.lblOTPMblNum.text = maskingIB(gblPHONENUMBER);
			//dismissLoadingScreenPopup();
		} else {
			frmIBAddMyAccnt.lblRefNum.setVisibility(false);
			frmIBAddMyAccnt.lblRefNumValue.setVisibility(false);
			frmIBAddMyAccnt.lblOTPMSG.setVisibility(false);
			frmIBAddMyAccnt.lblOTPMblNum.setVisibility(false);
			
			dismissLoadingScreenPopup();			
			if(callBackResponse["errMsg"]!= undefined)
			{
 				alert(" " + callBackResponse["errMsg"]);			
			}
			else
			{
 				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
			}		}
	}
}
// Service called from postshow of frmIBMyaccnts to get all the banks to populate as banktype combobox values while adding new account flow

function srvGetBankListIB() {
	//showLoadingScreenPopup();
	var getBankList_inputparam = {};
	invokeServiceSecureAsync("getBankList", getBankList_inputparam, getBankListServiceCallBackIB);
}

function getBankListServiceCallBackIB(status, resulttable) {
	gblBankList = [];
 	if (status == 400) {
 		if (resulttable["opstatus"] == 0) {
			bankListArray = [];
			var value = "";
			var key = "";
			// This locale is used to change bank name in list to current locale
			var locale = kony.i18n.getCurrentLocale();
			var collectionData = resulttable["Results"];
			for (var i = 0; i < resulttable.Results.length; i++) {
				var obj = [];
 				if ((resulttable.Results[i].bankStatus.toUpperCase() == "ACTIVE") && (resulttable.Results[i].smartFlag.toUpperCase() == "Y" || resulttable.Results[i].orftFlag.toUpperCase() == "Y")) {
					if (kony.string.startsWith(locale, "en", true) == true)
						value = resulttable.Results[i].bankNameEng;
					else
						value = resulttable.Results[i].bankNameThai;
					var key = resulttable.Results[i].bankCode;
					obj[0] = key;
					obj[1] = value;
					obj[2] = resulttable.Results[i].bankShortName;
					obj[3] = resulttable.Results[i].bankNameEng;
					obj[4] = resulttable.Results[i].bankNameThai;
 					bankListArray.push(obj);	
 					var tempRecord;			
 					if (locale == "en_US") {
					tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
						collectionData[i]["smartFlag"],collectionData[i]["bankAccntLen"],collectionData[i]["bankAccntLengths"],collectionData[i]["bankNameEng"],collectionData[i]["bankNameThai"]];
					} else {
					tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
						collectionData[i]["smartFlag"],collectionData[i]["bankAccntLen"],collectionData[i]["bankAccntLengths"],collectionData[i]["bankNameEng"],collectionData[i]["bankNameThai"]];
					}
					globalSelectBankData.push(tempRecord);	
				}
 			}
			gblBankList.push(bankListArray);
			frmIBMyAccnts.cmbobxBankType.masterData = bankListArray;
			frmIBMyAccnts.cmbobxBankType.selectedKey = "key1"

		} else {
			
			dismissLoadingScreenPopup();
			if(resulttable["errMsg"]!= undefined)
			{
 				alert(" " + resulttable["errMsg"]);			
			}
			else
			{
 				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
			}		}
	}  
}
// Service called from postshow of frmIBMyaccnts to get all the account types to populate as accounttype combobox values while adding new account flow

function getAccountNoIB(accntNo) {
	var tmpAccntNo = accntNo; // Same for LOC
	if (gblsegID == "segTMBAccntsList") {
		var tmpAccntType = frmIBMyAccnts.segTMBAccntsList.data[gblMyAccountSelectedIndex].hiddenAcctType
	} else {
		var tmpAccntType = frmIBMyAccnts.segOtherBankAccntsList.data[gblMyAccountSelectedIndex].hiddenAcctType
	}
	if (tmpAccntType == "CCA") {
		tmpAccntNo = tmpAccntNo.substring(tmpAccntNo.length - 16, tmpAccntNo.length); // Last 16 digit
	} else if (tmpAccntType == "CDA") {
		tmpAccntNo = tmpAccntNo.substring(4, 15); //10 digit after first 0
	} else if (tmpAccntType == "SDA" || tmpAccntType == "DDA") {
		tmpAccntNo = tmpAccntNo.substring(tmpAccntNo.length - 10, tmpAccntNo.length); // Last 10 digit
	}
	
	return tmpAccntNo;
}
// Called in preshow of frmIBMyaccnts and everywhere to refresh the segments data while editing n deleting the account

function gblAddAccntVarIB() {
	// Global Var that will be used in My Account Add
	MAX_OTHER_BANK = 50;
	MAX_ACC_ADD = 0; // Max no. of accnt that can be added at a time
	TOTAL_AC_ADD = 0; // Total accnt in cache - global var
	bankListArray = []; // Storing the list of Active Banks
	acctList = [];
	gblRawDataTMB = [];
	gblAccntData = []; // Store all accnt data TMb and NON TMB for further use in this module
	gblMyAccountFlowFlag = 01; // true for Credit card and false for others( loan and credit)
	IB_USER_STATUS_ID = 0;
	TOTAL_NON_TMB_AC = 0;
	MAX_ACC_LEN = 0;
	IS_ORFT = "N";
	flowSpa = false; // Delete This
	gblAccntType = "";
}


function sortByKeyIB(array, key) {
	return array.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}


function showAlertIB(keyMsg, KeyTitle) {
	var okk = kony.i18n.getLocalizedString("keyOK");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: handle2IB
	};
	var pspConf = {};
	var infoAlert = kony.ui.Alert(basicConf, pspConf);

	function handle2IB(response) {}
	return;
}

function maskingIB(phoneno) {
	if (phoneno != "") {
		return phoneno = "xxx-xxx-" + phoneno.slice(6);
	} else {
		alert("Mobile number is not avialable ");
		return false;
	}
}

function showCommonAlertMYA(msg, statusCode) {
	if (statusCode != null && statusCode.length > 0) {
		var errStr = kony.i18n.getLocalizedString(statusCode);
		if (errStr != null && errStr.length > 0) {
			alert(errStr + "[" + statusCode + "]");
		} else {
			if (msg != null && msg.length > 0) {
				alert(msg);
			} else {
 				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
			}
		}
	} else {
		if (msg != null && msg.length > 0) {
			alert(msg);
		} else {
 				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
		}
	}
}


/**
 * Callback method for startCrmUpdateProIBApplyServices()
 * @returns {}
 */

//Function to clear when logout is clicked and clled from callBackIBLogoutService

function gblInitMyaccnt() {
	gblMyAccntAddTmpData = [];
	gblMyAccntAddTmpData.length = 0; // Cache memory - global array
	frmIBAddMyAccnt.segAccntDetails.removeAll();

}

function myAccountDelServiceCallBackIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			gblDelAccnt = true;
			gblAddAccntVarIB();
			frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
			if(kony.application.getCurrentForm().id=="frmIBDreamSavingMaintenance")
			frmIBMyAccnts.show();
			else
			//myAccountListServiceIB();
			onClickBackFromEditMyAccountsIB();
		} else {
			dismissLoadingScreenPopup();
			if(resulttable["errMsg"]!= undefined)
			{
 				alert(" " + resulttable["errMsg"]);			
			}
			else
			{
 				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
			}
		}
	} 
}

function myAccountEditServiceCallBackIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) 
		{
			dismissLoadingScreenPopup();
			var newNickName = frmIBEditMyAccounts.txtbxEditAccntNN.text;
			frmIBEditMyAccounts.lblViewNickNameHdr.text = newNickName;
			if(frmIBMyAccnts.segTMBAccntsList.selectedItems != undefined) {
				frmIBMyAccnts.segTMBAccntsList.selectedItems[0].lblSegTMBAccntDetails = newNickName;
			}
			
			var bankCde="";
	
			if (gblEditNTMBbankCD == "" && gblsegID == "segOtherBankAccntsList") {
				bankCde=frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankCD;
			}else{
				bankCde=gblEditNTMBbankCD;
			}
			if(gblsegID == "segOtherBankAccntsList" && getORFTFlagIBMyAcc(bankCde) == "N")
			frmIBEditMyAccounts.lblViewAccntNameVal.text=frmIBEditMyAccounts.tbxEditAccName.text;
			gblMyAccntNickNme = newNickName;
			gblAddAccntVarIB();
			//myAccountListServiceIB();
			frmIBEditMyAccounts.hbxEditAccntNN.setVisibility(false);
			frmIBEditMyAccounts.hbxViewAccnt.setVisibility(true);
			frmIBEditMyAccounts.hbxTMBImg.setVisibility(false);
			frmIBEditMyAccounts.hbxEditAccntNN.setVisibility(false);
			if(gblEditClickFromAccountDetails){
				accountsummaryLangToggleIB();
			}else{
				onClickSegmentMyAccountsTMBIB();
			}
			//frmIBMyAccnts.show();
		} else {
			dismissLoadingScreenPopup();
			if(resulttable["errMsg"]!= undefined)
			{
 				alert(" " + resulttable["errMsg"]);			
			}
			else
			{
 				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
			}
			return false;
		}
	}
}

function saveAddAccountDetailsCallbackIB(status, callBackResponse){
		if (status == 400) {
			
			if(callBackResponse["addAccountList"] != null && callBackResponse["addAccountList"]!=undefined)
			var accounts=callBackResponse["addAccountList"].split(",");
			
			//New 0608
			var currForm = kony.application.getCurrentForm().id;
			
		
			if(gblSwitchToken == false && gblTokenSwitchFlag == false){
				checkAddMyAcctTokenFlag();
			} 
			if(gblTokenSwitchFlag == true && gblSwitchToken == false){
			
					frmIBAddMyAccnt.hbxTokenEntry.setVisibility(true);
					frmIBAddMyAccnt.hbxOTPEntry.setVisibility(false);
					frmIBAddMyAccnt.hbxOTP.setVisibility(true);
					frmIBAddMyAccnt.txtToken.setFocus(true);
					frmIBAddMyAccnt.hbox44747680935256.setVisibility(true);
					frmIBAddMyAccnt.label474165825167354.text=kony.i18n.getLocalizedString("Receipent_tokenId");
					frmIBAddMyAccnt.hbox450035142444916.setVisibility(false);
					frmIBAddMyAccnt.hbox44747680933513.setVisibility(false);
					frmIBAddMyAccnt.hbox44747680935256.setVisibility(true);                          
			} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
				    frmIBAddMyAccnt.hbxTokenEntry.setVisibility(false);
				    frmIBAddMyAccnt.hbxOTPEntry.setVisibility(true);
				    frmIBAddMyAccnt.txtOTP.setFocus(true);
				    frmIBAddMyAccnt.label474165825167354.text=kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBAddMyAccnt.hbox450035142444916.setVisibility(true);
					frmIBAddMyAccnt.hbox44747680933513.setVisibility(true);	                          
			        IBreqOTPMYAccnts();
			}
		}
}

function onAddMyAcctNextIB() {
	var inputParams={};
	var tmp_array = [];
	var accountList = "";
	var collection = gblMyAccntAddTmpData;
	
	/*
	for (var i = 0; i < collection.length; i++) {
		var accntNo = collection[i].lblAccntNumVal.replace(/-/g, "");
		if (collection[i].lblASufx != null && collection[i].lblASufx != undefined && collection[i].lblASufx != "")
			accntNo = "0" + accntNo + collection[i].lblASufxValue;
		
		accountList +=accntNo; 		
		
	}
	*/
	
	for (var i = 0; i < collection.length; i++) {
		var accntNo = collection[i].lblAccntNumVal.replace(/-/g, "");
		if (collection[i].lblASufx != null && collection[i].lblASufx != undefined && collection[i].lblASufx != "")
			accntNo = "0" + accntNo + collection[i].lblASufxValue;
		var acccname=collection[i].lblAccntNameVal;
			if(acccname == null || acccname == undefined || acccname== "")
				acccname = "null";
			else
				acccname = acccname.trim();	
		var tmpArray = ["", gblPersonalizeID, collection[i].bankCD, accntNo.trim(), collection[i].lblNNValue.trim(), "added", acccname.trim()];
		tmp_array.push(tmpArray)
	}
	
	//var inputCValue = crc32(new String(tmp_array.toString().trim()));
	//var encCrcVal = encryptData(inputCValue.toString());
	
	//inputParams["encCrcpersonalizedAccList"]= encCrcVal;
	inputParams["personalizedAccList"]= tmp_array.toString();	//encMyAccountData(accountList);			//encMyAccountData(tmp_array.toString());   //tmp_array.toString();
	
	//Changes for OTP
    var accDetailMsg = "";
    var tem = frmIBAddMyAccnt.segCnfrmtnStep2.data;
    var count1 = 0;
    var count2 = 0;
    var oCount = [];
    var obn="";
    var accString = kony.i18n.getLocalizedString("Receipent_Account");
    for (var f = 0; f < gblBankList.length; f++) {
        oCount.push(0);
    }
    for (var i = 0; i < tem.length; i++) {
        var bnName = tem[i]["lblBankName"];
        var isTmb = (bnName == kony.i18n.getLocalizedString("keyTMBBank")) ? true : false;
        if (isTmb) {
            count1++;
        } else {
            count2++;
            
            for (var g = 0; g < gblBankList[0].length; g++) {
                
                if (bnName == gblBankList[0][g][1] && bnName != kony.i18n.getLocalizedString("keyTMBBank")) {
                    oCount[g]=kony.os.toNumber(oCount[g]);
					 
					obn=gblBankList[0][g][2]
							oCount[g]++;
                    
                }
            }
        }
    }
    var tcount = count1 + count2;
    var accDetailMsgt = ""
    if(tcount>1){
	    for (var h = 0; h < oCount.length; h++) {
	        if (oCount[h] > 0) {
	            
	            accDetailMsgt = accDetailMsgt + oCount[h] + ":" + gblBankList[0][h][2] + ", "
	        }
	    }
    }
    else{
    		if(count2==0){
    				accDetailMsg= "1 "+accString+" (TMB)";
    		}
    		else{
    				accDetailMsg="1 "+accString+" ("+obn+")";
    		}   
    }
    if(accDetailMsgt.length>1)
    {
	  accDetailMsgt=accDetailMsgt.substring(0, accDetailMsgt.length - 2);
	}
    if (count1 > 0 && count2 > 0) {
        accDetailMsg = tcount +" "+ accString +" (" + count1 + ":TMB," + accDetailMsgt + ")";
    } else if (count1 == 0 && count2 > 1) {
        accDetailMsg = tcount + " "+ accString +" (" + accDetailMsgt + ")";
    } else if (count1 > 1 && count2 == 0) {
        accDetailMsg = tcount + " "+ accString +" (" + count1 + ":TMB)";
    }
    
   
   	//var inputCVal = crc32(new String(accDetailMsg.toString().trim()));
	//var encCrcValue = encryptData(inputCVal.toString());
   
    //inputParams["encCrcAccDetailMsg"] = encCrcValue;
    inputParams["AccDetailMsg"] = accDetailMsg;
	
	invokeServiceSecureAsync("saveAddAccountDetails", inputParams, saveAddAccountDetailsCallbackIB)
	
	/*
    var currForm = kony.application.getCurrentForm().id;
	

	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		checkAddMyAcctTokenFlag();
	} 
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
	
			frmIBAddMyAccnt.hbxTokenEntry.setVisibility(true);
			frmIBAddMyAccnt.hbxOTPEntry.setVisibility(false);
			frmIBAddMyAccnt.label474165825167354.text=kony.i18n.getLocalizedString("Receipent_tokenId");
			frmIBAddMyAccnt.hbox450035142444916.setVisibility(false);
			frmIBAddMyAccnt.hbox44747680933513.setVisibility(false);	                          
	} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
		    frmIBAddMyAccnt.hbxTokenEntry.setVisibility(false);
		    frmIBAddMyAccnt.hbxOTPEntry.setVisibility(true);
		    frmIBAddMyAccnt.label474165825167354.text=kony.i18n.getLocalizedString("keyotpmsgreq");
			frmIBAddMyAccnt.hbox450035142444916.setVisibility(true);
			frmIBAddMyAccnt.hbox44747680933513.setVisibility(true);	                          
	        IBreqOTPMYAccnts();
	}
	*/
}
function checkAddMyAcctTokenFlag() {
		var inputParam = [];
		inputParam["crmId"] = gblcrmId;
		showLoadingScreenPopup();
		invokeServiceSecureAsync("tokenSwitching", inputParam, ibAddMyAcctTokenFlagCallbackfunction);
}
function ibAddMyAcctTokenFlagCallbackfunction(status,callbackResponse){
		var currForm = kony.application.getCurrentForm().id;
		
		if(status==400){
				if(callbackResponse["opstatus"] == 0){
					dismissLoadingScreenPopup();
					if(callbackResponse["deviceFlag"].length==0){
						
						//return
					}
					if ( callbackResponse["deviceFlag"].length > 0 ){
							var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
							var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
							var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
                             if ( tokenFlag=="Y" && ( mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02'){
								frmIBAddMyAccnt.hbxTokenEntry.setVisibility(true);
								frmIBAddMyAccnt.hbxOTPEntry.setVisibility(false);
								frmIBAddMyAccnt.hbxOTP.setVisibility(true);
								frmIBAddMyAccnt.txtToken.setFocus(true);
								frmIBAddMyAccnt.hbox44747680935256.setVisibility(true);
								frmIBAddMyAccnt.label474165825167354.text=kony.i18n.getLocalizedString("Receipent_tokenId");
								frmIBAddMyAccnt.hbox450035142444916.setVisibility(false);
								frmIBAddMyAccnt.hbox44747680933513.setVisibility(false);	                          
								gblTokenSwitchFlag = true;
							} else {
							    frmIBAddMyAccnt.hbxTokenEntry.setVisibility(false);
							    frmIBAddMyAccnt.hbxOTPEntry.setVisibility(true);
							    frmIBAddMyAccnt.txtOTP.setFocus(true);
							    frmIBAddMyAccnt.label474165825167354.text=kony.i18n.getLocalizedString("keyotpmsgreq");
								frmIBAddMyAccnt.hbox450035142444916.setVisibility(true);
								frmIBAddMyAccnt.hbox44747680933513.setVisibility(true);	                          
								gblTokenSwitchFlag = false;
								IBreqOTPMYAccnts();
							}
						}
					 else
					{
					 
							    frmIBAddMyAccnt.hbxTokenEntry.setVisibility(false);
							    frmIBAddMyAccnt.hbxOTPEntry.setVisibility(true);
							    frmIBAddMyAccnt.txtOTP.setFocus(true);
							    frmIBAddMyAccnt.label474165825167354.text=kony.i18n.getLocalizedString("keyotpmsgreq");
								frmIBAddMyAccnt.hbox450035142444916.setVisibility(true);
								frmIBAddMyAccnt.hbox44747680933513.setVisibility(true);	                          
					            IBreqOTPMYAccnts();
					 }
						
				}
		}
		
} 


function funOTP(){
	frmIBAddMyAccnt.btnOTPReq.skin = btnIBREQotpFocus;
	frmIBAddMyAccnt.btnOTPReq.setEnabled(true);
	if(checkOtpRequestCounter()){
	frmIBAddMyAccnt.btnOTPReq.skin = btnIBREQotp;
	frmIBAddMyAccnt.btnOTPReq.setEnabled(false)
	}
	kony.timer.cancel("MYTimerID2");

}

function LoadMasterConfigurationsForMyAccounts(resulttable){
	MAX_ACC_ADD = resulttable["max_acc_add"]; // Max no. of accnt that can be added at a time
	TMB_BANK_CD = resulttable["tmb_bank_cd"]; //TMB bannk ID value
	HIDDEN_ACCT_VAL = resulttable["hidden_acct_val"];
	var key = "";
	var value = "";
	acctList=[];
	acctList.push(["key2",kony.i18n.getLocalizedString("keyIBAccounttype")]);
	for (var i = 0; i < resulttable.AcctList.length; i++) {
		var acctObj = [];
		key = i; // resulttable["tmb_bank_cd"];
		value = resulttable.AcctList[i].acctType;
		acctObj[0] = key;
		acctObj[1] = value;
		acctList.push(acctObj);
	}	
	frmIBMyAccnts.cmbobxAccntType.masterData = acctList;
}
function getORFTFlagIBMyAcc(bankCode){
	if(bankCode == undefined || bankCode == null){
		return "N";
	}
	var orftFlag="N";
	for(var i=0;i<bankListArray.length;i++){
			if(bankCode == bankListArray[i][0]){
				orftFlag= bankListArray[i][5];
				break;
			}
	}
	return orftFlag;
}

function onClickEditMyaccount(){
	
	if (getCRMLockStatus()) {
		curr_form=kony.application.getCurrentForm().id;
		popIBBPOTPLocked.show();
	}
	else{
		frmIBEditMyAccounts.hbxTMBImg.setVisibility(false);
		//frmIBMyAccnts.imgArrow.setVisibility(false);
		frmIBEditMyAccounts.hbxViewAccnt.setVisibility(false);
		frmIBEditMyAccounts.hbxEditAccntNN.setVisibility(true);
		frmIBEditMyAccounts.txtbxEditAccntNN.text = frmIBEditMyAccounts.lblViewNickNameHdr.text;
		frmIBEditMyAccounts.txtbxEditAccntNN.setFocus(true);
	}
	var bankCde="";
	if (gblEditNTMBbankCD == "" && gblsegID == "segOtherBankAccntsList") {
		bankCde=frmIBMyAccnts.segOtherBankAccntsList.selectedItems[0].bankCD;
	}
	else{
		bankCde=gblEditNTMBbankCD;
	}
	if(gblsegID == "segOtherBankAccntsList" && getORFTFlagIBMyAcc(bankCde) == "N"){
		frmIBEditMyAccounts.tbxEditAccName.text=frmIBEditMyAccounts.lblViewAccntNameVal.text;
		frmIBEditMyAccounts.lblEditAccName.setVisibility(true);
		frmIBEditMyAccounts.tbxEditAccName.setVisibility(true)
	}else{
		frmIBEditMyAccounts.tbxEditAccName.text=frmIBEditMyAccounts.lblViewAccntNameVal.text;
		frmIBEditMyAccounts.lblEditAccName.setVisibility(false);
		frmIBEditMyAccounts.tbxEditAccName.setVisibility(false);
	}
}
function LoadBankListforMyAccounts(resulttable) {
    gblBankList = [];
    bankListArray = [];
    bankListArray.push(["key1", kony.i18n.getLocalizedString("keylblBankName")]);
    var value = "";
    var key = "";
    // This locale is used to change bank name in list to current locale
    var locale = kony.i18n.getCurrentLocale();
    for (var i = 0; i < resulttable.BankList.length; i++) {
        var obj = [];
        if ((resulttable.BankList[i].bankStatus == "01" || resulttable.BankList[i].bankStatus.toUpperCase() == "ACTIVE") && (resulttable.BankList[i].smartFlag.toUpperCase() == "Y" || resulttable.BankList[i].orftFlag.toUpperCase() == "Y")) {
            if (kony.string.startsWith(locale, "en", true) == true) value = resulttable.BankList[i].bankNameEng;
            else value = resulttable.BankList[i].bankNameThai;
            var key = resulttable.BankList[i].bankCode;
            obj[0] = key;
            obj[1] = value;
            obj[2] = resulttable.BankList[i].bankShortName;
            obj[3] = resulttable.BankList[i].bankNameEng;
			obj[4] = resulttable.BankList[i].bankNameThai;
			obj[5] = resulttable.BankList[i].orftFlag;
 			bankListArray.push(obj);
        }
    }
    gblBankList.push(bankListArray);
    frmIBMyAccnts.cmbobxBankType.masterData = bankListArray;
    frmIBMyAccnts.cmbobxBankType.selectedKey = "key1"
}


function populateMyAccountsListScreenIB(resulttable) {
	
 			var imageName = "piggyblueico.png";
			var rawDataTMB = [];
			var rawDataOther = [];
			gblAccntData=[];
 			var locale = kony.i18n.getCurrentLocale();
			for (var i = 0; resulttable.custAcctRec != null && i < resulttable.custAcctRec.length; i++) {
				var nickName = resulttable.custAcctRec[i].acctNickName;
				//alert(resulttable.custAcctRec[i].accType);
				if(nickName == undefined || nickName =="")
				{
					var accountId = resulttable.custAcctRec[i].accId;
					var startIndx = resulttable.custAcctRec[i].accId.length - 4;
					if(kony.string.startsWith(locale, "en", true) == true){
						nickName =  resulttable.custAcctRec[i].ProductNameEng;
					 }
					 else
					 {
					 	nickName =  resulttable.custAcctRec[i].ProductNameThai
					 }
					
					 if(nickName != null && nickName != undefined && nickName.length > 15)
					 nickName = nickName.substring(0, 15);
					
					 if(resulttable.custAcctRec[i].accType == kony.i18n.getLocalizedString("Loan"))
					 nickName =  nickName +" "+ accountId.substring(7, 11) ;
					 else 
				     nickName =  nickName +" "+ accountId.substring(startIndx, accountId.length) 
				    	}			
 				gblAccntData.push({
					"nickName": nickName,
					"accntNo": resulttable.custAcctRec[i].accId,
					"bankCD": resulttable.custAcctRec[i].bankCD,
					"accntName": resulttable.custAcctRec[i].accountName,
					"accntStatus": resulttable.custAcctRec[i].personalisedAcctStatusCode
				});
				
				if (resulttable.custAcctRec[i].personalisedAcctStatusCode != HIDDEN_ACCT_VAL) {
 				/*** Product logos starts here.**/
					var imageName="";
 					imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
 				/*** till here product logos **/
  					rawDataTMB.push({
						"isTMB": "true",
						"lblSegTMBAccntDetails": nickName,
						"hiddenAcctType": resulttable.custAcctRec[i].accType,
						"hiddenAcctName": resulttable.custAcctRec[i].accountName,
						"imgAccountPicture": imageName,
						"SortId": resulttable.custAcctRec[i].SortId,
						"personalizedAcctId": resulttable.custAcctRec[i].accId,
						"hiddenAccountNo": resulttable.custAcctRec[i].accId,
						"personalizedId": resulttable.custAcctRec[i].persionlizedId,
						"bankCD": resulttable.custAcctRec[i].bankCD,
						"imgTMBSegArrow": "bg_arrow_right.png",
						"productID" :resulttable.custAcctRec[i].productID,
						"fiident": resulttable.custAcctRec[i].fiident,
						"hiddenOpeningMethod":resulttable.custAcctRec[i].openingMethod,
						"hiddenProductNameEng":resulttable.custAcctRec[i].ProductNameEng,
						"hiddenProductNameThai":resulttable.custAcctRec[i].ProductNameThai
					});
				}
			}
			if(resulttable.Results!= undefined){
			for (var i = 0; resulttable.Results != null && i < resulttable.Results.length; i++) {
 			   var bnkCode =resulttable.Results[i].bankCD
				var bnkLogoURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +
							"/Bank-logo" + "/bank-logo-";
 				var bankLogo = "";
							if (bnkCode == 11)
								bankLogo = "icon.png";
							else
								bankLogo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+bnkCode+"&modIdentifier=BANKICON";
			
				gblAccntData.push({
					"nickName": resulttable.Results[i].acctNickName,
					"accntNo": resulttable.Results[i].personalizedAcctId,
					"bankCD": resulttable.Results[i].bankCD,
					"accntName": resulttable.Results[i].accntName,
					"accntStatus": "NA"
				});
				if (resulttable.Results[i].acctStatus != HIDDEN_ACCT_VAL) {
				rawDataOther.push({
					"isTMB": "false",
					"lblSegOtherBnkAccntDetails": resulttable.Results[i].acctNickName,
					"imgotherBankAcntPic": bankLogo,
					"hiddenAccountNo": resulttable.Results[i].personalizedAcctId,
					"hiddenAcctName": resulttable.Results[i].accntName,
					"personalizedAcctId": resulttable.Results[i].personalizedAcctId,
					"personalizedId": resulttable.PersonalizedID,
					"imgSegOtherBArrow": "bg_arrow_right.png",
					"bankCD": resulttable.Results[i].bankCD,
					"bankNameEng": resulttable.Results[i].bankNameEn,
					"bankNameThi": resulttable.Results[i].bankNameTh
				});
				TOTAL_NON_TMB_AC++;
				}
			}
			}
			if (rawDataTMB.length > 0) {
				frmIBMyAccnts.lblTmbHeader.isVisible = true
				frmIBMyAccnts.lblTmbHeader.text = kony.i18n.getLocalizedString("keylblTMBBank");
				rawDataTMB = sortByKeyIB(rawDataTMB, 'SortId');
				frmIBMyAccnts.segTMBAccntsList.setData(rawDataTMB);
				frmIBAddMyAccnt.segTMBAccntListFAA.setData(rawDataTMB);
				frmIBAddMyAccnt.hbox44557018542145.isVisible = true;
				frmIBMyAccnts.scrollbox44557018542146.isVisible = true;
				frmIBAddMyAccnt.lblTmbHeader.isVisible = true;
			} else {
				frmIBMyAccnts.scrollbox44557018542146.isVisible = false;
				frmIBAddMyAccnt.hbox44557018542145.isVisible = false;
				frmIBMyAccnts.lblTmbHeader.isVisible = false;
				frmIBAddMyAccnt.lblTmbHeader.isVisible = false;
			}
			if (rawDataOther.length > 0) {
				frmIBMyAccnts.lblOtherBankHdr.isVisible = true;
 				frmIBMyAccnts.lblOtherBankHdr.text = kony.i18n.getLocalizedString("keylblOtherBankAccount");

				frmIBMyAccnts.segOtherBankAccntsList.setData(rawDataOther);
				frmIBAddMyAccnt.segOtherBListFAA.setData(rawDataOther);
				frmIBAddMyAccnt.hbox44557018542299.isVisible = true;
				frmIBMyAccnts.scrollbox44557018542300.isVisible = true;
				frmIBMyAccnts.lblOtherBankHdr.isVisible = true;
				frmIBAddMyAccnt.lblAddOtherBankHdr.isVisible = true;
			} else {
				
				frmIBMyAccnts.scrollbox44557018542300.isVisible = false;
				frmIBAddMyAccnt.hbox44557018542299.isVisible = false;
 				frmIBMyAccnts.lblOtherBankHdr.isVisible = false;
				frmIBAddMyAccnt.lblAddOtherBankHdr.isVisible = false;
			}
			if (rawDataTMB.length == 0 && rawDataOther.length == 0) {
				dismissLoadingScreenPopup()
				
   				frmIBMyAccnts.lblOtherBankHdr.setVisibility(true);
				frmIBMyAccnts.lblOtherBankHdr.text=kony.i18n.getLocalizedString("keyAllAccntHidden");
 			} 
			dismissLoadingScreenPopup();
			if(gblEditClickFromAccountDetails){
				//Simulating My Accounts Click for TMB Accounts
				var selectedAccountIndex=gblIndex; // figuring out the index based on clicked data.
				for(var i=0;i<frmIBMyAccnts.segTMBAccntsList.data.length;i++){
					if(gblAccountTable["custAcctRec"][gblIndex]["accId"] == frmIBMyAccnts.segTMBAccntsList.data[i].personalizedAcctId){
						selectedAccountIndex=i;
						break;
					}
				}	
				frmIBMyAccnts.segTMBAccntsList.selectedIndex=[0,selectedAccountIndex];
				frmIBMyAccnts.segTMBAccntsList.selectedItems = frmIBMyAccnts.segTMBAccntsList.data[selectedAccountIndex];
				onClickSegmentMyAccountsTMBIB();
			}
 }
 
 function syncIBMyAccounts(){
 	 if(kony.application.getCurrentForm().id == "frmIBChequeServiceReturnedChequeLanding"){
	 	  if(kony.i18n.getCurrentLocale() == "th_TH"){
	 	    frmIBChequeServiceReturnedChequeLanding.lblFromAccount.text = returnChequegblNameTh;
		  }else{
	 	   frmIBChequeServiceReturnedChequeLanding.lblFromAccount.text = returnChequegblNameEn;
		  }
 	 }
 if(kony.application.getCurrentForm().id == "frmIBMyAccnts"){
		frmIBMyAccnts.lblTmbHeader.text = kony.i18n.getLocalizedString("keylblTMBBank");
 		frmIBMyAccnts.lblOtherBankHdr.text = kony.i18n.getLocalizedString("keylblOtherBankAccount");
 		frmIBMyAccnts.lblViewAccntName.text=kony.i18n.getLocalizedString("AccountName");
 		frmIBMyAccnts.lblViewAccntNum.text=kony.i18n.getLocalizedString("keyAccountNoIB");
 		frmIBMyAccnts.lblAccntNum.text = kony.i18n.getLocalizedString("AccountNumber")+":";
 		frmIBMyAccnts.lblNickName.text = kony.i18n.getLocalizedString("keyAddAccountNickname");
 		frmIBMyAccnts.btnBackAccntDetails.text=kony.i18n.getLocalizedString("Back");
 		myAccountListServiceIB(); 		
 		}
 		else if(kony.application.getCurrentForm().id == "frmIBAddMyAccnt"){
 		frmIBAddMyAccnt.label474165825167354.text=kony.i18n.getLocalizedString("keyotpmsgreq")
 //		frmIBAddMyAccnt.txtOTP.placeholder=kony.i18n.getLocalizedString("enterOTP");
 //		frmIBAddMyAccnt.txtToken.placeholder=kony.i18n.getLocalizedString("enterToken");
 		myAccountListServiceIB(); 
 		for (var i=0;i<gblMyAccntAddTmpData.length;i++){
 			gblMyAccntAddTmpData[i].lblNN=kony.i18n.getLocalizedString("keyAddAccountNickname");
 			gblMyAccntAddTmpData[i].lblAccntNum=kony.i18n.getLocalizedString("keyAccountNoIB");
 			if(frmIBMyAccnts.hbxCreditCardNum.isVisible){
 			gblMyAccntAddTmpData[i].lblAccntNum=kony.i18n.getLocalizedString("keyCardNumberIB") + ":";
 			}
 			gblMyAccntAddTmpData[i].lblAccntName= kony.i18n.getLocalizedString("AccountName")
 			gblMyAccntAddTmpData[i].lblBankName=getBankNameCurrentLocale(gblMyAccntAddTmpData[i].bankCD)
 		
 		}
 		frmIBAddMyAccnt.segAccntDetails.setData(gblMyAccntAddTmpData);
 		frmIBAddMyAccnt.segCmplete.setData(gblMyAccntAddTmpData);
 		frmIBAddMyAccnt.segCnfrmtnStep2.setData(gblMyAccntAddTmpData);
 	   }
 	 }
function getBankNameCurrentLocale(bankCD) {
	
	if (bankListArray != undefined && bankListArray != null && bankListArray.length > 0) {
		
		var locale = kony.i18n.getCurrentLocale();
		for (var j = 0; j < bankListArray.length; j++) {
			if (bankCD == bankListArray[j][0]) {
				if (kony.string.startsWith(locale, "en", true) == true)
					return bankListArray[j][3];
				else
					return bankListArray[j][4];
			}
		}
	}
}

function onclickDeleteMyaccountFlow(){

	if( varFMA == true && varFAMA == false){
		popupDeleAccnt.dismiss();
		if(kony.application.getCurrentForm().id == "frmIBDreamSavingMaintenance")
			myAccountDreamDeleteIB()
		else
			myAccountEditServiceIBNew("delete")
	}else{
		if(gblMyAccntAddTmpData.length > 1)
		{
			var index = frmIBAddMyAccnt.segAccntDetails.selectedIndex[1];
			frmIBAddMyAccnt.segAccntDetails.removeAt(index, 0);
			if(index == 1){
				frmIBAddMyAccnt.line44747680925127.setVisibility(false);
			}
			popupDeleAccnt.dismiss();
		}	
		else{
			frmIBMyAccnts.hbxTMBImg.setVisibility(true);
			frmIBMyAccnts.hboxAddNewAccnt.setVisibility(false);
			frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
			frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
			frmIBMyAccnts.lineComboAccntype.setVisibility(false);
			frmIBMyAccnts.show();
			frmIBAddMyAccnt.segAccntDetails.removeAt(frmIBAddMyAccnt.segAccntDetails.selectedIndex[1], 0)
		}
	}
}

function srvTokenSwitchingDummyMyAccounts() {
    var inputParam = [];
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, srvTokenSwitchingDummyCallBackAddMyA);
}

function srvTokenSwitchingDummyCallBackAddMyA(status, callbackResponse) {
	dismissLoadingScreenPopup();
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
        	onAddMyAcctNextIB();
        }
	}
}


function onClickSegmentMyAccountsTMBIB(){
	//frmIBMyAccnts.arrwImgSeg2.setVisibility(false);
	//frmIBMyAccnts.arrwImgSeg1.setVisibility(true);

	//frmIBMyAccnts.vbox447417227428995.skin="vbxRightColumn100px";
	kony.print("frmIBMyAccnts.segTMBAccntsList.selectedIndex[1]-->: "+frmIBMyAccnts.segTMBAccntsList.selectedIndex)
	kony.print("frmIBMyAccnts.segTMBAccntsList.selectedIndex[1]-->: "+frmIBMyAccnts.segTMBAccntsList.selectedIndex[1])
	kony.print("frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAccountNo -->: "+frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAccountNo)


	gblsegID = frmIBMyAccnts.segTMBAccntsList.id;
	kony.print("----gblsegID------ "+gblsegID);


	gblEditTMBpersonalizedAcctId = ""
	gblEditTMBpersonalizedId = ""
	gblEditTMBbankCD = ""


	gblEditTMBhiddenAcctName = ""
	gblEditTMBpersonalizedAcctId1 = ""
	gblTMBBankCd = ""
	
	viewAccountTMBIB(frmIBMyAccnts.segTMBAccntsList.selectedIndex[1], frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAccountNo, "segTMBAccntsList")
}

function onClickBackFromEditMyAccountsIB(){
	if(gblEditClickFromAccountDetails){
		onClickCancelPointRedemptionIB();
	}else{
		var currentForm = kony.application.getCurrentForm();
		gblMenuSelection=1;
		segConvenientServicesLoad();
		currentForm.segMenuOptions.selectedIndex=[0,1];
		menuSegmentEnabling();
	}
}


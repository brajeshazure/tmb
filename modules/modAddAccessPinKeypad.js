function addAccessPinKeypad(frmName) {
  var flxForms = ["frmQRSuccess","frmLetterConsent","frmMBManageDebitCardConfirmation","frmMBManageCardLimit","frmApplicationCfrm","frmFPSetting","frmMBBlockCardCCDBConfirm","frmMBSavingsCareConfirmation","frmMBChangePINEnterExistsPin","frmCardlesswithdrawConfirmation","frmMFConfirmMB","frmMFSwitchConfirmMB","frmMBBlockDebitCardConfirm","frmCardActivationDetails","frmBillPaymentEditFutureNew","frmMBFTEdit","frmMBBlockCardRecommendation","frmMFcancelOrderToProcess","frmeditMyProfiles","frmEKYCIdpRequestDetailsAndComplete","frmMBeKYCFaceRecg"];
 
  if(frmName != undefined && frmName != null){
    if (frmName.flexKeyBoard == null || frmName.flexKeyBoard == undefined) {
        var layType = frmName.layoutType + "";
        var topVal = "100%"
        var topValTrasFlex = "-100%"
        var frmIndex = flxForms.indexOf(frmName.id);
      
        //if (frmName.id == "frmQRSuccess" || frmName.id == "frmLetterConsent" || frmName.id == "frmMBManageDebitCardConfirmation" ||frmName.id == "frmApplicationCfrm" || frmName.id =="frmFPSetting" || frmName.id == "frmMBBlockCardCCDBConfirm" || frmName.id == "frmMBSavingsCareConfirmation" || frmName.id == "frmMBRequestNewPin") {
        if(frmIndex != -1){
            var topVal = "100%"
            var topValTrasFlex = "0%"
        } else {
            var topVal = "0%"
            var topValTrasFlex = "-100%"
        }
         var forgotPinTextValue = kony.i18n.getLocalizedString("lblForgotPin");
         if(isSignedUser){
     		 forgotPinTextValue = "";
   			 }
        var flexKeyBoard = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "flexWhiteBG",
            "height": "60%",
            "id": "flexKeyBoard",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "flexWhiteBG",
            "top": topVal,
            "width": "100%",
            "zIndex": 20
        }, {}, {});
        flexKeyBoard.setDefaultUnit(kony.flex.DP);
        var flexLineTop = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flexLineTop",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "flexLightGreyGradient",
            "top": "1px",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flexLineTop.setDefaultUnit(kony.flex.DP);
        var CopyflexKeyboard0d61321914e8b40 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "CopyflexKeyboard0d61321914e8b40",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "skin": "flexWhite",
            "top": "3%",
            "width": "100%",
            "zIndex": 5
        }, {}, {});
        CopyflexKeyboard0d61321914e8b40.setDefaultUnit(kony.flex.DP);
        var FlexContainer0bb162cbd553640 = new kony.ui.FlexContainer({
            "clipBounds": true,
            "height": "8%",
            "id": "FlexContainer0bb162cbd553640",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "slFbox",
            "top": "9%",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        FlexContainer0bb162cbd553640.setDefaultUnit(kony.flex.DP);
        var lblEnterPin = new kony.ui.Label({
            "centerX": "50%",
            "height": "100%",
            "id": "lblEnterPin",
            "isVisible": true,
            "skin": "lblBlackMed150New",
            "text": kony.i18n.getLocalizedString("enterpin"),
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        FlexContainer0bb162cbd553640.add(lblEnterPin);
        var FlexContainer0f3fcc80cac7849 = new kony.ui.FlexContainer({
            "clipBounds": true,
            "height": "6%",
            "id": "FlexContainer0f3fcc80cac7849",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "skin": "slFbox",
            "top": "2%",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        FlexContainer0f3fcc80cac7849.setDefaultUnit(kony.flex.DP);
        var FlexContainer08c65958e957143 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "80%",
            "id": "FlexContainer08c65958e957143",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "skin": "slFbox",
            "width": "60%",
            "zIndex": 1
        }, {}, {});
        FlexContainer08c65958e957143.setDefaultUnit(kony.flex.DP);
        var imgone = new kony.ui.Image2({
            "height": "100%",
            "id": "imgone",
            "isVisible": true,
            "left": "2%",
            "skin": "slImage",
            "src": "key_default.png",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgTwo = new kony.ui.Image2({
            "height": "100%",
            "id": "imgTwo",
            "isVisible": true,
            "left": "-5%",
            "skin": "slImage",
            "src": "key_default.png",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgThree = new kony.ui.Image2({
            "height": "100%",
            "id": "imgThree",
            "isVisible": true,
            "left": "-5%",
            "skin": "slImage",
            "src": "key_default.png",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgFour = new kony.ui.Image2({
            "height": "100%",
            "id": "imgFour",
            "isVisible": true,
            "left": "-5%",
            "skin": "slImage",
            "src": "key_default.png",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgFive = new kony.ui.Image2({
            "height": "100%",
            "id": "imgFive",
            "isVisible": true,
            "left": "-5%",
            "skin": "slImage",
            "src": "key_default.png",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgSix = new kony.ui.Image2({
            "height": "100%",
            "id": "imgSix",
            "isVisible": true,
            "left": "-5%",
            "skin": "slImage",
            "src": "key_default.png",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        FlexContainer08c65958e957143.add(imgone, imgTwo, imgThree, imgFour, imgFive, imgSix);
        FlexContainer0f3fcc80cac7849.add(FlexContainer08c65958e957143);
        var CopyFlexContainer0iec7bd37207e41 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10%",
            "id": "CopyFlexContainer0iec7bd37207e41",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "slFbox",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer0iec7bd37207e41.setDefaultUnit(kony.flex.DP);
        var lblForgotPin = new kony.ui.Label({
            "centerX": "50%",
            "height": "100%",
            "id": "lblForgotPin",
            "isVisible": true,
            "skin": "lblBlueDarkBlueUnderline",
            "text": forgotPinTextValue,
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onTouchEnd": onClickForgotPin
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "textCopyable": false
        });
        CopyFlexContainer0iec7bd37207e41.add(lblForgotPin);
        var flexButtons = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60%",
            "id": "flexButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "skin": "slFbox",
            "top": "7%",
            "width": "100%",
            "zIndex": 15
        }, {}, {});
        flexButtons.setDefaultUnit(kony.flex.DP);
        var flexLine = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flexLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "flexLinedarkBlack",
            "top": "1px",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flexLine.setDefaultUnit(kony.flex.DP);
        flexLine.add();
        var CopyFlexContainer02fe6373c10e548 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22%",
            "id": "CopyFlexContainer02fe6373c10e548",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "skin": "flexWhite",
            "top": "0%",
            "width": "100.04%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer02fe6373c10e548.setDefaultUnit(kony.flex.DP);
        var CopyFlexContainer0bddc389f3e0d42 = new kony.ui.FlexContainer({
            "centerX": "50%",
            "clipBounds": true,
            "height": "95%",
            "id": "CopyFlexContainer0bddc389f3e0d42",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer0bddc389f3e0d42.setDefaultUnit(kony.flex.DP);
        var btnOne = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnOne",
            "isVisible": true,
            "left": "0%",
            "onClick": btnOneApprpval,
            "skin": "btnOneAprrove",
            "text": "1",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnTwo = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnTwo",
            "isVisible": true,
            "left": "0%",
            "onClick": btnTwoApproval,
            "skin": "btnOneAprrove",
            "text": "2",
            "top": "0dp",
            "width": "34%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnThree = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnThree",
            "isVisible": true,
            "left": "0%",
            "onClick": btnThreeApproval,
            "skin": "btnOneAprrove",
            "text": "3",
            "top": "0dp",
            "width": "33%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        CopyFlexContainer0bddc389f3e0d42.add(btnOne, btnTwo, btnThree);
        CopyFlexContainer02fe6373c10e548.add(CopyFlexContainer0bddc389f3e0d42);
        var CopyflexLine0c5fb14ca70984a = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "CopyflexLine0c5fb14ca70984a",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "flexLinedarkBlack",
            "top": "0px",
            "width": "100%",
            "zIndex": 10
        }, {}, {});
        CopyflexLine0c5fb14ca70984a.setDefaultUnit(kony.flex.DP);
        CopyflexLine0c5fb14ca70984a.add();
        var CopyFlexContainer054e6f588d77341 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22%",
            "id": "CopyFlexContainer054e6f588d77341",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "skin": "flexWhite",
            "top": "1%",
            "width": "100.04%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer054e6f588d77341.setDefaultUnit(kony.flex.DP);
        var CopyFlexContainer063256fd1562a46 = new kony.ui.FlexContainer({
            "centerX": "50%",
            "clipBounds": true,
            "height": "95%",
            "id": "CopyFlexContainer063256fd1562a46",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer063256fd1562a46.setDefaultUnit(kony.flex.DP);
        var btnFour = new kony.ui.Button({
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnFour",
            "isVisible": true,
            "left": "0.00%",
            "onClick": btnFourApproval,
            "skin": "btnOneAprrove",
            "text": "4",
            "top": "1dp",
            "width": "33%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnFive = new kony.ui.Button({
            "centerY": "57.14%",
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnFive",
            "isVisible": true,
            "left": "0.00%",
            "onClick": btnFiveApproval,
            "skin": "btnOneAprrove",
            "text": "5",
            "top": "0dp",
            "width": "34%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnSix = new kony.ui.Button({
            "centerY": "53.08%",
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnSix",
            "isVisible": true,
            "left": "0.00%",
            "onClick": btnSixApproval,
            "skin": "btnOneAprrove",
            "text": "6",
            "top": "0dp",
            "width": "33%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        CopyFlexContainer063256fd1562a46.add(btnFour, btnFive, btnSix);
        CopyFlexContainer054e6f588d77341.add(CopyFlexContainer063256fd1562a46);
        var flexLine2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flexLine2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "flexLinedarkBlack",
            "top": "0px",
            "width": "100%",
            "zIndex": 10
        }, {}, {});
        flexLine2.setDefaultUnit(kony.flex.DP);
        flexLine2.add();
        var CopyFlexContainer0b6c11cf9dc424a = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22%",
            "id": "CopyFlexContainer0b6c11cf9dc424a",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "skin": "flexWhite",
            "top": "2%",
            "width": "100.04%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer0b6c11cf9dc424a.setDefaultUnit(kony.flex.DP);
        var CopyFlexContainer0e661061f71994f = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "CopyFlexContainer0e661061f71994f",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer0e661061f71994f.setDefaultUnit(kony.flex.DP);
        var btnSeven = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnSeven",
            "isVisible": true,
            "left": "0%",
            "onClick": btnSevenApproval,
            "skin": "btnOneAprrove",
            "text": "7",
            "top": "0dp",
            "width": "33%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnEight = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnEight",
            "isVisible": true,
            "left": "0%",
            "onClick": btnEightApproval,
            "skin": "btnOneAprrove",
            "text": "8",
            "top": "0dp",
            "width": "34%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnNine = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "btnOneAprrove",
            "height": "100%",
            "id": "btnNine",
            "isVisible": true,
            "left": "0%",
            "onClick": btnNiteApproval,
            "skin": "btnOneAprrove",
            "text": "9",
            "top": "0dp",
            "width": "33%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        CopyFlexContainer0e661061f71994f.add(btnSeven, btnEight, btnNine);
        CopyFlexContainer0b6c11cf9dc424a.add(CopyFlexContainer0e661061f71994f);
        var CopyflexLine0b03d99ea14ee40 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "CopyflexLine0b03d99ea14ee40",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "skin": "flexLinedarkBlack",
            "top": "0px",
            "width": "100%",
            "zIndex": 10
        }, {}, {});
        CopyflexLine0b03d99ea14ee40.setDefaultUnit(kony.flex.DP);
        CopyflexLine0b03d99ea14ee40.add();
        var CopyFlexContainer082c6ea4f9c2340 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22%",
            "id": "CopyFlexContainer082c6ea4f9c2340",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "skin": "flexWhite",
            "top": "0px",
            "width": "100.04%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer082c6ea4f9c2340.setDefaultUnit(kony.flex.DP);
        var CopyFlexContainer096ba943765d344 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "CopyFlexContainer096ba943765d344",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        CopyFlexContainer096ba943765d344.setDefaultUnit(kony.flex.DP);
        var btnClose = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "btnAccessPinKeyDel",
            "height": "100%",
            "id": "btnClose",
            "isVisible": true,
            "left": "0.00%",
            "skin": "btnAccessPinKeyDel",
            "text": kony.i18n.getLocalizedString("CAV05_btnCancel"),
            "top": "0dp",
            "width": "33%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnZero = new kony.ui.Button({
            "centerY": "50.00%",
            "focusSkin": "btnOneAprrove",
            "height": "92%",
            "id": "btnZero",
            "isVisible": true,
            "left": "0.00%",
            "onClick": btnZeroApproval,
            "skin": "btnOneAprrove",
            "text": "0",
            "top": "0dp",
            "width": "34%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnDel = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "btnAccessPinKeyDel",
            "height": "100%",
            "id": "btnDel",
            "isVisible": true,
            "left": "-0.6%",
            "skin": "btnAccessPinKeyDel",
            "text": kony.i18n.getLocalizedString("keyDeletebenefit"),
            "top": "0dp",
            "width": "34%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        CopyFlexContainer096ba943765d344.add(btnClose, btnZero, btnDel);
        CopyFlexContainer082c6ea4f9c2340.add(CopyFlexContainer096ba943765d344);
        flexButtons.add(flexLine, CopyFlexContainer02fe6373c10e548, CopyflexLine0c5fb14ca70984a, CopyFlexContainer054e6f588d77341, flexLine2, CopyFlexContainer0b6c11cf9dc424a, CopyflexLine0b03d99ea14ee40, CopyFlexContainer082c6ea4f9c2340);
        CopyflexKeyboard0d61321914e8b40.add(FlexContainer0bb162cbd553640, FlexContainer0f3fcc80cac7849, CopyFlexContainer0iec7bd37207e41, flexButtons);
        var flexVertical = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55%",
            "id": "flexVertical",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "33%",
            "skin": "flexLinedarkBlack",
            "top": "45%",
            "width": "1px",
            "zIndex": 10
        }, {}, {});
        flexVertical.setDefaultUnit(kony.flex.DP);
        flexVertical.add();
        var CopyflexVertical0if62936317344b = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55%",
            "id": "CopyflexVertical0if62936317344b",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "66.50%",
            "skin": "flexLinedarkBlack",
            "top": "45%",
            "width": "1px",
            "zIndex": 15
        }, {}, {});
        CopyflexVertical0if62936317344b.setDefaultUnit(kony.flex.DP);
        CopyflexVertical0if62936317344b.add();
        flexKeyBoard.add(flexLineTop, CopyflexKeyboard0d61321914e8b40, flexVertical, CopyflexVertical0if62936317344b);
        frmName.add(flexKeyBoard);
        var flexTransparent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flexTransparent",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
          	"onClick": doNothing,
            "skin": "flexWhite50PerOpacity",
            "top": topValTrasFlex,
            "width": "100%",
            "zIndex": 10
        }, {}, {});
        flexTransparent.setDefaultUnit(kony.flex.DP);
        flexTransparent.add();
        frmName.add(flexTransparent);
        frmName.btnClose.onClick = closeApprovalKeypad;
        frmName.btnDel.onClick = onClickPinBackApproval;
    }
    }
}

function closeApprovalKeypad() {
    var frmName = kony.application.getCurrentForm();
    if(frmName.id == "frmMBeKYCFaceRecg"){ // This is because if the form have footers.
      frmMBeKYCFaceRecg.footers[0].isVisible = true;
    }
    
    //var layType = frmQRSuccess.layoutType + "";
   var flxForms = ["frmQRSuccess","frmLetterConsent","frmMBManageDebitCardConfirmation","frmMBManageCardLimit","frmApplicationCfrm","frmFPSetting","frmMBBlockCardCCDBConfirm","frmMBSavingsCareConfirmation","frmMBChangePINEnterExistsPin","frmCardlesswithdrawConfirmation","frmMFConfirmMB","frmMFSwitchConfirmMB","frmMBBlockDebitCardConfirm","frmCardActivationDetails","frmBillPaymentEditFutureNew","frmMBFTEdit","frmMBBlockCardRecommendation","frmMFcancelOrderToProcess","frmeditMyProfiles","frmEKYCIdpRequestDetailsAndComplete","frmMBeKYCFaceRecg"];
   var frmIndex = flxForms.indexOf(frmName.id);
  
    //if (frmName.id == "frmEDonationPaymentConfirm" || frmName.id == "frmQRSuccess" || frmName.id == "frmLetterConsent" || frmName.id == "frmMBManageDebitCardConfirmation" ||frmName.id == "frmApplicationCfrm" || frmName.id == "frmFPSetting" || frmName.id == "frmMBBlockCardCCDBConfirm" || frmName.id == "frmMBSavingsCareConfirmation" || frmName.id == "frmMBRequestNewPin") {
     if(frmIndex != -1){
        
          var topVal = "60%"
        
    } else {
      if(frmName.id == "frmMBCCUsageLimit"){
        
        var topValCard = "0%";
         frmMBCCUsageLimit.flxBody.animate(kony.ui.createAnimation({
        "100": {
            "top": topValCard,
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    });
        
//         frmMBCCUsageLimit.flxBody.top = "0%"
      }
        var topVal = "0%"
    }
  
    frmName.flexTransparent.isVisible = false;
    frmName.flexKeyBoard.animate(kony.ui.createAnimation({
        "100": {
            "top": topVal,
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    });
    frmName.flexKeyBoard.isVisible = false;    
    resetKeypadApproval();
    if (frmName.id == "frmFPSetting"){
        setEnableDisableTouchLogin();
    } 
}

function onClickApprovalKeyPad(eventobject) {
    onClickOfKeypadApproval();
    getPinApproval(eventobject);
}
gblPinCountApproval = 0

function onClickOfKeypadApproval() {
    var frmName = kony.application.getCurrentForm();
    if (gblPinCountApproval < 6) {
        gblPinCountApproval++;
    }
    switch (gblPinCountApproval) {
        case 1:
            frmName.imgone.src = "key_sel.png";
            break;
        case 2:
            frmName.imgTwo.src = "key_sel.png";
            break;
        case 3:
            frmName.imgThree.src = "key_sel.png";
            break;
        case 4:
            frmName.imgFour.src = "key_sel.png";
            break;
        case 5:
            frmName.imgFive.src = "key_sel.png";
            break;
        case 6:
            frmName.imgSix.src = "key_sel.png";
            break;
    }
}
gblNumApproval = ""

function getPinApproval(obj) {
    var temp;
    var caseVar = obj.id;
    switch (caseVar) {
        case "btnOne":
            temp = "1";
            break;
        case "btnTwo":
            temp = "2";
            break;
        case "btnThree":
            temp = "3";
            break;
        case "btnFour":
            temp = "4";
            break;
        case "btnFive":
            temp = "5";
            break;
        case "btnSix":
            temp = "6";
            break;
        case "btnSeven":
            temp = "7";
            break;
        case "btnEight":
            temp = "8";
            break;
        case "btnNine":
            temp = "9";
            break;
        case "btnZero":
            temp = "0";
            break;
    }
    //if (gblNumApproval.length < 6) {
        gblNumApproval = gblNumApproval + temp;
    //}
  
      gblNumApproval = gblNumApproval.trim();
   
     if (gblNumApproval.length > 6)
       return false;

    
    if (gblNumApproval.length == 6) {
        var frmName = kony.application.getCurrentForm();
        gblNum = gblNumApproval.trim();
        if (frmName.id == "frmUVApprovalMB") {
            invokeAccessPinVerification(gblNumApproval.trim());
        } else if (frmName.id == "frmQRSuccess") {
            if (gblqrflow == "preLogin") {
                invokeAccesspinVerificationQR(gblNumApproval.trim());
            } else {
                frmQRSuccessOnClickPay();
            }
        } else if (frmName.id == "frmEDonationPaymentConfirm") {
            if (gblqrflow == "preLogin") {
                invokeAccesspinVerificationQR(gblNumApproval.trim());
            } else {
                frmQRSuccessOnClickPay();
            }
        } else if (frmName.id == "frmQRSelectAccount") {
            //invokeAccesspinVerificationQR(gblNumApproval.trim());
            if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                callGenerateTransferRefNoserviceqrBillPay();
              }else{
                verifyTransferQRPay(gblNumApproval.trim());
              }
        }else if (frmName.id == "frmLetterConsent") {
                verifyLetterofConsent(gblNumApproval.trim());
        }else if (frmName.id == "frmMBManageDebitCardConfirmation") {
               invokeDebitcardCahngeApproval(gblNumApproval.trim());    
        }else if (frmName.id == "frmMBCCUsageLimit") {
               navigateToCCUsageLimtComplate(gblNumApproval.trim());    
        }else if (frmName.id == "frmApplicationCfrm") {
               invokeLoanConfirmation(gblNumApproval.trim());    
        }else if(frmName.id == "frmFPSetting"){
           //gblNum = gblNumApproval.trim();
          onConfirmGetDeviceID();
        }else if(frmName.id == "frmMBBlockCardCCDBConfirm" || frmName.id == "frmMBBlockDebitCardConfirm" || frmName.id == "frmMBBlockCardRecommendation"){
          //gblNum = gblNumApproval.trim();
          onClickBlockCardConfirmPop(gblNum);
        }else if(frmName.id == "frmMBSavingsCareConfirmation"){
          //gblNum = gblNumApproval.trim();
          onClickConfirmPopDS();
        }else if(frmName.id == "frmMBRequestNewPin"){
           //gblNum = gblNumApproval.trim();
           onClickRequestPINConfirmTxnPopup(gblNum);
        }else if(frmName.id == "frmMBChangePINEnterExistsPin"){
           onClickConfirmChangePINTransPop(gblNum);
        }else if(frmName.id == "frmCardlesswithdrawConfirmation"){
          verifyPwdForRequestAccessCodeConfimation(gblNum);
        }else if(frmName.id == "frmMFConfirmMB"){
          MBconfirmMFOrderCompositeJavaService(gblNum);
        }else if(frmName.id == "frmMFSwitchConfirmMB"){
          MBconfirmSwitchMFOrderCompositeJavaService(gblNum);
        }else if(frmName.id == "frmCAPaymentPlanConfirmation") {
          onClickCashAdvaceConfirmPop(gblNum);
        }else if(frmName.id == "frmCardActivationDetails"){
          verifyPwdForDebitCreditCardActivation(gblNum);
        }else if(frmName.id == "frmBillPaymentEditFutureNew"){
          checkEditBillPayVerifyPWDMB(gblNum);
        }else if(frmName.id == "frmMBFTEdit"){
           srvVerifyPasswordExFT_CS_MB(gblNum);
        }else if(frmName.id == "frmMFcancelOrderToProcess"){
           callCancelConfirmCompositeService();
        }else if(frmName.id == "frmeditMyProfiles"){
          verifyPWDMyAddressMB();
        } else if(frmName.id == "frmSend2SaveCreateSmartRequest"){
          saveAlertCompositeServiceDetails(gblNumApproval.trim());
        }else if(frmName.id == "frmMBSetEmail"){
          verifyAccessForEmailChange(gblNum);
          
        }else if(frmName.id == "frmEKYCIdpRequestDetailsAndComplete" || frmName.id == "frmMBeKYCFaceRecg"){
          verifyAccessForIdpRequest(gblNum);
          
        }
    }
}

function onClickPinBackApproval() {
    var frmName = kony.application.getCurrentForm();
    if (gblPinCountApproval > 0) {
        gblPinCountApproval--;
        var pinLen = gblNumApproval;
        var updatedPin = gblNumApproval.substring(0, pinLen.length - 1)
        gblNumApproval = updatedPin;
    }
    switch (gblPinCountApproval) {
        case 0:
            frmName.imgone.src = "key_default.png";
            break;
        case 1:
            frmName.imgTwo.src = "key_default.png";
            break;
        case 2:
            frmName.imgThree.src = "key_default.png";
            break;
        case 3:
            frmName.imgFour.src = "key_default.png";
            break;
        case 4:
            frmName.imgFive.src = "key_default.png";
            break;
        case 5:
            frmName.imgSix.src = "key_default.png";
            break;
    }
}

function resetKeypadApproval() {
    var frmName = kony.application.getCurrentForm();
    gblPinCountApproval = 0;
    gblNumApproval = "";
    frmName.imgone.src = "key_default.png";
    frmName.imgTwo.src = "key_default.png";
    frmName.imgThree.src = "key_default.png";
    frmName.imgFour.src = "key_default.png";
    frmName.imgFive.src = "key_default.png";
    frmName.imgSix.src = "key_default.png";
}
function onClickForgetPinAccessPin(){
  closeApprovalKeypad();
  onClickForgotPin();
}
function showAccessPinScreenKeypad() {
    gblPinCountApproval = 0;
    gblNumApproval = "";
    var frmName = kony.application.getCurrentForm();
     var flxForms = ["frmQRSuccess","frmLetterConsent","frmMBManageDebitCardConfirmation","frmMBManageCardLimit","frmApplicationCfrm","frmFPSetting","frmMBBlockCardCCDBConfirm","frmMBSavingsCareConfirmation","frmMBChangePINEnterExistsPin","frmCardlesswithdrawConfirmation","frmMFConfirmMB","frmMFSwitchConfirmMB","frmMBBlockDebitCardConfirm","frmCardActivationDetails","frmBillPaymentEditFutureNew","frmMBFTEdit","frmMBBlockCardRecommendation","frmMFcancelOrderToProcess","frmeditMyProfiles","frmEKYCIdpRequestDetailsAndComplete","frmMBeKYCFaceRecg"];
    var frmIndex = flxForms.indexOf(frmName.id);
    
    if(frmName.flexKeyBoard.isVisible == false){
      frmName.flexKeyBoard.isVisible = true;
    //if ( frmName.id == "frmQRSuccess" || frmName.id == "frmLetterConsent" || frmName.id =="frmMBManageDebitCardConfirmation" || frmName.id === "frmMBManageCardLimit" || frmName.id == "frmApplicationCfrm" || frmName.id == "frmFPSetting" || frmName.id == "frmMBBlockCardCCDBConfirm" || frmName.id == "frmMBSavingsCareConfirmation" || frmName.id == "frmMBRequestNewPin") {
      if(frmIndex != -1){
        var topVal = "40%"
    } else {
      if(frmName.id == "frmMBCCUsageLimit"){
        var topVal = "-20%";
        }else{
          var topVal = "-60%";
        }
    }
    frmName.flexKeyBoard.animate(kony.ui.createAnimation({
        "100": {
            "top": topVal,
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.5
    });
    frmName.flexTransparent.isVisible = true;
    if(isSignedUser){
      frmName.lblForgotPin.text = "";
    }else{
     frmName.lblForgotPin.text = kony.i18n.getLocalizedString("lblForgotPin"); 
     frmName.lblForgotPin.onTouchEnd = onClickForgetPinAccessPin;
    }
    
    frmName.lblForgotPin.skin = "lblBlueDarkBlueUnderline";
    
    frmName.lblEnterPin.text = kony.i18n.getLocalizedString("enterpin").replace(".", "");
  
    frmName.btnClose.text = kony.i18n.getLocalizedString("CAV05_btnCancel");
    frmName.btnDel.text = kony.i18n.getLocalizedString("keyDeletebenefit");  
    
       }
}
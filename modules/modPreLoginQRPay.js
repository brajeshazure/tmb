function validateQRPayment() {
  var amount = gblqrToAccntBal.replace(/,/g, "");
  var serviceAmount = parseFloat(amount);
  var inputAmount = parseFloat(gblQRPayData["transferAmt"]);
  if (inputAmount > 0.00) {
    if (inputAmount > serviceAmount) {
      if(frmQRSuccess.flexAmountlbl.isVisible){
        frmQRSuccess.flxErrorBody.setVisibility(true);
        frmQRSuccess.lblerrormsg1.text = kony.i18n.getLocalizedString("msg_QRInsufficientFund");
        frmQRSuccess.flxErrorBody.height = "83%"
        frmQRSuccess.flxErrorBody.setVisibility(true);
        frmQRSuccess.flxPayFooter.setVisibility(false);
        frmQRSuccess.flexChooseAccntFooter.setVisibility(true);
        frmQRSuccess.flxLoginFooter.setVisibility(false);
        frmQRSuccess.FlexFooter.setVisibility(true);
      } else {
        //alert(kony.i18n.getLocalizedString("msg_QRInsufficientFund"));
        showAlertWithCallBack(kony.i18n.getLocalizedString("msg_QRInsufficientFund"), kony.i18n.getLocalizedString("info"), function(){
          																														frmQRSuccess.tbxEnterAmount.setFocus(true);
        																													}); //MKI, MIB-9656
      }
    } else {
      if (isSignedUser) {
        gblqrflow="postLogin";
        gblqrTransferAmnt = gblQRPayData["transferAmt"];
        //frmQRSuccessOnClickPay(); //tran pass
        if(gblpp_isOwnAccount)// MKI, MIB-9972 start
       	{
          //write code for access pin skip
          loadMapforQrPreLoginPay();
        }
        else{
        	showAccessPinScreenKeypad();  
        } // MKI, MIB-9972 End
        
      } else {
        gblqrflow="preLogin";
        gblqrTransferAmnt = gblQRPayData["transferAmt"];
        showAccessPinScreenKeypad();
        
        //popUpAccesspinPwd.show();        
        //popUpAccesspinPwd.txtAccessPin.setFocus(true);
        //frmQRPaymentSuccess.show();
      }
    }
  } else {
    //alert(kony.i18n.getLocalizedString("msg_QRInsufficientFund")); 
    //frmQRSuccess.tbxEnterAmount.setFocus(true);
     showAlertWithCallBack(kony.i18n.getLocalizedString("msg_QRInsufficientFund"), kony.i18n.getLocalizedString("info"), function(){
          																														frmQRSuccess.tbxEnterAmount.setFocus(true);
        																													}); //MKI, MIB-9656
  }

}

function frmQRSuccessPreShow() {
  displayQRSuccessPreshow();
  var textMsg = "";
  if(gblQRPayData["custAcctName"] != undefined && gblQRPayData["custAcctName"] != ""){
    textMsg = kony.i18n.getLocalizedString("msgQRFromAccount");
    frmQRSuccess.LabelFromAccountName.text = kony.string.replace(textMsg, "<x>", gblQRPayData["custAcctName"]);
  } else {
    frmQRSuccess.LabelFromAccountName.text = "";
  }
  if(gblQRPayData["banckCode"]== "11"){
    frmQRSuccess.LabelToAccountName.text = gblqrToUsrName;
  }else{
    frmQRSuccess.LabelToAccountName.text = gblQRPayData["toAccountName"];
  }
  if(gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != ""){
    frmQRSuccess.LabelToMobileNo.text = formatMobileNumber(gblQRPayData["mobileNo"]);
  } else if(gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != ""){
    frmQRSuccess.LabelToMobileNo.text = formatCitizenID(gblQRPayData["cId"]);
  } else if(gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != ""){
    frmQRSuccess.LabelToMobileNo.text = formatEWalletNumber(gblQRPayData["eWallet"]);
  } else if(gblQRPayData["billerID"] !== undefined && gblQRPayData["billerID"] !==""){
    frmQRSuccess.LabelToAccountName.isVisible = true;
    frmQRSuccess.LabelToAccountName.text = gblQRDefaultAccountResponse["pp_toAcctName"];
  } else {
    frmQRSuccess.LabelToMobileNo.text = "";
  }

  if (gblQRPayData["fee"] != undefined && gblQRPayData["fee"] != "" && gblQRPayData["fee"] != "0.00") {
    kony.print("Inside True Fee");
    frmQRSuccess.LabelFee.setVisibility(true);
    frmQRSuccess.LabelFee.text = kony.i18n.getLocalizedString("keyFee") + gblQRPayData["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  } else {
    frmQRSuccess.LabelFee.setVisibility(false);
  }
  if (gblQRPayData["ref1"] != undefined && gblQRPayData["ref1"] != "" && gblQRPayData["ref1"] != "0.00") {
    kony.print("language"+kony.i18n.getCurrentDeviceLocale());
    if(kony.i18n.getCurrentLocale() == "th_TH") {
      kony.print("Inside Thai language");
      var lblRef1 = gblQRDefaultAccountResponse["labelRef1TH"];
      if(lblRef1 == "" || lblRef1 == undefined){
        lblRef1 = kony.i18n.getLocalizedString("Reference1");
      }
      var lblsearchRef1 = lblRef1.search(":");
      if(lblsearchRef1 == -1){
        frmQRSuccess.lblReferenceNum1.text=lblRef1+": "+gblQRPayData["ref1"];
      }else{
        frmQRSuccess.lblReferenceNum1.text=lblRef1+" "+gblQRPayData["ref1"];
      }
   }else{
     kony.print("Inside English language");
      var lblRef1 = gblQRDefaultAccountResponse["labelRef1EN"];
     kony.print("Inside english   "+lblRef1);
      if(lblRef1 == "" || lblRef1 == undefined){
        lblRef1 = kony.i18n.getLocalizedString("Reference1");
      }
     kony.print("Inside english   "+lblRef1);
      var lblsearchRef1 = lblRef1.search(":");
      if(lblsearchRef1 == -1){
        frmQRSuccess.lblReferenceNum1.text=lblRef1+": "+gblQRPayData["ref1"];
      }else{
        frmQRSuccess.lblReferenceNum1.text=lblRef1+" "+gblQRPayData["ref1"];
      }
   }
  }
  if (gblQRPayData["ref2"] != undefined && gblQRPayData["ref2"] != "" && gblQRPayData["ref2"] != "0.00" && gblQRPayData["ref2"] != "0") {
  	if(frmscanqrLanding.lblReferenceNum2.isVisible)
    {
      frmQRSuccess.lblReferenceNum2.setVisibility(true);
	if(kony.i18n.getCurrentLocale() == "th_TH") {
      if(gblQRDefaultAccountResponse["labelRef2TH"]!= "" && gblQRDefaultAccountResponse["labelRef2TH"]!= undefined){
          var lblRef2 = gblQRDefaultAccountResponse["labelRef2TH"];
          lblRef2 = lblRef2.search(":");
          if(lblRef2 == -1){
            frmQRSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+": "+gblQRPayData["ref2"];
          }else{
            frmQRSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+" "+gblQRPayData["ref2"];
          }
        }else{
           frmQRSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
        }
	}else{
      if(gblQRDefaultAccountResponse["labelRef2EN"]!= "" && gblQRDefaultAccountResponse["labelRef2EN"]!= undefined){
          var lblRef2 = gblQRDefaultAccountResponse["labelRef2EN"];
          lblRef2 = lblRef2.search(":");
          if(lblRef2 == -1){
            frmQRSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+": "+gblQRPayData["ref2"];
          }else{
            frmQRSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+" "+gblQRPayData["ref2"];
          }
        }else{
           frmQRSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
        }
  	}
      //frmQRSuccess.lblReferenceNum2.text=kony.i18n.getLocalizedString("Ref2")+" "+gblQRPayData["ref2"];
    }else{
      frmQRSuccess.lblReferenceNum2.setVisibility(false);
    }
  }else{
    frmQRSuccess.lblReferenceNum2.setVisibility(false);
 }
  frmQRSuccess.LabelOr.text = kony.i18n.getLocalizedString("keyor");
  frmQRSuccess.LabelSelectAccount.text = kony.i18n.getLocalizedString("lb_QRselectAccount");
  frmQRSuccess.lblPay.text = kony.i18n.getLocalizedString("btnPayQR");
  //frmQRSuccess.lblPay.text = kony.i18n.getLocalizedString("btnPayQR");
}

function invokeQRPaymentDefaultAccountService() {
  var inputParam = {};

  //showLoadingScreen();

  frmQRSuccess.FlexLoading.setVisibility(true);
  gblQRPaymentDeviceId = getDeviceID();
  //alert("device id"+gblQRPaymentDeviceId);
  /*
    var enCryptDeviceId = encryptData(gblQRPaymentDeviceId);
    kony.print("enCryptDeviceId :: " + enCryptDeviceId)
    inputParam["encryptDeviceId"] = enCryptDeviceId;
*/
  inputParam["encryptDeviceId"] = getDeviceID();
  if(gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != ""){
    inputParam["promptPayId"] = gblQRPayData["mobileNo"];
    inputParam["promptPayType"] = "02";
  } else if(gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
    inputParam["promptPayId"] = gblQRPayData["cId"];
    inputParam["promptPayType"] = "01";
  } else if(gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
    inputParam["promptPayId"] = gblQRPayData["eWallet"];
    inputParam["promptPayType"] = "04";
  } else if(gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
    inputParam["promptPayId"] = gblQRPayData["billerID"];
    inputParam["promptPayType"] = "05";
  } else {
    alert("Incorrect input param");
    return;
  }

  if (gblQRPayData["transferAmt"] != undefined && gblQRPayData["transferAmt"] != "") {
    inputParam["transAmount"] = gblQRPayData["transferAmt"];
  } else {
    inputParam["transAmount"] = "1";
  }
	if(gblQRPayData["QR_TransType"]== "QR_Bill_Payment"){
      inputParam["transType"] = "BILLPAY_PROMPTPAY";
      inputParam["ref1"] = gblQRPayData["ref1"];
      inputParam["ref2"] = gblQRPayData["ref2"];
      inputParam["ref3"] = gblQRPayData["ref3"];
    }else{
      inputParam["transType"] = "TRANSFER_PROMPTPAY";
    }
  gblDonationNormalFlow = false;
  invokeServiceSecureAsync("QRPaymentDefaultAccount", inputParam, callBackQRDefaultAccount);
}

function callBackQRDefaultAccount(status, resulttable) {
  try {
    
    var locale_app = kony.i18n.getCurrentLocale();
    //enableFormElements();
    var banckCode = "";
    var usrAccntNickname = "";
    var usrAcctNum = ""
    var accountNickName = "";
    var accountId = "";
    var accountNoFomatted = "";
    var availBalLabel = "";
    var availBalValue = ""
    var msgError = "";

    if (status == 400) {
      //addAccessPinKeypad(frmQRSuccess);
      
      kony.print("Inside here 123");
      if (resulttable["opstatus"] == "0" || resulttable["opstatus"] == 0) {
        
        gblQRDefaultAccountResponse = resulttable;

        if (resulttable["pp_promptPayFlag"] == "1") {
		 
          var errCode = resulttable["pp_errCode"];
          var errorText = "";

          if (errCode == 'XB240063') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PRecBankRejectAmt");
          } else if (errCode == 'XB240048') {
            kony.print("Inside here XB240048");
            msgError = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
          } else if (errCode == 'XB240066') {
             kony.print("Inside here XB240066");
            msgError = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
          } else if (errCode == 'XB240088') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PAccInActive");          
          } else if (errCode == 'XB240098') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PExceedeWal");
          } else if (errCode == 'XB240067') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PCutOffTime");
          } else if (errCode == 'XB240072') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PDestTimeout");

          } else if (errCode == 'X8899') {
            msgError = kony.i18n.getLocalizedString("MIB_P2PCloseBranchErr");
          } else if (errCode == 'X1120') {
            msgError = kony.i18n.getLocalizedString("billernotfoundTQRC");
          } else if (errCode == '8002') {
              msgError = kony.i18n.getLocalizedString("NoValidAccountsPayment");
          } else {
            var errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode + ")";
            msgError = errorText;
          }
		  showAlertWithMessage(msgError);
          return;
        } else if(resulttable["pp_destBankCode"] != undefined){
          banckCode = resulttable["pp_destBankCode"];
          gblisTMB = banckCode;
          gblqrUsrName = resulttable["pp_toAccTitle"];
		  msgError = kony.i18n.getLocalizedString("ECGenericError");
          if(!isNotBlank(gblisTMB)){
			  showAlertWithMessage(msgError);
			  return;
          }
          if(gblisTMB != gblTMBBankCD){
            gblqrUsrName = resulttable["pp_toAcctName"];
            gblqrUsrAccnt = resulttable["pp_toAcctNo"];
            gblqrTransFee = resulttable["pp_itmxfee"];
            gblqrToUsrName = resulttable["pp_toAccTitle"];
            
            gblQRPayData["banckCode"] = resulttable["pp_destBankCode"];
			
            gblQRPayData["billerCompCode"] = resulttable["qrCompCode"];
            gblQRPayData["toAccountName"] = resulttable["pp_toAccTitle"];
            gblQRPayData["toAccountName"] = resulttable["pp_toAcctName"];
            gblQRPayData["toAcctIDNoFormat"] = resulttable["pp_toAcctNo"];
            //gblQRPayData["custAcctName"] = resulttable["pp_toAccTitle"];
            if (resulttable["pp_itmxFee"] !== null || resulttable["pp_itmxFee"] !== undefined) {
              gblQRPayData["fee"] = resulttable["pp_itmxFee"];
            } else {
              gblQRPayData["fee"] = "0.00";
            }
          } else {
            gblqrUsrName = resulttable["pp_toAccTitle"];
            gblqrUsrAccnt = resulttable["pp_toAcctNo"];
            gblqrTransFee = resulttable["pp_itmxfee"];
            gblqrToUsrName = resulttable["pp_toAccTitle"];
            gblQRPayData["banckCode"] = resulttable["pp_destBankCode"];
			//commneted for bill pay
            gblQRPayData["billerCompCode"] = resulttable["qrCompCode"];
            gblQRPayData["toAccountName"] = resulttable["pp_toAccTitle"];
            gblQRPayData["toAccountName"] = resulttable["pp_toAcctName"];
            gblQRPayData["toAcctIDNoFormat"] = resulttable["pp_toAcctNo"];
            //gblQRPayData["custAcctName"] = resulttable["pp_toAccTitle"];
            if (resulttable["pp_itmxFee"] !== null || resulttable["pp_itmxFee"] !== undefined) {
              gblQRPayData["fee"] = resulttable["pp_itmxFee"];
            } else {
              gblQRPayData["fee"] = "0.00";
            }
            kony.print("Values Before checking #########"+gblQRPayData["custAcctName"]+gblQRPayData["custName"]+gblQRPayData["toAccountName"])
          }
          if (resulttable["custAcctRec"] != undefined && resulttable["custAcctRec"].length > 0) {
            kony.print("INSIDE the log custAcctRec $$$$$$$$$$$$$$");
            kony.print("Values Before checking &&&&&&&&&"+gblQRPayData["custAcctName"]+gblQRPayData["custName"]+gblQRPayData["toAccountName"])
            var collectionData = resulttable["custAcctRec"];
            //alert("Testing"+resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"]);
			gblQRPayData["accountName"]= resulttable["custAcctRec"][0]["accountName"]; //MKI, MIB-9891
            if(resulttable["custAcctRec"][0]["acctNickName"] != undefined 
               && resulttable["custAcctRec"][0]["acctNickName"] != ""){
              gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["acctNickName"];
              gblQRPayData["custName"] = resulttable["custAcctRec"][0]["acctNickName"];
             //gblQRPayData["accountName"]= resulttable["custAcctRec"][0]["accountName"]; //MKI, MIB-9891
              gblqrFromAccntNickName = resulttable["custAcctRec"][0]["acctNickName"];
            } else {
              if (locale_app == "th_TH") {
                gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
                gblQRPayData["custName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
                gblqrFromAccntNickName = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
              } else {
                gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
                gblQRPayData["custName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
                gblqrFromAccntNickName = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
              }
            }

            if ((resulttable["custAcctRec"][0]["accId"]) != null && (resulttable["custAcctRec"][0]["accId"]) != undefined) {
              accountId = resulttable["custAcctRec"][0]["accId"];
              gblqrFromAccntNum = resulttable["custAcctRec"][0]["accId"];
              gblQRPayDefaultAcct = gblqrFromAccntNum;
              glb_accId = gblQRPayDefaultAcct;
            }

            if ((resulttable["custAcctRec"][0]["accountNoFomatted"]) != null && (resulttable["custAcctRec"][0]["accountNoFomatted"]) != undefined) {
              gblqrToAccnt = resulttable["custAcctRec"][0]["accountNoFomatted"];
              gblQRPayData["fromAcctID"] = resulttable["custAcctRec"][0]["accountNoFomatted"];
            }

            if ((resulttable["custAcctRec"][0]["accType"]) == "CCA") {
              gblqrToAccntBal = resulttable["custAcctRec"][0]["availableCreditBalDisplay"];
            } else {
              gblqrToAccntBal = resulttable["custAcctRec"][0]["availableBalDisplay"];
            }
            
            if(resulttable["pp_isOwn"] == 'Y'){
              var defaultAccount = removeHyphenIB(resulttable["custAcctRec"][0]["accountNoFomatted"]);
              gblpp_isOwnAccount = true; // MKI, MIB-9972
              if (defaultAccount == resulttable["pp_toAcctNo"] && gblisTMB == gblTMBBankCD){
				showAlertWithMessage(kony.i18n.getLocalizedString("msgErrOwnAcctQR"));
				return;
              }
            }
			frmQRSuccess.flexValidQRWithAmnt.setVisibility(true);
            kony.print("Values Before checking !!$$$$$$$$$"+gblQRPayData["custAcctName"]+gblQRPayData["custName"]+gblQRPayData["toAccountName"]+" Testing:  "+gblQRPayData["toAccountName"])
            if(gblQRPayData["toAccountName"]!= "" && gblQRPayData["toAccountName"]!= undefined){
             gblQRPayData["selectAccName"] = gblQRPayData["toAccountName"];
            }else {
              gblQRPayData["selectAccName"] = "";
            }
            gblQRPayData["E_DONATION_CATEGORY"] = resulttable["E_DONATION_CATEGORY"];
            gblQRPayData["billerCategoryID"] = resulttable["billerCategoryID"];
            gblQRPayData["billerNameEN"] = resulttable["billerNameEN"];
            gblQRPayData["billerNameTH"] = resulttable["billerNameTH"];
            gblQRPayData["qrCompCode"] = resulttable["qrCompCode"];
            gblQRPayData["accountName"] = resulttable["custAcctRec"][0]["accountName"];
            gblQRPayData["acctNickName"] = resulttable["custAcctRec"][0]["acctNickName"];
            gblQRPayData["accountNoFomatted"] = resulttable["custAcctRec"][0]["accountNoFomatted"];
            gblQRPayData["availableBalDisplay"] = resulttable["custAcctRec"][0]["availableBalDisplay"];
            kony.print("QR_TransType"+gblQRPayData["QR_TransType"]);
            if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
              if(gblQRPayData["E_DONATION_CATEGORY"] == gblQRPayData["billerCategoryID"]){
                var CI_Flag = resulttable["CI_Flag"];
                var CI_Value = resulttable["CI_Value"];
                if (CI_Flag == "1") {
                	gblCustomerIDType = "CI";
                	gblCustomerIDValue = CI_Value;
                } else {
                	gblCustomerIDType = "OT";
                }              
                kony.print("Inside edonation one");
                if(gblQRPayData["ref2"] == "0.00" || gblQRPayData["ref2"] == "0" || gblQRPayData["ref2"] == "" || gblQRPayData["ref2"] == undefined){
                  kony.print("Inside edonation");
				  getFoundationDetails(gblQRPayData["qrCompCode"]);
                  showeDonationAmount();
                }else{
				  kony.print("Inside fail case edonation");
				  showAlertWithMessage(kony.i18n.getLocalizedString("eDonationQRNotCorrect"));
				  return;
                }
               }else{
                frmQRSuccess.FlexLoading.setVisibility(false);
                frmQRSuccess.FlexMain.setVisibility(true);
                frmQRSuccessPreShow();
               }               
            }else if(gblQRPayData["QR_TransType"]=="QR_Transfer"){
              frmQRSuccess.FlexLoading.setVisibility(false);
          	  frmQRSuccess.FlexMain.setVisibility(true);
              frmQRSuccessPreShow();
            }else{
              kony.print("when gbl type is undefined");
			  showAlertWithMessage(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
			  return;
            }
           
          } else {
			showAlertWithMessage(kony.i18n.getLocalizedString("keyNoEligibleAct"));
			return;
          }
        } else {
          frmQRSuccess.flexValidQRWithAmnt.setVisibility(false);
          var errCode = resulttable["pp_errCode"];
          var errorText = "";
          if (isNotBlank(errCode)) {
            if (errCode == 'X1120') {
              msgError = kony.i18n.getLocalizedString("billernotfoundTQRC");
            }else if (errCode == 'XB246072') {
              msgError = kony.i18n.getLocalizedString("ErrorNotRegBillPay");
            }else if (errCode == '8002') {
              msgError = kony.i18n.getLocalizedString("NoValidAccountsPayment");
            }else if (errCode == '8001'){
              var errorText = kony.i18n.getLocalizedString("ErrorGenBillPay");
              msgError = errorText;
          	}else if (resulttable["pp_errB24Msg"] !== null || resulttable["pp_errB24Msg"] !== undefined) {
              errorText = resulttable["pp_errB24Msg"];
              msgError = errorText;
            }else {
              msgError = kony.i18n.getLocalizedString("ECGenericError");
          	} 
          }else{
             var errorText = kony.i18n.getLocalizedString("ECGenericError");
            msgError = errorText;
          }          
		  showAlertWithMessage(msgError);
		  return;
        }
      } else if (resulttable["opstatus"] == 8005) {
        if (resulttable["errCode"] == 1002) {
          isUserStatusActive = false;
          alert(kony.i18n.getLocalizedString("VQB_MBStatus"));
          frmMBPreLoginAccessesPin.show(); //MKI, MIB-9955
        } else {
          var errMsg = resulttable["errMsg"];
          alert(errMsg);
        }
      } else if (resulttable["opstatus"] == 1011) {
		dismissLoadingScreen();
        alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
        if (resulttable["errCode"] == 1002) {
          isUserStatusActive = false;
          msgError = kony.i18n.getLocalizedString("VQB_MBStatus");
          alert(msgError);
        } else {
          alert(kony.i18n.getLocalizedString("VQB_ServiceDown"));
          msgError = resulttable["errMsg"];
          //alert(msgError);
          showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"), kony.i18n.getLocalizedString("deviceIDUnlock"));
        }
      } else if (resulttable["opstatus"] == 1011) {
		dismissLoadingScreen();
        msgError = kony.i18n.getLocalizedString("genErrorWifiOff");
        alert(msgError);
      } else {
		dismissLoadingScreen();
        msgError = kony.i18n.getLocalizedString("VQB_ServiceDown");
        //alert(msgError);
        showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"), kony.i18n.getLocalizedString("deviceIDUnlock"));
      }

    } else {
	  dismissLoadingScreen();
      alert("error");
    } 
  } catch (e) {
    dismissLoadingScreen();
    msgError = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
    alert(msgError);
  }
}

function showAlertWithMessage(msg){
	if(kony.application.getCurrentForm().id != "frmQRSuccess"){
		alert(msg);
	}else{
		frmQRSuccess.flexValidQRWithAmnt.setVisibility(false);
		frmQRSuccess.lblerrormsg1.text = msg;
		frmQRSuccess.flxErrorBody.height = "94%"
		frmQRSuccess.flxErrorBody.setVisibility(true);
		frmQRSuccess.flxPayFooter.setVisibility(false);
		frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
		frmQRSuccess.flxLoginFooter.setVisibility(false);
		frmQRSuccess.FlexFooter.setVisibility(false);  
		frmQRSuccess.FlexLoading.setVisibility(false);
		frmQRSuccess.FlexMain.setVisibility(true);
	}
	dismissLoadingScreen();
	return;
}

function assignQRFlow() {
  if (isSignedUser) {
    //frmscanqrLanding.enabledForIdleTimeout = true;
    //frmQRSuccess.enabledForIdleTimeout = true;
    //frmQRPaymentSuccess.enabledForIdleTimeout = true;
    loadMapforQrPay()
  } else {
    //frmscanqrLanding.enabledForIdleTimeout = false;
    //frmQRSuccess.enabledForIdleTimeout = false;
    //frmQRPaymentSuccess.enabledForIdleTimeout = false;
    showQRScanningLanding();
  }

}

function onClickBackFromQRSuccess() {
  if (isSignedUser) {
    /*showAccuntSummaryScreen();
    var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US"){
    	frmAccountSummaryLanding.lblAccntHolderName.text = gblCustomerName;
	}else{
		frmAccountSummaryLanding.lblAccntHolderName.text = gblCustomerNameTh;
	} */
    //showAccountSummaryNew();
    callCustomerAccountService();
  } else {
    accessPinOnClickLogin();
	gblApplicationLaunch = true;
    frmMBPreLoginAccessesPin.show();
  }
}

function displayQRSuccessPreshow() {
  frmQRSuccess.tbxEnterAmount.skin = "txtBlue300";
  frmQRSuccess.tbxEnterAmount.focusSkin = "txtBlue300";
  
  if(gblQRPayData["QR_TransType"]=="QR_Transfer"){
    frmQRSuccess.flexMobileNum.setVisibility(true);
    frmQRSuccess.flexRefNumber.setVisibility(false);
  }else{
    frmQRSuccess.flexMobileNum.setVisibility(false);
    frmQRSuccess.flexRefNumber.setVisibility(true);
  }
  if (gblQRPayData["transferAmt"] != undefined && gblQRPayData["transferAmt"] != "" && gblQRPayData["transferAmt"] != null) {
    //=============Cahgned by mayank========================Start
    var formattedAmount = amountFormat(gblQRPayData["transferAmt"]);
    frmQRSuccess.LabelAmount.text = formattedAmount[0];
    frmQRSuccess.LabelAmountDigits.text = formattedAmount[1];
    //frmQRSuccess.LabelAmount.text = commaFormatted(gblQRPayData["transferAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    //frmQRSuccess.LabelAmount.text = gblQRPayData["transferAmt"];
    //=============Cahgned by mayank====================================End
    frmQRSuccess.flexAmounttbx.setVisibility(false);
    frmQRSuccess.flexAmountlbl.setVisibility(true);
    frmQRSuccess.flxPayFooter.setVisibility(true);
  } else {
    frmQRSuccess.tbxEnterAmount.skin = "txtBlue48px";
    frmQRSuccess.tbxEnterAmount.focusSkin = "txtBlue48px";
    frmQRSuccess.tbxEnterAmount.text = "";
    frmQRSuccess.tbxEnterAmount.placeholder = "0.00"+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
    //frmQRSuccess.tbxEnterAmount.placeholder = kony.i18n.getLocalizedString("msgEnterAmount");
    frmQRSuccess.LabelAmount.text = "";
    frmQRSuccess.flexAmounttbx.setVisibility(true);
    frmQRSuccess.flexAmountlbl.setVisibility(false);
    frmQRSuccess.flxPayFooter.setVisibility(false);
    frmQRSuccess.tbxEnterAmount.setFocus(true);
  }
  
  //setting the default values
  frmQRSuccess.tbxEnterAmount.text = "";
  frmQRSuccess.flxErrorBody.setVisibility(false);
  frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
  frmQRSuccess.flxLoginFooter.setVisibility(false);
  frmQRSuccess.FlexFooter.setVisibility(true);
  //frmQRSuccess.show();
}
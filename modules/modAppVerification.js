gblIDPStatus = "";
gblStatusData = [];
function init_frmMBeKycAppVerification(){
  frmMBeKycAppVerification.preShow = frmMBeKycAppVerification_Preshow_Action;
}

function frmMBeKycAppVerification_Preshow_Action(){
  frmMBeKycAppVerification.imageSelectedApp.src = getIdpAppicon(gblSelectedAppData["provider"]);
  frmMBeKycAppVerification.btnClose.onClick = closeHowtoVerifyPopup;
  if(gblIDPStatus == "pending"){
    frmMBeKycAppVerification.flexBody.isVisible = true
    frmMBeKycAppVerification.flexHowtoverifyPopup.isVisible = false
    frmMBeKycAppVerification.flexFacenotMatch.isVisible = false
    frmMBeKycAppVerification.flexHowtoVerify.onTouchStart = onClickHowtoVerify;
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleIdentifyIdP"),null,false,"3");
    setFooterSinglebtnToForm(kony.i18n.getLocalizedString("eKYC_btnChangeBank"),onClickChangeApp); 
    frmMBeKycAppVerification.lblverifywith.text = kony.i18n.getLocalizedString("eKYC_msgVerifyWithBank")+" "+gblSelectedAppData["applicationName"];
    var pendMsg = kony.i18n.getLocalizedString("eKYC_lkHowtoVerifyBank");
    pendMsg = pendMsg.replace("{bank_name}",gblSelectedAppData["applicationName"]);
    frmMBeKycAppVerification.lblhowtoVerify.text = pendMsg;
    frmMBeKycAppVerification.lblExpireDate.text = kony.i18n.getLocalizedString("eKYC_lbExpiryDate")+":"+gblSelectedAppData["expireDate"];
    frmMBeKycAppVerification.lblNothingHappen.text = kony.i18n.getLocalizedString("eKYC_lbNothingHappen");
    frmMBeKycAppVerification.btnSendrequest.text = kony.i18n.getLocalizedString("eKYC_lkSendRequest");
    frmMBeKycAppVerification.flexBlurscreen.isVisible = true
    frmMBeKycAppVerification.flexHowtoVerify.isVisible = true
    frmMBeKycAppVerification.flexPending.isVisible = true
    frmMBeKycAppVerification.flexcompletedVerification.isVisible = false
    frmMBeKycAppVerification.flexRejected.isVisible = false
    frmMBeKycAppVerification.flexError.isVisible = false
    frmMBeKycAppVerification.flexSetpsFatca.isVisible = false 
    frmMBeKycAppVerification.btnSendrequest.onClick = onClickRequestBack;
  }else if(gblIDPStatus == "Rejected"){
    frmMBeKycAppVerification.flexBody.isVisible = true
    frmMBeKycAppVerification.flexHowtoverifyPopup.isVisible = false
    frmMBeKycAppVerification.flexFacenotMatch.isVisible = false
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleIdentifyIdP"),null,false,"3");
    setFooterSinglebtnToForm(kony.i18n.getLocalizedString("eKYC_btnChangeBank"),onClickChangeApp);  
    var rejectmsg = kony.i18n.getLocalizedString("eKYC_msgResultReject")
    rejectmsg = rejectmsg.replace("{bank_name}",gblSelectedAppData["applicationName"]);
    frmMBeKycAppVerification.lblverifywith.text = rejectmsg;
    frmMBeKycAppVerification.btnSendrequest.text = kony.i18n.getLocalizedString("eKYC_lkSendRequest");
    frmMBeKycAppVerification.flexBlurscreen.isVisible = true
    frmMBeKycAppVerification.flexcompletedVerification.isVisible = false
    frmMBeKycAppVerification.flexSetpsFatca.isVisible = false
    frmMBeKycAppVerification.flexRejected.isVisible = true
    frmMBeKycAppVerification.flexError.isVisible = true
    frmMBeKycAppVerification.flexHowtoVerify.isVisible = false
    frmMBeKycAppVerification.flexPending.isVisible = false
  }else if(gblIDPStatus == "Completed"){
    frmMBeKycAppVerification.flexBody.isVisible = true
    frmMBeKycAppVerification.flexHowtoverifyPopup.isVisible = false
    frmMBeKycAppVerification.flexFacenotMatch.isVisible = false
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleIdentifyIdP"),null,false,"3");
    setFooterSinglebtnToForm(kony.i18n.getLocalizedString("eKYC_btnFATCA"),onClickFATCA);  
    var successmsg = kony.i18n.getLocalizedString("eKYC_msgResultSuccess")
    successmsg = successmsg.replace("{bank_name}",gblSelectedAppData["applicationName"]);
    frmMBeKycAppVerification.lblverifywith.text = successmsg;
    frmMBeKycAppVerification.btnSendrequest.text = kony.i18n.getLocalizedString("eKYC_lkSendRequest");
    frmMBeKycAppVerification.flexBlurscreen.isVisible = false
    frmMBeKycAppVerification.flexcompletedVerification.isVisible = true
    frmMBeKycAppVerification.flexSetpsFatca.isVisible = true
    frmMBeKycAppVerification.flexRejected.isVisible = false
    frmMBeKycAppVerification.flexError.isVisible = false
    frmMBeKycAppVerification.flexHowtoVerify.isVisible = false
    frmMBeKycAppVerification.flexPending.isVisible = false
    frmMBeKycAppVerification.btnLearnFatca.onClick = onClickLearnFATCA;
  }else if(gblIDPStatus == "Failed"){
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleIdentifyIdP"),null,false,"3");
    //setFooterSinglebtnToForm(kony.i18n.getLocalizedString("eKYC_btnFATCA"),onClickFATCA);  
    frmMBeKycAppVerification.footers[0].isVisible =false
    var failmsg = kony.i18n.getLocalizedString("eKYC_msgResultFailed")
    failmsg = failmsg.replace("{bank_name}",gblSelectedAppData["applicationName"]);
    frmMBeKycAppVerification.lblverifywithFace.text = failmsg;
    frmMBeKycAppVerification.flexBody.isVisible = false
    frmMBeKycAppVerification.flexHowtoverifyPopup.isVisible = false
    frmMBeKycAppVerification.flexFacenotMatch.isVisible = true
    
    frmMBeKycAppVerification.imageOtherBank.base64 = gblStatusData["selfieData"];
    //frmMBeKycAppVerification.imageTMBbank.base64 = gblStatusData["idpData"];
    frmMBeKycAppVerification.lblFaceNotMatch.text = kony.i18n.getLocalizedString("eKYC_msgFaceNotMatched");
    frmMBeKycAppVerification.lblOtherBankName.text = " Other Bank";
    frmMBeKycAppVerification.lblTMBBankName.text = "TMB bank";
    frmMBeKycAppVerification.lblstaticText.text = kony.i18n.getLocalizedString("eKYC_txtFATCA");

  }
  
}

function getIdentitySendRequest(){
  try{
    var inputParam = {};
    inputParam["providerId"] = gblSelectedAppData["id"]; 
    invokeServiceSecureAsync("getIdentity", inputParam , callBackgetIdentitySendRequest);
  }catch(e){
    kony.print("@@@ getIdentitySendRequest() Exeption:::"+e);
  }
}

function callBackgetIdentitySendRequest(status, resultTable){
  try{
    if (status == 400){
      if (resultTable["opstatus"] == 0){
        var referenceId = resultTable["referenceId"];
        kony.print("referenceId:::"+referenceId);
        eKYCIdpRequestStatus();
      }
  	}
  }catch(e){
    kony.print("@@@ callBackgetIdentitySendRequest() Exeption:::"+e);
  }
}

function eKYCIdpRequestStatus(referenceId){
  try{
    var inputParam = {};
    inputParam["referenceId"] = "111436-fd55-4534-87bb-6a40e7920e2f";  
    invokeServiceSecureAsync("eKYCIdpRequestStatus", inputParam , callBackeKYCIdpRequestStatus);
  }catch(e){
    kony.print("@@@ eKYCIdpRequestStatus() Exeption:::"+e);
  }
}

function callBackeKYCIdpRequestStatus(status, resultTable){
  try{
    if (status == 400){
      if (resultTable["opstatus"] == 0){
        kony.print("resultTable ::"+JSON.stringify(resultTable));
        gblStatusData["selfieData"] = resultTable["selfieData"];
        gblStatusData["idpData"] = resultTable["idpData"];
        gblIDPStatus = resultTable["status"];
        if(gblIDPStatus == "pending"){
          gblSelectedAppData["provider"] = resultTable["provider"];
          gblSelectedAppData["applicationName"] = resultTable["applicationName"];
          gblSelectedAppData["expireDate"] = resultTable["expireDate"];
        }
        //gblIDPStatus = resultTable["status"]
        //gblIDPStatus = "Pending";
        //gblIDPStatus = "Rejected";
        //gblIDPStatus = "Completed";
        //gblIDPStatus = "Failed";
        frmMBeKycAppVerification.show();
      }
  	}
  }catch(e){
    kony.print("@@@ callBackgetRequestStatus() Exeption:::"+e);
  }
}

function onClickChangeApp(){
  gblSelappPressed = false;
  gblIDPData = "";
  gblSelectedAppData = [];
  gblIDPSearchData = "";
  gblIsSearch = false;
  getIdentityproviderService();
}

function onClickRequestBack(){
  getIdentitySendRequest();
}
function onClickHowtoVerify(){
  frmMBeKycAppVerification.footers[0].isVisible =false
  frmMBeKycAppVerification.headers[0].isVisible =false
  frmMBeKycAppVerification.flexBody.isVisible = false;
  frmMBeKycAppVerification.flexHowtoverifyPopup.isVisible = true;
  var txtMsg = kony.i18n.getLocalizedString("eKYC_lkHowtoVerifyBank");
  txtMsg = txtMsg.replace("{bank_name}",gblSelectedAppData["applicationName"]);
  frmMBeKycAppVerification.lblHowtoverifyHeader.text = txtMsg;
  frmMBeKycAppVerification.lblHowtoverifytxt.text = kony.i18n.getLocalizedString("eKYC_txtHowtoVerify");
}
function onClickLearnFATCA(){
  frmMBeKycAppVerification.footers[0].isVisible =false
  frmMBeKycAppVerification.headers[0].isVisible =false
  frmMBeKycAppVerification.flexBody.isVisible = false;
  frmMBeKycAppVerification.flexHowtoverifyPopup.isVisible = true;
  frmMBeKycAppVerification.lblHowtoverifyHeader.text = kony.i18n.getLocalizedString("eKYC_lbFATCA");
  frmMBeKycAppVerification.lblHowtoverifytxt.text = kony.i18n.getLocalizedString("eKYC_txtFATCA");
}
function closeHowtoVerifyPopup(){
  frmMBeKycAppVerification.flexBody.isVisible = true;
  frmMBeKycAppVerification.flexHowtoverifyPopup.isVisible = false;
  frmMBeKycAppVerification.footers[0].isVisible =true
  frmMBeKycAppVerification.headers[0].isVisible =true
}
function onClickFATCA(){
  navigateFATCAfromEKYC();
  //navigatefrmMBeKYCContactInfo();
}
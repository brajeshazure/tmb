function frmEDonationPaymentComplete_init_Action(){
  frmEDonationPaymentComplete.preShow = frmEDonationPaymentComplete_preShow_Action;
  frmEDonationPaymentComplete.btnGoBack.onClick = frmEDonationPaymentComplete_btnGoBack_Action;
  frmEDonationPaymentComplete.btnNext.onClick = frmEDonationPaymentComplete_btnNext_Action;
  frmEDonationPaymentComplete.btnBack.onClick = frmEDonationPaymentComplete_btnBack_Action;
  //frmEDonationPaymentComplete.postShow = frmEDonationPaymentComplete_postShow_Action;
  frmEDonationPaymentComplete.onDeviceBack = disableBackButton;
  
}

function frmEDonationPaymentComplete_btnBack_Action(){
  showLoadingScreen();
  setDefaultSkinToDonationAmt();
  frmDonationSelectAmount.imgTaxCheckbox.src = "radio_check.png";
  frmDonationSelectAmount.flexDonationAmtTextBox.skin = "sknFlexBoxBG";
  frmDonationSelectAmount.txtAmount.text = "";
  frmDonationSelectAmount.btnConfirm.skin = "sknBtnLightGreyDisableSKin";
  frmDonationSelectAmount.btnConfirm.focusSkin = "sknBtnLightGreyDisableSKin";
  gblEDonation["amount"] = "";
  if(gbleDonationType == "QR_Type"){
    gbleDonationType = "";
    gblDonationNormalFlow = false;
    frmEDonationPaymentComplete_btnBack_QR();
  }else{
    gbleDonationType = "";
    gblDonationNormalFlow = false;
    showAccountSummaryFromMenu();
  }
  dismissLoadingScreen();
}

function frmEDonationPaymentComplete_btnGoBack_Action(){
  showLoadingScreen();
  gbleDonationType = "";
  gblDonationNormalFlow = false;
  MBMyActivitiesShowCalendar();
  dismissLoadingScreen();
}


function frmEDonationPaymentComplete_btnNext_Action(){
  
  if(gbleDonationType == "QR_Type"){
    gbleDonationType = "";
    gblDonationNormalFlow = false;
    frmEDonationPaymentComplete_btnNext_QR();
  }else{
    gbleDonationType = "";
    gblDonationNormalFlow = false;
    scanBarcode();
  }
  setDefaultSkinToDonationAmt();
  frmDonationSelectAmount.imgTaxCheckbox.src = "radio_check.png";
  frmDonationSelectAmount.flexDonationAmtTextBox.skin = "sknFlexBoxBG";
  frmDonationSelectAmount.txtAmount.text = "";
  gblTaxDec = "T";
  gblScanAmount = "";
  frmDonationSelectAmount.btnConfirm.skin = "sknBtnLightGreyDisableSKin";
  frmDonationSelectAmount.btnConfirm.focusSkin = "sknBtnLightGreyDisableSKin";
  gblEDonation["amount"] = "";
}

function frmEDonationPaymentComplete_preShow_Action(){
  var locale = kony.i18n.getCurrentLocale();
  changeStatusBarColor();
  frmEDonationPaymentComplete.lblConfirmationHeader.text =  kony.i18n.getLocalizedString("MIB_P2PCompleteTitle");
  frmEDonationPaymentComplete.LabelSave.text = kony.i18n.getLocalizedString("TRShare_Image");
  frmEDonationPaymentComplete.LabelMessenger.text = kony.i18n.getLocalizedString("TRShare_Messenger");
  frmEDonationPaymentComplete.LabelLine.text = kony.i18n.getLocalizedString("TRShare_Line");
  frmEDonationPaymentComplete.LabelMore.text = kony.i18n.getLocalizedString("More");
  if (gblTaxDec == "T"){
    frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_eDSuccessTxn");
  }else{
    frmEDonationPaymentComplete.lblOnBehalfOf.text = kony.i18n.getLocalizedString("MB_eDSuccessTxnNoConsent");
  }
  
  frmEDonationPaymentComplete.lblDonateForm.text = kony.i18n.getLocalizedString("MB_eDFromAcct");
  frmEDonationPaymentComplete.lblAccountNumber.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountNumber");
  frmEDonationPaymentComplete.lblAccountName.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountName");
  frmEDonationPaymentComplete.lblCitizenID.text = removeColonFromEnd(kony.i18n.getLocalizedString("MB_eDCI"));
  frmEDonationPaymentComplete.lblDonateTo.text = kony.i18n.getLocalizedString("MB_eDToBiller");
  frmEDonationPaymentComplete.lblDonationDesc.text = kony.i18n.getLocalizedString("MB_eD1Time");
  frmEDonationPaymentComplete.lblTaxDeclared.text = kony.i18n.getLocalizedString("MB_eDTax");
  frmEDonationPaymentComplete.lblAmount.text = kony.i18n.getLocalizedString("keyAmount");
  frmEDonationPaymentComplete.lblPaymentDate.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyPaymentDateTime"));
  frmEDonationPaymentComplete.lblTransactionRefNo.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyTransactionRefNo"));
  frmEDonationPaymentComplete.lblStartOn.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyBillPaymentStartOn"));
  frmEDonationPaymentComplete.lblEnding.text = removeColonFromEnd(kony.i18n.getLocalizedString("Transfer_Ending"));
  frmEDonationPaymentComplete.lblRepeatAs.text = removeColonFromEnd(kony.i18n.getLocalizedString("Transfer_Repeat"));
  frmEDonationPaymentComplete.repeatAsVal.text = getFeqLocale();
  frmEDonationPaymentComplete.lblExecute.text = removeColonFromEnd(kony.i18n.getLocalizedString("TRConfirm_D1Time"));
  frmEDonationPaymentComplete.btnBack.text = kony.i18n.getLocalizedString("CUL02_btnHome");
  frmEDonationPaymentComplete.btnNext.text = kony.i18n.getLocalizedString("MB_eDDonateMoreBtn");
  frmEDonationPaymentComplete.btnGoBack.text = kony.i18n.getLocalizedString("keyReturn");
  frmEDonationPaymentComplete.lblSavingImageTxt.text = kony.i18n.getLocalizedString("MB_eDSaveImg");
  //No schedule for now
  //frmEDonationPaymentComplete.flxLineBox1.setVisibility(false);
  frmEDonationPaymentComplete.flxSection2.setVisibility(false);
  frmEDonationPaymentComplete.flxLineBox2.setVisibility(false);
  frmEDonationPaymentComplete.flxSection3.setVisibility(false);
  if(kony.application.getCurrentForm().id == "frmEDonationPaymentConfirm" || kony.application.getPreviousForm().id == "frmEDonationPaymentConfirm"){
    frmEDonationPaymentComplete.flxFooter.setVisibility(true);
    frmEDonationPaymentComplete.flxFooterHistoryView.setVisibility(false);
    //Need some condition to display schedule detail
  }else{
    frmEDonationPaymentComplete.lblExecute.text = removeColonFromEnd(kony.i18n.getLocalizedString("TRConfirm_D1Time"));
    frmEDonationPaymentComplete.flxFooter.setVisibility(false);
    frmEDonationPaymentComplete.flxFooterHistoryView.setVisibility(true);
  }
}

function frmEDonationPaymentComplete_postShow_Action(){
    if(kony.application.getPreviousForm().id == "frmEDonationPaymentConfirm"){
      	if(frmEDonationPaymentComplete.imagestatusicon.src == "completeico.png")
		autoShareImageQRDonation();
    } 
}

function getEDonationTxnDetCallBackMB(status, resulttable) {
    frmEDonationPaymentComplete.lblExecute.text = removeColonFromEnd(kony.i18n.getLocalizedString("TRConfirm_D1Time"));
    frmEDonationPaymentComplete.flxFooter.setVisibility(false);
    frmEDonationPaymentComplete.flxFooterHistoryView.setVisibility(true);
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			kony.print("MKI resulttable = " + JSON.stringify(resulttable));
			if (resulttable["ExecTransactionDetails"].length > 0) {
				frmEDonationPaymentComplete.lblDonateFormValue.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
				frmEDonationPaymentComplete.lblAccountNameValue.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
				frmEDonationPaymentComplete.lblAccountNumberValue.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
  				frmEDonationPaymentComplete.lblAccountNumberValueMasked.text = "xxx-x-" + frmEDonationPaymentComplete.lblAccountNumberValue.text.substring(6, 11) + "-x";
				var billerName = "";
              	billerName = resulttable["ExecTransactionDetails"][0]["billerCustomerName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["billerCustomerName"] : "";
				frmEDonationPaymentComplete.lblBillerName.text = billerName;
				frmEDonationPaymentComplete.lblPaymentDateVal.text = resulttable["ExecTransactionDetails"][0]["transferDate"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";

              if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
					frmEDonationPaymentComplete.transactionRefNoVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
						"transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
					frmEDonationPaymentComplete.lblAmountTxt.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"]
					[0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n
						.getLocalizedString("currencyThaiBaht") : " ";	
                var txnStatus = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"];
                frmEDonationPaymentComplete.hbxShareOption.setVisibility(true);
                frmEDonationPaymentComplete.flxBody.height = "70%";
                frmEDonationPaymentComplete.flxTopSection.setVisibility(true);
                if(txnStatus != "01"){
                  frmEDonationPaymentComplete.hbxShareOption.setVisibility(false);
                  frmEDonationPaymentComplete.flxTopSection.setVisibility(false);
                  frmEDonationPaymentComplete.flxBody.height = "80%";
                }
					txnStatus == "01" ? frmEDonationPaymentComplete.imagestatusicon.src = "completeico.png" : frmEDonationPaymentComplete.imagestatusicon.src = "mes2.png";
				} else {
					frmEDonationPaymentComplete.transactionRefNoVal.text = "";
					frmEDonationPaymentComplete.lblAmountTxt.text = "";
				}
              	var citizen = "";
              	if(resulttable["ExecTransactionDetails"][0]["billerRef2"] != undefined && resulttable["ExecTransactionDetails"][0]["billerRef2"] != "0" ){
              		citizen = resulttable["ExecTransactionDetails"][0]["billerRef2"];
              	}
              	
              if(citizen == ""){
                frmEDonationPaymentComplete.flxCitizenID.setVisibility(false);
                frmEDonationPaymentComplete.flxTaxDeclared.setVisibility(false);
                frmEDonationPaymentComplete.lblTaxDeclared.setVisibility(false);
                frmEDonationPaymentComplete.lblCitizenIDValueMasked.text ="";
                frmEDonationPaymentComplete.lblCitizenIDValue.text = "";
                frmEDonationPaymentComplete.imgForm.top = "6dp";
              }else{
                frmEDonationPaymentComplete.flxTaxDeclared.setVisibility(true);
                frmEDonationPaymentComplete.flxCitizenID.setVisibility(true);
                frmEDonationPaymentComplete.lblTaxDeclared.setVisibility(true);
                frmEDonationPaymentComplete.lblCitizenIDValueMasked.text =maskCitizenID(citizen);
                frmEDonationPaymentComplete.lblCitizenIDValue.text = convetFormatCitizenField(citizen);
                frmEDonationPaymentComplete.imgForm.top = "12dp";
              }
                var imagesUrl ="";
                imagesUrl=loadBillerIcons(resulttable["ExecTransactionDetails"][0]["billerCommCode"]);
              	frmEDonationPaymentComplete.imgTo.src = imagesUrl;
				frmEDonationPaymentComplete.imgForm.src = loadBankIcon(gblTMBBankCD);

				dismissLoadingScreen();
				frmEDonationPaymentComplete.show();
              	kony.timer.schedule("edonation", function(){if(kony.application.getPreviousForm().id == "frmEDonationPaymentConfirm"){
                if(frmEDonationPaymentComplete.imagestatusicon.src == "completeico.png")
                  autoShareImageQRDonation();
                }kony.timer.cancel("edonation");}, 1, false);
				gbleDonationType = "";
			} else {
				 dismissLoadingScreen();
				alert("No Records found");
			}
		} else {
			 dismissLoadingScreen();
		}
	}
	 dismissLoadingScreen();
}
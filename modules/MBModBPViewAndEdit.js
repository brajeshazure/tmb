gblEndingFreqOnLoadMB = "";
gblAmountSelectedMB = false;
gblNicknameAccount = "";
gblBillerNickName = "";
gblTPCustomerNameVal = "";
gblMinAmount = 0;
gblMaxAmount = 0;
//gblScheduleFreqChangedMB = false;
//gblexecutionOnLoadMB = "";
//gblendDateOnLoadMB = "";
/*
   **************************************************************************************
		Module	: numberOfExecution
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
// for Execution times 

function numberOfExecutionMB(date1, date2, repeatAs) {

	var startDate = parseDate(date1);
	var endDate = parseDate(date2);
	var executionTimes = "";
	if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("keyDaily")) || kony.string.equalsIgnoreCase(repeatAs, "Daily")) {
		executionTimes = daydiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("keyWeekly"))|| kony.string.equalsIgnoreCase(repeatAs, "Weekly") ) {
		executionTimes = weekdiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("keyMonthly")) || kony.string.equalsIgnoreCase(repeatAs, "Monthly")) {
		executionTimes = monthdiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("keyYearly")) || kony.string.equalsIgnoreCase(repeatAs, "Yearly")) {
		executionTimes = yeardiff(startDate, endDate);
	}
	return executionTimes;
}
/*
   **************************************************************************************
		Module	: endOnDateCalculator
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
// for end on date calculation

function endOnDateCalculatorMB(staringDateFromCalendar, repeatTimesTextBoxValue,repeatAs) {
    var endingDate = parseDate(staringDateFromCalendar);
    var numberOfDaysToAdd = 0;
    
	
    if (kony.string.equalsIgnoreCase(repeatAs, "Daily")) {
        numberOfDaysToAdd = repeatTimesTextBoxValue * 1;
            endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd - 1);
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Weekly")) {
        numberOfDaysToAdd = ((repeatTimesTextBoxValue-1) * 7);
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Monthly")) {
            	endingDate.setDate(endingDate.getDate());
                var dd = endingDate.getDate();
                
    			var mm = endingDate.getMonth() + 1;
    			
    			var newmm = parseFloat(mm.toString())+ parseFloat(repeatTimesTextBoxValue - 1);
    			
    			var newmmadd = newmm % 12;
    			if(newmmadd == 0){
    				newmmadd = 12;
    			}
    			
    			
    			var yearAdd = Math.floor((newmm / 12));
    			
   				var y = endingDate.getFullYear();
   				if(newmmadd == 12){
   				   y = parseFloat(y )+ parseFloat(yearAdd)-1;	
   				}else
   				   y = parseFloat(y )+ parseFloat(yearAdd);
   				   
   				mm = parseFloat(mm.toString())+ newmmadd;
   				
   				if(newmmadd == 2){
   				if(dd > 28){
   				dd = 28;
   				}
   				}
   				if(newmmadd == 4 || newmmadd == 6 || newmmadd == 9 || newmmadd == 11){
   				if(dd > 30){
   				dd = 30;
   				}
   				}
   				if (dd < 10) {
		        	dd = '0' + dd;
		   		 }
   				if(newmmadd < 10){
   					newmmadd = '0'+newmmadd;
   				}
   				
   				var someFormattedDate = dd + '/' + newmmadd + '/' + y;
   				return someFormattedDate;
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Yearly")) {
        var dd = endingDate.getDate();
   		var mm = endingDate.getMonth() + 1;
    	var y = endingDate.getFullYear();
    	var newYear = parseFloat(y)+ parseFloat(repeatTimesTextBoxValue - 1) ;
    	if (dd < 10) {
        	dd = '0' + dd
   		 }
   		 if (mm < 10) {
      	  mm = '0' + mm
   		 }
    var someFormattedDate = dd + '/' + mm + '/' + newYear;
    return someFormattedDate;
    }
    var dd = endingDate.getDate();
    var mm = endingDate.getMonth() + 1;
    var y = endingDate.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var someFormattedDate = dd + '/' + mm + '/' + y;
    return someFormattedDate;
}


//gblBillerStatus = "1";


gblBillerStatus = "0";
gblBPflag = false; 
gblScheID = "";
function getMasterBillerDataForMB(scheIDBP){
  // alert("master biller service");
    gblBPflag = true; 
    gblScheID = scheIDBP;
	var inputParam = {};
   // inputParam["IsActive"] = "1";
    inputParam["BillerGroupType"] = "0";
   	showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiryForEditBP", inputParam, callBackMasterBillerInqForEditBPMBService)
}

function callBackMasterBillerInqForEditBPMBService(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["MasterBillerInqRs"].length > 0 && result["MasterBillerInqRs"] != null) {
				invokeCustomerBillInqForEditBPMBService();
			} else {
				dismissLoadingScreen();
				alert(result["errMsg"]);
			}
		}
	}
}


function getMasterBillerDataForMBTU(scheIDTU){
  // alert("master biller service");
   gblScheID = scheIDTU;
   gblBPflag = false; 
   frmBillPaymentView.lblHead.text = kony.i18n.getLocalizedString("keyMBViewTopUP");//"View Top Up";
	var inputParam = {};
   // inputParam["IsActive"] = "1";
    inputParam["BillerGroupType"] = "1";
	showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiryForEditBP", inputParam, callBackMasterBillerInqForEditBPMBService)
}

function callBackMasterBillerInqForEditBPMBService(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["MasterBillerInqRs"].length > 0 && result["MasterBillerInqRs"] != null) {
				invokeCustomerBillInqForEditBPMBService();
			} else {
				dismissLoadingScreen();
			}
		}else{
			alert(result["errMsg"]);
		}
	}
}


function invokeCustomerBillInqForEditBPMBService() {
	var inputParam = {
	  			IsActive: "1"
	   				 };
	invokeServiceSecureAsync("customerBillInquiry", inputParam, callBackCustomerBillInqForEditBPMBService)
}

function callBackCustomerBillInqForEditBPMBService(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			callPaymentInqServiceForEditBPMB();
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	}
}

function callPaymentInqServiceForEditBPMB() {
	//alert("payment inq service ");
	var inputparam = [];
    inputparam["scheRefID"] = gblScheID;//"FB13000000005767";//"FP1300000001201600";//"FP130000000109280000"; // change this value when integrated with calender / future transactions page
   // inputparam["scheRefID"] = "SB1300000002171300";//"SB1300000002173500";
    inputparam["rqUID"] = "";
    invokeServiceSecureAsync("doPmtInqForEditBP", inputparam, callBackPaymenInqServiceForEditBPMB);

}



gblexecutionOnLoadMB = "";
gblendDateOnLoadMB = "";
function showHideDateSection(command){
	frmBillPaymentEditFutureNew.lblToTxt.setVisibility(command); //To
	frmBillPaymentEditFutureNew.lblEndOnDate.setVisibility(command); //To Date
	frmBillPaymentEditFutureNew.lblExecutetimes.setVisibility(command); //Execute
	frmBillPaymentEditFutureNew.lblTimes.setVisibility(command); //Time
    frmBillPaymentEditFutureNew.flxRecurringDetail.setVisibility(command); //Remaining to execute

}
function callBackPaymenInqServiceForEditBPMB(status, result) {
	showHideDateSection(true);
	frmBillPaymentEditFutureNew.flxDatePeriod.setVisibility(true);
	frmBillPaymentEditFutureNew.flxRepeatAs.setVisibility(true);
	gblTPCustomerNameVal = "";
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			frmBillPaymentEditFutureNew.flxRef2.setVisibility(true);
			if (result["PmtInqRs"].length > 0 && result["PmtInqRs"] != null) {
				var channelID = result["PmtInqRs"][0]["ChannelID"];
				gblFromAccIdMB = result["PmtInqRs"][0]["FromAccId"];
				gblFromAccTypeMB = result["PmtInqRs"][0]["FromAccType"];
				gblAmountMB = result["PmtInqRs"][0]["Amt"];
				gblstartOnMB = result["PmtInqRs"][0]["Duedt"];
				gblTransCodeMB = result["PmtInqRs"][0]["TransCode"];
				gblBillerCategoryID = result["PmtInqRs"][0]["billerCategoryID"]; //BillerCategoryID for creditcard mask
				var myNote = "";
				if(result["PmtInqRs"][0]["MyNote"] == null || result["PmtInqRs"][0]["MyNote"] == undefined || result["PmtInqRs"][0]["MyNote"] == ""){
                	myNote = "";
                }else{
                	myNote = result["PmtInqRs"][0]["MyNote"];
                }

				var scheRefNo = result["PmtInqRs"][0]["scheRefNo"];
				var execTimesOnLoadMB = result["PmtInqRs"][0]["ExecutionTimes"];
				var onLoadRepeatAsMB = result["PmtInqRs"][0]["RepeatAs"];
				
				gblToAccNoMB = result["PmtInqRs"][0]["ToAccId"];
				gblToAccTypeMB = result["PmtInqRs"][0]["ToAccType"];
				var endDateOnLoadMB = result["PmtInqRs"][0]["EndDate"];
				var pmtOrderdt = result["PmtInqRs"][0]["InitiatedDt"]; 
				var bankId = result["PmtInqRs"][0]["BankId"];
				gblCustPayeeIDMB = result["PmtInqRs"][0]["InternalBillerID"];
				gblPmtMethodFromPmt = result["PmtInqRs"][0]["PmtMethod"];
				
				gblRef2EditBPMB = "";
				if(result["PmtInqRs"][0]["Ref2"] != undefined && result["PmtInqRs"][0]["Ref2"] != "undefined"){
					gblRef2EditBPMB = result["PmtInqRs"][0]["Ref2"];
					frmBillPaymentEditFutureNew.lblRef2value.text = result["PmtInqRs"][0]["Ref2"];
				}else{
					gblRef2EditBPMB = "";
					frmBillPaymentEditFutureNew.lblRef2value.text = "";
				}
				frmBillPaymentEditFutureNew.lblamtvalue.text = commaFormatted(gblAmountMB);
				//frmBillPaymentEditFutureNew.lblamtvalue.text = commaFormatted(gblAmountMB) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				frmBillPaymentEditFutureNew.lblPaymentDateValue.text = dateFormatForDisplayWithTimeStampMB(pmtOrderdt);
              	if(myNote == null || myNote == undefined || myNote == ""){
                	frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text = "";
                }else{
                	frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text = myNote;
                }
				if(gblBPflag){
					frmBillPaymentEditFutureNew.lblScheduleRefValue.text = "SB" + scheRefNo.substring(2, scheRefNo.length);
				}else{
					frmBillPaymentEditFutureNew.lblScheduleRefValue.text = scheRefNo;//.replace("S", "F");
				}
				frmBillPaymentEditFutureNew.lblRecurring.text = "";
				onClickSpecifiedPayment();
				frmBillPaymentEditFutureNew.lblStartDate.text = dateFormatForDisplay(gblstartOnMB);
				if (onLoadRepeatAsMB != "" && onLoadRepeatAsMB != undefined && onLoadRepeatAsMB != null) {
					if (onLoadRepeatAsMB == "Annually") { //kony.i18n.getLocalizedString("KeyAnnually")
						onLoadRepeatAsMB = "Yearly";
						gblOnLoadRepeatAsMB = onLoadRepeatAsMB;
						frmBillPaymentEditFutureNew.labelRepeatAsValue.text = onLoadRepeatAsMB;
					} else {
						gblOnLoadRepeatAsMB = onLoadRepeatAsMB;
						frmBillPaymentEditFutureNew.labelRepeatAsValue.text = onLoadRepeatAsMB;
					}
					if (endDateOnLoadMB != "" && endDateOnLoadMB != undefined && endDateOnLoadMB != null) {
						gblEndingFreqOnLoadMB = "OnDate";    //kony.i18n.getLocalizedString("keyOnDate");	//"On Date"
						gblScheduleEndBPMB = gblEndingFreqOnLoadMB; 
						frmBillPaymentEditFutureNew.lblEndOnDate.text = dateFormatForDisplay(endDateOnLoadMB);
						var executiontimes = numberOfExecutionMB(frmBillPaymentEditFutureNew.lblStartDate.text, frmBillPaymentEditFutureNew.lblEndOnDate.text,gblOnLoadRepeatAsMB);
						frmBillPaymentEditFutureNew.lblExecutetimes.text = (executiontimes).toString();
						gblendDateOnLoadMB = dateFormatForDisplay(endDateOnLoadMB);
						gblexecutionOnLoadMB = executiontimes;
					} else if (execTimesOnLoadMB != "" & execTimesOnLoadMB != undefined && execTimesOnLoadMB != null) {
						gblEndingFreqOnLoadMB = "After";	//kony.i18n.getLocalizedString("keyAfter");	//"After"
						gblScheduleEndBPMB = gblEndingFreqOnLoadMB;
						frmBillPaymentEditFutureNew.lblExecutetimes.text = (execTimesOnLoadMB).toString();
						var endOnDate = endOnDateCalculatorMB(frmBillPaymentEditFutureNew.lblStartDate.text, execTimesOnLoadMB,gblOnLoadRepeatAsMB);
						frmBillPaymentEditFutureNew.lblEndOnDate.text = endOnDate;
						gblendDateOnLoadMB = endOnDate;
						gblexecutionOnLoadMB = execTimesOnLoadMB;
					} else {
						gblEndingFreqOnLoadMB = "Never";	//kony.i18n.getLocalizedString("keyNever");	//"Never"
						gblScheduleEndBPMB = gblEndingFreqOnLoadMB;
						frmBillPaymentEditFutureNew.lblEndOnDate.text = "-";
						frmBillPaymentEditFutureNew.lblExecutetimes.text = "-";
						gblexecutionOnLoadMB = "";
						gblendDateOnLoadMB = "";
                      	showHideDateSection(false);
					}
				}else{
					onLoadRepeatAsMB = "Once";
					gblOnLoadRepeatAsMB = onLoadRepeatAsMB;
					frmBillPaymentEditFutureNew.lblEndOnDate.text = dateFormatForDisplay(gblstartOnMB); //"-";
					frmBillPaymentEditFutureNew.lblExecutetimes.text = "1"; //"-";
					frmBillPaymentEditFutureNew.labelRepeatAsValue.text = onLoadRepeatAsMB;
					gblexecutionOnLoadMB = "1";//"";
					gblendDateOnLoadMB = dateFormatForDisplay(gblstartOnMB); //"";
					gblEndingFreqOnLoadMB ="none";
					gblScheduleEndBPMB = gblEndingFreqOnLoadMB;
				
				}
				if(kony.string.equalsIgnoreCase(gblOnLoadRepeatAsMB, "Daily")){
					frmBillPaymentEditFutureNew.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyDaily");
				}else if(kony.string.equalsIgnoreCase(gblOnLoadRepeatAsMB, "Weekly")){
					frmBillPaymentEditFutureNew.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyWeekly");
				}else if(kony.string.equalsIgnoreCase(gblOnLoadRepeatAsMB, "Monthly")){
					frmBillPaymentEditFutureNew.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyMonthly");
				}else if(kony.string.equalsIgnoreCase(gblOnLoadRepeatAsMB, "Yearly")){
					frmBillPaymentEditFutureNew.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyYearly");
				}else{
					frmBillPaymentEditFutureNew.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyOnce");
				}
				
              	var totalProcessesTransactions = parseInt(result["PmtInqRs"][0]["TotalProcessesTransactions"]) || 0;

              		//Never gblexecutionOnLoadMB = ""
              		if(gblexecutionOnLoadMB == ""){
                    	showHideDateSection(false);
                    }else{
                        var remainningExec = 0;
                      	var maxRecurring = "" + gblexecutionOnLoadMB;
                      	var processted = "" + totalProcessesTransactions;
                      	remainningExec = maxRecurring - processted; //have to chek 
              			frmBillPaymentEditFutureNew.lblRecurring.text ="" + remainningExec;
                    }

					gblRef1ValueBPMB = result["PmtInqRs"][0]["ref1ValueB"];
					if(result["PmtInqRs"][0]["ref1ValueB"] != null && result["PmtInqRs"][0]["ref1ValueB"] != undefined){
						var ref1Len = result["PmtInqRs"][0]["ref1ValueB"].length;
						//masking CR code
						//below line is changed for CR - PCI-DSS masked Credit card no
						if(result["PmtInqRs"][0]["maskedRef1ValueB"] != null && result["PmtInqRs"][0]["maskedRef1ValueB"] != undefined){
							frmBillPaymentEditFutureNew.lblRef1valueMasked.text = result["PmtInqRs"][0]["maskedRef1ValueB"];
						}							
						//below line is added for CR - PCI-DSS masked Credit card no
						frmBillPaymentEditFutureNew.lblRef1value.text = result["PmtInqRs"][0]["ref1ValueB"];
					}else {
					
					frmBillPaymentEditFutureNew.lblRef1value.text = "";
					//below line is added for CR - PCI-DSS masked Credit card no
					frmBillPaymentEditFutureNew.lblRef1valueMasked.text = "";
					gblBillerNickName = "";
					}
					gblBillerStatus = result["PmtInqRs"][0]["activeStatus"];
					if(gblBPflag){
						frmBillPaymentEditFutureNew.flxRef2.setVisibility(true);
						if (result["PmtInqRs"][0]["ref2ValueB"] != "" &&  result["PmtInqRs"][0]["ref2ValueB"] != undefined && result["PmtInqRs"][0]["ref2ValueB"] != "undefined" ) {
							var ref2Len = result["PmtInqRs"][0]["ref2ValueB"].length;
							if(ref2Len >= "10"){
								frmBillPaymentEditFutureNew.lblRef2value.text = result["PmtInqRs"][0]["ref2ValueB"];
							}else{
								frmBillPaymentEditFutureNew.lblRef2value.text = result["PmtInqRs"][0]["ref2ValueB"];
							}
						}	
					}else{
						frmBillPaymentEditFutureNew.flxRef2.setVisibility(false);
					}
					if (result["PmtInqRs"][0]["billerNickName"] != null && result["PmtInqRs"][0]["billerNickName"] != "") {
						if (gblBillerStatus == 0) {
								frmBillPaymentEditFutureNew.lblBillerNickName.setVisibility(false);
						} else {
								gblBillerNickName = result["PmtInqRs"][0]["billerNickName"];
								frmBillPaymentEditFutureNew.lblBillerNickName.text = result["PmtInqRs"][0]["billerNickName"];
						}		
					}  else {
						//If Not avialble , show blank
						frmBillPaymentEditFutureNew.lblBillerNickName.text = "";
					}
							gblEditBillMethodMB = result["PmtInqRs"][0]["billerMethod"];
                            gblEditBillerGroupTypeMB = result["PmtInqRs"][0]["billerGroupType"];
               				gblBillerMethod = gblEditBillMethodMB;
  							gblbillerGroupType = gblEditBillerGroupTypeMB;
                            billerShortNameMB = result["PmtInqRs"][0]["billerShortName"];
                            gblBillerCompcodeEditMB = result["PmtInqRs"][0]["billerCompcode"];
                            billerNamTH=result["PmtInqRs"][0]["billerNameTH"];
                            gblBPEditEffDtMB = result["PmtInqRs"][0]["effDt"];// get this value as we need for onlinepaymenyinq
							
                            var local = kony.i18n.getCurrentLocale();
                            var billerNameMB = "";
                            gblbillerNameMB_EN = "";
                            gblbillerNameMB_TH = "";
                            labelReferenceNumber1MB = "";
                            labelReferenceNumber2MB = "";
                            gblbillerNameMB_EN = result["PmtInqRs"][0]["billerNameEN"];
                            gblbillerNameMB_TH = result["PmtInqRs"][0]["billerNameTH"];
                            if (local == "en_US") {
                                billerNameMB = result["PmtInqRs"][0]["billerNameEN"];
                                labelReferenceNumber1MB = result["PmtInqRs"][0]["labelReferenceNumber1EN"];
                                labelReferenceNumber2MB = result["PmtInqRs"][0]["labelReferenceNumber2EN"];
                            } else if (local == "th_TH") {
                                billerNameMB = result["PmtInqRs"][0]["billerNameTH"];
                                labelReferenceNumber1MB = result["PmtInqRs"][0]["labelReferenceNumber1TH"];
                                labelReferenceNumber2MB = result["PmtInqRs"][0]["labelReferenceNumber2TH"];
                            }
                           gblRef1LblEN = result["PmtInqRs"][0]["labelReferenceNumber1EN"];
                           gblRef1LblTH = result["PmtInqRs"][0]["labelReferenceNumber1TH"];
                           gblRef2LblEN = result["PmtInqRs"][0]["labelReferenceNumber2EN"];
                           gblRef2LblTH = result["PmtInqRs"][0]["labelReferenceNumber2TH"];
                           gblBillerTaxID = result["PmtInqRs"][0]["billerTaxID"];
                            if (gblBillerStatus == "0") {
                                frmBillPaymentEditFutureNew.lblBillerName.text = billerShortNameMB;
                                 if(labelReferenceNumber1MB != ""){
                                	frmBillPaymentEditFutureNew.lblRef1.text = findColonLastString(labelReferenceNumber1MB) ? labelReferenceNumber1MB : labelReferenceNumber1MB;
                                }
                                frmBillPaymentEditFutureNew.btnedit.setEnabled(false);
                                frmBillPaymentEditFutureNew.btnedit.skin = "btnDisableGrey";
                                frmBillPaymentEditFutureNew.lblRef2.text = "";
                                frmBillPaymentEditFutureNew.flxRef2.setVisibility(false);
                            } else {
                                frmBillPaymentEditFutureNew.lblBillerName.text = billerNameMB + " (" + gblBillerCompcodeEditMB + ") ";
                                if(labelReferenceNumber2MB != null && labelReferenceNumber2MB != undefined && labelReferenceNumber2MB != "" && 
                                (result["PmtInqRs"][0]["ref2ValueB"] != "" && result["PmtInqRs"][0]["ref2ValueB"] != "undefined")){
                                	
                                	frmBillPaymentEditFutureNew.lblRef2.text = labelReferenceNumber2MB;
                                }else{
                                	frmBillPaymentEditFutureNew.lblRef2.text = "";
                                	frmBillPaymentEditFutureNew.flxRef2.setVisibility(false);
                                }
                                 if(labelReferenceNumber1MB != ""){
                                	frmBillPaymentEditFutureNew.lblRef1.text = labelReferenceNumber1MB;
                                }
                                
                            }
              				
              //step amount 
							
							if(!gblBPflag && gblEditBillMethodMB ==1){
								if (gblBillerCompcodeEditMB != "2704"){
									if (result["StepAmount"].length > 0 && result["StepAmount"] != null){
										comboDataEditTPMB = [];
										var temp = [];
										var j = 1;	
                						for (var i = 0; i < result["StepAmount"].length; i++) {
                    						
                    						var tmpAmt = gblAmountMB+".00";
                    						var resultAmt = result["StepAmount"][i]["Amt"];
											var temp = {
													"lblAmount": commaFormatted(resultAmt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
														};
                   							 comboDataEditTPMB.push(temp);
                						}									 
										frmTopUpAmount.segPop.data = comboDataEditTPMB; 
                						frmTopUpAmount.segPop.selectedKey=0;
                					
                					
                						//frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKey=0;
									}
								}else{
									if (result["BillerMiscData"].length > 0 && result["BillerMiscData"] != null){
										
		                				for (var i = 0; i < result["BillerMiscData"].length; i++) {
						                    if (result["BillerMiscData"][i]["MiscName"] == "OLN.AMOUNT.MIN") {
						                       gblTopupAmtMin = result["BillerMiscData"][i]["MiscText"];						                       
						                    }
						                    if (result["BillerMiscData"][i]["MiscName"] == "OLN.AMOUNT.MAX") {
						                       gblTopupAmtMax = result["BillerMiscData"][i]["MiscText"];						                       
						                    }                   
						                }                				
									}
								}						
							}
                          	//BILLER_LOGO_URL="https://vit.tau2904.com/tmb/ImageRender";//hard code to check
							//var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblBillerCompcodeEditMB+ "&modIdentifier=MyBillers&random=" + Math.floor((Math.random() * 10000) + 1);
							var imagesUrl = loadBillerIcons(gblBillerCompcodeEditMB);
              				//BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblBillerCompcodeEditMB;
							frmBillPaymentEditFutureNew.imgBiller.src=imagesUrl;
							frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
  							frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);	
              				if (gblEditBillMethodMB == 0 || gblEditBillMethodMB ==1){
								forBPBillPmtInqMB(gblEditBillMethodMB);
							}else{
								frmBillPaymentEditFutureNew.lblPaymentFeeValue.text = "0.0";
                              	frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
                				forBPCallCustomerAccountInqMB(); 
							}					
				}else{
					dismissLoadingScreen();
					alert(result["AdditionalStatusDesc"]);
					//			
				}
				
			//	}
		} else {
			dismissLoadingScreen();
			//alert(result["errmsg"]);
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		}
	} else {
		if (status == 300) {
			dismissLoadingScreen();
			
		}
	}
}

function dateFormatForDisplayWithTimeStampMB(datetimestamp) {
	var year = datetimestamp.substring(0, 4);
	var month = datetimestamp.substring(5, 7);
	var date = datetimestamp.substring(8, 10);
	var TimeofActivation = datetimestamp.substring(11, 19);
	var formatedDatewithTimeStamp = date + "/" + month + "/" + year + " " + TimeofActivation;
	NormalDateMB=date + "/" + month + "/" + year;
	return formatedDatewithTimeStamp;
}

function forBPCallCustomerAccountInqMB() {
	inputparam = [];
	inputparam["billPayInd"] = "billPayInd";
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, forBPcusAccInqcallBackFunctionMB);
}

function turnOnOrOffAction(action){
        	frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(action);//amount
        	frmBillPaymentEditFutureNew.btnFull2.setEnabled(action);//full spec
        	frmBillPaymentEditFutureNew.btnSpecified2.setEnabled(action);//full spec
        	frmBillPaymentEditFutureNew.btnMinimum1.setEnabled(action);//full min spec
        	frmBillPaymentEditFutureNew.btnSpecified1.setEnabled(action);//full min spec
        	frmBillPaymentEditFutureNew.btnFull1.setEnabled(action);//full min spec
        	frmBillPaymentEditFutureNew.flxAmountSelect.setEnabled(action);//combo box
        	frmBillPaymentEditFutureNew.flxDebitCardDateDetails.setEnabled(action); //schedule detail
}

function forBPcusAccInqcallBackFunctionMB(status, result){
	if(status == 400){
		if(result["opstatus"] == 0){
			//alert("customer accout");
				//alert("Len:    "+result["custAcctRec"].length);
				var ICON_ID = "";
				if(result["custAcctRec"].length >0 && result["custAcctRec"] != null){
					gblCustNickBP_TH = "";
					gblCustNickBP_EN = "";
					gblProductNameTH = "";
                    gblProductNameEN = "";
                    gblNicknameAccount = "";
					var accFound = false;
						for(var i=0; i < result["custAcctRec"].length ; i++ ){
							var cusAccNo = result["custAcctRec"][i]["accId"];
							var pmtFromAcctNoMB = gblFromAccIdMB;
							// Below code added due to  DEF734 defect for MB billpayment
							 if(gblFromAccTypeMB == "SDA" || gblFromAccTypeMB == "CDA" ){
                               if(pmtFromAcctNoMB.length == "10")
                                 pmtFromAcctNoMB = "0000" + gblFromAccIdMB;
                               } else {
                                  pmtFromAcctNoMB = gblFromAccIdMB;
                            }
							if(cusAccNo == pmtFromAcctNoMB){ 
								accFound = true;
								var fromAccNickName = result["custAcctRec"][i]["acctNickName"];
								gblNicknameAccount = fromAccNickName;
								gblCustNickBP_TH = result["custAcctRec"][i]["acctNickNameTH"];
                              	gblProductNameTH = result["custAcctRec"][i]["acctNickNameTH"];
                              	gblProductNameEN = result["custAcctRec"][i]["acctNickNameEN"];
								gblCustNickBP_EN = result["custAcctRec"][i]["acctNickNameEN"];
								
								var fromAcctStatus = result["custAcctRec"][i]["personalisedAcctStatusCode"];
								var fromAccName = result["custAcctRec"][i]["accountName"];
								gblAvailableBal = result["custAcctRec"][i]["availableBal"];
								var productIDMB =result["custAcctRec"][i]["productID"];
								ICON_ID = result["custAcctRec"][i]["ICON_ID"];
								if (gblSMART_FREE_TRANS_CODES.indexOf(productIDMB) >= 0) {
									frmBillPaymentEditFutureNew.flxFeewave.setVisibility(true);
								}else{
									frmBillPaymentEditFutureNew.flxFeewave.setVisibility(false);
								}
								if(fromAcctStatus == "02"){
									if (frmBillPaymentEditFutureNew.flxForm.isVisible) {
										frmBillPaymentEditFutureNew.flxForm.setVisibility(false);
									}
									if (!frmBillPaymentEditFutureNew.flxDelMessage.isVisible) {
										frmBillPaymentEditFutureNew.flxDelMessage.setVisibility(true);
									}
									frmBillPaymentEditFutureNew.richtext446432445207554.skin = rchBlack150;
									var delAccMessage = kony.i18n.getLocalizedString("S2S_DelAcctMsg1")+"<br>"+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+"</br><a onclick= 'addMyAccntBPTPMB();'  style='display:block'>" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a><br>"+kony.i18n.getLocalizedString("S2S_BeforeProcTrans")+"</br>";

									frmBillPaymentEditFutureNew.flxDelMessage.setVisibility(true);		
									frmBillPaymentEditFutureNew.richtext446432445207554.setVisibility(true);
									frmBillPaymentEditFutureNew.richtext446432445207554.text = delAccMessage;
									
                                    
									frmBillPaymentEditFutureNew.flxDelMessage.onClick = addMyAccntBPTPMB;
									frmBillPaymentEditFutureNew.btnedit.setEnabled(false);
									frmBillPaymentEditFutureNew.btnedit.skin = "btnDisableGrey"; // check for the skin - To Do 
								}else{
									frmBillPaymentEditFutureNew.btnedit.setEnabled(true);
									frmBillPaymentEditFutureNew.btnedit.skin = "btnBlueSkin"; // check for the skin - To Do 
									frmBillPaymentEditFutureNew.richtext446432445207554.setVisibility(false);
									if (!frmBillPaymentEditFutureNew.flxForm.isVisible) {
										frmBillPaymentEditFutureNew.flxForm.setVisibility(true);
									}
									if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible) {
										frmBillPaymentEditFutureNew.flxDelMessage.setVisibility(false);
									}
									if(fromAccNickName != "" &&  fromAccNickName != undefined){
										frmBillPaymentEditFutureNew.lblFromAccountNickname.text = fromAccNickName;
									} else {
									//for eng  :ProductNameEng
									//for TH : ProductNameThai
									var local = kony.i18n.getCurrentLocale();			
									var length = cusAccNo.length;	
									var accNum = cusAccNo.substring(length - 4, length); // last four digits of the account number
									if(local == "en_US"){
										var ProductNameEng = result["custAcctRec"][i]["productNmeEN"];
										//alert("prod : " +ProductNameEng);
										frmBillPaymentEditFutureNew.lblFromAccountNickname.text= ProductNameEng +" "+accNum ;
									}else if(local == "th_TH"){
										var ProductNameThai = result["custAcctRec"][i]["productNmeTH"];
										frmBillPaymentEditFutureNew.lblFromAccountNickname.text = ProductNameThai +" "+accNum ;
									}
								}
							}
							
							frmBillPaymentEditFutureNew.lblFromAccountName.text = fromAccName;
							var tmp = cusAccNo;
							tmp = tmp.substring(tmp.length-10,tmp.length);
							frmBillPaymentEditFutureNew.lblFromAccountNumber.text = addHyphenMB(tmp);
							
				var randomnum = Math.floor((Math.random() * 10000) + 1);  
			  //var frmProdIcon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&personalizedId=&billerId=11&modIdentifier=BANKICON&" + randomnum;
				var frmProdIcon=loadBankIcon("11");
			  //var frmProdIcon = "https://vit.tau2904.com:443/tmb/ImageRender?crmId=&&personalizedId=&billerId="+ICON_ID+"&modIdentifier=PRODICON";				
					frmBillPaymentEditFutureNew.imgFrom.src = frmProdIcon;
							//-- New change to handle easy Pass 
							if(gblBPflag == false && gblBillerCompcodeEditMB == "2151"){
                              srvOnlinePaymentInq();
							
							}else{
								//frmBillPaymentEditFutureNew.flxTPCusName.setVisibility(false);
								dismissLoadingScreen();
								checkMBStatusForEditBP();
								//frmBillPaymentEditFutureNew.show();
							}  //--
						}else{
								if(result["custAcctRec"].length == i+1){ 
									if( !accFound){
										dismissLoadingScreen();
										//alert("No Records Found for the given Account num");
									}
								}
							}
						}
					}else{
						dismissLoadingScreen();
						alert(result["StatusDesc"]);
					}

			}else {
				dismissLoadingScreen();
				alert(result["errMsg"]);
				return false;
			}
	} else {
		if (status == 300) {
			dismissLoadingScreen();
			
		}
	}
}

function srvOnlinePaymentInq(){
	var inputparam = [];
	inputparam["rqUID"] = "";
	inputparam["EffDt"] = "";
	inputparam["TrnId"] = ""; // change this value when integrated with calender / future transactions page
	inputparam["Amt"] = gblAmountMB;//"0.00"; /// for billpayment it is 0.00
	inputparam["compCode"] = gblBillerCompcodeEditMB;
	//inputparam["TranCode"] = gblTransCodeMB;
	//inputparam["AcctId"] = gblFromAccIdMB;
	//inputparam["AcctTypeValue"] = gblFromAccTypeMB;
	inputparam["MobileNumber"] = removeHyphenIB(frmBillPaymentEditFutureNew.lblRef1value.text);
	inputparam["BankId"]="011";
	inputparam["BranchId"]= "0001";
	
	
	invokeServiceSecureAsync("onlinePaymentInqForEditBP", inputparam, srvOnlinePaymentInqCallBack);

}

function srvOnlinePaymentInqCallBack(status, result) {
	if (status == 400) {
	
		if (result["opstatus"] == 0) {
			if ( result["OnlinePmtInqRs"] != undefined && result["OnlinePmtInqRs"].length > 0 && result["OnlinePmtInqRs"] != null) {
					if(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length >0 && result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"] != null){
						for(var i=0; i < result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length ;i++ ){
							if(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "CustomerName"){
								if(frmBillPaymentEditFutureNew.lblBillerNickName.text == "")
								frmBillPaymentEditFutureNew.lblBillerNickName.text = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
								gblTPCustomerNameVal = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
								checkMBStatusForEditBP();
							}	
						}
					}
					
			} else {
				dismissLoadingScreen();
				alert(""+result["errMsg"]);
				
			}
		}else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
	   }
	}
}



function addMyAccntBPTPMB(){
  frmMyAccountList.show();  
}

function forBPBillPmtInqMB(EditBillMethodMB) {
	inputParam = [];
	var tranCodeMB;
	if (gblFromAccTypeMB == "DDA") {
		tranCodeMB = "88" + "10";
	} else {
		tranCodeMB = "88" + "20";
	}
	inputParam["postedDate"] = gblstartOnMB; //changeDateFormatForService(gblstartOnMB);
	inputParam["ePayCode"] = "EPYS";
	inputParam["waiveCode"] = "I";
	inputParam["fIIdent"] = "";
	inputParam["tranCode"] = tranCodeMB; // from paymentInq
	inputParam["fromAcctTypeValue"] = gblFromAccTypeMB;
	
	if (EditBillMethodMB == 0){ // Offline Bill Payment
		//MIB-6010 P2P Bill Payment	
		var mobileOrCI = "05";
		inputParam["mobileOrCI"] = mobileOrCI;
		if(gblBillerCompcode.length > 4){
			inputParam["compCode"] = "";		
		}else{
			inputParam["compCode"] = gblBillerCompcode;		
		}
		inputParam["toAcctNo"] = gblBillerTaxID;		
		inputParam["fromAcctNo"] = gblFromAccIdMB;
		inputParam["transferAmt"] = gblAmountMB;		
		inputParam["ref1"] = removeHyphenIB(frmBillPaymentEditFutureNew.lblRef1value.text);
		inputParam["ref2"] = removeHyphenIB(frmBillPaymentEditFutureNew.lblRef2value.text);
		inputParam["ref3"] = "";
		inputParam["ref4"] = "";		
		invokeServiceSecureAsync("promptPayInq", inputParam, forBPpromptPayInqServiceCallBackMB);
	}else{ // Online Bill Payment
		inputParam["compCode"] = gblBillerCompcodeEditMB; //get from masterbillInq
		inputParam["fromAcctIdentValue"] = gblFromAccIdMB;		
		inputParam["transferAmount"] = gblAmountMB;		
		inputParam["pmtRefIdent"] = removeHyphenIB(frmBillPaymentEditFutureNew.lblRef1value.text);
		inputParam["invoiceNumber"] = "";
		invokeServiceSecureAsync("billPaymentInquiry", inputParam, forBPbillPaymentInquiryServiceCallBackMB);	
	}	
}

function forBPpromptPayInqServiceCallBackMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["WaiveFlag"] != null) {
				if (result["WaiveFlag"] == "Y") {
					frmBillPaymentEditFutureNew.lblPaymentFeeValue.text = "0.00";
				} else {
					frmBillPaymentEditFutureNew.lblPaymentFeeValue.text = commaFormatted(result["FeeAmt"]);
				}
				if(result["FeeAmt"] <= 0 || frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
					frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
				}else{
					frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(true);
				}
                if(!gblBPflag){
                  frmBillPaymentEditFutureNew.lblPaymentFeeValueCombo.text = frmBillPaymentEditFutureNew.lblPaymentFeeValue.text;
                  if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
                  	frmBillPaymentEditFutureNew.lblFeeCombo.setVisibility(false);
                    frmBillPaymentEditFutureNew.flxFeeCombo.setVisibility(false);
                  }else{
                    frmBillPaymentEditFutureNew.lblFeeCombo.setVisibility(true);
                    frmBillPaymentEditFutureNew.flxFeeCombo.setVisibility(true);
                  }
                }
                frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
				forBPCallCustomerAccountInqMB();
			} else {
				dismissLoadingScreen();
				alert(result["StatusDesc"]);
			}
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	} else {
		if (status == 300) {
			
		}
	}
}

function forBPbillPaymentInquiryServiceCallBackMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["BillPmtInqRs"].length > 0 && result["BillPmtInqRs"] != null) {
				if (result["BillPmtInqRs"][0]["WaiveFlag"] == "Y") {
					frmBillPaymentEditFutureNew.lblPaymentFeeValue.text = "0.00";
				} else {
					frmBillPaymentEditFutureNew.lblPaymentFeeValue.text = commaFormatted(result["BillPmtInqRs"][0]["FeeAmount"]);
				}
				if(result["BillPmtInqRs"][0]["FeeAmount"] <= 0 || frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
					frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
				}else{
					frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(true);
				}
                if(!gblBPflag){
                  frmBillPaymentEditFutureNew.lblPaymentFeeValueCombo.text = frmBillPaymentEditFutureNew.lblPaymentFeeValue.text;
                  if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
                  	frmBillPaymentEditFutureNew.lblFeeCombo.setVisibility(false);
                    frmBillPaymentEditFutureNew.flxFeeCombo.setVisibility(false);
                  }else{
                    frmBillPaymentEditFutureNew.lblFeeCombo.setVisibility(true);
                    frmBillPaymentEditFutureNew.flxFeeCombo.setVisibility(true);
                  }
                }
                frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
				forBPCallCustomerAccountInqMB();
			} else {
				dismissLoadingScreen();
				alert(result["StatusDesc"]);
			}
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	} else {
		if (status == 300) {
			
		}
	}
}

function checkMBStatusForEditBP() {
	var inputparam = [];
	inputparam["crmId"] = "";
	inputparam["rqUUId"] = "";
	showLoadingScreen();	
	invokeServiceSecureAsync("crmProfileInq", inputparam, callBackCrmProfileInqForEditBPMB);
}
/*
   **************************************************************************************
		Module	: callBackCrmProfileInqForEditBPMB
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackCrmProfileInqForEditBPMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var statusCode = result["statusCode"];
			var severity = result["Severity"];
			var statusDesc = result["StatusDesc"];
			if (statusCode != 0) {
				
			} else {
				var mbStat = result["mbFlowStatusIdRs"];
				var ibStat = result["ibUserStatusIdTr"];
				if (mbStat != null) {
					var inputParam = [];
					inputParam["mbStatus"] = mbStat; //check this inputparam
					inputParam["ibStatus"]= ibStat;
					invokeServiceSecureAsync("checkIBMBStatus", inputParam, callBackCheckStatusMBForEditBP);
				}
			}
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreen();
		}
	}
}
/*
   **************************************************************************************
		Module	: callBackCheckStatusMBForEditBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackCheckStatusMBForEditBP(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
                if (result["mbStatusFlag"] == "true") {
                     dismissLoadingScreen();
                     popTransferConfirmOTPLock.show();
                    return false;
                } else {
                    onClickEditButtonMB();
                }
        } else {
            dismissLoadingScreen();
        }
        //
    } else {
        if (status == 300) {
            dismissLoadingScreen();
            
        }
    }
}

// On click Edit button

function onClickEditButtonMB() {
  	repeatAsMB = "";
  	endFreqSaveMB = "";
  	gblTemprepeatAsTPMB = "";
	gblTempendFreqSaveTPMB = "";
    if(gblBPflag){
		doEditBillPayment();
    }else{
		doEditTopUp();
   }
}

function doEditBillPayment(){
		frmBillPaymentEditFutureNew.lblHead.text = kony.i18n.getLocalizedString("keyEditBillPaymentMB");
		frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
  		frmBillPaymentEditFutureNew.lblScheduleEndContainerHidden.text = "";
  		frmBillPaymentEditFutureNew.lblScheduleEndContainerHidden.setVisibility(false);
  
		frmBillPaymentEditFutureNew.flxAmountSelect.setVisibility(false);
    	if (gblEditBillMethodMB == 0 && gblEditBillerGroupTypeMB == 0) {
        	//New changes to handle visibility as per biller type
			frmBillPaymentEditFutureNew.flxFullSpecButton.setVisibility(false); // two option
			frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true); // to display txtAmntInput amount
			frmBillPaymentEditFutureNew.flxFullMinSpecButtons.setVisibility(false);		//3 buttons bbox
            frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
        	dismissLoadingScreen();
        	frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
				turnOnOrOffAction(false);
			}else{
				turnOnOrOffAction(true);
			}
          	if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
				frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
			}
	       	frmBillPaymentEditFutureNew.show();
    	} else if (gblEditBillMethodMB == 1 && gblEditBillerGroupTypeMB == 0) {
        	var inputparam = [];
        	inputparam["rqUID"] = "";
        	inputparam["EffDt"] = "";
        	inputparam["TrnId"] = ""; // change this value when integrated with calender / future transactions page
        	inputparam["Amt"] = "0.00"; /// for billpayment it is 0.00
        	inputparam["compCode"] = gblBillerCompcodeEditMB;
        	inputparam["MobileNumber"] = removeHyphenIB(frmBillPaymentEditFutureNew.lblRef1value.text);
			inputparam["BankId"]="011";
			inputparam["BranchId"]= "0001";
        	invokeServiceSecureAsync("onlinePaymentInqForEditBP", inputparam, forBPMBOnlinePaymentInquiryServiceCallBack);
    	} else if (gblEditBillMethodMB == 2 && gblEditBillerGroupTypeMB == 0) {
       	 	var inputparam = [];
        	inputparam["cardId"] = gblRef1ValueBPMB; //"00000000"+removeHyphenIB(frmBillPaymentView.lblRef1value.text);
        	inputparam["waiverCode"] = "I";
        	inputparam["tranCode"] = TRANSCODEMIN; //"8290";
        	inputparam["postedDt"] = getTodaysDate(); // current date you inq for smt
        	inputparam["rqUUId"] = "";
        	var cardId  = gblRef1ValueBPMB;
        	gblownCardBPMB = false;
        	var accountLength =gblAccountTable["custAcctRec"].length;
    		for(var i=0;i < accountLength;i++){
				if(gblAccountTable["custAcctRec"][i].accType=="CCA"){
					var accountNo = gblAccountTable["custAcctRec"][i].accId;
					accountNo = accountNo.substring(accountNo.length-16 , accountNo.length);
					if(accountNo == cardId){
						gblownCardBPMB =true;
						break;
					}
				}
    		}
			frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
        	if(gblownCardBPMB){
               //alert("creditcardDetailsInq");
        		invokeServiceSecureAsync("creditcardDetailsInq", inputparam, forBPMBCreditCardDetailsInqServiceCallBack);
        	}else{
        		frmBillPaymentEditFutureNew.flxFullMinSpecButtons.setVisibility(false);
				frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true);
				frmBillPaymentEditFutureNew.flxFullSpecButton.setVisibility(false); 
				dismissLoadingScreen();	
				frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
	        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
					turnOnOrOffAction(false);
				}else{
					turnOnOrOffAction(true);
				}
              	if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
                    frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
                }
				frmBillPaymentEditFutureNew.show();
              	frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
          		frmBillPaymentEditFutureNew.lblHead.setFocus(true);
        	}
        	
    	} else if (gblEditBillMethodMB == 3) {
			frmBillPaymentEditFutureNew.flxFullMinSpecButtons.setVisibility(false);
			frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true);
            frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
        	var inputparam = [];
        	var ref2 = frmBillPaymentEditFutureNew.lblRef2value.text
        	var acctId = "0"+removeHyphenIB(frmBillPaymentEditFutureNew.lblRef1value.text)+ref2;
        	inputparam["acctId"] = "0"+removeHyphenIB(frmBillPaymentEditFutureNew.lblRef1value.text)+ref2;
        	inputparam["acctType"] = "LOC";   //kony.i18n.getLocalizedString("Loan");
        	inputparam["rqUUId"] = "";
        	var ownAccnt = false;
        	var accountLength =gblAccountTable["custAcctRec"].length;
    		for(var i=0;i < accountLength;i++){
				if(gblAccountTable["custAcctRec"][i].accType=="LOC"){
					var accountNo = gblAccountTable["custAcctRec"][i].accId;
					if(accountNo == acctId){
						ownAccnt =true;
						break;
					}
				}
    		}
        	if(ownAccnt){
	        	invokeServiceSecureAsync("doLoanAcctInq", inputparam, forBPMBLoanAccountInquiryServiceCallBack);
        	}else{
				frmBillPaymentEditFutureNew.flxFullSpecButton.setVisibility(false); // to display buttons full/spec
				frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true);
        		frmBillPaymentEditFutureNew.txtAmtInput.setVisibility(true);
        		frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(true);
        		frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
				dismissLoadingScreen();
				frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
	        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
					turnOnOrOffAction(false);
				}else{
					turnOnOrOffAction(true);
				}
              	if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
                	frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
                }
				frmBillPaymentEditFutureNew.show();
        	}
        	
    	}else{
    		dismissLoadingScreen();
    		alert("you can't edit this Bill Payment ");
    	}
}

function formatEditBillPayAmountOnTextChange(){
	var enteredAmount = frmBillPaymentEditFutureNew.txtAmtInput.text;
	if(isNotBlank(enteredAmount)) {
		enteredAmount = kony.string.replace(enteredAmount, ",", "");
		if(!(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0)){
			frmBillPaymentEditFutureNew.txtAmtInput.text = commaFormattedTransfer(enteredAmount);
		}
	}
}

function frmBillPaymentEditFutureNewMenuPreshow() {
  	//mki, mib-11688 Start
  	//#ifdef android    
	frmBillPaymentEditFutureNew.flexBodyScroll.showFadingEdges  = false;
	//#endif  
	//mki, mib-11688 End
  
	if(gblCallPrePost)
	{
	    frmBillPaymentEditFutureNewPreShow.call(this);
	    isMenuShown = false;
	    isSignedUser = true;
	}
}

function onDoneEditingBillPaymentOnDone(){
	frmBillPaymentEditFutureNew.txtAmtInput.text = onDoneEditingAmountValue(frmBillPaymentEditFutureNew.txtAmtInput.text);
    frmBillPaymentEditFutureNew.lblHead.setFocus(true);
}

function onClickFullPayment(){
 
 if(gblEditBillMethodMB == 3 && gblEditBillerGroupTypeMB == 0){
 	frmBillPaymentEditFutureNew.txtAmtInput.text = fullAmtLoanMB;
 } else if(gblEditBillMethodMB == 1 && gblEditBillerGroupTypeMB == 0){
 frmBillPaymentEditFutureNew.txtAmtInput.text = commaFormatted(gblFullAmtVal);//gblFullAmtVal; //fullAmtValueOnline;
 }
 frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(false);
 frmBillPaymentEditFutureNew.txtAmtInput.skin = "lblDarkGrey48px";
}

function onClickFullPaymentThreeOp(){
 frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(false);
 frmBillPaymentEditFutureNew.txtAmtInput.text = gblMaxAmount;
 frmBillPaymentEditFutureNew.txtAmtInput.skin = "lblDarkGrey48px";
}

function onClickMinimumPaymentThreeOp(){
 frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(false);
 frmBillPaymentEditFutureNew.txtAmtInput.text = gblMinAmount;
 frmBillPaymentEditFutureNew.txtAmtInput.skin = "lblDarkGrey48px";
}

function onClickFullMinSpecColorBarSet(eventObject) {
  
  	frmBillPaymentEditFutureNew.btnFull1.skin = "btnScheduleEndLeft128";
  	frmBillPaymentEditFutureNew.btnMinimum1.skin = "btnScheduleEndMid";
	frmBillPaymentEditFutureNew.btnSpecified1.skin = "btnScheduleEndRight128";
	var buttonId = eventObject.id;
	if (kony.string.equalsIgnoreCase(buttonId, "btnFull1")) {
		eventObject.skin = "btnScheduleEndLeftFocus128";
	} else if (kony.string.equalsIgnoreCase(buttonId, "btnMinimum1")) {
		eventObject.skin = "btnScheduleEndMidFocus";
	} else if (kony.string.equalsIgnoreCase(buttonId, "btnSpecified1")) {
		eventObject.skin = "btnScheduleEndRightFocus128";
	}
}

function onClickSpecifiedPayment(){
  	frmBillPaymentEditFutureNew.txtAmtInput.skin = "txtBlueNormal200";
	frmBillPaymentEditFutureNew.txtAmtInput.setVisibility(true);
	frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(true);
	frmBillPaymentEditFutureNew.txtAmtInput.setFocus(true);
}

function onClickFullSpecColorBarSet(eventObject) {
	frmBillPaymentEditFutureNew.btnFull2.skin = "btnScheduleEndLeft128";
	frmBillPaymentEditFutureNew.btnSpecified2.skin = "btnScheduleEndRight128";
	var buttonId = eventObject.id;
	if (kony.string.equalsIgnoreCase(buttonId, "btnFull2")) {
		eventObject.skin = "btnScheduleEndLeftFocus128";
	} else if (kony.string.equalsIgnoreCase(buttonId, "btnSpecified2")) {
		eventObject.skin = "btnScheduleEndRightFocus128";
	}
}

function doEditTopUp(){
	 //Topup
	frmBillPaymentEditFutureNew.lblHead.text = kony.i18n.getLocalizedString("KeyMBEditTopUP");//"Edit Top UP";
	frmBillPaymentEditFutureNew.flxAmountSelect.setVisibility(false);
	frmBillPaymentEditFutureNew.flxFullMinSpecButtons.setVisibility(false);
	frmBillPaymentEditFutureNew.flxFullSpecButton.setVisibility(false); 
	frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
	frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true);
	frmBillPaymentEditFutureNew.lblScheduleEndContainerHidden.text = "";
  	frmBillPaymentEditFutureNew.lblScheduleEndContainerHidden.setVisibility(false);
		frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
	    if (gblEditBillMethodMB == 0 && gblEditBillerGroupTypeMB == 1) {
        	frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;        	
        	frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(true);
        	frmBillPaymentEditFutureNew.txtAmtInput.setVisibility(true);
        	frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
				turnOnOrOffAction(false);
			}else{
				turnOnOrOffAction(true);
			}
          	if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
				frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
			}
        	frmBillPaymentEditFutureNew.show();
          	frmBillPaymentEditFutureNew.lblHead.setFocus(true);
          	dismissLoadingScreen();
    	} else if (gblEditBillMethodMB == 1 && gblEditBillerGroupTypeMB == 1) {

          	if(gblBillerCompcodeEditMB == "2704"){
              	frmBillPaymentEditFutureNew.flxAmountSelect.setVisibility(false);
       			frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true);
            	frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
            }else{
              	frmBillPaymentEditFutureNew.flxAmountSelect.setVisibility(true);
       			frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(false);
              	frmBillPaymentEditFutureNew.btnTopUpAmountComboBox.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
            }
       		
        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
				turnOnOrOffAction(false);
			}else{
				turnOnOrOffAction(true);
			}
          	if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
				frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
			}
        	frmBillPaymentEditFutureNew.show();
          	frmBillPaymentEditFutureNew.lblHead.setFocus(true);
          	dismissLoadingScreen();
   	 	} else if (gblEditBillMethodMB == 2 && gblEditBillerGroupTypeMB == 1) {
        	frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
      		frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(true);
        	frmBillPaymentEditFutureNew.txtAmtInput.setVisibility(true);
        	frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
				turnOnOrOffAction(false);
			}else{
				turnOnOrOffAction(true);
			}
          	if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
				frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
			}
        	frmBillPaymentEditFutureNew.show();
          	frmBillPaymentEditFutureNew.lblHead.setFocus(true);
          	dismissLoadingScreen();
    	} else{
    		dismissLoadingScreen();
    		alert("you can't edit this transaction ");
    	}
}

function forBPMBOnlinePaymentInquiryServiceCallBack(status, result) {
	gblMinAmount = 0;
	gblMaxAmount = 0;
	gblPayFull = "";
	if (status == 400) {	
		if (result["opstatus"] == 0) {
			if ( result["OnlinePmtInqRs"] != undefined && result["OnlinePmtInqRs"].length > 0 && result["OnlinePmtInqRs"] != null) {
				activityStatus = "00";
				frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(true);	// to display penalty amount
				frmBillPaymentEditFutureNew.flxFullSpecButton.setVisibility(true); // to display buttons
				frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true); // to display total amount
				frmBillPaymentEditFutureNew.flxFullMinSpecButtons.setVisibility(false);
  				frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
				var local = kony.i18n.getCurrentLocale();
				var fullAmtValueOnline = "";
				if(result["OnlinePmtMiscDataDisp"].length >0 && result["OnlinePmtMiscDataDisp"] != null){
					for(i=0; i < result["OnlinePmtMiscDataDisp"].length ;i++ ){
						if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "BilledAmt"){
							var billedAmtLabelEN = result["OnlinePmtMiscDataDisp"][i]["MiscName_EN"];
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "FullAmt"){
							fullAmtValueOnline = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "PenaltyAmt"){
							var penaltyAmtLabelEN = result["OnlinePmtMiscDataDisp"][i]["MiscName_EN"];
							var penaltyAmtLabelTH = result["OnlinePmtMiscDataDisp"][i]["MiscName_TH"];
							var penaltyAmtValue = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
							if (local == "en_US") {
								frmBillPaymentEditFutureNew.labelPenaltyAmt.text = penaltyAmtLabelEN;
							}else if(local == "th_TH"){
								frmBillPaymentEditFutureNew.labelPenaltyAmt.text = penaltyAmtLabelTH;
							}
							frmBillPaymentEditFutureNew.labelPenaltyAmtValue.text = commaFormatted(penaltyAmtValue);
							if(penaltyAmtValue <= 0){
								frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
							}else{
								frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(true);
							}
						}//Getting Min Max amount from the service 
						else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "Minimum"){
							gblMinAmount = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "Maximum"){
							gblMaxAmount = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
						}   
					}
				}else{
					//New 
						
					var arrayTemp = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"];
					if(arrayTemp != undefined && arrayTemp.length > 0){
						for(var i=0; i < arrayTemp.length ;i++ ){
							if(arrayTemp[i]["MiscName"] == "BilledAmt"){
								var billedAmtValue = arrayTemp[i]["MiscText"];
								frmBillPaymentEditFutureNew.txtAmtInput.text = commaFormatted(billedAmtValue);
							}else if(arrayTemp[i]["MiscName"] == "FullAmt"){
								fullAmtValueOnline = arrayTemp[i]["MiscText"];
							}else if(arrayTemp[i]["MiscName"] == "PenaltyAmt"){
								var penaltyAmtValue = arrayTemp[i]["MiscText"];
								frmBillPaymentEditFutureNew.labelPenaltyAmtValue.text = commaFormatted(penaltyAmtValue);
								if(penaltyAmtValue <= 0){
									frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(false);
								}else{
									frmBillPaymentEditFutureNew.flxPenaltyAmt.setVisibility(true);
								}
							}//Get the min max value here
							else if(arrayTemp[i]["MiscName"] == "Minimum"){
								gblMinAmount = arrayTemp[i]["MiscText"];
							}else if(arrayTemp[i]["MiscName"] == "Maximum"){
								gblMaxAmount = arrayTemp[i]["MiscText"];
							}   
						}
					}	
					//	
				}
				gblFullAmtVal = fullAmtValueOnline;
				if(parseFloat(fullAmtValueOnline.toString()) == parseFloat(frmBillPaymentEditFutureNew.lblamtvalue.text)){
					frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
					frmBillPaymentEditFutureNew.btnFull2.skin = 	"btnScheduleEndLeftFocus128";
					frmBillPaymentEditFutureNew.btnSpecified2.skin ="btnScheduleEndRight128";
					frmBillPaymentEditFutureNew.txtAmtInput.skin = "lblDarkGrey48px";
					frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(false);
					
				}else{
					frmBillPaymentEditFutureNew.btnFull2.skin = "btnScheduleEndLeft128";
					frmBillPaymentEditFutureNew.btnSpecified2.skin ="btnScheduleEndRightFocus128";
  					frmBillPaymentEditFutureNew.txtAmtInput.skin = "txtBlueNormal200";
					frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
					//frmBillPaymentEdit.txtamountvalue.text = frmBillPaymentView.lblamtvalue.text;
					frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(true);
					//frmBillPaymentEdit.txtamountvalue.setVisibility(true);
					frmBillPaymentEditFutureNew.txtAmtInput.setVisibility(true);
				}
				
				gblPayFull = result["isFullPayment"];
				if(gblPayFull == "Y") {
					frmBillPaymentEditFutureNew.flxFullSpecButton.setVisibility(false);
					frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(false);
				}
				dismissLoadingScreen();
				frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
	        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
					turnOnOrOffAction(false);
				}else{
					turnOnOrOffAction(true);
				}
                if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
                    frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
                }
                frmBillPaymentEditFutureNew.show();
                frmBillPaymentEditFutureNew.lblHead.setFocus(true);
                dismissLoadingScreen();
			} else {
				dismissLoadingScreen();
				alert(result["errMsg"]);
			}

		}else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	}
}

function forBPMBCreditCardDetailsInqServiceCallBack(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			activityStatus = "00";
			gblMinAmount = 0;
			gblMaxAmount = 0;
			
			//
			if (result["fullPmtAmt"] != "0.00" && result["fullPmtAmt"] != undefined) {
				gblMaxAmount = commaFormatted(result["fullPmtAmt"]);
				if(parseInt(gblMaxAmount)<0)gblPaymentOverpaidFlag=true;
			} else {
				gblMaxAmount = result["fullPmtAmt"];
			}
			if (result["minPmtAmt"] != "0.00" && result["minPmtAmt"] != undefined) {
				gblMinAmount = commaFormatted(result["minPmtAmt"]);
			} else {
				gblMinAmount = result["minPmtAmt"];
			}
			frmBillPaymentEditFutureNew.flxFullMinSpecButtons.setVisibility(true);
			frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true);
			frmBillPaymentEditFutureNew.flxFullSpecButton.setVisibility(false);
			frmBillPaymentEditFutureNew.btnFull1.skin = btnScheduleEndLeft128;
			frmBillPaymentEditFutureNew.btnMinimum1.skin = btnScheduleEndMid;
			frmBillPaymentEditFutureNew.btnSpecified1.skin = btnScheduleEndRightFocus128;
			//enable specified button and amt text
			frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(true);
			frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
			dismissLoadingScreen();	
			frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
				turnOnOrOffAction(false);
			}else{
				turnOnOrOffAction(true);
			}
          	if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
				frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
			}
        	frmBillPaymentEditFutureNew.show();
          	frmBillPaymentEditFutureNew.lblHead.setFocus(true);
          	dismissLoadingScreen();
			paymentOverpaidCheck();
		}else{
			dismissLoadingScreen();
			alert(result["errMsg"]);
			return false;
		}
	}
}

function forBPMBLoanAccountInquiryServiceCallBack(status,result){


	if(status == 400){
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmBillPaymentEditFutureNew.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentEditFutureNew.lblRef1value.text);    //Biller Name + Ref1
		var activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB; //Old Amount + New Amount (Edit case)		
       	var logLinkageId = "";
		if(result["opstatus"] == 0){
			activityStatus = "00";
			// display full and specified amt
			fullAmtLoanMB = "";
			if (result["regPmtCurAmt"] != "0.00") {
				fullAmtLoanMB = commaFormatted(result["regPmtCurAmt"]);
			} else {
				fullAmtLoanMB = result["regPmtCurAmt"];
			}
			frmBillPaymentEditFutureNew.flxFullSpecButton.setVisibility(true); // to display buttons full/spec
			frmBillPaymentEditFutureNew.flxAmountInput.setVisibility(true);			//hbox for txtAmtInput	

			var fulAmt = fullAmtLoanMB;
			var amt = frmBillPaymentEditFutureNew.lblamtvalue.text;
			fulAmt = fulAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(fulAmt.indexOf(",") != -1)fulAmt = replaceCommon(fulAmt, ",", "");
			if(amt.indexOf(",") != -1)amt = replaceCommon(amt, ",", "");
			
			
			
			if(parseFloat(fulAmt) == parseFloat(amt)){
				frmBillPaymentEditFutureNew.btnFull2.skin = "btnScheduleEndLeftFocus128";
				frmBillPaymentEditFutureNew.btnSpecified2.skin ="btnScheduleRight128";
				frmBillPaymentEditFutureNew.txtAmtInput.skin = "lblDarkGrey48px";
			}else{
				frmBillPaymentEditFutureNew.btnFull2.skin = "btnScheduleEndLeft128";
				frmBillPaymentEditFutureNew.btnSpecified2.skin ="btnScheduleRightFocus128";
				frmBillPaymentEditFutureNew.txtAmtInput.skin = "txtBlueNormal200";
			}
			frmBillPaymentEditFutureNew.txtAmtInput.text = frmBillPaymentEditFutureNew.lblamtvalue.text;
			frmBillPaymentEditFutureNew.txtAmtInput.setEnabled(false);
			dismissLoadingScreen();
			frmBillPaymentEditFutureNew.txtAmtInput.setFocus(false);
        	if (frmBillPaymentEditFutureNew.flxDelMessage.isVisible || gblBillerStatus == "0") {
				turnOnOrOffAction(false);
			}else{
				turnOnOrOffAction(true);
			}
          	if(frmBillPaymentEditFutureNew.lblPaymentFeeValue.text == "0.00"){
				frmBillPaymentEditFutureNew.flxFeeBox.setVisibility(false);
			}
        	frmBillPaymentEditFutureNew.show();
          	frmBillPaymentEditFutureNew.lblHead.setFocus(true);
          	dismissLoadingScreen();
		}else {
			activityStatus = "02";
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	}
}

function onClickScheduleButtonMB() {
	
	
	frmScheduleBillPayEditFuture.calScheduleStartDate.dateComponents = dateFormatForDateComp(gblTempTPStartOnDateMB);
	frmScheduleBillPayEditFuture.calScheduleStartDate.validStartDate = currentDateForcalender();
	frmScheduleBillPayEditFuture.calScheduleEndDate.validStartDate = currentDateForcalender();
	
	if (repeatAsMB == "" && endFreqSaveMB == "") {
		onLoadShowRepeatFreqMB(gblOnLoadRepeatAsMB);
		onLoadShowEndFreqMB(gblEndingFreqOnLoadMB);
	} else {
		onLoadShowRepeatFreqMB(repeatAsMB);
		onLoadShowEndFreqMB(endFreqSaveMB);
	}
	
	//New
	gblTemprepeatAsTPMB = repeatAsMB;
	gblTempendFreqSaveTPMB = endFreqSaveMB;
	
	frmScheduleBillPayEditFuture.show();
}

function onClickSchedueEditBillpay(){
  	gblreccuringDisablePay = "N";
  	gblreccuringDisableAdd = "N";

	gblTempTPStartOnDateMB = frmBillPaymentEditFutureNew.lblStartDate.text;
	gblTempTPEndOnDateMB = frmBillPaymentEditFutureNew.lblEndOnDate.text;
	gblTempTPRepeatAsMB = frmBillPaymentEditFutureNew.lblExecutetimes.text;
  	//frmSchedue setup
  	gblStartBPDate = gblTempTPStartOnDateMB;
  	gblEndBPDate = gblTempTPEndOnDateMB;
  	gblNumberOfDays = gblTempTPRepeatAsMB;
  
	frmSchedule.calScheduleStartDate.dateComponents = dateFormatForDateComp(gblTempTPStartOnDateMB);
	frmSchedule.calScheduleStartDate.validStartDate = currentDateForcalender();
	frmSchedule.calScheduleEndDate.validStartDate = currentDateForcalender();
  	
	if(gblTempTPRepeatAsMB != "" && gblTempTPRepeatAsMB != ""){
      frmSchedule.tbxAfterTimes.text = gblTempTPRepeatAsMB;
    }
     	
	
	if (repeatAsMB == "" && endFreqSaveMB == "") {

      	gblScheduleRepeatBPTmp = gblOnLoadRepeatAsMB;
        gblScheduleRepeatBP = gblOnLoadRepeatAsMB;
      	gblScheduleEndBPTmp = gblEndingFreqOnLoadMB;
        gblScheduleEndBP = gblEndingFreqOnLoadMB;
      
		onLoadShowRepeatFreqMB(gblOnLoadRepeatAsMB);
		onLoadShowEndFreqMB(gblEndingFreqOnLoadMB);
	} else {
      	gblScheduleRepeatBPTmp = repeatAsMB;
        gblScheduleRepeatBP = repeatAsMB;
        gblScheduleEndBPTmp = endFreqSaveMB;
        gblScheduleEndBP = endFreqSaveMB;
      
		onLoadShowRepeatFreqMB(repeatAsMB);
		onLoadShowEndFreqMB(endFreqSaveMB);
	}
	//New
	gblTemprepeatAsTPMB = repeatAsMB;
	gblTempendFreqSaveTPMB = endFreqSaveMB;
	frmSchedule.show();
}

function onLoadShowEndFreqMB(endingFreqMB) {
  	var currentForm;
    currentForm = frmSchedule;
	if(OnClickRepeatAsMB == "Once"){
		disableEndAfterBtnHolderBPEdit();
	}else{
		if ((endingFreqMB == kony.i18n.getLocalizedString("keyOnDate")) || (endingFreqMB == "On Date") || (endingFreqMB == "OnDate")) { //kony.i18n.getLocalizedString("keyOnDate")
			endingFreqMB = "OnDate";
          	currentForm.calScheduleEndDate.dateComponents = dateFormatForDateComp(gblTempTPEndOnDateMB);
			setFocusOnEndOnDateBPEdit();
		} else if ((endingFreqMB == kony.i18n.getLocalizedString("keyAfter")) || (endingFreqMB == "After")) { //kony.i18n.getLocalizedString("keyAfter")
			currentForm.tbxAfterTimes.text = frmBillPaymentEditFutureNew.lblExecutetimes.text;
			setFocusOnEndAfterBPEdit();
		} else if ((endingFreqMB == kony.i18n.getLocalizedString("keyNever"))|| (endingFreqMB == "Never")) { //kony.i18n.getLocalizedString("keyNever")
			setFocusOnEndNeverBPEdit();
		}
	}
}

function onLoadShowRepeatFreqMB(repeatFreqMB) {
  		var currentForm;
        currentForm = frmSchedule;
		disableRepeatBtnHolderBPEdit();
		setRepeatClickedToFalse();
	if (kony.string.equalsIgnoreCase(repeatFreqMB, kony.i18n.getLocalizedString("keyDaily")) || kony.string.equalsIgnoreCase(repeatFreqMB, "Daily")) { //kony.i18n.getLocalizedString("keyDaily")
		currentForm.lblEnd.setVisibility(true);
		currentForm.hbxEndAfterButtonHolder.setVisibility(true);
		currentForm.btnDaily.skin = "btnScheduleLeftFocus";
		OnClickRepeatAsMB = "Daily";
		DailyClicked = true;
	} else if (kony.string.equalsIgnoreCase(repeatFreqMB, kony.i18n.getLocalizedString("keyWeekly")) || kony.string.equalsIgnoreCase(repeatFreqMB, "Weekly")) { //kony.i18n.getLocalizedString("keyWeekly")
		currentForm.lblEnd.setVisibility(true);
		currentForm.hbxEndAfterButtonHolder.setVisibility(true);
		currentForm.btnWeekly.skin = "btnScheduleMidFocus";
		OnClickRepeatAsMB = "Weekly";
		weekClicked = true;
	} else if (kony.string.equalsIgnoreCase(repeatFreqMB, kony.i18n.getLocalizedString("keyMonthly"))|| kony.string.equalsIgnoreCase(repeatFreqMB, "Monthly")) { //kony.i18n.getLocalizedString("keyMonthly")
		currentForm.lblEnd.setVisibility(true);
		currentForm.hbxEndAfterButtonHolder.setVisibility(true);
		currentForm.btnMonthly.skin = "btnScheduleMidFocus";
		OnClickRepeatAsMB = "Monthly";
		monthClicked = true;
	} else if (kony.string.equalsIgnoreCase(repeatFreqMB, kony.i18n.getLocalizedString("keyYearly")) || kony.string.equalsIgnoreCase(repeatFreqMB, "Yearly")) { //kony.i18n.getLocalizedString("keyYearly")
		currentForm.lblEnd.setVisibility(true);
		currentForm.hbxEndAfterButtonHolder.setVisibility(true);
		currentForm.btnYearly.skin = "btnScheduleRightFocus";
		OnClickRepeatAsMB = "Yearly";
		YearlyClicked = true;
	} else {
		OnClickRepeatAsMB = "Once";
      	currentForm.btnDaily.skin = "btnScheduleLeft";
      	currentForm.btnWeekly.skin = "btnScheduleMid";
      	currentForm.btnMonthly.skin = "btnScheduleMid";
      	currentForm.btnYearly.skin = "btnScheduleRight";
		currentForm.lblEnd.setVisibility(false);
		currentForm.hbxEndAfterButtonHolder.setVisibility(false);
		currentForm.hbxEndAfter.setVisibility(false);
	}
}

function dateFormatForDateComp(date) {
	var currDate = new Date(GLOBAL_TODAY_DATE);
   	var currDate_yyyy = currDate.getFullYear();
   	var diff_yyyy = 0;
   	
	//Day format 12/07/2013
	var dateSplit = date.split("/");
	var dd = dateSplit[0];
	var mm = dateSplit[1];
	var yyyy = dateSplit[2];
	diff_yyyy = dateSplit[2] - currDate_yyyy;
	//var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
     	var returnedValue = iPhoneCalendar.getDeviceDateLocale();
       	var deviceDate = returnedValue.split("|")[0];
       	var deviceDate_yyyy = deviceDate.substr(0,4);
       	yyyy = parseInt(deviceDate_yyyy) + diff_yyyy;
    }
	var formattedDate = [dd, mm, yyyy, "0", "0", "0"]; // discuss about this format display 
	return formattedDate;
}
var OnClickRepeatAsMB = "";
var OnClickEndFreqMB = "";

function repeatScheduleFrequencyMB(eventObject) {
	var btnEventId = eventObject.id;
	if (kony.string.equalsIgnoreCase(btnEventId, "btnDaily")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleLeftFocus")){
			frmScheduleBillPayEditFuture.btnDaily.skin = "btnScheduleLeft";
			disableRepeatBtnHolderBPEdit();
			disableEndAfterBtnHolderBPEdit();
			setRepeatClickedToFalse();
		}else{
			if(isScheduleFirstShow()){
    			enableEndAfterBtnHolderBPEdit();
    		}
			disableRepeatBtnHolderBPEdit();
			frmScheduleBillPayEditFuture.btnDaily.skin = "btnScheduleLeftFocus";
			OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyDaily");
			DailyClicked = true;
		}
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnWeekly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleMidFocus")){
			frmScheduleBillPayEditFuture.btnDaily.skin = "btnScheduleMid";
			disableRepeatBtnHolderBPEdit();
			disableEndAfterBtnHolderBPEdit();
			setRepeatClickedToFalse();
		}else{
			if(isScheduleFirstShow()){
    			enableEndAfterBtnHolderBPEdit();
    		}
			disableRepeatBtnHolderBPEdit();
			frmScheduleBillPayEditFuture.btnWeekly.skin = "btnScheduleMidFocus";
			OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyWeekly");
			weekClicked = true;
		}
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnMonthly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleMidFocus")){
			frmScheduleBillPayEditFuture.btnMonthly.skin = "btnScheduleMid";
			disableRepeatBtnHolderBPEdit();
			disableEndAfterBtnHolderBPEdit();
			setRepeatClickedToFalse();
		}else{
			if(isScheduleFirstShow()){
    			enableEndAfterBtnHolderBPEdit();
    		}
			disableRepeatBtnHolderBPEdit();
			frmScheduleBillPayEditFuture.btnMonthly.skin = "btnScheduleMidFocus";
			OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyMonthly");
			monthClicked = true;
		}
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnYearly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleRightFocus")){
			frmScheduleBillPayEditFuture.btnYearly.skin = "btnScheduleRight";
			disableRepeatBtnHolderBPEdit();
			disableEndAfterBtnHolderBPEdit();
			setRepeatClickedToFalse();
		}else{
			if(isScheduleFirstShow()){
    			enableEndAfterBtnHolderBPEdit();
    		}
			disableRepeatBtnHolderBPEdit();
			frmScheduleBillPayEditFuture.btnYearly.skin = "btnScheduleRightFocus";
			OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyYearly");
			YearlyClicked = true;
		}
	}
}

function endingScheduleFrequencyMB(eventObject) {
	var btnEventId = eventObject.id;
	disableEndAfterBtnHolderBPEdit();
	if (kony.string.equalsIgnoreCase(btnEventId, "btnNever")) {
		setFocusOnEndNeverBPEdit();
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnAfter")) {
		setFocusOnEndAfterBPEdit();
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnOnDate")) {
		setFocusOnEndOnDateBPEdit();
	}
}

function enableEndAfterBtnHolderBPEdit(){
	setFocusOnEndNeverBPEdit();
}
function setFocusOnEndNeverBPEdit(){
	OnClickEndFreqMB = "Never";
    var currentForm;
    currentForm = frmSchedule;

	currentForm.tbxAfterTimes.text = "";
	currentForm.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(currentForm.calScheduleStartDate.dateComponents);
	currentForm.lblEnd.setVisibility(true);
	currentForm.hbxEndAfterButtonHolder.setVisibility(true);
	currentForm.btnNever.skin = "btnScheduleEndLeftFocus";
	currentForm.btnAfter.skin = "btnScheduleEndMid";
	currentForm.btnOnDate.skin = "btnScheduleEndRight";
	
}
function setFocusOnEndAfterBPEdit(){
  	var currentForm;
    currentForm = frmSchedule;
	OnClickEndFreqMB =  "After";
	currentForm.lblEnd.setVisibility(true);
	currentForm.hbxEndAfter.setVisibility(true);
	currentForm.hbxEndAfterButtonHolder.setVisibility(true);
	currentForm.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(currentForm.calScheduleStartDate.dateComponents);
	
  	currentForm.btnNever.skin = "btnScheduleEndLeft";
	currentForm.btnAfter.skin = "btnScheduleEndMidFocus";
	currentForm.btnOnDate.skin = "btnScheduleEndRight";	
}
function setFocusOnEndOnDateBPEdit(){
  	var currentForm;
    currentForm = frmSchedule;
	OnClickEndFreqMB = "OnDate";
	currentForm.lblEnd.setVisibility(true);
	currentForm.hbxEndAfter.setVisibility(false);
	currentForm.hbxEndAfterButtonHolder.setVisibility(true);
 	currentForm.hbxEndOnDate.setVisibility(true);
	
  	currentForm.btnNever.skin = "btnScheduleEndLeft";
	currentForm.btnAfter.skin = "btnScheduleEndMid";
	currentForm.btnOnDate.skin = "btnScheduleRightFocus";
}
function disableRepeatBtnHolderBPEdit(){
	OnClickRepeatAsMB = "Once";
  	var currentForm;
    currentForm = frmSchedule;
	currentForm.btnDaily.skin = "btnScheduleLeft";
	currentForm.btnWeekly.skin = "btnScheduleMid";
	currentForm.btnMonthly.skin = "btnScheduleMid";
	currentForm.btnYearly.skin = "btnScheduleRight";
}

function disableEndAfterBtnHolderBPEdit(){
  	var currentForm;
    currentForm = frmSchedule;
	currentForm.hbxEndAfterButtonHolder.setVisibility(false);
	currentForm.lblEnd.setVisibility(false);
	currentForm.hbxEndOnDate.setVisibility(false);
	currentForm.hbxEndAfter.setVisibility(false);
	disableEndBtnHolderBPEdit();
}
function disableEndBtnHolderBPEdit(){
	OnClickEndFreqMB = "";
    var currentForm;
    currentForm = frmSchedule;
	currentForm.btnNever.skin = "btnScheduleEndLeft";
	currentForm.btnAfter.skin = "btnScheduleEndMid";
	currentForm.btnOnDate.skin = "btnScheduleEndRight";
}

//
gblScheduleFreqChangedMB = false;
var repeatAsMB = "";
var endFreqSaveMB = "";
function onClickSaveSchedulefrm() {
	var scheduleRepeatasEditMBFlag = false;
	var scheduleEndingEditMBFlag = false;
	var scheduleStartOnEditMBFlag = false;
	var currentForm;
	var currentSchedueForm;
	currentForm = frmBillPaymentEditFutureNew;
	currentSchedueForm = frmSchedule;
	OnClickRepeatAsMB = gblScheduleRepeatBP;
	OnClickEndFreqMB = gblScheduleEndBP;

  	var tmpStartDate;
	tmpStartDate = currentForm.lblStartDate.text;
	var startOnDateMB = currentSchedueForm.calScheduleStartDate.formattedDate;
	startOnDateMB = getFormattedDate(startOnDateMB, kony.i18n.getCurrentLocale());
	var executionNumberOftimesMB = "";
	var endOnDateMB = "";
	
	

	//check if local variable is updated with the latest value else work with global variable
	//OnClickRepeatAsMB - repeat as (daily,weekly,monthly,yearly) -local variable
	//OnClickEndFreqMB - end freq (never,after and ondate) - local variable
	//gblOnLoadRepeatAsMB - repeat As - global variable
	//gblEndingFreqOnLoadMB - end freq - global variable
	if (OnClickRepeatAsMB != gblOnLoadRepeatAsMB && OnClickRepeatAsMB != "") {
		scheduleRepeatasEditMBFlag = true;
	} else {
		scheduleRepeatasEditMBFlag = false;
	}
	if (OnClickEndFreqMB != gblEndingFreqOnLoadMB && OnClickEndFreqMB != "") {
		scheduleEndingEditMBFlag = true;
	} else {
		scheduleEndingEditMBFlag = false;
	}
	if (OnClickRepeatAsMB != "") {
		repeatAsMB = OnClickRepeatAsMB;
	} else {
		repeatAsMB = gblOnLoadRepeatAsMB;
	}
	if (OnClickEndFreqMB != "") {
		endFreqSaveMB = OnClickEndFreqMB;
	} else {
		endFreqSaveMB = gblEndingFreqOnLoadMB;
	}
	
	if (repeatAsMB != "Once") {
		if (endFreqSaveMB == "") {
			alert(kony.i18n.getLocalizedString("Error_NoEndingValueSelected"));
			repeatAsMB = "";
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			return false;
		}
	}
	
	if(startOnDateMB == null || startOnDateMB == ""){
		alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
		return false;
	}else{
		var curDt = currentDate();
		if( parseDate(curDt) >= parseDate(startOnDateMB)){ 
			alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
			return false;
		}
	}
	
	currentForm.lblStartDate.text = startOnDateMB;
	//frmBillPaymentEdit.lblStartDate.text = startOnDateMB;
	// New changes for once display scenario
	if(repeatAsMB == "Once"){
		currentForm.lblEndOnDate.text = currentForm.lblStartDate.text;
		currentForm.labelRepeatAsValue.text = "Once";
		currentForm.lblExecutetimes.text = "1"; 
		endFreqSaveMB = "";
	}
	//--
	if (parseDate(startOnDateMB) > parseDate(tmpStartDate) || parseDate(startOnDateMB) < parseDate(tmpStartDate)) {
		scheduleStartOnEditMBFlag = true;
	}
	if (kony.string.equalsIgnoreCase(endFreqSaveMB, "Never")) { //kony.i18n.getLocalizedString("keyNever")
		currentForm.lblExecutetimes.text = "-";
		currentForm.lblEndOnDate.text = "-";
	} else if (kony.string.equalsIgnoreCase(endFreqSaveMB, "After")) { //kony.i18n.getLocalizedString("keyAfter")
		executionNumberOftimesMB = currentSchedueForm.tbxAfterTimes.text;
		if (executionNumberOftimesMB == "" || executionNumberOftimesMB == "0" || executionNumberOftimesMB == "0.00") {
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes"));//alert("please enter valid number of times");
			endFreqSaveMB = "";
			repeatAsMB = "";
			return false;
		}
		
		if (executionNumberOftimesMB > 99) {
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			alert("Recurring should be less than 99");
			endFreqSaveMB = "";
			repeatAsMB = "";
			return false;
		}
		
		if (gblexecutionOnLoadMB != "") {
			if (executionNumberOftimesMB != gblexecutionOnLoadMB) { //if selected execution times is greater than or less than the execution times from service then set the flag to true
				scheduleEndingEditFlag = true;
			}
		} else {
			scheduleEndingEditFlag = true;
		}
		endOnDateMB = endOnDateCalculatorMB(currentSchedueForm.calScheduleStartDate.formattedDate,
			executionNumberOftimesMB, repeatAsMB);
			
		endOnDateMB = getFormattedDate(endOnDateMB, kony.i18n.getCurrentLocale());
		currentForm.labelRepeatAsValue.text = "After";
		currentForm.lblEndOnDate.text = endOnDateMB;
		currentForm.lblExecutetimes.text = executionNumberOftimesMB;
	} else if (kony.string.equalsIgnoreCase(endFreqSaveMB, "On Date") || kony.string.equalsIgnoreCase(endFreqSaveMB, "OnDate")) { //kony.i18n.getLocalizedString("keyOnDate")
		endOnDateMB = getFormattedDate(currentSchedueForm.calScheduleEndDate.formattedDate, kony.i18n.getCurrentLocale());
		if (endOnDateMB == null || endOnDateMB == "") {
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));//alert("Please select valid end date");
			return false;
		}
		if (parseDate(endOnDateMB) <= parseDate(startOnDateMB)) {
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));//alert("End date should be greater than Start Date");
			return false;
		}
		if (gblendDateOnLoadMB != "") {
			//alert(" gblendDateOnLoadMB :: " + gblendDateOnLoadMB);
			if (parseDate(endOnDateMB) > parseDate(gblendDateOnLoadMB) || parseDate(endOnDateMB) < parseDate(gblendDateOnLoadMB)) { //if selected end date is greater than or less than the end date from service then set the flag to true
            	scheduleEndingEditMBFlag = true;
			}
		} else {
			scheduleEndingEditMBFlag = true;
		}
		executionNumberOftimesMB = numberOfExecutionMB(currentSchedueForm.calScheduleStartDate.formattedDate,
			currentSchedueForm.calScheduleEndDate.formattedDate, repeatAsMB);
		if (executionNumberOftimesMB > 99) {
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			//alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));//alert("End date should be greater than Start Date");
			alert("Recurring should be less than 99");
			return false;
		}	
		currentForm.lblEndOnDate.text = endOnDateMB;
		currentForm.lblExecutetimes.text = executionNumberOftimesMB;
	}
	currentForm.labelRepeatAsValue.text = repeatAsMB;
	
	if (scheduleRepeatasEditMBFlag == true || scheduleEndingEditMBFlag == true || scheduleStartOnEditMBFlag == true) {
		gblScheduleFreqChangedMB = true;
	} else {
		gblScheduleFreqChangedMB = false;
	}
	currentForm.show();
	if(OnClickEndFreqMB != ""){
		gblScheduleEndBPMB = OnClickEndFreqMB;
	}
}


function onClickCancelEditPage() {
	repeatAsMB = "";
	endFreqSaveMB = "";
	gblAmountSelectedMB = false;
	gblScheduleFreqChangedMB = false;
	frmBillPaymentView.show();
}

function nextEditBillPay(currentForm){
  	var userEnteredAmtMB = "";
	var amtMB = 0;
	if (gblEditBillMethodMB == 0) {
		amtMB = currentForm.txtAmtInput.text;
		userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	} else if (gblEditBillMethodMB == 2) {
		amtMB = currentForm.txtAmtInput.text;			
		if(amtMB != null){
			userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
		}
	} else if (gblEditBillMethodMB == 1 || gblEditBillMethodMB == 3) {
		amtMB = currentForm.txtAmtInput.text;
		if(amtMB != null)
			 userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
      	if(amtMB == frmBillPaymentEditFutureNew.lblamtvalue.text){
        	gblAmountSelectedMB = true;
        }else{
        	gblAmountSelectedMB = false;
        }
	}	
	validateInput(currentForm, userEnteredAmtMB,amtMB);
}

function removeFormat(numberFormat){
	return removeCommaIB(numberFormat.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), ""));
}

function validateInput(currentForm, userEnteredAmtMB, amtMB){
	var tempAmt1 = userEnteredAmtMB;
	if(tempAmt1 != ""){
		if(tempAmt1.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
			tempAmt1.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		}
		var pat1= /[^0-9.,]/g
		var  pat2 = /[\s]/g
		var isAlpha = pat1.test(tempAmt1);
		var containsSpace = pat2.test(tempAmt1);  
		if(isAlpha == true || containsSpace == true){
		  dismissLoadingScreen();
		  alert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"));
		  return false;
	     }
	}
	userEnteredAmtMB = removeFormat(userEnteredAmtMB);
	if(gblBillerCompcodeEditMB == "2704"){
		if (scheduleValidationMBTopUpAmtMinMax(userEnteredAmtMB)){
			return false;
		}
	}
	
	if (userEnteredAmtMB == "" || userEnteredAmtMB == "0" || userEnteredAmtMB == "0.0") {
      	gblAmountSelectedMB = false;
      	if(currentForm.flxAmountInput.isVisible){
        	showAlertWithCallBack(kony.i18n.getLocalizedString("Error_Invalid_Amount"), 
                                  kony.i18n.getLocalizedString("info"),callBackEditBillPayAmountFields);  
        }else{
			alert(kony.i18n.getLocalizedString("Error_Invalid_Amount"));	          
        }
		return false;
	} else {
		//if userEnteredAmtMB is not equal to amountMB then set the flag to true
		var amtvalidationMB = amtValidationIB(userEnteredAmtMB);
		if (amtvalidationMB == false) {
			//alert("Please enter a valid amount");
			gblAmountSelectedMB = false;
          	if(currentForm.flxAmountInput.isVisible){
                showAlertWithCallBack(kony.i18n.getLocalizedString("Error_Invalid_Amount"), 
                                      kony.i18n.getLocalizedString("info"),callBackEditBillPayAmountFields);  
            }else{
                alert(kony.i18n.getLocalizedString("Error_Invalid_Amount"));	          
            }
			//dismissLoadingScreen();
			return false;
		} else {
			var amountMB = currentForm.lblamtvalue.text;
			amountMB = amountMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(amountMB.indexOf(",") != -1)
				amountMB = replaceCommon(amountMB, ",", "");
			
			var amt = userEnteredAmtMB+"";
			amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(amt.indexOf(",") != -1)
				amt = replaceCommon(amt, ",", "");
			if (parseFloat(amt) != parseFloat(amountMB.toString())) { // check this for online,creditcard,loan
				gblAmountSelectedMB = true;
			} else {
				gblAmountSelectedMB = false;
			}
			gblUserEnteredAmtMB = userEnteredAmtMB;
			
		}
	}

	if(gblBPflag == true && gblEditBillMethodMB == 1 && currentForm.txtAmtInput.isVisible){
      	var amt1;
        amt1 = currentForm.lblamtvalue.text;
		amt1 = amt1.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		if(amt1.indexOf(",") != -1)replaceCommon(amt1, ",", "");
		var amt2 = currentForm.txtAmtInput.text;
		amt2 = amt2.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		if(amt2.indexOf(",") != -1)replaceCommon(amt2, ",", "");
		var isChangedToFullAmt = parseFloat(amt1.toString())== parseFloat(amt2.toString());//(parseFloat(frmBillPaymentView.lblamtvalue.text) == parseFloat(frmBillPaymentEdit.txtAmtInput.text));
		if(currentForm.txtAmtInput.disabled && gblScheduleFreqChangedMB == false && isChangedToFullAmt == true ){
			alert(kony.i18n.getLocalizedString("Error_NoEditingDoneBP"));
			return false
		}else{
			// call billerValidation service here
			var compCodeBillPay = gblBillerCompcodeEditMB;
			var ref1ValBillPay = currentForm.lblRef1value.text;
			var ref2ValBillPay = gblRef2EditBPMB;
			var billAmountBillPay = userEnteredAmtMB;
			var scheduleDtBillPay = currentForm.lblStartDate.text;
			callBillerValidationEditBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay,
				scheduleDtBillPay);
		}
	}else{
      	if(!gblBPflag){
         	checkForTPTransLimitCallOnlinePaymentInqMB(currentForm);
        }else{
          	if (gblAmountSelectedMB == true || gblScheduleFreqChangedMB == true) {
				// call biller validation service here
              	var compCodeBillPay = gblBillerCompcodeEditMB;
                var ref1ValBillPay = currentForm.lblRef1value.text;
                var ref2ValBillPay = gblRef2EditBPMB;
                var billAmountBillPay = userEnteredAmtMB;
                var scheduleDtBillPay = currentForm.lblStartDate.text;
                callBillerValidationEditBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay,
                                                       scheduleDtBillPay);
            } else {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("Error_NoEditingDoneBP"));
                return false
            }
        }
	}
}

function callBackEditBillPayAmountFields(){
	frmBillPaymentEditFutureNew.txtAmtInput.setFocus(true);
}

function scheduleValidationMBTopUpAmtMinMax(topupAmt){
       var j = 0;
       var tmp = "";
       var trueMoney = "2704";
       var amtMin = "";
       var amtMax = "";
       var errorText ="";

       amtMin = gblTopupAmtMin; 
       amtMax = gblTopupAmtMax;                        
       
       if (topupAmt == null || topupAmt == "" || topupAmt == " " || topupAmt == "0" || topupAmt == "0.0" || topupAmt == "0.00") {
			  var errorText = kony.i18n.getLocalizedString("MIB_BPMinimum");
              errorText = errorText.replace("{amt_value}", commaFormatted(amtMin));
              alert(errorText);
              return true;
	   }else if (parseFloat(removeCommos(topupAmt)) < parseFloat(amtMin)){
              var errorText = kony.i18n.getLocalizedString("MIB_BPMinimum");
              errorText = errorText.replace("{amt_value}", commaFormatted(amtMin));
              alert(errorText);
              return true;
       }else if (parseFloat(removeCommos(topupAmt)) > parseFloat(amtMax)){
              var errorText = kony.i18n.getLocalizedString("MIB_BPMaximum");
              errorText = errorText.replace("{amt_value}", commaFormatted(amtMax));
              alert(errorText);
              return true;
       }
       return false; 
}

function checkForTPTransLimitCallOnlinePaymentInqMB(currentForm){
	showLoadingScreen();
    var inputparam = [];
    inputparam["rqUID"] = "";
    inputparam["EffDt"] = "";
    inputparam["TrnId"] = ""; // change this value when integrated with calender / future transactions page
    inputparam["Amt"] = gblUserEnteredAmtMB; /// for billpayment it is 0.00
    inputparam["compCode"] = gblBillerCompcodeEditMB;
    inputparam["MobileNumber"] = removeHyphenIB(currentForm.lblRef1value.text);
    inputparam["BankId"]="011";
    inputparam["BranchId"]= "0001";
    invokeServiceSecureAsync("onlinePaymentInqForEditBP", inputparam, callBackCheckForTPTransLimitCallOnlinePaymentInqMB);
}

function callBackCheckForTPTransLimitCallOnlinePaymentInqMB(status,result){
	if (status == 400) {
		if(result["opstatus"] == 0){
          	dismissLoadingScreen();
          	if (gblAmountSelectedMB == true || gblScheduleFreqChangedMB == true) {
				// call biller validation service here
              	var compCodeBillPay = gblBillerCompcodeEditMB;
                var ref1ValBillPay = currentForm.lblRef1value.text;
                var ref2ValBillPay = gblRef2EditBPMB;
                var billAmountBillPay = userEnteredAmtMB;
                var scheduleDtBillPay = currentForm.lblStartDate.text;
                callBillerValidationEditBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay,
                                                       scheduleDtBillPay);
            } else {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("Error_NoEditingDoneBP"));
                return false
            }
		}else{
			dismissLoadingScreen();
			alert(""+result["errMsg"]);
        	return false;
		}
	}
}

function nextEditTopup(currentForm){
  	if (gblEditBillMethodMB != 1) {
    	amtMB = currentForm.txtAmtInput.text;
    } else if (gblEditBillMethodMB == 1){
      	if(gblBillerCompcodeEditMB == "2704"){
        	amtMB = currentForm.txtAmtInput.text;
        }else{
        	amtMB = currentForm.btnTopUpAmountComboBox.text;
        }
   	}
  	userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
  	validateInput(currentForm, userEnteredAmtMB,amtMB);
}
function onClickNextEditPage() {
  	currentForm = frmBillPaymentEditFutureNew;
    if(gblBPflag){
      nextEditBillPay(currentForm);
    }else{
      nextEditTopup(currentForm);
    }
	
}

/**
 * Biller validation for edit billers. Added as ENHANCEMENT on 18-02-2015
 */
function callBillerValidationEditBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay,
	scheduleDtBillPay) {
	showLoadingScreen();
	inputParam = {};
	inputParam["BillerCompcode"] = compCodeBillPay;
	inputParam["ReferenceNumber1"] = ref1ValBillPay;
	inputParam["ReferenceNumber2"] = ref2ValBillPay;
	inputParam["Amount"] = billAmountBillPay;
	inputParam["ScheduleDate"] = scheduleDtBillPay;
	inputParam["billerCategoryID"] = gblBillerCategoryID;
	inputParam["billerMethod"] = gblEditBillMethodMB;
	inputParam["ModuleName"] = "BillPay";
	invokeServiceSecureAsync("billerValidation", inputParam, callBillerValidationEditBillPayMBServiceCallBack);
}

/**
 * Biller validation call back for edit bill payment. Added as ENHANCEMENT on 18-02-2015
 */
function callBillerValidationEditBillPayMBServiceCallBack(status, result) {
	if (status == 400) {
		dismissLoadingScreen();
		var validationBillPayMBFlag = "";
		var isValidOnlineBiller = result["isValidOnlineBiller"];
		if (result["opstatus"] == 0) {
			validationBillPayMBFlag = result["validationResult"];
		} else {
			dismissLoadingScreen();
			validationBillPayMBFlag = result["validationResult"];
		}
		
		if (validationBillPayMBFlag == "true") {
			//added the below services for piblic key exchange on SPA
			callCrmProfileInqServiceMBForEditBPAmt();
		} else {
			dismissLoadingScreen();
			if(isValidOnlineBiller != undefined && isValidOnlineBiller == "true"){
       	    	alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
            } else{
				alert(kony.i18n.getLocalizedString("keyBillerValidationFailed"));
			}	
			return false;
		}
	}
}


//added the below services for piblic key exchange on SPA
function spaEditBPToeknExchng()
{
	var inputParam = [];
 	showLoadingScreen();
 	invokeServiceSecureAsync("tokenSwitching", inputParam, spaEditBPToeknExchngCallbackfunction);
}


function spaEditBPToeknExchngCallbackfunction(status,resulttable){
  if (status == 400) {
   if(resulttable["opstatus"] == 0){
    	callCrmProfileInqServiceMBForEditBPAmt();
   }else{
    dismissLoadingScreen();
    alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
   }
   }
}


function callCrmProfileInqServiceMBForEditBPAmt() {
	var inputparam = [];
	showLoadingScreen();
	var currentForm = frmBillPaymentEditFutureNew;
	var editedAmountMB = gblUserEnteredAmtMB;  //removeCommaIB(gblUserEnteredAmtMB);
	editedAmountMB = editedAmountMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")

	if (editedAmountMB.indexOf(",") != -1) {
   	    editedAmountMB = parseFloat(replaceCommon(editedAmountMB, ",", "")).toFixed(2);
	}else{
	    editedAmountMB = parseFloat(editedAmountMB).toFixed(2);
	}
	var billerName = currentForm.lblBillerName.text;
	billerName = billerName.split("(")[0];
	billerName = billerName.toString().trim();
	inputparam["amtEdited"] = editedAmountMB;
	inputparam["billerName"] = billerName; //frmBillPaymentEdit.lblBillerNickName.text;
	inputparam["billerRef1"] = currentForm.lblRef1value.text;

	if (gblAmountSelectedMB == true && gblScheduleFreqChangedMB == false) {
		if(gblBPflag){
			inputparam["extTrnRefID"] = "SB" + currentForm.lblScheduleRefValue.text.substring(2, currentForm.lblScheduleRefValue.text.length);
		}else{
			inputparam["extTrnRefID"] = currentForm.lblScheduleRefValue.text.replace("F", "S");
		}
		inputparam["startOnDate"] = changeDateFormatForService(currentForm.lblStartDate.text);
		inputparam["editEditBillPayFlag"] = "amtChangeEdit";
	}else if((gblAmountSelectedMB == true && gblScheduleFreqChangedMB == true) || (gblAmountSelectedMB == false && gblScheduleFreqChangedMB == true)){
		inputparam["editEditBillPayFlag"] = "freqChangeEdit";
		if(gblBPflag){
			inputparam["scheID"] = "SB" +currentForm.lblScheduleRefValue.text.substring(2, currentForm.lblScheduleRefValue.text.length);
		}else{
			inputparam["scheID"] = currentForm.lblScheduleRefValue.text.replace("F", "S");
		}
	}
	invokeServiceSecureAsync("crmProfileInq", inputparam, forEditBPCrmProfileInqServiceCallBackMB);
}

function forEditBPCrmProfileInqServiceCallBackMB(status, result) {
	if (status == 400) {
		var activityTypeID = "";
      	var currentForm = frmBillPaymentEditFutureNew;
		if(gblBPflag){
			activityTypeID = "067";
		}else{
			activityTypeID = "068";
		}
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = currentForm.lblBillerName.text +"+"+ removeHyphenIB(currentForm.lblRef1value.text);    //Biller Name + Ref1
		var activityFlexValues3 = ""; 
		var activityFlexValues4 = "";
		var activityFlexValues5 = ""; 
		if(gblScheduleFreqChangedMB == true){
			activityFlexValues3 = dateFormatForDisplay(gblstartOnMB) +"+"+currentForm.lblStartDate.text; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsMB +"+"+currentForm.labelRepeatAsValue.text; //Old Frequency + New Frequency (Edit case)			
		}else{
			activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)	
		}

		if(gblAmountSelectedMB == true){
			activityFlexValues5 = gblAmountMB +"+"+gblUserEnteredAmtMB; //Old Amount + New Amount (Edit case)
		}else{
			activityFlexValues5 = gblAmountMB; //Old Amount + New Amount (Edit case)
		}
		var logLinkageId = "";
		if (result["opstatus"] == 0) {
			activityStatus = "00";
			var StatusCode = result["statusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				var dailyChannelLimitMB = result["ebMaxLimitAmtCurrent"];
				dailyChannelLimitMB = parseFloat(dailyChannelLimitMB.toString());
				var feeMB = currentForm.lblPaymentFeeValue.text;
				var totalamtMB = parseInt(removeCommaIB(gblUserEnteredAmtMB)) + parseInt(feeMB.substring(0, feeMB.length - 1).trim());
				if (dailyChannelLimitMB < totalamtMB) {
					activityStatus = "02";
					alert(kony.i18n.getLocalizedString("Error_Amount_Daily_Limit"));
					dismissLoadingScreen();
					return false;
				} else {
					activityStatus = "00";
					showConfirmationPageForEditBPMB();
				}
			} else {
				activityStatus = "02";
				dismissLoadingScreen();
				alert(" " + StatusDesc);
			}
		} else {
			activityStatus = "02";
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);	
	}
}

function showConfirmationPageForEditBPMB() {
	currentForm;
	currentForm = frmBillPaymentEditFutureNew;
	setSchedueDateConfirm(currentForm);
	if(gblBPflag){
		doEditBillPayConfirm();
	}else{
     	doEditTopupConfirm();
    }
}

function setSchedueDateConfirm(currentForm){
    frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text = currentForm.lblPaymentDateValue.text;
	frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text = currentForm.lblPaymentFeeValue.text;
	//Schedule details :
	frmEditFutureBillPaymentConfirm.lblStartDate.text = currentForm.lblStartDate.text;
	frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text = currentForm.labelRepeatAsValue.text;
	frmEditFutureBillPaymentConfirm.lblEndOnDate.text = currentForm.lblEndOnDate.text;
	frmEditFutureBillPaymentConfirm.lblExecutetimes.text = currentForm.lblExecutetimes.text
	//scheduled ref num :
	frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text = currentForm.lblScheduleRefValue.text;
}

function doEditBillPayConfirm(){
      var currentForm;
      currentForm = frmBillPaymentEditFutureNew;
        	// from Account details :
        frmEditFutureBillPaymentConfirm.imgFrom.src=currentForm.imgFrom.src;
        frmEditFutureBillPaymentConfirm.imgBiller.src=currentForm.imgBiller.src;
        frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text = currentForm.lblFromAccountNickname.text;
        frmEditFutureBillPaymentConfirm.lblFromAccountName.text = currentForm.lblFromAccountName.text;
        frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text = currentForm.lblFromAccountNumber.text;
        // To biller details :
        frmEditFutureBillPaymentConfirm.lblBillerName.text = currentForm.lblBillerName.text;
        if (currentForm.lblBillerNickName.isVisible) {
            frmEditFutureBillPaymentConfirm.lblBillerNickName.text = currentForm.lblBillerNickName.text;
        } else {
            frmEditFutureBillPaymentConfirm.lblBillerNickName.setVisibility(false);
        }
        frmEditFutureBillPaymentConfirm.lblref1.text = currentForm.lblRef1.text;
        frmEditFutureBillPaymentConfirm.lblRef1Value.text = currentForm.lblRef1value.text;
        //below line is added for CR - PCI-DSS masked Credit card no
        frmEditFutureBillPaymentConfirm.lblRef1ValueMasked.text = currentForm.lblRef1valueMasked.text;
        if (currentForm.flxRef2.isVisible) {
            frmEditFutureBillPaymentConfirm.hbxRef2.setVisibility(true);
            frmEditFutureBillPaymentConfirm.lblRef2.text = currentForm.lblRef2.text;
            frmEditFutureBillPaymentConfirm.lblRef2Value.text = currentForm.lblRef2value.text;
        } else {
            frmEditFutureBillPaymentConfirm.hbxRef2.setVisibility(false);
        }
      if (gblEditBillMethodMB == 0 || gblEditBillMethodMB == 3 || gblEditBillMethodMB == 1) {
		var amount = currentForm.txtAmtInput.text;
		if(amount.indexOf(",") != -1){
			amount = replaceCommon(amount, ",", "");
		}
		if (currentForm.txtAmtInput.text.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1) {
			frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount) +" "+kony.i18n.getLocalizedString("currencyThaiBaht");	

		} else {
			amount = amount.trim();
			amount = amount.substring(0, amount.length-1).trim();
			frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+" "+kony.i18n.getLocalizedString("currencyThaiBaht"); ;
		}
	} else {
		var amount = "";
		amount = currentForm.txtAmtInput.text;
		if(amount.indexOf(",") != -1){
			amount = replaceCommon(amount, ",", "");
		}
		amount = amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+ " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
		frmEditFutureBillPaymentConfirm.lblAmtValue.text = currentForm.txtAmtInput.text;
	}
      frmEditFutureBillPaymentConfirm.hbxTPCusName.setVisibility(false);
  	  onClickNextEditBPConfirm();
}
function doEditTopupConfirm(){
  		var currentForm;
  		currentForm = frmBillPaymentEditFutureNew;
      	// from Account details :
        frmEditFutureBillPaymentConfirm.imgFrom.src=currentForm.imgFrom.src;
        frmEditFutureBillPaymentConfirm.imgBiller.src=currentForm.imgBiller.src;
        frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text = currentForm.lblFromAccountNickname.text;
        frmEditFutureBillPaymentConfirm.lblFromAccountName.text = currentForm.lblFromAccountName.text;
        frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text = currentForm.lblFromAccountNumber.text;
        // To biller details :
        frmEditFutureBillPaymentConfirm.lblBillerName.text = currentForm.lblBillerName.text;
        if (currentForm.lblBillerNickName.isVisible) {
            frmEditFutureBillPaymentConfirm.lblBillerNickName.text = currentForm.lblBillerNickName.text;
        } else {
            frmEditFutureBillPaymentConfirm.lblBillerNickName.setVisibility(false);
        }
        frmEditFutureBillPaymentConfirm.lblref1.text = currentForm.lblRef1.text;
        frmEditFutureBillPaymentConfirm.lblRef1Value.text = currentForm.lblRef1value.text;
        //below line is added for CR - PCI-DSS masked Credit card no
        frmEditFutureBillPaymentConfirm.lblRef1ValueMasked.text = currentForm.lblRef1valueMasked.text;
        if (currentForm.flxRef2.isVisible) {
            frmEditFutureBillPaymentConfirm.hbxRef2.setVisibility(true);
            frmEditFutureBillPaymentConfirm.lblRef2.text = currentForm.lblRef2.text;
            frmEditFutureBillPaymentConfirm.lblRef2Value.text = currentForm.lblRef2value.text;
        } else {
            frmEditFutureBillPaymentConfirm.hbxRef2.setVisibility(false);
        }
		if (gblEditBillMethodMB != 1) {
			var amount = currentForm.txtAmtInput.text; 
			if(amount.indexOf(",") != -1){
				amount = replaceCommon(amount, ",", "");
			}
		
			if (currentForm.txtAmtInput.text.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1) {
				//frmEditFutureBillPaymentConfirm.lblAmtValue.text = frmBillPaymentEdit.txtAmtInput.text +" "+kony.i18n.getLocalizedString("currencyThaiBaht");
				frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
			} else {
				amount = amount.trim();
				amount = amount.substring(0, amount.length-1).trim();
				//frmEditFutureBillPaymentConfirm.lblAmtValue.text = frmBillPaymentEdit.txtAmtInput.text;
				frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+ " "+kony.i18n.getLocalizedString("currencyThaiBaht");;
			}
		}else{
			var amount = 0;
          	if(gblBillerCompcodeEditMB == "2704"){
            	amount = currentForm.txtAmtInput.text;
            }else{
            	amount = gblUserEnteredAmtMB;
            }
			if(amount.indexOf(",") != -1){
				amount = replaceCommon(amount, ",", ""); 
			}
			
			if (gblUserEnteredAmtMB.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1) {
				//frmEditFutureBillPaymentConfirm.lblAmtValue.text = gblUserEnteredAmtMB +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
				frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount) +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
			} else {
				amount = amount.trim();
				amount = amount.substring(0, amount.length-1).trim();
				//frmEditFutureBillPaymentConfirm.lblAmtValue.text = gblUserEnteredAmtMB;
				frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+" "+ kony.i18n.getLocalizedString("currencyThaiBaht");;
			}
		}
    if(gblBillerCompcodeEditMB == "2151"){
			frmEditFutureBillPaymentConfirm.hbxTPCusName.setVisibility(true);
			frmEditFutureBillPaymentConfirm.lblTPCustomerName.text = "Easy Pass Customer Name"; 
			frmEditFutureBillPaymentConfirm.lblTPCustomerNameVal.text = gblTPCustomerNameVal;
	}else{
    	frmEditFutureBillPaymentConfirm.hbxTPCusName.setVisibility(false);
    }	
    onClickNextEditBPConfirm();
}

function onClickNextEditBPConfirm() {
		if  (gblUserLockStatusMB == "03" ) {
			dismissLoadingScreen(); 
			popTransferConfirmOTPLock.show();
	        return false;		
		}	
		else{
           var lblText = kony.i18n.getLocalizedString("transPasswordSub");
		   var refNo = "";
		   var mobNO = "";
          
          if(gblAuthAccessPin == true){
            dismissLoadingScreen();
            showAccesspinPopup();
          }else{             
		     popupTractPwd.hbxPopupTranscPwd.skin = hbxPopupTrnsPwdBlue;
		     popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue; 
		     popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
		     showOTPPopup(lblText, refNo, mobNO, editBPConfirmation, 3);    
          }
		
		}
}

function editBPConfirmation(txnPwd) {
	if (txnPwd != "") {
		checkEditBillPayVerifyPWDMB(txnPwd);
	} else {
		  setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
	}
}

function checkEditBillPayVerifyPWDMB(txnPwd) {
	var inputParam = [];
	inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin; // check values  -- To DO
	inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin; // check values  -- To DO
	inputParam["password"] = txnPwd;
  	var frequency = frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
	popupTractPwd.txtOTP.text = "";
	showLoadingScreen();
	if ((gblAmountSelectedMB == true && gblScheduleFreqChangedMB == true) || (gblAmountSelectedMB == false && gblScheduleFreqChangedMB == true)) {
		inputParam["selectedEditOption"] = "Frequency";
		var amtMB = frmEditFutureBillPaymentConfirm.lblAmtValue.text;  //removeCommaIB(frmEditFutureBillPaymentConfirm.lblAmtValue.text);
		if (amtMB.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
			amtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim(); 
		} 
	    if (amtMB.indexOf(",") != -1) {
       	    amtMB = parseFloat(replaceCommon(amtMB, ",", "")).toFixed(2);
   		}else{
			amtMB = parseFloat(amtMB).toFixed(2);
		}   
		inputParam["amt"] = amtMB;
		inputParam["fromAcct"] = gblFromAccIdMB; // from Account ID (from PaymentInq)
		inputParam["fromAcctType"] = gblFromAccTypeMB; // from Account Type (from PaymentInq)
		inputParam["toAcct"] = gblToAccNoMB; // TO Biller No (from PaymentInq)
		inputParam["toAccTType"] = gblToAccTypeMB; // To Biller Type ((from PaymentInq)
		inputParam["dueDate"] = changeDateFormatForService(frmEditFutureBillPaymentConfirm.lblStartDate.text); // Start On Date

		inputParam["pmtRefNo1"] = removeHyphenIB(frmEditFutureBillPaymentConfirm.lblRef1Value.text); // Ref 1 value
		inputParam["custPayeeId"] = gblCustPayeeIDMB; // check this with TL
		inputParam["transCode"] = gblTransCodeMB; //TransCode (from PaymentInq)
		var note = frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text;
		if(isNotBlank(note) && note != undefined && note != "undefined"){
			inputParam["memo"] = note; // My Note 
		}else{
			inputParam["memo"] = "";
		}
		
	    var ref1MB = frmEditFutureBillPaymentConfirm.lblRef1Value.text;
		var haveRef2 = false; 
	    if(ref1MB.indexOf("-") != -1){
	      ref1MB = kony.string.replace(ref1MB, "-", "");
	    }
		if(gblEditBillMethodMB == "1"){
		 inputParam["PmtMiscType"] = "MOBILE";
	     inputParam["MiscText"] = ref1MB;
		}
		inputParam["billMethod"] = gblEditBillMethodMB;
			if(frmBillPaymentEditFutureNew.flxRef2.isVisible){
				haveRef2 = true;
				inputParam["pmtRefNo2"] = frmEditFutureBillPaymentConfirm.lblRef2Value.text;
			}
		if( gblBPflag == true && gblRef2EditBPMB != ""){
			inputParam["pmtRefNo2"] = gblRef2EditBPMB;
		}
		inputParam["pmtMethod"] = gblPmtMethodFromPmt; // get this value from paymentInq
			
		if(gblBPflag){
			inputParam["extTrnRefId"] = "SB" +frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.substring(2, frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.length); // Updated Schedule Ref No	
		}else{
			inputParam["extTrnRefId"] = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.replace("F", "S");
		}
		var endFreqFlag = "";
		if (endFreqSaveMB == "After") {
			inputParam["NumInsts"] = frmEditFutureBillPaymentConfirm.lblExecutetimes.text; // Execution times 
			endFreqFlag="After";
		}
		if (endFreqSaveMB == "On Date" || endFreqSaveMB == "OnDate") {
			inputParam["FinalDueDt"] = changeDateFormatForService(frmEditFutureBillPaymentConfirm.lblEndOnDate.text); // End On Date
			endFreqFlag="OnDate";
		}
		inputParam["endFreqSave"] = endFreqFlag;
		inputParam["frequencyCode"] = "0";
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDailyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDailyNE"))) {
			inputParam["dayOfWeek"] = "";
			frequency = "Daily";
			inputParam["frequencyCode"] = "4";
		}
		
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeeklyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeeklyNE"))) {
			inputParam["dayOfWeek"] = "";
			frequency = "Weekly";
			inputParam["frequencyCode"] = "1";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthlyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthlyNE"))) {
			inputParam["dayOfMonth"] = "";
			frequency = "Monthly";
			inputParam["frequencyCode"] = "2";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearlyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearlyNE"))) {
			frequency = "Annually";
			inputParam["dayofMonth"] = "";
			inputParam["monthofYear"] = "";
			inputParam["frequencyCode"] = "3";
		}
      	frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text =frequency;
		//if(kony.string.equalsIgnoreCase(frequency, "Once")) 
        //Modified for DEF590
		if(kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnceNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnceNE"))) 
		{
			inputParam["freq"] = "once";
		}else{
			inputParam["freq"] = frequency;
		}
		if(flowSpa){
			inputParam["channelId"]="IB";
		}else{
			inputParam["channelId"]="MB";
		}
		//inputParam["channelId"]="MB";
		inputParam["desc"] = "";
		if(gblBPflag){
			inputParam["scheID"] = "SB" +frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.substring(2, frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.length);
		}else{
			inputParam["scheID"] = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.replace("F", "S");
		}
		if(gblBPflag){
			inputParam["transRefType"] = "SB";
		}else{
			inputParam["transRefType"] = "SU";
		}
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmEditFutureBillPaymentConfirm.lblBillerName.text +"+"+ removeHyphenIB(frmEditFutureBillPaymentConfirm.lblRef1Value.text);     //Biller Name + Ref1
		var activityFlexValues3 = "";
		var activityFlexValues4 = "";
		var activityFlexValues5 = "";
		if(gblScheduleFreqChangedMB == true){
			activityFlexValues3 = dateFormatForDisplay(gblstartOnMB)+"+"+frmEditFutureBillPaymentConfirm.lblStartDate.text; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsMB+"+"+frequency; //Old Frequency + New Frequency (Edit case)
		}else{
			activityFlexValues3 = dateFormatForDisplay(gblstartOnMB);
			activityFlexValues4 = gblOnLoadRepeatAsMB;
		}
		
		activityFlexValues5 = gblAmountMB+"+"+removeCommaIB(frmEditFutureBillPaymentConfirm.lblAmtValue.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")); //Old Amount + New Amount (Edit case)
	} else if (gblAmountSelectedMB == true && gblScheduleFreqChangedMB == false) {
		//callPaymentUpdateServiceMB();
      	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDailyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDailyNE"))) {
			frequency = "Daily";
		}
		
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeeklyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeeklyNE"))) {
			frequency = "Weekly";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthlyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthlyNE"))) {
			frequency = "Monthly";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearlyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearlyNE"))) {
			frequency = "Annually";
		}
		var editedAmountMB = frmEditFutureBillPaymentConfirm.lblAmtValue.text;		//removeCommaIB(frmEditFutureBillPaymentConfirm.lblAmtValue.text);
		
		if (editedAmountMB.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) 
            editedAmountMB = editedAmountMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	    
	    if (editedAmountMB.indexOf(",") != -1) {
       	    editedAmountMB = parseFloat(replaceCommon(editedAmountMB, ",", "")).toFixed(2);
   		}else
    	    editedAmountMB = parseFloat(editedAmountMB).toFixed(2);
		
		if(gblBPflag){
			inputParam["extTrnRefID"] = "SB" + frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.substring(2, frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.length);
		}else{
			inputParam["extTrnRefID"] = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.replace("F", "S");
		}
		inputParam["Amt"] = editedAmountMB;
		inputParam["Starton"] = changeDateFormatForService(frmEditFutureBillPaymentConfirm.lblStartDate.text);
		inputParam["selectedEditOption"] = "Amount";
		
		var userEnteredAmt = frmEditFutureBillPaymentConfirm.lblAmtValue.text;
       	userEnteredAmt = userEnteredAmt.substring(0,userEnteredAmt.length-1);
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmEditFutureBillPaymentConfirm.lblBillerName.text +"+"+ removeHyphenIB(frmEditFutureBillPaymentConfirm.lblRef1Value.text);    //Biller Name + Ref1
		var activityFlexValues3 = frmEditFutureBillPaymentConfirm.lblStartDate.text; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = frequency; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB +"+"+userEnteredAmt; //Old Amount + New Amount (Edit case)		
	}
		var activityTypeID = "";
		if(gblBPflag){
			activityTypeID = "067";
		}else{
			activityTypeID = "068";
		}
		//inputParam["channelName"] = "MB";
		if(flowSpa){
			inputParam["channelName"]="IB";
		}else{
			inputParam["channelName"]="MB";
		}
		inputParam["notificationType"] = "Email"; // check this : which value we have to pass
		inputParam["phoneNumber"] = gblPHONENUMBER;
		inputParam["emailId"] = gblEmailId;
		inputParam["customerName"] = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text 
		var fromAccount = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text
		if(fromAccount.indexOf("-") != -1){
		    fromAccount = kony.string.replace(fromAccount, "-", "");
		}
		inputParam["fromAccount"] = fromAccount; 
		inputParam["fromAcctNick"] = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text;
		inputParam["fromAcctName"] = frmEditFutureBillPaymentConfirm.lblFromAccountName.text;
		inputParam["billerNick"] = frmEditFutureBillPaymentConfirm.lblBillerNickName.text ;
		inputParam["billerName"] = frmEditFutureBillPaymentConfirm.lblBillerName.text;
		inputParam["billerNameTH"]=billerNamTH;
	    inputParam["ref1EN"] = gblRef1LblEN+" : " + frmEditFutureBillPaymentConfirm.lblRef1Value.text;
	    inputParam["ref1TH"] = gblRef1LblTH+" : " + frmEditFutureBillPaymentConfirm.lblRef1Value.text;
		var ref2 = "";
		if(haveRef2){
			ref2 = frmEditFutureBillPaymentConfirm.lblRef2Value.text;
			
			inputParam["ref2EN"] = gblRef2LblEN+" : " + ref2;
	        inputParam["ref2TH"] = gblRef2LblTH+" : " + ref2;
		}else{
			inputParam["ref2EN"] = "";
	        inputParam["ref2TH"] = "";
		}
		inputParam["amount"] =  frmEditFutureBillPaymentConfirm.lblAmtValue.text;
		inputParam["fee"] = frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		inputParam["initiationDt"] =NormalDateMB;
		inputParam["recurring"] = frequency;
		inputParam["startDt"] = frmEditFutureBillPaymentConfirm.lblStartDate.text;
		inputParam["endDt"] = frmEditFutureBillPaymentConfirm.lblEndOnDate.text;
		inputParam["refID"] = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text;
		inputParam["mynote"] = frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text;
		inputParam["memo"] = frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text;
		//inputParam["channelId"] = "MB";
		if(flowSpa){
			inputParam["channelId"]="IB";
		}else{
			inputParam["channelId"]="MB";
		}
		inputParam["source"] = "FutureBillPaymentAndTopUp";
		inputParam["Locale"] = kony.i18n.getCurrentLocale();
		
		inputParam["activityTypeID"] = activityTypeID;
		inputParam["activityFlexValues1"] = activityFlexValues1;
		inputParam["activityFlexValues2"] = activityFlexValues2;
		inputParam["activityFlexValues3"] = activityFlexValues3;
		inputParam["activityFlexValues4"] = activityFlexValues4;
		inputParam["activityFlexValues5"] = activityFlexValues5;
		if(frmEditFutureBillPaymentConfirm.lblEndOnDate.text != "" || frmEditFutureBillPaymentConfirm.lblEndOnDate.text != "-"){
			inputParam["NotificationAdd_endDt"] = inputParam["endDt"] + " - " + frmEditFutureBillPaymentConfirm.lblExecutetimes.text + " "+  kony.i18n.getLocalizedString("keyTimesMB");

		}
		var scheduleDetails = "";
		if(frequency == "Once"){
	 		scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frequency;
		 }else if(frmEditFutureBillPaymentConfirm.lblExecutetimes.text == "-"){
		 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frequency;
		 } else{
		 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text+ " " + kony.i18n.getLocalizedString("keyTo") + " " +frmEditFutureBillPaymentConfirm.lblEndOnDate.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmEditFutureBillPaymentConfirm.lblExecutetimes.text +" "+kony.i18n.getLocalizedString("keyTimesIB") ;
		 }
	 	inputParam["PaymentSchedule"] = scheduleDetails;
	 	
		invokeServiceSecureAsync("FutureBillPaymentEditExecute", inputParam, callBackEditBillPayVerifyPWDMB)
}

function callBackEditBillPayVerifyPWDMB(status, result) {


	if (status == 400) {
		if (result["opstatusVPX"] == 0) {
			popupTractPwd.dismiss();
            onClickCancelAccessPin();
			gblRetryCountRequestOTP = "0";
			editBillPaymentFlowMB(result);
		} else {
		          dismissLoadingScreen();
					gblRtyCtrVrfyTxPin = result["retryCounterVerifyTransPwd"];
				if (result["errCode"] == "VrfyAcPWDErr00001" || result["errCode"] == "VrfyAcPWDErr00002"){
                    popupEnterAccessPin.lblWrongPin.setVisibility(true);
                    kony.print("invalid pin transfer flow"); //To do : set red skin to enter access pin
                    resetAccessPinImg(result["badLoginCount"]);
                    return false;
                }else if (result["errCode"] == "VrfyAcPWDErr00003"){
                    onClickCancelAccessPin();
                    gotoUVPINLockedScreenPopUp();
                    return false;                  
                }else if (result["errCode"] == "VrfyTxPWDErr00001") {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen(); // keyECUserNotFound
					showAlert(kony.i18n.getLocalizedString("invalidOTP"), kony.i18n.getLocalizedString("info"));
					return false;
				} else if (result["errCode"] == "VrfyTxPWDErr00002") {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen();
					 popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
					 popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
					 popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
					 popupTractPwd.hbxPopupTranscPwd.skin = txtErrorBG;
					 popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
					 popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
					//
					 return false;
				} else if (result["errCode"] == "VrfyTxPWDErr00003") {
				
					showTranPwdLockedPopup();
					dismissLoadingScreen();
					return false;
				} else if (result["errCode"] == "JavaErr00001") {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen();
					showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
					return false;
				}else if (result["errCode"] == "EditPaymentErr001") {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen();
					showAlert("Transaction Can Not Be Preocessed", kony.i18n.getLocalizedString("info"));
					return false;
				}
				 else {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen();
					showAlert(kony.i18n.getLocalizedString("TRErr_Common"), kony.i18n.getLocalizedString("info"));
					return false;
				 }
			
		}
	}
}

function editBillPaymentFlowMB(result) {
  	//alert("gblAmountSelectedMB=" + gblAmountSelectedMB + " gblScheduleFreqChangedMB=" + gblScheduleFreqChangedMB);
	if (gblAmountSelectedMB == true && gblScheduleFreqChangedMB == true) {
		callBackPmtCanServiceForEditBPMB(result);
	} else if (gblAmountSelectedMB == false && gblScheduleFreqChangedMB == true) {
		callBackPmtCanServiceForEditBPMB(result);
	} else if (gblAmountSelectedMB == true && gblScheduleFreqChangedMB == false) {
		callBackPmtModServiceForEditBPMB(result);
		gblAmountSelectedMB = false;
		gblScheduleFreqChangedMB = false;
	}
}

function callBackPmtCanServiceForEditBPMB(result) {
		if (result["opstatusCancel"] == 0) {
			var StatusCode = result["statusCodeCancel"];
			var StatusDesc = result["statusDescCancel"];
			if (StatusCode == 0) {
				var refNum = result["transRefNum"];
                frmBillPaymentEditFutureComplete.lblTxnNumValue.text = refNum;
				callBackPmtAddServiceForEditBPMB(result, frmBillPaymentEditFutureComplete); 
			} else {
				gblAmountSelectedMB = false;
				gblScheduleFreqChangedMB = false;
				showAlert(kony.i18n.getLocalizedString("TRErr_Common"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreen();
				return false;
			}
		} else {
			gblAmountSelectedMB = false;
			gblScheduleFreqChangedMB = false;
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	
}

function callBackPmtAddServiceForEditBPMB(result, currentForm) {
        gblAmountSelectedMB = false;
		gblScheduleFreqChangedMB = false;
		if (result["opstatusPayAdd"] == 0) {
			var StatusCode = result["statusCodeAdd"];
			var StatusDesc = result["statusDescAdd"];
			if (StatusCode == 0) {
				currentForm.lblPaymentDateValue.text = result["currentTime"];
				populateEditBillPaymentMBCompleteScreen();
				//callNotificationAddServiceMB();
			} else {
				alert(result["statusDescAdd"]);
				dismissLoadingScreen();
				return false;
			}
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
}

function callBackPmtModServiceForEditBPMB(result) {
        var logLinkageId = "";
		if (result["opstatusBP"] == 0) {
			var StatusCode = result["StatusCode"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
                 frmBillPaymentEditFutureComplete.lblTxnNumValue.text = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text;
  	 			 frmBillPaymentEditFutureComplete.lblPaymentDateValue.text = frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text;
				 populateEditBillPaymentMBCompleteScreen();
				// callNotificationAddServiceMB();
			} else {
				dismissLoadingScreen();
				alert(kony.i18n.getLocalizedString("TRErr_Common"));
				return false;
			}
		} else {
			dismissLoadingScreen();
			alert(" " + result["opstatusBP"]);
		}
}

function populateEditBillPaymentMBCompleteScreen() {
		if(gblBPflag){
			gotoBillPaymentFutureCompleteMB();
		}else{
			gotoTopupFutureCompleteMB();
		}
}

function gotoTopupFutureCompleteMB(){

	if (frmEditFutureBillPaymentConfirm.lblBillerNickName.isVisible) {
      	frmBillPaymentEditFutureComplete.lblBillerNickname.text = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text;
		frmBillPaymentEditFutureComplete.hbxAddToMyBills.setVisibility(false);
		gblBillerPresentInMyBills = true;
    }else{
      	gblBillerPresentInMyBills = false;
		frmBillPaymentEditFutureComplete.hbxAddToMyBills.setVisibility(true);
	}
	frmBillPaymentEditFutureComplete.line968116430627578.setVisibility(false);
	frmBillPaymentEditFutureComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
    frmBillPaymentEditFutureComplete.hbxFreeTrans.setVisibility(false);  
    //added for New UI
  	var datetime = frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text.split(" ");
  	
    frmBillPaymentEditFutureComplete.lblPaymentDateValue.text = datetime[0] +" [" +datetime[1] + "]";
     if (!frmBillPaymentEditFutureComplete.hbxStartEndDate.isVisible && !frmBillPaymentEditFutureComplete.hbxRepeatExcuteTimes.isVisible) {
        frmBillPaymentEditFutureComplete.hbxStartEndDate.setVisibility(true);
        frmBillPaymentEditFutureComplete.hbxRepeatExcuteTimes.setVisibility(true);
    } 
    frmBillPaymentEditFutureComplete.hbxBalanceAfter.setVisibility(false); //no amount balance field in future	
  	frmBillPaymentEditFutureComplete.vbox156335099531837.setVisibility(false);
  	frmBillPaymentEditFutureComplete.line6801452681250922.setVisibility(false);		 
    frmBillPaymentEditFutureComplete.lblBillerNickname.text = frmEditFutureBillPaymentConfirm.lblBillerNickName.text;
    frmBillPaymentEditFutureComplete.lblRef1.text = frmEditFutureBillPaymentConfirm.lblref1.text;
    //below line is changed for CR - PCI-DSS masked Credit card no
    frmBillPaymentEditFutureComplete.lblRef1ValueMasked.text = frmEditFutureBillPaymentConfirm.lblRef1ValueMasked.text;
    if (frmEditFutureBillPaymentConfirm.hbxRef2.isVisible) {
        frmBillPaymentEditFutureComplete.hbxRef2.setVisibility(true);
        frmBillPaymentEditFutureComplete.lblRef2.text = frmEditFutureBillPaymentConfirm.lblRef2.text;
        frmBillPaymentEditFutureComplete.lblRef2Value.text = frmEditFutureBillPaymentConfirm.lblRef2Value.text;
    } else {
        frmBillPaymentEditFutureComplete.hbxRef2.setVisibility(false);
    }
    //if (gblEasyPassTopUp) {
    //    frmBillPaymentEditFutureComplete.hbxEasyPass.setVisibility(true);
    //    frmBillPaymentEditFutureComplete.hbxEasyPassTxnId.setVisibility(true);
    //    frmBillPaymentEditFutureComplete.hbxCardBal.setVisibility(true);
    //} else {
        frmBillPaymentEditFutureComplete.hbxEasyPass.setVisibility(false);
        frmBillPaymentEditFutureComplete.hbxEasyPassTxnId.setVisibility(false);
        frmBillPaymentEditFutureComplete.hbxCardBal.setVisibility(false);
    //}
  
    frmBillPaymentEditFutureComplete.lblAccUserName.text = frmEditFutureBillPaymentConfirm.lblFromAccountName.text;
  //Preshow Fixcode
  	  	frmBillPaymentEditFutureComplete.lblPaymentFeeValueH.text = "[" + frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text +" " +kony.i18n.getLocalizedString("currencyThaiBaht") +"]";
    frmBillPaymentEditFutureComplete.lblPaymentFeeValue.text = frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text;
    frmBillPaymentEditFutureComplete.lblAmountValue.text = frmEditFutureBillPaymentConfirm.lblAmtValue.text;
    frmBillPaymentEditFutureComplete.imgBillerPic.src = frmEditFutureBillPaymentConfirm.imgBiller.src;
    frmBillPaymentEditFutureComplete.imgFromAccount.src = frmEditFutureBillPaymentConfirm.imgFrom.src;
    frmBillPaymentEditFutureComplete.lblAccountName.text = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text;
    frmBillPaymentEditFutureComplete.lblAccountNum.text = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text;
    frmBillPaymentEditFutureComplete.lblStartOnValue.text = frmEditFutureBillPaymentConfirm.lblStartDate.text;
    frmBillPaymentEditFutureComplete.lblEndOnValue.text = frmEditFutureBillPaymentConfirm.lblEndOnDate.text;
    gblScheduleRepeatBP = frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
    frmBillPaymentEditFutureComplete.lblExecuteValue.text = frmEditFutureBillPaymentConfirm.lblExecutetimes.text + " "+  kony.i18n.getLocalizedString("keyTimesMB");;

   	frmBillPaymentEditFutureComplete.hbox101271281131304.setVisibility(true);
   	//gblDisplayBalanceBillPayment = true;
    frmBillPaymentEditFutureComplete.imgHeaderMiddle.setVisibility(false);
    frmBillPaymentEditFutureComplete.hbxShareOption.setVisibility(true);
	frmBillPaymentEditFutureComplete.hbxspace.setVisibility(false);
	if(frmBillPaymentEditFutureNew.flxFeewave.isVisible){
		frmBillPaymentEditFutureComplete.hbxScheduleNote.setVisibility(true);
	}else{
		frmBillPaymentEditFutureComplete.hbxScheduleNote.setVisibility(false);
	}
  	GblBillTopFlag = true;
  	gblPaynow = false;
    gblFirstTimeBillPayment = true;
    gblFirstTimeTopUp = true;
    gblBPScheduleFirstShow = true;
    gblScheduleEndBP = "none";
    gblPenalty = false;
    gblFullPayment = false;
    gblBillpaymentNoFee = false;
  	gblMyBillerTopUpBB = "1";
	frmBillPaymentEditFutureComplete.show();
    dismissLoadingScreen();
}

function gotoBillPaymentFutureCompleteMB() {
	if (frmEditFutureBillPaymentConfirm.lblBillerNickName.isVisible) {
      	frmBillPaymentEditFutureComplete.lblBillerNickname.text = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text;
		frmBillPaymentEditFutureComplete.hbxAddToMyBills.setVisibility(false);
		gblBillerPresentInMyBills = true;
    }else{
      	gblBillerPresentInMyBills = false;
		frmBillPaymentEditFutureComplete.hbxAddToMyBills.setVisibility(true);
	}
	frmBillPaymentEditFutureComplete.line968116430627578.setVisibility(false);
	frmBillPaymentEditFutureComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
    frmBillPaymentEditFutureComplete.hbxFreeTrans.setVisibility(false);  
    //added for New UI
  	var datetime = frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text.split(" ");
  	
    frmBillPaymentEditFutureComplete.lblPaymentDateValue.text = datetime[0] +" [" +datetime[1] + "]";
     if (!frmBillPaymentEditFutureComplete.hbxStartEndDate.isVisible && !frmBillPaymentEditFutureComplete.hbxRepeatExcuteTimes.isVisible) {
        frmBillPaymentEditFutureComplete.hbxStartEndDate.setVisibility(true);
        frmBillPaymentEditFutureComplete.hbxRepeatExcuteTimes.setVisibility(true);
    } 
    frmBillPaymentEditFutureComplete.hbxBalanceAfter.setVisibility(false); //no amount balance field in future	
  	frmBillPaymentEditFutureComplete.vbox156335099531837.setVisibility(false);
  	frmBillPaymentEditFutureComplete.line6801452681250922.setVisibility(false);		 
    frmBillPaymentEditFutureComplete.lblBillerNickname.text = frmEditFutureBillPaymentConfirm.lblBillerNickName.text;
    frmBillPaymentEditFutureComplete.lblRef1.text = frmEditFutureBillPaymentConfirm.lblref1.text;
    //below line is changed for CR - PCI-DSS masked Credit card no
    frmBillPaymentEditFutureComplete.lblRef1ValueMasked.text = frmEditFutureBillPaymentConfirm.lblRef1ValueMasked.text;
    if (frmEditFutureBillPaymentConfirm.hbxRef2.isVisible) {
        frmBillPaymentEditFutureComplete.hbxRef2.setVisibility(true);
        frmBillPaymentEditFutureComplete.lblRef2.text = frmEditFutureBillPaymentConfirm.lblRef2.text;
        frmBillPaymentEditFutureComplete.lblRef2Value.text = frmEditFutureBillPaymentConfirm.lblRef2Value.text;
    } else {
        frmBillPaymentEditFutureComplete.hbxRef2.setVisibility(false);
    }
    //if (gblEasyPassTopUp) {
    //    frmBillPaymentEditFutureComplete.hbxEasyPass.setVisibility(true);
    //    frmBillPaymentEditFutureComplete.hbxEasyPassTxnId.setVisibility(true);
    //    frmBillPaymentEditFutureComplete.hbxCardBal.setVisibility(true);
    //} else {
        frmBillPaymentEditFutureComplete.hbxEasyPass.setVisibility(false);
        frmBillPaymentEditFutureComplete.hbxEasyPassTxnId.setVisibility(false);
        frmBillPaymentEditFutureComplete.hbxCardBal.setVisibility(false);
    //}
  
    frmBillPaymentEditFutureComplete.lblAccUserName.text = frmEditFutureBillPaymentConfirm.lblFromAccountName.text;
  //Preshow Fixcode
  	frmBillPaymentEditFutureComplete.lblPaymentFeeValueH.text = "[" + frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text +" " +kony.i18n.getLocalizedString("currencyThaiBaht") +"]";
    frmBillPaymentEditFutureComplete.lblPaymentFeeValue.text = frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text;
    frmBillPaymentEditFutureComplete.lblAmountValue.text = frmEditFutureBillPaymentConfirm.lblAmtValue.text;
    frmBillPaymentEditFutureComplete.imgBillerPic.src = frmEditFutureBillPaymentConfirm.imgBiller.src;
    frmBillPaymentEditFutureComplete.imgFromAccount.src = frmEditFutureBillPaymentConfirm.imgFrom.src;
    frmBillPaymentEditFutureComplete.lblAccountName.text = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text;
    frmBillPaymentEditFutureComplete.lblAccountNum.text = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text;
    frmBillPaymentEditFutureComplete.lblStartOnValue.text = frmEditFutureBillPaymentConfirm.lblStartDate.text;
    frmBillPaymentEditFutureComplete.lblEndOnValue.text = frmEditFutureBillPaymentConfirm.lblEndOnDate.text;
    gblScheduleRepeatBP = frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
    frmBillPaymentEditFutureComplete.lblExecuteValue.text = frmEditFutureBillPaymentConfirm.lblExecutetimes.text + " "+  kony.i18n.getLocalizedString("keyTimesMB");;

   	frmBillPaymentEditFutureComplete.hbox101271281131304.setVisibility(true);
   	//gblDisplayBalanceBillPayment = true;
    frmBillPaymentEditFutureComplete.imgHeaderMiddle.setVisibility(false);
    frmBillPaymentEditFutureComplete.hbxShareOption.setVisibility(true);
	frmBillPaymentEditFutureComplete.hbxspace.setVisibility(false);
	if(frmBillPaymentEditFutureNew.flxFeewave.isVisible){
		frmBillPaymentEditFutureComplete.hbxScheduleNote.setVisibility(true);
	}else{
		frmBillPaymentEditFutureComplete.hbxScheduleNote.setVisibility(false);
	}
  	GblBillTopFlag = true;
  	gblPaynow = false;
    gblFirstTimeBillPayment = true;
    gblFirstTimeTopUp = true;
    gblBPScheduleFirstShow = true;
    gblScheduleEndBP = "none";
    gblPenalty = false;
    gblFullPayment = false;
    gblBillpaymentNoFee = false;
  	gblMyBillerTopUpBB = "0";
	frmBillPaymentEditFutureComplete.show();
    dismissLoadingScreen();
}

/*
   **************************************************************************************
		Module	: callBackNotificationAddService
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackNotificationAddServiceMB(status, result) {


	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				//populateEditBillPaymentCompleteScreen();
				
			} else {
				
				return false;
			}
		} else {
			
		}
	}
}









function deleteFutureBillPaymentFromViewPageMB() {
	var inputparam = [];
  	var currentForm;
	inputparam["rqUID"] = "";
	//inputparam["PmtId"] = ""; 
  	currentForm = frmBillPaymentEditFutureNew;
	if(gblBPflag){
		inputparam["scheID"] ="SB"+frmBillPaymentEditFutureNew.lblScheduleRefValue.text.substring(2, frmBillPaymentEditFutureNew.lblScheduleRefValue.text.length); //get this value from Calender or future transactions
	}else{
		inputparam["scheID"] = frmBillPaymentEditFutureNew.lblScheduleRefValue.text.replace("F", "S");
	}
	
    var logLinkageId = "";
	if(gblBPflag){
		inputparam["EditFTFlow"] = "EditBP";
	}else
		inputparam["EditFTFlow"] = "EditTP";
	
	inputparam["isCancelFlow"] = "true";
	inputparam["activityFlexValues1"] = "Cancel";
	inputparam["activityFlexValues2"] = currentForm.lblBillerName.text +"+"+ removeHyphenIB(currentForm.lblRef1value.text);     //Biller Name + Ref1
	inputparam["activityFlexValues3"] = dateFormatForDisplay(gblstartOnMB);
	inputparam["activityFlexValues4"] = gblOnLoadRepeatAsMB
	inputparam["activityFlexValues5"] = gblAmountMB;
	
	invokeServiceSecureAsync("doPmtCan", inputparam, callBackPmtCanServiceForDelBPMB)
}

function callBackPmtCanServiceForDelBPMB(status, result) {
  	var currentForm;
	if (status == 400) {
		var activityTypeID = "";
      	currentForm = frmBillPaymentEditFutureNew;
		if(gblBPflag){
			activityTypeID = "067";
		}else{
			activityTypeID = "068";
		}
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Cancel";
        var activityFlexValues2 = currentForm.lblBillerName.text +"+"+ removeHyphenIB(currentForm.lblRef1value.text);     //Biller Name + Ref1
		var activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB;  //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
		
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				activityStatus = "01";
				//Commented as  financial Activity is not required 
				/*
					var finSchduleRefId ="";	
				if(gblBPflag){
					finSchduleRefId = "SB" +frmBillPaymentView.lblScheduleRefValue.text.substring(2, frmBillPaymentView.lblScheduleRefValue.text.length);  
				}else{
					finSchduleRefId = frmBillPaymentView.lblScheduleRefValue.text.replace("F", "S"); 
				}
        	 	setFinancialActivityLogFTDelete(finSchduleRefId);
        	 	*/  
				//alert("Deleted Sucessfully");
				 alert(kony.i18n.getLocalizedString("ScheduledPayment_Delete")); 
				 /*
				 frmMBMyActivities.show();
				 showCalendar(gsSelectedDate,frmMBMyActivities,1);
				 */
				 MBMyActivitiesReloadAndShowCalendar();
			} else {
				activityStatus = "02";
				alert(result["StatusDesc"]);
				return false;
			}
		} else {
			activityStatus = "02";
			alert(result["errMsg"]);
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);	
	}
}


function checkMBStatusForDeleteMB(){
	var inputparam = [];
	inputparam["crmId"] = "";
	inputparam["rqUUId"] = "";
	showLoadingScreen();	
	invokeServiceSecureAsync("crmProfileInq", inputparam, callBackCrmProfileInqForDeleteMB);

}

function callBackCrmProfileInqForDeleteMB(status,result){
 if (status == 400) {
  if (result["opstatus"] == 0) {
   var statusCode = result["statusCode"];
   var severity = result["Severity"];
   var statusDesc = result["StatusDesc"];
   if (statusCode != 0) {
    alert(statusDesc);
   } else {
    var mbStat = result["mbFlowStatusIdRs"];
    var ibStat = result["ibUserStatusIdTr"];
    if (mbStat != null) {
     var inputParam = [];
     inputParam["mbStatus"] = mbStat; //check this inputparam
	 inputParam["ibStatus"]= ibStat;
     invokeServiceSecureAsync("checkIBMBStatus", inputParam, callBackCheckStatusMBForDelete);
    }
   }
  } else {
   dismissLoadingScreen();
   alert(result["errMsg"]);
  }
 } else {
  if (status == 300) {
   dismissLoadingScreen();
   //
  }
 }
}

function callBackCheckStatusMBForDelete(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
				if(flowSpa)
					{
						if (result["ibStatusFlag"] == "true") {
							//showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"),kony.i18n.getLocalizedString("info"));
							popTransferConfirmOTPLock.show();
							dismissLoadingScreen();	
							return false;
							}
						else
						{
							
							dismissLoadingScreen();	
						if(gblBPflag){
						popupEditBPDele.lblDeleteMsg.text =	kony.i18n.getLocalizedString("BPDeletesureMessage");//"Are you sure to delete this Bill Payment transaction ?";
						}else{
						popupEditBPDele.lblDeleteMsg.text =	kony.i18n.getLocalizedString("BPDeletesureMessage");//"Are you sure to delete this Top Up Payment transaction ?";
						}
						popupEditBPDele.show();
					}
					}
					else 
				{
				if (result["mbStatusFlag"] == "true") {
				popTransferConfirmOTPLock.show();
				//alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
				dismissLoadingScreen();	
				return false;
				} 
				else {
				
				dismissLoadingScreen();	
				if(gblBPflag){
					popupEditBPDele.lblDeleteMsg.text =	kony.i18n.getLocalizedString("BPDeletesureMessage");//"Are you sure to delete this Bill Payment transaction ?";
				}else{
					popupEditBPDele.lblDeleteMsg.text =	kony.i18n.getLocalizedString("BPDeletesureMessage");//"Are you sure to delete this Top Up Payment transaction ?";
				}
				popupEditBPDele.show();
			}
			}
		} else {
			dismissLoadingScreen();	
			alert("  " + result["opstatus"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreen();	
			//
		}
	}
}



//Generating PDF and Image 
function onClickGeneratePDFImageForEditMB(filetype){
	var inputParam = {};
	var scheduleDetails ="";
	var ref2Flag = false;
	var pdfImagedata ={};
	//fileTypeForEdit = filetype;
	var AccountNo = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text;
	if(AccountNo.indexOf("-") != -1)AccountNo = replaceCommon(AccountNo, "-", "");
	var len = AccountNo.length;
	if(len == 10){
		AccountNo = "XXX-X-"+AccountNo.substring(len-6, len-1)+"-X";
	}else
	    AccountNo = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text;
	
	if(frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text == "Once"){
	 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text + " "+ " Repeat " +frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
	 }else if(frmEditFutureBillPaymentConfirm.lblExecutetimes.text == "-"){
	 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text + " "+ " Repeat " +frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
	 } else{
	 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text +" to " +frmEditFutureBillPaymentConfirm.lblEndOnDate.text + " "+ " Repeat " +frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text +" for "+frmEditFutureBillPaymentConfirm.lblExecutetimes.text+" times " ;
	 }
	 if(frmEditFutureBillPaymentConfirm.hbxRef2.isVisible){
	 	ref2Flag = true;
	 }
	 if(ref2Flag){
	 		 pdfImagedata = {
     			"AccountNo" : AccountNo,
     			"AccountName" :frmEditFutureBillPaymentConfirm.lblFromAccountName.text ,
     			"BillerName" : frmEditFutureBillPaymentConfirm.lblBillerName.text,
     			"Ref1Label" : frmEditFutureBillPaymentConfirm.lblref1.text,
	 			"Ref1Value" : frmEditFutureBillPaymentConfirm.lblRef1Value.text,
	 			"Ref2Label":frmEditFutureBillPaymentConfirm.lblRef2.text,
	 			"Ref2Value":frmEditFutureBillPaymentConfirm.lblRef2Value.text,
				"Amount" : frmEditFutureBillPaymentConfirm.lblAmtValue.text,
	 			"Fee" : frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text,	
	 			"PaymentOrderDate"	: frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text,
	 			"PaymentSchedule":scheduleDetails,
	 			"MyNote":frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text ,
	 			"TransactionRefNo": frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text,
	 			"localeId":kony.i18n.getCurrentLocale() 
   		 }
	 }else{
	 
	 		 pdfImagedata = {
    		 "AccountNo" : AccountNo,
     		 "AccountName" :frmEditFutureBillPaymentConfirm.lblFromAccountName.text ,
     		 "BillerName" : frmEditFutureBillPaymentConfirm.lblBillerName.text,
     		 "Ref1Label" : frmEditFutureBillPaymentConfirm.lblref1.text,
	 		 "Ref1Value" : frmEditFutureBillPaymentConfirm.lblRef1Value.text,
	         "Amount" : frmEditFutureBillPaymentConfirm.lblAmtValue.text,
	 		 "Fee" : frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text,	
	 		 "PaymentOrderDate"	: frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text,
	 		 "PaymentSchedule":scheduleDetails,
			 "MyNote":frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text ,
	 		"TransactionRefNo": frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text,
	 		"localeId":kony.i18n.getCurrentLocale() 
    	}
	 
	 }

    inputParam["filetype"] = filetype;
    var activityTypeId = "";
    if(gblBPflag){
    	activityTypeId = "067";
    	inputParam["templatename"] = "FutureBillPaymentTemplate";
    	inputParam["outputtemplatename"]= "Future_Bill_Payment_Set_Details_"+kony.os.date("ddmmyyyy");
    }else{
    	activityTypeId = "068";
    	inputParam["templatename"] = "FutureTopUpPaymentComplete";
    	inputParam["outputtemplatename"] =  "Future_Top_Up_Set_Details_"+kony.os.date("ddmmyyyy");
    }
    inputParam["Ref2Falg"] = ref2Flag;
   // inputParam["localeId"]= kony.i18n.getCurrentLocale();
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    // var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext+"/GenericPdfImageServlet";
	//var url = "http://10.11.12.212:9080/tmb/GenericPdfImageServlet";

	//invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);

 //	kony.net.invokeServiceAsync(url, inputParam, callbackfunctionForPDFImageMBBPTP);

	saveFuturePDF(filetype, activityTypeId, frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text);
}



function shareFBMBBPTP(){

		url="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.tmbbank.com&t";
	//	frmTransferGeneratePDF.browTransferPDF.requestURLConfig={URL:url,requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"}
	    frmContactusFAQMB.browser506459299404741.requestURLConfig={URL:url,requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"}
	    frmContactusFAQMB.hbox475124774143.setVisibility(false);
	    frmContactusFAQMB.hbox476018633224052.setVisibility(false);
	    frmContactusFAQMB.button506459299404751.setVisibility(false);
		frmContactusFAQMB.show();
		return false;
}

function isBPScheduleEdited(){
	gblScheduleFreqChangedMB = false;
	
	var tmpDate = formatDateFT(gblstartOnMB);
	var currentForm;
    currentForm = frmBillPaymentEditFutureNew;
    var currentRepeatAs = currentForm.labelRepeatAsValue.text;  
    var feq = "";
		if (kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyDailyNT")) || kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyDailyNE"))) {
			feq = "Daily";
		}
		if (kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyWeeklyNT")) || kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyWeeklyNE"))) {
			feq = "Weekly";
		}
		if (kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyMonthlyNT")) || kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyMonthlyNE"))) {
			feq = "Monthly";
		}
		if (kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyYearlyNT")) || kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyYearlyNE"))) {
			feq = "Yearly";
		}
		if(kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyOnceNT")) || kony.string.equalsIgnoreCase(currentRepeatAs, kony.i18n.getLocalizedString("keyOnceNE"))) 
		{
			feq = "Once";
		}
		      
	if(tmpDate != currentForm.lblStartDate.text){
		gblScheduleFreqChangedMB = true;
	}else{
		gblScheduleFreqChangedMB = false;
		if(gblOnLoadRepeatAsMB != feq){
			gblScheduleFreqChangedMB = true;
		}else{
			gblScheduleFreqChangedMB = false;
			
			if(gblEndingFreqOnLoadMB !=  gblScheduleEndBPMB){
				gblScheduleFreqChangedMB = true;  //OnClickEndFreqMB
			}else{
				 gblScheduleFreqChangedMB = false;
				if(gblEndingFreqOnLoadMB == kony.i18n.getLocalizedString("keyOnDate") || gblEndingFreqOnLoadMB == "OnDate"){
					 var temp = gblendDateOnLoadMB;
					 if(temp.indexOf("-",0) != -1){
			         	temp = formatDateFT(gblFTViewEndDate);
			       	 }
			       	 if(temp != currentForm.lblEndOnDate.text)
			       	 		 gblScheduleFreqChangedMB = true;
				}else if(gblEndingFreqOnLoadMB == kony.i18n.getLocalizedString("keyAfter") || gblEndingFreqOnLoadMB == "After"){
				
					if(parseFloat(gblexecutionOnLoadMB.toString()) != parseFloat(currentForm.lblExecutetimes.text))
									gblScheduleFreqChangedMB = true;
					
				}
			}
		}
	}
	onClickNextEditPage();	
	}

function returnToEditBillPaymentFuture() {
	frmBillPaymentEditFutureNew.lblStartDate.text = gblTempTPStartOnDateMB;
	frmBillPaymentEditFutureNew.lblEndOnDate.text = gblTempTPEndOnDateMB;
	frmBillPaymentEditFutureNew.lblExecutetimes.text = gblTempTPRepeatAsMB;
	frmBillPaymentEditFutureNew.show();
	repeatAsMB = gblTemprepeatAsTPMB;
	endFreqSaveMB = gblTempendFreqSaveTPMB;
 
	if (repeatAsMB == "" || endFreqSaveMB == "") {
		if (gblEndingFreqOnLoadMB == "Never"){
			OnClickRepeatAsMB = "";
			OnClickEndFreqMB = "";
		}else if(gblOnLoadRepeatAsMB == "Once"){
			OnClickRepeatAsMB = repeatAsIB;
			OnClickEndFreqMB = endFreqSave;
		}else {
			OnClickRepeatAsMB = "";
			OnClickEndFreqMB = "";
		}
	} else {
		OnClickRepeatAsMB = repeatAsMB;
		OnClickEndFreqMB = endFreqSaveMB;
	}
}

function repeatScheduleFutureButtonSet(eventObject) {
	if (!gblScheduleTransfers) {
		if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
			alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
			return;
		}
	}
	var isCurrentDate = validateCurrentDate("setSchedule");
   	if (isCurrentDate){
		return;
   	}
	if (gblBPScheduleFirstShow) {
		enableEndAfterButtonHolder();
	}
	
	if(gblScheduleEndBP == null || gblScheduleEndBP == "" || gblScheduleEndBP == "none") {
		gblScheduleEndBP = "Never";
	}
	setRepeatClickedToFalse();
	var buttonId = eventObject.id;
	if (kony.string.equalsIgnoreCase(buttonId, "btnDaily")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleLeftFocus")){
			frmScheduleBillPay.btnDaily.skin = "btnScheduleLeft";
			gblScheduleRepeatBP = "Once";
			disableEndAfterButtonHolder();
		}else{
			frmScheduleBillPay.btnDaily.skin = "btnScheduleLeftFocus";
			gblScheduleRepeatBP = "Daily";
			DailyClicked = true;
		}
		frmScheduleBillPay.btnWeekly.skin = "btnScheduleMid";
		frmScheduleBillPay.btnMonthly.skin = "btnScheduleMid";
		frmScheduleBillPay.btnYearly.skin = "btnScheduleRight";
	} else if (kony.string.equalsIgnoreCase(buttonId, "btnWeekly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleMidFocus")){
			frmScheduleBillPay.btnWeekly.skin = "btnScheduleMid";
			gblScheduleRepeatBP = "Once";
			disableEndAfterButtonHolder();
		}else{
			frmScheduleBillPay.btnWeekly.skin = "btnScheduleMidFocus";
			weekClicked = true;
			gblScheduleRepeatBP = "Weekly";
		}
		frmScheduleBillPay.btnDaily.skin = "btnScheduleLeft";
		frmScheduleBillPay.btnMonthly.skin = "btnScheduleMid";
		frmScheduleBillPay.btnYearly.skin = "btnScheduleRight";
	} else if (kony.string.equalsIgnoreCase(buttonId, "btnMonthly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleMidFocus")){
			frmScheduleBillPay.btnMonthly.skin = "btnScheduleMid";
			disableEndAfterButtonHolder();
			gblScheduleRepeatBP = "Once";
		}else{
			frmScheduleBillPay.btnMonthly.skin = "btnScheduleMidFocus";
			monthClicked = true;
			gblScheduleRepeatBP = "Monthly";
		}
		frmScheduleBillPay.btnDaily.skin = "btnScheduleLeft";
		frmScheduleBillPay.btnWeekly.skin = "btnScheduleMid";
		frmScheduleBillPay.btnYearly.skin = "btnScheduleRight";
	} else if (kony.string.equalsIgnoreCase(buttonId, "btnYearly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleRightFocus")){
			frmScheduleBillPay.btnYearly.skin = "btnScheduleRight";
			gblScheduleRepeatBP = "Once";
			disableEndAfterButtonHolder();
		}else{
			frmScheduleBillPay.btnYearly.skin = "btnScheduleRightFocus";
			YearlyClicked = true;
			gblScheduleRepeatBP = "Yearly";
		}
		frmScheduleBillPay.btnDaily.skin = "btnScheduleLeft";
		frmScheduleBillPay.btnWeekly.skin = "btnScheduleMid";
		frmScheduleBillPay.btnMonthly.skin = "btnScheduleMid";
	}else{
		gblScheduleRepeatBP = "Once";
	}
}

function openComboBox(){
   popUpMyBillers.lblFlag.text = "frmBillPaymentEditFutureNew";
   frmTopUpAmount.show();

}
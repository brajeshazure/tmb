







function getTokenActivationCode(){

	showLoadingScreenPopup();
	var tokenActiInputParams = {};
	tokenActiInputParams["crmId"] = gblcrmId;
	tokenActiInputParams["serialNum"] = frmIBTokenActivationPage.tbxTokenSerialNo.text;
	if(frmIBTokenActivationPage.tbxTokenSerialNo.text == null || frmIBTokenActivationPage.tbxTokenSerialNo.text.trim()== ""){
			alert(kony.i18n.getLocalizedString("keyTokenSerialNo"));
			dismissLoadingScreenPopup();
 			return false;	
	}
	invokeServiceSecureAsync("getTokenActivationCode", tokenActiInputParams, getTokenActivationCodeCallBack);
}

function getTokenActivationCodeCallBack(status, callBackResponse) {

    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
        	dismissLoadingScreenPopup();
            
            
			var statusNumber = callBackResponse["TokenActivation"][0]["TOKEN_SERIAL_NO"];
			//alert(statusNumber);
			kony.application.dismissLoadingScreen();
            frmIBTokenActivationPage.hbxSerialNo.setVisibility(false);
            frmIBTokenActivationPage.hbxConfirm.setVisibility(true);
            frmIBTokenActivationPage.hbxSerialNumber.setVisibility(false);
            frmIBTokenActivationPage.hbxTokenNumber.setVisibility(true);
            frmIBTokenActivationPage.lblTokenHeader.text = kony.i18n.getLocalizedString("keylblConfirmation");
			frmIBTokenActivationPage.lblTokenNumber.text=statusNumber;
			frmIBTokenActivationPage.tbxTokenOTP.setFocus(true);
			tokenActivationParamsInSession(frmIBTokenActivationPage.tbxTokenSerialNo.text);
        } else if(callBackResponse["opstatus"] == "9"){
       		 
			alert(kony.i18n.getLocalizedString("keyTokenBlock"));
			dismissLoadingScreenPopup();
			return false;
   	    }
        else if(callBackResponse["opstatus"] == "1"){
        	
			alert(kony.i18n.getLocalizedString("KeyTokenStatusError"));
			dismissLoadingScreenPopup();
			return false;
   	    }else if(callBackResponse["opstatus"] == "2"){
   	    	
			alert(kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
			dismissLoadingScreenPopup();
			return false;
    	} else {
    		
			alert(kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
			dismissLoadingScreenPopup();
			return false;
    	}
    }
         
}

function tokenActivationParamsInSession(serialNo) {
    inputParam = {};
	inputParam["param1"] = serialNo.toString();
	invokeServiceSecureAsync("SaveParamsForTokenActivation", inputParam, tokenActivationParamsInSessionCallBack);
}

function tokenActivationParamsInSessionCallBack(status, result){
    if (status == 400) {
		if (result["opstatus"] == 0) {
		}
	}
}


function cancelActivationPage(){
	 showLoadingScreenPopup();
	 frmIBTokenActivationPage.lblTokenHeader.text = kony.i18n.getLocalizedString("tokenActivation");
	 frmIBTokenActivationPage.hbxSerialNo.setVisibility(true);
	 frmIBTokenActivationPage.tbxTokenSerialNo.text="";
     frmIBTokenActivationPage.hbxConfirm.setVisibility(false);
    	frmIBTokenActivationPage.hbxSerialNumber.setVisibility(true);
       frmIBTokenActivationPage.hbxTokenNumber.setVisibility(false);
     frmIBTokenActivationPage.btnTokenSNoNext.text =  kony.i18n.getLocalizedString("Next");
     dismissLoadingScreenPopup();
}

function menuOptionEnableTokenActivation(){

	showLoadingScreenPopup();
	var tokenMenuEnableParams = {};
	tokenMenuEnableParams["crmId"] = gblcrmId;
	invokeServiceSecureAsync("menuOptionEnableTokenActivation", tokenMenuEnableParams, menuOptionEnableTokenActivationCallBack);
}

function menuOptionEnableTokenActivationCallBack(status, callBackResponse){
  if (status == 400) {
    if (callBackResponse["opstatus"] == 0) {
      var tokenMenuFlag = false
      var tokenEnable = callBackResponse["menuEnableStatus"];
      if(tokenEnable == "true"){
        //frmIBTokenActivationPage.show()
        gblTokenActivationFlag=true;
      }else{
        //frmAccountSummaryLanding.show();
        showAccountSummaryNew();
      }
    }
  }
}

function verifyHWTKN(){

	
		 if( frmIBTokenActivationPage.tbxTokenOTP.text == undefined || frmIBTokenActivationPage.tbxTokenOTP.text.trim() == ""){
		 		alert(kony.i18n.getLocalizedString("emptyToken"));
	 			return false;
		 } else {
		 	gblVerifyToken = gblVerifyToken + 1;
		 	inputParam = {};
	    	inputParam["loginModuleId"] = "IB_HWTKN";
	   		inputParam["userStoreId"] = "DefaultStore";
	   		inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
	    	inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
	    	inputParam["userId"] = gblUserName;
	    	
	    	inputParam["password"] = frmIBTokenActivationPage.tbxTokenOTP.text;
	    	
	    	inputParam["serialNum"]=frmIBTokenActivationPage.lblTokenNumber.text;
	    	inputParam["sessionVal"] = "";
	    	inputParam["segmentId"] = "segmentId";
		    inputParam["segmentIdVal"] = "MIB";
		    inputParam["channel"] = "InterNet Banking";
		    
		    frmIBTokenActivationPage.tbxTokenOTP.text="";
			invokeServiceSecureAsync("verifyTokenEx", inputParam, verifyPasswordCallBack);
			showLoadingScreenPopup();
		 }
			
}
 
function verifyPasswordCallBack(status, callBackResponse){
	
	
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			
			dismissLoadingScreenPopup();
			frmIBTokenActivationPage.hbxConfirm.setVisibility(false);
			frmIBTokenActivationPage.hbxCompleteConfirm.setVisibility(true);
			frmIBTokenActivationPage.hbxSerialNumber.setVisibility(false);
      		frmIBTokenActivationPage.hbxTokenNumber.setVisibility(false);
      		frmIBTokenActivationPage.hbxTMBImage.setVisibility(true);
			frmIBTokenActivationPage.lblTokenHeader.text=kony.i18n.getLocalizedString("Complete");
			gblTokenActivationFlag=false;
			//if ( gblVerifyToken <= 3 ) {
//				inputParam={};
//				inputParam["tokenStatus"]="02";
//				inputParam["crmId"]=gblcrmId;
//				inputParam["serialNum"]=frmIBTokenActivationPage.lblTokenNumber.text;
//				//invokeServiceSecureAsync("updateTokenStatus", inputParam, updateTokenStatusCallBack);
//				//activityLogServiceCall("006", "", "01", "", "Success", "Success", "Token Activation", "", "", "");
//				
//			} else {
//				inputParam["tokenStatus"]="09";
//				inputParam["crmId"]=gblcrmId;
//				inputParam["serialNum"]=frmIBTokenActivationPage.lblTokenNumber.text;
//				//invokeServiceSecureAsync("updateTokenStatus", inputParam, updateTokenStatusCallBackforFailure);
//				//activityLogServiceCall("006", "", "02", "", "Fail", "Fail", "Token Activation", "", "", "");
//			}
			
		}else if(callBackResponse["opstatus"] == "1"){
			 dismissLoadingScreenPopup();
			 alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
		}
		else{
			dismissLoadingScreenPopup();
			
			
				if(callBackResponse["errCode"]=='VrfyOTPErr00002'){
					popIBTransNowOTPLocked.show();
					popIBTransNowOTPLocked.lblDetails1.text =kony.i18n.getLocalizedString("tokenLockText");	
				}
				if(callBackResponse["errCode"]=='VrfyOTPErr00001'){
					if(callBackResponse["errMsg"]!= undefined){
						//alert(callBackResponse["errMsg"]);
					//alert(kony.i18n.getLocalizedString("invalidOTP"));
						frmIBTokenActivationPage.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidOTP");//kony.i18n.getLocalizedString("invalidOTP"); //
                       //  frmIBTokenActivationPage.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBTokenActivationPage.hbxOTPincurrect.isVisible = true;
                    //frmIBAddMyAccnt.hbox450035142444916.isVisible = false;
                    //frmIBAddMyAccnt.hbox44747680933513.isVisible = false;
                    frmIBTokenActivationPage.tbxTokenOTP.text = "";
                    frmIBTokenActivationPage.tbxTokenOTP.setFocus(true);
					} else {
						alert(kony.i18n.getLocalizedString("invalidOTP"));
					}
		 			return false;
				}
			
			}
	} 
	
}

//function updateTokenStatusCallBack(status, callBackResponse){
//
//
//if (status == 400) {
//	if (callBackResponse["opstatus"] == 0) {
//		
//			frmIBTokenActivationPage.hbxConfirm.setVisibility(false);
//			frmIBTokenActivationPage.hbxCompleteConfirm.setVisibility(true);
//			frmIBTokenActivationPage.hbxSerialNumber.setVisibility(false);
//      		frmIBTokenActivationPage.hbxTokenNumber.setVisibility(false);
//      		frmIBTokenActivationPage.hbxTMBImage.setVisibility(true);
//			frmIBTokenActivationPage.lblTokenHeader.text=kony.i18n.getLocalizedString("Complete");
//		//inputParam={};
////		inputParam["actionType"]="00";
////		inputParam["crmId"]=gblcrmId;
////		inputParam["tokenDeviceFlag"]="Y";
////		
////		invokeServiceSecureAsync("crmProfileMod", inputParam, crmProfileModTokenCallBack);
//		
//		}
//
//	}
//}
//function crmProfileModTokenCallBack(status, callBackResponse){
//	
//	
//	
//	if (status == 400) {
//		if (callBackResponse["opstatus"] == 0) {
//		gblUserLockStatusIB = callBackResponse["IBUserStatusID"];
//			
//		
//			frmIBTokenActivationPage.hbxConfirm.setVisibility(false);
//			frmIBTokenActivationPage.hbxCompleteConfirm.setVisibility(true);
//			frmIBTokenActivationPage.hbxSerialNumber.setVisibility(false);
//      		frmIBTokenActivationPage.hbxTokenNumber.setVisibility(false);
//      		frmIBTokenActivationPage.hbxTMBImage.setVisibility(true);
//			frmIBTokenActivationPage.lblTokenHeader.text=kony.i18n.getLocalizedString("Complete");
//			
//		}
//	}
//
//
//}

//function updateTokenStatusCallBackforFailure(status, callBackResponse){
//	
//	
//	if (status == 400) {
//		if (callBackResponse["opstatus"] == 0) {
//			
//			//frmIBAccntSummary.show();
//				popIBTransNowOTPLocked.show();
//				popIBTransNowOTPLocked.lblDetails1.text =kony.i18n.getLocalizedString("tokenLockText");
//		}
//	}
//}


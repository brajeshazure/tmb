




gblExecuteTransaction = "";
gblToMobile = false; //Use to change locale
gblTDTransfer = false;
accountReturned = "";
gblBpBalHideUnhide = "true";
/*function mySwipeCalendar(myWidget, gestureInfo) {
	
	var hboxID = myWidget["id"];
	
	var indexString = hboxID.split("hboxTxns");
	
	var index = indexString[1];
	
	var allwidgets = gsFormId.vboxTxnsMain.widgets();
	
	for (var j = 0; j < allwidgets.length; j++) {
		var hboxTxns = "hboxTxns" + index;
		
		if (allwidgets[j]["id"] == hboxTxns) {
			
			
			var subwidgets = allwidgets[j].widgets();
			//
			for (var k = 0; k < subwidgets.length; k++) {
				var vboxTxns = "vboxTxns" + index;
				
				if (subwidgets[k]["id"] == vboxTxns) {
					
					
					var subsubwidgets = subwidgets[k].widgets();
					for (var l = 0; l < subsubwidgets.length; l++) {
						var hboxPayNow = "hboxPayNow" + index;
						
						if (subsubwidgets[l]["id"] == hboxPayNow) {
							
							
							subsubwidgets[l].isVisible = true;
							subsubwidgets[l].setFocus = true;
							///break;
							return;
						}
					}
				}
			}
		}
	}
}*/

/*
**************************************************************************************************************************************
  	Name    : showFrmMBMyActivities
  	Author  : Phanidhar Anumula 
  	Date    : Sep 28 2013
  	Purpose : Redirect to Calendar Page onClick of My Activities tab from MB Main Menu
**************************************************************************************************************************************
--*/
function showFrmMBMyActivities()
{
  	//kony.modules.loadFunctionalModule("eDonationModule");
   loadFunctionalModuleSync("eDonationModule");
	gblFlagMenu = "";
	iphone6check();
	//destroyMenuSpa();
	gblRetryCountRequestOTP = "0";
	LinkMyActivities = 0;	
	gblMBCalView = 1;
	clearCalendarData();
	frmMBMyActivities.show();
	
}

/*
**************************************************************************************************************************************
  	Name    : onLinkMyActivitiesClickMB
  	Author  : Phanidhar Anumula 
  	Date    : Dec 04 2013
  	Purpose : Redirect to Calendar Page onClick of My Activities link from My Account details Page
**************************************************************************************************************************************
--*/
function onLinkMyActivitiesClickMB() {
	LinkMyActivities = 1;
	gblMBCalView = 1;
	clearCalendarData();
	//var nickName = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"] != undefined ? gblAccountTable["custAcctRec"][gblIndex]["acctNickName"] : "";
	//frmMBMyActivities.lblHdr.text = kony.i18n.getLocalizedString("MyActivitiesIB") + "-" + nickName;
	//show list view by default
	frmMBMyActivities.show();	
}

/*
**************************************************************************************************************************************
  	Name    : getTransferTxnDetCallBackMB
  	Author  : Rishika Kushwaha
  	Date    : Nov 7 2013
  	Purpose : Redirect to Complete page for executed transactions from Calendar 
**************************************************************************************************************************************
--*/
function getTransferTxnDetCallBackMB(status, resulttable) {
	
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["ExecTransactionDetails"].length > 0) {
				frmTransfersAckCalendar.lblTransNPbAckFrmCustomerName.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["fromAcctName"]) ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
				frmTransfersAckCalendar.lblTransNPbAckFrmName.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["fromAcctNickName"]) ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";

				//masking CR code
				frmTransfersAckCalendar.lblTransNPbAckFrmNum.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
					
				frmTransfersAckCalendar.lblFromAcctNumHidden.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
				
				accountReturned = resulttable["ExecTransactionDetails"][0]["fromAcctId"];
				var accountType = "";
				for(var i=0;i<gblAccountTable["custAcctRec"].length;i++){
					if(!(gblAccountTable["custAcctRec"][i]["accId"].indexOf(accountReturned)<0)){
						accountType = gblAccountTable["custAcctRec"][i]["accType"];
					}
				}
				if (accountType== kony.i18n.getLocalizedString("termDeposit")){
					gblTDTransfer = true;
					frmTransfersAckCalendar.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_NetAmount");
				}else{
					gblTDTransfer = false;
					frmTransfersAckCalendar.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_AmountFee");
				}
				frmTransfersAckCalendar.lblTransNPbAckFrmBal.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["balAfterPymnt"]) ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
				
				frmTransfersAckCalendar.lblTransNPbAckToAccountName.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["toAccName"]) ?
					resulttable["ExecTransactionDetails"][0]["toAccName"] : "";
				//masking CR code
				frmTransfersAckCalendar.lblTransNPbAckToNum.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["maskedToAcctId"]) ?
					encodeAccntNumbers(resulttable["ExecTransactionDetails"][0]["maskedToAcctId"]) : " ";
				frmTransfersAckCalendar.lblToAcctNumHidden.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["toAccId"]) ?
					resulttable["ExecTransactionDetails"][0]["toAccId"] : " ";
				var mobileOrCI = resulttable["ExecTransactionDetails"][0]["toAccMobileOrCI"];
              	//mobileOrCI="00";
				kony.print("mobileOrCI in getTransferTxnDetCallBackMB>>"+mobileOrCI);	
				if(isNotBlank(mobileOrCI) && mobileOrCI == "02") { //Mobile
					gblSelTransferMode = 2;
					frmTransfersAckCalendar.lblMobileNumber.text = maskMobileNumber(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
					frmTransfersAckCalendar.lblToAcctNumHidden.text = formatMobileNumber(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
					frmTransfersAckCalendar.lblMobileNumber.setVisibility(true);
					frmTransfersAckCalendar.lblTransNPbAckToNum.setVisibility(false);
					frmTransfersAckCalendar.lblTransNPbAckToName.text = getPhraseToMobileNumber();
				} else if(isNotBlank(mobileOrCI) && mobileOrCI == "01") { //Citizen
					gblSelTransferMode = 3;
					frmTransfersAckCalendar.lblMobileNumber.text = maskCitizenID(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
					frmTransfersAckCalendar.lblToAcctNumHidden.text = formatCitizenID(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
					frmTransfersAckCalendar.lblMobileNumber.setVisibility(true);
					frmTransfersAckCalendar.lblTransNPbAckToNum.setVisibility(false);
					frmTransfersAckCalendar.lblTransNPbAckToName.text = getPhraseToMobileNumber();
				} else if(isNotBlank(mobileOrCI) && mobileOrCI == "03") { //Tax ID
					gblSelTransferMode = 4;
					frmTransfersAckCalendar.lblMobileNumber.text = maskCitizenID(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
					frmTransfersAckCalendar.lblToAcctNumHidden.text = formatCitizenID(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
					frmTransfersAckCalendar.lblMobileNumber.setVisibility(true);
					frmTransfersAckCalendar.lblTransNPbAckToNum.setVisibility(false);
					frmTransfersAckCalendar.lblTransNPbAckToName.text = getPhraseToMobileNumber();
				} else if(isNotBlank(mobileOrCI) && mobileOrCI == "04") { //eWallet
					gblSelTransferMode = 5;
					frmTransfersAckCalendar.lblMobileNumber.text = maskeWalletID(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
					frmTransfersAckCalendar.lblToAcctNumHidden.text = formatEWalletNumber(resulttable["ExecTransactionDetails"][0]["toAccMobile"]);
					frmTransfersAckCalendar.lblMobileNumber.setVisibility(true);
					frmTransfersAckCalendar.lblTransNPbAckToNum.setVisibility(false);
					frmTransfersAckCalendar.lblTransNPbAckToName.text = getPhraseToMobileNumber();
				}else if(isNotBlank(mobileOrCI) && mobileOrCI == "00") { //To Acc PromptPay
					gblSelTransferMode = 0;
                    frmTransfersAckCalendar.lblMobileNumber.text = formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]);
                  	frmTransfersAckCalendar.lblToAcctNumHidden.text = resulttable["ExecTransactionDetails"][0]["toAccId"];
                 	 frmTransfersAckCalendar.lblMobileNumber.setVisibility(true);
                  	frmTransfersAckCalendar.lblTransNPbAckToNum.setVisibility(false);
                  	frmTransfersAckCalendar.lblTransNPbAckToName.text = kony.i18n.getLocalizedString("MIB_P2PToAcctLabel");
                }else{
                    gblSelTransferMode = 1;
					frmTransfersAckCalendar.lblMobileNumber.setVisibility(false)
					frmTransfersAckCalendar.lblTransNPbAckToNum.setVisibility(true);;
				}
				frmTransfersAckCalendar.lblSmartFlag.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["smartFlag"]) ?
					resulttable["ExecTransactionDetails"][0]["smartFlag"] : " ";
				frmTransfersAckCalendar.lblMyNote.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["note"]) ?
					resulttable["ExecTransactionDetails"][0]["note"] : " ";
              	frmTransfersAckCalendar.lblConfirmSendersNotes.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["noteToRecipient"]) ?
					resulttable["ExecTransactionDetails"][0]["noteToRecipient"] : " ";
				frmTransfersAckCalendar.lblToBankCDHidden.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["toAccBankCode"]) ?
					resulttable["ExecTransactionDetails"][0]["toAccBankCode"] : " ";
				var nickname = isNotBlank(resulttable["ExecTransactionDetails"][0]["toAccNickName"]) ?
					resulttable["ExecTransactionDetails"][0]["toAccNickName"] : resulttable["ExecTransactionDetails"][0]["bankShortName"];
				if(!checkP2pToAccount(nickname)){
					frmTransfersAckCalendar.lblTransNPbAckToName.text = nickname;
				}						
			if(frmTransfersAckCalendar.lblTransNPbAckToName.text == getPhraseToMobileNumber()){
				gblToMobile = true;
				frmTransfersAckCalendar.lblTransNPbAckToName.text = getPhraseToMobileNumber();
			} else {
				gblToMobile = false;
			}
				frmTransfersAckCalendar.lblTransNPbAckDateVal.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["transferDate"]) ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
				//default show Ref No.s if mega transfer
				frmTransfersAckCalendar.lblHideMore.text = kony.i18n.getLocalizedString("Hide");
				frmTransfersAckCalendar.imgMore.src = "arrow_down.png";
				
				frmTransfersAckCalendar.hbxShareOption.setVisibility(true);
				frmTransfersAckCalendar.imgHeaderMiddle.src = "arrowtop.png";
				
				frmTransfersAckCalendar.segTransAckSplit.removeAll();
				if(isNotBlank(resulttable["ExecTransactionDetails"][0]["smartFlag"]) && resulttable["ExecTransactionDetails"][0]["smartFlag"] == "Y") {
					gblSmartFlag = true;
					frmTransfersAckCalendar.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive02");
					if(isNotBlank(resulttable["ExecTransactionDetails"][0]["dueDate"])) {
						frmTransfersAckCalendar.lblSmartDateVal.text = resulttable["ExecTransactionDetails"][0]["dueDate"];
						frmTransfersAckCalendar.lblSmartDateVal.skin = lblbluemedium142;
					} else {
						frmTransfersAckCalendar.lblSmartDateVal.text = "";
						frmTransfersAckCalendar.lblSmartDateVal.skin = lblGreyRegular142;
					}
											
				} else {
					frmTransfersAckCalendar.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
					gblSmartFlag = false;
					frmTransfersAckCalendar.lblSmartDateVal.text = frmTransfersAckCalendar.lblTransNPbAckDateVal.text;
					frmTransfersAckCalendar.lblSmartDateVal.skin = lblGreyRegular142;	
				}	
				
				var transferAmount = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				var transferFee = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				gblXferSplit = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"];
				if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 1) {
					var tempdata = [];
					var successCount = 0;
					var failureCount = 0;
					var successAmount = "0.00";
					var successFee = "0.00";
					 var allSuccess = "0";
					for (var i = 0; i < resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length; i++) {
						var amount = isNotBlank(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["amount"]) ? 
							resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["amount"]: "0.00";							
							
						var fee = isNotBlank(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["fee"]) ? 
							resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["fee"]: "0.00";
						
						if(gblXferSplit[i]["txnStatus"]=="01" || gblXferSplit[i]["txnStatus"]=="03") {
							successCount++; 
							successAmount = parseFloat(successAmount) + parseFloat(removeCommos(amount));
							successFee = parseFloat(successFee) + parseFloat(removeCommos(fee));
						} else {
							failureCount++;
						}
						tempdata.push({
							"lblSegTrans": kony.i18n.getLocalizedString("keyTransactionRefNo"),
							"lblSegAmount": kony.i18n.getLocalizedString("MyActivitiesIB_Amount"),
							"lblSegFee": kony.i18n.getLocalizedString("keyFee"),
							"lblSegTransRefNo": isNotBlank(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["transRefId"]) ? resulttable[
								"ExecTransactionDetails"][0]["AmntFeeDetails"][i]["transRefId"]: " ",
							"lblsegAmountDes":amount + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "("+ fee + " " + kony.i18n.getLocalizedString("currencyThaiBaht") +")",							
							"lblsegFeeDes": fee + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
							"imgMegaTransStatus": {
								"src": isNotBlank(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["txnStatus"]) ? getXferImage(
									resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][i]["txnStatus"]) : ""
							}
						});
					}
					var totalAmount = isNotBlank(resulttable["ExecTransactionDetails"][0]["totalAmount"]) ? resulttable["ExecTransactionDetails"][0]["totalAmount"] : "0.00";
					var totalFee = isNotBlank(resulttable["ExecTransactionDetails"][0]["totalFee"]) ? resulttable["ExecTransactionDetails"][0]["totalFee"] : "0.00";
					
					frmTransfersAckCalendar.lblTransNPbAckTotVal2.text = commaFormatted(parseFloat(removeCommos(totalAmount)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "("+ commaFormatted(parseFloat(removeCommos(totalFee)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")  + ")";
					
					transferAmount = commaFormatted(parseFloat(successAmount).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
					transferFee = commaFormatted(parseFloat(successFee).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
					
					frmTransfersAckCalendar.segTransAckSplit.setData(tempdata);
					frmTransfersAckCalendar.segTransAckSplit.setVisibility(true);
					frmTransfersAckCalendar.lblTransNPbAckRefDes.text = "";
					frmTransfersAckCalendar.lblTransNPbAckRefDes.setVisibility(false);
					frmTransfersAckCalendar.hbxMoreHide.setVisibility(true);
					
					if(successCount > 0) {
						frmTransfersAckCalendar.imgTransNBpAckComplete.src = "completeico.png";
					} else {
						frmTransfersAckCalendar.imgTransNBpAckComplete.src = "iconnotcomplete.png"
					}
					
					if(failureCount > 0) {
						 allSuccess = "1";
					}
					frmTransfersAckCalendar.lblAllSuccess.text=allSuccess;	
				} else {
					frmTransfersAckCalendar.segTransAckSplit.setVisibility(false);
					frmTransfersAckCalendar.lblTransNPbAckRefDes.setVisibility(true);
					frmTransfersAckCalendar.hbxMoreHide.setVisibility(false);
					if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"] != "") {
						var amt = replaceCommon(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"], ",", "");
						var fee = replaceCommon(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"], ",", "");
						
						frmTransfersAckCalendar.lblTransNPbAckRefDes.text = isNotBlank(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"]) ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
						
					 	transferAmount = isNotBlank(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"]) ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
						transferFee = isNotBlank(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"]) ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
						frmTransfersAckCalendar.lblHiddenTransferAmt.text = transferAmount;
						frmTransfersAckCalendar.lblTransNPbAckTotVal2.text = transferAmount + "("+ transferFee + ")";
						
						if(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" || resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "03") {
						 	frmTransfersAckCalendar.imgTransNBpAckComplete.src = "completeico.png";
						 } else {
						   	frmTransfersAckCalendar.imgTransNBpAckComplete.src = "iconnotcomplete.png";
						   	transferAmount = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
							transferFee = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
						 }
						 if(resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02") {
						 	setVisiblityBalanceTransferMB(frmTransfersAckCalendar.lblTransNPbAckBalAfter, frmTransfersAckCalendar.lblTransNPbAckFrmBal, frmTransfersAckCalendar.hbxTransCnfmBalBefVal, false);
						 	frmTransfersAckCalendar.imgHide.setVisibility(false);
						 } else {
						 	setVisiblityBalanceTransferMB(frmTransfersAckCalendar.lblTransNPbAckBalAfter, frmTransfersAckCalendar.lblTransNPbAckFrmBal, frmTransfersAckCalendar.hbxTransCnfmBalBefVal, true);
						 	frmTransfersAckCalendar.imgHide.setVisibility(true);
						 }
					} else {
						frmTransfersAckCalendar.lblTransNPbAckRefDes.text = "";
						frmTransfersAckCalendar.lblTransNPbAckTotVal.text = "";
						frmTransfersAckCalendar.lblTransNPbAckTotFeeVal.text = "";
					}
				}
									
				frmTransfersAckCalendar.lblTransNPbAckTotVal.text = transferAmount;	
                frmTransfersAckCalendar.lblHiddenTransferAmt.text = transferAmount;
				frmTransfersAckCalendar.lblTransNPbAckTotFeeVal.text = "[" + transferFee + "]";
					
				//frmTransfersAckCalendar.imgTransNPbAckFrm.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=" + gblTMBBankCD + "&modIdentifier=BANKICON";
				frmTransfersAckCalendar.imgTransNPbAckFrm.src = loadBankIcon(gblTMBBankCD);
				if(isNotBlank(resulttable["ExecTransactionDetails"][0]["toAccMobile"])) {
					if(mobileOrCI == "02"){
						frmTransfersAckCalendar.imgTransNPbAckTo.src = loadBankIcon("toMobile");
					}else if(mobileOrCI == "01"){
						frmTransfersAckCalendar.imgTransNPbAckTo.src = loadBankIcon("toCitizen");
					}else if(mobileOrCI == "03"){
						frmTransfersAckCalendar.imgTransNPbAckTo.src = loadBankIcon("toTaxID");
					}else if(mobileOrCI == "04"){
						frmTransfersAckCalendar.imgTransNPbAckTo.src = loadBankIcon("toeWallet");
					}else if(mobileOrCI == "00"){
                      var tempBankIcon = frmTransfersAckCalendar.lblToBankCDHidden.text;
						frmTransfersAckCalendar.imgTransNPbAckTo.src = loadBankIcon(tempBankIcon);
                    }
				}
				else{
                  	var tempBankIcon = frmTransfersAckCalendar.lblToBankCDHidden.text;
					frmTransfersAckCalendar.imgTransNPbAckTo.src = loadBankIcon(tempBankIcon);
				}
				//for QR
				frmTransfersAckCalendar.lblhidden.text="";
                if(resulttable["ExecTransactionDetails"][0]["flexValue1"]=="QR")
                {
                  //frmTransfersAckCalendar.imgTransNPbAckTo.src="";
                  frmTransfersAckCalendar.imgTransNPbAckTo.src="thaiqrlogo.png";
                  frmTransfersAckCalendar.hbxqrback.setVisibility(true);
                  frmTransfersAckCalendar.hbxConfirmCancel.setVisibility(false);                  
                  frmTransfersAckCalendar.lblhidden.text="QR";
                }
                else
                {
                  frmTransfersAckCalendar.lblhidden.text="";
                  frmTransfersAckCalendar.hbxqrback.setVisibility(false);
                  frmTransfersAckCalendar.hbxConfirmCancel.setVisibility(true);
                }
				dismissLoadingScreen();
				frmTransfersAckCalendar.show();
			} else {
				 dismissLoadingScreen();
			}
		} else {
			 dismissLoadingScreen();
		}
	}
	 dismissLoadingScreen();
}

function showHideTransferAfterBalance() {
	if (frmTransfersAckCalendar.hbxTransCnfmBalBefVal.isVisible) {
		frmTransfersAckCalendar.hbxTransCnfmBalBefVal.setVisibility(false);
		frmTransfersAckCalendar.imgHide.src = "arrow_up.png";
	} else {
		frmTransfersAckCalendar.hbxTransCnfmBalBefVal.setVisibility(true);
		frmTransfersAckCalendar.imgHide.src = "arrow_down.png";
	}
	
}

function showHideMegaTransferDetails() {
	
	if(frmTransfersAckCalendar.segTransAckSplit.isVisible){
		frmTransfersAckCalendar.lblHideMore.text = kony.i18n.getLocalizedString("show");
		frmTransfersAckCalendar.imgMore.src = "arrow_up.png";
		frmTransfersAckCalendar.segTransAckSplit.setVisibility(false);
		
	}else{
		frmTransfersAckCalendar.lblHideMore.text = kony.i18n.getLocalizedString("Hide");
		frmTransfersAckCalendar.imgMore.src = "arrow_down.png";
		frmTransfersAckCalendar.segTransAckSplit.setVisibility(true);
	}
	
}

/*
**************************************************************************************************************************************
  	Name    : getBillPaymentTxnDetCallBackMB
  	Author  : Rishika Kushwaha
  	Date    : Nov 7 2013
  	Purpose : Redirect to Complete page for executed transactions from Calendar 
**************************************************************************************************************************************
--*/
function getBillPaymentTxnDetCallBackMB(status, resulttable) {
  try{
    kony.print("@@@ In getBillPaymentTxnDetCallBackMB() @@@");
	if (status == 400) {
        //MIB-3963 Add data for keep in FIN_ACTIVITYLOG 	
        gblBillerCompCodeCalendar="";
        gblBillerMeterNoEn="";
        gblBillerCustNameEn="";
        gblBillerCustAddressEn="";
        gblBillerMeterNoTh="";
        gblBillerCustNameTh="";
        gblBillerCustAddressTh="";
        gblBillerCustAddressValue="";
		
      	displayMEADetailsOnMyActivity(false);
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["ExecTransactionDetails"].length > 0) {
				frmBillPaymentCompleteCalendar.lblAccountName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
				frmBillPaymentCompleteCalendar.lblAccUserName.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
				//frmBillPaymentCompleteCalendar.lblAccountNum.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
					//formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
				//masking CR code
				frmBillPaymentCompleteCalendar.lblAccountNum.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
				frmBillPaymentCompleteCalendar.lblBillerNickname.text = resulttable["ExecTransactionDetails"][0]["billerName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["billerName"] : " ";
				frmBillPaymentCompleteCalendar.lblBalAfterPayValue.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString(
						"currencyThaiBaht") : " ";
				 
				 /*if(resulttable["ExecTransactionDetails"][0]["billerShortName"] !=undefined && resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined ){
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text =resulttable["ExecTransactionDetails"][0]["billerShortName"] + "("+
						resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")";
					}else{
						if(resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined)
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text =resulttable["ExecTransactionDetails"][0]["billerShortName"];
						else if(resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined){
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = "("+resulttable["ExecTransactionDetails"][0]["billerCommCode"]+")"
						}else{
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = "";
						}
					}
				      kony.print("gblActivityNiceName:"+gblActivityNiceName+":Biller Name:"+resulttable["ExecTransactionDetails"][0]["billerName"]);   
                        if(gblActivityNiceName == resulttable["ExecTransactionDetails"][0]["billerName"]){
                              frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = gblActivityNiceName;
                        }*/
                    
                    // MIB-5750 R75-IB Should display full name at calendar and view transaction when pay bill via MB //    
                   /*frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = resulttable["ExecTransactionDetails"][0]["billerCustomerName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["billerCustomerName"] : " ";  */
					
					//MIB-6505 MB- View activity detail display Topup biller incorrect
					if(resulttable["ExecTransactionDetails"][0]["billerCommCode"]=="2151"){
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = resulttable["ExecTransactionDetails"][0]["billerName"] !=
						undefined ? resulttable["ExecTransactionDetails"][0]["billerName"] : " ";
					}else {
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = resulttable["ExecTransactionDetails"][0]["billerCustomerName"] !=
						undefined ? resulttable["ExecTransactionDetails"][0]["billerCustomerName"] : " ";
					}	   
				
				frmBillPaymentCompleteCalendar.hbxRef2.setVisibility(true);	 
				//frmBillPaymentCompleteCalendar.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["billerRef1"] != undefined ?
					//resulttable["ExecTransactionDetails"][0]["billerRef1"] : " ";
				//masking CR code
				frmBillPaymentCompleteCalendar.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"] != undefined ?
					resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"] : " ";
				frmBillPaymentCompleteCalendar.lblRef2Value.text = resulttable["ExecTransactionDetails"][0]["billerRef2"] != undefined ?
					resulttable["ExecTransactionDetails"][0]["billerRef2"] : " ";
				frmBillPaymentCompleteCalendar.lblPaymentDateValue.text = resulttable["ExecTransactionDetails"][0]["transferDate"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
				//frmBillPaymentCompleteCalendar.lblMyNoteValue.text = resulttable["ExecTransactionDetails"][0]["note"] != undefined ?resulttable["ExecTransactionDetails"][0]["note"] : " ";
				frmBillPaymentCompleteCalendar.hbxFlexiParam1.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxFlexiParam2.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxFlexiParam3.setVisibility(false);
					
				if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
					frmBillPaymentCompleteCalendar.lblTxnNumValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
						"transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
					frmBillPaymentCompleteCalendar.lblAmountValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"]
					[0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n
						.getLocalizedString("currencyThaiBaht") : " ";						
					//frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] !=
//						undefined ? "[" + resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString(
//							"currencyThaiBaht") + "]"  : " ";							
					frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] !=
						undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString(
							"currencyThaiBaht")  : " ";																
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmBillPaymentCompleteCalendar.imgTransNBpAckComplete
						.src = "completeico.png" : frmBillPaymentCompleteCalendar.imgTransNBpAckComplete.src = "mes2.png";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransferMB(frmBillPaymentCompleteCalendar.lblBalanceAfterPayment, 
						frmBillPaymentCompleteCalendar.lblBalAfterPayValue, frmBillPaymentCompleteCalendar.hbox101271281131304, false) : setVisiblityBalanceTransferMB(frmBillPaymentCompleteCalendar.lblBalanceAfterPayment, 
						frmBillPaymentCompleteCalendar.lblBalAfterPayValue, frmBillPaymentCompleteCalendar.hbox101271281131304, true);
				} else {
					frmBillPaymentCompleteCalendar.lblTxnNumValue.text = "";
					frmBillPaymentCompleteCalendar.lblAmountValue.text = "";
					frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text = "";
				}
				var flowQR=resulttable["ExecTransactionDetails"][0]["flexValue1"];
                
                var imagesUrl ="";
                frmBillPaymentCompleteCalendar.lblHiddenQRFlowCheck.text = "";
                if(isNotBlank(flowQR)){
                  if(flowQR=="QR"){
                    imagesUrl=loadBankIcon("staticqr");
                    frmBillPaymentCompleteCalendar.lblHiddenQRFlowCheck.text = "QR";
                  }else{
                    imagesUrl=loadBillerIcons(resulttable["ExecTransactionDetails"][0]["billerCommCode"]);
                  }                  
                }else{
                  imagesUrl=loadBillerIcons(resulttable["ExecTransactionDetails"][0]["billerCommCode"]);
                }

				frmBillPaymentCompleteCalendar.imgBillerPic.src =imagesUrl;
				frmBillPaymentCompleteCalendar.imgFromAccount.src = loadBankIcon(gblTMBBankCD);
				//frmBillPaymentCompleteCalendar.imgFromAccount.src =  "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblTMBBankCD+"&modIdentifier=BANKICON";
				
				gblExecRef1ENVal=resulttable["ExecTransactionDetails"][0]["Ref1EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref1EN"] : "";
				gblExecRef2ENVal=resulttable["ExecTransactionDetails"][0]["Ref2EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref2EN"] : "";
				gblExecRef1THVal=resulttable["ExecTransactionDetails"][0]["Ref1TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref1TH"] : "";
				gblExecRef2THVal=resulttable["ExecTransactionDetails"][0]["Ref2TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref2TH"] : "";
				gblExecRef2Req=resulttable["ExecTransactionDetails"][0]["isRef2Req"]!=undefined ? resulttable["ExecTransactionDetails"][0]["isRef2Req"] : "";
				showRefValuesMB();
				
				frmBillPaymentCompleteCalendar.hbxStartEndDate.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxRepeatExcuteTimes.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxEasyPass.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxCardBal.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxEasyPassTxnId.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxShareOption.setVisibility(true);
				//frmBillPaymentCompleteCalendar.hboxMyNote.setVisibility(true);
				//frmBillPaymentCompleteCalendar.hboxTxnNo.skin = "hboxLightGrey";

				if(resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined){																
					if(gblEA_BILLER_COMP_CODES.indexOf(resulttable["ExecTransactionDetails"][0]["billerCommCode"]) >= 0){
						gblBillerCompCodeCalendar = resulttable["ExecTransactionDetails"][0]["billerCommCode"];
						gblBillerMeterNoEn=resulttable["ExecTransactionDetails"][0]["billerMeterNoEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerMeterNoEn"] : " ";
						gblBillerCustNameEn=resulttable["ExecTransactionDetails"][0]["billerCustNameEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustNameEn"] : " ";
						gblBillerCustAddressEn=resulttable["ExecTransactionDetails"][0]["billerCustAddressEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustAddressEn"] : " ";
						gblBillerMeterNoTh=resulttable["ExecTransactionDetails"][0]["billerMeterNoTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerMeterNoTh"] : " ";
						gblBillerCustNameTh=resulttable["ExecTransactionDetails"][0]["billerCustNameTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustNameTh"] : " ";
						gblBillerCustAddressTh=resulttable["ExecTransactionDetails"][0]["billerCustAddressTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustAddressTh"] : " ";		
						gblBillerCustAddressValue=resulttable["ExecTransactionDetails"][0]["flexValue3"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["flexValue3"] : " ";			
										
						frmBillPaymentCompleteCalendar.lblCustNameValue.text = resulttable["ExecTransactionDetails"][0]["flexValue1"] !=
							undefined ? resulttable["ExecTransactionDetails"][0]["flexValue1"] : " ";
						frmBillPaymentCompleteCalendar.lblMeterNumValue.text = resulttable["ExecTransactionDetails"][0]["flexValue2"] !=
							undefined ? resulttable["ExecTransactionDetails"][0]["flexValue2"] : " ";
						//frmBillPaymentCompleteCalendar.lblCustAddressValue.text = resulttable["ExecTransactionDetails"][0]["flexValue3"] !=
							//undefined ? resulttable["ExecTransactionDetails"][0]["flexValue3"] : " ";
					
						if (kony.i18n.getCurrentLocale() != "th_TH"){
							frmBillPaymentCompleteCalendar.lblMeterNum.text=resulttable["ExecTransactionDetails"][0]["billerMeterNoEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerMeterNoEn"] : " ";
							frmBillPaymentCompleteCalendar.lblCustName.text=resulttable["ExecTransactionDetails"][0]["billerCustNameEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustNameEn"] : " ";							
							frmBillPaymentCompleteCalendar.lblCustAddressValue.text = gblBillerCustAddressEn + "  " + gblBillerCustAddressValue;
							
						}else{
							frmBillPaymentCompleteCalendar.lblMeterNum.text=resulttable["ExecTransactionDetails"][0]["billerMeterNoTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerMeterNoTh"] : " ";
							frmBillPaymentCompleteCalendar.lblCustName.text=resulttable["ExecTransactionDetails"][0]["billerCustNameTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustNameTh"] : " ";
							frmBillPaymentCompleteCalendar.lblCustAddressValue.text = gblBillerCustAddressTh + "  " + gblBillerCustAddressValue;
						}		
																				
						displayMEADetailsOnMyActivity(true);
					} else if(GLOBAL_ONLINE_PAYMENT_VERSION_MWA.indexOf(resulttable["ExecTransactionDetails"][0]["billerCommCode"]) != -1 ){  
						gblBillerCompCodeCalendar = resulttable["ExecTransactionDetails"][0]["billerCommCode"];
						gblBillerMeterNoEn=resulttable["ExecTransactionDetails"][0]["billerMeterNoEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerMeterNoEn"] : " ";
						gblBillerCustNameEn=resulttable["ExecTransactionDetails"][0]["billerCustNameEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustNameEn"] : " ";
						gblBillerCustAddressEn=resulttable["ExecTransactionDetails"][0]["billerCustAddressEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustAddressEn"] : " ";
						gblBillerMeterNoTh=resulttable["ExecTransactionDetails"][0]["billerMeterNoTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerMeterNoTh"] : " ";
						gblBillerCustNameTh=resulttable["ExecTransactionDetails"][0]["billerCustNameTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustNameTh"] : " ";
						gblBillerCustAddressTh=resulttable["ExecTransactionDetails"][0]["billerCustAddressTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustAddressTh"] : " ";		
						gblBillerCustAddressValue=resulttable["ExecTransactionDetails"][0]["flexValue3"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["flexValue3"] : " ";			
										
						frmBillPaymentCompleteCalendar.lblCustNameValue.text = resulttable["ExecTransactionDetails"][0]["flexValue1"] !=
							undefined ? resulttable["ExecTransactionDetails"][0]["flexValue1"] : " ";
						frmBillPaymentCompleteCalendar.lblMeterNumValue.text = resulttable["ExecTransactionDetails"][0]["flexValue2"] !=
							undefined ? resulttable["ExecTransactionDetails"][0]["flexValue2"] : " ";
						//frmBillPaymentCompleteCalendar.lblCustAddressValue.text = resulttable["ExecTransactionDetails"][0]["flexValue3"] !=
							//undefined ? resulttable["ExecTransactionDetails"][0]["flexValue3"] : " ";
					
						if (kony.i18n.getCurrentLocale() != "th_TH"){
							frmBillPaymentCompleteCalendar.lblMeterNum.text=resulttable["ExecTransactionDetails"][0]["billerMeterNoEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerMeterNoEn"] : " ";
							frmBillPaymentCompleteCalendar.lblCustName.text=resulttable["ExecTransactionDetails"][0]["billerCustNameEn"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustNameEn"] : " ";							
							frmBillPaymentCompleteCalendar.lblCustAddressValue.text = gblBillerCustAddressEn + "  " +  gblBillerCustAddressValue;
							
						}else{
							frmBillPaymentCompleteCalendar.lblMeterNum.text=resulttable["ExecTransactionDetails"][0]["billerMeterNoTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerMeterNoTh"] : " ";
							frmBillPaymentCompleteCalendar.lblCustName.text=resulttable["ExecTransactionDetails"][0]["billerCustNameTh"] !=
								undefined ? resulttable["ExecTransactionDetails"][0]["billerCustNameTh"] : " ";
							frmBillPaymentCompleteCalendar.lblCustAddressValue.text = gblBillerCustAddressTh + "  " + gblBillerCustAddressValue;
						}	
						
						displayMEADetailsOnMyActivity(true);
					}else{
						displayMEADetailsOnMyActivity(false);
					}
				}else{
					displayMEADetailsOnMyActivity(false);
				}	

				// Hide Balance after when from account is credit card
				if (frmBillPaymentCompleteCalendar.lblAccountNum.text.length == 19) {
					frmBillPaymentCompleteCalendar.hbxOAFrmDetAck.setVisibility(false);
				} else {
					frmBillPaymentCompleteCalendar.hbxOAFrmDetAck.setVisibility(true);
				}
				dismissLoadingScreen();
				frmBillPaymentCompleteCalendar.show();
                
			} else {
				 dismissLoadingScreen();
				alert("No Records found");
			}
		} else {
			 dismissLoadingScreen();
		}
	}
	 dismissLoadingScreen();
  }catch(e){
    kony.print("@@@ In getBillPaymentTxnDetCallBackMB() Exception:::"+e);
  }
}

function displayMEADetailsOnMyActivity(isShow){
	frmBillPaymentCompleteCalendar.lblCustName.setVisibility(isShow);	
	frmBillPaymentCompleteCalendar.lblMeterNum.setVisibility(isShow);	
	frmBillPaymentCompleteCalendar.lblCustAddressValue.setVisibility(isShow);
						
	frmBillPaymentCompleteCalendar.hbxMEACustDetails.setVisibility(isShow);
	frmBillPaymentCompleteCalendar.hbxMEACustName.setVisibility(isShow);
    if(isNotBlank(gblBillerCompCodeCalendar) && gblPEA_BILLER_COMP_CODE.indexOf(gblBillerCompCodeCalendar) >= 0) {
      frmBillPaymentCompleteCalendar.hbxMeterNum.setVisibility(false);
    } else {
      frmBillPaymentCompleteCalendar.hbxMeterNum.setVisibility(isShow);
    }
	frmBillPaymentCompleteCalendar.hbxCustAddress.setVisibility(isShow);
	
	if(!isShow){
		frmBillPaymentCompleteCalendar.lblCustName.text = "";
		frmBillPaymentCompleteCalendar.lblMeterNum.text = "";	
		//frmBillPaymentCompleteCalendar.lblCustAddress.text = "";
	}
	
	gblBpBalHideUnhide = "true";
}

/*
**************************************************************************************************************************************
  	Name    : getTopUpTxnDetCallBackMB
  	Author  : Rishika Kushwaha
  	Date    : Nov 7 2013
  	Purpose : Redirect to Complete page for executed transactions from Calendar 
**************************************************************************************************************************************
--*/
function getTopUpTxnDetCallBackMB(status, resulttable) {
	if (status == 400) {
		gblBillerCompCodeCalendar="";
      	displayMEADetailsOnMyActivity(false);
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["ExecTransactionDetails"].length > 0) {
			
			
				if (resulttable["ExecTransactionDetails"][0]["txnStatus"] == "01") {
					//frmBillPaymentCompleteCalendar.hbxSuccess.setVisibility(true);
				} else {
					frmBillPaymentCompleteCalendar.imgTransNBpAckComplete.src = "mes2.png";
				}
				frmBillPaymentCompleteCalendar.lblAccountName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ?
					resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
				frmBillPaymentCompleteCalendar.lblAccUserName.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ?
					resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
				//frmBillPaymentCompleteCalendar.lblAccountNum.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
					//formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
				//masking CR code
				frmBillPaymentCompleteCalendar.lblAccountNum.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
				frmBillPaymentCompleteCalendar.lblBillerNickname.text = resulttable["ExecTransactionDetails"][0]["billerName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["billerName"] : " ";
				frmBillPaymentCompleteCalendar.lblBalAfterPayValue.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString(
						"currencyThaiBaht") : " ";
					/* MIB-5783
					if(resulttable["ExecTransactionDetails"][0]["billerShortName"] !=undefined && resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined ){
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text =resulttable["ExecTransactionDetails"][0]["billerShortName"] + "("+
						resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")";
					}else{
						if(resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined)
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text =resulttable["ExecTransactionDetails"][0]["billerShortName"];
						else if(resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined){
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text ="("+resulttable["ExecTransactionDetails"][0]["billerCommCode"]+")"
						}else{
						frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = "";
						}
					}
				kony.print("gblActivityNiceName:"+gblActivityNiceName+":Biller Name:"+resulttable["ExecTransactionDetails"][0]["billerName"]);   
                if(gblActivityNiceName == resulttable["ExecTransactionDetails"][0]["billerName"]){
                     frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = gblActivityNiceName;
                }
                */
                
                /*frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = resulttable["ExecTransactionDetails"][0]["billerCustomerName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["billerCustomerName"] : " ";*/			
				//MIB-6505 MB- View activity detail display Topup biller incorrect
				if(resulttable["ExecTransactionDetails"][0]["billerCommCode"]=="2151"){
					frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = resulttable["ExecTransactionDetails"][0]["billerName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["billerName"] : " ";
				}else {
					frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = resulttable["ExecTransactionDetails"][0]["billerCustomerName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["billerCustomerName"] : " ";
				}
					
				frmBillPaymentCompleteCalendar.hbxRef2.setVisibility(false);
				frmBillPaymentCompleteCalendar.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["billerRef1"] != undefined ?
					resulttable["ExecTransactionDetails"][0]["billerRef1"] : " ";
				frmBillPaymentCompleteCalendar.lblPaymentDateValue.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ?
					resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
				//frmBillPaymentCompleteCalendar.lblMyNoteValue.text = resulttable["ExecTransactionDetails"][0]["note"] != undefined ? resulttable["ExecTransactionDetails"][0]["note"] : " ";
				frmBillPaymentCompleteCalendar.hbxFlexiParam1.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxFlexiParam2.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxFlexiParam3.setVisibility(false);
					if(resulttable["ExecTransactionDetails"][0]["billerCommCode"]=="2151"){
						frmBillPaymentCompleteCalendar.hbxEasyPass.setVisibility(true);
						frmBillPaymentCompleteCalendar.hbxCardBal.setVisibility(true);
						frmBillPaymentCompleteCalendar.hbxEasyPassTxnId.setVisibility(true);
						frmBillPaymentCompleteCalendar.lblEasyPassCustValue.text=resulttable["ExecTransactionDetails"]["0"]["billerCustomerName"]!=undefined?resulttable["ExecTransactionDetails"]["0"]["billerCustomerName"]:" ";
						frmBillPaymentCompleteCalendar.lblCardBalVal.text=resulttable["ExecTransactionDetails"]["0"]["billerBalance"]!=undefined? ProfileCommFormat(resulttable["ExecTransactionDetails"]["0"]["billerBalance"]) + " " + kony.i18n.getLocalizedString(
							"currencyThaiBaht") :" ";
						frmBillPaymentCompleteCalendar.lblTopUpRefNumValue.text=resulttable["ExecTransactionDetails"]["0"]["BillerMethod"]!=undefined?resulttable["ExecTransactionDetails"]["0"]["BillerMethod"]:" ";//parameter nt added
					}else{
						frmBillPaymentCompleteCalendar.hbxEasyPass.setVisibility(false);
						frmBillPaymentCompleteCalendar.hbxCardBal.setVisibility(false);
						frmBillPaymentCompleteCalendar.hbxEasyPassTxnId.setVisibility(false);
					}
				if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
					frmBillPaymentCompleteCalendar.lblTxnNumValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] !=
						undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
					frmBillPaymentCompleteCalendar.lblAmountValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] !=
						undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString(
							"currencyThaiBaht") : " ";							
					//frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] !=
//						undefined ? "[" + resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + kony.i18n.getLocalizedString(
//							"currencyThaiBaht") + "]" : " ";
					frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] !=
						undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + kony.i18n.getLocalizedString(
							"currencyThaiBaht")  : " ";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmBillPaymentCompleteCalendar.imgTransNBpAckComplete
						.src = "completeico.png" : frmBillPaymentCompleteCalendar.imgTransNBpAckComplete.src = "mes2.png";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransferMB(frmBillPaymentCompleteCalendar.lblBalanceAfterPayment, 
						frmBillPaymentCompleteCalendar.lblBalAfterPayValue, frmBillPaymentCompleteCalendar.hbox101271281131304, false) : setVisiblityBalanceTransferMB(frmBillPaymentCompleteCalendar.lblBalanceAfterPayment, 
						frmBillPaymentCompleteCalendar.lblBalAfterPayValue, frmBillPaymentCompleteCalendar.hbox101271281131304, true);
				} else {
					frmBillPaymentCompleteCalendar.lblTxnNumValue.text = "";
					frmBillPaymentCompleteCalendar.lblAmountValue.text = "";
					frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text = "";
				}
				var flowQR=resulttable["ExecTransactionDetails"][0]["flexValue1"];
                
                var imagesUrl ="";
                if(isNotBlank(flowQR)){
                  if(flowQR=="QR"){
                    imagesUrl=loadBankIcon("staticqr");
                  }else{
                    imagesUrl=loadBillerIcons(resulttable["ExecTransactionDetails"][0]["billerCommCode"]);
                  }                  
                }else{
                  imagesUrl=loadBillerIcons(resulttable["ExecTransactionDetails"][0]["billerCommCode"]);
                }

				frmBillPaymentCompleteCalendar.imgBillerPic.src =imagesUrl;
				//frmBillPaymentCompleteCalendar.imgFromAccount.src =  "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblTMBBankCD+"&modIdentifier=BANKICON";
				frmBillPaymentCompleteCalendar.imgFromAccount.src = loadBankIcon(gblTMBBankCD);
				gblExecRef1ENVal=resulttable["ExecTransactionDetails"][0]["Ref1EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref1EN"] : "";
				gblExecRef2ENVal=resulttable["ExecTransactionDetails"][0]["Ref2EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref2EN"] : "";
				gblExecRef1THVal=resulttable["ExecTransactionDetails"][0]["Ref1TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref1TH"] : "";
				gblExecRef2THVal=resulttable["ExecTransactionDetails"][0]["Ref2TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref2TH"] : "";
				gblExecRef2Req=resulttable["ExecTransactionDetails"][0]["isRef2Req"]!=undefined ? resulttable["ExecTransactionDetails"][0]["isRef2Req"] : "";
				showRefValuesMB();
				
				frmBillPaymentCompleteCalendar.hbxStartEndDate.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxRepeatExcuteTimes.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxShareOption.setVisibility(true);
				//frmBillPaymentCompleteCalendar.hboxMyNote.setVisibility(true);
				//frmBillPaymentCompleteCalendar.hboxTxnNo.skin = "hboxLightGrey";
				dismissLoadingScreen();
				frmBillPaymentCompleteCalendar.hbxOAFrmDetAck.setVisibility(true);
				frmBillPaymentCompleteCalendar.show();
				
			} else {
				 dismissLoadingScreen();
				alert("No Records found");
			}
		} else {
			 dismissLoadingScreen();
		}
	}
	 dismissLoadingScreen();
}

/*
**************************************************************************************************************************************
  	Name    : getePmntTxnDetCallBackMB
  	Author  : Yugal S
  	Date    : August 8 2014
  	Purpose : Redirect to Complete page for e=payment executed txns
**************************************************************************************************************************************
--*/
function getePmntTxnDetCallBackMB(status, resulttable) {
	if (status == 400) {
		displayMEADetailsOnMyActivity(false);
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["ExecTransactionDetails"].length > 0) {
				frmBillPaymentCompleteCalendar.lblAccountName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
				frmBillPaymentCompleteCalendar.lblAccUserName.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
				//frmBillPaymentCompleteCalendar.lblAccountNum.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
					//formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
				//masking CR code
				frmBillPaymentCompleteCalendar.lblAccountNum.text = resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"] != undefined ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["maskedFromAcctId"]) : " ";
				if(resulttable["ExecTransactionDetails"][0]["billerShortName"] !=undefined && resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined ){
					frmBillPaymentCompleteCalendar.lblBillerNickname.text = resulttable["ExecTransactionDetails"][0]["billerShortName"] + "("+
						resulttable["ExecTransactionDetails"][0]["billerCommCode"] + ")";
				}
				else {
					if(resulttable["ExecTransactionDetails"][0]["billerShortName"] != undefined)
						frmBillPaymentCompleteCalendar.lblBillerNickname.text =resulttable["ExecTransactionDetails"][0]["billerShortName"];
					else if(resulttable["ExecTransactionDetails"][0]["billerCommCode"] != undefined){
						frmBillPaymentCompleteCalendar.lblBillerNickname.text  ="("+resulttable["ExecTransactionDetails"][0]["billerCommCode"]+")"
					}else{
						frmBillPaymentCompleteCalendar.lblBillerNickname.text = "";
					}
				}
				frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text = "";
				
				gblExecRef1ENVal=resulttable["ExecTransactionDetails"][0]["Ref1EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref1EN"] : "";
				gblExecRef2ENVal=resulttable["ExecTransactionDetails"][0]["Ref2EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref2EN"] : "";
				gblExecRef1THVal=resulttable["ExecTransactionDetails"][0]["Ref1TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref1TH"] : "";
				gblExecRef2THVal=resulttable["ExecTransactionDetails"][0]["Ref2TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Ref2TH"] : "";
				gblExecRef2Req=resulttable["ExecTransactionDetails"][0]["isRef2Req"]!=undefined ? resulttable["ExecTransactionDetails"][0]["isRef2Req"] : "";
				var gblExecFlex1ENVal=resulttable["ExecTransactionDetails"][0]["Flex1EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Flex1EN"] : "";
				var gblExecFlex1THVal=resulttable["ExecTransactionDetails"][0]["Flex1TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Flex1TH"] : "";
				var gblExecFlex2ENVal=resulttable["ExecTransactionDetails"][0]["Flex2EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Flex2EN"] : "";
				var gblExecFlex2THVal=resulttable["ExecTransactionDetails"][0]["Flex2TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Flex2TH"] : "";
				var gblExecFlex3ENVal=resulttable["ExecTransactionDetails"][0]["Flex3EN"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Flex3EN"] : "";
				var gblExecFlex3THVal=resulttable["ExecTransactionDetails"][0]["Flex3TH"]!=undefined ? resulttable["ExecTransactionDetails"][0]["Flex3TH"] : "";
				if(resulttable["ExecTransactionDetails"][0]["flexValue1"] != undefined) {
					frmBillPaymentCompleteCalendar.hbxFlexiParam1.setVisibility(true);
					frmBillPaymentCompleteCalendar.lblFlexiParam1Val.text = resulttable["ExecTransactionDetails"][0]["flexValue1"];
					if(kony.i18n.getCurrentLocale() == "en_US")
						frmBillPaymentCompleteCalendar.lblFlexiParam1.text = gblExecFlex1ENVal;
					else
						frmBillPaymentCompleteCalendar.lblFlexiParam1.text = gblExecFlex1THVal;
				}
				if(resulttable["ExecTransactionDetails"][0]["flexValue2"] != undefined) {
					frmBillPaymentCompleteCalendar.hbxFlexiParam2.setVisibility(true);
					frmBillPaymentCompleteCalendar.lblFlexiParam2Val.text = resulttable["ExecTransactionDetails"][0]["flexValue2"];
					if(kony.i18n.getCurrentLocale() == "en_US")
						frmBillPaymentCompleteCalendar.lblFlexiParam2.text = gblExecFlex2ENVal;
					else
						frmBillPaymentCompleteCalendar.lblFlexiParam2.text = gblExecFlex2THVal;
				}
				if( resulttable["ExecTransactionDetails"][0]["flexValue3"] != undefined) {
					frmBillPaymentCompleteCalendar.hbxFlexiParam3.setVisibility(true);
					frmBillPaymentCompleteCalendar.lblFlexiParam3Val.text = resulttable["ExecTransactionDetails"][0]["flexValue3"];
					if(kony.i18n.getCurrentLocale() == "en_US")
						frmBillPaymentCompleteCalendar.lblFlexiParam3.text = gblExecFlex3ENVal;
					else
						frmBillPaymentCompleteCalendar.lblFlexiParam3.text = gblExecFlex3THVal;
				}
				frmBillPaymentCompleteCalendar.lblBalAfterPayValue.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? 
					resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
				//frmBillPaymentCompleteCalendar.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["billerRef1"] !=
					//undefined ? resulttable["ExecTransactionDetails"][0]["billerRef1"] : " ";
				//masking CR code
				frmBillPaymentCompleteCalendar.lblRef1Value.text = resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["maskedBillerRef1"] : " ";
				if(resulttable["ExecTransactionDetails"][0]["billerRef2"] != undefined){
					frmBillPaymentCompleteCalendar.lblRef2Value.text = resulttable["ExecTransactionDetails"][0]["billerRef2"];
					frmBillPaymentCompleteCalendar.hbxRef2.setVisibility(true);
				} else {
					frmBillPaymentCompleteCalendar.lblRef2Value.text = "";
					frmBillPaymentCompleteCalendar.hbxRef2.setVisibility(false);
				}
				frmBillPaymentCompleteCalendar.lblPaymentDateValue.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ?
					resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
				if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
					frmBillPaymentCompleteCalendar.lblTxnNumValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
						"transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
					frmBillPaymentCompleteCalendar.lblAmountValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] !=
						undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString(
							"currencyThaiBaht") : " ";
					frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] !=
						undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["fee"] + " " + kony.i18n.getLocalizedString(
							"currencyThaiBaht") : " ";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmBillPaymentCompleteCalendar.imgTransNBpAckComplete
						.src = "iconcomplete.png" : frmBillPaymentCompleteCalendar.imgTransNBpAckComplete.src = "icon_notcomplete.png";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransferMB(frmBillPaymentCompleteCalendar.lblBalanceAfterPayment, 
						frmBillPaymentCompleteCalendar.lblBalAfterPayValue, frmBillPaymentCompleteCalendar.hbox101271281131304, false) : setVisiblityBalanceTransferMB(frmBillPaymentCompleteCalendar.lblBalanceAfterPayment, 
						frmBillPaymentCompleteCalendar.lblBalAfterPayValue, frmBillPaymentCompleteCalendar.hbox101271281131304, true);
				}
				else {
					frmBillPaymentCompleteCalendar.lblTxnNumValue.text = "";
					frmBillPaymentCompleteCalendar.lblAmountValue.text = "";
					frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text = "";
				}
				var flowQR=resulttable["ExecTransactionDetails"][0]["flexValue1"];
                
                var imagesUrl ="";
                if(isNotBlank(flowQR)){
                  if(flowQR=="QR"){
                    imagesUrl=loadBankIcon("staticqr");
                  }else{
                    imagesUrl=loadBillerIcons(resulttable["ExecTransactionDetails"][0]["billerCommCode"]);
                  }                  
                }else{
                  imagesUrl=loadBillerIcons(resulttable["ExecTransactionDetails"][0]["billerCommCode"]);
                }

				frmBillPaymentCompleteCalendar.imgBillerPic.src =imagesUrl;
				frmBillPaymentCompleteCalendar.imgFromAccount.src =  "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["ICON_ID"]+"&modIdentifier=PRODICON";
				
				showRefValuesMB();
				//frmBillPaymentCompleteCalendar.hbxScheduleDetailsConfirm.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxEasyPass.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxEasyPassTxnId.setVisibility(false);
				frmBillPaymentCompleteCalendar.hbxCardBal.setVisibility(false);
				//frmBillPaymentCompleteCalendar.hboxMyNote.setVisibility(false);
				//frmBillPaymentCompleteCalendar.hboxTxnNo.skin = "";
				frmBillPaymentCompleteCalendar.hbxOAFrmDetAck.setVisibility(true);
				dismissLoadingScreen();
				frmBillPaymentCompleteCalendar.show();
			} else {
				dismissLoadingScreen();
				alert("No Records found");
			}
		} else {
			dismissLoadingScreen();
		}
	}
	dismissLoadingScreen();
}

/*
**************************************************************************************************************************************
  	Name    : getSCTxnDetCallBackMB
  	Author  : Rishika Kushwaha
  	Date    : Nov 7 2013
  	Purpose : Redirect to Complete page for executed transactions from Calendar 
**************************************************************************************************************************************
--*/
function getSCTxnDetCallBackMB(status,resulttable){
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["ExecTransactionDetails"].length > 0) {
					gblProdNameEN = resulttable["openProductNameEN"];
					gblBranchNameEN = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"];
					gblProdNameTH = resulttable["openProductNameTH"];
					gblBranchNameTH = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameTH"];
					if (kony.i18n.getCurrentLocale() == "en_US") {
						frmOpenActSavingCareCnfNAckCalendar.lblOASCBranchVal.text = gblBranchNameEN;
						frmOpenActSavingCareCnfNAckCalendar.lblOASCTitle.text=gblProdNameEN;
					} else {
						frmOpenActSavingCareCnfNAckCalendar.lblOASCBranchVal.text = gblBranchNameTH;
						frmOpenActSavingCareCnfNAckCalendar.lblOASCTitle.text=gblProdNameTH;
					}		
					frmOpenActSavingCareCnfNAckCalendar.lblOASCIntRateVal.text = resulttable["ExecTransactionDetails"][0]["TDInterestRate"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDInterestRate"] +""+"%": " ";
					frmOpenActSavingCareCnfNAckCalendar.lblOASCFrmNameAck.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
					frmOpenActSavingCareCnfNAckCalendar.lblOASCFrmTypeAck.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
					frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBalAmt.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString(
						"currencyThaiBaht") : " ";						
					frmOpenActSavingCareCnfNAckCalendar.lblOASCNicNamVal.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
					frmOpenActSavingCareCnfNAckCalendar.lblOASCActNumVal.text = resulttable["ExecTransactionDetails"][0]["toAccId"] !=
					undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
					frmOpenActSavingCareCnfNAckCalendar.lblOASCActNameVal.text = resulttable["ExecTransactionDetails"][0]["toAccName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
					frmOpenActSavingCareCnfNAckCalendar.lblOASCOpenDateVal.text =resulttable["ExecTransactionDetails"][0]["transferDate"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
					frmOpenActSavingCareCnfNAckCalendar.imgOASCFrmPicAck.src =  "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["ICON_ID"]+"&modIdentifier=PRODICON";
					frmOpenActSavingCareCnfNAckCalendar.imgOASCTitle.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["openICONID"]+"&modIdentifier=PRODICON";
					if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
					frmOpenActSavingCareCnfNAckCalendar.lblOASCTranRefNoVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
						"transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
					frmOpenActSavingCareCnfNAckCalendar.lblOASCOpenActVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmOpenActSavingCareCnfNAckCalendar.imgOASCComplete
						.src = "completeico.png" : frmOpenActSavingCareCnfNAckCalendar.imgOASCComplete.src = "mes2.png";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransferMB(frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBal, 
						frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBalAmt, frmOpenActSavingCareCnfNAckCalendar.hbox101271281131304, false) : setVisiblityBalanceTransferMB(frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBal, 
						frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBalAmt, frmOpenActSavingCareCnfNAckCalendar.hbox101271281131304, true);
				} else {
					frmOpenActSavingCareCnfNAckCalendar.lblOASCTranRefNoVal.text = "";
					frmOpenActSavingCareCnfNAckCalendar.lblOASCOpenActVal.text = "";
				}
				careDetails = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"];
                if (careDetails.length == 1) {
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"]+""+"%";
                } else if (careDetails.length == 2) {
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm2.setVisibility(true);
                } else if (careDetails.length == 3) {
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm3.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm3.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm3.setVisibility(true);
                } else if (careDetails.length == 4) {
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm3.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm3.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm3.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm4.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm4.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm4.setVisibility(true);
                } else if (careDetails.length == 5) {
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal1.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][0]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal2.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][1]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal3.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][2]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal4.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][3]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.lblBefNameValCnfrm5.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][4]["careBeneficiaryName"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefRsCnfrmVal5.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][4]["careBeneficiaryRelation"];
                    frmOpenActSavingCareCnfNAckCalendar.lblBefPerCnfrmVal5.text = resulttable["ExecTransactionDetails"][0]["CareBeneficiaryDetails"][4]["careBeneficiaryPct"]+""+"%";
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm2.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm3.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm3.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm3.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm4.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm4.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm4.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefNameCnfrm5.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefRsCnfrm5.setVisibility(true);
                    frmOpenActSavingCareCnfNAckCalendar.hbxBefPerCnfrm5.setVisibility(true);
                }
					dismissLoadingScreen();
					frmOpenActSavingCareCnfNAckCalendar.show();
	
					
			}
		}
	}
	dismissLoadingScreen();
}

/*
**************************************************************************************************************************************
  	Name    : getTDTxnDetCallBackMB
  	Author  : Rishika Kushwaha
  	Date    : Nov 7 2013
  	Purpose : Redirect to Complete page for executed transactions from Calendar 
**************************************************************************************************************************************
--*/
function getTDTxnDetCallBackMB(status,resulttable){
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["ExecTransactionDetails"].length > 0) {
					
					 gblProdNameEN = resulttable["openProductNameEN"];
	                gblBranchNameEN = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"];
	                gblProdNameTH = resulttable["openProductNameTH"];
	                gblBranchNameTH = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameTH"];
			        if (kony.i18n.getCurrentLocale() == "en_US") {
	                    frmOpenActTDAckCalendar.lblOATDAckTitle.text = gblProdNameEN;
	                    frmOpenActTDAckCalendar.lblOATDBranchValAck.text = gblBranchNameEN;
	                } else {
	                    frmOpenActTDAckCalendar.lblOATDAckTitle.text = gblProdNameTH;
	                    frmOpenActTDAckCalendar.lblOATDBranchValAck.text = gblBranchNameTH;
	                }
			        frmOpenActTDAckCalendar.lblOATDTermValAck.text = resulttable["ExecTransactionDetails"][0]["TDTerm"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDTerm"] : "";	
					frmOpenActTDAckCalendar.lblOAFrmNameAck.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
					frmOpenActTDAckCalendar.lblOAFrmTypeAck.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
					frmOpenActTDAckCalendar.lblOpenActCnfmBalAmt.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString(
						"currencyThaiBaht") : " ";						
					frmOpenActTDAckCalendar.lblOATDNickNameValAck.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
					frmOpenActTDAckCalendar.lblOATDAckActNoVal.text = resulttable["ExecTransactionDetails"][0]["toAccId"] !=
					undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
					frmOpenActTDAckCalendar.lblOATDActNameValAck.text = resulttable["ExecTransactionDetails"][0]["toAccName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
					frmOpenActTDAckCalendar.lblTDOpenDateValAck.text =resulttable["ExecTransactionDetails"][0]["transferDate"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
					frmOpenActTDAckCalendar.lblTDMatDateValAck.text = resulttable["ExecTransactionDetails"][0]["TDMaturityDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDMaturityDate"] : "";
					frmOpenActTDAckCalendar.imgOAFrmPicAck.src =  "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["ICON_ID"]+"&modIdentifier=PRODICON";
					frmOpenActTDAckCalendar.imgOATDAckTitle.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["openICONID"]+"&modIdentifier=PRODICON";
					frmOpenActTDAckCalendar.lblOATDIntRateValAck.text = resulttable["ExecTransactionDetails"][0]["TDInterestRate"] != undefined ? resulttable["ExecTransactionDetails"][0]["TDInterestRate"]+""+"%" : "";					
					if(resulttable["ExecTransactionDetails"][0]["openPrdCode"] == '300' || resulttable["ExecTransactionDetails"][0]["openPrdCode"] == '301' ||resulttable["ExecTransactionDetails"][0]["openPrdCode"] == '302'){
	                	frmOpenActTDAckCalendar.lblOATDToActType.text = resulttable["ExecTransactionDetails"][0]["affliatedAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["affliatedAcctId"]) : "";
	                	frmOpenActTDAckCalendar.lblOATDToNicNam.text = resulttable["ExecTransactionDetails"][0]["affliatedAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["affliatedAcctNickName"] : "";
	                	frmOpenActTDAckCalendar.hbxOAPayIntAck.setVisibility(false);
	                	frmOpenActTDAckCalendar.lblOAPayIntAck.setVisibility(false);
	                }else{
	                	frmOpenActTDAckCalendar.imgOATDToAck.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["AFFLIATED_ICON_ID"]+"&modIdentifier=PRODICON";
	                	frmOpenActTDAckCalendar.lblOATDToActType.text = resulttable["ExecTransactionDetails"][0]["affliatedAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["affliatedAcctId"]) : "";
	                	frmOpenActTDAckCalendar.lblOATDToNicNam.text = resulttable["ExecTransactionDetails"][0]["affliatedAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["affliatedAcctNickName"] : "";
	                	frmOpenActTDAckCalendar.hbxOAPayIntAck.setVisibility(true);
	                	frmOpenActTDAckCalendar.lblOAPayIntAck.setVisibility(true);
	                }
					if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
						frmOpenActTDAckCalendar.lblOATDTransRefAckVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
							"transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
						frmOpenActTDAckCalendar.lblOATDAmtValAck.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
						resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmOpenActTDAckCalendar.imgOATDAckComplete
							.src = "completeico.png" : frmOpenActTDAckCalendar.imgOATDAckComplete.src = "mes2.png";
						resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransferMB(frmOpenActTDAckCalendar.lblOpenActCnfmBal, 
							frmOpenActTDAckCalendar.lblOpenActCnfmBalAmt, frmOpenActTDAckCalendar.hbox101271281131304, false) : setVisiblityBalanceTransferMB(frmOpenActTDAckCalendar.lblOpenActCnfmBal, 
							frmOpenActTDAckCalendar.lblOpenActCnfmBalAmt, frmOpenActTDAckCalendar.hbox101271281131304, true);
					} else {
					frmOpenActTDAckCalendar.lblOATDAmtValAck.text = "";
						frmOpenActTDAckCalendar.lblOATDTransRefAckVal.text = "";
					}
					dismissLoadingScreen();
					frmOpenActTDAckCalendar.show();
			}
		}
	}
	dismissLoadingScreen();
}

/*
**************************************************************************************************************************************
  	Name    : getDSTxnDetCallBackMB
  	Author  : Rishika Kushwaha
  	Date    : Nov 7 2013
  	Purpose : Redirect to Complete page for executed transactions from Calendar 
**************************************************************************************************************************************
--*/
function getDSTxnDetCallBackMB(status,resulttable){
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["ExecTransactionDetails"].length > 0) {
					
					var currentLocale = kony.i18n.getCurrentLocale();
			        if (currentLocale == "en_US") {
			            frmOpenActDSAckCalendar.lblDSAckTitle.text = resulttable["openProductNameEN"] !=
						undefined ? resulttable["openProductNameEN"] : " ";
			        } else if (currentLocale == "th_TH") {
			            frmOpenActDSAckCalendar.lblDSAckTitle.text = resulttable["openProductNameTH"] !=
						undefined ? resulttable["openProductNameTH"] : " ";
			        }
					frmOpenActDSAckCalendar.lblOAMnthlySavNameAck.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
					frmOpenActDSAckCalendar.lblOAMnthlySavTypeAck.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
					frmOpenActDSAckCalendar.lblOpenActCnfmBalAmt.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString(
						"currencyThaiBaht") : " ";						
					frmOpenActDSAckCalendar.lblOADSNickNameValAck.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
					frmOpenActDSAckCalendar.lblDSAckActNoVal.text = resulttable["ExecTransactionDetails"][0]["toAccId"] !=
					undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
					frmOpenActDSAckCalendar.lblOADSActNameValAck.text = resulttable["ExecTransactionDetails"][0]["toAccName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
					//frmOpenActDSAckCalendar.lblOADSAmtValAck.text =
					//frmOpenActDSAckCalendar.lblOADSBranchValAck.text =
					//frmOpenActDSAckCalendar.lblOADIntRateValAck.text =
					frmOpenActDSAckCalendar.lblDSOpenDateValAck.text =resulttable["ExecTransactionDetails"][0]["transferDate"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
					frmOpenActDSAckCalendar.imgOAMnthlySavPicAck.src  =  "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["ICON_ID"]+"&modIdentifier=PRODICON";
					frmOpenActDSAckCalendar.imgOAMnthlySavPicAck.src  = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["openICONID"]+"&modIdentifier=PRODICON";
					
					if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
					frmOpenActDSAckCalendar.lblOATDTransRefAckVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
						"transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"].slice(2) : " ";
					
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmOpenActDSAckCalendar.imgODSAckComplete
						.src = "completeico.png" : frmOpenActDSAckCalendar.imgODSAckComplete.src = "mes2.png";
				} else {
					frmOpenActDSAckCalendar.lblOATDTransRefAckVal.text = "";
				}
					dismissLoadingScreen();
					frmOpenActDSAckCalendar.show();
	
					
			}
		}
	}
	dismissLoadingScreen();
}

/*
**************************************************************************************************************************************
  	Name    : getNSTxnDetCallBackMB
  	Author  : Rishika Kushwaha
  	Date    : Nov 7 2013
  	Purpose : Redirect to Complete page for executed transactions from Calendar 
**************************************************************************************************************************************
--*/
function getNSTxnDetCallBackMB(status,resulttable){
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			if (resulttable["ExecTransactionDetails"].length > 0) {
				
				
				var currentLocale = kony.i18n.getCurrentLocale();
		        if (currentLocale == "en_US") {
		            frmOpenAccountNSConfirmationCalendar.lblProdNSConfName.text = resulttable["openProductNameEN"] !=
					undefined ? resulttable["openProductNameEN"] : " ";
					frmOpenAccountNSConfirmationCalendar.lblProdTypeVal.text = resulttable["openProductNameEN"] !=
					undefined ? resulttable["openProductNameEN"] : " ";
		        } else if (currentLocale == "th_TH") {
		            frmOpenAccountNSConfirmationCalendar.lblProdNSConfName.text = resulttable["openProductNameTH"] !=
					undefined ? resulttable["openProductNameTH"] : " ";
					frmOpenAccountNSConfirmationCalendar.lblProdTypeVal.text = resulttable["openProductNameTH"] !=
					undefined ? resulttable["openProductNameTH"] : " ";
		        }	
    
              	gblProdNameEN = resulttable["openProductNameEN"] !=	undefined ? resulttable["openProductNameEN"] : " ";
              	gblProdNameTH = resulttable["openProductNameTH"] !=	undefined ? resulttable["openProductNameTH"] : " ";
              	gblBranchNameEN = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"] : " ";
                gblBranchNameTH = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameTH"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAcctBranchNameTH"] : " ";
         			
				frmOpenAccountNSConfirmationCalendar.lblNFAccName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
				frmOpenAccountNSConfirmationCalendar.lblNFAccBal.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ?
					formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
				frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString(
					"currencyThaiBaht") : " ";
				frmOpenAccountNSConfirmationCalendar.lblNickNameVal.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
				frmOpenAccountNSConfirmationCalendar.lblAccuntNoVal.text = resulttable["ExecTransactionDetails"][0]["toAccId"] !=
					undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
				frmOpenAccountNSConfirmationCalendar.lblActName.text = resulttable["ExecTransactionDetails"][0]["toAccName"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
				frmOpenAccountNSConfirmationCalendar.lblOpeningDateVal.text = resulttable["ExecTransactionDetails"][0]["transferDate"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
				frmOpenAccountNSConfirmationCalendar.lblBranchVal.text = resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["toAcctBranchNameEN"] : " ";
				frmOpenAccountNSConfirmationCalendar.lblInterestRateVal.text = resulttable["ExecTransactionDetails"][0]["TDInterestRate"] !=
					undefined ? resulttable["ExecTransactionDetails"][0]["TDInterestRate"] + "" +"%": " ";
				frmOpenAccountNSConfirmationCalendar.imgNoFix.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["ICON_ID"]+"&modIdentifier=PRODICON";
				frmOpenAccountNSConfirmationCalendar.imgProdNSConfName.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["openICONID"]+"&modIdentifier=PRODICON";
				
				  if(resulttable["ExecTransactionDetails"][0]["openPrdCode"] !=null &&  (resulttable["ExecTransactionDetails"][0]["openPrdCode"] == "225" || resulttable["ExecTransactionDetails"][0]["openPrdCode"]=="226")){
	              	frmOpenAccountNSConfirmationCalendar.hbxCardFee.setVisibility(true);
					frmOpenAccountNSConfirmationCalendar.hbxCardType.setVisibility(true);					
					frmOpenAccountNSConfirmationCalendar.lblAddressHeader.setVisibility(true);
					frmOpenAccountNSConfirmationCalendar.lblAddress1.setVisibility(true);
					frmOpenAccountNSConfirmationCalendar.lblAddress2.setVisibility(true);
	              	frmOpenAccountNSConfirmationCalendar.lblAddressHeader.text = appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
	              	frmOpenAccountNSConfirmationCalendar.lblAddress1.text = resulttable["ExecTransactionDetails"][0]["flexValue3"];;
					frmOpenAccountNSConfirmationCalendar.lblAddress2.text = resulttable["ExecTransactionDetails"][0]["flexValue4"];;
					frmOpenAccountNSConfirmationCalendar.lblCardFeeValue.text = resulttable["ExecTransactionDetails"][0]["flexValue2"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
					frmOpenAccountNSConfirmationCalendar.lblCardTypeValue.text = resulttable["ExecTransactionDetails"][0]["flexValue1"];
					
               }else{
	              	frmOpenAccountNSConfirmationCalendar.hbxCardFee.setVisibility(false);
					frmOpenAccountNSConfirmationCalendar.hbxCardType.setVisibility(false);
					frmOpenAccountNSConfirmationCalendar.lblAddressHeader.text=appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
					frmOpenAccountNSConfirmationCalendar.lblAddressHeader.setVisibility(false);
					frmOpenAccountNSConfirmationCalendar.lblAddress1.setVisibility(false);
					frmOpenAccountNSConfirmationCalendar.lblAddress2.setVisibility(false);
               }
				if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) {
					frmOpenAccountNSConfirmationCalendar.lbltransactionVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
						"transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
					frmOpenAccountNSConfirmationCalendar.lblOpenAmtVal.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
						"amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
				
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmOpenAccountNSConfirmationCalendar.imgTxnStatus.src = 
						"completeico.png" : frmOpenAccountNSConfirmationCalendar.imgTxnStatus.src = "mes2.png";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? setVisiblityBalanceTransferMB(frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBal, 
							frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt, frmOpenAccountNSConfirmationCalendar.hbox101271281131304, false) : setVisiblityBalanceTransferMB(frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBal, 
							frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt, frmOpenAccountNSConfirmationCalendar.hbox101271281131304, true);
				} else {
					frmOpenAccountNSConfirmationCalendar.lbltransactionVal.text = "";
				}
				dismissLoadingScreen();
				frmOpenAccountNSConfirmationCalendar.show();
			}
		}
	}
	dismissLoadingScreen();
}

/*
**************************************************************************************************************************************
  	Name    : getS2STxnDetCallBackMB
  	Author  : Rishika Kushwaha
  	Date    : Nov 7 2013
  	Purpose : Redirect to Complete page for executed transactions from Calendar 
**************************************************************************************************************************************
--*/

function getS2STxnDetCallBackMB(status, resulttable){
if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            
            if (resulttable["ExecTransactionDetails"].length > 0) {
                
                
                frmSSSExecuteCalendar.lblAmntLmtMax.text = resulttable["fromMaxAmount"] != undefined ? commaFormatted(resulttable["fromMaxAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                frmSSSExecuteCalendar.lblAmntLmtMin.text = resulttable["toMaxAmount"] != undefined ? commaFormatted(resulttable["toMaxAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
                frmSSSExecuteCalendar.lblSendToAccType.text = resulttable["ExecTransactionDetails"][0]["toAccName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccName"] : " ";
                frmSSSExecuteCalendar.lblSendFromAccType.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
				frmSSSExecuteCalendar.lblSendToAccBal.text = resulttable["ExecTransactionDetails"][0]["toAcctBalance"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAcctBalance"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
				frmSSSExecuteCalendar.lblSendFromAccName.text = resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctNickName"] : " ";
				//frmSSSExecuteCalendar.lblSendFromAccType.text = resulttable["ExecTransactionDetails"][0]["fromAcctName"] != undefined ? resulttable["ExecTransactionDetails"][0]["fromAcctName"] : " ";
				frmSSSExecuteCalendar.lblSendFromAccNo.text = resulttable["ExecTransactionDetails"][0]["fromAcctId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["fromAcctId"]) : " ";
				frmSSSExecuteCalendar.lblSendToAccName.text = resulttable["ExecTransactionDetails"][0]["toAccNickName"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccNickName"] : " ";
				frmSSSExecuteCalendar.lblFromAccBalAftr.text = resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] != undefined ? resulttable["ExecTransactionDetails"][0]["balAfterPymnt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") : " ";
				//frmSSSExecuteCalendar.lblTrnsAmnt.text = resulttable["ExecTransactionDetails"][0]["totalAmount"] != undefined ? resulttable["ExecTransactionDetails"][0]["totalAmount"] : " ";
				frmSSSExecuteCalendar.lblSendToAccNo.text = resulttable["ExecTransactionDetails"][0]["toAccId"] != undefined ? formatAccountNumber(resulttable["ExecTransactionDetails"][0]["toAccId"]) : " ";
				frmSSSExecuteCalendar.lblTransDate.text = resulttable["ExecTransactionDetails"][0]["transferDate"] != undefined ? resulttable["ExecTransactionDetails"][0]["transferDate"] : " ";
				
				if (resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"].length > 0) 
				{
					frmSSSExecuteCalendar.lblTrnsRefNo.text = resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
					 "transRefId"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["transRefId"] : " ";
					 
					frmSSSExecuteCalendar.lblTrnsAmnt.text=resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0][
					 "amount"] != undefined ? resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["amount"] + " " + kony.i18n.getLocalizedString(
						"currencyThaiBaht") : " "; 						
					
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "01" ? frmSSSExecuteCalendar.imgComplete.src = "completeico.png" : frmSSSExecuteCalendar.imgComplete.src = "mes2.png";
					resulttable["ExecTransactionDetails"][0]["AmntFeeDetails"][0]["txnStatus"] == "02" ? frmSSSExecuteCalendar.hbxFromAccBalAftr.setVisibility(false) : frmSSSExecuteCalendar.hbxFromAccBalAftr.setVisibility(true);
				} else {
				 frmSSSExecuteCalendar.lblTrnsRefNo.text = "";
				}
				frmSSSExecuteCalendar.imgFrom.src =  "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable["ICON_ID"]+"&modIdentifier=PRODICON";
				var personalizedId=resulttable["ExecTransactionDetails"][0]["PersonlizedID"] != undefined ? resulttable["ExecTransactionDetails"][0]["PersonlizedID"] : "";
				var toAccPic=resulttable["ExecTransactionDetails"][0]["toAccPic"] != undefined ? resulttable["ExecTransactionDetails"][0]["toAccPic"] : "";
				var ownflag=resulttable["ExecTransactionDetails"][0]["ownFlag"] != undefined ? resulttable["ExecTransactionDetails"][0]["ownFlag"] : "";
				
				frmSSSExecuteCalendar.imgTo.src=getRecipientTxnCompletePic(ownflag,toAccPic,personalizedId);
							dismissLoadingScreen();
				frmSSSExecuteCalendar.show();
            }
        }
    }
    dismissLoadingScreen();
}





function savePDFExecTOANSMB(filetype) {
	var inputParam = {};
	var TOANSJsondata = {};
	TOANSJsondata={
     		"toAccountNo" : frmOpenAccountNSConfirmationCalendar.lblAccuntNoVal.text,
			"toAccountName" : frmOpenAccountNSConfirmationCalendar.lblActName.text,
			"productName" : frmOpenAccountNSConfirmationCalendar.lblProdTypeVal.text,
			"branch" : frmOpenAccountNSConfirmationCalendar.lblBranchVal.text,
			"fromAccountNo" : frmOpenAccountNSConfirmationCalendar.lblNFAccBal.text,
			"fromAccountName" : frmOpenAccountNSConfirmationCalendar.lblNFAccName.text,
			"Amount" : frmOpenAccountNSConfirmationCalendar.lblOpenAmtVal.text,
			"openingDate" : frmOpenAccountNSConfirmationCalendar.lblOpeningDateVal.text,
			"TransactionRefNo" : frmOpenAccountNSConfirmationCalendar.lbltransactionVal.text,
	 		"localeId" : kony.i18n.getCurrentLocale()
	 		}
	inputParam["outputtemplatename"]= "OpenAccount_Details_"+kony.os.date("DDMMYYYY");
	inputParam["filetype"] = filetype;
    inputParam["templatename"] = "OpenAccountNS";
    inputParam["datajson"] = JSON.stringify(TOANSJsondata, "", "");
    
    saveOpenAccountPDFIB(filetype, "063", frmOpenAccountNSConfirmationCalendar.lbltransactionVal.text, "NS");
	//invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
}

function savePDFExecTOASCMB(filetype) {
	var inputParam = {};
	var TOANSJsondata = {};
	TOANSJsondata={
     		"toAccountNo" : frmOpenActSavingCareCnfNAckCalendar.lblOASCActNumVal.text,
			"toAccountName" : frmOpenActSavingCareCnfNAckCalendar.lblOASCActNameVal.text,
			"productName" : gblProdNameEN,
			"branch" : frmOpenActSavingCareCnfNAckCalendar.lblOASCBranchVal.text,
			"fromAccountNo" : frmOpenActSavingCareCnfNAckCalendar.lblOASCFrmTypeAck.text,
			"fromAccountName" : frmOpenActSavingCareCnfNAckCalendar.lblOASCFrmNameAck.text,
			"Amount" : frmOpenActSavingCareCnfNAckCalendar.lblOASCOpenActVal.text,
			"openingDate" : frmOpenActSavingCareCnfNAckCalendar.lblOASCOpenDateVal.text,
			"TransactionRefNo" : frmOpenActSavingCareCnfNAckCalendar.lblOASCTranRefNoVal.text,
			"beneficiariesList" : careDetails, 
	 		"localeId" : kony.i18n.getCurrentLocale()
	 		}
	inputParam["outputtemplatename"]= "OpenAccount_Details_"+kony.os.date("DDMMYYYY");
	inputParam["filetype"] = filetype;
    inputParam["templatename"] = "OpenAccountSC";
    inputParam["datajson"] = JSON.stringify(TOANSJsondata, "", "");
	invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
}


function savePDFExecTOADSMB(filetype) {
	var inputParam = {};
	var TOANSJsondata = {};
	TOANSJsondata={
     		"toAccountNo" : frmOpenActDSAckCalendar.lblDSAckActNoVal.text,
			"toAccountName" : frmOpenActDSAckCalendar.lblOADSActNameValAck.text,
			"productName" : frmOpenActDSAckCalendar.lblDSAckTitle.text,
			"branch" : frmOpenActDSAckCalendar.lblOADSBranchValAck.text,
			"fromAccountNo" : frmOpenActDSAckCalendar.lblOAMnthlySavTypeAck.text,
			"fromAccountName" : frmOpenActDSAckCalendar.lblOAMnthlySavNameAck.text,
			"openingdate" : frmOpenActDSAckCalendar.lblDSOpenDateValAck.text,
			"dreamdesc" : "",
			"targetAmount" : frmOpenActDSAckCalendar.lblOADreamDetailTarAmtValAck.text,
			"term" : "",
			"transferrecdate" : "",
	 		"localeId" : kony.i18n.getCurrentLocale()
	 		}
	inputParam["outputtemplatename"]= "OpenAccount_Details_"+kony.os.date("DDMMYYYY");
	inputParam["filetype"] = filetype;
    inputParam["templatename"] = "OpenAccountDS";
    inputParam["datajson"] = JSON.stringify(TOANSJsondata, "", "");
	invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
}

function savePDFExecTOATDMB(filetype) {
	var inputParam = {};
	var TOANSJsondata = {};
	TOANSJsondata={
     		"toAccountNo" : frmOpenActTDAckCalendar.lblOATDAckActNoVal.text,
			"toAccountName" : frmOpenActTDAckCalendar.lblOATDActNameValAck.text,
			"productName" : frmOpenActTDAckCalendar.lblOATDAckTitle.text,
			"branch" : frmOpenActTDAckCalendar.lblOATDBranchValAck.text,
			"fromAccountNo" : frmOpenActTDAckCalendar.lblOAFrmTypeAck.text,
			"fromAccountName" : frmOpenActTDAckCalendar.lblOAFrmNameAck.text,
			"affiliatedAccountNo" : frmOpenActTDAckCalendar.lblOATDToAmt.text,
			"affiliatedAccountName" : frmOpenActTDAckCalendar.lblOATDToActType.text,
			"Amount" : frmOpenActTDAckCalendar.lblOATDAmtValAck.text,
			"openingDate" : frmOpenActTDAckCalendar.lblTDOpenDateValAck.text,
			"term" : frmOpenActTDAckCalendar.lblOATDTermValAck.text,
			"maturityDate" : frmOpenActTDAckCalendar.lblTDMatDateValAck.text,
			"TransactionRefNo" :  frmOpenActTDAckCalendar.lblOATDTransRefAckVal.text,
	 		"localeId" : kony.i18n.getCurrentLocale()
	 		}
	inputParam["outputtemplatename"]= "OpenAccount_Details_"+kony.os.date("DDMMYYYY");
	inputParam["filetype"] = filetype;
    inputParam["templatename"] = "OpenAccountTD";
    inputParam["datajson"] = JSON.stringify(TOANSJsondata, "", "");

 	saveOpenAccountPDFIB(filetype, "063", frmOpenActTDAckCalendar.lblOATDTransRefAckVal.text, "TD");
//	invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);
}

function showRefValuesMB(){
	if(kony.i18n.getCurrentLocale()=="en_US"){
		frmBillPaymentCompleteCalendar.lblRef1.text = gblExecRef1ENVal;
		if(gblExecRef2Req=="Y" && frmBillPaymentCompleteCalendar.hbxRef2.isVisible == true){
			frmBillPaymentCompleteCalendar.lblRef2.text=gblExecRef2ENVal;
			frmBillPaymentCompleteCalendar.hbxRef2.setVisibility(true);
		}else if(gblExecRef2Req=="N" && frmBillPaymentCompleteCalendar.hbxRef2.isVisible == true){
			frmBillPaymentCompleteCalendar.hbxRef2.setVisibility(false) ;
		}
		
	}else if(kony.i18n.getCurrentLocale()=="th_TH"){
		frmBillPaymentCompleteCalendar.lblRef1.text = gblExecRef1THVal;
		if(gblExecRef2Req=="Y" && frmBillPaymentCompleteCalendar.hbxRef2.isVisible == true){
			frmBillPaymentCompleteCalendar.lblRef2.text=gblExecRef2THVal;
			frmBillPaymentCompleteCalendar.hbxRef2.setVisibility(true);
		}else if(gblExecRef2Req=="N" && frmBillPaymentCompleteCalendar.hbxRef2.isVisible == true){
			frmBillPaymentCompleteCalendar.hbxRef2.setVisibility(false) ;
		}
	}

}

function fbShareExecutedTxnMB(){
	var currentForm=kony.application.getCurrentForm();	
	gblPOWcustNME=gblCustomerName;
	if(kony.application.getCurrentForm().id == "frmTransfersAckCalendar"){
	   	gblPOWtransXN="Transfer";
	   	gblPOWchannel="Mobile Banking";
	}else if(kony.application.getCurrentForm().id == "frmBillPaymentCompleteCalendar" && frmBillPaymentCompleteCalendar.hbxRef2.isVisible == true){
	   	gblPOWtransXN="Bill Payment";
	   	gblPOWchannel="Mobile Banking";
	}else if(kony.application.getCurrentForm().id == "frmBillPaymentCompleteCalendar" && frmBillPaymentCompleteCalendar.hbxRef2.isVisible == false){
	   	gblPOWtransXN="Top-Up";
	   	gblPOWchannel="Mobile Banking";
	}else if(kony.application.getCurrentForm().id == "frmBBExecuteConfirmAndComplete"){
	   	gblPOWtransXN="Execute Beep & Bill";
	   	gblPOWchannel="Mobile Banking";
	}else if(kony.application.getCurrentForm().id == "frmSSSExecuteCalendar"){
	   	gblPOWtransXN="Execute Send to Save";
	   	gblPOWchannel="Mobile Banking";
	}else if(kony.application.getCurrentForm().id == "frmOpenAccountNSConfirmationCalendar"){
	   	gblPOWtransXN="Open Account";
	   	gblPOWchannel="Mobile Banking";
	}else if(kony.application.getCurrentForm().id == "frmOpenActSavingCareCnfNAckCalendar"){
	   	gblPOWtransXN="Open Account";
	   	gblPOWchannel="Mobile Banking";
	}else if(kony.application.getCurrentForm().id == "frmOpenActTDAckCalendar"){
	   	gblPOWtransXN="Open Account";
	   	gblPOWchannel="Mobile Banking";
	}else if(kony.application.getCurrentForm().id == "frmOpenActDSAckCalendar"){
	   	gblPOWtransXN="Open Account";
	   	gblPOWchannel="Mobile Banking";
	}
	requestfromform = currentForm;
	postOnWall();
}

function setVisiblityBalanceTransferMB(widget1, widget2, widget3, bool) {
	widget1.setVisibility(bool);
	widget2.setVisibility(bool);
	widget3.setVisibility(bool);
}



function onClickSaveImagePaymenCal(){
   if(frmBillPaymentCompleteCalendar.lblHiddenQRFlowCheck.text == "QR"){
     gblPaymentCompleteShareFlow = "saveimage"
     generateQRforPaymentRefIdTransCal();
   }else{
     onClickSaveImagePaymenCalGenCode();
   }
  
}

function onClickMessengerPaymenCal(){
  if(frmBillPaymentCompleteCalendar.lblHiddenQRFlowCheck.text == "QR"){
     gblPaymentCompleteShareFlow = "messenger"
     generateQRforPaymentRefIdTransCal();
   }else{
     onClickMessengerPaymenCalGenCode();
   }
}

function onClickLinePaymenCal(){
   if(frmBillPaymentCompleteCalendar.lblHiddenQRFlowCheck.text == "QR"){
     gblPaymentCompleteShareFlow = "line"
     generateQRforPaymentRefIdTransCal();
   }else{
     onClickLinePaymenCalGenCode();
   }
}

function onClickOthersPaymenCal(){
   if(frmBillPaymentCompleteCalendar.lblHiddenQRFlowCheck.text == "QR"){
     gblPaymentCompleteShareFlow = "others"
     generateQRforPaymentRefIdTransCal();
   }else{
     onClickOthersPaymenCalGenCode();
   }
}


function onClickSaveImagePaymenCalGenCode(){
  if ((gblActivityIds == "27" || gblActivityIds == "28")) {
        //#ifdef iphone
        //#define preprocessdecision_onClick_80415245716329691_iphone
        //#endif
        //#ifdef preprocessdecision_onClick_80415245716329691_iphone
        screenShotCall.call(this, "screenshot");
        //#endif
        //#ifdef android
        //#define preprocessdecision_onClick_1240315245716323274_android
        //#endif
        //#ifdef preprocessdecision_onClick_1240315245716323274_android
        shareIntentmoreCallandroid.call(this, "screenshot", "BillPayment");
        //#endif
    } else {
        //#ifdef iphone
        //#define preprocessdecision_onClick_3776515245716328075_iphone
        //#endif
        //#ifdef preprocessdecision_onClick_3776515245716328075_iphone
        screenShotCall.call(this, "screenshot");
        //#endif
        //#ifdef android
        //#define preprocessdecision_onClick_2042215245716328779_android
        //#endif
        //#ifdef preprocessdecision_onClick_2042215245716328779_android
        shareIntentmoreCallandroid.call(this, "screenshot", "TopupPayment");
        //#endif
    }
}

function onClickMessengerPaymenCalGenCode(){
    if ((gblActivityIds == "27" || gblActivityIds == "28")) {
        shareIntentCall.call(this, "facebook", "BillPayment");
    } else {
        shareIntentCall.call(this, "facebook", "TOPUpPayment");
    }
}

function onClickLinePaymenCalGenCode(){
   if ((gblActivityIds == "27" || gblActivityIds == "28")) {
        shareIntentCall.call(this, "line", "BillPayment");
    } else {
        shareIntentCall.call(this, "line", "TOPUpPayment");
    }
}

function onClickOthersPaymenCalGenCode(){
    if ((gblActivityIds == "27" || gblActivityIds == "28" || gblActivityIds == "027" || gblActivityIds == "028")) {
        //#ifdef iphone
        //#define preprocessdecision_onClick_4534315245716324026_iphone
        //#endif
        //#ifdef preprocessdecision_onClick_4534315245716324026_iphone
        shareIntentmoreCall.call(this, "more", "BillPayment");
        //#endif
        //#ifdef android
        //#define preprocessdecision_onClick_1410915245716327117_android
        //#endif
        //#ifdef preprocessdecision_onClick_1410915245716327117_android
        shareIntentmoreCallandroid.call(this, "more", "BillPayment");
        //#endif
    } else {
        //#ifdef iphone
        //#define preprocessdecision_onClick_4170815245716327279_iphone
        //#endif
        //#ifdef preprocessdecision_onClick_4170815245716327279_iphone
        shareIntentmoreCall.call(this, "more", "TopupPayment");
        //#endif
        //#ifdef android
        //#define preprocessdecision_onClick_498915245716322637_android
        //#endif
        //#ifdef preprocessdecision_onClick_498915245716322637_android
        shareIntentmoreCallandroid.call(this, "more", "TopupPayment");
        //#endif
    }
}


function generateQRforPaymentRefIdTransCal() {
    if (gblShowQRPayslip == "ON") {
        //  var currForm = kony.application.getCurrentForm();
        //showLoadingScreen();
        var inputParams = {};
        frmBillPaymentCompleteCalendar.lblAccountNameQR.text = frmBillPaymentCompleteCalendar.lblAccountName.text
        frmBillPaymentCompleteCalendar.lblAccountNumQR.text = frmBillPaymentCompleteCalendar.lblAccountNum.text
        frmBillPaymentCompleteCalendar.lblAccUserNameQR.text = frmBillPaymentCompleteCalendar.lblAccUserName.text
        frmBillPaymentCompleteCalendar.lblPaymentDateValueQR.text = frmBillPaymentCompleteCalendar.lblPaymentDateValue.text
        frmBillPaymentCompleteCalendar.lblTxnNumValueQR.text = frmBillPaymentCompleteCalendar.lblTxnNumValue.text
        frmBillPaymentCompleteCalendar.lblBillerNameCompCodeQR.text = frmBillPaymentCompleteCalendar.lblBillerNameCompCode.text
        frmBillPaymentCompleteCalendar.lblFlexiParam1ValQR.text = frmBillPaymentCompleteCalendar.lblFlexiParam1Val.text
        frmBillPaymentCompleteCalendar.lblAmountValueQR.text = frmBillPaymentCompleteCalendar.lblAmountValue.text
        frmBillPaymentCompleteCalendar.lblPaymentFeeValueQR.text = "("+frmBillPaymentCompleteCalendar.lblPaymentFeeValue.text + ")"
        inputParams["qrCodeType"] = "qrBillPayment";
        if (frmTransfersAckCalendar.lblTransNPbAckRefDes.text != "") {
            inputParams["transactionRef"] = frmTransfersAckCalendar.lblTransNPbAckRefDes.text;
            invokeServiceSecureAsync("BOTQRCodeGenerator", inputParams, generateQRforPaymentRefIdTransCalCallback);
        }
    }
}

function generateQRforPaymentRefIdTransCalCallback(status, resulttable) {
    dismissLoadingScreen();
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            if (resulttable["qrDataSet"].length > 0) {
                //  var currForm = kony.application.getCurrentForm();
                frmBillPaymentCompleteCalendar.imgQR.base64 = resulttable["qrDataSet"][0]["base64QR"];
              if(gblPaymentCompleteShareFlow == "saveimage"){
                 onClickSaveImagePaymenCalGenCode();
              }  else if(gblPaymentCompleteShareFlow == "messenger"){
                onClickMessengerPaymenCalGenCode();
              } else if(gblPaymentCompleteShareFlow == "line"){
                onClickLinePaymenCalGenCode();
              } else if(gblPaymentCompleteShareFlow == "others"){
                onClickOthersPaymenCalGenCode();
              }
            }
        } else {
            dismissLoadingScreen();
        }
    } else {
        dismissLoadingScreen();
    }
}
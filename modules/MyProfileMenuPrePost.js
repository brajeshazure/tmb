function frmChangeMobNoTransLimitMBMenuPreshow() {
    if (gblCallPrePost) {
        frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.skin = txtNormalBG;
        frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.focusSkin = txtNormalBG;
        frmChangeMobNoTransLimitMB.txtChangeMobileNumber.skin = txtNormalBG;
        frmChangeMobNoTransLimitMB.txtChangeMobileNumber.focusSkin = txtNormalBG;
        PreShowChngeMobNuTransLimit();
        frmChangeMobNoTransLimitMB.scrollboxMain.scrollToEnd();
        isMenuRendered = false;
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges(frmChangeMobNoTransLimitMB);
    }

//     if(gblLoanNav == "popup"){
//      frmChangeMobNoTransLimitMB.hbxheaderRecipient.setVisibility(false);
//      frmChangeMobNoTransLimitMB.hbxArrow.setVisibility(false);
//      frmChangeMobNoTransLimitMB.hboxMobileClosebtn.setVisibility(true);
//      frmChangeMobNoTransLimitMB.hboxMobileTitleHeader.setVisibility(true);
//      frmChangeMobNoTransLimitMB.btnclosePopUpProfile.onClick = navLoanPersonalInfo;
//      frmChangeMobNoTransLimitMB.button474230331217991.onClick = navLoanPersonalInfo;
//   }else{
//      frmChangeMobNoTransLimitMB.hbxheaderRecipient.setVisibility(true);
//      frmChangeMobNoTransLimitMB.hbxArrow.setVisibility(true);
//      frmChangeMobNoTransLimitMB.hboxMobileClosebtn.setVisibility(false);
//      frmChangeMobNoTransLimitMB.hboxMobileTitleHeader.setVisibility(false);
//      frmChangeMobNoTransLimitMB.btnclosePopUpProfile.onClick = null;
//   }
 
}

// function navLoanPersonalInfo(){
//   frmLoanPersonalInfo.show();
//   gblLoanNav = "";
//   handleSaveBtnPersonalInfo();
// }

function frmChangeMobNoTransLimitMBMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}


function frmCheckContactInfoMenuPreshow() {
    if (gblCallPrePost) {
        frmCheckContactInfo.scrollboxMain.scrollToEnd();
        frmCheckContactInfo.lblHdrTxt.text = kony.i18n.getLocalizedString("Kyc_ttl");
        frmCheckContactInfo.lblContactinfo.text = kony.i18n.getLocalizedString("Kyc_lbl_contactInfo");
        frmCheckContactInfo.lblName.text = kony.i18n.getLocalizedString("Kyc_lbl_thname");
        frmCheckContactInfo.lblName2.text = kony.i18n.getLocalizedString("Kyc_lbl_enname");
        frmCheckContactInfo.lblRegisterAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_registeraddr");
        frmCheckContactInfo.lblContactAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_contactaddr");
        frmCheckContactInfo.lblMobileNum.text = kony.i18n.getLocalizedString("Kyc_lbl_mobileno");
        frmCheckContactInfo.lblEmailAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_email");
        frmCheckContactInfo.lblbottomlabel.text = kony.i18n.getLocalizedString("Kyc_lbl_contactdesc");
        frmCheckContactInfo.lblMobile.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
        frmCheckContactInfo.btnnext.text = kony.i18n.getLocalizedString("Next");
        frmCheckContactInfo.btnback.text = kony.i18n.getLocalizedString("Back");
    }
}


function frmCheckContactInfoMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}


function frmCMChgAccessPinMenuPreshow() {
    if (gblCallPrePost) {
        //#ifdef iphone
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            frmCMChgAccessPin.label4769997143084.containerWeight = 43
            frmCMChgAccessPin.label4769997143085.containerWeight = 36
            frmCMChgAccessPin.vbox47505874723766.containerWeight = 62
            frmCMChgAccessPin.txtAccessPwd.containerWeight = 50
            frmCMChgAccessPin.tbxCurAccPin.containerWeight = 50
        } else {
            frmCMChgAccessPin.label4769997143084.containerWeight = 51
            frmCMChgAccessPin.label4769997143085.containerWeight = 50
            frmCMChgAccessPin.txtAccessPwd.containerWeight = 37
            frmCMChgAccessPin.tbxCurAccPin.containerWeight = 45
        }
        //#endif

        //#ifdef android
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            frmCMChgAccessPin.label4769997143084.containerWeight = 38
            frmCMChgAccessPin.label4769997143085.containerWeight = 33
        } else {
            frmCMChgAccessPin.label4769997143084.containerWeight = 64
            frmCMChgAccessPin.label4769997143085.containerWeight = 53
        }
        //#endif

        isMenuShown = false;
        frmCMChgAccessPin.scrollboxMain.scrollToEnd();
        if (flowSpa) {} else {
            frmCMChgAccessPin.tbxCurAccPin.skin = txtNormalBG;
            frmCMChgAccessPin.txtAccessPwd.skin = txtNormalBG;
            frmCMChgAccessPin.tbxCurAccPin.text = "";
            frmCMChgAccessPin.txtAccessPwd.text = "";
        }
        if ((null != kony.os.deviceInfo().name) && (kony.string.equalsIgnoreCase("android", kony.os.deviceInfo().name))) {
            frmCMChgAccessPin.tbxCurAccPin.keyBoardStyle = constants.TEXTBOX_NUMERIC_PASSWORD;
            frmCMChgAccessPin.txtAccessPwd.keyBoardStyle = constants.TEXTBOX_NUMERIC_PASSWORD;
        }
        //#ifdef iphone
        if (flowSpa) {} else {
            frmCMChgAccessPin.tbxCurAccPin.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            frmCMChgAccessPin.txtAccessPwd.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
        }
        //#endif

        gblShowPwd = 0;
        gblflag = 0;
        gblShowPwd = gblShowPwdNo * 2;

        frmCMChgAccessPinPreShow();
        DisableFadingEdges(frmCMChgAccessPin);
    }
}

function frmCMChgAccessPinMenuPostshow() {
    if (gblCallPrePost) {
        frmCMChgAccessPin.scrollboxMain.scrollToEnd();

    }
    assignGlobalForMenuPostshow();
}

function frmCMChgPwdSPAMenuPreshow() {
    if (gblCallPrePost) {
        frmCMChgPwdSPA.tbxTranscCrntPwd.text = ""
        frmCMChgPwdSPA.txtTransPass.text = ""
        frmCMChgPwdSPA.txtConfirmPassword.text = ""
        frmCMChgPwdSPA.lblPasswdStrength.setVisibility(false);
        frmCMChgPwdSPAPreShow();
    }
}

function frmCMChgPwdSPAMenuPostshow() {
    if (gblCallPrePost) {
        frmCMChgPwdSPA.scrollboxMain.scrollToEnd();
        frmCMChgPwdSPA.label47428873338427.text = kony.i18n.getLocalizedString("CurrentPwdIB");
        frmCMChgPwdSPA.label47428873338438.text = kony.i18n.getLocalizedString("NewPasswordIB");
        frmCMChgPwdSPA.label47428873338465.text = kony.i18n.getLocalizedString("RetypeNewPwdIB");
    }
    assignGlobalForMenuPostshow();

}

function frmCMChgTransPwdMenuPreshow() {
    if (gblCallPrePost) {
        //ehFrmCMChgTransPwd_frmCMChgTransPwd_preshow(eventobject, 0);
		frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtNormalBG;
	    frmCMChgTransPwd.txtTransPass.skin = txtNormalBG;
	    frmCMChgTransPwd.txtTemp.skin = txtNormalBG;
	    frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtNormalBG;
	    frmCMChgTransPwd.txtTransPass.skin = txtNormalBG;
	    frmCMChgTransPwd.txtTemp.skin = txtNormalBG;
	    frmCMChgTransPwd.hbxPasswdStrength.text = ""
	    //frmCMChgTransPwd.hbxPasswdStrength.skin="No skin"
	    //frmCMChgTransPwd.txtTransPass.setVisibility(true);
	    //frmCMChgTransPwd.button50285458147.setFocus(true);
	    frmCMChgTransPwd.txtTemp.setVisibility(false);
	    frmCMChgTransPwd.tbxTranscCrntPwdTemp.setVisibility(false);
	    frmCMChgTransPwd.lblPasswdStrength.setVisibility(false);
	    transCount = 0
	    frmCMChgTransPwd.txtTransPass.setVisibility(true);
	    frmCMChgTransPwd.tbxTranscCrntPwd.setVisibility(true);
	    frmCMChgTransPwd.btnPwdOn.skin = "btnOnNormal";
	    frmCMChgTransPwd.btnPwdOff.skin = "btnOffNorm";
	    frmCMChgTransPwd.txtTransPass.text = "";
	    frmCMChgTransPwd.tbxTranscCrntPwd.text = "";
	    //frmCMChgTransPwd.hbxPasswdStrength.skin = "";
	    gblShowPwd = gblShowPwdNo * 2;
	    gblflag = 0;
	    frmCMChgTransPwd.scrollboxMain.scrollToEnd();
	    isMenuShown = false;
	    frmCMChgTransPwdPreShow();
	    DisableFadingEdges(frmCMChgTransPwd);
    }

}

function frmCMChgTransPwdMenuPostshow() {
    if (gblCallPrePost) {
        //ehFrmCMChgTransPwd_frmCMChgTransPwd_postshow(eventobject, 0)
    }
    assignGlobalForMenuPostshow();

}

function frmConfirmationarchivedMenuPreshow() {
    if (gblCallPrePost) {}
}

function frmConfirmationarchivedMenuPostshow() {
    if (gblCallPrePost) {}
    assignGlobalForMenuPostshow();
}

function frmeditContactInfoMenuPreshow() {
    if (gblCallPrePost) {
        frmeditContactInfo.txtemailvalue.skin = txtNormalBG;
        frmeditContactInfo.txtemailvalue.focusSkin = txtNormalBG;
        frmeditContactInfo.txtAddress1.skin = txtNormalBG;
        frmeditContactInfo.txtAddress1.focusSkin = txtNormalBG;
        frmeditContactInfo.txtAddress2.skin = txtNormalBG;
        frmeditContactInfo.txtAddress2.focusSkin = txtNormalBG;
		 
        mptemp = "preshow";
	    confirmEdit = false;
	    selectState = "";
	    selectDist = "";
	    selectSubDist = "";
	    selectZip = "";
	    resulttableState = [];
	    resulttableDist = [];
	    resulttableSubDist = [];
	    resulttableStateZip = [];
	    changeState = false;
	    changedist = false;
	    changeSubDist = false;
	    changeZip = false;
        
        frmeditContactInfo.txtemailvalue.text = gblEmailAddr;
        frmeditContactInfo.txtAddress1.text = gblAddress1Value;
		frmeditContactInfo.txtAddress2.text = gblAddress2Value;
		if(kony.i18n.getCurrentLocale() == "th_TH") {
			frmeditContactInfo.lblProvince.text = gblStateValue;
			frmeditContactInfo.lbldistrict.text = gbldistrictValue;
			frmeditContactInfo.lblsubdistrict.text = gblsubdistrictValue;
		}else{
			frmeditContactInfo.lblProvince.text = gblStateEng;
			frmeditContactInfo.lbldistrict.text = gblDistEng;
			frmeditContactInfo.lblsubdistrict.text = gblSubDistEng;
		}
		frmeditContactInfo.lblzipcode.text = gblzipcodeValue;
		
        frmeditConactInfoPreShow.call(this);
        frmeditContactInfo.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmeditContactInfo);
    }
}

function frmeditContactInfoMenuPostshow() {
    if (gblCallPrePost) {
        // Write the post code here
    }
    assignGlobalForMenuPostshow();
}

function frmeditMyProfileMenuPreshow() {
  try{
  if(gblLoanNav == "popup"){
    frmeditMyProfile.hbox475868836198148.setVisibility(false);
    frmeditMyProfile.hbox475124774143.setVisibility(false);
    frmeditMyProfile.hbxArrow.setVisibility(false);
    frmeditMyProfile.hbox475868836197589.setVisibility(false);
    frmeditMyProfile.hbox475868836198148.setVisibility(false);
    frmeditMyProfile.hbxchangepic.setVisibility(false);
    frmeditMyProfile.hboxClosebtn.setVisibility(true);
    frmeditMyProfile.hboxTitleHeader.setVisibility(true);
    frmeditMyProfile.btnclosePopUpProfile.onClick = closePopUpEditMyProfile;
    frmeditMyProfile.button474230331217991.onClick = closePopUpEditMyProfile;
  }else{
    frmeditMyProfile.hbox475868836198148.setVisibility(true);
    frmeditMyProfile.hbox475124774143.setVisibility(true);
    frmeditMyProfile.hbxArrow.setVisibility(true);
    frmeditMyProfile.hbox475868836197589.setVisibility(true);
    frmeditMyProfile.hbox475868836198148.setVisibility(true);
    frmeditMyProfile.hbxchangepic.setVisibility(true);
    frmeditMyProfile.hboxClosebtn.setVisibility(false);
    frmeditMyProfile.hboxTitleHeader.setVisibility(false);
    frmeditMyProfile.btnclosePopUpProfile.onClick = null;
  }
  
    if (gblCallPrePost) {
        frmeditMyProfile.txtdevicenamevalue.skin = txtNormalBG;
        frmeditMyProfile.txtdevicenamevalue.focusSkin = txtNormalBG;
        frmeditMyProfile.txtemailvalue.skin = txtNormalBG;
        frmeditMyProfile.txtemailvalue.focusSkin = txtNormalBG;
        frmeditMyProfile.txtAddress1.skin = txtNormalBG;
        frmeditMyProfile.txtAddress1.focusSkin = txtNormalBG;
        frmeditMyProfile.txtAddress2.skin = txtNormalBG;
        frmeditMyProfile.txtAddress2.focusSkin = txtNormalBG;
        frmeditMyProfilePreShow.call(this);
        frmeditMyProfile.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmeditMyProfile);
    }
  } catch(e){
    alert("loanEditMyProfile "+e);
  }
}

function closePopUpEditMyProfile() {
    frmLoanPersonalInfo.show();
    gblLoanNav = "";
  }

function frmeditMyProfileMenuPostshow() {
    if (gblCallPrePost) {
        // Write the post code here
    }
    assignGlobalForMenuPostshow();
}

function frmFBLoginMenuPreshow() {
    if (gblCallPrePost) {

    }
}

function frmFBLoginMenuPostshow() {
    if (gblCallPrePost) {
        assignHandleRequest.call(this);
    }
    assignGlobalForMenuPostshow();
}


function frmFBProfileLoginMenuPreshow() {
    if (gblCallPrePost) {

    }
}

function frmFBProfileLoginMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}

function frmMyProfileMenuPreshow() {
    if (gblCallPrePost) {
        frmMyProfilePreShow.call(this);
        frmViewMyProfilePreShow.call(this);
    }
}

function changeMobileNum(){
  
  TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
  gblMobNoTransLimitFlag=true;
  
  editbuttonflag="number";
 
  getIBMBEditProfileStatus();
}

function frmMyProfileMenuPostshow() {
    if (gblCallPrePost) {


    }
    assignGlobalForMenuPostshow();
}


function frmMyProfileReqHistoryMenuPreshow() {
    if (gblCallPrePost) {
        frmMyProfileReqHistory.scrollboxMain.scrollToEnd()
        DisableFadingEdges.call(this, frmMyProfileReqHistory);

    }
}

function frmMyProfileReqHistoryMenuPostshow() {
    if (gblCallPrePost) {


    }
    assignGlobalForMenuPostshow();
}


function frmRequestHistoryMenuPreshow() {
    if (gblCallPrePost) {
        frmRequestHistory.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmRequestHistory);

    }
}

function frmRequestHistoryMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}
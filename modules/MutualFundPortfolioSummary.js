//Type your code here
function frmMFSummaryMBInitiatization(){
  frmMFSummary.preShow = frmMFSummaryLandingPreShow; 
  frmMFSummary.postShow = frmMFSummaryLandingPostShow; 
  frmMFSummary.btnMainMenu.onClick = handleMenuBtn;
  frmMFSummary.FlexMenuMore.onClick = onClickMoreMBMenu;
  frmMFSummary.FlexMenuPurchase.onClick = onClickPurchaseMBMenu;
  frmMFSummary.FlexMenuRedeem.onClick = onClickRedeemMBMenu;
  frmMFSummary.FlexMenuSwitch.onClick = onClickSwitchMBMenu; 
  frmMFSummary.FlexSelectUnitHolderNo.onClick = frmMFSummaryonSelectUnitHolderNo;
  frmMFSummary.btnMFFundPortflio.onClick = onClickfundPortfolio;
  frmMFSummary.btnMFAutoSmartPortfolio.onClick = onClickAutoSmartPortfolio;
  frmMFSummary.btnMFOverview.onClick = onClickOverviewMF;
  frmMFSummary.btnBackMFSummary.onClick = showAccountSummaryFromMenu;
  
  frmMFSummary.FlexMenuPortfolio.onClick = doNothing;
  frmMFSummary.flxNoASPortfolioPopup.onClick = doNothing;
  frmMFSummary.flxFreezePeriodPopup.onClick = doNothing;
  
  frmMFSummary.ButtonClose.onClick = closeAutosmartPopup;
  frmMFSummary.btnGotit.onClick = closeAutosmartPopup;
  frmMFSummary.btnFreezeCLose.onClick = closeFreezeRebalancePopup;
  frmMFSummary.ButtonCloseFreeze.onClick = closeFreezeRebalancePopup;
  
  frmMFSummary.flxBody.showFadingEdges = false;
  frmMFSummary.CopyflxScrlBody0fa3951d96a1c43.showFadingEdges = false;
}

function closeAutosmartPopup(){
//  gblMFPortfolioType = "PT";
  gblMFPortfolioType = "OW";
  gblUnitHolderNoSelected = "";
  frmMFSummary.btnMFFundPortflio.skin = "btnWhiteBGMF"; //"btnBlueBGMF";
  frmMFSummary.btnMFAutoSmartPortfolio.skin = "btnWhiteBGMF";
  frmMFSummary.btnMFOverview.skin = "btnBlueBGMF";
  frmMFSummary.flxNoASPortfolioPopup.setVisibility(false);
  frmMFSummary.flxFreezePeriodPopup.setVisibility(false);
  frmMFSummary.show();
}

function closeFreezeRebalancePopup(){
  
  gblMFPortfolioType = "OW";
  gblUnitHolderNoSelected = "";
  frmMFSummary.btnMFFundPortflio.skin = "btnWhiteBGMF";
  frmMFSummary.btnMFAutoSmartPortfolio.skin = "btnWhiteBGMF";
  frmMFSummary.btnMFOverview.skin = "btnBlueBGMF";
  frmMFSummary.flxNoASPortfolioPopup.setVisibility(false);
  frmMFSummary.flxFreezePeriodPopup.setVisibility(false);  
  frmMFSummary.show();
}

function onClickfundPortfolio(){
  gblMFPortfolioType = "PT";
  gblUnitHolderNoSelected = "";
  frmMFSummary.btnMFOverview.skin = "btnWhiteBGMF";
  frmMFSummary.btnMFFundPortflio.skin = "btnBlueBGMF";
  frmMFSummary.btnMFAutoSmartPortfolio.skin = "btnWhiteBGMF";
  dismissLoadingScreen();
  frmMFSummary.show();
}


function onClickAutoSmartPortfolio(){
  gblMFPortfolioType = "AP";
  gblUnitHolderNoSelected = "";
  frmMFSummary.btnMFOverview.skin = "btnWhiteBGMF";
  frmMFSummary.btnMFFundPortflio.skin = "btnWhiteBGMF";
  frmMFSummary.btnMFAutoSmartPortfolio.skin = "btnBlueBGMF";
  dismissLoadingScreen();
  frmMFSummary.show();
}

function onClickOverviewMF(){
  gblMFPortfolioType = "OW";
  gblUnitHolderNoSelected = "";
  frmMFSummary.btnMFOverview.skin = "btnBlueBGMF";
  frmMFSummary.btnMFFundPortflio.skin = "btnWhiteBGMF";
  frmMFSummary.btnMFAutoSmartPortfolio.skin = "btnWhiteBGMF";
  dismissLoadingScreen();
  frmMFSummary.show();
  
}

//================== change by Mayank======================
function setFilterUnitHolderNoCombobox() {
  var unitHolderNoData = [];
  if(gblMFPortfolioType == "PT"){
    unitHolderNoData.push(["0", kony.i18n.getLocalizedString("MU_SUM_Viewall")]);
  }
  if (gblChannel == "IB") {
    gblUnitHolderNoList.sort();
  }
  for (var i = 0; i < gblUnitHolderNoList.length; i++) {
    var count = 1 + i;
    unitHolderNoData.push([count.toString(),formatUnitHolderNumer(gblUnitHolderNoList[i])]);
  }
  if (gblChannel == "IB") {
    frmIBMutualFundsPortfolio.cmbFilterby.masterData = unitHolderNoData;
    frmIBMutualFundsPortfolio.cmbFilterby.selectedKey = "0";
    setFilterUnitHolderNoComboboxSelectionChanged();
  }
}

function setFilterUnitHolderNoComboboxSelectionChanged() {
  var inputParam = {};
  var unitholder = "";
  var mfTotalAmount;
  unitholder = frmIBMutualFundsPortfolio.cmbFilterby.selectedKeyValue[1];
  if (unitholder != kony.i18n.getLocalizedString("MU_SUM_Viewall")) {
    gblUnitHolderNoSelected = unitholder.replace(/-/gi, "");
  } else {
    gblUnitHolderNoSelected = "";
  }
  mfAmount = calculateTotalInvestmentValue(gblMFSummaryData, gblUnitHolderNoSelected);
  calculateFundClassPercentageIB(gblMFSummaryData, mfAmount["totalInvestmentValue"], gblUnitHolderNoSelected);
  if (mfAmount["totalInvestmentValue"] > 0.00) {
    frmIBMutualFundsPortfolio.HboxValues.setVisibility(true);
    frmIBMutualFundsPortfolio.hbxChart.setVisibility(true);
    deleteTagforChartIBMutualFund();
  } else {
    frmIBMutualFundsPortfolio.HboxValues.setVisibility(false);
    frmIBMutualFundsPortfolio.hbxChart.setVisibility(false);
  }
  frmIBMutualFundsPortfolio.LabelInvestmentVal.text = numberWithCommas(numberFormat(mfAmount["totalInvestmentValue"], 2));
  if (mfAmount["totalUnrealizeValue"] >= 0.00) {
    frmIBMutualFundsPortfolio.LabelUnrealized.skin = lblIB24pxRegGreen;
  } else {
    frmIBMutualFundsPortfolio.LabelUnrealized.skin = lblIB24pxRegRed;
  }
  frmIBMutualFundsPortfolio.LabelUnrealized.text = numberWithCommas(numberFormat(mfAmount["totalUnrealizeValue"], 2));
  var segmentData = populateDataMutualFundsSummary(gblMFSummaryData);
  frmIBMutualFundsPortfolio.segAccountDetails.removeAll();
  frmIBMutualFundsPortfolio.segAccountDetails.setData(segmentData);
  //popupIBSelectUnitHolderNoMF.dismiss();
  frmIBMutualFundsPortfolio.hbxAccntDetailsHeader.setVisibility(false);
  frmIBMutualFundsPortfolio.hbxAccntDetails.setVisibility(false);
  frmIBMutualFundsPortfolio.hbxTMBImage.setVisibility(true);

}
//=======================End===============================
function setFilterUnitHolderNoPortfolioChart(){
  try{
    var unitHolderNoData = [];
    if(gblMFPortfolioType == "PT"){
      var tempAllRecord = { 
      "LabelUnitHolderNo": kony.i18n.getLocalizedString("MU_SUM_Viewall")
      };
      unitHolderNoData.push(tempAllRecord);
    }  
    for(var i=0 ; i<gblUnitHolderNoList.length ; i++){

      var tempRecord = { 
        "LabelUnitHolderNo": formatUnitHolderNumer(gblUnitHolderNoList[i])
      };
      unitHolderNoData.push(tempRecord);
    }
    if(gblChannel == "MB"){
      frmMFFilterUnitHolderNoMB.SegmentList.removeAll();
      frmMFFilterUnitHolderNoMB.SegmentList.setData(unitHolderNoData);
    }else if(gblChannel == "IB"){
      popupIBSelectUnitHolderNoMF.SegmentList.removeAll();
      popupIBSelectUnitHolderNoMF.SegmentList.setData(unitHolderNoData);
    }
  } catch(e) {
    kony.print("Error :::" + e);
  }
}

function frmMFSummaryonSelectUnitHolderNo(){
  try{
    setFilterUnitHolderNoPortfolioChart();
    frmMFFilterUnitHolderNoMB.show();
    frmMFSummary.flexChart.removeAll(); //MKI,MIB-9014
    
    //frmMFSummary.destroy();
  } catch(e) {
    kony.print("Error :::" + e);
  }

}
function onClickPortfolioMenu(){
  gblUnitHolderNoSelected = "";
  frmMFSummary.show();
}
function onClickPurchaseMBMenu(){
  if(gblUserLockStatusMB == "03"){
    showTranPwdLockedPopup();
  }else{
    if(gblMFSummaryData["MF_BusinessHrsFlag"]!=null && gblMFSummaryData["MF_BusinessHrsFlag"]=="false"){
       kony.print("1512 Inside If Condition, Other flow");
       var endTime = gblMFSummaryData["MF_EndTime"];
       var startTime = gblMFSummaryData["MF_StartTime"]; 
       var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
       messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
       messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
       showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
       } else {
        kony.print("1512 Inside else Condition, Normal flow");
        gblOrderFlow = true;
        gblMFOrder = {};
        gblFundRulesData = {};
        gblMFEventFlag = MF_EVENT_PURCHASE;
        gblMFOrder["orderType"] = MF_ORD_TYPE_PURCHASE; 
        gblMFOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
        //MBcallGetFundList();
        getMFSuitibilityReviewDetails();
  }
 }
}
function onClickRedeemMBMenu(){
  if(gblUserLockStatusMB == "03"){
    showTranPwdLockedPopup();
  }else{
    if(gblMFSummaryData["MF_BusinessHrsFlag"]!=null && gblMFSummaryData["MF_BusinessHrsFlag"]=="false"){
       kony.print("1512 Inside If Condition, Other flow");
       var endTime = gblMFSummaryData["MF_EndTime"];
       var startTime = gblMFSummaryData["MF_StartTime"]; 
       var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
       messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
       messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
       showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
       } else {
        kony.print("1512 Inside else Condition, Normal flow");
        gblMFOrder = {};
        gblFundRulesData = {};
        gblMFEventFlag = MF_EVENT_REDEEM;
        gblMFOrder["orderType"] = MF_ORD_TYPE_REDEEM; 
        gblMFOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
        MBcallGetFundList();
  }
  }
}
function onClickMoreMBMenu(){
  frmMFMoreMenuMB.show();
}

function frmMFMoreMenuMBInitiatization(){
  frmMFMoreMenuMB.btnBack.onClick = frmMFMoreMenuMBonClickBack;
  frmMFMoreMenuMB.FlexSuitabilityScore.onClick = onClickSuitabilityScoreMBMenu;
  frmMFMoreMenuMB.FlexMenuPortfolio.onClick = onClickPortfolioMenu;
  frmMFMoreMenuMB.FlexMenuPurchase.onClick = onClickPurchaseMBMenu;
  frmMFMoreMenuMB.FlexMenuRedeem.onClick = onClickRedeemMBMenu;
  frmMFMoreMenuMB.FlexMenuMore.onClick = doNothing;
  frmMFMoreMenuMB.preShow= preshowfrmMFMore;

}

function preshowfrmMFMore(){
  frmMFMoreMenuMB.lblHeader.text=kony.i18n.getLocalizedString("MU_Ttl_More");
  frmMFMoreMenuMB.LabelSuitabilityScore.text=kony.i18n.getLocalizedString("MU_SB_Suitability");
  frmMFMoreMenuMB.LabelPort.text=kony.i18n.getLocalizedString("MU_ACC_InvestVal");
  frmMFMoreMenuMB.LabelPurchase.text=kony.i18n.getLocalizedString("key_MF_Purchase");
  frmMFMoreMenuMB.LabelRedeem.text=kony.i18n.getLocalizedString("key_MF_Redeem");
  frmMFMoreMenuMB.LabelMore.text=kony.i18n.getLocalizedString("MU_More");
  var gblDeviceInfo = kony.os.deviceInfo();
  var deviceModel = gblDeviceInfo["model"];
  if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
    frmMFMoreMenuMB.LabelPort.skin = "lblWhiteNormal";
    frmMFMoreMenuMB.LabelPurchase.skin = "lblWhiteNormal";
    frmMFMoreMenuMB.LabelRedeem.skin = "lblWhiteNormal";
    frmMFMoreMenuMB.LabelMore.skin = "lblWhiteNormal";
  }
  else
  {
    frmMFMoreMenuMB.LabelPort.skin = "lbl20pxWhiteNormal";
    frmMFMoreMenuMB.LabelPurchase.skin = "lbl20pxWhiteNormal";
    frmMFMoreMenuMB.LabelRedeem.skin = "lbl20pxWhiteNormal";
    frmMFMoreMenuMB.LabelMore.skin = "lbl20pxWhiteNormal";
  }

}
function onClickSuitabilityScoreMBMenu(){
  gblOrderFlow = false;
  getMFSuitibilityReviewDetails();
}

function frmMFMoreMenuMBonClickBack(){
  gblUnitHolderNoSelected = "";
  frmMFSummary.show();
}

function onClickRedeemIBMenu(){
  if(getCRMLockStatus()){
    curr_form = kony.application.getCurrentForm().id;
    dismissLoadingScreenPopup();
    popIBBPOTPLocked.show();
  }
  else{
    showLoadingScreenPopup();
    gblMFOrder = {};
    gblFundRulesData = {};
    gblMFEventFlag = MF_EVENT_REDEEM;
    gblMFOrder["orderType"] = MF_ORD_TYPE_REDEEM;
    gblMFOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
    IBcallGetFundList();
    //testGetFundListIB();
  } 
}

function onClickPurchaseIBMenu(){
  if(getCRMLockStatus()){
    curr_form = kony.application.getCurrentForm().id;
    dismissLoadingScreenPopup();
    popIBBPOTPLocked.show();
  }
  else{
    gblOrderFlow = true;
    showLoadingScreenPopup();
    gblMFOrder = {};
    gblFundRulesData = {};
    gblMFEventFlag = MF_EVENT_PURCHASE;
    gblMFOrder["orderType"] = MF_ORD_TYPE_PURCHASE;
    gblMFOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
    //IBcallGetFundList();
    //testGetFundListIB();
    getMFSuitibilityReviewDetailsIB();
  }
}

function frmIBMutualFundsPortfolioOnSelectUnitHolderNo(){
  setFilterUnitHolderNoPortfolioChart();
  popupIBSelectUnitHolderNoMF.show();
}
function calculateFundClassPercentageIB(summaryDataTotal, mfTotalAmount, unitHolderNoSelected) {
    var summaryData = summaryDataTotal["FundClassDS"];
    /*
      100 Local Fixed Income กองทุนตราสารหนี้ไทย
      200 Foreign Fixed Income กองทุนตราสารหนี้ต่างประเทศ
      300 Balanced กองทุนผสม
      400 Local Equity กองทุนหุ้นไทย
      500 Foreign Equity กองทุนหุ้นต่างประเทศ
      600 Others อื่นๆ
      700 Foreign Balanced กองทุนผสมต่างประเทศ
      */
    gblFundClassPercentageData[100] = 0;
    gblFundClassPercentageData[200] = 0;
    gblFundClassPercentageData[300] = 0;
    gblFundClassPercentageData[400] = 0;
    gblFundClassPercentageData[500] = 0;
    gblFundClassPercentageData[600] = 0;
    gblFundClassPercentageData[700] = 0;
  	gblFundClassPercentageData[800] = 0; //MKI, MIB-11485
    gblFundClassPercentageData[900] = 0; //MKI, MIB-11485
    var totalFundClassValue = 0;
    for (var i = 0; i < summaryData.length; i++) {
        var fundHouseDS = summaryData[i].FundHouseDS;
        var fundHouseLength = fundHouseDS.length;
        var fundValue = 0;
        for (var j = 0; j < fundHouseLength; j++) {
            var fundCodeDS = fundHouseDS[j].FundCodeDS;
            var fundCodeDSLength = fundCodeDS.length;
            var fundHouseCode = fundHouseDS[j]["FundHouseCode"]
            for (var k = 0; k < fundCodeDSLength; k++) {
                if (unitHolderNoSelected == "" || (unitHolderNoSelected != "" && unitHolderNoSelected == fundCodeDS[k]["UnitHolderNo"])) {
                    fundValue = fundValue + parseFloat(fundCodeDS[k]["MarketValue"]);
                }
            } //for loop end k
        } //for loop end j
        var fundClassValue = (100 * fundValue) / mfTotalAmount;    
		fundClassValue =fundClassValue.toFixed(2); 
        if (totalFundClassValue + fundClassValue > 100) {
            if (fundClassValue > 0) {
                fundClassValue = 100 - totalFundClassValue;
            }
        }
        totalFundClassValue = totalFundClassValue + fundClassValue;
        var fundClassCode = summaryData[i]["FundClassCode"];
        //alert(fundClassCode + " : " + fundValue + " - " + fundClassValue + " -> " + totalFundClassValue + "%");
        gblFundClassPercentageData[fundClassCode] = fundClassValue;
    } //for loop end i
}
function popupIBSelectUnitHolderNoMFOnSeleteData(){
  var inputParam = {};
  var selectedIndex = popupIBSelectUnitHolderNoMF.SegmentList.selectedIndex[1];
  var fundDataObject = popupIBSelectUnitHolderNoMF.SegmentList.data[selectedIndex];
  var unitholder = "";
  var mfTotalAmount;

  unitholder = fundDataObject.LabelUnitHolderNo;
  if(unitholder != kony.i18n.getLocalizedString("MU_SUM_Viewall")) {
    gblUnitHolderNoSelected = unitholder.replace(/-/gi, "");
    frmIBMutualFundsPortfolio.LabelUnitHolderNo.text = formatUnitHolderNumer(gblUnitHolderNoSelected);
  } else {
    gblUnitHolderNoSelected = "";
    frmIBMutualFundsPortfolio.LabelUnitHolderNo.text = kony.i18n.getLocalizedString("MU_SUM_Viewall");
  }

  mfAmount = calculateTotalInvestmentValue(gblMFSummaryData, gblUnitHolderNoSelected);
  calculateFundClassPercentageIB(gblMFSummaryData, mfAmount["totalInvestmentValue"], gblUnitHolderNoSelected);
  if(mfAmount["totalInvestmentValue"] > 0.00){
    frmIBMutualFundsPortfolio.HboxValues.setVisibility(true);
    frmIBMutualFundsPortfolio.hbxChart.setVisibility(true);
    //addgenerateChart();
  } else {
    frmIBMutualFundsPortfolio.HboxValues.setVisibility(false);
    frmIBMutualFundsPortfolio.hbxChart.setVisibility(false);
  }

  frmIBMutualFundsPortfolio.LabelInvestmentVal.text = numberWithCommas(numberFormat(mfAmount["totalInvestmentValue"], 2));
  if(mfAmount["totalUnrealizeValue"] >= 0.00){
    frmIBMutualFundsPortfolio.LabelUnrealized.skin = lblIB24pxRegGreen;
  } else {
    frmIBMutualFundsPortfolio.LabelUnrealized.skin = lblIB24pxRegRed;
  }
  frmIBMutualFundsPortfolio.LabelUnrealized.text = numberWithCommas(numberFormat(mfAmount["totalUnrealizeValue"], 2));

  var segmentData= populateDataMutualFundsSummary(gblMFSummaryData);
  frmIBMutualFundsPortfolio.segAccountDetails.removeAll();
  frmIBMutualFundsPortfolio.segAccountDetails.setData(segmentData);
  popupIBSelectUnitHolderNoMF.dismiss();
}



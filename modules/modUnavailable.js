var unavailable_types = {
  UNKNOWN: "MB_UNKNOWN",
  NO_INTERNET: "MB_NO_INTERNET",
  MAINTENANCE: "MB_MAINTENANCE",
  SERVER_DOWN: "MB_SERVER_DOWN"
};

gblUnavailableType = null;
gblUnavailableJsonData = null;
gblCalledi18nFlag = false;
isTimeoutCheckServerDown = false;
isTimeoutCheckMaintenance = false;
doneCheckMBUnavailable = false;
gblDisplayCurrentForm = false;
gblMBHttpRequest = null;
gblMaxIntegrationFailure = 1;
gblIntegrationFailureCount = 0;

function checkMBUnavailable(showFlag) {
  kony.print("#### checkMBUnavailable showFlag "+showFlag);
  showLoadingScreen();
  resetCheckMBUnavailable();

  gblUnavailableType = unavailable_types.UNKNOWN;
  doneCheckMBUnavailable = false;

  if (!isMBNetworkAvailable()) {
    doneCheckMBUnavailable = true;
    gblUnavailableType = unavailable_types.NO_INTERNET;
    showFlag = true;
  } else {
    checkMBServiceDown();
  }

  if (isNotBlank(showFlag) && showFlag) {
    if (isNotBlank(frmMBUnavailable)) {
      kony.print("#### checkMBUnavailable frmMBUnavailable.show()");
      frmMBUnavailable.show();  
    }else{
      kony.print("#### checkMBUnavailable frmMBUnavailable ##### NULL");
    }
  }
}

function isMBNetworkAvailable(){
  var isNetworkAvailable = kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
  kony.print("#################### first time isNetworkAvailable = "+isNetworkAvailable);
  //For Model Samsung Note, On Power Saving Mode need to wait for network connection
  //We can set timeout for wait (default is 3 seconds)
  if (isNetworkAvailable === false) {
    try{
      kony.print("#################### gblTimeoutNetworkInSecond = "+gblTimeoutNetworkInSecond);
      var startDate = new Date();
      var endDate   = null;
      var usageTime = 0;
      while(!isNetworkAvailable && usageTime < gblTimeoutNetworkInSecond) {
        isNetworkAvailable = kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
        kony.print("#################### isNetworkAvailable in loop ="+isNetworkAvailable);
        if (isNetworkAvailable === true) {
          break;
        }
        endDate   = new Date();
        usageTime = Math.floor((endDate.getTime() - startDate.getTime()) / (1000));
      }
      kony.print("#################### isNetworkAvailable in loop usageTime ="+usageTime+" seconds");
    }catch(e){
      kony.print("#################### isNetworkAvailable error in loop"+e.stack);
    }
  }
  kony.print("#################### after attempt isNetworkAvailable = "+isNetworkAvailable);
  return isNetworkAvailable;
}

function recheckMBUnavailable() {
  kony.print("#### recheckMBUnavailable "+gblUnavailableFlag);
  try {
    gblDisplayCurrentForm = false;
    kony.print("#### gblIsBackgroundEvent "+gblIsBackgroundEvent);
    kony.print("#### doneCheckMBUnavailable "+doneCheckMBUnavailable);
    if (gblUnavailableFlag && !gblIsBackgroundEvent && isInMBCheckingForm() && doneCheckMBUnavailable) {
      kony.print("#### recheckMBUnavailable again");
      if ("frmMBUnavailable" == kony.application.getCurrentForm().id) {
        //showing new form
        gblDisplayCurrentForm = false;
        checkMBUnavailable(true);
      }else{
        //stay on old form
        gblDisplayCurrentForm = true;
        checkMBUnavailable(false);
      }
    }
  }catch(e){
    kony.print("Error recheckMBUnavailable="+e.stack);
  }
}

function checkMBServiceDown() {
  if (!doneCheckMBUnavailable) {

    cancelCheckMBServiceDownID();

    kony.print("#### checkMBServiceDown gblTimeoutServerDownInSecond = "+gblTimeoutServerDownInSecond);
    kony.timer.schedule("checkMBServiceDownID", timeoutCheckMBServiceDown, gblTimeoutServerDownInSecond, false);

    var inputparams = {};

    //checking service down?
    invokeServiceSecureAsync("checkConnection", inputparams, function(checkConnectionStatus, checkConnectionResulttable) {
      if (!doneCheckMBUnavailable) {
        if (checkConnectionStatus === 400 && checkConnectionResulttable.opstatus === 0) {
          doneCheckMBUnavailable = true;
          resetCheckMBUnavailable();
          gblUnavailableType = null;
          //kony.print("#### checkConnection resulttable > " + JSON.stringify(checkConnectionResulttable));
          showLoadingScreen();
          if (gblCalledi18nFlag === false) {
            loadResourceBundle(function(loadResourceBundleStatus, loadResourceBundleResulttable) {
              if (!currentFormIsFrmUVApprovalMB() && gblDisplayCurrentForm === false) {
                kony.print("#### checkMBServiceDown getMBStartupForm().show() #1");
                getMBStartupForm().show();
              }
              callbackgetphrasesMB(loadResourceBundleStatus, loadResourceBundleResulttable);
              gblCalledi18nFlag = true;
              dismissLoadingScreen();
            });
          } else {
            if (!currentFormIsFrmUVApprovalMB() && gblDisplayCurrentForm === false) {
              kony.print("#### checkMBServiceDown getMBStartupForm().show() #2");
              getMBStartupForm().show();
              checkForDisplayFingerPrintOrTouchID();
            }
            dismissLoadingScreen();
          }
        } else {
          checkMBMaintenance();
        }
      }
    });
  }
}

function checkMBMaintenance() {
  if (!doneCheckMBUnavailable) {

    cancelCheckMBServiceDownID();
    cancelCheckMBMaintenanceID();

    kony.print("#### checkMBMaintenance gblTimeoutMaintenanceInSecond = "+gblTimeoutMaintenanceInSecond);
    kony.timer.schedule("checkMBMaintenanceID", timeoutCheckMBMaintenance, gblTimeoutMaintenanceInSecond, false);

    var maintenance_url = "https://" + appConfig.serverIp + gblMaintenanceURI;
    var gblMBHttpRequest = new kony.net.HttpRequest();
    gblMBHttpRequest.onReadyStateChange = checkMBMaintenanceStateChange;
    gblMBHttpRequest.timeout = parseInt(gblTimeoutMaintenanceInSecond * 1000);
    kony.print("#### checkMBMaintenance URL =>" + maintenance_url);
    gblMBHttpRequest.open(constants.HTTP_METHOD_POST, maintenance_url, true);
    gblMBHttpRequest.setRequestHeader("Content-Type", "application/json");
    var postdata = {};
    gblMBHttpRequest.send(postdata);
  }  
}

function checkMBMaintenanceStateChange() {
  if (!doneCheckMBUnavailable) {	
    if (this.readyState == constants.HTTP_READY_STATE_UNSENT) {
      kony.print("#### HTTP_READY_STATE_UNSENT " + new Date());
    } else if (this.readyState == constants.HTTP_READY_STATE_OPENED) {
      kony.print("#### HTTP_READY_STATE_OPENED " + new Date());
    } else if (this.readyState == constants.HTTP_READY_STATE_HEADERS_RECEIVED) {
      kony.print("#### HTTP_READY_STATE_HEADERS_RECEIVED " + new Date());
    } else if (this.readyState == constants.HTTP_READY_STATE_LOADING) {
      kony.print("#### HTTP_READY_STATE_LOADING " + new Date());
    } else if (this.readyState == constants.HTTP_READY_STATE_DONE) {
      kony.print("#### HTTP_READY_STATE_DONE " + new Date());
      if (this.status == 200 && this.response) {
        if (typeof(this.response) === 'string') {
          try {
            gblUnavailableJsonData = JSON.parse(this.response);
          }catch(e){
            kony.print("#### Error JSON.parse " + e.stack);
            gblUnavailableJsonData = null;
          }
        }else{
          gblUnavailableJsonData = this.response;
        }
        kony.print("#### jsonResponse = " + JSON.stringify(gblUnavailableJsonData));
      } else {
        kony.print("#### request.status = " + this.status);
        gblUnavailableJsonData = null;
      }
      showMBUnavailable();
    }
  }
}

function showMBUnavailable() {
  kony.print("#### showMBUnavailable <======");
  doneCheckMBUnavailable = true;
  cancelCheckMBServiceDownID();
  cancelCheckMBMaintenanceID();
  try{
    TMBCallPopup.dismiss();
  }catch(e){
  }
  if (isTimeoutCheckServerDown && isTimeoutCheckMaintenance) {
    gblUnavailableType = unavailable_types.NO_INTERNET;
  } else if (isNotBlank(gblUnavailableJsonData) && "Y" === gblUnavailableJsonData.maintenance_flag) {
    gblUnavailableType = unavailable_types.MAINTENANCE;
  } else if (isNotBlank(gblUnavailableJsonData) && "N" === gblUnavailableJsonData.maintenance_flag) {
    gblUnavailableType = unavailable_types.SERVER_DOWN;   
  } else {
    //TODO: should handle this case
    gblUnavailableType = unavailable_types.NO_INTERNET;
  }
  kony.print("#### showMBUnavailable frmMBUnavailable.show()");
  frmMBUnavailable.show();
  dismissLoadingScreen();
}

function timeoutCheckMBServiceDown() {
  kony.print("#### timeoutCheckMBServiceDown");
  isTimeoutCheckServerDown = true;
  checkMBMaintenance();
}

function timeoutCheckMBMaintenance() {
  kony.print("#### timeoutCheckMBMaintenance");
  isTimeoutCheckMaintenance = true;
  showMBUnavailable();
}

function getMBStartupForm() {
  kony.print("#### getMBStartupForm <======");
  var startupForm = frmMBPreLoginAccessesPin;
  if (isNotBlank(gblUnavailableType)) {
    startupForm = frmMBUnavailable;
  } else {
    var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
    if (getEncrKeyFromDevice !== null) {
      startupForm = frmMBPreLoginAccessesPin;
    } else {
      startupForm = frmMBanking;
    }
  }
  return startupForm;
}

function frmMBUnavailableInit() {
  frmMBUnavailable.preShow = frmMBUnavailablePreShow;
  frmMBUnavailable.postShow = frmMBUnavailablePostShow;
  frmMBUnavailable.onDeviceBack = closeApplicationBackBtn;
  frmMBUnavailable.flxMain.onClick = frmMBUnavailableflxMainOnClick;
  frmMBUnavailable.btnRetry.onClick = frmMBUnavailableBtnRetryOnClick;
}

function frmMBUnavailablePreShow() {
  kony.print("#### frmMBUnavailablePreShow gblUnavailableType = " + gblUnavailableType);
  changeStatusBarColor();

  frmMBUnavailable.btnRetry.text = kony.i18n.getLocalizedString("MB_NoConnecttionBtn");

  if (!isNotBlank(gblUnavailableType) || gblUnavailableType == unavailable_types.UNKNOWN) {
    frmMBUnavailable.btnRetry.isVisible = false;
    frmMBUnavailable.imgStatic.isVisible = false;
    frmMBUnavailable.imgBG.isVisible = true;
    var deviceInfo = kony.os.deviceInfo();
    var deviceModel = deviceInfo.model;
    kony.print("#### deviceModel = " + deviceModel);
    if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
      frmMBUnavailable.imgBG.src = "splash_i5.png";  
      frmMBUnavailable.imgBG.imageScaleMode = constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS;
      frmMBUnavailable.imgBG.centerY = "528px";
      frmMBUnavailable.imgBG.height = "1136px";
    }else if (deviceModel == "iPhone 6" || deviceModel == "iPhone 6S" || deviceModel == "iPhone 7" || deviceModel == "iPhone 8") { 
      frmMBUnavailable.imgBG.src = "splash_i6.png";  
      frmMBUnavailable.imgBG.imageScaleMode = constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS;
      frmMBUnavailable.imgBG.centerY = "627px";
      frmMBUnavailable.imgBG.height = "1334px";
    }else if (deviceModel == "iPhone 6 Plus" || deviceModel == "iPhone 6S Plus" || deviceModel == "iPhone 7 Plus" || deviceModel == "iPhone 8 Plus") {
      frmMBUnavailable.imgBG.src = "splash_plus.png";
      frmMBUnavailable.imgBG.imageScaleMode = constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS;
      frmMBUnavailable.imgBG.centerY = "1064px";
      frmMBUnavailable.imgBG.height = "2208px";       
    }else{
      frmMBUnavailable.imgBG.src = "splash.png";
      frmMBUnavailable.imgBG.imageScaleMode = constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
      frmMBUnavailable.imgBG.centerY = "50%";
      frmMBUnavailable.imgBG.height = "100%";    
    }
    frmMBUnavailable.lblUrl.text = "";
  } else if (gblUnavailableType == unavailable_types.NO_INTERNET) {
    frmMBUnavailable.btnRetry.isVisible = true;
    frmMBUnavailable.imgStatic.isVisible = true;
    frmMBUnavailable.imgBG.isVisible = false;
    frmMBUnavailable.lblUrl.text = "";
  } else if (gblUnavailableType == unavailable_types.MAINTENANCE) {
    frmMBUnavailable.btnRetry.isVisible = false;
    frmMBUnavailable.imgStatic.isVisible = false;
    frmMBUnavailable.imgBG.isVisible = true;
    if (isNotBlank(gblUnavailableJsonData) && isNotBlank(gblUnavailableJsonData.MB)) {
      if (isNotBlank(gblUnavailableJsonData.MB.maintenance_image_url)) {
        frmMBUnavailable.imgBG.src = getUnavailableImageSRC(gblUnavailableJsonData.MB.maintenance_image_url);
      }
      if (isNotBlank(gblUnavailableJsonData.MB.maintenance_link_url)) {
        frmMBUnavailable.lblUrl.text = gblUnavailableJsonData.MB.maintenance_link_url;
      }
    }
  } else if (gblUnavailableType == unavailable_types.SERVER_DOWN) {
    frmMBUnavailable.btnRetry.isVisible = false;
    frmMBUnavailable.imgStatic.isVisible = false;
    frmMBUnavailable.imgBG.isVisible = true;
    if (isNotBlank(gblUnavailableJsonData) && isNotBlank(gblUnavailableJsonData.MB)) {
      if (isNotBlank(gblUnavailableJsonData.MB.serverdown_image_url)) {
        frmMBUnavailable.imgBG.src = getUnavailableImageSRC(gblUnavailableJsonData.MB.serverdown_image_url);
      }
      if (isNotBlank(gblUnavailableJsonData.MB.serverdown_link_url)) {
        frmMBUnavailable.lblUrl.text = gblUnavailableJsonData.MB.serverdown_link_url;
      }
    }
  }
}

function frmMBUnavailablePostShow() {
  kony.print("#### frmMBUnavailablePostShow");
  if (isNotBlank(gblUnavailableType) && gblUnavailableType == unavailable_types.UNKNOWN){
    showLoadingScreen();
  }else{
    dismissLoadingScreen();
  }
}

function frmMBUnavailableflxMainOnClick() {
  if (isNotBlank(frmMBUnavailable.lblUrl.text)) {
    kony.print("#### frmMBUnavailable openURL " + frmMBUnavailable.lblUrl.text);
    kony.application.openURL(frmMBUnavailable.lblUrl.text);
  } else {
    kony.print("#### frmMBUnavailable.lblUrl.text blank (no event)");
  }
}

function frmMBUnavailableBtnRetryOnClick() {
  kony.print("#### frmMBUnavailableBtnRetryOnClick");
  gblDisplayCurrentForm = false;
  checkMBUnavailable(true);
}

function getUnavailableImageSRC(url) {
  var imageSrc = null;
  if (isNotBlank(url) && (url.indexOf("http") === 0 || url.indexOf("https") === 0)) {
    imageSrc = url + "?random=" + Math.floor((Math.random() * 10000) + 1);
  } else {
    imageSrc = url;
  }
  kony.print("#### getUnavailableImageSRC " + imageSrc);
  return imageSrc;
}
function resetCheckMBUnavailable(){
  kony.print("#### resetCheckMBUnavailable <======");
  try {
    if (gblMBHttpRequest !== null) {
      gblMBHttpRequest.abort();
      gblMBHttpRequest = null;
    }
  }catch(e){
    kony.print("Error gblMBHttpRequest.abort() :"+e.stack);
  }     

  gblUnavailableJsonData = null;
  isTimeoutCheckServerDown = false;
  isTimeoutCheckMaintenance = false;
  cancelCheckMBServiceDownID();
  cancelCheckMBMaintenanceID();
}
function cancelCheckMBServiceDownID(){
  try {
    kony.print("#### cancelCheckMBServiceDownID <======");
    kony.timer.cancel("checkMBServiceDownID");
  }catch(e){

  }  
}
function cancelCheckMBMaintenanceID(){
  try {
    kony.print("#### cancelCheckMBMaintenanceID <======");
    kony.timer.cancel("checkMBMaintenanceID");
  }catch(e){

  }
}
function currentFormIsFrmUVApprovalMB(){
  var currForm = kony.application.getCurrentForm();
  if (isNotBlank(currForm) && isNotBlank(currForm.id)) {
    return currForm.id == "frmUVApprovalMB";
  }else{
    return false;
  }
}

function isInMBCheckingForm(){
  var result = false;
  var currForm = kony.application.getCurrentForm();
  if (isNotBlank(currForm) && isNotBlank(currForm.id) && 
      ((currForm.id == "frmMBPreLoginAccessesPin" && !isSignedUser && !gblIsLoading && !gblMBLoggingIn)  || 
       currForm.id == "frmMBanking" || currForm.id == "frmPreMenu" || currForm.id == "frmContactUsMB" || 
       currForm.id == "frmContactusFAQMB" ||  currForm.id == "frmAppTour" || currForm.id == "frmATMBranch" ||  
       currForm.id == "frmMBUnavailable")) {
    result = true;
  }
  return result;
}

function isInMBStartupForm(){
  var result = false;
  var currForm = kony.application.getCurrentForm();
  if (isNotBlank(currForm) && isNotBlank(currForm.id) && 
      (currForm.id == "frmMBPreLoginAccessesPin" ||
       currForm.id == "frmMBanking" || currForm.id == "frmPreMenu" || currForm.id == "frmContactUsMB" || 
       currForm.id == "frmContactusFAQMB" ||  currForm.id == "frmAppTour" || currForm.id == "frmATMBranch" ||  
       currForm.id == "frmMBUnavailable")) {
    result = true;
  }
  return result;
}

function checkForDisplayFingerPrintOrTouchID(){
  if(!isSignedUser){
    var currForm = kony.application.getCurrentForm();
    if (isNotBlank(currForm) && isNotBlank(currForm.id) && currForm.id == "frmMBPreLoginAccessesPin") {
      //#ifdef android
      if(isAuthUsingTouchSupported() && hasRegisteredFinger() && isTouchIDEnabled()){
        showLoadingScreen();
        GBL_FLOW_ID_CA = 13;
        getUniqueID();
      }
      //#endif
      //#ifdef iphone    
      showTouchIdPopupUsingKonyAPI();
      //#endif      
    }else{
      dismissLoadingScreen();
    }
  }
}
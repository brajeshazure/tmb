mySuggestedTopUpList = []; // Top up Suggested billers Global variable
function getMyBillTopUpListMB() {
	var inputParams = {
		IsActive: "1"
	};
  
   if(!isCacheEmpty("BillerFlowCache") && gblMyBillerTopUpBB === 0)
     getMyBillTopUpListMBCallBack(400, getCacheData("BillerFlowCache"));
   else
	invokeServiceSecureAsync("customerBillInquiry", inputParams, getMyBillTopUpListMBCallBack);
}

function getMyBillTopUpListMBCallBack(status, callBackResponse) {
  if (status == 400) {
    if (callBackResponse["opstatus"] == "0") {
      var responseData = callBackResponse["CustomerBillInqRs"];
      mySelectBillerListMB = [];
      gblMyBillList = [];
      if (responseData.length > 0) {
        gblMyBillList = responseData;
        populateMyBills(callBackResponse["CustomerBillInqRs"]);
      }
      var segData = []; 
      if(gblMyBillerTopUpBB == 0){
        var myBillTopUpHeader = kony.i18n.getLocalizedString("keyMyBills");
        var suggestedBillTopUpHeader = kony.i18n.getLocalizedString("keySuggestedBillers");				
      }else{
        var myBillTopUpHeader = kony.i18n.getLocalizedString("myTopUpsMB"); 
        var suggestedBillTopUpHeader = kony.i18n.getLocalizedString("keySuggestedTopUps"); 
      }


      if(mySelectBillerListMB.length>0){
        segData.push({
          lblSuggestedBillerTopUpHeader:myBillTopUpHeader,
          caterogeryFlag : "header",
          template: hbxMyBillsHeader
        });
        segData = segData.concat(mySelectBillerListMB);
      }

      segData.push({
        lblSuggestedBillerTopUpHeader:suggestedBillTopUpHeader,
        caterogeryFlag : "header",
        template: hbxMyBillsHeader
      });
      if(gblMyBillerTopUpBB == 0){
        segData = segData.concat(mySelectBillerSuggestListMB);
      }else{
        segData = segData.concat(mySuggestedTopUpList);
      }

      frmSelectBiller.segMyBills.widgetDataMap = {
        lblSuggestedBillerTopUpHeader: "lblSuggestedBillerTopUpHeader",
        lblSuggestedBiller: "lblSuggestedBiller",
        lblSuggestedBillerShow: "lblSuggestedBillerShow",
        lblSuggestedBillerCompCode: "lblSuggestedBillerCompCode",
        imgSuggestedBiller: "imgSuggestedBiller",
        imgBillerLogo:"imgBillerLogo",
        lblBillerNickname: "lblBillerNickname",
        lblBillerName: "lblBillerName",
        BillerTaxID: "BillerTaxID"
      };
      if (GblBillTopFlag) {
        frmSelectBiller.segMyBills.removeAll();
        frmSelectBiller.segMyBills.setVisibility(true);
        frmSelectBiller.segMyBills.setData(segData);
        dismissLoadingScreen();
        if(kony.application.getCurrentForm().id != "frmSelectBiller")
        frmSelectBiller.show();
      } else {
        dismissLoadingScreen();
        if(gblSelectBillerCategoryID == 0){
          frmSelectBiller.lblCategories.text = kony.i18n.getLocalizedString("MIB_BPCateTitle");
        } else {
          var locale = kony.i18n.getCurrentLocale();
          if (kony.string.startsWith(locale, "en", true) == true){
            frmSelectBiller.lblCategories.text = frmBillPaymentBillerCategories.segBillerCategories.selectedItems[0].lblCategoryEN.text;
          }else{
            frmSelectBiller.lblCategories.text = frmBillPaymentBillerCategories.segBillerCategories.selectedItems[0].lblCategoryTH.text;
          }
        }
        searchMyBillsAndSuggestedBillers();
      }

    } else {
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  } else {
    if (status == 300) {
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  }
}


function populateMyBills(collectionData) {
	var billername = "";
	var ref1label = "";
	var ref2label = "";
	var i = 0;
	var locale = kony.i18n.getCurrentLocale();
	if (kony.string.startsWith(locale, "en", true) == true) {
		billername = "BillerNameEN";
		ref1label = "LabelReferenceNumber1EN";
		ref2label = "LabelReferenceNumber2EN";
	} else if (kony.string.startsWith(locale, "th", true) == true) {
		billername = "BillerNameTH";
		ref1label = "LabelReferenceNumber1TH";
		ref2label = "LabelReferenceNumber2TH";
	}
		myTopUpListRs.length = 0;
		
		myBillerTopupListMB = collectionData;
		for (var i = 0; i < collectionData.length; i++) {
			
			if (collectionData[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
				//var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
				var imagesUrl = loadBillerIcons(collectionData[i]["BillerCompcode"]);
          		kony.print("$$$$$ imagesUrl is $$$$$$"+imagesUrl);
              	var billerName=collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")";
				var billerNameEN=collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")";
				var billerNameTH=collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")";
				var isRef2Req = collectionData[i]["IsRequiredRefNumber2Pay"];
			//ENH_113	
			gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay= "";
            gblbillerAllowSetSched= "";
            gblbillerTotalPayAmtEn= "";
            gblbillerTotalPayAmtTh= "";
            gblbillerTotalIntEn= "";
            gblbillerTotalIntTh= "";
            gblbillerDisconnectAmtEn= "";
            gblbillerDisconnectAmtTh= "";
            gblbillerServiceType="";
            gblbillerTransType="";
            gblMeaFeeAmount="";
            gblbillerAmountEn="";
    		gblbillerAmountTh="";
    		gblbillerMeterNoEn="";
            gblbillerMeterNoTh="";
            gblbillerCustNameEn="";
            gblbillerCustNameTh="";
            gblbillerCustAddressEn="";
            gblbillerCustAddressTh="";
            gblbillerMeterNoEn="";
            gblbillerMeterNoTh="";
            gblbillerCustNameEn="";
            gblbillerCustNameTh="";
            gblbillerCustAddressEn="";
            gblbillerCustAddressTh="";
            kony.print("BillPayment >> populateMyBills"+collectionData[i]["ITMXFlag"]);
            if(gblEA_BILLER_COMP_CODES.indexOf(collectionData[i]["BillerCompcode"]) >= 0){
            
            	if(collectionData[i]["ValidChannel"] != undefined && collectionData[i]["ValidChannel"].length > 0){
            		for (var t = 0; t < collectionData[i]["ValidChannel"].length; t++) {
            			if(collectionData[i]["ValidChannel"][t]["ChannelCode"] == "01"){
            				gblMeaFeeAmount=collectionData[i]["ValidChannel"][t]["FeeAmount"];
            			}
            		
            		}
            	}
	            if (collectionData[i]["BillerMiscData"] != undefined && collectionData[i]["BillerMiscData"].length > 0) {
	
	                for (var j = 0; j < collectionData[i]["BillerMiscData"].length; j++) {
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
	                       gblbillerStartTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
	                        gblbillerEndTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
	                        gblbillerFullPay = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
	                        gblbillerAllowSetSched = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
		                        gblbillerMeterNoEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
	                        gblbillerMeterNoTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
	                        gblbillerCustNameEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
	                        gblbillerCustNameTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
	                        gblbillerCustAddressEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
	                        gblbillerCustAddressTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
	                        gblbillerTotalPayAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
	                        gblbillerTotalPayAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
	                        gblbillerAmountEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
	                        gblbillerAmountTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
	                        gblbillerTotalIntEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
	                        gblbillerTotalIntTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
	                        gblbillerDisconnectAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
	                        gblbillerDisconnectAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
	                        gblbillerServiceType = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
	                        gblbillerTransType = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    
	                }
	             
	                
	            }
	          }
	          
            //ENH113 end
			// #CheckPoint_MWA
			else if(GLOBAL_ONLINE_PAYMENT_VERSION_MWA.indexOf(collectionData[i]["BillerCompcode"]) != -1 ){
            
            	if(collectionData[i]["ValidChannel"] != undefined && collectionData[i]["ValidChannel"].length > 0){
            		for (var t = 0; t < collectionData[i]["ValidChannel"].length; t++) {
            			if(collectionData[i]["ValidChannel"][t]["ChannelCode"] == "01"){
            				gblMeaFeeAmount=collectionData[i]["ValidChannel"][t]["FeeAmount"];
            			}
            		
            		}
            	}
	            if (collectionData[i]["BillerMiscData"] != undefined && collectionData[i]["BillerMiscData"].length > 0) {
	
	                for (var j = 0; j < collectionData[i]["BillerMiscData"].length; j++) {
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
	                       gblbillerStartTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
	                        gblbillerEndTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
	                        gblbillerFullPay = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
	                        gblbillerAllowSetSched = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
		                    gblbillerMeterNoEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
	                        gblbillerMeterNoTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
	                        gblbillerCustNameEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
	                        gblbillerCustNameTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
	                        gblbillerCustAddressEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
	                        gblbillerCustAddressTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
	                        gblbillerTotalPayAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
	                        gblbillerTotalPayAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
	                        gblbillerAmountEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
	                        gblbillerAmountTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
	                        gblbillerTotalIntEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
	                        gblbillerTotalIntTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
	                        gblbillerDisconnectAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
	                        gblbillerDisconnectAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
	                        gblbillerServiceType = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
	                        gblbillerTransType = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    
	                }
	             
	                
	            }
	          }
				var tempRecord = {
					"crmId": collectionData[i]["CRMID"],
					"lblBillerName":{
						"text": shortenBillerName(billerName,34)
						},
					"lblBillerNickname": {
						"text": collectionData[i]["BillerNickName"]
					},
					"BillerCategoryID": {
						"text": collectionData[i]["BillerCategoryID"]
					},
					"BillerCompCode": {
						"text": collectionData[i]["BillerCompcode"]
					},
					"lblRef1Value": collectionData[i]["ReferenceNumber1"],
					"lblRef1ValueMasked": maskCreditCard(collectionData[i]["ReferenceNumber1"]),
					"lblRef2Value": isNotBlank(collectionData[i]["ReferenceNumber2"]) ? collectionData[i]["ReferenceNumber2"] : "",
					"imgBillerLogo": {
						"src": imagesUrl
					},
					"lblRef1": appendColon(collectionData[i][ref1label]),
					"lblRef2": (isRef2Req == "Y") ? appendColon(collectionData[i][ref2label]) : "",
					"lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"],
					"lblRef2TH": (isRef2Req == "Y") ? collectionData[i]["LabelReferenceNumber2TH"] : "",
					"lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"],
					"lblRef2EN": (isRef2Req == "Y") ? collectionData[i]["LabelReferenceNumber2EN"] : "",
					"Ref2Len" : (isRef2Req == "Y") ? collectionData[i]["Ref2Len"] : "0",
					"Ref1Len": collectionData[i]["Ref1Len"],
					"lblBillerNameEN": shortenBillerName(billerNameEN,20),
					"lblBillerNameENFull": billerNameEN,
					"lblBillerNameTH": shortenBillerName(billerNameTH,20),
					"lblBillerNameTHFull": billerNameTH,
					"billerMethod": collectionData[i]["BillerMethod"],
					"billerGroupType": collectionData[i]["BillerGroupType"],
					"waiverCode": collectionData[i]["ReferenceNumber1"],
					"ToAccountKey": collectionData[i]["ToAccountKey"],
					"IsFullPayment" : collectionData[i]["IsFullPayment"],
					"CustomerBillID": collectionData[i]["CustomerBillID"],
					"IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
					"IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
					"billerStartTime": gblbillerStartTime,
	                "billerEndTime": gblbillerEndTime,
	                "billerFullPay": gblbillerFullPay,
	                "billerAllowSetSched": gblbillerAllowSetSched,
	                "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
	                "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
	                "billerTotalIntEn": gblbillerTotalIntEn,
	                "billerTotalIntTh": gblbillerTotalIntTh,
	                "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
	                "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
	                "billerServiceType": gblbillerServiceType,
                	"billerTransType": gblbillerTransType,
                	"billerFeeAmount":gblMeaFeeAmount,
                	"billerAmountEn":gblbillerAmountEn,
   				    "billerAmountTh":gblbillerAmountTh,
   				    "billerMeterNoEn":gblbillerMeterNoEn,
					"billerMeterNoTh":gblbillerMeterNoTh,
					"billerCustNameEn":gblbillerCustNameEn,
					"billerCustNameTh":gblbillerCustNameTh,
					"billerCustAddressEn":gblbillerCustAddressEn,
				    "billerCustAddressTh":gblbillerCustAddressTh,
				    "billerBancassurance":collectionData[i]["IsBillerBancassurance"],
				    "allowRef1AlphaNum":collectionData[i]["AllowRef1AlphaNum"],
				    "BillerTaxID":collectionData[i]["BillerTaxID"],
			    	"caterogeryFlag" : "mybills",
                   "ITMXFlag" : collectionData[i]["ITMXFlag"],
				    template: hbxMyBills
				}
		
				myTopUpListRs.push(tempRecord);
			}
		}
		mySelectBillerListMB = myTopUpListRs;
}

var isMasterBillerCacheFlow = false;

function getMyBillTopUpSuggestBillerListMB() {
  
  frmSelectBiller.segMyBills.removeAll();
  var billerGroupType = "";
  if (gblMyBillerTopUpBB == 0) {
    billerGroupType = "0";
  } else {
    billerGroupType = "1";
  }

  var inputParams = {
    IsActive: "1",
    BillerGroupType: billerGroupType,
    clientDate: getCurrentDate(),
    flagBillerList : "MB"
  };

  var refreshTimer24 = false;
  if(!isCacheEmpty("24_Hour_Refresh_MasterBillerCache")&& (Math.round(new Date().getTime()/1000) - getCacheData("24_Hour_Refresh_MasterBillerCache")) > (24*60*60))
    refreshTimer24 = true;

  //If 24 hours are not comleted and master biller cache is not empty.
  if('Y' === GET_MASTER_BILLER_REFRESH_FLAG && gblMyBillerTopUpBB == 0 && refreshTimer24 === false && !isCacheEmpty("MasterBillerInqCacheLength") && !isCacheEmpty("MasterBillerInqCache"+ (getCacheData("MasterBillerInqCacheLength") -1)) && getCacheData("MasterBillerInqCacheLength") > 10){
    isMasterBillerCacheFlow = true;
    masterBillerRowsCount = 0;
    loadBillersIncrementally = true;
    gblAfterSearch = false;
    loadIcreamentallyMasterBillerRows();
  }
  //If 24 hours are over and master biller cache is not empty. 
  else if('Y' === GET_MASTER_BILLER_REFRESH_FLAG && gblMyBillerTopUpBB == 0 && !isCacheEmpty("MasterBillerInqCacheLength") && !isCacheEmpty("MasterBillerInqCache"+ (getCacheData("MasterBillerInqCacheLength") -1)) && getCacheData("MasterBillerInqCacheLength") > 10){
    isMasterBillerCacheFlow = true;
    masterBillerRowsCount = 0;
    loadBillersIncrementally = true;
    gblAfterSearch = false;
    loadIcreamentallyMasterBillerRows();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, updateMasterBillerCacheOnBackGround);
  }
  //If 24 hours are over or in activation flow and master biller cache is empty. 
  else{
    isMasterBillerCacheFlow = false;
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, getMyBillTopUpSuggestBillerListMBCallback);
  }
}

function updateMasterBillerCacheOnBackGround(status, callBackResponse){
  if (status == 400) {
    //Setting master biller cache
    if(gblMyBillerTopUpBB === 0 && callBackResponse["MasterBillerInqRs"].length > 0){
      setMasterBillerBatchCache(callBackResponse["MasterBillerInqRs"]);
      setCacheData("24_Hour_Refresh_MasterBillerCache",Math.round(new Date().getTime()/1000));
    }
  }
}

function getMyBillTopUpSuggestBillerListMBCallback(status, callBackResponse) {
  if (status == 400) {
    if(isMasterBillerCacheFlow){
      populateSuggestBillers([]);
      getMyBillTopUpListMB(); 
    }else{
      if (callBackResponse["opstatus"] == "0") {
        
        if (callBackResponse["MasterBillerInqRs"].length > 0) {
          
          //Setting master biller cache
          if(gblMyBillerTopUpBB === 0){
           setMasterBillerBatchCache(callBackResponse["MasterBillerInqRs"]);
           setCacheData("24_Hour_Refresh_MasterBillerCache",Math.round(new Date().getTime()/1000));
          }

          gblBillersFound = true;
          frmSelectBiller.lblNoBillers.setVisibility(false);
          populateSuggestBillers(callBackResponse["MasterBillerInqRs"]);
          getMyBillTopUpListMB();               
        } else {
          mySelectBillerSuggestListMB = [];
          gblBillersFound = false;
          dismissLoadingScreen();
        }
      } else {
        dismissLoadingScreen();
        alert(kony.i18n.getLocalizedString("ECGenericError"));
      }
    }
  } else {
    if (status == 300) {
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("ECGenericError"));
    }
  }
}

function populateSuggestBillers(collectiondata) {
  
  if(isMasterBillerRowCreatorFlow == true){
      suggestedTopUpsNBillers = [];
  }
  
  var billername = "";
  var ref1label = "";
  var ref2label = "";
 
  //BILLER_LOGO_URL="https://vit.tau2904.com/tmb/ImageRender";//hard code to check
  var locale = kony.i18n.getCurrentLocale();
  if (kony.string.startsWith(locale, "en", true) == true) {
    billername = "BillerNameEN";
    ref1label = "LabelReferenceNumber1EN";
    ref2label = "LabelReferenceNumber2EN";
  } else if (kony.string.startsWith(locale, "th", true) == true) {
    billername = "BillerNameTH";
    ref1label = "LabelReferenceNumber1TH";
    ref2label = "LabelReferenceNumber2TH";
  }

  myTopUpSuggestListRs.length = 0;
  var masterData = [];
  var tempMinAmount = [];
  gblstepMinAmount = [];

  for (var i = 0; i < (isMasterBillerCacheFlow && isMasterBillerRowCreatorFlow == false ? getCacheData("MasterBillerInqCacheLength") :  collectiondata.length); i++) {
    /* minimum amount locha */
    if(isMasterBillerCacheFlow && isMasterBillerRowCreatorFlow == false){
      collectiondata[i] = kony.store.getItem("MasterBillerInqCache"+i);
    }  
    var billerMethod = collectiondata[i]["BillerMethod"];


        if (billerMethod == "1" && collectiondata[i]["BillerGroupType"] == "1" && collectiondata[i]["BillerCompcode"]!="2605") {

            var min = collectiondata[i]["StepAmount"][0]["Amt"];

            tempMinAmount = {
                "compcode": collectiondata[i]["BillerCompcode"],
                "minAmount": min
            }
        } else {
            tempMinAmount = [];
        }
        if (collectiondata[i]["BillerGroupType"] == gblMyBillerTopUpBB) {
            //
          	
            //var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectiondata[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            var imagesUrl="";
          	imagesUrl = loadBillerIcons(collectiondata[i]["BillerCompcode"]);
          	kony.print("$$$$$ imagesUrl is $$$$$$"+imagesUrl);
          	//
            var billerNameEN = collectiondata[i]["BillerNameEN"] + " (" + collectiondata[i]["BillerCompcode"] + ")";
            var billerNameTH = collectiondata[i]["BillerNameTH"] + " (" + collectiondata[i]["BillerCompcode"] + ")";

            //ENH113 
			gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay= "";
            gblbillerAllowSetSched= "";
            gblbillerTotalPayAmtEn= "";
            gblbillerTotalPayAmtTh= "";
            gblbillerTotalIntEn= "";
            gblbillerTotalIntTh= "";
            gblbillerDisconnectAmtEn= "";
            gblbillerDisconnectAmtTh= "";
            gblbillerServiceType="";
            gblbillerTransType="";
            gblMEABillerFee = "";
            gblbillerAmountEn="";
    		gblbillerAmountTh="";
    		gblbillerMeterNoEn="";
            gblbillerMeterNoTh="";
            gblbillerCustNameEn="";
            gblbillerCustNameTh="";
            gblbillerCustAddressEn="";
            gblbillerCustAddressTh="";
            gblbillerMeterNoEn="";
            gblbillerMeterNoTh="";
            gblbillerCustNameEn="";
            gblbillerCustNameTh="";
            gblbillerCustAddressEn="";
            gblbillerCustAddressTh="";
            gblAllowSearch = "";
            gblTopupAmtMin = "";
            gblTopupAmtMax = "";
            gblBillerTaxID = "";
    		
			if(gblEA_BILLER_COMP_CODES.indexOf(collectiondata[i]["BillerCompcode"]) >= 0){
			 if (collectiondata[i]["ValidChannel"] != undefined && collectiondata[i]["ValidChannel"].length > 0){
			   for (var j = 0; j < collectiondata[i]["ValidChannel"].length; j++){
			     if (collectiondata[i]["ValidChannel"][j]["ChannelCode"] == "02"){
			       gblMEABillerFee = collectiondata[i]["ValidChannel"][j]["FeeAmount"];
			       kony.print("gblMEABillerFee :"+gblMEABillerFee);
			     }
			   }
			 }
			    
			   
	            if (collectiondata[i]["BillerMiscData"] != undefined && collectiondata[i]["BillerMiscData"].length > 0) {
	
	                for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
	                       gblbillerStartTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
	                        gblbillerEndTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
	                        gblbillerFullPay = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
	                        gblbillerAllowSetSched = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
		                        gblbillerMeterNoEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
	                        gblbillerMeterNoTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
	                        gblbillerCustNameEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
	                        gblbillerCustNameTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
	                        gblbillerCustAddressEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
	                        gblbillerCustAddressTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
	                        gblbillerTotalPayAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
	                        gblbillerTotalPayAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
	                        gblbillerAmountEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
	                        gblbillerAmountTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
	                        gblbillerTotalIntEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
	                        gblbillerTotalIntTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
	                        gblbillerDisconnectAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
	                        gblbillerDisconnectAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
	                        gblbillerServiceType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
	                        gblbillerTransType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    
	                }
	              kony.print("gblbillerStartTime: " + gblbillerStartTime + "gblbillerEndTime :" + gblbillerEndTime);
	            }
	          }
            //ENH113 end
			else if (collectiondata[i]["BillerCompcode"]=="2704"){
	        	//True Money
				if (collectiondata[i]["BillerMiscData"] != undefined && collectiondata[i]["BillerMiscData"].length > 0) {
	
	                for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.AMOUNT.MIN") {
	                       gblTopupAmtMin = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.AMOUNT.MAX") {
	                        gblTopupAmtMax = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }                   
	                }
	            }	        	
	        }
			
			// #CheckPoint_MWA        				
			else if(GLOBAL_ONLINE_PAYMENT_VERSION_MWA.indexOf(collectiondata[i]["BillerCompcode"]) != -1 ){			
			 if (collectiondata[i]["ValidChannel"] != undefined && collectiondata[i]["ValidChannel"].length > 0){
			   for (var j = 0; j < collectiondata[i]["ValidChannel"].length; j++){
			     if (collectiondata[i]["ValidChannel"][j]["ChannelCode"] == "02"){
			       gblMEABillerFee = collectiondata[i]["ValidChannel"][j]["FeeAmount"];
			       kony.print("gblMEABillerFee :"+gblMEABillerFee);
			     }
			   }
			 }
			    
			   
	            if (collectiondata[i]["BillerMiscData"] != undefined && collectiondata[i]["BillerMiscData"].length > 0) {
	
	                for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
	                       gblbillerStartTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
	                        gblbillerEndTime = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
	                        gblbillerFullPay = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
	                        gblbillerAllowSetSched = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
		                        gblbillerMeterNoEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
	                        gblbillerMeterNoTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
	                        gblbillerCustNameEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
	                        gblbillerCustNameTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
	                        gblbillerCustAddressEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
	                        gblbillerCustAddressTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
	                        gblbillerTotalPayAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
	                        gblbillerTotalPayAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
	                        gblbillerAmountEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
	                        gblbillerAmountTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
	                        gblbillerTotalIntEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
	                        gblbillerTotalIntTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
	                        gblbillerDisconnectAmtEn = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
	                        gblbillerDisconnectAmtTh = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
	                        gblbillerServiceType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
	                        gblbillerTransType = collectiondata[i]["BillerMiscData"][j]["MiscText"];
	                    }	
	                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSearch") {
            				gblAllowSearch = collectiondata[i]["BillerMiscData"][j]["MiscText"];
        				}                    
	                }
	              kony.print("gblbillerStartTime: " + gblbillerStartTime + "gblbillerEndTime :" + gblbillerEndTime);
	            }
	          }
			
            var tempRecord = {
                "lblSuggestedBiller": shortenBillerName(collectiondata[i][billername] + " (" + collectiondata[i]["BillerCompcode"] + ")",20),
                "lblSuggestedBillerShow": shortenBillerName(collectiondata[i][billername],20),
                "lblSuggestedBillerCompCode": "(" + collectiondata[i]["BillerCompcode"] + ")",
              	"btnAddBiller": {
                    "text": "  ",
                    "skin": "btnAdd"
                },
                "BarcodeOnly": collectiondata[i]["BarcodeOnly"],
                "BillerID": collectiondata[i]["BillerID"],
                "BillerCompCode": collectiondata[i]["BillerCompcode"],
                "Ref1Label": collectiondata[i][ref1label],
                "Ref2Label": collectiondata[i][ref2label],
                "imgSuggestedBiller": {
                    "src": imagesUrl
                },
                "IsRequiredRefNumber2Add": collectiondata[i]["IsRequiredRefNumber2Add"],
                "IsRequiredRefNumber2Pay": collectiondata[i]["IsRequiredRefNumber2Pay"],
                "EffDt": collectiondata[i]["EffDt"],
                "lblBillerNameEN": shortenBillerName(billerNameEN,20),
                "lblBillerNameENFull": billerNameEN,
                "lblBillerNameTH": shortenBillerName(billerNameTH,20),
                "lblBillerNameTHFull": billerNameTH,
                "BillerMethod": collectiondata[i]["BillerMethod"],
                "BillerGroupType": collectiondata[i]["BillerGroupType"],
                "ToAccountKey": collectiondata[i]["ToAccountKey"],
                "IsFullPayment": collectiondata[i]["IsFullPayment"],
                "Ref1Len": {
                    "text": collectiondata[i]["Ref1Len"]
                },
                "Ref2Len": {
                    "text": collectiondata[i]["Ref2Len"]
                },
                "BillerCategoryID": {
                    "text": collectiondata[i]["BillerCategoryID"]
                },
                "hdnLabelRefNum1EN": {
                    "text": collectiondata[i]["LabelReferenceNumber1EN"]
                },
                "hdnLabelRefNum1TH": {
                    "text": collectiondata[i]["LabelReferenceNumber1TH"]
                },

                "hdnLabelRefNum2EN": {
                    "text": collectiondata[i]["LabelReferenceNumber2EN"]
                },
                "hdnLabelRefNum2TH": {
                    "text": collectiondata[i]["LabelReferenceNumber2TH"]
                },
                "StepAmount": collectiondata[i]["StepAmount"],
                "billerStartTime": gblbillerStartTime,
                "billerEndTime": gblbillerEndTime,
                "billerFullPay": gblbillerFullPay,
                "billerAllowSetSched": gblbillerAllowSetSched,
                "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                "billerTotalIntEn": gblbillerTotalIntEn,
                "billerTotalIntTh": gblbillerTotalIntTh,
                "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                "billerFeeAmount":gblMEABillerFee,
                "billerServiceType": gblbillerServiceType,
                "billerTransType": gblbillerTransType,
                "billerAmountEn":gblbillerAmountEn,
    			"billerAmountTh":gblbillerAmountTh,
    			"billerMeterNoEn":gblbillerMeterNoEn,
				"billerMeterNoTh":gblbillerMeterNoTh,
				"billerCustNameEn":gblbillerCustNameEn,
				"billerCustNameTh":gblbillerCustNameTh,
				"billerCustAddressEn":gblbillerCustAddressEn,
			    "billerCustAddressTh":gblbillerCustAddressTh,
			    "billerBancassurance":collectiondata[i]["IsBillerBancassurance"],
			    "allowRef1AlphaNum":collectiondata[i]["AllowRef1AlphaNum"],
                "ITMXFlag":collectiondata[i]["ITMXFlag"],
			    "TopupAmtMin":gblTopupAmtMin,
			    "TopupAmtMax":gblTopupAmtMax,
			    "BillerTaxID":collectiondata[i]["BillerTaxID"],
			    caterogeryFlag : "suggested",
			    template: hbxSuggestedBillers
            }
            if(isMasterBillerRowCreatorFlow == true){
                    return tempRecord;
            }
            if (gblAllowSearch != "N") {
            	myTopUpSuggestListRs.push(tempRecord);
            }            
            if(!isSearched) TempDataBillers.push(tempRecord);
            if (collectiondata[i]["BillerGroupType"] == "1") {
                if (tempMinAmount.length != 0)
                    gblstepMinAmount.push(tempMinAmount);
            }

    }
  }

  if(gblMyBillerTopUpBB == 0){
    mySelectBillerSuggestListMB = myTopUpSuggestListRs;
  }else{
    mySuggestedTopUpList = myTopUpSuggestListRs;
  }

}

function loadIcreamentallyMasterBillerRows(){
  try{     
    if(loadBillersIncrementally == false)
      return;
    var masterBillerInqCacheLength = getCacheData("MasterBillerInqCacheLength");
    var i = masterBillerRowsCount;
    if(i >= masterBillerInqCacheLength)
      return;
    mySelectBiller = [];
    var rowsLoad = 0;
    if(masterBillerRowsCount === 0){
      rowsLoad = 10;
    }
    else{
      rowsLoad = 50;
    }
    while(i < masterBillerInqCacheLength && (i < masterBillerRowsCount +rowsLoad  || i + rowsLoad > masterBillerInqCacheLength )){
      isMasterBillerRowCreatorFlow = true;
      var collectionData = [];
      collectionData.push(getCacheData("MasterBillerInqCache"+i++));
      mySelectBiller.push(populateSuggestBillers(collectionData));
      isMasterBillerRowCreatorFlow = false;
    }
    masterBillerRowsCount = i;
    mySelectBillerSuggestListMB = [];
    if(gblAfterSearch === true || kony.application.getCurrentForm().id !== "frmSelectBiller"){
      gblAfterSearch = false;
      mySelectBillerSuggestListMB = mySelectBiller;
      getMyBillTopUpListMB();

    }
    else{
      frmSelectBiller.segMyBills.addAll(mySelectBiller);
    }
  }catch(e){
    kony.print("error in loadIcreamentallyMasterBillerRows "+e);
  }
}

function searchMyBillsAndSuggestedBillers() {
  try{
    gblAfterSearch = true;
    var searchMyBillList = [];
    var searchSuggestedBillList = [];
    var searchText = frmSelectBiller.tbxSearch.text;
    var searchValue = ""; 
    //var suggestedTopUpsNBillers=[];	

    var masterBillerInqCacheLength = getCacheData("MasterBillerInqCacheLength");

    if(gblMyBillerTopUpBB == 0){
      // suggestedTopUpsNBillers = mySelectBillerSuggestListMB;
    }else{
      suggestedTopUpsNBillers = mySuggestedTopUpList;
    }

    //MyBills Search
    if(mySelectBillerListMB != null && mySelectBillerListMB.length > 0) { // Atleast one biller should be avilable
      if (isNotBlank(searchText) && searchText.length >= 3) { //Atleast 3 characters should be entered in search box

        searchText = searchText.toLowerCase();

        for (j = 0, i = 0; i < mySelectBillerListMB.length; i++) {

          searchValue = mySelectBillerListMB[i].lblBillerNickname.text.toLowerCase() 
            + "~" + mySelectBillerListMB[i].lblBillerNameENFull.toLowerCase() 
            + "~" + mySelectBillerListMB[i].lblBillerNameTHFull.toLowerCase() 
            + "~" + mySelectBillerListMB[i].BillerCompCode.text.toLowerCase() 
            + "~" + mySelectBillerListMB[i].BillerTaxID
            + "~" + mySelectBillerListMB[i].lblRef1Value;

          if (searchValue.indexOf(searchText) > -1) {
            if(gblSelectBillerCategoryID == "0" || mySelectBillerListMB[i].BillerCategoryID.text == gblSelectBillerCategoryID) {
              searchMyBillList[j] = mySelectBillerListMB[i];
              j++;
            }
          }
        }//For Loop End

      } else { //searchText condition End
        // Search text is less than 3 characters hence show all My Bills
        if(gblSelectBillerCategoryID == "0") {
          searchMyBillList = mySelectBillerListMB;
        } else {
          for (j = 0, i = 0; i < mySelectBillerListMB.length; i++) {
            if(mySelectBillerListMB[i].BillerCategoryID.text == gblSelectBillerCategoryID) {
              searchMyBillList[j] = mySelectBillerListMB[i];
              j++;
            }
          }
        }
      }
    }

    //Suggested Biller Search 
    //if(suggestedTopUpsNBillers != null && suggestedTopUpsNBillers.length > 0) { // Atleast one biller should be avilable
    if (isNotBlank(searchText) && searchText.length >= 3) { //Atleast 3 characters should be entered in search box
      searchText = searchText.toLowerCase();
      if(suggestedTopUpsNBillers.length == 0){
        var j = 0;
        searchString_masterBillerCache_KeyValue = getCacheData("searchString_masterBillerCache_KeyValue");
        for (var searchkey in searchString_masterBillerCache_KeyValue) {
          if (searchString_masterBillerCache_KeyValue.hasOwnProperty(searchkey)) {

            if (searchkey.indexOf(searchText) > -1) {
              var billerObject = getCacheData(searchString_masterBillerCache_KeyValue[searchkey]);
              if(gblSelectBillerCategoryID == "0" || billerObject.BillerCategoryID == gblSelectBillerCategoryID) {
                isMasterBillerRowCreatorFlow = true;
                var collectionData = [];
                collectionData.push(billerObject);
                searchSuggestedBillList[j] = populateSuggestBillers (collectionData);
                isMasterBillerRowCreatorFlow = false;
                j++;
              }
            }
          }
        }
      }else{
        for (j = 0, i = 0; i < suggestedTopUpsNBillers.length; i++) {

          var searchValue = suggestedTopUpsNBillers[i].lblBillerNameENFull.toLowerCase()
          + "~" + suggestedTopUpsNBillers[i].lblBillerNameTHFull.toLowerCase() 
          + "~" + suggestedTopUpsNBillers[i].BillerTaxID
          + "~" + suggestedTopUpsNBillers[i].BillerCompCode.toLowerCase();

          if (searchValue.indexOf(searchText) > -1) {
            if(gblSelectBillerCategoryID == "0" || suggestedTopUpsNBillers[i].BillerCategoryID.text == gblSelectBillerCategoryID) {
              searchSuggestedBillList[j] = suggestedTopUpsNBillers[i];
              j++;
            }
          }
        }

      }
      loadBillersIncrementally = false;
    } else { 
      //searchText condition End
      // Search text is less than 3 characters hence show all My Bills
      if(gblSelectBillerCategoryID == "0") {
        if(gblMyBillerTopUpBB != 0){
          searchSuggestedBillList = suggestedTopUpsNBillers;
        }else{
          masterBillerRowsCount = 0;
          loadBillersIncrementally = true;
          loadIcreamentallyMasterBillerRows();
          return;
        }
      } else {
        if(gblMyBillerTopUpBB != 0){
          for (j = 0, i = 0; i < suggestedTopUpsNBillers.length; i++) {
            if(suggestedTopUpsNBillers[i].BillerCategoryID.text == gblSelectBillerCategoryID) {
              searchSuggestedBillList[j] = suggestedTopUpsNBillers[i];
              j++;
            }
          }
        }else{
          suggestedTopUpsNBillers = [];
          var j = 0;
          for (i = 0; i < masterBillerInqCacheLength; i++) {
            var masterBillerObject = getCacheData("MasterBillerInqCache"+i);
            if(masterBillerObject["BillerCategoryID"] == gblSelectBillerCategoryID) {
              isMasterBillerRowCreatorFlow = true;
              var collectionData = [];
              collectionData.push(masterBillerObject);
              searchSuggestedBillList[j] = populateSuggestBillers(collectionData);
              j++;
              isMasterBillerRowCreatorFlow = false;
            }
          }
          //suggestedTopUpsNBillers = searchSuggestedBillList;
          loadBillersIncrementally = false;
        }
      }
    }
    var segData = []; 

    if(searchMyBillList.length > 0) {
      segData.push({
        lblSuggestedBillerTopUpHeader:kony.i18n.getLocalizedString("keyMyBills"),
        caterogeryFlag : "header",
        template: hbxMyBillsHeader
      });
      segData = segData.concat(searchMyBillList);
    }
    if(searchSuggestedBillList.length > 0) {
      segData.push({
        lblSuggestedBillerTopUpHeader:kony.i18n.getLocalizedString("keySuggestedBillers"),
        caterogeryFlag : "header",
        template: hbxMyBillsHeader
      });

      segData = segData.concat(searchSuggestedBillList);
    }
    frmSelectBiller.lblNoBillers.setVisibility(false);
    frmSelectBiller.segMyBills.setVisibility(true);
    frmSelectBiller.segMyBills.removeAll();
    frmSelectBiller.segMyBills.setData(segData);
    if(searchMyBillList.length == 0 && searchSuggestedBillList.length == 0){
      frmSelectBiller.segMyBills.setVisibility(false);
      frmSelectBiller.lblNoBillers.setVisibility(true);
      frmSelectBiller.lblNoBillers.text = kony.i18n.getLocalizedString("keybillernotfound");
    }
  }catch(e){
    kony.print("Exception occured during biller search "+e);
  }
}

function validateSearch(searchText) {
    var isValid = true;
	if(isNotBlank(searchText)) {
		var mypattern = /^[A-Za-z0-9\u0E00-\u0E7F\s._@&-]+$/;
		isValid = mypattern.test(searchText);
	}
	return isValid;
}

function myBillersSegmentRowSelct(){
	var caterogeryFlag = frmSelectBiller["segMyBills"]["selectedItems"][0]["caterogeryFlag"];
  kony.print("Siva raja >> caterogeryFlag"+caterogeryFlag);
	if(caterogeryFlag == "mybills"){
		myBillsRowSelected();
	}else if(caterogeryFlag == "suggested"){
		otherBillerSelected();
	}
}

function getBillerNickNameFromMyBills(myBillList, compCode, Ref1, Ref2) {
	var nickName = "";
	gblBillerPresentInMyBills = false;
	if (myBillList.length > 0) {
		//Checking whether Scanned biller is available in My Bills or not
		for (var i = 0; i < myBillList.length; i++) {
			if(myBillList[i]["BillerGroupType"] == gblbillerGroupType){
				if ((myBillList[i]["BillerCompcode"] == compCode) && (myBillList[i]["ReferenceNumber1"] == Ref1)) {
					if(myBillList[i]["IsRequiredRefNumber2Add"] == "Y" && gblbillerGroupType == 0) {
						if(myBillList[i]["ReferenceNumber2"] == Ref2) {
							gblBillerPresentInMyBills = true;
							nickName = myBillList[i]["BillerNickName"] ;
							gblBillerID = myBillList[i]["CustomerBillID"] ;
							break;
						}
					} else {
						gblBillerPresentInMyBills = true;
						nickName = myBillList[i]["BillerNickName"] ;
						gblBillerID = myBillList[i]["CustomerBillID"] ;
						break;
					}
				}
			}
		}
	}
	
	return nickName;
}

function checkBillerNicknameInMyBills(myBillList, newNickname) {
	gblBillerPresentInMyBills = false;
	if (myBillList.length > 0) {
		//Checking whether new Nickname is already there in My Bills or not
		for (var i = 0; i < myBillList.length; i++) {
			if(myBillList[i]["BillerGroupType"] == gblbillerGroupType){
				if (kony.string.equals(myBillList[i]["BillerNickName"], newNickname)){
					return true;
				}
			}
		}
	}
	return false;
}
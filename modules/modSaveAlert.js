
function onClickSaveAlertMenu(){
  gblSavelAlertMode = "normal";
  var saveAlertIntro = kony.store.getItem("SaveAlertIntro");
  kony.print("@@@saveAlertIntro::"+saveAlertIntro);
  if(isNotBlank(saveAlertIntro) && saveAlertIntro == "True"){
    saveAlertList();
  }else{
    frmSend2SaveSmartRequest.show();
  }
}

function onClickSaveAlertStartUsing(){
  kony.store.setItem("SaveAlertIntro", "True");
  saveAlertList();
}

function frmSend2SaveSmartRequestInit() {
  try {
    kony.print("@@@ In frmSend2SaveSmartRequestInit() @@@");
    frmSend2SaveSmartRequest.preShow = frmSend2SaveSmartRequestPreshow;
  } catch (e) {
    kony.print("@@@ In frmSend2SaveSmartRequestInit() Exception:::" + e);
  }
}

function frmSend2SaveSmartRequestPreshow() {
  try {
    frmSend2SaveSmartRequest.segSend2SaveSmartRequest.pageSkin = noSkinSegmentRow;
    frmSend2SaveSmartRequest.label475124774164.text = kony.i18n.getLocalizedString("MB_S2SIntroTitle");
    setDataTofrmSend2SaveSmartRequest();
    frmSend2SaveSmartRequest.segSend2SaveSmartRequest.selectedIndex = [0, 0];
    gblNextIndex = 0;
    frmSend2SaveSmartRequest.btnNext.text = kony.i18n.getLocalizedString("MB_S2SNextbtn");
    frmSend2SaveSmartRequest.btnSkipIntro.text = kony.i18n.getLocalizedString("MB_S2SLaterbtn");
    frmSend2SaveSmartRequest.btnStartUsing.text = kony.i18n.getLocalizedString("MB_S2SStartbtn");
    frmSend2SaveSmartRequest.btnStartUsing.onClick = onClickSaveAlertStartUsing;
  	frmSend2SaveSmartRequest.btnSkipIntro.onClick = onClickSaveAlertStartUsing;
    frmSend2SaveSmartRequest.btnNext.onClick = onClickS2SIntroNext;
  } catch (e) {
    kony.print("@@@ In frmSend2SaveSmartRequestPreshow() Exception:::" + e);
  }
}

function setDataTofrmSend2SaveSmartRequest() {
  try {
    var data = [{
      "Label0e0c8fd24d22945": kony.i18n.getLocalizedString("MB_S2SBeneTitle1"),
      "imageTutorial": "savealertintro1.png",
      "richTextActication": kony.i18n.getLocalizedString("MB_S2SBeneDesc1")
    }, {
      "Label0e0c8fd24d22945": kony.i18n.getLocalizedString("MB_S2SBeneTitle2"),
      "imageTutorial": "savealertintro2.png",
      "richTextActication": kony.i18n.getLocalizedString("MB_S2SBeneDesc2")
    }, {
      "Label0e0c8fd24d22945": kony.i18n.getLocalizedString("MB_S2SBeneTitle3"),
      "imageTutorial": "savealertintro3.png",
      "richTextActication": kony.i18n.getLocalizedString("MB_S2SBeneDesc3")
    }]
    frmSend2SaveSmartRequest.segSend2SaveSmartRequest.removeAll();
    frmSend2SaveSmartRequest.segSend2SaveSmartRequest.setData(data);
  } catch (e) {
    kony.print("@@@ In setDataTofrmSend2SaveSmartRequest() Exception:::" + e);
  }
}

function swipeSend2SaveSmartRequest() {
  try {
    swipeSend2SaveSmartRequest.prototype.paginationSwitch = function(sectionIndex, rowIndex) {
      kony.print("@@in swipe gblNextIndex vale"+gblNextIndex);
      rowIndex = parseFloat(rowIndex);
      frmSend2SaveSmartRequest.segSend2SaveSmartRequest.selectedIndex = [0, rowIndex];
      gblNextIndex = rowIndex;
      kony.print("@@@@@rowIndex@@@@"+gblNextIndex);
      frmS2SSuccessSetVisible();
    }
  } catch (e) {
    kony.print("@@@ In swipeSend2SaveSmartRequest() Exception:::" + e);
  }
}

function showfrmSend2SaveDashboard() {
  try{
    if(gblSavelAlertMode == "normal"){
      frmSend2SaveDashboard.FlextextSearch.setVisibility(false);
      frmSend2SaveDashboard.btnCancelS2S.setVisibility(false);
      frmSend2SaveDashboard.txtSearch.text = "";
      frmSend2SaveDashboard.show();
    }else if(gblSavelAlertMode == "edit"){
      deleteSaveAlertRequest();
    }
  }catch(e) {
    kony.print("@@@ In showfrmSend2SaveDashboard() Exception:::" + e);
  }
}

function frmSend2SaveDashboardInit() {
  try {
    kony.print("@@@ In frmSend2SaveDashboardInit() @@@");
    frmSend2SaveDashboard.preShow = frmSend2SaveDashboardPreshow;
    frmSend2SaveDashboardActions();
  } catch (e) {
    kony.print("@@@ In frmSend2SaveDashboardInit() Exception:::" + e);
  }
}

function frmSend2SaveDashboardPreshow() {
  try {
    frmSend2SaveDashboard.lblHeader.text = kony.i18n.getLocalizedString("MB_S2SDBTitle");
    frmSend2SaveDashboard.btnCancelS2S.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmSend2SaveDashboard.txtSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
    frmSend2SaveDashboard.lblRequestList.text = kony.i18n.getLocalizedString("MB_S2SActivelistLab");
    frmSend2SaveDashboard.lblAddRequestList.text = kony.i18n.getLocalizedString("MB_S2SDBnolistDesc");
    frmSend2SaveDashboard.imgSearch.src = "searchico.png";
    frmSend2SaveDashboard.ImageArrowS2S.src = "icon_arrow_guide.png";
    frmSend2SaveDashboard.ImageAddMoreRequest.src = "icon_new_request.png";
    frmSend2SaveDashboard.ImageAddNewRequest.src = "icon_new_request.png";
  } catch (e) {
    kony.print("@@@ In frmSend2SaveDashboardPreshow() Exception:::" + e);
  }
}

function frmSend2SaveDashboardActions(){
    frmSend2SaveDashboard.btnSearchS2S.onClick = onClickbtnSearchS2S ;
    frmSend2SaveDashboard.btnCancelS2S.onClick = onClickbtnCancelS2SHeader ;
  	frmSend2SaveDashboard.txtSearch.onTextChange = searchSaveAlertList;
    frmSend2SaveDashboard.flxbtnAddMoreRequest.onClick = function(){customerAccountInquirySaveAlertAccounts("normal");};
    frmSend2SaveDashboard.flxbtnAddNewRequest.onClick = function(){customerAccountInquirySaveAlertAccounts("normal");};
    frmSend2SaveDashboard.segSend2SaveDashboardDetails.onRowClick = onClickOfSegmentSaveAlertDashboard;
}

function onClickbtnSearchS2S() {
  try {
    frmSend2SaveDashboard.FlextextSearch.setVisibility(true);
    frmSend2SaveDashboard.btnCancelS2S.setVisibility(true);
    frmSend2SaveDashboard.btnSearchS2S.setVisibility(false);
    frmSend2SaveDashboard.lblHeader.setVisibility(false);
  } catch (e) {
    kony.print("@@@ In onClickbtnSearchS2S() Exception:::" + e);
  }
}

function onClickbtnCancelS2SHeader() {
  try {
    frmSend2SaveDashboard.FlextextSearch.setVisibility(false);
    frmSend2SaveDashboard.btnCancelS2S.setVisibility(false);
    frmSend2SaveDashboard.btnSearchS2S.setVisibility(true);
    frmSend2SaveDashboard.lblHeader.setVisibility(true);
    frmSend2SaveDashboard.txtSearch.text = "";
    setSaveAlertListData(gblSaveAlertList);
  } catch (e) {
    kony.print("@@@ In onClickbtnCancelS2SHeader() Exception:::" + e);
  }
}

function saveAlertList(){
 try{
    gblSavelAlertMode = "normal";
   	var inputParam = {};
    showLoadingScreen();
   	invokeServiceSecureAsync("saveAlertList", inputParam, callBacksaveAlertList);
  }catch(e){
   	kony.print("@@@ Exception in saveAlertList:"+e);
 }
}

function callBacksaveAlertList(status, resulttable){
  try{
    kony.print("@@@ In callBacksaveAlertList() @@@");
    if (resulttable["opstatus"] == 0){
      dismissLoadingScreen();
      if(isNotBlank(resulttable["saveAlertListDS"])){
        frmSend2SaveDashboard.flxNoResults.setVisibility(false);
        frmSend2SaveDashboard.flxCreateNewRequest.setVisibility(false);
        frmSend2SaveDashboard.flxRequestList.setVisibility(true);
        var resSaveAlertList = resulttable["saveAlertListDS"];
        gblSaveAlertList = resulttable["saveAlertListDS"];
        setSaveAlertListData(resSaveAlertList);
      }else{
        frmSend2SaveDashboard.flxNoResults.setVisibility(false);
        frmSend2SaveDashboard.flxRequestList.setVisibility(false);
        frmSend2SaveDashboard.flxCreateNewRequest.setVisibility(true);
      }  
      showfrmSend2SaveDashboard();
    }else if(resulttable["opstatus"] == 1){
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return;
    }
  }catch(e){
    dismissLoadingScreen();
    kony.print("@@@ In callBacksaveAlertList() Exception:::"+e);
  }
}

function setSaveAlertListData(saveAlertList){
  try{
    kony.print("@@@ In setSaveAlertListData() @@@");
    var saveAlertData = [];
    kony.print("@@@ saveAlertList:::"+JSON.stringify(saveAlertList));
    if(saveAlertList != null && saveAlertList != "" && saveAlertList != undefined){
      for(var i=0; i < saveAlertList.length; i++){
        var recuringVal = "1";
        var executeTimes = saveAlertList[i]["ExecutionTimes"];
        var processTrans = saveAlertList[i]["TotalProcessesTransactions"];
        if(isNotBlank(executeTimes) && isNotBlank(processTrans)){
          recuringVal =  parseInt(executeTimes) - parseInt(processTrans);
        }
        var tempData = {
          "lblRecurringRequest": recuringVal+"x",
          "lblRequestName": checkStringVal(saveAlertList[i]["Memo"]),
          "imgArrow": "navarrowblue.png",
          "TranAmt": checkStringVal(saveAlertList[i]["TranAmt"]),
          "ReceiverAcctDispName": checkStringVal(saveAlertList[i]["ReceiverAcctDispName"]),
          "ExpiryDate": checkStringVal(saveAlertList[i]["ExpiryDate"]),
          "RTPStatus": checkStringVal(saveAlertList[i]["RTPStatus"]),
          "SenderBankCode": checkStringVal(saveAlertList[i]["SenderBankCode"]),
          "ReceiverBankCode": checkStringVal(saveAlertList[i]["ReceiverBankCode"]),
          "SenderAcctName": checkStringVal(saveAlertList[i]["SenderAcctName"]),
          "RTPTranRef": checkStringVal(saveAlertList[i]["RTPTranRef"]),
          "ReceiverAcctNo": checkStringVal(saveAlertList[i]["ReceiverAcctNo"]),
          "SenderAcctNo": checkStringVal(saveAlertList[i]["SenderAcctNo"]),
          "toAccNumber": checkStringVal(saveAlertList[i]["toAccNumber"]),
          "ReceiverProxyType": checkStringVal(saveAlertList[i]["ReceiverProxyType"]),
          "RepeatTimes": recuringVal+"",
          "scheRefNo" : checkStringVal(saveAlertList[i]["scheRefNo"]),
          "isOneTime": checkStringVal(saveAlertList[i]["isOneTime"]),
          "isSchedule" : checkStringVal(saveAlertList[i]["isSchedule"]),
          "ExpireDays": checkStringVal(saveAlertList[i]["ExpireDays"]),
          "TotalProcessesTransactions":checkStringVal(saveAlertList[i]["TotalProcessesTransactions"]),
          "NextDueDt":checkStringVal(saveAlertList[i]["NextDueDt"]),
          "isEditable":checkStringVal(saveAlertList[i]["isEditable"]),
          "RepeatAs": checkStringVal(saveAlertList[i]["RepeatAs"])
        };
        saveAlertData.push(tempData);
      }
      kony.print("@@@saveAlertData Len:::"+saveAlertData.length);
      frmSend2SaveDashboard.segSend2SaveDashboardDetails.setData(saveAlertData);
      frmSend2SaveDashboard.flxNoResults.setVisibility(false);
      frmSend2SaveDashboard.flxRequestList.setVisibility(true);
      frmSend2SaveDashboard.flxCreateNewRequest.setVisibility(false);
    }else{
      frmSend2SaveDashboard.flxNoResults.setVisibility(false);
      frmSend2SaveDashboard.flxRequestList.setVisibility(false);
      frmSend2SaveDashboard.flxCreateNewRequest.setVisibility(true);
    }
  }catch(e){
    dismissLoadingScreen();
    kony.print("@@@ In setSaveAlertListData() Exception:::"+e);
  }
}

function searchSaveAlertList() {
  try {
    var searchKey = frmSend2SaveDashboard.txtSearch.text;
    if(isNotBlank(searchKey)){
      var keyLen = searchKey.length;
      kony.print("@@@ keyLen:::"+keyLen);
      if(parseInt(keyLen) >= 3){
        var searchData = [];
        for(var i=0; i < gblSaveAlertList.length; i++){
          if(isSearchKeyExists(gblSaveAlertList[i]["Memo"], searchKey) || isSearchKeyExists(gblSaveAlertList[i]["TranAmt"], searchKey)){
            searchData.push(gblSaveAlertList[i]);
          }
        }
        kony.print("@@@searchData.length:::"+searchData.length);
        if(searchData != null && searchData != "" && searchData.length > 0){
          setSaveAlertListData(searchData);
        }else{
          frmSend2SaveDashboard.flxNoResults.setVisibility(true);
          frmSend2SaveDashboard.flxRequestList.setVisibility(false);
          frmSend2SaveDashboard.flxCreateNewRequest.setVisibility(false);
        }
      }
    }else{
      kony.print("---- On Clear the Search ----");
      setSaveAlertListData(gblSaveAlertList);
    }
  } catch (e) {
    kony.print("@@@ In searchSaveAlertList() Exception:::" + e);
  }
}

function isSearchKeyExists(contentVal, keyVal){
  try{
    var flag = false;
    if(isNotBlank(contentVal) && isNotBlank(keyVal)){
      contentVal = contentVal.toUpperCase();
      keyVal = keyVal.toUpperCase();
      var isExists = contentVal.search(keyVal);
      if(isExists >= 0){
        kony.print("---KeyMatched---");
        flag = true;
      }
    }
    return flag;
  }catch(e){
    kony.print("@@@ In isSearchKeyExists() Exception:::"+e);
  }
}

function frmSend2SaveCreateSmartRequestInit() {
  try {
    kony.print("@@@ In frmSend2SaveSmartRequestInit() @@@");
    frmSend2SaveCreateSmartRequest.preShow = frmSend2SaveCreateSmartRequestPreshow;
    frmSend2SaveCreateSmartRequest.postShow = frmSend2SaveCreateSmartRequestPostshow;
    frmSend2SaveCreateSmartRequestActions();
  } catch (e) {
    kony.print("@@@ In frmSend2SaveCreateSmartRequestInit() Exception:::" + e);
  }
}

function frmSend2SaveCreateSmartRequestPreshow() {
  try {
    kony.print("@@@ In frmSend2SaveCreateSmartRequest @@@");
    //frmSend2SaveCreateSmartRequest.lblS2SCreateSmartRequestTitle.text = kony.i18n.getLocalizedString("MB_S2SNewTitle");
    frmSend2SaveCreateSmartRequest.lblPayerPromptPayIDStatic.text = kony.i18n.getLocalizedString("MB_S2SPayerNoLab");
    frmSend2SaveCreateSmartRequest.lblTitlePayerPromptPayID.text = kony.i18n.getLocalizedString("MB_S2SPayerNoLab");
    frmSend2SaveCreateSmartRequest.lblTitleRequestSubject.text = kony.i18n.getLocalizedString("MB_S2SSubjectLab");
    frmSend2SaveCreateSmartRequest.lblAmountTitle.text = kony.i18n.getLocalizedString("MB_S2SAmtLab");
    frmSend2SaveCreateSmartRequest.lblDueDateTitleHeader.text = kony.i18n.getLocalizedString("MB_S2SDuedateLab2");
    frmSend2SaveCreateSmartRequest.lblDueDateTitle.text = kony.i18n.getLocalizedString("MB_S2SDuedateLab2");
    frmSend2SaveCreateSmartRequest.lblRepeat.text = kony.i18n.getLocalizedString("MB_S2SRepatLab2");
    frmSend2SaveCreateSmartRequest.lblReceivedAccount.text = kony.i18n.getLocalizedString("MB_S2SRecieveAcctLab");
    frmSend2SaveCreateSmartRequest.txtValuePromptPayID.placeholder =   kony.i18n.getLocalizedString("MB_S2SPayerNoLab"); 
    frmSend2SaveCreateSmartRequest.txtRequestSubject.placeholder =   kony.i18n.getLocalizedString("MB_S2SSubjectLab");
    frmSend2SaveCreateSmartRequest.txtAmount.placeholder =  "0.00" ;
    frmSend2SaveCreateSmartRequest.lblPayer.text = kony.i18n.getLocalizedString("MB_S2SPayerNameLab");
    //frmSend2SaveCreateSmartRequest.lblTitleRequestSubject.setVisibility(true);
    //frmSend2SaveCreateSmartRequest.lblTitlePayerPromptPayID.setVisibility(false);
  } catch (e) {
    kony.print("@@@ In frmSend2SaveCreateSmartRequestPreshow() Exception:::" + e);
  }
}

function frmSend2SaveCreateSmartRequestPostshow(){
 	if(isSaveAlertAutoFocus == false && gblSavelAlertMode == "normal"){
      //kony.print("@@@in post show",isSaveAlertAutoFocus);
 		 frmSend2SaveCreateSmartRequest.txtValuePromptPayID.setFocus(true);
    }
  addAccessPinKeypad(frmSend2SaveCreateSmartRequest);
}

function frmSend2SaveCreateSmartRequestActions() {
  try {
    kony.print("@@@ In frmSend2SaveCreateSmartRequestActions() @@@111");
    frmSend2SaveCreateSmartRequest.txtValuePromptPayID.onTextChange = ontextChangePromptPayID;
    frmSend2SaveCreateSmartRequest.txtAmount.onTextChange = amountOnTextChangeSaveAlert;
    //#ifdef android
    frmSend2SaveCreateSmartRequest.txtValuePromptPayID.onDone= saveAlertPromptPayInqCallService;
    frmSend2SaveCreateSmartRequest.txtAmount.onDone = onDoneSaveAlertAmount;
    //#endif
    
    //#ifdef iPhone
    frmSend2SaveCreateSmartRequest.txtValuePromptPayID.onEndEditing = saveAlertPromptPayInqCallService;
    frmSend2SaveCreateSmartRequest.txtAmount.onEndEditing = onDoneSaveAlertAmount;
    //#endif
    frmSend2SaveCreateSmartRequest.flxReceivedAccount.onClick = showAccountListScreeen;
    frmSend2SaveCreateSmartRequest.flxDueDate.onClick = showfrmSend2SaveSchedule;
    frmSend2SaveCreateSmartRequest.flxDueDateValues.onClick = showfrmSend2SaveSchedule;
    frmSend2SaveCreateSmartRequest.btnBack.onClick = showfrmSend2SaveDashboard;
    frmSend2SaveCreateSmartRequest.btnNext.onClick = saveAlertNextRequest;
    frmSend2SaveCreateSmartRequest.txtRequestSubject.onTextChange = notAllowEmojiChars;
    frmSend2SaveCreateSmartRequest.txtRequestSubject.onBeginEditing = assignTextNoEmoji;
    frmSend2SaveCreateSmartRequest.btnHeaderBack.onClick = saveAlertDashboardShow;
    frmSend2SaveCreateSmartRequest.txtRequestSubject.onEndEditing = disableLabelRequestSubject;
    frmSend2SaveCreateSmartRequest.txtRequestSubject.onTouchEnd =disableLabelRequestSubject;
  } catch (e) {
    kony.print("@@@ In frmSend2SaveCreateSmartRequestActions() Exception:::" + e);
  }
}

function onDoneSaveAlertAmount(){
  if(gblSavelAlertMode == "normal"){
     saveAlertPromptPayInqCallService();
  }
}

function saveAlertDashboardShow(){
  frmSend2SaveDashboard.show();
}

function saveAlertNextRequest(){
    var toAccNum = frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.text;
    if(gblSavelAlertMode == "edit"){////if(!isNotBlank(toAccNum)){
        gblSaveAlertNext = true;
        saveAlertPromptPayInqCallService(); 
    }else{
       gblSaveAlertNext = false;
       onClickNextSaveAlertCreateRequest();
    }
}
function onClickNextSaveAlertCreateRequest(){
    var toAccNum = frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.text;
    if(gblSavelAlertMode == "normal"){
        if(!validateSaveAlertPromptPayID(toAccNum)){
          return false;
        }
        var reqSubject = frmSend2SaveCreateSmartRequest.txtRequestSubject.text;
        if(!validateSaveAlertRequestSubject(reqSubject)){
          return false;
        }  
    }else{
        if(!validateSaveAlertPromptPayID(toAccNum)){
          return false;
        }
    }

    var enterAmount = frmSend2SaveCreateSmartRequest.txtAmount.text;
    var fromAccNum = frmSend2SaveCreateSmartRequest.lblAccNumber.text;
    var dateField = frmSend2SaveCreateSmartRequest.lblDueDateValue.text;

    if(!validateSaveAlertAmount(enterAmount)){
      return false;
    }
    if(!validateSaveAlertDateFiled(dateField)){
      return false;
    }

    if(isNotBlank(toAccNum) && isNotBlank(enterAmount) && isNotBlank(fromAccNum) && isNotBlank(dateField)){ 
      showAccessPinScreenKeypad();
    }
}

function customerAccountInquirySaveAlertAccounts(objMode) {
	showLoadingScreen();
	var inputParam = {}
    kony.print("objMode>>"+objMode);
    gblSavelAlertMode = objMode;
    isSaveAlertAutoFocus = false; 
	inputParam["saveAlertFlag"] = "true";
    invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackSaveAlertAccounts);
}

function callBackSaveAlertAccounts(status,resulttable){
 try{
	if(status == 400){
      dismissLoadingScreen();
       kony.print("resulttable>>"+JSON.stringify(resulttable));
      	if (resulttable["opstatus"] == 0) {
           if(gblSavelAlertMode == "edit"){
               var length = resulttable["custAcctRec"].length;
               if(length>1){
                   frmSend2SaveCreateSmartRequest.flxReceivedAccount.setEnabled(true);
                   frmSend2SaveCreateSmartRequest.imgArrowReceivedAccount.setVisibility(true);
                   saveAlertCustomerAccounts(status, resulttable);
                   frmSelAccntMutualFund.show();
               }else if(length == 1){
                  var accNameValue = resulttable["custAcctRec"][0]["acctNickName"];
                  var accNumber = formatAccountNumber(resulttable["custAcctRec"][0]["accId"]);
                  frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.text = accNameValue +" "+accNumber;
                  frmSend2SaveCreateSmartRequest.lblProdCode.text = resulttable["custAcctRec"][0]["productID"];
                  frmSend2SaveCreateSmartRequest.lblAccNumber.text = resulttable["custAcctRec"][0]["accId"];
                  frmSend2SaveCreateSmartRequest.lblAccountName.text = resulttable["custAcctRec"][0]["accountName"];
                  frmSend2SaveCreateSmartRequest.lblAccType.text = resulttable["custAcctRec"][0]["accType"];
                  frmSend2SaveCreateSmartRequest.lblAccNickName.text = resulttable["custAcctRec"][0]["acctNickName"];
                  frmSend2SaveCreateSmartRequest.flxReceivedAccount.setEnabled(false);
                  frmSend2SaveCreateSmartRequest.show();
              }
            }else{
              	frmSend2SaveCreateSmartRequest.lblS2SCreateSmartRequestTitle.text = kony.i18n.getLocalizedString("MB_S2SNewTitle");
                frmSend2SaveCreateSmartRequest.lblPayerName.skin  = "lblBlue40px";
                frmSend2SaveCreateSmartRequest.lblValueRequestSubject.skin = "lblBlue40px";
                frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.skin = "lblBlueDBOzone128";
                frmSend2SaveCreateSmartRequest.txtAmount.skin = "txtBlueNormal200";
                frmSend2SaveCreateSmartRequest.lblDueDateValue.skin = "lblBlue40px";
                frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.skin = "lblBlue40px";  
                showOrHideSaveAlertDueDate(true);
              	showOrHideSaveAlertPromptPayTextBox(true);
                frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text = "";
              	frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.text = "";
                frmSend2SaveCreateSmartRequest.txtRequestSubject.text = "";
                frmSend2SaveCreateSmartRequest.txtAmount.text ="";
                frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.text ="";
                frmSend2SaveCreateSmartRequest.lblDueDateValue.text = "";
                frmSend2SaveCreateSmartRequest.btnBack.text = kony.i18n.getLocalizedString("keyCancelButton");
                frmSend2SaveCreateSmartRequest.btnNext.text = kony.i18n.getLocalizedString("Next");
                frmSend2SaveCreateSmartRequest.txtAmount.setVisibility(true);
                frmSend2SaveCreateSmartRequest.lblAmountValue.setVisibility(false);
                showOrHideSaveAlertSubjectTextBox(true);
                frmSend2SaveCreateSmartRequest.btnNext.setEnabled(true);
                frmSend2SaveCreateSmartRequest.flxDueDateValues.setEnabled(true);
                frmSend2SaveCreateSmartRequest.imgDuedate.setVisibility(true);
                frmSend2SaveCreateSmartRequest.lblTitlePayerPromptPayID.setVisibility(false);
                frmSend2SaveCreateSmartRequest.lblTitleRequestSubject.setVisibility(false);
                var accNameValue = resulttable["custAcctRec"][0]["acctNickName"];
                var accNumber = formatAccountNumber(resulttable["custAcctRec"][0]["accId"]);
                frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.text = accNameValue +" "+accNumber;
                frmSend2SaveCreateSmartRequest.lblProdCode.text = resulttable["custAcctRec"][0]["productID"];
                frmSend2SaveCreateSmartRequest.lblAccNumber.text = resulttable["custAcctRec"][0]["accId"];
                frmSend2SaveCreateSmartRequest.lblAccountName.text = resulttable["custAcctRec"][0]["accountName"];
                frmSend2SaveCreateSmartRequest.lblAccType.text = resulttable["custAcctRec"][0]["accType"];
                frmSend2SaveCreateSmartRequest.lblAccNickName.text = resulttable["custAcctRec"][0]["acctNickName"];
                var length = resulttable["custAcctRec"].length;
                kony.print("Accounts length>>"+length);
                frmSend2SaveCreateSmartRequest.imgArrowReceivedAccount.setVisibility(false);
                if(length>1){
                  frmSend2SaveCreateSmartRequest.flxReceivedAccount.setEnabled(true);
                  frmSend2SaveCreateSmartRequest.imgArrowReceivedAccount.setVisibility(true);
                  saveAlertCustomerAccounts(status, resulttable);
                  frmSend2SaveCreateSmartRequest.show();
                }else if(length == 1){
                  frmSend2SaveCreateSmartRequest.flxReceivedAccount.setEnabled(false);
                  frmSend2SaveCreateSmartRequest.show();
               }else{
                  dismissLoadingScreen();
                  showAlert(kony.i18n.getLocalizedString("MB_S2SNoEligibleAcct"), kony.i18n.getLocalizedString("info"));
                  frmSend2SaveDashboard.show();

               }
            }
    }else if (resulttable["opstatus"] == 1) {   
        	dismissLoadingScreen();
            if(resulttable["errCode"] == "NO_VALID_ACCOUNTS") {
              	showAlert(kony.i18n.getLocalizedString("MB_S2SNoEligibleAcct"), kony.i18n.getLocalizedString("info"));
                frmSend2SaveDashboard.show();
            }else {
              	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
  }
 }
}catch(e){
  dismissLoadingScreen();
  kony.print("Exception in callBackSaveAlertAccounts>>"+e);
}
}


function showOrHideSaveAlertPromptPayTextBox(isShow) {
	frmSend2SaveCreateSmartRequest.flxPayerWithTextBox.setVisibility(isShow);
  	frmSend2SaveCreateSmartRequest.flxPayerWithLabel.setVisibility(!isShow);
}

function showOrHideSaveAlertDueDate(isShow) {
    frmSend2SaveCreateSmartRequest.flxDueDate.setVisibility(isShow);
    frmSend2SaveCreateSmartRequest.flxDueDateValues.setVisibility(!isShow);
}

function showOrHideSaveAlertSubjectTextBox(isShow) {
 	frmSend2SaveCreateSmartRequest.txtRequestSubject.setVisibility(isShow);
    frmSend2SaveCreateSmartRequest.lblValueRequestSubject.setVisibility(!isShow);
}

function setAccountDataForSaveAlert(){
   var selectedRow =  frmSelAccntMutualFund.segTransFrm.selectedItems[0];
   kony.print("selectedRow>>"+selectedRow);
   var accNameValue = selectedRow["lblCustName"];
   var accNumber =   formatAccountNumber(selectedRow["lblActNoval"]);
   frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.text = accNameValue +" "+accNumber;
   frmSend2SaveCreateSmartRequest.lblProdCode.text = selectedRow["prodCode"];
   frmSend2SaveCreateSmartRequest.lblAccNumber.text = selectedRow["accountNo"];
   frmSend2SaveCreateSmartRequest.lblAccountName.text = selectedRow["custAcctName"];
   frmSend2SaveCreateSmartRequest.lblAccType.text = selectedRow["tdFlag"];// fromAccType
   frmSend2SaveCreateSmartRequest.lblAccNickName.text = selectedRow["lblCustName"];// fromAccNickname
   frmSend2SaveCreateSmartRequest.show();
}

function showAccountListScreeen(){
  isSaveAlertAutoFocus = true;
  if(gblSavelAlertMode == "normal"){
  	  frmSelAccntMutualFund.show();  
  }else{
    customerAccountInquirySaveAlertAccounts("edit")
  }
}

function onClosefrmMFSelectPurchaseFundMB(){
  if(kony.application.getPreviousForm().id == "frmSend2SaveCreateSmartRequest"){
    frmSend2SaveCreateSmartRequest.show();
  }else{
    frmMFSelectPurchaseFundMB.show();
  }
}

function onClickOfSegmentSaveAlertDashboard(){
  try{
      gblSavelAlertMode = "edit";
      showLoadingScreen(); 
     var selectedRow = frmSend2SaveDashboard.segSend2SaveDashboardDetails.selectedRowItems[0];
     kony.print("selectedRow>>"+selectedRow);
     if(isNotBlank(selectedRow)){
         
       	 gblSaveAlertExistValues.transferAmt  = selectedRow["TranAmt"];
         gblSaveAlertExistValues.fromAcctNo  = selectedRow["SenderAcctNo"];
         gblSaveAlertExistValues.isOneTime  = selectedRow["isOneTime"];
         gblSaveAlertExistValues.isSchedule  = selectedRow["isSchedule"];
         gblSaveAlertExistValues.ExpireDays  = selectedRow["ExpireDays"];
         
       	 showOrHideSaveAlertPromptPayTextBox(false);
       
       	 frmSend2SaveCreateSmartRequest.lblPayerName.text = selectedRow["ReceiverAcctDispName"];
       	 showOrHideSaveAlertDueDate(false);
       	 frmSend2SaveCreateSmartRequest.lblTitleRequestSubject.setVisibility(true);	
         var promptpayNum="";
       	 if(isNotBlank(selectedRow["toAccNumber"])){
            kony.print("toAccNumber>>"+selectedRow["toAccNumber"]);
            if((selectedRow["toAccNumber"].length) == "10" ){
              promptpayNum = formatMobileNumber(selectedRow["toAccNumber"]);
         	}else{
              promptpayNum = convetFormatCitizenField(selectedRow["toAccNumber"]);
            }
          }
       	 frmSend2SaveCreateSmartRequest.lblS2SCreateSmartRequestTitle.text = selectedRow["lblRequestName"];
         frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.setVisibility(true);
         frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.text = promptpayNum;
         frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.text = selectedRow["SenderAcctName"]+" "+formatAccountNumber(selectedRow["SenderAcctNo"]);
         showOrHideSaveAlertSubjectTextBox(false);
         frmSend2SaveCreateSmartRequest.lblValueRequestSubject.text = selectedRow["lblRequestName"];
         frmSend2SaveCreateSmartRequest.lblProxyType.text = selectedRow["ReceiverProxyType"];
         frmSend2SaveCreateSmartRequest.lblProxyValue.text = selectedRow["toAccNumber"];
         frmSend2SaveCreateSmartRequest.lblRecAccNo.text = selectedRow["ReceiverAcctNo"];
         frmSend2SaveCreateSmartRequest.lblAccNumber.text = selectedRow["SenderAcctNo"];
         if(selectedRow["isSchedule"] == "Y"){
            if(selectedRow["isEditable"]=="Y"){
               	kony.print("isEditable is Y and all Blue Skins ");
                frmSend2SaveCreateSmartRequest.lblPayerName.skin  = "lblBlack40px";
                frmSend2SaveCreateSmartRequest.lblValueRequestSubject.skin = "lblBlack40px";
              	frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.skin = "lblGreyRegular128";
                frmSend2SaveCreateSmartRequest.txtAmount.skin = "txtBlueNormal200";
                frmSend2SaveCreateSmartRequest.lblDueDateValue.skin = "lblBlue40px";
                frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.skin = "lblBlue40px";  
                
              	frmSend2SaveCreateSmartRequest.lblAmountValue.setVisibility(false);	
              	frmSend2SaveCreateSmartRequest.txtAmount.setVisibility(true);
                var enterAmount = numberWithCommas(fixedToTwoDecimal(selectedRow["TranAmt"]));
        		frmSend2SaveCreateSmartRequest.txtAmount.text = enterAmount;
              	frmSend2SaveCreateSmartRequest.btnNext.setEnabled(true);
                frmSend2SaveCreateSmartRequest.flxDueDateValues.setEnabled(true);
                frmSend2SaveCreateSmartRequest.imgDuedate.setVisibility(true);
                frmSend2SaveCreateSmartRequest.flxReceivedAccount.setEnabled(true);
                frmSend2SaveCreateSmartRequest.imgArrowReceivedAccount.setVisibility(true);
            }else{
                kony.print("isEditable is N and all Black Skins ");
                frmSend2SaveCreateSmartRequest.lblPayerName.skin = "lblBlack40px";
                frmSend2SaveCreateSmartRequest.lblValueRequestSubject.skin = "lblBlack40px";
                frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.skin = "lblGreyRegular128";
                frmSend2SaveCreateSmartRequest.textAmount.skin = "txtBlackNormal200";
                frmSend2SaveCreateSmartRequest.lblDueDateValue.skin = "lblBlack40px";
                frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.skin = "lblBlack40px";
              
                frmSend2SaveCreateSmartRequest.txtAmount.setVisibility(false);
                frmSend2SaveCreateSmartRequest.lblAmountValue.setVisibility(true);
                var enterAmount = numberWithCommas(fixedToTwoDecimal(selectedRow["TranAmt"]));
                frmSend2SaveCreateSmartRequest.txtAmount.text = enterAmount;
                frmSend2SaveCreateSmartRequest.btnNext.setEnabled(false);
                frmSend2SaveCreateSmartRequest.flxDueDateValues.setEnabled(false);
                frmSend2SaveCreateSmartRequest.imgDuedate.setVisibility(false);
                frmSend2SaveCreateSmartRequest.flxReceivedAccount.setEnabled(false); 
                frmSend2SaveCreateSmartRequest.imgArrowReceivedAccount.setVisibility(false);
            }
           	if(selectedRow["isOneTime"] == "Y"){
               kony.print(" isOneTime is Y");
               var selectedDate = changeDateFormatForSaveAlert(yyymmddToddmmyyyySaveAlert(selectedRow["ExpiryDate"]));
         	   frmSend2SaveCreateSmartRequest.lblDueDateValue.text = selectedDate;
               frmSend2SaveCreateSmartRequest.lblRepeatValue.text = "";
               frmSend2SaveCreateSmartRequest.lblRepeatTimes.text = "";
               frmSend2SaveCreateSmartRequest.lblRepeat.setVisibility(false);
               frmSend2SaveCreateSmartRequest.lblRepeatValue.setVisibility(false);
           }else{
               kony.print(" isOneTime is --- else");
               var selectedDate = changeDateFormatForSaveAlert(yyymmddToddmmyyyySaveAlert(selectedRow["NextDueDt"]));
               frmSend2SaveCreateSmartRequest.lblDueDateValue.text = selectedDate;
               frmSend2SaveCreateSmartRequest.lblRepeatValue.setVisibility(true);
               frmSend2SaveCreateSmartRequest.lblRepeat.setVisibility(true);
               frmSend2SaveCreateSmartRequest.lblRepeatValue.text = selectedRow["RepeatTimes"] +" "+ kony.i18n.getLocalizedString("keyTimesMB");
               frmSend2SaveCreateSmartRequest.lblRepeatTimes.text = selectedRow["RepeatTimes"];
           
           } 
         }else{
            kony.print(" Not Schedule All Black Skins ");
           	frmSend2SaveCreateSmartRequest.lblAmountValue.setVisibility(true);
            var enterAmount = numberWithCommas(fixedToTwoDecimal(selectedRow["TranAmt"]));
           	frmSend2SaveCreateSmartRequest.lblAmountValue.text = enterAmount;
            
            var selectedDate = changeDateFormatForSaveAlert(yyymmddToddmmyyyySaveAlert(selectedRow["ExpiryDate"]));
            frmSend2SaveCreateSmartRequest.lblDueDateValue.text = selectedDate;
            
            frmSend2SaveCreateSmartRequest.lblPayerName.skin  = "lblBlack40px";
            frmSend2SaveCreateSmartRequest.lblValueRequestSubject.skin = "lblBlack40px";
            frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.skin = "lblGreyRegular128";
            frmSend2SaveCreateSmartRequest.lblAmountValue.skin = "txtBlackNormal200";
            frmSend2SaveCreateSmartRequest.lblDueDateValue.skin = "lblBlack40px";
            frmSend2SaveCreateSmartRequest.lblReceivedAccountValue.skin = "lblBlack40px";
           
            frmSend2SaveCreateSmartRequest.btnNext.setEnabled(false);
            frmSend2SaveCreateSmartRequest.lblRepeat.setVisibility(false);
            frmSend2SaveCreateSmartRequest.lblRepeatValue.setVisibility(false);
            frmSend2SaveCreateSmartRequest.txtAmount.setVisibility(false);
            
            frmSend2SaveCreateSmartRequest.flxDueDateValues.setEnabled(false);
            frmSend2SaveCreateSmartRequest.imgDuedate.setVisibility(false);
            frmSend2SaveCreateSmartRequest.imgArrowReceivedAccount.setVisibility(false);
            frmSend2SaveCreateSmartRequest.flxReceivedAccount.setEnabled(false);
           	
         }
        
         frmSend2SaveCreateSmartRequest.btnBack.text = kony.i18n.getLocalizedString("MB_S2SDelbtn");
         frmSend2SaveCreateSmartRequest.btnNext.text = kony.i18n.getLocalizedString("keysave");
         dismissLoadingScreen();
         frmSend2SaveCreateSmartRequest.show();
     }  
  }catch(e){
    kony.print("Exception in onClickOfSegmentSaveAlertDashboard"+e);
  }
}

function saveAlertPromptPayInqCallService(){
	try{
        
        kony.print("saveAlertPromptPayInqCallService fun calling");
		var inputParam = {};
        var toAccNum = "";
        if(gblSavelAlertMode == "normal"){
           toAccNum = frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text;
        }else{
          toAccNum = frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.text;
        }
       
        if(!validateSaveAlertPromptPayID(toAccNum)){
          gblSaveAlertNext = false;
          return false;
        } 
      	var enterAmount = frmSend2SaveCreateSmartRequest.txtAmount.text; 
        enterAmount = numberWithCommas(fixedToTwoDecimal(enterAmount));
        frmSend2SaveCreateSmartRequest.txtAmount.text = enterAmount;
        enterAmount = kony.string.replace(enterAmount, ",", "");
        kony.print("Before inputparams");
		var fromAccNum = frmSend2SaveCreateSmartRequest.lblAccNumber.text;
		var mobileOrCI  = checkMobileOrCIType(toAccNum);
		if(isNotBlank(toAccNum) && isNotBlank(enterAmount) && isNotBlank(fromAccNum)){
          inputParam["toAcctNo"]= removeHyphenIB(toAccNum);
          inputParam["transferAmt"]= enterAmount;
          inputParam["fromAcctNo"]= fromAccNum;
          inputParam["prodCode"]= frmSend2SaveCreateSmartRequest.lblProdCode.text;
          inputParam["mobileOrCI"] = mobileOrCI;
          showLoadingScreen();
          kony.print("inputParam>>"+JSON.stringify(inputParam));
          invokeServiceSecureAsync("saveAlertPromptPayInq", inputParam, callBacksaveAlertPromptPayInqCallService);  
		}
	}catch(e){
		kony.print("@@@ Exception in saveAlertPromptPayInqCallService:"+e);
	}
}

function callBacksaveAlertPromptPayInqCallService(status, resulttable){
   try{
     kony.print("callBacksaveAlertPromptPayInqCallService fun...");
     kony.print("status>>"+status);
     kony.print("resulttbale>>"+JSON.stringify(resulttable));
     if(resulttable["opstatus"]== 0){
        dismissLoadingScreen();
         if(resulttable["toBankCode"] == 11 && (!isNotBlank(resulttable["BPConsent"]) || resulttable["BPConsent"] == "N" )){
           	  showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyErrConsent"), 
              kony.i18n.getLocalizedString("info"), callBackPromptpaySaveAlert);
              return false;
         }
       
        showOrHideSaveAlertPromptPayTextBox(false);
        frmSend2SaveCreateSmartRequest.lblPayerName.text = resulttable["toAcctName"];
         var promptpayNum="";
         if(resulttable["proxyType"] == "02" ){
           		promptpayNum = formatMobileNumber(resulttable["proxyValue"]);
         }else{
           		promptpayNum = convetFormatCitizenField(resulttable["proxyValue"]);
         }
         frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.text = promptpayNum
         frmSend2SaveCreateSmartRequest.lblProxyType.text = resulttable["proxyType"];
         frmSend2SaveCreateSmartRequest.lblProxyValue.text = resulttable["proxyValue"];
         frmSend2SaveCreateSmartRequest.lblBankCode.text = resulttable["toBankCode"];
         frmSend2SaveCreateSmartRequest.lblRecAccNo.text = resulttable["ToAccIdentVal"];//To Account Number
         if(gblSavelAlertMode == "normal"){
             if(frmSend2SaveCreateSmartRequest.flxPayerWithLabel.isVisible == true){
              	kony.print("@@@@on click of flxPayerWithLabel  ");
                frmSend2SaveCreateSmartRequest.flxPayerWithLabel.onClick = onClickflxPayerWithLabel;
            }
         }else{
           frmSend2SaveCreateSmartRequest.flxPayerWithLabel.setEnabled(false);
         }
     	 if(gblSaveAlertNext){
           gblSaveAlertNext = false; 
           onClickNextSaveAlertCreateRequest();
         }    
     }else if(resulttable["opstatus"]== 1){
         gblSaveAlertNext = false;
         dismissLoadingScreen();
         showOrHideSaveAlertPromptPayTextBox(true);
         if(isNotBlank(resulttable["errCode"]) && resulttable["errCode"] == "B247048"){
            var incorrectMoibileOrCI = kony.i18n.getLocalizedString("MIB_P2PkeyErrMobCINoRegConsent");
                kony.print("incorrectMoibileOrCI before:"+incorrectMoibileOrCI);
                incorrectMoibileOrCI = kony.string.replace(incorrectMoibileOrCI, "{mobile_or_ci}", frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text);
                kony.print("incorrectMoibileOrCI After"+incorrectMoibileOrCI);
            	showAlertWithCallBack(incorrectMoibileOrCI, kony.i18n.getLocalizedString("info"), callBackPromptpaySaveAlert);
                return false;
         }else{
        	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));   
            return false;
         }
         
     }
     
   }catch(e){
     	kony.print("@@@ Exception in callBacksaveAlertPromptPayInqCallService:"+e);
   }
}

function saveAlertCompositeServiceDetails(pinValue){
  var inputParam = {};
  var toAccNum = frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.text;
  var enterAmount = frmSend2SaveCreateSmartRequest.txtAmount.text;
  var fromAccNum = frmSend2SaveCreateSmartRequest.lblAccNumber.text;
  var fromAccName = frmSend2SaveCreateSmartRequest.lblAccountName.text;
  var mobileOrCI  = checkMobileOrCIType(toAccNum);
  
  var selRequestDate = "";
  if(gblS2SSendRequestType == "Today"){
    selRequestDate = 0;
  }else{
    selRequestDate = gblS2SSendRequestType;
  }
  kony.print("@@@gblNoofDays::"+gblNoofDays+"||gblS2SSendRequestType:::"+gblS2SSendRequestType+"||gblS2SRepeatType:::"+gblS2SRepeatType+"||selRequestDate::"+selRequestDate);
  
  if(gblS2SSendRequestType == "Today" || parseInt(gblNoofDays) <= parseInt(selRequestDate)){
    inputParam["isSchedule"] = "N";
  }else{
     inputParam["isSchedule"] = "Y";
  }
  if(gblS2SRepeatType == "OneTime"){
     inputParam["isOneTime"] = "Y";
     
  }else{
      inputParam["isOneTime"] = "N";
      
  }
  enterAmount = kony.string.replace(enterAmount,",","");
  
  if (kony.string.containsChars(enterAmount, ["."]))
	enterAmount = enterAmount;
  else
	enterAmount = enterAmount + ".00";
		
  enterAmount = parseFloat(enterAmount);
  enterAmount = enterAmount.toFixed(2);
  inputParam["mobileOrCI"] = mobileOrCI;
  inputParam["bankCode"] = frmSend2SaveCreateSmartRequest.lblBankCode.text;
  inputParam["tranAmt"] = enterAmount;
  inputParam["senderAcctNo"] = fromAccNum;
  inputParam["senderAcctName"] = fromAccName;
  inputParam["receiverProxyId"] = frmSend2SaveCreateSmartRequest.lblProxyValue.text ;
  inputParam["receiverProxyType"] = frmSend2SaveCreateSmartRequest.lblProxyType.text;
  inputParam["fromAcctNickName"] = frmSend2SaveCreateSmartRequest.lblAccNickName.text;
  inputParam["receiverAcctNo"] = frmSend2SaveCreateSmartRequest.lblRecAccNo.text;
  inputParam["receiverAcctDispName"] = frmSend2SaveCreateSmartRequest.lblPayerName.text;
  inputParam["memo"] = frmSend2SaveCreateSmartRequest.txtRequestSubject.text;
  inputParam["expiryDate"] = gblSelectedDateSA;
  inputParam["tmbProduct"] = frmSend2SaveCreateSmartRequest.lblProdCode.text;
  inputParam["NumInsts"] = frmSend2SaveCreateSmartRequest.lblRepeatTimes.text;
  inputParam["fromAcctType"] = frmSend2SaveCreateSmartRequest.lblAccType.text;
  if(gblS2SSendRequestType == "Today"){
     inputParam["xDaysBefore"] = 0;  
  }else{
    inputParam["xDaysBefore"] =gblS2SSendRequestType;  
  }
  
  if(isNotBlank(pinValue)){
      inputParam["password"] =  encryptData(pinValue);  
  } 
  if(gblSavelAlertMode =="edit"){
      inputParam["mode"] = "edit";
      var selectedRow = frmSend2SaveDashboard.segSend2SaveDashboardDetails.selectedRowItems[0];
      inputParam["memo"] = selectedRow["lblRequestName"];
      inputParam["schedRefId"] = selectedRow["RTPTranRef"];
      inputParam["oldReceivedAcctNo"] = gblSaveAlertExistValues["fromAcctNo"]
      inputParam["oldAmount"] = gblSaveAlertExistValues["transferAmt"];
      inputParam["oldFreq"] = selectedRow["RepeatAs"];
      if(isNotBlank(gblSaveAlertExistValues.ExpireDays)){
        inputParam["xDaysBefore"] = gblSaveAlertExistValues.ExpireDays
      }else{
      	inputParam["xDaysBefore"] = 0;  
      }
      
   }
   showLoadingScreen();
   kony.print("inputparams>>"+JSON.stringify(inputParam));
   invokeServiceSecureAsync("saveAlertCompositeService", inputParam, callBackSaveAlertCompositeServiceDetails);
}

function callBackSaveAlertCompositeServiceDetails(status,resulttable){
 try{
  	if (status == 400) {
        kony.print("Printing the result for callBackSaveAlertCompositeServiceDetails "+JSON.stringify(resulttable));
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            kony.print("Form id>>"+kony.application.getCurrentForm().id);
          	if(kony.application.getCurrentForm().id == "frmSend2SaveCreateSmartRequest"){
             	kony.print("if the current form is frmSend2SaveCreateSmartRequest ");
 				closeApprovalKeypad();
                 kony.print("After closing keyPadApprol fun ");
            }
           saveAlertList();
       } else if (resulttable["opstatus"] == 1) {
            if(resulttable["errCode"] == "E10020" || resulttable["errMsg"] == "Wrong Password" ){
              //Wrong password Entered 
            	resetKeypadApproval();
            	var badLoginCount = resulttable["badLoginCount"];
            	var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
            	incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
            	frmSend2SaveCreateSmartRequest.lblForgotPin.text = incorrectPinText;
            	frmSend2SaveCreateSmartRequest.lblForgotPin.skin = "lblBlackMed150NewRed";
            	frmSend2SaveCreateSmartRequest.lblForgotPin.onTouchEnd = doNothing;
                dismissLoadingScreen();
              
            }else if(resulttable["errCode"] == "E10403" || resulttable["errMsg"] == "Password Locked" ){
              // password Locked 
              closeApprovalKeypad();
              dismissLoadingScreen();
              gotoUVPINLockedScreenPopUp();
            }else{
              closeApprovalKeypad();
              dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
              saveAlertList();
            }
          
        }else{
          	dismissLoadingScreen();
        	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            saveAlertList();
        }
        
    }

  }catch(e){
    kony.print("Exception in callBackSaveAlertCompositeServiceDetails:"+e);
  }
}

function deleteSaveAlertRequest(){
  showAlertWithYesNoHandler(kony.i18n.getLocalizedString("MB_S2SConfirmDel"), "", 
                                      kony.i18n.getLocalizedString("keyOK"), kony.i18n.getLocalizedString("keyCancelButton"), 
                                      cancelSaveAlertRequests, saveAlertFailuree);
}


function saveAlertFailuree(){
  kony.print("Save alert failure...");
}

function cancelSaveAlertRequests(){
   var inputParam = {};
   inputParam["mode"] = "delete";
   var selectedRow = frmSend2SaveDashboard.segSend2SaveDashboardDetails.selectedRowItems[0];
   inputParam["schedRefId"] = selectedRow["RTPTranRef"];
   inputParam["isSchedule"] = gblSaveAlertExistValues.isSchedule;
   inputParam["senderAcctName"] = selectedRow["SenderAcctName"];
   inputParam["tranAmt"] = selectedRow["TranAmt"];
   inputParam["receiverProxyId"] = selectedRow["toAccNumber"];
   inputParam["receiverAcctNo"] = selectedRow["SenderAcctNo"];
   inputParam["mobileOrCI"] = checkMobileOrCIType(selectedRow["toAccNumber"]);
   showLoadingScreen();
   invokeServiceSecureAsync("saveAlertCompositeService", inputParam, callBackCancelSaveAlertRequests);
}

function callBackCancelSaveAlertRequests(status, resulttable){
  if (status == 400) {
        kony.print("Printing the result for callBackCancelSaveAlertRequests "+JSON.stringify(resulttable));
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            saveAlertList();
        }else{
          dismissLoadingScreen(); 
          showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
   }
  
}

function saveAlertCustomerAccounts(status, resulttable) {
  var selectedIndex = 0;
  if (status == 400) //success responce
  {
    if (resulttable["opstatus"] == 0) {
      var StatusCode = resulttable["statusCode"];
	  var fromData = []
      var j = 1
      var nonCASAAct = 0;
      gbltranFromSelIndex = [0, 0];
      gbleDonationType = "";
      for (var i = 0; i < resulttable.custAcctRec.length; i++) {
        var accountStatus = resulttable["custAcctRec"][i].acctStatus;
        if (accountStatus.indexOf("Active") == -1) {
          nonCASAAct = nonCASAAct + 1;
        }
        //if (accountStatus.indexOf("Active") >= 0) {// hide /forsen
          var icon = "";
          var iconcategory = "";
          var CCImagetype  = resulttable.custAcctRec[i]["ProductImg"];
          if (resulttable.custAcctRec[i].personalisedAcctStatusCode == "02") {
            continue; 
          }
          icon=loadFromPalleteIcons(resulttable["custAcctRec"][i]["ICON_ID"]); //mki, mib-11372
          kony.print("^^^^^Mutual fund MB^^^^^"+icon);
          iconcategory = resulttable.custAcctRec[i]["ICON_ID"];
          if (iconcategory == "ICON-01" || iconcategory == "ICON-02"|| iconcategory == "ICON-03"|| iconcategory == "ICON-04"){
            if (iconcategory == "ICON-01" || iconcategory == "ICON-02") {
            	var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew1, icon)
            } else if (iconcategory == "ICON-03") {
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew2, icon)
            } else if (iconcategory == "ICON-04") {
                var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew3, icon)
            }
          } else if(iconcategory == "ICON-06" || iconcategory == "ICON-07"){
            kony.print("esult for 06 and 07 "+JSON.stringify(resulttable.custAcctRec[i]));
            if(CCImagetype == "SoSmart"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderSoSmart, icon)
            }else if(CCImagetype == "SoChill"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderSoChill, icon)
            }else if(CCImagetype == "SoFast"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderSofast, icon)
            }else if(CCImagetype == "RoyalTopBrass"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderTopBrass, icon)
            }else if(CCImagetype == "VisaDefault"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderDefaultVisa, icon)
            }else if(CCImagetype == "MasterDefault"){
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderDefaultMaster, icon)
            }else 
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderDefaultVisa, icon)
            }
           
          //}
          var jointActXfer = resulttable.custAcctRec[i].partyAcctRelDesc;
          if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {

          } else {

            kony.table.insert(fromData, temp[0])
          }
          j++;
        }
      gblNoOfFromAcs = fromData.length;
      for (var i = 0; i < gblNoOfFromAcs; i++) {
        if (glb_accId == fromData[i].accountNo) {
          selectedIndex = i;
          glb_accId = 0;
          break;
        }
      }
      if (selectedIndex != 0)
        gbltranFromSelIndex = [0, selectedIndex];
      else
        gbltranFromSelIndex = [0, 0];

      frmSelAccntMutualFund.segTransFrm.widgetDataMap = [];
      frmSelAccntMutualFund.segTransFrm.widgetDataMap = {
        lblAcntType: "lblAcntType",
        img1: "img1",
        lblCustName: "lblCustName",
        lblBalance: "lblBalance",
        lblActNoval: "lblActNoval",
        lblDummy: "lblDummy",
        lblSliderAccN2: "lblSliderAccN2",
        lblRemainFee: "lblRemainFee",
        lblRemainFeeValue: "lblRemainFeeValue"
      }
      frmSelAccntMutualFund.segTransFrm.data = [];
      if (nonCASAAct == resulttable.custAcctRec.length) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
        return false;
      }
      if (fromData.length == 0) {
        frmSelAccntMutualFund.segTransFrm.setVisibility(false);
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
        return false;
      } else {
        if (gblDeviceInfo["name"] == "android") {
          if (fromData.length == 1) {
            frmSelAccntMutualFund.segTransFrm.viewConfig = {
              "coverflowConfig": {
                "rowItemRotationAngle": 0,
                "isCircular": false,
                "spaceBetweenRowItems": 10,
                "projectionAngle": 90,
                "rowItemWidth": 80
              }
            };
          } else {
            frmSelAccntMutualFund.segTransFrm.viewConfig = {
              "coverflowConfig": {
                "rowItemRotationAngle": 0,
                "isCircular": true,
                "spaceBetweenRowItems": 10,
                "projectionAngle": 90,
                "rowItemWidth": 80
              }
            };
          }
        }
        frmSelAccntMutualFund.segTransFrm.data = fromData;
        frmSelAccntMutualFund.segTransFrm.selectedIndex = gbltranFromSelIndex;
      }
      //billpaymentNoOfAccounts = fromData.length;
      dismissLoadingScreen();
      frmSelAccntMutualFund.LabelHeader.text = kony.i18n.getLocalizedString("MF_PRC_lbl_select_Account"); 
      frmSelAccntMutualFund.btnTransCnfrmCancel.text = kony.i18n.getLocalizedString("keyOK"); 
      //frmSelAccntMutualFund.show();
    } else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  } else {
    dismissLoadingScreen();
  }
}

// This function will check given input is Mobile or Citizen id
function checkMobileOrCIType(inValue){
  var outValue ="";
  if(isNotBlank(inValue)){
     var resultantString = kony.string.startsWith(inValue, "0", true)
     if(resultantString){
       outValue = "02";
     }else{
       outValue = "01";
     }
  }
  return outValue;
}

function ontextChangePromptPayID(){
  var outValue ="";
  var inPutval = frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text;
  if(isNotBlank(inPutval)){
     if (Gbl_StartDigsMobileNum.indexOf(inPutval.substring(0,2)) >= 0){
       	outValue = onTextChangeFormatMobileNumber(inPutval);  
        frmSend2SaveCreateSmartRequest.txtValuePromptPayID.maxTextLength = 12;
        frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text = outValue;
     }else{
        outValue = ontextChangeFomatCitizenIDNumber(inPutval); 
        frmSend2SaveCreateSmartRequest.txtValuePromptPayID.maxTextLength = 17;
        frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text = outValue;
     }
   }
}

function ontextChangeFomatCitizenIDNumber(txt){
  if (txt == null) return false;
  	
  	var numChars = txt.length;
  	var temp = "";
 	var i, txtLen = numChars;
  	var currLen = numChars;
    var iphenText = txt;
  	if (gblPrevLen < currLen) {
    	for (i = 0; i < numChars; ++i) {
      		if (txt[i] != '-') {
        		temp = temp + txt[i];
      		} else {
        		txtLen--;
      		}
    	}
    	iphenText = "";
    	for (i = 0; i < txtLen; i++) {
      		iphenText += temp[i];
      		if (i == 0 || i == 4 || i == 9 || i == 11) {
              iphenText += '-';
            }
        }
    	
  	}
  	gblPrevLen = currLen;
  return iphenText;

}

function onTextChangeFormatMobileNumber(txt){
  if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
    var iphenText = txt;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 5) {
				iphenText += '-';
			}
		}
	}
	gblPrevLen = currLen;
   return iphenText;
}

function callBackPromptpaySaveAlert(){
  frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text = "";
  frmSend2SaveCreateSmartRequest.txtValuePromptPayID.setFocus(true);
}
function callBackSaveAlertAmountFields(){
  frmSend2SaveCreateSmartRequest.txtAmount.text ="";
  frmSend2SaveCreateSmartRequest.txtAmount.setFocus(true);
}

function callBackRequestSubjectSaveAlert(){
  frmSend2SaveCreateSmartRequest.txtRequestSubject.setFocus(true);
}

function onClickS2SIntroNext(){
  try{
    kony.print("@@inside onClickS2SIntroNext"+gblNextIndex);
  gblNextIndex += 1;
    kony.print("@@after gblNextIndex vale"+gblNextIndex);
  frmSend2SaveSmartRequest.segSend2SaveSmartRequest.selectedIndex = [0, gblNextIndex];
  frmS2SSuccessSetVisible();
    }catch(e){
    kony.print("Exception in onClickS2SIntroNext:"+e);
  }
}

function frmS2SSuccessSetVisible(){
kony.print("@@inside frmS2SSuccessSetVisible"+gblNextIndex);
 if (gblNextIndex == 1) {
        frmSend2SaveSmartRequest.btnNext.isVisible = true;
        frmSend2SaveSmartRequest.btnSkipIntro.isVisible = true;
        frmSend2SaveSmartRequest.btnStartUsing.isVisible = false;
      } else if (gblNextIndex == 2) {
        frmSend2SaveSmartRequest.btnNext.isVisible = false;
        frmSend2SaveSmartRequest.btnSkipIntro.isVisible = false;
        frmSend2SaveSmartRequest.btnStartUsing.isVisible = true;
      }else {
        frmSend2SaveSmartRequest.btnNext.isVisible = true;
        frmSend2SaveSmartRequest.btnSkipIntro.isVisible = true;
        frmSend2SaveSmartRequest.btnStartUsing.isVisible = false;
      }
}

//  input Date format should be dd/MM/yyyy
function changeDateFormatForSaveAlert(enteredDateString) {
    var monthNames = {
      "01": kony.i18n.getLocalizedString("keyCalendarJan"),
      "02": kony.i18n.getLocalizedString("keyCalendarFeb"),
      "03": kony.i18n.getLocalizedString("keyCalendarMar"),
      "04": kony.i18n.getLocalizedString("keyCalendarApr"),
      "05": kony.i18n.getLocalizedString("keyCalendarMay"),
      "06": kony.i18n.getLocalizedString("keyCalendarJun"),
      "07": kony.i18n.getLocalizedString("keyCalendarJul"),
      "08": kony.i18n.getLocalizedString("keyCalendarAug"),
      "09": kony.i18n.getLocalizedString("keyCalendarSep"),
      "10": kony.i18n.getLocalizedString("keyCalendarOct"),
      "11": kony.i18n.getLocalizedString("keyCalendarNov"),
      "12": kony.i18n.getLocalizedString("keyCalendarDec")
    };
	if(isNotBlank(enteredDateString)){
		kony.print("enteredDateString>>"+enteredDateString);
		var tokenizedDate = enteredDateString.split("/", 3);
		var dd = tokenizedDate[0];
		var mm = tokenizedDate[1];
		var yyyy = tokenizedDate[2];
		gblSelectedDateSA =  dd + '/' + mm + '/' + yyyy; 
		kony.print("gblSelectedDateSA>>"+gblSelectedDateSA);
        enteredDateString =  dd +" " + monthNames[mm]+" "+yyyy;
		kony.print("enteredDateString after format:"+enteredDateString);
		
	}
	return enteredDateString;
}

function yyymmddToddmmyyyySaveAlert(inputDate){
   var dueDate = inputDate;
  if(isNotBlank(inputDate)){
       dueDate =  inputDate.split("-", 3);
       dueDate = dueDate[2] +"/" + dueDate[1] + "/" + dueDate[0];
       kony.print("ExpiryDate after convert>>"+ dueDate);
  }
  return dueDate;
}

function amountOnTextChangeSaveAlert(){
  try{
   	kony.print("amountOnTextChange @@@@");
   	frmSend2SaveCreateSmartRequest.txtAmount.maxTextLength = 14;
   	var enterAmount = frmSend2SaveCreateSmartRequest.txtAmount.text;
   	enterAmount = kony.string.replace(enterAmount, ",", "");
    if(enterAmount.length > 0 && parseFloat(enterAmount, 10) == 0){
      frmSend2SaveCreateSmartRequest.txtAmount.text = commaFormattedTransfer(enterAmount);
      frmSend2SaveCreateSmartRequest.txtAmount.text = numberWithCommas(fixedToTwoDecimal(enterAmount));
    }
  }catch(e) {
    kony.print("Exception in amountOnTextChangeSaveAlert"+e);
  } 
}

function disableLabelPromptPay(){
  if (!(isNotBlank(frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text))){
    //alert("@@here123");
    frmSend2SaveCreateSmartRequest.lblTitlePayerPromptPayID.setVisibility(false);
  }else{
    frmSend2SaveCreateSmartRequest.lblTitlePayerPromptPayID.setVisibility(true);
  }
}


function disableLabelRequestSubject(){
  if (!(isNotBlank(frmSend2SaveCreateSmartRequest.txtRequestSubject.text))){
    //alert("@@here456");
    frmSend2SaveCreateSmartRequest.lblTitleRequestSubject.setVisibility(false);
  }else{
    frmSend2SaveCreateSmartRequest.lblTitleRequestSubject.setVisibility(true);
  }
}


function validateSaveAlertPromptPayID(toAccNum){
   if(isNotBlank(toAccNum)){
       toAccNum = removeHyphenIB(toAccNum);
       if (Gbl_StartDigsMobileNum.indexOf(toAccNum.substring(0,2)) >= 0){
             if(toAccNum.length == 10){
                  if (!recipientMobileVal(toAccNum)){
                     showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyEnteredMobileNumberisnotvalid"), kony.i18n.getLocalizedString("info"), callBackPromptpaySaveAlert);
                     return false;
                  } else{
                     kony.print("@@@@acc no successfull");
                     disableLabelPromptPay();
                  } 
            }else{
                  showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyEnteredMobileNumberisnotvalid"), kony.i18n.getLocalizedString("info"), callBackPromptpaySaveAlert);
                  return false;
            }

       }else{
           if(toAccNum.length == 13){
               if (!checkCitizenID(toAccNum)){
                   showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"), kony.i18n.getLocalizedString("info"), callBackPromptpaySaveAlert);
                   return false;
               } else{
                   kony.print("@@@@acc no successfull");
                   disableLabelPromptPay();
               } 
           }else{
              showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"), kony.i18n.getLocalizedString("info"), callBackPromptpaySaveAlert);
              return false;
           }
       }
   }else{
       showAlertWithCallBack(kony.i18n.getLocalizedString("MB_S2SEnterPP"), kony.i18n.getLocalizedString("info"), callBackPromptpaySaveAlert);
       return false;
   }
  return true;
}


function validateSaveAlertRequestSubject(reqSubject){
  if(!isNotBlank(reqSubject)){
		showAlert(kony.i18n.getLocalizedString("MIB_S2SNoSubject"), kony.i18n.getLocalizedString("info"),callBackRequestSubjectSaveAlert);
		return false;
  }
  return true;
}

function validateSaveAlertAmount(enterAmount){
    if(isNotBlank(enterAmount)) {
        enterAmount = kony.string.replace(enterAmount, ",", "");
        var isAmountValid = amountValidationMB(enterAmount);
        if (!isAmountValid) {
          showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"), callBackSaveAlertAmountFields);
          return false;
        }
    }else{
        showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyPleaseEnterAmount"), kony.i18n.getLocalizedString("info"), callBackSaveAlertAmountFields);
		return false;
    }
   return true;
}

function validateSaveAlertDateFiled(dateField){
    if(!isNotBlank(dateField)){
     	showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_S2SNoDuedate"), kony.i18n.getLocalizedString("info") );
     	return false;
   }
  
   return true;
  
}

function onClickflxPayerWithLabel(){
      var promptpayNum = frmSend2SaveCreateSmartRequest.lblPayerPromptPayNumberStatic.text;
      promptpayNum = removeHyphenIB(promptpayNum)
      kony.print("@@promptpayNum@"+promptpayNum);
      if (Gbl_StartDigsMobileNum.indexOf(promptpayNum.substring(0,2)) >= 0){
           promptpayNum = formatMobileNumber(promptpayNum);
       }else{
           promptpayNum = convetFormatCitizenField(promptpayNum);
      }
  	  showOrHideSaveAlertPromptPayTextBox(true);
      frmSend2SaveCreateSmartRequest.txtValuePromptPayID.text = promptpayNum;
  	  frmSend2SaveCreateSmartRequest.txtValuePromptPayID.setFocus(true);
}
